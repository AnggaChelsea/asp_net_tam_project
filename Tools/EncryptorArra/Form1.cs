using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Xml;

namespace EncryptorArra
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //static string Encrypt(string textToEncrypt)
        //{
        //    try
        //    {
        //        //string textToEncrypt = "WaterWorld";
        //        string ToReturn = "";
        //        string publickey = "12345678";
        //        string secretkey = "87654321";
        //        byte[] secretkeyByte = { };
        //        secretkeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
        //        byte[] publickeybyte = { };
        //        publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
        //        MemoryStream ms = null;
        //        CryptoStream cs = null;
        //        byte[] inputbyteArray = System.Text.Encoding.UTF8.GetBytes(textToEncrypt);
        //        using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
        //        {
        //            ms = new MemoryStream();
        //            cs = new CryptoStream(ms, des.CreateEncryptor(publickeybyte, secretkeyByte), CryptoStreamMode.Write);
        //            cs.Write(inputbyteArray, 0, inputbyteArray.Length);
        //            cs.FlushFinalBlock();
        //            ToReturn = Convert.ToBase64String(ms.ToArray());
        //        }
        //        return ToReturn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message, ex.InnerException);
        //    }
        //}

        //static string Decrypt(string textToDecrypt)
        //{
        //    try
        //    {
        //        //string textToDecrypt = "6+PXxVWlBqcUnIdqsMyUHA==";
        //        string ToReturn = "";
        //        string publickey = "12345678";
        //        string secretkey = "87654321";
        //        byte[] privatekeyByte = { };
        //        privatekeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
        //        byte[] publickeybyte = { };
        //        publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
        //        MemoryStream ms = null;
        //        CryptoStream cs = null;
        //        byte[] inputbyteArray = new byte[textToDecrypt.Replace(" ", "+").Length];
        //        inputbyteArray = Convert.FromBase64String(textToDecrypt.Replace(" ", "+"));
        //        using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
        //        {
        //            ms = new MemoryStream();
        //            cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
        //            cs.Write(inputbyteArray, 0, inputbyteArray.Length);
        //            cs.FlushFinalBlock();
        //            Encoding encoding = Encoding.UTF8;
        //            ToReturn = encoding.GetString(ms.ToArray());
        //        }
        //        return ToReturn;
        //    }
        //    catch (Exception ae)
        //    {
        //        throw new Exception(ae.Message, ae.InnerException);
        //    }
        //}

        public static string Encrypt(string dataToEncrypt, string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("EncryptionKey", "Please initialize your encryption key.");

            if (string.IsNullOrEmpty(dataToEncrypt))
                return string.Empty;

            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                //aes.Key = Encoding.UTF8.GetBytes(key);
                aes.Key = CreateKey(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(dataToEncrypt);
                        }
                        array = memoryStream.ToArray();
                    }
                }
            }
            string result = Convert.ToBase64String(array);
            return result;
        }

        public static string Decrypt(string dataToDecrypt, string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("EncryptionKey", "Please initialize your encryption key.");

            if (string.IsNullOrEmpty(dataToDecrypt))
                return string.Empty;

            byte[] iv = new byte[16];

            using (Aes aes = Aes.Create())
            {
                //aes.Key = Encoding.UTF8.GetBytes(key);
                aes.Key = CreateKey(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                var buffer = Convert.FromBase64String(dataToDecrypt);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        private static byte[] CreateKey(string password, int keyBytes = 32)
        {
            byte[] Salt = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80 };
            const int Iterations = 300;
            var keyGenerator = new Rfc2898DeriveBytes(password, Salt, Iterations);
            return keyGenerator.GetBytes(keyBytes);
        }

        public static string getKey()
        {
            XmlDocument xmlDcoument = new XmlDocument();
            xmlDcoument.Load("config.xml");
            XmlNodeList? xmlNodeList = xmlDcoument.DocumentElement.SelectNodes("/config");
            //foreach (XmlNode xmlNode in xmlNodeList)
            //{
            string output = xmlNodeList[0].SelectSingleNode("key").InnerText;
            return output;
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                string public_key;
                string raw_text;
                string encrypt_text;

                //public_key = txtPublicKey.Text;
                public_key = getKey();
                raw_text = txtRawText.Text;
                encrypt_text = Encrypt(raw_text, public_key);

                txtEncryptedText.Text = encrypt_text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                string public_key;
                string raw_text;
                string encrypt_text;

                //public_key = txtPublicKey.Text;
                public_key = getKey();
                encrypt_text = txtEncryptedText.Text;
                raw_text = Decrypt(encrypt_text, public_key);

                txtRawText.Text = raw_text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}