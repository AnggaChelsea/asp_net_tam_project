﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class Validation
    {
        public Int32 UniqueId { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string ValidationType { get; set; }
        public string RuleType { get; set; }
        public string FieldName { get; set; }
        public string ValueExpression { get; set; }
        public string OtherCondsExpres { get; set; }
        public string ValidMsg { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
