﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportTableWeb
    {
        [Key]
        public long UniqueId { get; set; }        
        public string ReportCd { get; set; }                             
        public string TableCd { get; set; }                             
        public string TableDesc { get; set; }                             
        public string TableFilter { get; set; }                             
        public int TableSequence { get; set; }                             
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
