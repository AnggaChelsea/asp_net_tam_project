﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class GenerateTextFile
    {
        public Int32 UniqueId { get; set; }
        public DateTime ReportDate { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string OjkFormCode { get; set; }
        public string FormName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public long TotalRecord { get; set; }
        public string IdOperational { get; set; }
        public string FilePeriodType { get; set; }
        public Boolean Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
