﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class TransformFormMapping
    {
        [Key]
        public Int16 UniqueId { get; set; }
        public string ProcessNo { get; set; }
        public string FormCode { get; set; }
    }
}
