﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class InterFormValidationResult
    {
        public Int32 UniqueId { get; set; }
        public DateTime DataDate { get; set; }
        public string PeriodType { get; set; }
        public string SrcFormCode { get; set; }
        public string SrcSandi { get; set; }
        public decimal? SrcAmount { get; set; }
        public string CmpFormCode { get; set; }
        public string CmpSandi { get; set; }
        public decimal? CmpAmount { get; set; }
        public string CmpValidSts { get; set; }
        public decimal? Difference { get; set; }
        public string Msg { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
        public string Branch { get; set; }
    }
}
