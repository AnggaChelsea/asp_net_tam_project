﻿using System;

namespace ARRA.MODULE.Domain.Entities
{
    public class ValidationScenarioResult
    {
        public Int64 UniqueId { get; set; }
        public DateTime ReportingDate { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string Branch { get; set; }
        public string ValidMessage { get; set; }
        public string IdAssertion { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}