﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportRow
    {
        public long UniqueId { get; set; }
        [Key]
        public string ReportCd { get; set; }        
        public int RowSeq { get; set; }
        public string RowDesc { get; set; }
        public string ComponentCd { get; set; }
        public string DbNm { get; set; }
        public string TableNm { get; set; }
        public int ParentRowSeq { get; set; }
        public string Text { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
        public string FormCodeDetail { get; set; }
    }
}
