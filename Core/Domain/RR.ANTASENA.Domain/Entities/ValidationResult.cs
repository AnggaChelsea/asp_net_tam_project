﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class ValidationResult
    {
        public Int64 UniqueId { get; set; }
        public string FormCode { get; set; }
        public string Branch { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public Int32 ValidNo { get; set; }
        public string ValidMsg { get; set; }
        public Int64 UniqueIdRef { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
