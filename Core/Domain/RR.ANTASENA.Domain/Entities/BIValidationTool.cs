﻿using System;

namespace ARRA.MODULE.Domain.Entities
{
    public class BIValidationTool
    {
        public Int16 UniqueId { get; set; }
        public string PeriodType { get; set; }
        public string ProcessNo { get; set; }
        public string ProcessName { get; set; }
        public string ETLPackage { get; set; }
        public string FormCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}