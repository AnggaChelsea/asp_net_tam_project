﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportColumn
    {
        [Key]
        public long UniqueId { get; set; }        
        public string ReportCd { get; set; }        
        public int ColumnSeq { get; set; }        
        public string ColumnExcelPos { get; set; }        
        public string ColumnDest { get; set; }        
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
