﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportMaster
    {
        public long UniqueId { get; set; }
        [Key]
        public string ReportCd { get; set; }        
        public string ReportDesc { get; set; }
        public string ReportTemplate { get; set; }
        public string ReportSheet { get; set; }
        public string FormCdDtl { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
