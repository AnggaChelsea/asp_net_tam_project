﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportRowColumn
    {
        [Key]
        public long UniqueId { get; set; }        
        public string ReportCd { get; set; }        
        public int RowSeq { get; set; }             
        public int ColumnSeq { get; set; }             
        public string ColumnFormula { get; set; }             
        public string ColumnStatic { get; set; }             
        public string IsRecalculate { get; set; }             
        public int RecalculateSeq { get; set; }    
        public string RecalculateType { get; set; }
        public string ColumnAggregate { get; set; }
        public string DivisorAfterAggregate { get; set; }
        public string RoundedAfterAggregate { get; set; }
        public string AbsoluteAfterAggregate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
