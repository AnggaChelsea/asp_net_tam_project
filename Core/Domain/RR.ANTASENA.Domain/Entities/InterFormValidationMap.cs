﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class InterFormValidationMap
    {
        public Int32 UniqueId { get; set; }
        public string PeriodType { get; set; }
        public string SrcFormCode { get; set; }
        public string SrcSandiField { get; set; }
        public string SrcSandi { get; set; }
        public string SrcAmoutField { get; set; }
        public string SrcOtherCondition { get; set; }
        public string CmpFormCode { get; set; }
        public string CmpSandiField { get; set; }
        public string CmpSandi { get; set; }
        public string CmpAmoutField { get; set; }
        public string CmpOtherCondition { get; set; }
        public string Message { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
