﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class ProcessLogHeader
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public DateTime? DataDate { get; set; }
        public string PeriodType { get; set; }
        public string ProcessType { get; set; }
        public string ProcessNo { get; set; }
        public string ProcessName { get; set; }
        public string Branch { get; set; }
        public string ProcessStatus { get; set; }
        public string Remark { get; set; }
        public Int64? QueueId { get; set; }
        public Int64? TotalRow { get; set; }
        public Int64? TotalFailed { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
