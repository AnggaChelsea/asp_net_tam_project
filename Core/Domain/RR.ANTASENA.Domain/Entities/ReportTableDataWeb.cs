﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportTableDataWeb
    {
        [Key]
        public long UniqueId { get; set; }
        public string ReportCd { get; set; }
        public int ColumnSeq { get; set; }
        public string ColumnDesc { get; set; }
        public string TableCd { get; set; }
        public string ColumnData { get; set; }
        public string ColumnAlias{ get; set; }
        public string IsHide { get; set; }
        public string IsCompare { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
