﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.MODULE.Domain.Entities
{
    public class ProcessDependencies
    {
        public Int16 UniqueId { get; set; }
        public string ProcessType { get; set; }
        public string ProcessNo { get; set; }
        public string DependProcessNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
