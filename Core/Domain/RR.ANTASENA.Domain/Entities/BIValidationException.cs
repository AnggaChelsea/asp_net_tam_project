﻿using System;

namespace ARRA.MODULE.Domain.Entities
{
    public class BIValidationException
    {
        public long UniqueId { get; set; }
        public DateTime ReportDate { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string IdPelapor { get; set; }
        public string ValidationStatus { get; set; }
        public string ValidationTest { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
