﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ARRA.MODULE.Domain.Entities
{
    public class ReportFilter
    {
        [Key]
        public long UniqueId { get; set; }        
        public string ReportCd { get; set; }        
        public int RowSeq { get; set; }             
        public int ColumnSeq { get; set; }             
        public string Prefix { get; set; }             
        public string ColumnName { get; set; }             
        public string Expression { get; set; }             
        public string ColumnVal { get; set; }             
        public string Operator { get; set; }             
        public string Suffix { get; set; }             
        public int FilterSeq { get; set; }             
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
