﻿using System;

namespace ARRA.MODULE.Domain.Entities
{
    public class ValidationExceptionSummary
    {
        public Int64 UniqueId { get; set; }
        public DateTime ReportingDate { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string Branch { get; set; }
        public string FieldName { get; set; }
        public string IdAssertion { get; set; }
        public string LabAssertion { get; set; }
        public string ColumnName { get; set; }
        public Int64? TotalRecord { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}