﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum OperationType
    {
        ADD,
        EDIT,
        REMOVE,
        UPLOAD,
        EXPORT,
        APPROVAL,
        REPLACE
    }
}
