﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum ApprovalStatus
    {
        Pending,
        Approved,
        Rejected,
        Revise,
        Revoke
    }
}