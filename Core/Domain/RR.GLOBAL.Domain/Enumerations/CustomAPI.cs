﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum CustomAPI
    {
        EDITOR_API,
        LOOKUP_API,
        DATA_API,
        DATA_DETAIL_API,
        SAVE_DATA_API,
        SAVE_FILTER_API,
        EXP_TASK_API,
        DOWNLOAD_API,
        UPLOAD_API,
        SELECT_ALL_API,
        SELECT_DEPEND_API,
        ACTION_API
    }
}