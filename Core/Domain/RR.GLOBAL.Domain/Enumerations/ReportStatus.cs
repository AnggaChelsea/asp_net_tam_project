﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum ReportStatus
    {
        DONE,
        PROGRESS
    }
}
