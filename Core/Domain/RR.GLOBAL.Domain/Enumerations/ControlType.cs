﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum ControlType
    {
        NUMBER,
        TEXT,
        LOOKUP,
        DATE,
        DATETIME,
        MONEY,
        TIME,
        PERCENTAGE,
        BOOL,
        MULTICHOICE
    }
}