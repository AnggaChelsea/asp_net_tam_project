﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum AuditType
    {
        ADD,
        EDIT,
        REMOVE,
        PROCESS,
        CANCEL_PROCESS
    }
}
