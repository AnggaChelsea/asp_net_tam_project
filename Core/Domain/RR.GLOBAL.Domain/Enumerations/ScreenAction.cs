﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum ScreenAction
    {
        ADD,
        ADD_DTL,
        EDIT,
        EDIT_DTL,
        REMOVE,
        REMOVE_DTL,
        IMPORT,
        MANUAL_UPLOAD
    }
}
