﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum FilterValueType
    {
        FIX,
        CURR_DATE,
        PREV_DATE,
        CURR_RPT_DAILY,
        CURR_RPT_WEEK,
        CURR_RPT_MONTH,
        CURR_RPT_TRIWULAN,
        CURR_RPT_SEMESTER,
        CURR_RPT_YEAR
    }
}
