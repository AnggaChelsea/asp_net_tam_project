﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum PeriodType
    {
        ADHOC,
        DAILY,
        WEEKLY,
        MONTHLY,
        QUARTERLY,
        SEMESTER,
        YEARLY
    }
}
