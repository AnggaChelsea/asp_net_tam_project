﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum SystemParameterType
    {
        EXPORT_FILE_LOCATION,
        //EXPORT_FILE_LOCATION_SHARE,
        IMPORT_FILE_LOCATION,
        //IMPORT_FILE_LOCATION_SHARE,
        TEXT_FILE_LOCATION,
        //TEXT_FILE_LOCATION_SHARE,
        TEXT_FILE_TYPE,
        SOURCE_FILE_LOCATION,
        //SOURCE_FILE_LOCATION_SHARE,
        TEXT_FILE_METADATA_VERSION,
        SANDI_BANK,
        TEXT_FILE_PERIOD_RULE,
        MANUAL_FILE_PATH,
        //MANUAL_FILE_PATH_SHARE,
        BI_TF_HEADER,
        KONSOLIDASI_BR
    }
}