﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum LoginStatus
    {
        Success,
        Failed,
        Logout
    }
}
