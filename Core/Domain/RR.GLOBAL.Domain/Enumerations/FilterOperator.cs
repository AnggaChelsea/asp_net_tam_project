﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Enumerations
{
    public enum FilterOperator
    {
        CONTAINS,
        NOTCONTAINS,
        INCLUDE,
        NOTINCLUDE,
        EQUALS,
        NOTEQUALS,
        EMPTY,
        NOTEMPTY,
        STARTWITH,
        ENDWITH,
        GREATER,
        GREATERTHAN,
        LESS,
        LESSTHAN
    }
}