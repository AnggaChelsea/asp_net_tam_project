﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class UserFormFilter
    {
        public Int32 UniqueId { get; set; }
        public string UserId { get; set; }
        public string FormId { get; set; }
        public string FieldName { get; set; }
        public Int16? FieldSeq { get; set; }
        public string Operator { get; set; }
        public string DefaultValue { get; set; }
        public string DefaultValueType { get; set; }
        public Boolean? UseLookup { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
