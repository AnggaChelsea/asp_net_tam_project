﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ApprovalDetail
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public Int64 HeaderId { get; set; }
        public string FieldName { get; set; }
        public string FormColName { get; set; }
        public Int16? FormSeq { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public bool? KeyConds { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
