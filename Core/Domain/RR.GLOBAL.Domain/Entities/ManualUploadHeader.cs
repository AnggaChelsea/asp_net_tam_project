﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ManualUploadHeader
    {
        public Int32 UniqueId { get; set; }
        public string Module { get; set; }
        [Key]
        public string ProcessNo { get; set; }
        public string ProcessName { get; set; }
        public string FileType { get; set; }
        public string DelimitedChar { get; set; }
        public string TargetTable { get; set; }
        public string TemplateName { get; set; }
        public string UploadType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
