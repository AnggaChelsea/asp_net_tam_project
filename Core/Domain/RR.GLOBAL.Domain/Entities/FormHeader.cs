﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class FormHeader
    {
        public Int32 UniqueId { get; set; }
        public string Module { get; set; }
        [Key]
        public string FormCode { get; set; }
        public string FormName { get; set; }
        public string OJKFormCode { get; set; }
        public string FormDesc { get; set; }
        public string TableName { get; set; }
        public string OriginalTableName { get; set; }
        public string FilterConditionEx { get; set; }
        public string LinkToFormCode { get; set; }
        public string TFType { get; set; }
        public string TFDelimtedChar { get; set; }
        public string FormOrder { get; set; }
        public string DatabaseName { get; set; }
        public Boolean? ExportDirect { get; set; }
        public Boolean? CheckModule { get; set; }
        public Boolean? CheckBranch { get; set; }
        public Boolean? CheckLocalBranch { get; set; }
        public string PeriodType { get; set; }
        public string TFPeriodCode { get; set; }
        public string TFSpecialCase { get; set; }
        public string BusinessType { get; set; }
        public string LinkFieldPK { get; set; }
        public string LinkFieldFK { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
		public string LinkValResult { get; set; }
        public bool? Archive { get; set; }
        public Int32? ArchiveTotalKeepInMonths { get; set; }
        public string ArchiveDateFilter { get; set; }
        public Boolean? ShowCopy { get; set; }
    }
}
