﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class AuditTrailHeader
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public string UserId { get; set; }
        public string Module { get; set; }
        public string MenuCode { get; set; }
        public string MenuName { get; set; }
        public string ActivityAction { get; set; }
        public Boolean? WithApproval { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string ActivityHost { get; set; }
        public Int64? LinkToAudit { get; set; }
        public Int64? RefferalId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
