﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ExportFileList
    {
        public Int64 UniqueId { get; set; }
        public string UserId { get; set; }
        public string Module { get; set; }
        public string FormCode { get; set; }
        public Int64 JobQueueId { get; set; }
        public string FileName { get; set; }
        public Int64? TotalRecord { get; set; }
        public Int64? TotalInserted { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public Int64? ApprovalId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
