﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class FormNavbar
    {
        public Int32 UniqueId { get; set; }
        public string FormId { get; set; }
        public string IsParent { get; set; }
        public string ActionGroup { get; set; }
        public string NavbarType { get; set; }
        public string ActionType { get; set; }
        public string ActionIcon { get; set; }
        public string ActionTitle { get; set; }
        public string IsNeedPermission { get; set; }
        public string ActionPermission { get; set; }
        public Int16 ActionSeq { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
