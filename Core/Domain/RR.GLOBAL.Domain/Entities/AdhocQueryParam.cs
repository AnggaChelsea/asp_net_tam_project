﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class AdhocQueryParam
    {
        public Int64 UniqueId { get; set; }
        public Int64 QueryId { get; set; }
        public string ParamId { get; set; }
        public string ParamName { get; set; }
        public string ParamType { get; set; }
        public string DefaultValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
