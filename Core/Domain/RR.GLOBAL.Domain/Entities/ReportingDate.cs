﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ReportingDate
    {
        public Int16 UniqueId { get; set; }
        public string Module { get; set; }
        public string PeriodType { get; set; }
        public DateTime ReportDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
