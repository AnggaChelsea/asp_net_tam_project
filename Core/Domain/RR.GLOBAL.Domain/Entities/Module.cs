﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class Module
    {
        public Int32 UniqueId { get; set; }
        [Key]
        public string ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string DatabaseName { get; set; }
        public string Prefix { get; set; }
        public string BaseUrl { get; set; }
        public string BusinessType { get; set; }
        public Int16? Sequence { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
