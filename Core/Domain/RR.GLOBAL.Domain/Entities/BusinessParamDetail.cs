﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class BusinessParamDetail
    {
        [Key]
        public Int32 UniqueId { get; set; }
        public string ParamCode { get; set; }
        public string ParamValue { get; set; }
        public string ParamValue2 { get; set; }
        public string ParamValue3 { get; set; }
        public string ParamMapping { get; set; }
        public string ParamMapping2 { get; set; }
        public string ParamMapping3 { get; set; }
        public string ParamDesc { get; set; }
        public Int32? ParamSeq { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
