﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ManualUploadDetail
    {
        [Key]
        public Int32 UniqueId { get; set; }
        public string ProcessNo { get; set; }
        public string SourceColumn { get; set; }
        public string TargetColumn { get; set; }
        public Int16? TFSeq { get; set; }
        public Int16? TFLength { get; set; }
        public bool? IsKey { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
