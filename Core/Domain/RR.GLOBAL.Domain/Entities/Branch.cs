﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class Branch
    {
        [Key]
        public Int32 UniqueId { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string CountryCode { get; set; }
        public string Region { get; set; }
        public string BranchArea { get; set; }
        public string Unit { get; set; }
        public string RegulatorBranch { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
