﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ProcessDataSource
    {
        public Int16 UniqueId { get; set; }
        [Key]
        public string ProcessNo { get; set; }
        public string ProcessName { get; set; }
        public string PeriodType { get; set; }
        public string FileName { get; set; }
        public string FileNameDateFormat { get; set; }
        public Int16? FileDateGap { get; set; }
        public string SourcePath { get; set; }
        public string TargetPath { get; set; }
        public string StartTimeCopyFile { get; set; }
        public string EndTimeCopyFile { get; set; }
        public string ETLPackage { get; set; }
        public bool UsingTF { get; set; }//not null
        public bool? As32Bit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
