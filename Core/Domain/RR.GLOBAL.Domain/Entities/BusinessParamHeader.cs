﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class BusinessParamHeader
    {
        public Int32 UniqueId { get; set; }
        public string Module { get; set; }
        [Key]
        public string ParamCode { get; set; }
        public string ParamName { get; set; }
        public string ParamDesc { get; set; }
        public string ParamValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
