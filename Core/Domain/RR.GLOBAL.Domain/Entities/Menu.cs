﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class Menu
    {
        public Int32 UniqueId { get; set; }
        public string Module { get; set; }
        [Key]
        public string MenuCode { get; set; }
        public string ParentMenuCode { get; set; }
        public string MenuName { get; set; }
        public string MenuDesc { get; set; }
        public Int16? MenuSeq { get; set; }
        public string MenuUrl { get; set; }
        public string Icon { get; set; }
        public string PageTitle { get; set; }
        public bool? FeatureInsert { get; set; }
        public bool? FeatureUpdate { get; set; }
        public bool? FeatureDelete { get; set; }
        public bool? FeatureExport { get; set; }
        public bool? FeatureImport { get; set; }
        public bool? FeatureApproval { get; set; }
        public string FormCode { get; set; }
        public Boolean? IsMenu { get; set; }
        public Boolean? WaitApproval { get; set; }
        public Boolean? Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
