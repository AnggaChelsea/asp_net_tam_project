﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class FormDetail
    {
        [Key]
        public Int32 UniqueId { get; set; }
        public string FormCode { get; set; }
        public string FieldName { get; set; }
        public string GrdColumnName { get; set; }
        public Boolean? GrdColumnShow { get; set; }
        public Int32? GrdColumnSeq { get; set; }
        public string GrdAggregate { get; set; }
        public string EdrControlType { get; set; }
        public string EdrLookupCode { get; set; }
        public string EdrLookupNested { get; set; }
        public Boolean? EdrReadonly { get; set; }
        public Boolean? EdrMandatory { get; set; }
        public Boolean? EdrShow { get; set; }
        public string EdrDefaultValue { get; set; }
        //public Int16? EdrMaxLength { get; set; } //ubah jadi double 20200424, rizwan
        public double? EdrMaxLength { get; set; }
        public string EdrFormatExpression { get; set; }
        public string EdrLookupSrcTable { get; set; }
        public string EdrLookupSrcCondition { get; set; }
        public string EdrLookupText { get; set; }
        public string EdrLookupValue { get; set; }
        public Boolean? TFShow { get; set; }
        public Int16? TFLength { get; set; }
        public string TFAlignment { get; set; }
        public string TFReplicateChar { get; set; }
        public bool? TFAbsolute { get; set; }
        public string TFFormat { get; set; }
        public string TFDefaultValue { get; set; }
        public string ChangesNote { get; set; }
        public Boolean? Status { get; set; }
        public string IdElement { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
        public string EdrDefaultNestedField { get; set; } // add 20200505 rizwan
        public string EdrDefaultNestedSrc { get; set; } // add 20200506 rizwan
        public string EdrDefaultNestedMap { get; set; } // add 20200506 rizwan
        public string EdrCalculation { get; set; } // add 20200527 rizwan
    }
}