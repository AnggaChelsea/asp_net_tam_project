﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ProcessLogDetail
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public Int64 HeaderId { get; set; }
        public Int64? RowNumber { get; set; }
        public string ErrDesc { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
