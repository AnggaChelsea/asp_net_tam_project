﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class MenuGroupDetail
    {
        [Key]
        public Int32 UniqueId { get; set; }
        public string GroupId { get; set; }
        public string MenuCode { get; set; }
        public bool? AllowView { get; set; }
        public bool? AllowInsert{ get; set; }
        public bool? AllowUpdate{ get; set; }
        public bool? AllowDelete{ get; set; }
        public bool? AllowExport{ get; set; }
        public bool? AllowImport{ get; set; }
        public bool? AllowApproval { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
