﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class FormCustomAPI
    {
        public Int32 UniqueId { get; set; }
        public string FormCode { get; set; }
        public string ApiName { get; set; }
        public string ApiRoute { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
