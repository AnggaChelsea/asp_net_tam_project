﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class IssueList
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public DateTime DateDate { get; set; }
        public string Module { get; set; }
        public string PeriodType { get; set; }
        public string FormCode { get; set; }
        public string Problem { get; set; }
        public string Solution { get; set; }
        public string ActionTake { get; set; }
        public string IssueStatus { get; set; }

    }
}
