﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ExportCompareTemp
    {
        public string UID { get; set; }
        public long RefId { get; set; }
        public long Line { get; set; }
    }
}
