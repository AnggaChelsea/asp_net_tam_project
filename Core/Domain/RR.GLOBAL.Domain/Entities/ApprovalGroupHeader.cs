﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ApprovalGroupHeader
    {
        public Int32 UniqueId { get; set; }
        [Key]
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }
        public Boolean Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
