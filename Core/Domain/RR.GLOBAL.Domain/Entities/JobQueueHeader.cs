﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class JobQueueHeader
    {
        public Int64 UniqueId { get; set; }
        public string JobType { get; set; }
        public string PeriodType { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string ThreadId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
