﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class ApprovalHeader
    {
        [Key]
        public Int64 UniqueId { get; set; }
        public string Module { get; set; }
        public string MenuCode { get; set; }
        public string MenuName { get; set; }
        public string TaskType { get; set; }
        public string ActionType { get; set; }
        public string ApprovalStatus { get; set; }
        public Int64? LinkToHeaderId { get; set; }
        public Int64? RefferalId { get; set; }
        public String FormCode { get; set; }
        public String PeriodType { get; set; }
        public String TableName { get; set; }
        public String Note { get; set; }
        public Boolean? IdentityFK { get; set; }
        public string FileName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }

    }
}
