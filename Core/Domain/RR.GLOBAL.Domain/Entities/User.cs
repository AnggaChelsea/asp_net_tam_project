﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class User
    {
        public Int32 UniqueId { get; set; }
        [Key]
        public string UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeId { get; set; }
        public string Position { get; set; }
        public string MenuGroup { get; set; }
        public string BranchGroup { get; set; }
        public string ApprovalGroup { get; set; }
        public string ModuleAccess { get; set; }
        public Boolean? LockStatus { get; set; }
        public Int16? LockCounter { get; set; }
        public DateTime? LastLoginDateTime { get; set; }
        public Boolean Status { get; set; }
        public Boolean? PasswordExpired { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
