﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public enum ParamNameCollection
    {
        VALIDATION_PACKAGE,
        RECONCILIATION_PACKAGE,
        SANDI_BANK,
        BI_VALIDATOR_LICENSE
    }
}
