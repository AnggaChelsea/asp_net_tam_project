﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ARRA.GLOBAL.Domain.Entities
{
    public class FormSearchField
    {
        [Key]
        public Int32 UniqueId { get; set; }        
        public string FormCode { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public Int32? Sequence { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }    }
}
