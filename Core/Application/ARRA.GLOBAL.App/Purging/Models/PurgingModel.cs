﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Purging.Models
{
    public class PurgingModel
    {
        public string UID { get; set; }
        public string dbNm { get; set; }
        public string tableNm { get; set; }
        public string periodColumnNm { get; set; }
        public string purgingPeriodTp { get; set; }
        public string purgingPeriodKeep { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string createdBy { get; set; }
        public string createdDt { get; set; }
    }
}
