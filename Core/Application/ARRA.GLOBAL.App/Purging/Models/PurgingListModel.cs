﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Purging.Models
{
    public class PurgingListModel
    {
        public IList<PurgingModel> data { get; set; }
    }
}
