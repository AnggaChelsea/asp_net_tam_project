﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.Domain.Entities;
using System.Collections.Generic;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace ARRA.GLOBAL.App.Purging.Commands
{
    public class CreatePurgingTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreatePurgingTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public CreatePurgingTaskCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreatePurgingTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            JobQueueHeader header = new JobQueueHeader
            {
                JobType = QueueJobType.PURGING.ToString(),
                Status = JobStatus.Temp.ToString(),
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            };

            _context.JobQueueHeaders.Add(header);
            await _context.SaveChangesAsync(cancellationToken);

            long jobQueueId = header.UniqueId;
            List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "REPORT_DT",
                ParamValue = req.CommandModel.reportDt,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "UIDS",
                ParamValue = string.Join(",", req.CommandModel.uids.ToArray()),
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });

            _context.JobQueueDetails.AddRange(listDetail);
            header.Status = JobStatus.Pending.ToString();
            await _context.SaveChangesAsync(cancellationToken);

            foreach (string uid in req.CommandModel.uids)
            {
                string query = "INSERT INTO TR_PURGING_HIST" +
                    " (CREATED_BY, CREATED_DT, REPORT_DT, MS_PURGING_ID, STS)" +
                    " VALUES" +
                    " (@CREATED_BY, GETDATE(), @REPORT_DT, @MS_PURGING_ID, @STS)";

                DynamicParameters spParam = new DynamicParameters();
                spParam.Add("@CREATED_BY", ARRA.Common.Utility.SafeSqlString(req.UserIdentity.UserId));
                spParam.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.reportDt));
                spParam.Add("@MS_PURGING_ID", ARRA.Common.Utility.SafeSqlString(uid));
                spParam.Add("@STS", JobStatus.Progress.ToString());
                await _context.Database.GetDbConnection().QueryAsync(query, spParam, null, null, CommandType.Text);
            }            

            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
            };
        }
    }
}