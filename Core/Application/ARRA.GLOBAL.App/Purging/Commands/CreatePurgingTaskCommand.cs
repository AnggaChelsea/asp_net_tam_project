﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Purging.Commands
{
    public class CreatePurgingTaskCommand
    {
        public string reportDt { get; set; }
        public List<string> uids { get; set; }
    }
}