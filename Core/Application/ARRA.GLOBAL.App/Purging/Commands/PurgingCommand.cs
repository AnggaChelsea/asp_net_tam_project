﻿namespace ARRA.GLOBAL.App.Purging.Commands
{
    public class PurgingCommand
    {
        public long jobId { get; set; }
        public string createdBy { get; set; }
        public string reportDt { get; set; }
        public string uids { get; set; }
    }
}