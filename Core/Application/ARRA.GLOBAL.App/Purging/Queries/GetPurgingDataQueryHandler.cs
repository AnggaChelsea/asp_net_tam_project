﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Purging.Models;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;

namespace ARRA.GLOBAL.App.Purging.Queries
{
    public class GetPurgingDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetPurgingDataQuery, PurgingListModel>, PurgingListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetPurgingDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<PurgingListModel> Handle(QueriesModel<GetPurgingDataQuery, PurgingListModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters spParam = new DynamicParameters();
            spParam.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.reportDt));
            PurgingListModel output = new PurgingListModel();
            var spOutput = await _context.Database.GetDbConnection().QueryAsync<PurgingModel>("DBO.UDPS_PURGING", spParam, null, null, CommandType.StoredProcedure);
            output.data = spOutput.ToList();
            return output;
        }
    }
}