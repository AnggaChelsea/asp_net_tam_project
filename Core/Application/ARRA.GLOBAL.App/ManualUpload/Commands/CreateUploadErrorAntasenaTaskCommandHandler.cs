﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.MODULE.Domain.Entities;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class CreateUploadErrorAntasenaTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateUploadErrorAntasenaTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public CreateUploadErrorAntasenaTaskCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateUploadErrorAntasenaTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            Domain.Entities.Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);

            using (MODULEDbContext ctx = base.GetDbContext("ANTASENA_BUS"))
            {
                IList<TMP_FORM_WO_BR_ERR_OJK> listForm =
                                               await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode)
                                                .Select(c => new TMP_FORM_WO_BR_ERR_OJK
                                                {
                                                    FormCode = c.FormCode,
                                                    PeriodType = c.PeriodType,
                                                    OJKCode = c.OJKFormCode,
                                                    FormName = c.FormName
                                                }).ToListAsync(cancellationToken);

                TMP_FORM_WO_BR_ERR_OJK fInfo = listForm.Where(f => f.FormCode == req.CommandModel.formCode).FirstOrDefault();

                //add to job queue list
                JobQueueHeader header = new JobQueueHeader
                {
                    JobType = QueueJobType.UPLOAD_ERRANTASENA.ToString(),
                    PeriodType = "MONTHLY",
                    Status = JobStatus.Temp.ToString(),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                ctx.JobQueueHeaders.Add(header);
                await ctx.SaveChangesAsync(cancellationToken);

                long jobQueueId = header.UniqueId;

                List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_NAME",
                    ParamValue = req.CommandModel.fileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_NAME_ORIGINAL",
                    ParamValue = req.CommandModel.originalFileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                //listDetail.Add(new JobQueueDetail
                //{
                //    HeaderId = jobQueueId,
                //    ParamName = "REPORT_DATE",
                //    ParamValue = req.CommandModel.reportDate,
                //    CreatedBy = req.UserIdentity.UserId,
                //    CreatedDate = req.CurrentDateTime,
                //    CreatedHost = req.UserIdentity.Host
                //});
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FORM_CODE",
                    ParamValue = req.CommandModel.formCode,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                ctx.JobQueueDetails.AddRange(listDetail);

                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLogs = new MODULE.Domain.Entities.ProcessLogHeader
                {
                    DataDate = DateTime.Now,
                    PeriodType = fInfo.PeriodType,
                    Branch = null,
                    ProcessNo = req.CommandModel.formCode,
                    ProcessName = fInfo.FormName,
                    ProcessStatus = JobStatus.Progress.ToString(),
                    ProcessType = QueueJobType.UPLOAD_ERRANTASENA.ToString(),
                    QueueId = jobQueueId,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                ctx.ProcessLogHeaders.Add(hdrLogs);

                header.Status = JobStatus.Pending.ToString();

                await ctx.SaveChangesAsync(cancellationToken);

                header = null;
                listDetail = null;
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}
