﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class UploadDblCommandHandler : AppBase, IRequestHandler<CommandsModel<UploadDblCommand, StatusModel>, StatusModel>
    {
        private readonly ITextFileConnector _tf;

        public UploadDblCommandHandler(ITextFileConnector tf, IConfiguration configuration) : base(configuration)
        {
            _tf = tf;
        }

        public async Task<StatusModel> Handle(CommandsModel<UploadDblCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long id = 0;
            string filePath = string.Empty;

            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                filePath = await globalCtx.SystemParameterHeaders.Where(f => f.ParamCode == "GB042")
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);
            }

            using (MODULEDbContext ctx = base.GetDbContext("SLIK"))
            {
                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                    await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.queueId
                                        && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                hdrLog.ModifiedBy = "ROBIN-SYSTEM";
                hdrLog.ModifiedDate = req.CurrentDateTime;

                try
                {
                    id = await _tf.UploadDbl(ctx.Database.GetDbConnection(), req.UserIdentity, filePath + req.CommandModel.fileName, req.CommandModel.reportDate, req.CommandModel.formCode);

                    hdrLog.ProcessStatus = JobStatus.Success.ToString();
                    ctx.ProcessLogHeaders.Update(hdrLog);

                    await ctx.SaveChangesAsync(cancellationToken);
                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                    hdrLog.Remark = ex.Message;
                    ctx.ProcessLogHeaders.Update(hdrLog);

                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = id
            };
        }
    }
}
