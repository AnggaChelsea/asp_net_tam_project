﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class UploadErrorOJKCommand
    {
        public string processNo { get; set; }
        public string fileName { get; set; }
        public string oriFileName { get; set; }
        public long queueId { get; set; }
        public string reportDate { get; set; }
        public string formCode { get; set; }
    }
}
