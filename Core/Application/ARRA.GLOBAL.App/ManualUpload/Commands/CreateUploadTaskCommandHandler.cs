﻿using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using MediatR;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class CreateUploadTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateUploadTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public CreateUploadTaskCommandHandler(
            GLOBALDbContext context,IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateUploadTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }

            string processName = await _context.ManualUploadHeaders.Where(f => f.ProcessNo == req.CommandModel.processNo)
                .Select(c => c.ProcessName)
                .FirstOrDefaultAsync(cancellationToken);

            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if (req.AccessMatrix.AllowApproval) //with approval.
            {
                if (menu != null)
                {
                    //insert into approval.
                    ApprovalHeader mHeader = this.GetApprovalHeader(menu, req, "TR_JOB_QUEUED_HDR",0,false,req.CommandModel.fileName,req.CommandModel.formCode,form.PeriodType);
                    _context.ApprovalHeaders.Add(mHeader);
                    await _context.SaveChangesAsync(cancellationToken);
                    long headerid = mHeader.UniqueId;

                    _context.ApprovalDetails.Add(this.GetApprovalDetail(headerid, 1, "Process Name", "JOB_TP", req, QueueJobType.MANUAL_UPLOAD.ToString()));
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(headerid, 2, "Status", "STS", req,JobStatus.Temp.ToString()));
                    
                    //detail 1
                    mHeader = this.GetApprovalHeader(menu, req, "TR_JOB_QUEUED_DTL", headerid,true,"", req.CommandModel.formCode,form.PeriodType);
                    _context.ApprovalHeaders.Add(mHeader);
                    await _context.SaveChangesAsync(cancellationToken);
                    long hdrdetail = mHeader.UniqueId;
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 1, "Parameter Name 1", "PAR_NM", req,"PROCESS_NO"));
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 2, "Parameter Value 1", "PAR_VAL", req, req.CommandModel.processNo));

                    //detail 2
                    mHeader = this.GetApprovalHeader(menu, req, "TR_JOB_QUEUED_DTL", headerid, true,"", req.CommandModel.formCode,form.PeriodType);
                    _context.ApprovalHeaders.Add(mHeader);
                    await _context.SaveChangesAsync(cancellationToken);
                    hdrdetail = mHeader.UniqueId;

                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 3, "Parameter Name 2", "PAR_NM",req, "PROCESS_NAME"));
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 4, "Parameter Value 2", "PAR_VAL", req, processName));

                    //detail 3
                    mHeader = this.GetApprovalHeader(menu, req, "TR_JOB_QUEUED_DTL", headerid, true, "", req.CommandModel.formCode, form.PeriodType);
                    _context.ApprovalHeaders.Add(mHeader);
                    await _context.SaveChangesAsync(cancellationToken);
                    hdrdetail = mHeader.UniqueId;

                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 5, "Parameter Name 3", "PAR_NM", req, "FILE_NAME"));
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 6, "Parameter Value 3", "PAR_VAL", req, req.CommandModel.fileName));

                    //detail 3
                    mHeader = this.GetApprovalHeader(menu, req, "TR_JOB_QUEUED_DTL", headerid, true,"", req.CommandModel.formCode,form.PeriodType);
                    _context.ApprovalHeaders.Add(mHeader);
                    await _context.SaveChangesAsync(cancellationToken);
                    hdrdetail = mHeader.UniqueId;

                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 7, "Parameter Name 4", "PAR_NM", req, "FILE_NAME_ORIGINAL"));
                    _context.ApprovalDetails.Add(this.GetApprovalDetail(hdrdetail, 8, "Parameter Value 4", "PAR_VAL",req, req.CommandModel.originalFileName));

                    await _context.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                //add to job queue list
                JobQueueHeader header = new JobQueueHeader
                {
                    JobType = QueueJobType.MANUAL_UPLOAD.ToString(),
                    PeriodType = form.PeriodType,
                    Status = JobStatus.Temp.ToString(),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                _context.JobQueueHeaders.Add(header);
                await _context.SaveChangesAsync(cancellationToken);

                long jobQueueId = header.UniqueId;

                List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "PROCESS_NO",
                    ParamValue = req.CommandModel.processNo,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "PROCESS_NAME",
                    ParamValue = processName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_NAME",
                    ParamValue = req.CommandModel.fileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_NAME_ORIGINAL",
                    ParamValue = req.CommandModel.originalFileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                _context.JobQueueDetails.AddRange(listDetail);

                header.Status = JobStatus.Pending.ToString();

                await _context.SaveChangesAsync(cancellationToken);

                header = null;
                listDetail = null;


                AuditTrailHeader adthdr = new AuditTrailHeader
                {
                    Module = form.Module,
                    ActivityAction = ScreenAction.MANUAL_UPLOAD.ToString(),
                    ActivityDate = req.CurrentDateTime,
                    ActivityHost = req.UserIdentity.Host,
                    MenuCode = menu.MenuCode,
                    MenuName = menu.MenuName,
                    UserId = req.UserIdentity.UserId,
                    WithApproval = false,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                _context.AuditTrailHeaders.Add(adthdr);
                await _context.SaveChangesAsync(cancellationToken);

                AuditTrailDetail adtdtl = new AuditTrailDetail
                {
                    FieldName = "PROCESS_NO",
                    FormColName = "Process No",
                    FormSeq = 1,
                    HeaderId = adthdr.UniqueId,
                    ValueAfter = req.CommandModel.processNo,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                _context.AuditTrailDetails.Add(adtdtl);
                adtdtl = new AuditTrailDetail
                {
                    FieldName = "PROCESS_NAME",
                    FormColName = "Process Name",
                    FormSeq = 1,
                    HeaderId = adthdr.UniqueId,
                    ValueAfter = processName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                _context.AuditTrailDetails.Add(adtdtl);

                adtdtl = new AuditTrailDetail
                {
                    FieldName = "FILE_NAME",
                    FormColName = "File Name",
                    FormSeq = 2,
                    HeaderId = adthdr.UniqueId,
                    ValueAfter = req.CommandModel.fileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                _context.AuditTrailDetails.Add(adtdtl);

                adtdtl = new AuditTrailDetail
                {
                    FieldName = "FILE_NAME_ORIGINAL",
                    FormColName = "File Name Original",
                    FormSeq = 3,
                    HeaderId = adthdr.UniqueId,
                    ValueAfter = req.CommandModel.originalFileName,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                _context.AuditTrailDetails.Add(adtdtl);

                await _context.SaveChangesAsync(cancellationToken);
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

        private ApprovalHeader GetApprovalHeader(Menu menu, CommandsModel<CreateUploadTaskCommand, StatusModel> req,string tableNm,long headerid,bool identityFK,string fileName,string formCode,string period)
        {
            ApprovalHeader mHeader = new ApprovalHeader
            {
                MenuCode = menu.MenuCode,
                MenuName = menu.MenuName,
                ActionType = ScreenAction.MANUAL_UPLOAD.ToString(),
                ApprovalStatus = ApprovalStatus.Pending.ToString(),
                Module = menu.Module,
                RefferalId = 0,
                TaskType = TaskType.Approval.ToString(),
                TableName = tableNm,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host,
                IdentityFK = identityFK,
                FileName = fileName,
                FormCode = formCode,
                PeriodType = period
            };

            if (headerid > 0)
            {
                mHeader.LinkToHeaderId = headerid;
            }

            return mHeader;
        }

        private ApprovalDetail GetApprovalDetail(long headerid,short seq, string colNm,string field, CommandsModel<CreateUploadTaskCommand, StatusModel> req,string value)
        {
            return new ApprovalDetail
            {
                FieldName = field,
                FormColName = colNm,
                FormSeq = seq,
                HeaderId = headerid,
                ValueAfter = value,
                ValueBefore = "",
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            };
        }
    }
}