﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class CreateUploadErrorOJKTaskCommand
    {
        public string formCode { get; set; }
        public string processNo { get; set; }
        public string fileName { get; set; }
        public string originalFileName { get; set; }
        public string reportDate { get; set; }
    }
}
