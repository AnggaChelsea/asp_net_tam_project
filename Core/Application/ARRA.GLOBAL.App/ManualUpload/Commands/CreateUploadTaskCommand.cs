﻿namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class CreateUploadTaskCommand
    {
        public string formCode { get; set; }
        public string processNo { get; set; }
        public string fileName { get; set; }
        public string originalFileName { get; set; }
    }
}