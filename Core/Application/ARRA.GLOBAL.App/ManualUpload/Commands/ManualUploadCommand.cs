﻿namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class ManualUploadCommand
    {
        public string processNo { get; set; }
        public string fileName { get; set; }
        public string oriFileName { get; set; }
        public long queueId { get; set; }
    }
}