﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Common;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.ManualUpload.Commands
{
    public class ManualUploadCommandHandler : AppBase, IRequestHandler<CommandsModel<ManualUploadCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _tf;
        //public ManualUploadCommandHandler(
        //    GLOBALDbContext context, IExcelConnector excel, ITextFileConnector tf) : base(context)
        //{
        //    _context = context;
        //    _excel = excel;
        //    _tf = tf;
        //}
        public ManualUploadCommandHandler(
            IExcelConnector excel, ITextFileConnector tf, IConfiguration configuration) : base(configuration)
        {
            _excel = excel;
            _tf = tf;
        }

        public async Task<StatusModel> Handle(CommandsModel<ManualUploadCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long id = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    ManualUploadHeader hdr = await globalCtx.ManualUploadHeaders.FindAsync(req.CommandModel.processNo);
                    string delimiter = hdr.DelimitedChar;
                    string module = hdr.Module;
                    string fileType = hdr.FileType;
                    string targetTable = Utility.SafeSqlString(hdr.TargetTable);
                    string uploadAs = hdr.UploadType;
                    string processName = hdr.ProcessName;
                    hdr = null;

                    if (!targetTable.Contains(".")) //not included db name
                    {
                        string db = await globalCtx.Modules.Where(f => f.ModuleId == module).Select(c => c.DatabaseName).FirstOrDefaultAsync(cancellationToken);
                        targetTable = db + ".DBO." + targetTable;
                    }

                    //Get File Path
                    string filePath = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.MANUAL_FILE_PATH.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);

                    ImportFileListHeader hdrLog = new ImportFileListHeader
                    {
                        UserId = req.UserIdentity.UserId,
                        Status = JobStatus.Progress.ToString(),
                        FileName = req.CommandModel.fileName,
                        OriginalFileName = req.CommandModel.oriFileName,
                        FormCode = req.CommandModel.processNo,
                        Module = module,
                        PeriodType = "",
                        ProcessName = processName,
                        JobQueueId = req.CommandModel.queueId,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };
                    globalCtx.ImportFileListHeaders.Add(hdrLog);
                    await globalCtx.SaveChangesAsync(cancellationToken);

                    id = hdrLog.UniqueId;
                    hdrLog = null;

                    IList<ManualUploadDetail> listColumns =
                        await globalCtx.ManualUploadDetails.Where(f => f.ProcessNo == req.CommandModel.processNo).OrderBy(o => o.TFSeq).ToListAsync(cancellationToken);
                    long totalRecord = 0;
                    if (fileType == FileType.EXCEL.ToString())
                    {
                        totalRecord = await _excel.ManualUpload(globalCtx.Database.GetDbConnection(), req.UserIdentity, targetTable, listColumns, filePath + req.CommandModel.fileName, id, uploadAs);
                    }
                    else if (fileType == FileType.DELIMITED.ToString() ||
                        fileType == FileType.CSV.ToString())
                    {
                        totalRecord = await _tf.ManualUpload(globalCtx.Database.GetDbConnection(), req.UserIdentity, targetTable, listColumns, filePath + req.CommandModel.fileName, id, uploadAs, delimiter);
                    }
                    else if (fileType == FileType.FIXLENGTH.ToString())
                    {
                        totalRecord = await _tf.ManualUploadFixLength(globalCtx.Database.GetDbConnection(), req.UserIdentity, targetTable, listColumns, filePath + req.CommandModel.fileName, id, uploadAs);
                    }
                    listColumns = null;

                    long totalFailed = await globalCtx.ImportFileListDetails.Where(f => f.HeaderId == id).LongCountAsync(cancellationToken);

                    var entity = await globalCtx.ImportFileListHeaders.FindAsync(id);
                    entity.TotalRecord = totalRecord;
                    entity.TotalProceed = totalRecord;
                    entity.TotalFailed = totalFailed;
                    entity.Status = totalFailed == 0 ? JobStatus.Success.ToString() : JobStatus.Failed.ToString();
                    entity.ModifiedBy = req.UserIdentity.UserId;
                    entity.ModifiedDate = req.CurrentDateTime;
                    entity.ModifiedHost = req.UserIdentity.Host;
                    globalCtx.ImportFileListHeaders.Update(entity);
                    entity = null;
                    await globalCtx.SaveChangesAsync(cancellationToken);

                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    if (id > 0)
                    {
                        var entity = await globalCtx.ImportFileListHeaders.FindAsync(id);
                        entity.Status = JobStatus.Failed.ToString();
                        entity.Message = ex.Message;
                        entity.ModifiedBy = req.UserIdentity.UserId;
                        entity.ModifiedDate = req.CurrentDateTime;
                        entity.ModifiedHost = req.UserIdentity.Host;
                        globalCtx.ImportFileListHeaders.Update(entity);
                        entity = null;
                        await globalCtx.SaveChangesAsync(cancellationToken);
                    }
                }

            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = id
            };
        }
    }
}