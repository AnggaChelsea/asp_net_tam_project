﻿namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class ManualFileModel
    {
        public string procNo { get; set; }
        public string name { get; set; }
        public string fileType { get; set; }
    }
}
