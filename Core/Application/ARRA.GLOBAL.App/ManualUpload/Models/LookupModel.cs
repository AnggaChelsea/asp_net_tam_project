﻿namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class LookupModel
    {
        public string code { get; set; }
        public string desc { get; set; }
    }
}