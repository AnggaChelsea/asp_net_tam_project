﻿namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class TemplateFileModel
    {
        public string fileName { get; set; }
        public string path { get; set; }
    }
}