﻿using System;
namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class ManualFileScreenInfoModel
    {
        public bool? allowApproval { get; set; }
        public bool? allowExport { get; set; }
        public bool? allowImport { get; set; }
    }
}