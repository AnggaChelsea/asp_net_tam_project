﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class ManualFileListModel
    {
        public IList<ManualFileModel> data { get; set; }
    }
}
