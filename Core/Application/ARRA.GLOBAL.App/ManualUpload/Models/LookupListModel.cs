﻿using System.Collections.Generic;
namespace ARRA.GLOBAL.App.ManualUpload.Models
{
    public class LookupListModel
    {
        public IList<LookupModel> data { get; set; }
        public string defaultValue { get; set; }
    }
}