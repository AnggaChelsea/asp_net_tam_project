﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ManualUpload.Models;
using ARRA.Common.Model;
using System.IO;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetTemplateFileQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetTemplateFileQuery, TemplateFileModel>, TemplateFileModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly ITextFileConnector _tf;
        private readonly IExcelConnector _excel;
        public GetTemplateFileQueryHandler(GLOBALDbContext context, ITextFileConnector tf, IExcelConnector excel) : base(context)
        {
            _context = context;
            _tf = tf;
            _excel = excel;
        }

        public async Task<TemplateFileModel> Handle(QueriesModel<GetTemplateFileQuery, TemplateFileModel> req, CancellationToken cancellationToken)
        {
            TemplateFileModel vm = new TemplateFileModel();
            if (!string.IsNullOrEmpty(req.QueryModel.processNo))
            {

                ManualUploadHeader hdr = await _context.ManualUploadHeaders.FindAsync(req.QueryModel.processNo);
                string delimiter = hdr.DelimitedChar;
                string module = hdr.Module;
                string fileType = hdr.FileType;
                hdr = null;

                IList<ManualUploadDetail> listColums = await _context.ManualUploadDetails.Where(f => f.ProcessNo == req.QueryModel.processNo)
                    .OrderBy(o=>o.TFSeq)
                    .ToListAsync(cancellationToken);

                string path = await _context.SystemParameterHeaders.Where(f => f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString() && f.Module == module)
                    .Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                string fileName = req.QueryModel.processNo;

                if (fileType == FileType.EXCEL.ToString())
                {
                    fileName += ".xlsx";
                    _excel.GenerateManualUploadTemplate(listColums, path + fileName);
                }
                else if (fileType == FileType.CSV.ToString())
                {
                    fileName += ".csv";
                    _tf.GenerateTemplate(listColums, path + fileName, fileType, delimiter);
                }
                else if (fileType == FileType.DELIMITED.ToString() ||
                    fileType == FileType.FIXLENGTH.ToString())
                {
                    fileName += ".txt";
                    _tf.GenerateTemplate(listColums, path + fileName, fileType, delimiter);
                }

                vm.fileName = fileName;
                vm.path = path;

                listColums = null;
            }
            else
            {
                throw new CustomException(ErrorMessages.UploadDataEmpty);
            }

            return vm;
        }
    }
}