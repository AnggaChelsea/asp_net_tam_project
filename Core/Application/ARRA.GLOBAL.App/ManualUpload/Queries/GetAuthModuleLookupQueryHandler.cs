﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ManualUpload.Models;

namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetAuthModuleLookupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetAuthModuleLookupQuery, LookupListModel>, LookupListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetAuthModuleLookupQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<LookupListModel> Handle(QueriesModel<GetAuthModuleLookupQuery, LookupListModel> req, CancellationToken cancellationToken)
        {
            LookupListModel vm = new LookupListModel();
            vm.defaultValue = req.AccessMatrix.Module;
            string[] strs = req.UserIdentity.Modules.Split(',');
            IList<LookupModel> list = await _context.Modules.Where(f => strs.Contains(f.ModuleId)).OrderBy(o => o.Sequence)
                .Select(c => new LookupModel
                {
                    code = c.ModuleId,
                    desc = c.ModuleName
                }).ToListAsync(cancellationToken);
            vm.data = list;

            list = null;
            return vm;
        }
    }
}