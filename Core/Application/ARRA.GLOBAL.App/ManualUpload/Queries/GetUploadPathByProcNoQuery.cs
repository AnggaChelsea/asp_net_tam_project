﻿namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetUploadPathByProcNoQuery
    {
        public string procNo { get; set; }
    }
}