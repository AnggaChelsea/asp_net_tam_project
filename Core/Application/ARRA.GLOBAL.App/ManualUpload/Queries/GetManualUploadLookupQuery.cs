﻿namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetManualUploadLookupQuery
    {
        public string formCode { get; set; }
        public string module { get; set; }
    }
}