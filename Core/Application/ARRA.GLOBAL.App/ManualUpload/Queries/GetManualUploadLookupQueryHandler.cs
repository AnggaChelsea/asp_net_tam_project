﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ManualUpload.Models;

namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetManualUploadLookupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetManualUploadLookupQuery, ManualFileListModel>, ManualFileListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetManualUploadLookupQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<ManualFileListModel> Handle(QueriesModel<GetManualUploadLookupQuery, ManualFileListModel> req, CancellationToken cancellationToken)
        {
            ManualFileListModel vm = new ManualFileListModel();
            string[] strs = req.UserIdentity.Modules.Split(',');
            IList<ManualFileModel> list = await _context.ManualUploadHeaders.Where(f => f.Module == req.QueryModel.module)
                .OrderBy(o => o.ProcessName)
                .Select(c => new ManualFileModel
                {
                    procNo = c.ProcessNo,
                    name = c.ProcessName,
                    fileType = c.FileType
                }).ToListAsync(cancellationToken);

            vm.data = list;
            list = null;
            return vm;
        }
    }
}