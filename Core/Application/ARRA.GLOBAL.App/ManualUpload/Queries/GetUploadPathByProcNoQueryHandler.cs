﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ManualUpload.Models;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetUploadPathByProcNoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetUploadPathByProcNoQuery, string>, string>
    {
        private readonly GLOBALDbContext _context;
        public GetUploadPathByProcNoQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<string> Handle(QueriesModel<GetUploadPathByProcNoQuery, string> req, CancellationToken cancellationToken)
        {
            string module = await _context.ManualUploadHeaders.Where(f => f.ProcessNo == req.QueryModel.procNo)
                .Select(c => c.Module).FirstOrDefaultAsync(cancellationToken);

            string path = await _context.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.MANUAL_FILE_PATH.ToString())
                .Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

            return path;
        }
    }
}