﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ManualUpload.Models;

namespace ARRA.GLOBAL.App.ManualUpload.Queries
{
    public class GetFormInfoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormInfoQuery, ManualFileScreenInfoModel>, ManualFileScreenInfoModel>
    {
        private readonly GLOBALDbContext _context;
        public GetFormInfoQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<ManualFileScreenInfoModel> Handle(QueriesModel<GetFormInfoQuery, ManualFileScreenInfoModel> req, CancellationToken cancellationToken)
        {
            ManualFileScreenInfoModel vm = 
                await _context.MenuGroupDetails.Where(f => f.GroupId == req.UserIdentity.GroupId && f.MenuCode == req.AccessMatrix.MenuCode)
                    .Select(c => new ManualFileScreenInfoModel
                    {
                        allowExport = c.AllowExport,
                        allowImport = c.AllowImport,
                        allowApproval = c.AllowApproval
                    }).FirstOrDefaultAsync(cancellationToken);

            return vm;
        }
    }
}