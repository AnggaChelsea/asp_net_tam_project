﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Jobs.Models;
using Microsoft.Extensions.Configuration;
using System;
using ARRA.Common.Enumerations;
using System.Transactions;

namespace ARRA.GLOBAL.App.Jobs.Queries
{
    public class GetRecentJobProgressQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetRecentJobProgressQuery, ProgressCountModel>, ProgressCountModel>, IDisposable
    {
        //private readonly GLOBALDbContext _context;

        //public GetRecentJobProgressQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}
        public GetRecentJobProgressQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }
        public void Dispose()
        {
        }

        public async Task<ProgressCountModel> Handle(QueriesModel<GetRecentJobProgressQuery, ProgressCountModel> req, CancellationToken cancellationToken)
        {
            int count = 0;
            //using (TransactionScope trans = new TransactionScope(
            //        TransactionScopeOption.Required,
            //        new TransactionOptions
            //        {
            //            IsolationLevel = IsolationLevel.ReadUncommitted
            //        }))
            //{
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                ctx.ChangeTracker.LazyLoadingEnabled = false;

                if (req.QueryModel.jobTypes == null)
                {
                    count = await ctx.JobQueueHeaders.Where(f =>
                               f.Status == JobStatus.Progress.ToString()
                               //&& Convert.ToDateTime(f.ModifiedDate).AddHours(6) > req.CurrentDateTime
                               && EF.Functions.DateDiffHour(f.ModifiedDate, DateTime.Now) > 6
                               && f.JobType != QueueJobType.CANCEL_PROCESS.ToString()
                            ).CountAsync(cancellationToken);
                }
                else
                {
                    count = await ctx.JobQueueHeaders.Where(f =>
                               f.Status == JobStatus.Progress.ToString()
                               //&& Convert.ToDateTime(f.ModifiedDate).AddHours(6) > req.CurrentDateTime
                               && EF.Functions.DateDiffHour(f.ModifiedDate, DateTime.Now) > 6
                               && f.JobType != QueueJobType.CANCEL_PROCESS.ToString()
                               && req.QueryModel.jobTypes.Contains(f.JobType)
                            ).CountAsync(cancellationToken);
                }

                ctx.Dispose();
            }

            //    trans.Complete();
            //    trans.Dispose();
            //}

            this.Dispose();
            return new ProgressCountModel
            {
                count = count
            };
        }
    }
}