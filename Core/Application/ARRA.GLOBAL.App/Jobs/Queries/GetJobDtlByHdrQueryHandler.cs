﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Jobs.Models;
using Microsoft.Extensions.Configuration;
using System;

namespace ARRA.GLOBAL.App.Jobs.Queries
{
    public class GetJobDtlByHdrQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetJobDtlByHdrQuery, DetailListModel>, DetailListModel>, IDisposable
    {
        //private readonly GLOBALDbContext _context;

        //public GetJobDtlByHdrQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}
        public GetJobDtlByHdrQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public async Task<DetailListModel> Handle(QueriesModel<GetJobDtlByHdrQuery, DetailListModel> req, CancellationToken cancellationToken)
        {
            DetailListModel vm = new DetailListModel();
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                ctx.ChangeTracker.LazyLoadingEnabled = false;
                vm = new DetailListModel
                {
                    listDetails = await (from a in ctx.JobQueueDetails
                                         where a.HeaderId == req.QueryModel.headerId
                                         select new DetailModel
                                         {
                                             paramName = a.ParamName,
                                             paramValue = a.ParamValue
                                         }).ToListAsync(cancellationToken)
                };
            }

            return vm;
        }
    }
}