﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jobs.Queries
{
    public class GetJobHdrByStatusQuery
    {
        public string status { get; set; }
        public string module { get; set; }
        public string[] jobTypes { get; set; }
    }
}