﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jobs.Queries
{
    public class GetJobDtlByHdrQuery
    {
        public long headerId { get; set; }
        public string module { get; set; }
    }
}