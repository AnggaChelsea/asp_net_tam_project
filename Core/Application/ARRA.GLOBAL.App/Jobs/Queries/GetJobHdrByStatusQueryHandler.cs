﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Jobs.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System;

namespace ARRA.GLOBAL.App.Jobs.Queries
{
    public class GetJobHdrByStatusQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetJobHdrByStatusQuery, HeaderListModel>, HeaderListModel>, IDisposable
    {
        //private readonly GLOBALDbContext _context;

        //public GetJobHdrByStatusQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}

        public GetJobHdrByStatusQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public void Dispose()
        {
        }

        public async Task<HeaderListModel> Handle(QueriesModel<GetJobHdrByStatusQuery, HeaderListModel> req, CancellationToken cancellationToken)
        {
            IList<HeaderModel> listResult = new List<HeaderModel>();

            //using (TransactionScope trans = new TransactionScope(
            //        TransactionScopeOption.Required,
            //        new TransactionOptions
            //        {
            //            IsolationLevel = IsolationLevel.ReadUncommitted
            //        }))
            //{

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                ctx.ChangeTracker.LazyLoadingEnabled = false;

                if (req.QueryModel.jobTypes == null)
                {
                    listResult = await (from a in ctx.JobQueueHeaders
                                        where a.Status == req.QueryModel.status
                                        orderby a.UniqueId
                                        select new HeaderModel
                                        {
                                            uniqueId = a.UniqueId,
                                            jobType = a.JobType,
                                            status = a.Status,
                                            createdBy = a.CreatedBy,
                                            createdDate = a.CreatedDate,
                                            createdHost = a.CreatedHost,
                                            threadId = a.ThreadId,
                                            modifiedDate = a.ModifiedDate
                                        }).ToListAsync(cancellationToken);
                }
                else
                {
                    listResult = await (from a in ctx.JobQueueHeaders
                                        where a.Status == req.QueryModel.status
                                            && req.QueryModel.jobTypes.Contains(a.JobType)
                                        orderby a.UniqueId
                                        select new HeaderModel
                                        {
                                            uniqueId = a.UniqueId,
                                            jobType = a.JobType,
                                            status = a.Status,
                                            createdBy = a.CreatedBy,
                                            createdDate = a.CreatedDate,
                                            createdHost = a.CreatedHost,
                                            threadId = a.ThreadId,
                                            modifiedDate = a.ModifiedDate
                                        }).ToListAsync(cancellationToken);
                }

                ctx.Dispose();
            }

            //    trans.Complete();
            //    trans.Dispose();
            //}

            this.Dispose();
            return new HeaderListModel
            {
                listJobs = listResult
            };
        }
    }
}