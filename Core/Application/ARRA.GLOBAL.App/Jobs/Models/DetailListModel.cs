﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jobs.Models
{
    public class DetailListModel
    {
        public IList<DetailModel> listDetails { get; set; }
    }
}
