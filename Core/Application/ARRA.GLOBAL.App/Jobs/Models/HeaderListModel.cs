﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jobs.Models
{
    public class HeaderListModel
    {
        public IList<HeaderModel> listJobs { get; set; }
    }
}
