﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jobs.Models
{
    public class HeaderModel
    {
        public long uniqueId { get; set; }
        public string jobType { get; set; }
        public string status { get; set; }
        public string module { get; set; }
        public string threadId { get; set; }
        public string createdBy { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdHost { get; set; }
        public DateTime? modifiedDate { get; set; }
    }
}