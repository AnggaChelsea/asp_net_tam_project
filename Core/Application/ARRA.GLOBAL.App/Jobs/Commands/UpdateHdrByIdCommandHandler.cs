﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Authentications.Commands;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Jobs.Commands
{
    public class UpdateHdrByIdCommandHandler : AppBase, IRequestHandler<CommandsModel<UpdateHdrByIdCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        //public UpdateHdrByIdCommandHandler(
        //    GLOBALDbContext context,IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}
        public UpdateHdrByIdCommandHandler(
            IConfiguration configuration) : base(configuration)
        {
        }

        async Task<StatusModel> IRequestHandler<CommandsModel<UpdateHdrByIdCommand, StatusModel>, StatusModel>.Handle(CommandsModel<UpdateHdrByIdCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            try
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ctx.ChangeTracker.QueryTrackingBehavior = Microsoft.EntityFrameworkCore.QueryTrackingBehavior.NoTracking;
                    ctx.ChangeTracker.LazyLoadingEnabled = false;

                    var entity = await ctx.JobQueueHeaders.FindAsync(req.CommandModel.uniqueId);

                    if (entity != null)
                    {
                        if (!string.IsNullOrEmpty(req.CommandModel.status))
                            entity.Status = req.CommandModel.status;
                        if (!string.IsNullOrEmpty(req.CommandModel.message))
                            entity.Message = req.CommandModel.message;
                        if (!string.IsNullOrEmpty(req.CommandModel.threadId))
                            entity.ThreadId = req.CommandModel.threadId;

                        entity.ModifiedBy = SystemName.ARRASERVICES.ToString();
                        entity.ModifiedDate = req.CurrentDateTime;
                        ctx.Update(entity);

                        await ctx.SaveChangesAsync();

                        ctx.Dispose();
                    }
                }
            }
            catch
            {
                
                //throw ex;
            }

            //_context.Dispose();
            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}