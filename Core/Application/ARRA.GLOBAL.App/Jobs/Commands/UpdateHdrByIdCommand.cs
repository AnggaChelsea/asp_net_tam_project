﻿using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ARRA.GLOBAL.App.Jobs.Commands;

namespace ARRA.GLOBAL.App.Jobs.Commands
{
    public class UpdateHdrByIdCommand
    {
        public long uniqueId { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string threadId { get; set; }
        public string module { get; set; }

    }
}