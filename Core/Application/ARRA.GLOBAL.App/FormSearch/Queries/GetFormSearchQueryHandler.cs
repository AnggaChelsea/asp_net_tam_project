﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSearchQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSearchQuery, FormSearchModel>, FormSearchModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSearchQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSearchModel> Handle(QueriesModel<GetFormSearchQuery, FormSearchModel> req, CancellationToken cancellationToken)
        {
            IList<FormSearchFieldModel> fields = await _context.FormSearchFields
                .Where(a => a.FormCode == req.QueryModel.formCode)
                .OrderBy(a => a.Sequence)
                .Select(a => new FormSearchFieldModel
                {
                    FormCode = a.FormCode,
                    Key = a.Key,
                    Label = a.Label,
                    Sequence = a.Sequence,
                })
                .ToListAsync(cancellationToken);

            //IList<FormSearchListModel> forms = await _context.FormSearchLists
            //    .Where(a => a.FormCode == req.QueryModel.formCode)
            //    .OrderBy(a => a.Sequence)
            //    .Select(a => new FormSearchListModel { 
            //        FormCode = a.FormCode,
            //        Value = a.Value,
            //        FieldMapping = a.FieldMapping,
            //        Sequence = a.Sequence,
            //    }).ToListAsync(cancellationToken);

            IList<FormSearchListModel> forms = await (from a in _context.FormSearchLists
                                                      join b in _context.FormHeaders on a.Value equals b.FormCode
                                                      where a.FormCode == req.QueryModel.formCode
                                                      select new FormSearchListModel
                                                      {
                                                          FormCode = a.FormCode,
                                                          Label = b.FormName,
                                                          Value = a.Value,
                                                          FieldMapping = a.FieldMapping,
                                                          Sequence = a.Sequence,
                                                      }).ToListAsync(cancellationToken);

            return new FormSearchModel
            {
                fields = fields,
                forms = forms
            };
        }
    }
}