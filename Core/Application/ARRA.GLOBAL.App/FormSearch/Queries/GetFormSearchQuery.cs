﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSearchQuery
    {
        public string formCode { get; set; }
    }

}