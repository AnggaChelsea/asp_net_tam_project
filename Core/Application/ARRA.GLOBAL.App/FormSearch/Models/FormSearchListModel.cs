﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Models
{
    public class FormSearchListModel
    {
        public string FormCode { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public string FieldMapping { get; set; }
        public Int32? Sequence { get; set; }
    }
}