﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Models
{
    public class FormSearchFieldModel
    {
        public string FormCode { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public Int32? Sequence { get; set; }
    }
}