﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Models
{
    public class FormSearchModel
    {
        public dynamic fields { get; set; }
        public dynamic forms { get; set; }
    }
}