﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using Dapper;
using System.Data;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormAuthListQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormAuthListQuery, FormAuthListModel>, FormAuthListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetFormAuthListQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FormAuthListModel> Handle(QueriesModel<GetFormAuthListQuery, FormAuthListModel> req, CancellationToken cancellationToken)
        {
            IList<FormAuthModel> list = null;
            string module = await _context.FormHeaders.Where(f => f.FormCode == req.QueryModel.formCode).Select(c => c.Module).FirstOrDefaultAsync<string>(cancellationToken);
            if (string.IsNullOrEmpty(req.QueryModel.periodType))
            {
                list = await (from f in _context.FormHeaders
                              join m in _context.Menus
                               on f.FormCode equals m.FormCode
                              join g in _context.MenuGroupDetails
                               on m.MenuCode equals g.MenuCode
                              where g.GroupId == req.UserIdentity.GroupId
                                && m.Status==true
                                && f.Module == module
                              orderby f.PeriodType, f.FormName
                              select new FormAuthModel
                              {
                                  ojkCode = f.OJKFormCode,
                                  code = f.FormCode,
                                  name = f.FormName
                              }).ToListAsync(cancellationToken);
            }
            else
            {
                list = await (from f in _context.FormHeaders
                              join m in _context.Menus
                               on f.FormCode equals m.FormCode
                              join g in _context.MenuGroupDetails
                               on m.MenuCode equals g.MenuCode
                              where g.GroupId == req.UserIdentity.GroupId
                                && m.Status == true
                                && f.PeriodType == req.QueryModel.periodType
                                && f.Module == module
                              orderby f.PeriodType, f.FormName
                              select new FormAuthModel
                              {
                                  ojkCode = f.OJKFormCode,
                                  code = f.FormCode,
                                  name = f.FormName
                              }).ToListAsync(cancellationToken);
            }

            return new FormAuthListModel
            {
                data = list
            };
        }
    }
}