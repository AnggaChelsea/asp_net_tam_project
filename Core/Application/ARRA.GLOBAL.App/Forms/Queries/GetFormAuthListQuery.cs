﻿namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormAuthListQuery
    {
        public string formCode { get; set; }
        public string periodType { get; set; }
    }
}