﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ARRA.Common;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileAdhocByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetExportedFileAdhocByIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;
        public GetExportedFileAdhocByIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetExportedFileAdhocByIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            ExportFileList exp;
            //FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
            //if (form.ExportDirect == true)
            //{
                exp = await _context.ExportFileLists.FindAsync(req.QueryModel.id);
            //}
            //else
            //{
            //    exp = await _context.ExportFileLists.Where(f => f.JobQueueId == req.QueryModel.id).FirstOrDefaultAsync(cancellationToken);
            //}

            string fileName = "";
            string filePath = "";
            string err = "";
            if (exp != null)
            {
                if (exp.CreatedBy == req.UserIdentity.UserId)
                {
                    if (exp != null)
                    {
                        if (!string.IsNullOrEmpty(exp.FileName))
                        {
                            //Get File Path
                            string path = await _context.SystemParameterHeaders.Where(f => f.Module == "GLOBAL" && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                                .Select(c => c.ParamValue)
                                .FirstOrDefaultAsync(cancellationToken);

                            fileName = exp.FileName;
                            filePath = path;
                        }
                        else if (exp.Status == JobStatus.Failed.ToString())
                        {
                            err = exp.Message;
                        }
                    }
                }
                else
                {
                    //prevent unaothorized action 
                    throw new Exception(ErrorMessages.NotAUthorizedToAction);
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath,
                error = err
            };
        }
    }
}
