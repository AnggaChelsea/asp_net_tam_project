﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetAuditImportFileByIdQuery
    {
        public long id { get; set; }
        public string module { get; set; }
    }
}
