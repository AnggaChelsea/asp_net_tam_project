﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using ARRA.Common;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetAuditImportFileByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetAuditImportFileByIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetAuditImportFileByIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetAuditImportFileByIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            string PROCESSNAME = string.Empty;
            string FILENAME = string.Empty;
            string FILEPATH = string.Empty;
            string err = string.Empty;
            string PATHPARNAME = string.Empty;
            string FILECOLNAME = string.Empty;
            bool? WITHAPPROVAL = false;

            try
            {
                // GET PROCESS NAME
                //PROCESSNAME = await _context.AuditTrailDetails.Where(f => f.HeaderId == req.QueryModel.id && f.FormColName == "Process Name")
                //    .Select(g => g.ValueAfter).FirstOrDefaultAsync(cancellationToken);
                PROCESSNAME = await _context.AuditTrailHeaders.Where(f => f.UniqueId == req.QueryModel.id)
                    .Select(g => g.ActivityAction).FirstOrDefaultAsync(cancellationToken);

                WITHAPPROVAL = await _context.AuditTrailHeaders.Where(f => f.UniqueId == req.QueryModel.id)
                    .Select(g => g.WithApproval).FirstOrDefaultAsync(cancellationToken);

                // GET FILE NAME
                if (PROCESSNAME == "IMPORT")
                {
                    if (Convert.ToBoolean(WITHAPPROVAL))
                    {
                        FILECOLNAME = "Parameter Value 2";
                    }
                    else
                    {
                        FILECOLNAME = "File Name";
                    }
                }
                else
                {
                    if (Convert.ToBoolean(WITHAPPROVAL))
                    {
                        FILECOLNAME = "Parameter Value 3";
                    }
                    else
                    {
                        FILECOLNAME = "File Name";
                    }
                }

                FILENAME = await _context.AuditTrailDetails.Where(f => f.HeaderId == req.QueryModel.id && f.FormColName == FILECOLNAME)
                    .Select(g => g.ValueAfter).FirstOrDefaultAsync(cancellationToken);

                // GET FILE PATH
                if(PROCESSNAME == "IMPORT")
                {
                    PATHPARNAME = "IMPORT_FILE_LOCATION";
                }
                else
                {
                    PATHPARNAME = "MANUAL_FILE_PATH";
                }

                FILEPATH = await _context.SystemParameterHeaders.Where(f => f.Module == req.QueryModel.module && f.ParamName == PATHPARNAME)
                    .Select(g => g.ParamValue).FirstOrDefaultAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new FileModel
            {
                fileName = FILENAME,
                path = FILEPATH,
                error = err
            };

        }
    }
}
