﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormDataDetailQuery
    {
        public string formCode { get; set; }
        public long uid { get; set; }
    }
}