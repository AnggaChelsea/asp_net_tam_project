﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileByIdQuery
    {
        public string formCode { get; set; }
        public long id { get; set; }
    }
}