﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Common;
using System;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.App.Forms.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormDetailGridQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormDetailGridQuery, FormListModel>, FormListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormDetailGridQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FormListModel> Handle(QueriesModel<GetFormDetailGridQuery, FormListModel> req, CancellationToken cancellationToken)
        {
            FormListModel vm = new FormListModel();

            var form = (await _context.FormHeaders.FindAsync(req.QueryModel.FormCode));

            if (form == null)
            {
                throw new NotFoundException(nameof(FormHeader), req.QueryModel.FormCode);
            }

            vm.formName = form.FormName;
            vm.linkDetail = form.LinkToFormCode == null ? "" : form.LinkToFormCode;
            vm.linkPK = form.LinkFieldPK;
            vm.linkFK = form.LinkFieldFK;
            vm.expDirect = form.ExportDirect;
            vm.sort = form.FormOrder;
            vm.allowIns = req.AccessMatrix.AllowInsert;
            vm.allowUpd = req.AccessMatrix.AllowUpdate;
            vm.allowDel = req.AccessMatrix.AllowDelete;
            vm.allowExp = req.AccessMatrix.AllowExport;
            vm.allowImp = req.AccessMatrix.AllowImport;
            vm.allowApv = req.AccessMatrix.AllowApproval;
            vm.linkValRes = form.LinkValResult;

            //get custom api
            List<FormCustomAPI> listApi = await _context.FormCustomAPIes.Where(f => f.FormCode == req.QueryModel.FormCode).ToListAsync(cancellationToken);
            if (listApi.Count > 0)
            {
                foreach (FormCustomAPI api in listApi)
                {
                    switch (Utility.ParseEnum<CustomAPI>(api.ApiName))
                    {
                        case CustomAPI.DATA_API:
                            vm.dataAPI = api.ApiRoute;
                            break;
                        case CustomAPI.DATA_DETAIL_API:
                            vm.dataDetailAPI = api.ApiRoute;
                            break;
                        case CustomAPI.DOWNLOAD_API:
                            vm.downloadAPI = api.ApiRoute;
                            break;
                        case CustomAPI.EDITOR_API:
                            vm.editorAPI = api.ApiRoute;
                            break;
                        case CustomAPI.EXP_TASK_API:
                            vm.exportTaskAPI = api.ApiRoute;
                            break;
                        case CustomAPI.LOOKUP_API:
                            vm.lookupAPI = api.ApiRoute;
                            break;
                        case CustomAPI.SAVE_DATA_API:
                            vm.saveDataAPI = api.ApiRoute;
                            break;
                        case CustomAPI.SAVE_FILTER_API:
                            vm.saveFilterAPI = api.ApiRoute;
                            break;
                        case CustomAPI.UPLOAD_API:
                            vm.uploadAPI = api.ApiRoute;
                            break;
                        case CustomAPI.SELECT_ALL_API:
                            vm.selectAllAPI = api.ApiRoute;
                            break;
                        case CustomAPI.SELECT_DEPEND_API:
                            vm.selectDependAPI = api.ApiRoute;
                            break;
                        case CustomAPI.ACTION_API:
                            vm.actionAPI = api.ApiRoute;
                            break;
                    }
                }
            }
            listApi = null;


            IList<ReportingDate> repotingdt = await _context.ReportingDates.Where(f => f.Module == form.Module).ToListAsync(cancellationToken);

            //get grid configuration
            IList<GridModel> grid = await (from a in _context.FormDetails
                                           where a.FormCode == req.QueryModel.FormCode && a.GrdColumnShow == true
                                           orderby a.GrdColumnSeq
                                           select new GridModel
                                           {
                                               caption = a.GrdColumnName,
                                               field = a.FieldName,
                                               datatype = a.EdrControlType,
                                               format = a.EdrFormatExpression,
                                               aggregate = a.GrdAggregate,
                                               lCode = a.EdrLookupCode
                                           }).ToListAsync(cancellationToken);

            //get system filter configuration.
            IList<FilterModel> defaultFtr = await (from a in _context.FormFilters
                                                   where a.FormId == req.QueryModel.FormCode
                                                   orderby a.FieldSeq
                                                   select new FilterModel
                                                   {
                                                       field = a.FieldName,
                                                       optr = a.Operator,
                                                       value = a.DefaultValue,
                                                       valuetype = a.DefaultValueType,
                                                       defaultftr = true,
                                                       orifield = a.FieldName,
                                                       orioptr = a.Operator,
                                                       orivalue = a.DefaultValue,
                                                       wlookup = a.UseLookup == null ? false : true,
                                                       autorefresh = a.AutoRefresh == null ? false : a.AutoRefresh
                                                   }).ToListAsync(cancellationToken);

            //get user filter configuration.
            IList<FilterModel> usrFtr = await (from a in _context.UserFormFilters
                                               where a.FormId == req.QueryModel.FormCode
                                               && a.UserId == req.UserIdentity.UserId
                                               orderby a.FieldSeq
                                               select new FilterModel
                                               {
                                                   field = a.FieldName,
                                                   optr = a.Operator,
                                                   value = a.DefaultValue,
                                                   valuetype = a.DefaultValueType,
                                                   defaultftr = false,
                                                   orifield = a.FieldName,
                                                   orioptr = a.Operator,
                                                   orivalue = a.DefaultValue,
                                                   wlookup = a.UseLookup == null ? false : true,
                                                   autorefresh = false
                                               }).ToListAsync(cancellationToken);

            //get action row
            IList<ActionRowModel> actionRows = await (from a in _context.FormActionRows
                                                     where a.FormId == req.QueryModel.FormCode
                                                   orderby a.ActionSeq
                                                   select new ActionRowModel
                                                   {
                                                       title = a.ActionTitle,
                                                       icon = a.ActionIcon,
                                                       isNeedPermission = a.IsNeedPermission,
                                                       permission = a.ActionPermission,
                                                       type = a.ActionType,
                                                       sequence = a.ActionSeq,
                                                   }).ToListAsync(cancellationToken);

            //get navbar
            IList<NavbarModel> navbars = await (from a in _context.FormNavbars
                                                      where a.FormId == req.QueryModel.FormCode
                                                      orderby a.ActionSeq
                                                      select new NavbarModel
                                                      {
                                                          isParent = a.IsParent,
                                                          navbarGroup = a.ActionGroup,
                                                          navbarType = a.NavbarType,
                                                          title = a.ActionTitle,
                                                          icon = a.ActionIcon,
                                                          isNeedPermission = a.IsNeedPermission,
                                                          permission = a.ActionPermission,
                                                          type = a.ActionType,
                                                          sequence = a.ActionSeq,
                                                      }).ToListAsync(cancellationToken);

            //setting filter
            IEnumerable<FilterModel> ftrs = defaultFtr.Concat(usrFtr);

            List<string> fLookup = ftrs.Where(f => f.wlookup == true).Select(c => c.field).ToList();
            IEnumerable<LookupModel> listLookup = new List<LookupModel>();
            if (fLookup.Count > 0)
            {
                List<string> lKeys = grid.Where(f => fLookup.Contains(f.field) && string.IsNullOrEmpty(f.lCode) == false).Select(c => c.lCode).ToList();
                if (lKeys.Count > 0)
                {
                    listLookup = await (from a in _context.BusinessParamDetails
                                        where lKeys.Contains(a.ParamCode)
                                        orderby a.ParamCode, a.ParamSeq
                                        select new LookupModel
                                        {
                                            code = a.ParamValue,
                                            desc = a.ParamDesc,
                                            partype = a.ParamCode
                                        }).ToListAsync(cancellationToken);
                }
                lKeys = null;
            }
            fLookup = null;

            foreach (FilterModel ftr in ftrs)
            {
                GridModel mdl = grid.Where(f => f.field == ftr.field).FirstOrDefault();
                if (mdl != null)
                {
                    ftr.datatype = mdl.datatype;
                    ftr.oridatatp = mdl.datatype;
                    if (ftr.wlookup == true)
                    {
                        ftr.lookup = listLookup.Where(f => f.partype == mdl.lCode)
                            .Select(c => new { code = c.code, desc = c.desc }).ToList();
                    }
                }
                else
                {
                    ftr.datatype = "";
                    ftr.oridatatp = "";
                }

                if (ftr.valuetype != FilterValueType.FIX.ToString() && !string.IsNullOrEmpty(ftr.valuetype))
                {
                    ftr.value = base.GetValueByType(Utility.ParseEnum<FilterValueType>(ftr.valuetype), repotingdt, req.CurrentDateTime);
                    ftr.orivalue = ftr.value;
                }

                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (ftr.datatype == ControlType.DATE.ToString() ||
                        ftr.datatype == ControlType.DATETIME.ToString())
                    {
                        ftr.valuedt = Utility.GetDateStruct(ftr.value);
                    }
                }

                // add 20201216 [rizwan]
                // untuk filter period tp gl adjustmnet
                if(req.QueryModel.FormCode == "F_ANGLADJ" && ftr.field == "PERIOD_TP")
                {
                    ftr.datatype = "CONSTANT";
                    ftr.oridatatp = "CONSTANT";
                    ftr.lookup = await (from a in _context.BusinessParamDetails
                                        where a.ParamCode.Equals("ANJURPT")
                                        orderby a.ParamCode, a.ParamSeq
                                        select new 
                                        {
                                            code = a.ParamValue,
                                            desc = a.ParamDesc
                                        }).ToListAsync(cancellationToken);
                }

                mdl = null;
            }

            // get static cell MS_REPORT_STATIC_CELL
            if (form.Module.Equals("RWA"))
            {
                using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                {
                    IList<ReportStaticCellModel> staticCells = await (from a in ctx.ReportStaticCell
                                                                      where a.ReportCd == req.QueryModel.FormCode
                                                                      orderby a.RowSequence, a.CellPosition
                                                                      select new ReportStaticCellModel
                                                                      {
                                                                          UniqueId = a.UniqueId,
                                                                          ReportCd = a.ReportCd,
                                                                          RowSequence = a.RowSequence,
                                                                          CellPosition = a.CellPosition,
                                                                          CellType = a.CellType,
                                                                          CellValue = a.CellValue,
                                                                      }).ToListAsync(cancellationToken);

                    vm.reportStaticCells = staticCells.ToList();
                }
            }

            listLookup = null;

            vm.filter = ftrs.ToList();
            vm.actionRow = actionRows.ToList();
            vm.navbar = navbars.ToList();
            vm.grid = grid;

            grid = null;
            defaultFtr = null;
            usrFtr = null;
            ftrs = null;
            repotingdt = null;


            return vm;
        }


    }

}