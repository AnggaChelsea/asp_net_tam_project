﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using ARRA.Common;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Queries;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileModuleByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetExportedFileModuleByIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetExportedFileModuleByIdQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetExportedFileModuleByIdQuery, FileModel> req, CancellationToken cancellationToken)
        {

            ARRA.MODULE.Domain.Entities.ExportFileList exp;
            string fileName = "";
            string filePath = "";
            string err = "";

            using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
            {
                FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
                if (form.ExportDirect == true)
                {
                    exp = await ctx.ExportFileLists.FindAsync(req.QueryModel.id);
                }
                else
                {
                    exp = await ctx.ExportFileLists.Where(f => f.JobQueueId == req.QueryModel.id).FirstOrDefaultAsync(cancellationToken);
                }


                if (exp != null)
                {
                    if (exp.CreatedBy == req.UserIdentity.UserId)
                    {
                        if (exp != null)
                        {
                            if (!string.IsNullOrEmpty(exp.FileName))
                            {
                                //Get File Path
                                string path = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                                    .Select(c => c.ParamValue)
                                    .FirstOrDefaultAsync(cancellationToken);

                                fileName = exp.FileName;
                                filePath = path;
                            }
                            else if (exp.Status == JobStatus.Failed.ToString())
                            {
                                err = exp.Message;
                            }
                        }
                    }
                    else
                    {
                        //prevent unaothorized action 
                        throw new Exception(ErrorMessages.NotAUthorizedToAction);
                    }
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath,
                error = err
            };

        }
    }
}