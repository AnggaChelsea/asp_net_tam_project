﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using System;
using Microsoft.Data.SqlClient;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormDataQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormDataQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }


        public async Task<DataListModel> Handle(QueriesModel<GetFormDataQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            long totalRecord = 0;
            dynamic formData = new List<dynamic>();
            dynamic aggregatedata = "";

            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
                p.Add("@USR_ID", req.UserIdentity.UserId);
                IEnumerable<Int64> records = await _context.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                totalRecord = records.FirstOrDefault();
                records = null;

                if (totalRecord > 0)
                {
                    p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                    p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
                    p.Add("@SORT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.sort));
                    p.Add("@PAGE", req.QueryModel.start);
                    p.Add("@PAGESIZE", req.QueryModel.pageSize);
                    p.Add("@USR_ID", req.UserIdentity.UserId);
                    formData = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORM_DATA", p, null, 6000, CommandType.StoredProcedure);

                    p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                    p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
                    p.Add("@USR_ID", req.UserIdentity.UserId);
                    aggregatedata = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORM_DATA_AGGREGATE", p, null, null, CommandType.StoredProcedure);
                }
            }
            catch (SqlException ex)
            {
                throw new DatabaseException(ex.Number, ex.Message);
            }

            return new DataListModel
            {
                totalRecord = totalRecord,
                data = formData,
                aggregate = aggregatedata
            };
        }
    }
}