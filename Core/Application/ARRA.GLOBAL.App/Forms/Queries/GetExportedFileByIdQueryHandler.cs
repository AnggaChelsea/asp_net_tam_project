﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using ARRA.Common;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetExportedFileByIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetExportedFileByIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetExportedFileByIdQuery, FileModel> req, CancellationToken cancellationToken)
        {

            ExportFileList exp;
            FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
            if (form.ExportDirect == true)
            {
                exp = await _context.ExportFileLists.FindAsync(req.QueryModel.id);
            }
            else
            {
                exp = await _context.ExportFileLists.Where(f => f.JobQueueId == req.QueryModel.id).FirstOrDefaultAsync(cancellationToken);
            }

            string fileName = "";
            string filePath = "";
            string err = "";
            if (exp != null)
            {
                if (exp.CreatedBy == req.UserIdentity.UserId)
                {
                    if (exp != null)
                    {
                        if (!string.IsNullOrEmpty(exp.FileName))
                        {
                            //Get File Path
                            string path = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                                .Select(c => c.ParamValue)
                                .FirstOrDefaultAsync(cancellationToken);

                            fileName = exp.FileName;
                            filePath = path;
                        }
                        else if (exp.Status == JobStatus.Failed.ToString())
                        {
                            err = exp.Message;
                        }
                    }
                }
                else
                {
                    //prevent unaothorized action 
                    throw new Exception(ErrorMessages.NotAUthorizedToAction);
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath,
                error = err
            };

        }
    }
}