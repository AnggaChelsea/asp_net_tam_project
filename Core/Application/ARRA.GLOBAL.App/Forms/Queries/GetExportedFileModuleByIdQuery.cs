﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileModuleByIdQuery
    {
        public string formCode { get; set; }
        public long id { get; set; }
    }
}
