﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormEditorQuery
    {
        public string FormCode { get; set; }
    }
}