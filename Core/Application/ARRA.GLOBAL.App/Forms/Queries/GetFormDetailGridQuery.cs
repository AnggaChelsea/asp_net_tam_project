﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormDetailGridQuery
    {
        public string FormCode { get; set; }
    }
}