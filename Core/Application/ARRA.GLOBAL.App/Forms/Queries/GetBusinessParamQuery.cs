﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetBusinessParamQuery
    {
        public string FormCode { get; set; }
        public Boolean isHeaderOnly { get; set; }
        public Boolean isDetailOnly { get; set; }
        public IList<String> ParamCodes { get; set; }
    }
}