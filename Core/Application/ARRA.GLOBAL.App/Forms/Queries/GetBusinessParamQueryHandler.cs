﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using Dapper;
using System.Data;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetBusinessParamQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBusinessParamQuery, BusinessParamModel>, BusinessParamModel>
    {
        private readonly GLOBALDbContext _context;

        public GetBusinessParamQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<BusinessParamModel> Handle(QueriesModel<GetBusinessParamQuery, BusinessParamModel> req, CancellationToken cancellationToken)
        {
            BusinessParamModel businessParamModel = new BusinessParamModel();
            if (req.QueryModel.isHeaderOnly)
            {
                IList<BusinessParamHdrModel> headers = await (from a in _context.BusinessParamHeaders
                                                              where req.QueryModel.ParamCodes.Contains(a.ParamCode)
                                                              select new BusinessParamHdrModel
                                                              {
                                                                  paramCode = a.ParamCode,
                                                                  paramName = a.ParamName,
                                                                  paramDesc = a.ParamDesc,
                                                                  paramValue = a.ParamValue
                                                              }).ToListAsync(cancellationToken);
                businessParamModel.headers = headers;
            }

            if (req.QueryModel.isDetailOnly)
            {
                IList<BusinessParamDtlModel> details = await (from a in _context.BusinessParamDetails
                                                              where req.QueryModel.ParamCodes.Contains(a.ParamCode)
                                                              orderby a.ParamCode, a.ParamSeq
                                                              select new BusinessParamDtlModel
                                                              {
                                                                  paramCode = a.ParamCode,
                                                                  paramValue = a.ParamValue,
                                                                  paramDesc = a.ParamDesc,
                                                                  paramSeq = a.ParamSeq,
                                                                  paramValue2 = a.ParamValue2,
                                                                  paramValue3 = a.ParamValue3
                                                              }).ToListAsync(cancellationToken);
                businessParamModel.details = details;
            }

            return businessParamModel;
        }
    }
}