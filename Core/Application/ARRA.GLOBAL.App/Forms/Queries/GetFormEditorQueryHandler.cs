﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormEditorQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormEditorQuery, EditorListModel>, EditorListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormEditorQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<EditorListModel> Handle(QueriesModel<GetFormEditorQuery, EditorListModel> req, CancellationToken cancellationToken)
        {
            IList<EditorModel> cols = await (from a in _context.FormDetails
                                             where a.FormCode == req.QueryModel.FormCode// && a.EdrShow == true //will handle on frontend.
                                             orderby a.GrdColumnSeq
                                             select new EditorModel
                                             {
                                                 caption = a.GrdColumnName,
                                                 editor = a.EdrControlType,
                                                 defaultvalue = a.EdrDefaultValue,
                                                 field = a.FieldName,
                                                 exprs = a.EdrFormatExpression == null ? "" : a.EdrFormatExpression,
                                                 lookup = a.EdrLookupCode,
                                                 mandatory = a.EdrMandatory,
                                                 maxLength = a.EdrMaxLength,
                                                 readOnly = a.EdrReadonly,
                                                 show = a.EdrShow,
                                                 dylookup = !string.IsNullOrEmpty(a.EdrLookupSrcTable) && !string.IsNullOrEmpty(a.EdrLookupText) && !string.IsNullOrEmpty(a.EdrLookupValue) ? "T" + a.UniqueId.ToString() : "",
                                                 nested = a.EdrLookupNested,
                                                 defaultNestedField = a.EdrDefaultNestedField,
                                                 defaultNestedSrc = a.EdrDefaultNestedSrc,
                                                 defaultNestedMap = a.EdrDefaultNestedMap,
                                                 edrCalculation = a.EdrCalculation
                                             }).ToListAsync(cancellationToken);

            List<string> keyLookup = new List<string>();
            foreach (EditorModel item in cols)
            {
                if (item.show==true)
                {
                    if (string.IsNullOrEmpty(item.lookup))
                    {
                        if (item.dylookup != "" && item.dylookup!=null)
                        {
                            item.lookup = item.dylookup;
                        }
                    }
                    else
                    {
                        keyLookup.Add(item.lookup);
                    }
                }
            }

            IEnumerable<LookupModel> listLookup = new List<LookupModel>();

            if (keyLookup.Count > 0)
            {
                listLookup = await (from a in _context.BusinessParamDetails
                                    where keyLookup.Contains(a.ParamCode)
                                    orderby a.ParamCode, a.ParamSeq
                                    select new LookupModel
                                    {
                                        code = a.ParamValue,
                                        desc = a.ParamDesc,
                                        partype = a.ParamCode
                                    }).ToListAsync(cancellationToken);


            }

            IEnumerable<LookupModel> allLookup;

            if (cols.Where(f => f.dylookup != "").ToList().Count > 0)
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@FORM_CD", req.QueryModel.FormCode);
                p.Add("@USR_ID", req.UserIdentity.UserId);
                IEnumerable<LookupModel> iDyLookup = await _context.Database.GetDbConnection().QueryAsync<LookupModel>("DBO.UDPS_GET_DYNAMIC_LOOKUP", p, null, null, CommandType.StoredProcedure);
                allLookup = listLookup.Concat(iDyLookup.ToList<LookupModel>());
                iDyLookup = null;
            }
            else
            {
                allLookup = listLookup.AsEnumerable<LookupModel>();
            }

            return new EditorListModel
            {
                data = cols,
                lookup = allLookup.ToList()
            };
        }
    }
}