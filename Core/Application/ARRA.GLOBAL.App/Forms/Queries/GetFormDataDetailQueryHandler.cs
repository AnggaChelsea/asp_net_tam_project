﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using System;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetFormDataDetailQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormDataDetailQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormDataDetailQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetFormDataDetailQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@FORM_CD", req.QueryModel.formCode);
            p.Add("@UID", req.QueryModel.uid);
            p.Add("@USR_ID", req.UserIdentity.UserId);
            dynamic formData = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORM_DATA_DTL", p, null, 3000, CommandType.StoredProcedure);

            string status = "";

            if (req.AccessMatrix.AllowApproval)
            {
                StatusApproval sts = await (from a in _context.ApprovalHeaders
                                            join u in _context.Users
                                                on a.CreatedBy equals u.UserId
                                            where a.FormCode == req.QueryModel.formCode
                                                && a.RefferalId == req.QueryModel.uid
                                                && a.ApprovalStatus == ApprovalStatus.Pending.ToString()
                                            orderby a.CreatedDate descending
                                            select new StatusApproval
                                            {
                                                SubmitBy = u.FirstName,
                                                SubmitDate = a.CreatedDate,
                                                Action = a.ActionType
                                            }).FirstOrDefaultAsync(cancellationToken);
                if (sts != null)
                {
                    status = string.Format(InfoMessages.PendingApprovalStatus, sts.SubmitBy, Convert.ToDateTime(sts.SubmitDate).ToString("yyyy-MM-dd HH:mm:ss"), sts.Action);
                    sts = null;
                }
            }

            return new DataListModel
            {
                totalRecord = 1,
                data = formData,
                sts = status
            };
        }
    }

    public class StatusApproval
    {
        public string SubmitBy { get; set; }
        public DateTime? SubmitDate { get; set; }
        public string Action { get; set; }
    }
}