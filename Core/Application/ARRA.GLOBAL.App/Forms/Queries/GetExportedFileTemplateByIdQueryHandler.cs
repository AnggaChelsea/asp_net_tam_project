﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using ARRA.Common;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.App.Forms.Queries
{
    public class GetExportedFileTemplateByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetExportedFileTemplateByIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetExportedFileTemplateByIdQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetExportedFileTemplateByIdQuery, FileModel> req, CancellationToken cancellationToken)
        {

            MODULE.Domain.Entities.ExportFileList exp;
            FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
            MODULEDbContext ctx = base.GetDbContext(form.Module);

            if (form.ExportDirect == true)
            {
                exp = await ctx.ExportFileLists.FindAsync(req.QueryModel.id);
            }
            else
            {
                exp = await ctx.ExportFileLists.Where(f => f.JobQueueId == req.QueryModel.id).FirstOrDefaultAsync(cancellationToken);
            }

            string fileName = "";
            string filePath = "";
            string err = "";
            if (exp != null)
            {
                if (exp.CreatedBy == req.UserIdentity.UserId)
                {
                    if (exp != null)
                    {
                        if (!string.IsNullOrEmpty(exp.FileName))
                        {
                            //Get File Path
                            string path = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                                .Select(c=>c.ParamValue)
                                .FirstOrDefaultAsync(cancellationToken);

                            fileName = exp.FileName;
                            filePath = path;
                        }
                        else if (exp.Status == JobStatus.Failed.ToString())
                        {
                            err = exp.Message;
                        }
                    }
                }
                else
                {
                    //prevent unaothorized action 
                    throw new Exception(ErrorMessages.NotAUthorizedToAction);
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath,
                error = err
            };

        }
    }
}