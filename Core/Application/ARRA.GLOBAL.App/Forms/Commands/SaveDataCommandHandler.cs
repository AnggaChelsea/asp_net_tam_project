﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class SaveDataCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveDataCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveDataCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if (menu != null)
            {
                //MenuGroupDetail access = await _context.MenuGroupDetails.Where(f => f.GroupId == req.UserIdentity.GroupId && f.MenuCode == menu.MenuCode).FirstOrDefaultAsync(cancellationToken);
                FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);

                if (form == null)
                {
                    throw new NotFoundException(nameof(FormHeader), req.CommandModel.formCode);
                }

                await this.Validation(form.FormCode, form.Module, form.CheckBranch,form.CheckLocalBranch, req.UserIdentity.BranchGroup, req.CommandModel.data);

                IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                if (formDetail.Count > 0)
                {
                    //save header
                    var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
                    JToken action, uid;
                    jObject.TryGetValue("screenActionType", out action);
                    jObject.TryGetValue("UID", out uid);
                    if (Convert.ToString(uid)=="") { uid = "0"; }

                    ApprovalHeader mHeader = new ApprovalHeader
                    {
                        MenuCode = menu.MenuCode,
                        MenuName = menu.MenuName,
                        ActionType = base.GetActionType(Convert.ToString(action)),
                        ApprovalStatus = ApprovalStatus.Pending.ToString(),
                        Module = menu.Module,
                        RefferalId = Convert.ToInt64(uid),
                        TaskType = req.AccessMatrix.AllowApproval?TaskType.Approval.ToString(): TaskType.Temp.ToString(),
                        FormCode = req.CommandModel.formCode,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host,
                        PeriodType = form.PeriodType
                    };

                    _context.ApprovalHeaders.Add(mHeader);

                    await _context.SaveChangesAsync(cancellationToken);

                    Int64 headerid = mHeader.UniqueId;
                    foreach (FormDetail item in formDetail)
                    {
                        JToken value, valueBefore;
                        jObject.TryGetValue(item.FieldName, out value);
                        jObject.TryGetValue(item.FieldName + "_before", out valueBefore);

                        _context.ApprovalDetails.Add(new ApprovalDetail
                        {
                            FieldName = item.FieldName,
                            FormColName = item.GrdColumnName,
                            FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                            HeaderId = headerid,
                            ValueAfter = this.GetValue(item.EdrControlType,ARRA.Common.Utility.SafeSqlString(Convert.ToString(value))),
                            ValueBefore = Convert.ToString(valueBefore),
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host
                        });
                    }
                    
                    await _context.SaveChangesAsync(cancellationToken);

                    if (!string.IsNullOrEmpty(form.LinkToFormCode))
                    {
                        bool identityFK = false;
                        if (form.LinkFieldPK != null && form.LinkFieldPK.ToUpper() == "UID")
                        {
                            identityFK = true;
                        }
                        formDetail = await _context.FormDetails.Where(f => f.FormCode == form.LinkToFormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                        //detail
                        if (req.CommandModel.details.Count > 0)
                        {
                            foreach (dynamic item in req.CommandModel.details)
                            {
                                if (item != null)
                                {
                                    jObject = ((Newtonsoft.Json.Linq.JObject)item);

                                    jObject.TryGetValue("screenActionType", out action);
                                    jObject.TryGetValue("UID", out uid);
                                    if (Convert.ToString(uid) == "") { uid = "0"; }

                                    mHeader = new ApprovalHeader
                                    {
                                        MenuCode = menu.MenuCode,
                                        MenuName = menu.MenuName,
                                        ActionType = base.GetActionType(Convert.ToString(action)),
                                        ApprovalStatus = ApprovalStatus.Pending.ToString(),
                                        Module = menu.Module,
                                        RefferalId = Convert.ToInt64(uid),
                                        TaskType = TaskType.Approval.ToString(),
                                        FormCode = form.LinkToFormCode,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                        LinkToHeaderId = headerid,
                                        PeriodType = form.PeriodType,
                                        IdentityFK = identityFK
                                    };

                                    _context.ApprovalHeaders.Add(mHeader);

                                    await _context.SaveChangesAsync(cancellationToken);

                                    Int64 headeriddetail = mHeader.UniqueId;
                                    foreach (FormDetail item1 in formDetail)
                                    {
                                        JToken value, valueBefore;
                                        jObject.TryGetValue(item1.FieldName, out value);
                                        jObject.TryGetValue(item1.FieldName + "_before", out valueBefore);

                                        // changed by arif 20220323 penambahan kondisi, untuk data detail dengan link "UID" ke header tidak perlu diinsert ke detail
                                        if (
                                            form.LinkFieldPK != null 
                                            && (
                                                (form.LinkFieldPK.ToUpper() == "UID" && form.LinkFieldFK.ToUpper() != item1.FieldName.ToUpper())
                                                || (form.LinkFieldPK.ToUpper() != "UID")
                                               )
                                            )
                                        {
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = item1.FieldName,
                                                FormColName = item1.GrdColumnName,
                                                FormSeq = Convert.ToInt16(item1.GrdColumnSeq),
                                                HeaderId = headeriddetail,
                                                ValueAfter = this.GetValue(item1.EdrControlType, ARRA.Common.Utility.SafeSqlString(Convert.ToString(value))),
                                                ValueBefore = Convert.ToString(valueBefore),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                        }
                                    }

                                    await _context.SaveChangesAsync(cancellationToken);
                                }

                            }
                        }

                    }

                    //cek has approval atau tidak.
                    try
                    {
                        if (!req.AccessMatrix.AllowApproval)
                        {
                            DynamicParameters p = new DynamicParameters();
                            p.Add("@APPV_ID", headerid);
                            p.Add("@USR_ID", req.UserIdentity.UserId);
                            p.Add("@HOST", req.UserIdentity.Host);
                            p.Add("@APPROVAL", req.AccessMatrix.AllowApproval);

                            await _context.Database.GetDbConnection().ExecuteAsync("DBO.UDPU_EXECUTE_CRUD", p, null, null, CommandType.StoredProcedure);
                        }

                    }
                    catch(SqlException ex)
                    {
                        throw new DatabaseException(ex.Number,ex.Message);
                    }

                    //foreach (KeyValuePair<string, JToken> pair in jObject)
                    //{
                    //    string key = pair.Key;
                    //    string val = pair.Value.ToString();
                    //}
                }
            }
            else
            {
                throw new NotFoundException("Menu not found", "not found");
            }


            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

        private async Task Validation(string formCode,string module, bool? checkBranch, bool? checkBranchLocal, string branchGroup, dynamic data)
        {
            if (checkBranch==true)
            {
                var jObject = ((Newtonsoft.Json.Linq.JObject)data);

                //check branch AUTH
                JToken branch;
                jObject.TryGetValue("RG_BRANCH", out branch);
                if (branch != null)
                {
                    int count = await (from a in _context.BranchGroupDetails
                                       join b in _context.Branches on a.BranchCode equals b.BranchCode
                                       where a.GroupId == branchGroup
                                       && b.RegulatorBranch == branch.ToString()
                                       select a.BranchCode
                                       ).CountAsync();
                    if (count == 0)
                    {
                        throw new CustomException(string.Format(ErrorMessages.NotAuthorizedBranch, branch.ToString()));
                    }
                }

                //check branch lock server side
                JToken reportdt;
                jObject.TryGetValue("REPORT_DT", out reportdt);
                jObject.TryGetValue("RG_BRANCH", out branch);
                if (branch!=null && reportdt != null)
                {
                    using(MODULEDbContext ctx = base.GetDbContext(module))
                    {
                        long lockData =
                            await ctx.LockDatas
                                .Where(f => f.ReportDate == Convert.ToDateTime(reportdt)
                                    && f.FormCode == formCode && f.Branch == branch.ToString())
                                .CountAsync();
                        if (lockData > 0)
                        {
                            throw new CustomException(ErrorMessages.BranchLocked);
                        }
                    }
                }
                jObject = null;
            }
            else if (checkBranchLocal == true)
            {
                var jObject = ((Newtonsoft.Json.Linq.JObject)data);

                //check branch AUTH
                JToken branch;
                jObject.TryGetValue("BRANCH", out branch);
                if (branch != null)
                {
                    int count = await _context.BranchGroupDetails.Where(f => f.GroupId == branchGroup
                       && f.BranchCode == branch.ToString()).CountAsync();
                    if (count == 0)
                    {
                        throw new CustomException(string.Format(ErrorMessages.NotAuthorizedBranch, branch.ToString()));
                    }
                }

                //check branch lock server side
                JToken reportdt;
                jObject.TryGetValue("REPORT_DT", out reportdt);
                jObject.TryGetValue("RG_BRANCH", out branch);
                if (branch != null && reportdt != null)
                {
                    using (MODULEDbContext ctx = base.GetDbContext(module))
                    {
                        long lockData =
                            await ctx.LockDatas
                                .Where(f => f.ReportDate == Convert.ToDateTime(reportdt)
                                    && f.FormCode == formCode && f.Branch == branch.ToString())
                                .CountAsync();
                        if (lockData > 0)
                        {
                            throw new CustomException(ErrorMessages.BranchLocked);
                        }
                    }
                }
                jObject = null;
            }

        }

        private string GetValue(string type,string value)
        {
            string result = value;
            if (type == ControlType.MULTICHOICE.ToString())
            {
                result = value.Replace(" ", "").Replace("]", "").Replace("[", "").Replace("\"", "").Replace("\r\n", "");
            }

            return result;
        }
    }
}