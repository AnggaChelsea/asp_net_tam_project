﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataTemplateDtlCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportDataTemplateDtlCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;

        public ExportDataTemplateDtlCommandHandler(
            GLOBALDbContext context, IExcelConnector excel, ITextFileConnector textfile,IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportDataTemplateDtlCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;

            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                try
                {
                    ReportMaster reportMaster = await ctx.ReportMasters.FindAsync(form.FormCode); 
                    string formCodeDtl = reportMaster.FormCdDtl;
                    IList<FormDetail> listColumns = await _context.FormDetails.Where(f => f.FormCode == formCodeDtl && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                    IList<FilterModel> listFtr = new List<FilterModel>();
                    if (!String.IsNullOrEmpty(req.CommandModel.filter))
                    {
                        listFtr = JsonConvert.DeserializeObject<List<FilterModel>>(req.CommandModel.filter);
                    }

                    string reportDt = listFtr.Where(filter => filter.field == "REPORT_DT").First().value;
                    string row = listFtr.Where(filter => filter.field == "ROW_SEQ").First().value;
                    string column = listFtr.Where(filter => filter.field == "COLUMN_SEQ").First().value;

                    //Get File Path
                    string filePath = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    //get total record                    
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    p.Add("@REPORT_DT", reportDt);
                    p.Add("@ROW", row);
                    p.Add("@COLUMN", column);
                    p.Add("@PAGE_SIZE", "0");
                    p.Add("@PAGE", "0");
                    p.Add("@SORT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.sort));
                    p.Add("@FILTER", ResolveFilter(listFtr));
                    p.Add("@IS_COUNT", "1");
                    IEnumerable<Int64> records = await ctx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_TABLE_DETAIL", p, null, 6000, CommandType.StoredProcedure);
                    long totalRecord = records.FirstOrDefault();

                    //get data       
                    p.Add("@IS_COUNT", "0");
                    string fileName = "";

                    var exportlist = new MODULE.Domain.Entities.ExportFileList
                    {
                        UserId = req.UserIdentity.UserId,
                        Module = form.Module,
                        FormCode = req.CommandModel.formCode,
                        JobQueueId = req.CommandModel.jobId,
                        Status = JobStatus.Progress.ToString(),
                        TotalRecord = null,
                        TotalInserted = null,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };

                    ctx.ExportFileLists.Add(exportlist);
                    await ctx.SaveChangesAsync(cancellationToken);
                    downloadId = exportlist.UniqueId;

                    fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                    if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                    {
                        fileName += ".zip";
                        _excel.WriteExcelDtlBatch(ctx.Database.GetDbConnection(), p, listColumns, req.CommandModel.formCode, filePath, fileName, totalRecord);
                    }                    
                    //using (IDataReader rd = await ctx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_TABLE_DETAIL", p, null, null, CommandType.StoredProcedure))
                    //{
                    //    fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                    //    if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                    //    {
                    //        fileName += ".xlsx";
                    //        _excel.WriteExcel(rd, listColumns, filePath + fileName);
                    //    }
                    //    else if (req.CommandModel.fileType == FileType.CSV.ToString())
                    //    {
                    //        fileName += ".csv";
                    //        _textfile.WriteCSV(rd, listColumns, filePath + fileName);
                    //    }
                    //    else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                    //    {
                    //        fileName += ".txt";
                    //        _textfile.WriteDelimited(rd, listColumns, filePath + fileName, form.TFDelimtedChar);
                    //    }
                    //    else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                    //    {
                    //        fileName += ".txt";
                    //        _textfile.WriteFixLength(rd, listColumns, filePath + fileName);
                    //    }

                    //    await ctx.SaveChangesAsync(cancellationToken);

                    //    rd.Close();
                    //}

                    MODULE.Domain.Entities.ExportFileList explst = await ctx.ExportFileLists.FindAsync(exportlist.UniqueId);
                    explst.FileName = fileName;
                    explst.TotalRecord = totalRecord;
                    explst.TotalInserted = totalRecord;
                    explst.Status = JobStatus.Success.ToString();
                    explst.ModifiedBy = req.UserIdentity.UserId;
                    explst.ModifiedDate = req.CurrentDateTime;
                    explst.ModifiedHost = req.UserIdentity.Host;
                    await ctx.SaveChangesAsync(cancellationToken);
                    explst = null;
                } catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    if (downloadId > 0)
                    {
                        MODULE.Domain.Entities.ExportFileList explst = await ctx.ExportFileLists.FindAsync(downloadId);
                        explst.Status = JobStatus.Failed.ToString();
                        explst.Message = message;
                        explst.ModifiedBy = req.UserIdentity.UserId;
                        explst.ModifiedDate = req.CurrentDateTime;
                        explst.ModifiedHost = req.UserIdentity.Host;
                        await ctx.SaveChangesAsync(cancellationToken);
                        explst = null;
                    }
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
    }
}