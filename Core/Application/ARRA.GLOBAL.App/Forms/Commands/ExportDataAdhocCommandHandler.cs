﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataAdhocCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportDataAdhocCommand, StatusModel>, StatusModel>
    {
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;

        public ExportDataAdhocCommandHandler(
            IExcelConnector excel, ITextFileConnector textfile, IConfiguration configuration) : base(configuration)
        {
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportDataAdhocCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            using (GLOBALDbContext globalCtx = req.CommandModel.jobId > 0 ? base.GetGlobalDbContext() : base.GetGlobalReadDbContext())
            {
                try
                {
                    FormHeader form = new FormHeader();
                    form.FormCode = "ADHOC";
                    form.TFDelimtedChar = ",";
                    IList<FormDetail> listColumns = null;

                    //Get File Path
                    string filePath = await globalCtx.SystemParameterHeaders.Where(f => f.Module == "GLOBAL" && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    string fileName = "";

                    var exportlist = new ExportFileList
                    {
                        UserId = req.UserIdentity.UserId,
                        Module = "GLOBAL",
                        FormCode = req.CommandModel.formCode,
                        JobQueueId = req.CommandModel.jobId,
                        Status = JobStatus.Progress.ToString(),
                        TotalRecord = null,
                        TotalInserted = null,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };

                    globalCtx.ExportFileLists.Add(exportlist);
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    downloadId = exportlist.UniqueId;

                    using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync(req.CommandModel.queryVal, null, null, null, CommandType.Text))
                    {
                        listColumns = GetColumnFromDataReader(rd);

                        fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                        if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                        {
                            fileName += ".xlsx";
                            _excel.WriteExcel(rd, listColumns, filePath + fileName);
                        }
                        else if (req.CommandModel.fileType == FileType.CSV.ToString())
                        {
                            fileName += ".csv";
                            _textfile.WriteCSV(rd, listColumns, filePath + fileName);
                        }
                        else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                        {
                            fileName += ".txt";
                            _textfile.WriteDelimited(rd, listColumns, filePath + fileName, form.TFDelimtedChar);
                        }
                        else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                        {
                            fileName += ".txt";
                            _textfile.WriteFixLength(rd, listColumns, filePath + fileName);
                        }

                        await globalCtx.SaveChangesAsync(cancellationToken);

                        rd.Close();
                    }

                    ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(exportlist.UniqueId);
                    explst.FileName = fileName;
                    explst.TotalRecord = 0;
                    explst.TotalInserted = 0;
                    explst.Status = JobStatus.Success.ToString();
                    explst.ModifiedBy = req.UserIdentity.UserId;
                    explst.ModifiedDate = req.CurrentDateTime;
                    explst.ModifiedHost = req.UserIdentity.Host;
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    explst = null;
                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    if (downloadId > 0)
                    {
                        ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(downloadId);
                        explst.Status = JobStatus.Failed.ToString();
                        explst.Message = message;
                        explst.ModifiedBy = req.UserIdentity.UserId;
                        explst.ModifiedDate = req.CurrentDateTime;
                        explst.ModifiedHost = req.UserIdentity.Host;
                        await globalCtx.SaveChangesAsync(cancellationToken);
                        explst = null;
                    }
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }

        private IList<FormDetail> GetColumnFromDataReader(IDataReader rd)
        {
            List<FormDetail> dtl = new List<FormDetail>();

            for (int i = 0; i < rd.FieldCount; i++)
            {
                FormDetail fd = new FormDetail()
                {
                    GrdColumnName = rd.GetSchemaTable().Rows[i].ItemArray[0].ToString(),
                    EdrControlType = this.ConvertDatabaseFieldType(rd.GetSchemaTable().Rows[i].ItemArray[24].ToString()),
                    FieldName = rd.GetSchemaTable().Rows[i].ItemArray[0].ToString()
                };

                dtl.Add(fd);
            }

            return dtl;
        }

        private string ConvertDatabaseFieldType(string dbType)
        {
            string excelType = string.Empty;

            switch (dbType.ToUpper())
            {
                case "BIT":
                    excelType = "BOOL";
                    break;
                case "TINYINT":
                case "SMALLINT":
                case "INT":
                case "BIGINT":
                case "NUMERIC":
                    excelType = "NUMBER";
                    break;
                case "DECIMAL":
                case "REAL":
                    excelType = "MONEY";
                    break;
                case "FLOAT":
                    excelType = "PERCENTAGE";
                    break;
                case "DATE":
                    excelType = "DATE";
                    break;
                case "TIME":
                    excelType = "TIME";
                    break;
                case "DATETIME":
                    excelType = "DATETIME";
                    break;
                case "CHAR":
                case "VARCHAR":
                case "VARCHAR(MAX)":
                case "TEXT":
                case "NCHAR":
                case "NVARCHAR":
                case "NVARCHAR(MAX)":
                case "NTEXT":
                    excelType = "TEXT";
                    break;
                default:
                    excelType = "TEXT";
                    break;
            }

            return excelType;
        }
    }
}
