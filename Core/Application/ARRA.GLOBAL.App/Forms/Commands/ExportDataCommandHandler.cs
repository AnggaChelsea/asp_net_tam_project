﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportDataCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;
        public ExportDataCommandHandler(
            IExcelConnector excel, ITextFileConnector textfile,IConfiguration configuration) : base(configuration)
        {
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    IList<FilterModel> listFtr = new List<FilterModel>();
                    if (!String.IsNullOrEmpty(req.CommandModel.filter))
                    {
                        listFtr = JsonConvert.DeserializeObject<List<FilterModel>>(req.CommandModel.filter);
                    }

                    //get total record
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    p.Add("@FTR", ResolveFilter(listFtr));
                    p.Add("@USR_ID", req.UserIdentity.UserId);
                    IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                    long totalRecord = records.FirstOrDefault();

                    // get max row & page
                    int maxRow = 0;
                    int maxPage = 0;
                    bool isBatch = false;
                    if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                    {
                        string maxRowStr = await globalCtx.SystemParameterDetails
                            .Where(f => f.ParamCode == "GB217" && f.ParamDesc == "EXCEL_MAX_ROW")
                            .Select(c => c.ParamValue)
                            .FirstOrDefaultAsync(cancellationToken);
                        string maxPageStr = await globalCtx.SystemParameterDetails
                            .Where(f => f.ParamCode == "GB217" && f.ParamDesc == "EXCEL_MAX_PAGE")
                            .Select(c => c.ParamValue)
                            .FirstOrDefaultAsync(cancellationToken);
                        maxRow = Int32.Parse(maxRowStr);
                        maxPage = Int32.Parse(maxPageStr);

                        if (totalRecord > maxRow)
                        {
                            isBatch = true;
                        }
                    } else if (req.CommandModel.fileType == FileType.CSV.ToString())
                    {
                        string maxRowStr = await globalCtx.SystemParameterDetails
                            .Where(f => f.ParamCode == "GB217" && f.ParamDesc == "CSV_MAX_ROW")
                            .Select(c => c.ParamValue)
                            .FirstOrDefaultAsync(cancellationToken);
                        string maxPageStr = await globalCtx.SystemParameterDetails
                            .Where(f => f.ParamCode == "GB217" && f.ParamDesc == "CSV_MAX_PAGE")
                            .Select(c => c.ParamValue)
                            .FirstOrDefaultAsync(cancellationToken);
                        maxRow = Int32.Parse(maxRowStr);
                        maxPage = Int32.Parse(maxPageStr);

                        if (totalRecord > maxRow)
                        {
                            isBatch = true;
                        }
                    }

                    //get form info
                    FormHeader form = await globalCtx.FormHeaders.FindAsync(req.CommandModel.formCode);
                    IList<FormDetail> listColumns = await globalCtx.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                    //Get File Path
                    string filePath = await globalCtx.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    //get data
                    p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    p.Add("@FTR", ResolveFilter(listFtr));
                    p.Add("@SORT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.sort));
                    p.Add("@USR_ID", req.UserIdentity.UserId);
                    string fileName = "";

                    var exportlist = new ExportFileList
                    {
                        UserId = req.UserIdentity.UserId,
                        Module = form.Module,
                        FormCode = req.CommandModel.formCode,
                        JobQueueId = req.CommandModel.jobId,
                        Status = JobStatus.Progress.ToString(),
                        TotalRecord = null,
                        TotalInserted = null,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };

                    globalCtx.ExportFileLists.Add(exportlist);
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    downloadId = exportlist.UniqueId;

                    if (isBatch && req.CommandModel.fileType == FileType.EXCEL.ToString())
                    {
                        fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString() + ".zip";
                        _excel.WriteExcelBatch(globalCtx.Database.GetDbConnection(), p, listColumns, form.FormCode, filePath, fileName, totalRecord, maxRow, maxPage);
                    }
                    else if (isBatch && req.CommandModel.fileType == FileType.CSV.ToString())
                    {
                        fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString() + ".zip";
                        _textfile.WriteCSVBatch(globalCtx.Database.GetDbConnection(), p, listColumns, form.FormCode, filePath, fileName, totalRecord, maxRow, maxPage);
                    } else
                    {
                        using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                        {

                            fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                            if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                            {
                                fileName += ".xlsx";
                                _excel.WriteExcel(rd, listColumns, filePath + fileName);
                            }
                            else if (req.CommandModel.fileType == FileType.CSV.ToString())
                            {
                                fileName += ".csv";
                                _textfile.WriteCSV(rd, listColumns, filePath + fileName);
                            }
                            else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                            {
                                fileName += ".txt";
                                _textfile.WriteDelimited(rd, listColumns, filePath + fileName, form.TFDelimtedChar);
                            }
                            else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                            {
                                fileName += ".txt";
                                _textfile.WriteFixLength(rd, listColumns, filePath + fileName);
                            }

                            await globalCtx.SaveChangesAsync(cancellationToken);

                            rd.Close();
                        }
                    }

                    ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(exportlist.UniqueId);
                    explst.FileName = fileName;
                    explst.TotalRecord = totalRecord;
                    explst.TotalInserted = totalRecord;
                    explst.Status = JobStatus.Success.ToString();
                    explst.ModifiedBy = req.UserIdentity.UserId;
                    explst.ModifiedDate = req.CurrentDateTime;
                    explst.ModifiedHost = req.UserIdentity.Host;
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    explst = null;
                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    if (downloadId > 0)
                    {
                        ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(downloadId);
                        explst.Status = JobStatus.Failed.ToString();
                        explst.Message = message;
                        explst.ModifiedBy = req.UserIdentity.UserId;
                        explst.ModifiedDate = req.CurrentDateTime;
                        explst.ModifiedHost = req.UserIdentity.Host;
                        await globalCtx.SaveChangesAsync(cancellationToken);
                        explst = null;
                    }
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
    }
}