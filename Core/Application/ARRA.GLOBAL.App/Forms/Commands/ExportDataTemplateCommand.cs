﻿namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataTemplateCommand
    {
        public string formCode { get; set; }
        public string reportDt { get; set; }

        public long jobId { get; set; }
    }
}