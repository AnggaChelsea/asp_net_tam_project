﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ImportDataCommandHandler : AppBase, IRequestHandler<CommandsModel<ImportDataCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly IConfiguration _configuration;
        //public ImportDataCommandHandler(
        //    GLOBALDbContext context, IExcelConnector excel, IConfiguration configuration) : base(context)
        //{
        //    _context = context;
        //    _excel = excel;
        //    _configuration = configuration;
        //}
        public ImportDataCommandHandler(
            IExcelConnector excel, IConfiguration configuration) : base(configuration)
        {
            _excel = excel;
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<ImportDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long id = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    //get form info
                    FormHeader form = await globalCtx.FormHeaders.FindAsync(req.CommandModel.formCode);
                    string detailTableName = "", detailFK = "", linkPK = "";
                    if (!string.IsNullOrEmpty(form.LinkToFormCode))
                    {
                        FormHeader formDtl =  await globalCtx.FormHeaders.Where(f => f.FormCode == form.LinkToFormCode).FirstOrDefaultAsync(cancellationToken);
                        detailTableName = string.IsNullOrEmpty(formDtl.OriginalTableName) ? formDtl.TableName : formDtl.OriginalTableName;
                        detailFK = form.LinkFieldFK;
                        linkPK = form.LinkFieldPK;
                        formDtl = null;
                    }

                    List<string> listAuthBranch = new List<string>();
                    if (form.CheckBranch == true)
                    {
                        listAuthBranch = await (from a in globalCtx.Branches
                                                join b in globalCtx.BranchGroupDetails
                                                on a.BranchCode equals b.BranchCode
                                                join c in globalCtx.Users
                                                on b.GroupId equals c.BranchGroup
                                                where c.UserId == req.UserIdentity.UserId
                                                select a.RegulatorBranch
                                                  ).Distinct().ToListAsync(cancellationToken);
                    }
                    else if (form.CheckLocalBranch == true)
                    {
                        listAuthBranch = await (from a in globalCtx.BranchGroupDetails
                                                join c in globalCtx.Users
                                                on a.GroupId equals c.BranchGroup
                                                where c.UserId == req.UserIdentity.UserId
                                                select a.BranchCode
                                                  ).Distinct().ToListAsync(cancellationToken);
                    }

                    IList<FormDetail> listColumns = await globalCtx.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);
                    string db = "";
                    if (string.IsNullOrEmpty(form.DatabaseName))
                    {
                        ARRA.GLOBAL.Domain.Entities.Module md = await globalCtx.Modules.FindAsync(form.Module);
                        db = md.DatabaseName;
                        md = null;
                    }
                    else
                    {
                        db = form.DatabaseName.Trim();
                    }
                    string tablename = db + ".DBO." + form.TableName;
                    detailTableName = db + ".DBO." + detailTableName;

                    string parentTable = "", parentKey = "", parentFkKey = "";
                    FormHeader formParent = await globalCtx.FormHeaders.Where(f => f.LinkToFormCode == form.FormCode).FirstOrDefaultAsync(cancellationToken);
                    if (formParent != null && form.Module==ModuleType.GLOBAL.ToString()) //validate only for module global.
                    {
                        parentTable = string.IsNullOrEmpty(formParent.OriginalTableName) == true ? formParent.TableName : formParent.OriginalTableName;
                        parentTable = db + ".DBO." + parentTable;
                        parentKey = formParent.LinkFieldPK;
                        parentFkKey = formParent.LinkFieldFK;
                    }
                    formParent = null;

                    //Get File Path
                    string filePath = await globalCtx.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.IMPORT_FILE_LOCATION.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);

                    ImportFileListHeader hdrLog = new ImportFileListHeader
                    {
                        UserId = req.CommandModel.userId,
                        Status = JobStatus.Progress.ToString(),
                        FileName = req.CommandModel.fileName,
                        OriginalFileName = req.CommandModel.oriFileName,
                        FormCode = req.CommandModel.formCode,
                        Module = form.Module,
                        PeriodType = form.PeriodType,
                        ProcessName = form.FormName,
                        JobQueueId = req.CommandModel.queueId,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };
                    globalCtx.ImportFileListHeaders.Add(hdrLog);
                    await globalCtx.SaveChangesAsync(cancellationToken);

                    id = hdrLog.UniqueId;

                    long totalRecord = await _excel.ImportData(globalCtx.Database.GetDbConnection(), _configuration, req.UserIdentity, tablename, listColumns, filePath + req.CommandModel.fileName, id, req.CommandModel.formCode, linkPK, detailTableName, detailFK,parentTable,parentKey,parentFkKey,
                        form.CheckBranch,form.CheckLocalBranch,listAuthBranch);

                    long totalFailed = await globalCtx.ImportFileListDetails.Where(f => f.HeaderId == id).LongCountAsync(cancellationToken);

                    var entity = await globalCtx.ImportFileListHeaders.FindAsync(id);
                    entity.TotalRecord = totalRecord;
                    entity.TotalProceed = totalRecord;
                    entity.TotalFailed = totalFailed;
                    entity.Status = totalFailed == 0 ? JobStatus.Success.ToString() : JobStatus.Failed.ToString();
                    entity.ModifiedBy = req.UserIdentity.UserId;
                    entity.ModifiedDate = req.CurrentDateTime;
                    entity.ModifiedHost = req.UserIdentity.Host;
                    globalCtx.ImportFileListHeaders.Update(entity);
                    await globalCtx.SaveChangesAsync(cancellationToken);

                    entity = null;
                    form = null;
                    hdrLog = null;
                    listColumns = null;
                    listAuthBranch = null;

                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;

                    if (id > 0)
                    {
                        var entity = await globalCtx.ImportFileListHeaders.FindAsync(id);
                        entity.Status = JobStatus.Failed.ToString();
                        entity.Message = ex.Message;
                        entity.ModifiedBy = req.UserIdentity.UserId;
                        entity.ModifiedDate = req.CurrentDateTime;
                        entity.ModifiedHost = req.UserIdentity.Host;
                        globalCtx.ImportFileListHeaders.Update(entity);
                        await globalCtx.SaveChangesAsync(cancellationToken);
                        entity = null;
                    }
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = id
            };
        }
    }
}