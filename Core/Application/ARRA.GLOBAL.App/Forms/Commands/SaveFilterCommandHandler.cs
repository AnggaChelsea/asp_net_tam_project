﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class SaveFilterCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveFilterCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFilterCommandHandler(
            GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFilterCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            IList<FilterModel> ftrs = req.CommandModel.filters.Where(f => f.defaultftr == false).ToList();
            var listftr = await _context.UserFormFilters.Where(f => f.UserId == req.UserIdentity.UserId && f.FormId == req.CommandModel.formCode).ToListAsync(cancellationToken);

            if (ftrs.Count > 0 || listftr.Count > 0)
            {
                if (listftr.Count > 0)
                {
                    _context.RemoveRange(listftr);
                }

                UserFormFilter m;
                for (int i = 0; i < ftrs.Count; i++)
                {
                    m = new UserFormFilter
                    {
                        FormId = req.CommandModel.formCode,
                        FieldName = ftrs[i].field,
                        FieldSeq = Convert.ToInt16(i + 1),
                        Operator = ftrs[i].optr,
                        UserId = req.UserIdentity.UserId,
                        DefaultValue = ftrs[i].value,
                        DefaultValueType = ftrs[i].valuetype,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };

                    _context.UserFormFilters.Add(m);
                }

                await _context.SaveChangesAsync(cancellationToken);
            }
            else
            {
                throw new CustomException(ErrorMessages.AddFilterToSave);
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}