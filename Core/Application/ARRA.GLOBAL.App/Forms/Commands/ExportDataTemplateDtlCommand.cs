﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataTemplateDtlCommand
    {
        public string formCode { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
        public string fileType { get; set; }
        public long jobId { get; set; }
    }
}