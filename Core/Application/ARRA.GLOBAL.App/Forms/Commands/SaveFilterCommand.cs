﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class SaveFilterCommand
    {
        public string formCode { get; set; }
        public IList<FilterModel> filters { get; set; }
    }
}