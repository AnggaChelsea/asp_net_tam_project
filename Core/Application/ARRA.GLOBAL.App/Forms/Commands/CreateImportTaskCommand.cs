﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class CreateImportTaskCommand
    {
        public string formCode { get; set; }
        public string fileName { get; set; }
        public string originalFileName { get; set; }
    }
}