﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class CreateExportTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateExportTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public CreateExportTaskCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateExportTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }

            long downloadId = 0;
            long jobQueueId = 0;

            if (form.ExportDirect == true)
            {
                CommandsModel<ExportDataCommand, StatusModel> cmd = new CommandsModel<ExportDataCommand, StatusModel>();
                cmd.UserIdentity = new UserIdentityModel
                {
                    UserId = req.UserIdentity.UserId,
                    Host = req.UserIdentity.Host
                };
                cmd.CurrentDateTime = req.CurrentDateTime;
                cmd.CommandModel = new ExportDataCommand
                {
                    fileType = req.CommandModel.fileType,
                    filter = req.CommandModel.filter,
                    formCode = req.CommandModel.formCode,
                    sort = req.CommandModel.sort,
                    jobId = 0
                };
                StatusModel stsM = await _mediator.Send(cmd);
                if (stsM != null)
                {
                    downloadId = stsM.id;
                }
                cmd = null;
                stsM = null;
            }
            else
            {
                //add to job queue list
                JobQueueHeader header = new JobQueueHeader
                {
                    JobType = QueueJobType.EXPORT.ToString(),
                    PeriodType = form.PeriodType,
                    Status = JobStatus.Temp.ToString(),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                _context.JobQueueHeaders.Add(header);
                await _context.SaveChangesAsync(cancellationToken);

                jobQueueId = header.UniqueId;

                List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FORM_CD",
                    ParamValue = req.CommandModel.formCode,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILTER",
                    ParamValue = req.CommandModel.filter,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "SORT",
                    ParamValue = req.CommandModel.sort,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_TYPE",
                    ParamValue = req.CommandModel.fileType,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                _context.JobQueueDetails.AddRange(listDetail);
                header.Status = JobStatus.Pending.ToString();

                await _context.SaveChangesAsync(cancellationToken);

                header = null;
                listDetail = null;
            }


            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
                id = form.ExportDirect == true ? downloadId : jobQueueId
            };
        }
    }
}