﻿using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class CreateExportTaskAdhocCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateExportTaskAdhocCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public CreateExportTaskAdhocCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateExportTaskAdhocCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            long downloadId = 0;

            if (req.CommandModel.isServerSide == "YES")
            {
                long jobQueueId = 0;
                JobQueueHeader header = new JobQueueHeader
                {
                    JobType = QueueJobType.EXPORT_ADHOC.ToString(),
                    PeriodType = "ADHOC",
                    Status = JobStatus.Temp.ToString(),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                _context.JobQueueHeaders.Add(header);
                await _context.SaveChangesAsync(cancellationToken);

                List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = header.UniqueId,
                    ParamName = "QUERY",
                    ParamValue = req.CommandModel.queryVal,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = header.UniqueId,
                    ParamName = "FILE_TYPE",
                    ParamValue = req.CommandModel.fileType,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                _context.JobQueueDetails.AddRange(listDetail);
                header.Status = JobStatus.Pending.ToString();

                await _context.SaveChangesAsync(cancellationToken);
            }
            else
            {
                CommandsModel<ExportDataAdhocCommand, StatusModel> cmd = new CommandsModel<ExportDataAdhocCommand, StatusModel>();
                cmd.UserIdentity = new UserIdentityModel
                {
                    UserId = req.UserIdentity.UserId,
                    Host = req.UserIdentity.Host
                };
                cmd.CurrentDateTime = req.CurrentDateTime;
                cmd.CommandModel = new ExportDataAdhocCommand
                {
                    fileType = req.CommandModel.fileType,
                    formCode = req.CommandModel.formCode,
                    queryVal = req.CommandModel.queryVal
                };
                StatusModel stsM = await _mediator.Send(cmd);
                if (stsM != null)
                {
                    downloadId = stsM.id;
                }
                cmd = null;
                stsM = null;
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
                id = downloadId
            };
        }
    }
}
