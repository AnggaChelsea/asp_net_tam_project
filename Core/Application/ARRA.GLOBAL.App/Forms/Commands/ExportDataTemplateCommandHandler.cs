﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Interfaces;
//using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;
using ARRA.MODULE.Domain.Entities;
using System.Security.Cryptography;
using ARRA.GLOBAL.App.Jobs.Models;
using System;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ExportDataTemplateCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportDataTemplateCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;

        public ExportDataTemplateCommandHandler(
            GLOBALDbContext context, IExcelConnector excel, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _excel = excel;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportDataTemplateCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            ARRA.GLOBAL.Domain.Entities.FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                ExportFileList exportFileList = new ExportFileList()
                {
                    UserId = req.UserIdentity.UserId,
                    Module = form.Module,
                    FormCode = req.CommandModel.formCode,
                    JobQueueId = req.CommandModel.jobId,
                    Status = JobStatus.Progress.ToString(),
                    TotalRecord = 100,
                    TotalInserted = 0,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };
                ctx.Add(exportFileList);
                await ctx.SaveChangesAsync(cancellationToken);

                DynamicParameters dpExcelConf= new DynamicParameters();
                dpExcelConf.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                dpExcelConf.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.reportDt));
                var excelConf = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_EXCEL_CONF", dpExcelConf, null, null, CommandType.StoredProcedure);

                string templatePath = excelConf.First().TEMPLATE_PATH;
                string outputPath = excelConf.First().OUTPUT_PATH;
                string sheetName = excelConf.First().SHEET_NAME;

                DynamicParameters dpCellValue = new DynamicParameters();
                dpCellValue.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                dpCellValue.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.reportDt));
                var cellValueList = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_EXCEL_CELL_VAL", dpCellValue, null, null, CommandType.StoredProcedure);

                // get static cell value from job detail
                string[] ftrStaticCell = { "FORM_CD", "REPORT_DT" };
                var staticCell = await (from a in ctx.JobQueueDetails
                                        where a.HeaderId == req.CommandModel.jobId && !ftrStaticCell.Contains(a.ParamName)
                                        select new DetailModel
                                        {
                                            paramName = a.ParamName,
                                            paramValue = a.ParamValue,
                                        }).ToListAsync(cancellationToken);

                foreach (var cell in staticCell)
                {
                    var parNm = cell.paramName;
                    var parVl = cell.paramValue;

                    var spltPar = parNm.Split("_");

                    if (cellValueList.Any(x => Convert.ToString(x.ROW_SEQ) == spltPar[0] && Convert.ToString(x.COLUMN_EXCEL_POS) == spltPar[1]))
                    {
                        cellValueList = cellValueList.Select(item =>
                        {
                            if (Convert.ToString(item.ROW_SEQ) == spltPar[0] && Convert.ToString(item.COLUMN_EXCEL_POS) == spltPar[1])
                            {
                                item.COLUMN_VALUE = parVl;
                            }

                            return item;
                        });
                    }
                }

                _excel.WriteExcelTemplate(templatePath, outputPath, sheetName, cellValueList.ToList());

                ExportFileList explst = await ctx.ExportFileLists.FindAsync(exportFileList.UniqueId);
                explst.FileName = excelConf.First().OUTPUT_NAME;
                explst.Status = JobStatus.Success.ToString();
                explst.TotalInserted = 100;                
                explst.ModifiedBy = req.UserIdentity.UserId;
                explst.ModifiedDate = req.CurrentDateTime;
                explst.ModifiedHost = req.UserIdentity.Host;
                await ctx.SaveChangesAsync(cancellationToken);
                explst = null;
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

    }
}