﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class CreateExportTaskAdhocCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
        public string queryVal { get; set; }
        public string isServerSide { get; set; }
    }
}
