﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Commands
{
    public class ImportDataCommand
    {
        public string formCode { get; set; }
        public string fileName { get; set; }
        public string oriFileName { get; set; }
        public long queueId { get; set; }
        public string userId { get; set; }
    }
}