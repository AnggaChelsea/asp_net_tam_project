﻿using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Shared
{
    public class Function
    {
        public static DynamicFilterModel GetFilterConds(IDictionary<string,List<object>> dicftr,string formFilterExp)
        {
            DynamicFilterModel m = new DynamicFilterModel();

            string ftrexp = "";
            List<object> ftrargs = new List<object>();
            if (dicftr.Count > 0)
            {
                foreach (KeyValuePair<string, List<object>> item in dicftr)
                {
                    ftrexp = item.Key;
                    ftrargs = item.Value;
                }
            }

            if (!string.IsNullOrEmpty(formFilterExp)) //ex: FIELD1|VALUE1^FIELD2|VALUE2
            {
                if (formFilterExp.Contains("|"))
                {
                    string[] strs = formFilterExp.Split('^');
                    for (int x = 0; x < strs.Length; x++)
                    {
                        if (!string.IsNullOrEmpty(strs[x]))
                        {
                            string[] vals = strs[x].Split('|');
                            if (vals.Length == 2)
                            {
                                ftrexp += " AND " + vals[0] + " = @" + ftrargs.Count;
                                ftrargs.Add(vals[1]);
                            }
                        }//
                    }
                }
            }

            m.filterParams = ftrexp;
            m.values = ftrargs.ToArray();

            return m;
        }
    }
}
