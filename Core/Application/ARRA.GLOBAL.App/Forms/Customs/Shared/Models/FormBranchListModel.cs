﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class FormBranchListModel
    {
        public IList<TreeModel> branch { get; set; }
        public IList<TreeModel> form { get; set; }
    }
}
