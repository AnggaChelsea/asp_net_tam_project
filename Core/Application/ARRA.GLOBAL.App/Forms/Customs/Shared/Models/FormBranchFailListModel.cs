﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class FormBranchFailListModel
    {
        public IList<FormBranchFailModel> errors { get; set; }
    }
}
