﻿using System.Collections.Generic;
namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class SelectionListModel
    {
        public IList<string> data { get; set; }
    }
}
