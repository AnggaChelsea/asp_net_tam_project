﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class DynamicFilterModel
    {
        public string filterParams { get; set; }
        public object[] values { get; set; }
    }
}