﻿using System.Collections.Generic;
namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class TreeModel
    {
        public string value { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string parent { get; set; }
        public bool? select { get; set; }
        public bool? isValue { get; set; }
        public IList<TreeModel> child { get; set; }
    }
}