﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Shared.Models
{
    public class FormBranchFailModel
    {
        public string form { get; set; }
        public string branch { get; set; }
        public string message { get; set; }
    }
}
