﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Models;
using ARRA.GLOBAL.App.Exceptions;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Commands
{
    public class DownloadEFCommandHandler : AppBase, IRequestHandler<CommandsModel<DownloadEFCommand, ListFileExportedModel>, ListFileExportedModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public DownloadEFCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<ListFileExportedModel> Handle(CommandsModel<DownloadEFCommand, ListFileExportedModel> req, CancellationToken cancellationToken)
        {
            ListFileExportedModel vm = new ListFileExportedModel();

            string err = "";
            if (string.IsNullOrEmpty(req.CommandModel.select) == false)
            {
                List<Int64> lst = req.CommandModel.select.Split('$').Where(f => f != "").Select(Int64.Parse).ToList();
                
                // select paramvalue and module
                var paths = _context.SystemParameterHeaders
                        .Where(f => f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                        .Select(f => new { f.ParamValue, f.Module })
                        .ToList();

                try
                {
                    using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
                    {
                        // select filename and module from ExportFileLists
                        var files = await ctx.ExportFileLists.Where(f => lst.Contains(f.UniqueId))
                                .Select(c => new { c.FileName, c.Module })
                                .ToListAsync(cancellationToken);

                        List<FileExportedModel> lstMdl = new List<FileExportedModel>();

                        foreach (var item in files)
                        {
                            FileExportedModel mdl = new FileExportedModel();
                            mdl.file = item.FileName;
                            mdl.filePath = paths.Where(a => a.Module == item.Module).Select(c => c.ParamValue).FirstOrDefault() + @"\";

                            lstMdl.Add(mdl);
                        }

                        vm.files = lstMdl;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                err = "Please chooose file to download";
            }

            if (err != "")
            {
                throw new CustomException(err);
            }

            return vm;
        }
    }
    
    // OLD 20221201
    //public class DownloadEFCommandHandler : AppBase, IRequestHandler<CommandsModel<DownloadEFCommand, FileExportedListModel>, FileExportedListModel>
    //{
    //    private readonly GLOBALDbContext _context;
    //    private readonly IConfiguration _configuration;
    //    public DownloadEFCommandHandler(
    //        GLOBALDbContext context,IConfiguration configuration) : base(context,configuration)
    //    {
    //        _context = context;
    //        _configuration = configuration;
    //    }

    //    public async Task<FileExportedListModel> Handle(CommandsModel<DownloadEFCommand, FileExportedListModel> req, CancellationToken cancellationToken)
    //    {
    //        FileExportedListModel vm = new FileExportedListModel();

    //        string err = "";
    //        if (string.IsNullOrEmpty(req.CommandModel.select) == false)
    //        {
    //            List<Int64> select = req.CommandModel.select.Split('$').Where(f=>f!="").Select(Int64.Parse).ToList();
    //            if (select.Count > 1)
    //            {
    //                err = "You can download only one file";
    //            } else if (select.Count == 1)
    //            {
    //                try
    //                {
    //                    using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
    //                    {
    //                        vm.files = await ctx.ExportFileLists.Where(f => select.Contains(f.UniqueId))
    //                            .Select(c=>c.FileName)
    //                            .ToListAsync(cancellationToken);

    //                        string path = await _context.SystemParameterHeaders
    //                            .Where(f => f.Module == req.AccessMatrix.Module 
    //                                && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
    //                            .Select(c=>c.ParamValue)
    //                            .FirstOrDefaultAsync(cancellationToken);
    //                        vm.filePath = path + @"\";

    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    throw ex;
    //                }
    //            }
    //        }
    //        else
    //        {
    //            err = "Please chooose file to download";
    //        }

    //        if (err != "")
    //        {
    //            throw new CustomException(err);
    //        }

    //        return vm;
    //    }
    //}
}