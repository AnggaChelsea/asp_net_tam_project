﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using System.Linq.Dynamic.Core;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.GLOBAL.Domain.Enumerations;
using System;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Queries
{
    public class GetDownloadEFSelectAllQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDownloadEFSelectAllQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetDownloadEFSelectAllQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetDownloadEFSelectAllQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == CreatedDateField.CREATED_DT.ToString()).FirstOrDefault();

            FilterModel ftr_ctr = null;

            var ctr_filter = req.QueryModel.filter;
            int index;
            List<int> IndexList = new List<int>();

            foreach (var a in ctr_filter)
            {
                if (a.field != CreatedDateField.CREATED_DT.ToString() || !string.IsNullOrEmpty(a.value))
                {
                    ftr_ctr = a;
                }

                index = req.QueryModel.filter.IndexOf(a);

                if (string.IsNullOrEmpty(a.value))
                {
                    IndexList.Add(index);
                }
            }

            foreach (var f in IndexList)
            {
                req.QueryModel.filter.RemoveAt(f);
            }

            ftr_ctr = req.QueryModel.filter.Where(f => f.field == CreatedDateField.CREATED_DT.ToString()).FirstOrDefault();

            if (ftr_ctr != null)
            {
                if (!string.IsNullOrEmpty(ftr_ctr.value))
                {
                    TMP_Form form = await _context.FormHeaders.Where(f => f.FormCode == req.QueryModel.formCode)
                                    .Select(c => new TMP_Form
                                    {
                                        Module = c.Module,
                                        Filter = c.FilterConditionEx
                                    })
                                    .FirstOrDefaultAsync(cancellationToken);

                    //get active only
                    req.QueryModel.filter.Add(new FilterModel { field = "STS", optr = "EQUALS", datatype = "TEXT", value = "SUCCESS" });

                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterNoConvertDateDynamicLinq(req.QueryModel.filter), form.Filter);
                    
                    using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                    {
                        try
                        {
                            listSelect = await ctx.ExportFileLists
                            .Select(c => new
                            {
                                uid = c.UniqueId,
                                USR_ID = c.UserId,
                                MODULE = c.Module,
                                FORM_CD = c.FormCode,
                                JOB_QUEUE_ID = c.JobQueueId,
                                FILE_NM = c.FileName,
                                TOTAL_RECORD = c.TotalRecord,
                                STS = c.Status,
                                MESSAGE = c.Message,
                                APPV_ID = c.ApprovalId,
                                CREATED_BY = c.CreatedBy,
                                CREATED_DT = c.CreatedDate,
                            })
                            .Where(objFtr.filterParams, objFtr.values)
                            .Select(c => c.uid.ToString())
                            .ToListAsync(cancellationToken);
                        }
                        catch(Exception ex)
                        {
                            var xx = ex;
                        }

                        
                        objFtr = null;
                    }
                    form = null;
                    objFtr = null;
                }
            }

            return new SelectionListModel
            {
                data = listSelect
            };
        }
    }
    public class TMP_Form
    {
        public string Module { get; set; }
        public string Filter { get; set; }
    }
}