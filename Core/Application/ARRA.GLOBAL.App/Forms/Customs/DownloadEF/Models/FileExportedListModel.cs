﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Models
{
    public class FileExportedListModel
    {
        public IList<string> files { get; set; }
        public string filePath { get; set; }
    }

    public class FileExportedModel
    {
        public string file { get; set; }
        public string filePath { get; set; }
    }

    public class ListFileExportedModel
    {
        public IList<FileExportedModel> files { get; set; }
    }
}