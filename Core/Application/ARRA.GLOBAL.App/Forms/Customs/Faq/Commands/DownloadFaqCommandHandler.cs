﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Customs.Faq.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Customs.Faq.Commands
{
    class DownloadFaqCommandHandler : AppBase, IRequestHandler<CommandsModel<DownloadFaqCommand, FaqFileListModel>, FaqFileListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public DownloadFaqCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<FaqFileListModel> Handle(CommandsModel<DownloadFaqCommand, FaqFileListModel> req, CancellationToken cancellationToken)
        {
            FaqFileListModel vm = new FaqFileListModel();
            string err = "";

            if (string.IsNullOrEmpty(req.CommandModel.select) == false)
            {
                List<Int64> select = req.CommandModel.select.Split('$').Where(f => f != "").Select(Int64.Parse).ToList();
                if (select.Count > 0)
                {
                    try
                    {
                        vm.files = await _context.ManualFaqs.Where(f => select.Contains(f.UniqueId))
                            .Select(c => new FaqFileModel
                            {
                                fileName = c.DocumentName,
                                filePath = c.FilePath
                            })
                            .ToListAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                err = "Please select file to download";
            }

            if (err != "")
            {
                throw new CustomException(err);
            }

            return vm;
        }
    }
}
