﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Faq.Models
{
    public class FaqFileModel
    {
        public string fileName { get; set; }
        public string filePath { get; set; }
    }
}
