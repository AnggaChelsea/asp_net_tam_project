﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Faq.Models
{
    public class FaqFileListModel
    {
        public IList<FaqFileModel> files { get; set; }
    }
}
