﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.User.Commands
{
    public class SaveUserCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveUserCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        private readonly IAuthenticationService _authenticationService;
        private readonly IConfiguration _configuration;

        public SaveUserCommandHandler(
            GLOBALDbContext context, IMediator mediator,
            IAuthenticationService authenticationService, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _mediator = mediator;
            _authenticationService = authenticationService;
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveUserCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel sts = new StatusModel();
            var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
            JToken userId,locksts;
            jObject.TryGetValue("USR_ID", out userId);
            jObject.TryGetValue("LOCK_STS", out locksts);
            if (userId != null)
            {
                //check user exits
                if (req.CommandModel.action == ScreenAction.ADD.ToString())
                {
                    int exists = await _context.Users.Where(f => f.UserId == Convert.ToString(userId)).CountAsync(cancellationToken);
                    if (exists > 0)
                    {
                        throw new CustomException(ErrorMessages.UserExists);
                    }
                }

                //QA Request
                if (req.CommandModel.action == ScreenAction.REMOVE.ToString())
                {
                    bool isActive = await _context.Users.Where(f => f.UserId == Convert.ToString(userId)).Select(c => c.Status).FirstOrDefaultAsync(cancellationToken);
                    if (isActive == true)
                    {
                        throw new CustomException(ErrorMessages.UserActiveDelete);
                    }
                }

                //QA Request
                if (req.CommandModel.action == ScreenAction.ADD.ToString() || req.CommandModel.action == ScreenAction.EDIT.ToString())
                {
                    JToken appvGrup;
                    jObject.TryGetValue("APPROVAL_GROUP", out appvGrup);
                    bool validateGroup = false;
                    if (appvGrup != null)
                    {
                        if (Convert.ToString(appvGrup) != "")
                        {
                            if (await _context.ApprovalGroupDetails.Where(f => f.GroupId == Convert.ToString(appvGrup)).CountAsync(cancellationToken) == 0)
                            {
                                validateGroup = false;
                                throw new CustomException(ErrorMessages.ApprovalGroupEmpty);
                            }
                        }
                        else validateGroup = true;
                    }
                    else
                    {
                        validateGroup = true;
                    }
                    //QA Request
                    if (validateGroup)
                    {
                        JToken menuGrup;
                        jObject.TryGetValue("MENU_GROUP", out menuGrup);
                        if (menuGrup != null)
                        {
                            int count = await _context.MenuGroupDetails.Where(f => f.GroupId == Convert.ToString(menuGrup) && f.AllowApproval == true).Take(1).CountAsync(cancellationToken);
                            if (count > 0)
                            {
                                throw new CustomException(ErrorMessages.ApprovalGroupRequired);
                            }
                        }
                    }
                }

                string saveNewPass = "";
                string IS_LDAP = _configuration.GetSection("LdapSettings:IS_LDAP").Value;

                if (IS_LDAP != "1")
                {
                    if (req.CommandModel.action == ScreenAction.ADD.ToString())
                    {
                        saveNewPass = _authenticationService.Encrypt(req.CommandModel.password);
                    }
                    else if (req.CommandModel.action == ScreenAction.EDIT.ToString())
                    {
                        ARRA.GLOBAL.Domain.Entities.User usr = await _context.Users.FindAsync(Convert.ToString(userId));
                        if (usr.Password.Equals(req.CommandModel.password))
                        {
                            saveNewPass = usr.Password;
                        }
                        else
                        {
                            if (!req.CommandModel.password.Equals(req.CommandModel.confirmPassword))
                            {
                                throw new CustomException(ErrorMessages.PasswordNotMatch);
                            }
                            saveNewPass = _authenticationService.Encrypt(req.CommandModel.password);
                        }
                    }
                } 
                else if(IS_LDAP == "1" && !req.CommandModel.password.Equals(string.Empty))
                {
                    if (!req.CommandModel.password.Equals(req.CommandModel.confirmPassword))
                    {
                        throw new CustomException(ErrorMessages.PasswordNotMatch);
                    }
                    saveNewPass = _authenticationService.Encrypt(req.CommandModel.password);
                    jObject.Properties().Where(f => f.Name == "PASSWD").FirstOrDefault().Value = saveNewPass;
                    jObject.Properties().Where(f => f.Name == "PASSWD_CRFM").FirstOrDefault().Value = saveNewPass;
                }
            
                if (!req.CommandModel.action.Equals(ScreenAction.REMOVE.ToString()))
                {
                    if (IS_LDAP != "1")
                    {
                        jObject.Properties().Where(f => f.Name == "PASSWD").FirstOrDefault().Value = saveNewPass;
                        jObject.Properties().Where(f => f.Name == "PASSWD_CRFM").FirstOrDefault().Value = saveNewPass;
                    }
                    if (req.CommandModel.action == ScreenAction.EDIT.ToString())
                    {
                        if (!ConvertValue.ToBoolean(locksts))
                        {
                            ARRA.GLOBAL.Domain.Entities.User usr = await _context.Users.FindAsync(Convert.ToString(userId));
                            usr.LockCounter = 0;
                            //_context.Users.Update(usr);
                            await _context.SaveChangesAsync(cancellationToken);

                            //jObject.Properties().Where(f => f.Name == "LOCK_CTR").FirstOrDefault().Value = "0";
                        }
                    }
                }

                //run base function
                CommandsModel<SaveDataCommand, StatusModel> cmd = new CommandsModel<SaveDataCommand, StatusModel>();
                cmd.UserIdentity = new UserIdentityModel
                {
                    UserId = req.UserIdentity.UserId,
                    Host = req.UserIdentity.Host
                };
                cmd.AccessMatrix = req.AccessMatrix;
                cmd.CurrentDateTime = req.CurrentDateTime;
                
                cmd.CommandModel = new SaveDataCommand
                {
                    action = req.CommandModel.action,
                    formCode = req.CommandModel.formCode,
                    data = jObject,
                    details = null
                };

                await _mediator.Send(cmd);


                sts.status = CommandStatus.Success.ToString();
            }
            else
            {
                sts.status = CommandStatus.Failed.ToString();
                sts.message = "User Not Found";
            }


            return sts;
        }

    }
}