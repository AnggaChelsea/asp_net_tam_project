﻿using FluentValidation;
using System;
using ARRA.Common;
using ARRA.GLOBAL.App.Forms.Customs.User.Commands;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class SaveUserCommandValidator : AbstractValidator<SaveUserCommand>
    {
        public SaveUserCommandValidator(IConfiguration configuration)
        {

            string IS_LDAP = configuration.GetSection("LdapSettings:IS_LDAP").Value;

            //using custom validator to retrieve password value.
            RuleFor(x => x.data).Custom((x, context) => {
                var jObject = ((Newtonsoft.Json.Linq.JObject)x);
                JToken action, pwd,pwd_before, confirmpass;
                jObject.TryGetValue("screenActionType", out action);
                
                    jObject.TryGetValue("PASSWD", out pwd);
                    jObject.TryGetValue("PASSWD_before", out pwd_before);
                    jObject.TryGetValue("PASSWD_CRFM", out confirmpass);
                
                string act = Convert.ToString(action);
                string pass = Convert.ToString(pwd);
                string pass_before = Convert.ToString(pwd_before);
                string cpass = Convert.ToString(confirmpass);
                jObject = null;

                ARRA.Common.Model.ApplicationSettings sett = new Common.Model.ApplicationSettings(configuration);

                if (IS_LDAP != "1")
                {

                    if (act == ScreenAction.ADD.ToString() || (act == ScreenAction.EDIT.ToString() && pass != pass_before))
                    {
                        if (string.IsNullOrEmpty(pass))
                        {
                            context.AddFailure("password", ErrorMessages.PasswordEmpty);
                        }
                        else if (!pass.Equals(cpass))
                        {
                            context.AddFailure("password", ErrorMessages.PasswordNotMatch);
                        }
                        else if (pass.Length < sett.PasswordMinLength)
                        {
                            context.AddFailure("password", string.Format(ErrorMessages.PasswordLength, sett.PasswordMinLength.ToString()));
                        }
                        else if (!Regex.IsMatch(pass, "[A-Z]") && sett.PasswordUppercase)
                        {
                            context.AddFailure("password", ErrorMessages.PasswordUppercaseLetter);
                        }
                        else if (!Regex.IsMatch(pass, "[a-z]") && sett.PasswordLowercase)
                        {
                            context.AddFailure("password", ErrorMessages.PasswordLowercaseLetter);
                        }
                        else if (!Regex.IsMatch(pass, "[0-9]") && sett.PasswordNumeric)
                        {
                            context.AddFailure("password", ErrorMessages.PasswordDigit);
                        }
                        else if (!Regex.IsMatch(pass, "[^a-zA-Z0-9]") && sett.PasswordSpecialChar)
                        {
                            context.AddFailure("password", ErrorMessages.PasswordSpecialCharacter);
                        }
                    }
                }

            });
        }
        

        private string GetPassword(dynamic data)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)data);
            JToken pass;
            jObject.TryGetValue("PASSWD", out pass);
            jObject = null;
            return Convert.ToString(pass);
        }

        private string GetConfirmPassword(dynamic data)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)data);
            JToken confirmpass;
            jObject.TryGetValue("PASSWD_CRFM", out confirmpass);
            jObject = null;
            return Convert.ToString(confirmpass);
        }

    }
}