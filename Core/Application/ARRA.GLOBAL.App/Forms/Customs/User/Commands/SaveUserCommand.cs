﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.User.Commands
{
    public class SaveUserCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }

        public string password { get; set; }
        public string confirmPassword { get; set; }
    }
}