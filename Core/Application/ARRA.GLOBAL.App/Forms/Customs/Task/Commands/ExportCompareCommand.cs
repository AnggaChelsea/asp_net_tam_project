﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class ExportCompareCommand
    {
        public string filePath { get; set; }
        public string module { get; set; }
        public string formCode { get; set; }
        public string fileName { get; set; }
        public string reportDt { get; set; }
        public string comparisonDt { get; set; }
        public long approvalId { get; set; }
        public long jobId { get; set; }
    }
}