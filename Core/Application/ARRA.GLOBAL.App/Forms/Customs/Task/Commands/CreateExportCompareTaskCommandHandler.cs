﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using ARRA.Common.Enumerations;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class CreateExportCompareTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateExportCompareTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public CreateExportCompareTaskCommandHandler(
            GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateExportCompareTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            ApprovalHeader hdr = await _context.ApprovalHeaders.FindAsync(Convert.ToInt64(req.CommandModel.approvalId));
            if (hdr == null)
            {
                throw new NotFoundException(typeof(ApprovalHeader).ToString(), req.CommandModel.approvalId.ToString());
                ;
            }

            //add to job queue list
            JobQueueHeader header = new JobQueueHeader
            {
                JobType = QueueJobType.EXPORT_COMPARE.ToString(),
                //PeriodType = hdr.
                Status = JobStatus.Temp.ToString(),
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            };

            _context.JobQueueHeaders.Add(header);
            await _context.SaveChangesAsync(cancellationToken);

            long jobQueueId = header.UniqueId;

            List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "FORM_CD",
                ParamValue = hdr.FormCode,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });

            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "FILE_NAME",
                ParamValue = hdr.FileName,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });

            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "APPV_ID",
                ParamValue = req.CommandModel.approvalId,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });

            _context.JobQueueDetails.AddRange(listDetail);

            header.Status = JobStatus.Pending.ToString();

            await _context.SaveChangesAsync(cancellationToken);


            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
                id = Convert.ToInt64(req.CommandModel.approvalId)
            };
        }
    }
}