﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class ExportSumCompareCommand
    {
        public string filePath { get; set; }
        public string module { get; set; }
        public string formCode { get; set; }
        public string fileName { get; set; }
        public string reportDt { get; set; }
        public string comparisonDt { get; set; }
        public long approvalId { get; set; }
        public long jobId { get; set; }
    }
}
