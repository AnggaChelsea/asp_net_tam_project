﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class ApprovalCommand
    {
        public Int64 uid { get; set; }
        public string action { get; set; }
        public string note { get; set; }
    }
}