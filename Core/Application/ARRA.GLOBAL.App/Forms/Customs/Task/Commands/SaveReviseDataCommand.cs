﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class SaveReviseDataCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }
    }
}