﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class ApprovalCommandHandler : AppBase, IRequestHandler<CommandsModel<ApprovalCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        private readonly IAuthenticationService _authenticationService;
        private readonly IConfiguration _configuration;
        public ApprovalCommandHandler(
            GLOBALDbContext context, IMediator mediator,
            IAuthenticationService authenticationService,
            IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _mediator = mediator;
            _authenticationService = authenticationService;
            _configuration = configuration;
        }


        public async Task<StatusModel> Handle(CommandsModel<ApprovalCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel sts = new StatusModel();

            ApprovalHeader hdr = await _context.ApprovalHeaders.FindAsync(req.CommandModel.uid);
            ARRA.GLOBAL.Domain.Entities.User usr = await _context.Users.FindAsync(hdr.CreatedBy);
            string action = req.CommandModel.action.Trim();

            //check if this user authorized to approved this record. or revoke own record.
            int exist = await _context.ApprovalGroupDetails.Where(f => f.GroupId == usr.ApprovalGroup && f.UserId == req.UserIdentity.UserId).CountAsync(cancellationToken);
            if (exist > 0 || (action == ApprovalStatus.Revoke.ToString() && hdr.CreatedBy==req.UserIdentity.UserId))
            {
                //QA request.
                if (hdr.ApprovalStatus != ApprovalStatus.Pending.ToString())
                {
                    throw new CustomException(ErrorMessages.ItemActioned);
                }

                if (ApprovalStatus.Approved.ToString() == action ||
                    ApprovalStatus.Rejected.ToString() == action ||
                    ApprovalStatus.Revoke.ToString() == action)
                {
                    hdr.ApprovalStatus = action;
                }
                else if (ApprovalStatus.Revise.ToString() == action)
                {
                    hdr.ApprovalStatus = ApprovalStatus.Pending.ToString();
                    hdr.TaskType = TaskType.Revise.ToString();
                }

                hdr.Note = req.CommandModel.note.Trim();
                hdr.ModifiedBy = req.UserIdentity.UserId;
                hdr.ModifiedDate = req.CurrentDateTime;
                hdr.ModifiedHost = req.UserIdentity.Host;

                IList<ApprovalHeader> hdrDetail = await _context.ApprovalHeaders.Where(f => f.LinkToHeaderId == hdr.UniqueId).ToListAsync(cancellationToken);
                if (hdrDetail.Count > 0)
                {
                    foreach (ApprovalHeader item in hdrDetail)
                    {
                        item.ApprovalStatus = hdr.ApprovalStatus;
                        item.TaskType = hdr.TaskType;
                        item.ModifiedBy = req.UserIdentity.UserId;
                        item.ModifiedDate = req.CurrentDateTime;
                        item.ModifiedHost = req.UserIdentity.Host;
                    }
                }
                _context.ApprovalHeaders.UpdateRange(hdrDetail);

                bool allowSave = true;
                if (ApprovalStatus.Approved.ToString() == action)
                {
                    try
                    {
                        // rizwan 20201218 add approval gl adjustment
                        if (hdr.FormCode == "F_ANGLADJ" && hdr.ActionType == "PROCESS")
                        {
                            DynamicParameters p = new DynamicParameters();
                            p.Add("@APPV_ID", req.CommandModel.uid);
                            p.Add("@USR_ID", req.UserIdentity.UserId);
                            p.Add("@HOST", req.UserIdentity.Host);
                            p.Add("@APPROVAL", 1);
                            await _context.Database.GetDbConnection().ExecuteAsync("DBO.UDPU_EXECUTE_GL_ADJUSTMENT", p, null, null, CommandType.StoredProcedure);
                        }
                        else if (hdr.ActionType == "RECALC_PROCESS")
                        {
                            var appDetail = await _context.ApprovalDetails.Where(aprv => aprv.HeaderId == hdr.UniqueId).ToArrayAsync(cancellationToken);
                            string module = appDetail.Where(dtl => dtl.FieldName == "MODULE").FirstOrDefault().ValueAfter;
                            string reportDt = appDetail.Where(dtl => dtl.FieldName == "REPORT_DT").FirstOrDefault().ValueAfter;
                            string formCD = appDetail.Where(dtl => dtl.FieldName == "FORM_CD").FirstOrDefault().ValueAfter;
                            string rowSeq = appDetail.Where(dtl => dtl.FieldName == "ROW_SEQ").FirstOrDefault().ValueAfter;
                            string columnSeq = appDetail.Where(dtl => dtl.FieldName == "COLUMN_SEQ").FirstOrDefault().ValueAfter;
                            string newValue = appDetail.Where(dtl => dtl.FieldName == "NEW_VALUE").FirstOrDefault().ValueAfter;

                            using (MODULEDbContext ctx = base.GetDbContext(module))
                            {
                                DynamicParameters dpTable = new DynamicParameters();
                                dpTable.Add("@REPORT_DT", reportDt);
                                dpTable.Add("@FORM_CD", formCD);
                                dpTable.Add("@ROW_SEQ", rowSeq);
                                dpTable.Add("@COLUMN_SEQ", columnSeq);
                                dpTable.Add("@NEW_VALUE", newValue);
                                await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPP_REPORT_MANUAL_RECALCULATE", dpTable, null, null, CommandType.StoredProcedure);
                            }
                        }
                        else
                        {
                            DynamicParameters p = new DynamicParameters();
                            p.Add("@APPV_ID", req.CommandModel.uid);
                            p.Add("@USR_ID", req.UserIdentity.UserId);
                            p.Add("@HOST", req.UserIdentity.Host);
                            p.Add("@APPROVAL", 1);
                            await _context.Database.GetDbConnection().ExecuteAsync("DBO.UDPU_EXECUTE_CRUD", p, null, null, CommandType.StoredProcedure);
                        }

                        sts.status = CommandStatus.Success.ToString();
                    }
                    catch (SqlException ex)
                    {
                        sts.status = CommandStatus.Failed.ToString();
                        sts.message = ex.Message;
                        allowSave = false;

                        //throw new DatabaseException(ex.Number, ex.Message);
                        throw new DatabaseException(ex.Number, ARRA.Common.ErrorMessages.GetSQLErrorMessage(ex.Number, ex.Message));
                    }
                }
                else
                {
                    sts.status = CommandStatus.Success.ToString();
                }

                if (allowSave)
                {
                    await _context.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                sts.status = CommandStatus.Failed.ToString();
                sts.message = ErrorMessages.NotAUthorizedToAction;
            }

            hdr = null;
            usr = null;

            return sts;
        }

    }
}