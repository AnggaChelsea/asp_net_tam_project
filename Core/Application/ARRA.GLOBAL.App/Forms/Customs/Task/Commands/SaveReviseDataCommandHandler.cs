﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;
using ARRA.GLOBAL.App.Interfaces;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class SaveReviseDataCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveReviseDataCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IAuthenticationService _auth;
        public SaveReviseDataCommandHandler(
            GLOBALDbContext context, IConfiguration configuration, IAuthenticationService auth) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
            _auth = auth;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveReviseDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if (menu != null)
            {
                FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);

                if (form == null)
                {
                    throw new NotFoundException(nameof(FormHeader), req.CommandModel.formCode);
                }

                //custom validation
                req.CommandModel.data = await this.Validation(form.FormCode, form.Module, form.CheckBranch, form.CheckLocalBranch, req.UserIdentity.BranchGroup, req.CommandModel.data, req.CommandModel.details);

                IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                if (formDetail.Count > 0)
                {
                    //save header
                    var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
                    JToken action, uid;
                    jObject.TryGetValue("screenActionType", out action);
                    jObject.TryGetValue("UID", out uid);
                    if (Convert.ToString(uid)=="") { uid = "0"; }
                    
                    using (var trans = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            ApprovalHeader hdr = await _context.ApprovalHeaders.FindAsync(Convert.ToInt64(uid));
                            if (hdr != null)
                            {
                                hdr.TaskType = TaskType.Approval.ToString();
                                hdr.ModifiedBy = req.UserIdentity.UserId;
                                hdr.ModifiedDate = req.CurrentDateTime;
                                hdr.ModifiedHost = req.UserIdentity.Host;

                                IList<ApprovalDetail> dtlofhdr = await _context.ApprovalDetails.Where(f => f.HeaderId == hdr.UniqueId).ToListAsync(cancellationToken);
                                _context.ApprovalDetails.RemoveRange(dtlofhdr);
                                dtlofhdr = null;

                                await _context.SaveChangesAsync(cancellationToken);

                                foreach (FormDetail item in formDetail)
                                {
                                    JToken value, valueBefore;
                                    jObject.TryGetValue(item.FieldName, out value);
                                    jObject.TryGetValue(item.FieldName + "_before", out valueBefore);

                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = hdr.UniqueId,
                                        ValueAfter = this.GetValue(item.EdrControlType, ARRA.Common.Utility.SafeSqlString(Convert.ToString(value))),
                                        ValueBefore = Convert.ToString(valueBefore),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                await _context.SaveChangesAsync(cancellationToken);

                                if (!string.IsNullOrEmpty(form.LinkToFormCode))
                                {
                                    formDetail = await _context.FormDetails.Where(f => f.FormCode == form.LinkToFormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                                    //detail
                                    if (req.CommandModel.details.Count > 0)
                                    {
                                        foreach (dynamic item in req.CommandModel.details)
                                        {
                                            if (item != null)
                                            {
                                                jObject = ((Newtonsoft.Json.Linq.JObject)item);

                                                jObject.TryGetValue("screenActionType", out action);
                                                jObject.TryGetValue("UID", out uid);
                                                if (Convert.ToString(uid) == "") { uid = "0"; }
                                                long uidlong = Convert.ToInt64(uid);
                                                ApprovalHeader dHeader;
                                                bool addToDetail = false;
                                                if (uidlong == 0)
                                                {
                                                    dHeader = new ApprovalHeader
                                                    {
                                                        MenuCode = menu.MenuCode,
                                                        MenuName = menu.MenuName,
                                                        ActionType = base.GetActionType(Convert.ToString(action)),
                                                        ApprovalStatus = ApprovalStatus.Pending.ToString(),
                                                        Module = menu.Module,
                                                        RefferalId = 0,
                                                        TaskType = TaskType.Approval.ToString(),
                                                        FormCode = form.LinkToFormCode,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host,
                                                        LinkToHeaderId = hdr.UniqueId
                                                    };

                                                    _context.ApprovalHeaders.Add(dHeader);

                                                    await _context.SaveChangesAsync(cancellationToken);

                                                    addToDetail = true;

                                                }
                                                else
                                                {
                                                    dHeader = await _context.ApprovalHeaders.FindAsync(uidlong);
                                                    dHeader.ModifiedBy = req.UserIdentity.UserId;
                                                    dHeader.ModifiedDate = req.CurrentDateTime;
                                                    dHeader.ModifiedHost = req.UserIdentity.Host;

                                                    IList<ApprovalDetail> listDetail = await _context.ApprovalDetails.Where(f => f.HeaderId == uidlong).ToListAsync(cancellationToken);
                                                    _context.ApprovalDetails.RemoveRange(listDetail);
                                                    listDetail = null;
                                                    
                                                    //compare with first submit action.
                                                    if (dHeader.ActionType == ScreenAction.ADD.ToString())
                                                    {
                                                        if (base.GetActionType(Convert.ToString(action)) == ScreenAction.EDIT.ToString()) //update to added item
                                                        {
                                                            addToDetail = true;
                                                        }
                                                        else if (base.GetActionType(Convert.ToString(action)) == ScreenAction.REMOVE.ToString()) //cancel add
                                                        {
                                                            addToDetail = false;
                                                        }
                                                    }
                                                    else if (dHeader.ActionType == ScreenAction.EDIT.ToString())
                                                    {
                                                        if (base.GetActionType(Convert.ToString(action)) == ScreenAction.REMOVE.ToString()) //cancel edit
                                                        {
                                                            addToDetail = false;
                                                        }
                                                        else if (base.GetActionType(Convert.ToString(action)) == ScreenAction.EDIT.ToString())//update previous edit.
                                                        {
                                                            addToDetail = true;
                                                        }
                                                    }
                                                    else if (dHeader.ActionType == ScreenAction.REMOVE.ToString())
                                                    {
                                                        if (base.GetActionType(Convert.ToString(action)) == ScreenAction.REMOVE.ToString()) //cancel delete
                                                        {
                                                            addToDetail = false;
                                                        }
                                                        else if (base.GetActionType(Convert.ToString(action)) == ScreenAction.EDIT.ToString()) //cancel remove and update data.
                                                        {
                                                            addToDetail = true;
                                                            dHeader.ActionType = ScreenAction.EDIT.ToString();
                                                        }
                                                    }

                                                    if (addToDetail == false)
                                                    {
                                                        _context.ApprovalHeaders.Remove(dHeader);
                                                    }

                                                    await _context.SaveChangesAsync(cancellationToken);

                                                }

                                                if (addToDetail)
                                                {
                                                    foreach (FormDetail item1 in formDetail)
                                                    {
                                                        JToken value, valueBefore;
                                                        jObject.TryGetValue(item1.FieldName, out value);
                                                        jObject.TryGetValue(item1.FieldName + "_before", out valueBefore);

                                                        _context.ApprovalDetails.Add(new ApprovalDetail
                                                        {
                                                            FieldName = item1.FieldName,
                                                            FormColName = item1.GrdColumnName,
                                                            FormSeq = Convert.ToInt16(item1.GrdColumnSeq),
                                                            HeaderId = dHeader.UniqueId,
                                                            ValueAfter = this.GetValue(item1.EdrControlType, ARRA.Common.Utility.SafeSqlString(Convert.ToString(value))),
                                                            ValueBefore = Convert.ToString(valueBefore),
                                                            CreatedBy = req.UserIdentity.UserId,
                                                            CreatedDate = req.CurrentDateTime,
                                                            CreatedHost = req.UserIdentity.Host
                                                        });
                                                    }

                                                    await _context.SaveChangesAsync(cancellationToken);
                                                }

                                            }

                                        }
                                    }

                                }
                                //

                            }
                            trans.Commit();
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();

                            throw ex;
                        }
                    }

                }
            }
            else
            {
                throw new NotFoundException("Menu not found", "not found");
            }


            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

        #region CustomValidation
        private async Task<dynamic> Validation(string formCode, string module, bool? checkBranch, bool? checkLocalBranch, string branchGroup, dynamic data,List<dynamic> details)
        {
            dynamic overwriteData = data;
            try
            {
                if (checkBranch == true)
                {
                    var jObject = ((Newtonsoft.Json.Linq.JObject)data);

                    //check branch AUTH
                    JToken branch;
                    jObject.TryGetValue("RG_BRANCH", out branch);
                    if (branch != null)
                    {
                        int count = await (from a in _context.BranchGroupDetails
                                           join b in _context.Branches on a.BranchCode equals b.BranchCode
                                           where a.GroupId == branchGroup
                                           && b.RegulatorBranch == branch.ToString()
                                           select a.BranchCode
                                       ).CountAsync();

                        if (count == 0)
                        {
                            throw new CustomException(string.Format(ErrorMessages.NotAuthorizedBranch, branch.ToString()));
                        }
                    }

                    //check branch lock server side
                    JToken reportdt;
                    jObject.TryGetValue("REPORT_DT", out reportdt);
                    jObject.TryGetValue("RG_BRANCH", out branch);
                    if (branch != null && reportdt != null)
                    {
                        using (MODULEDbContext ctx = base.GetDbContext(module))
                        {
                            long lockData =
                                await ctx.LockDatas
                                    .Where(f => f.ReportDate == Convert.ToDateTime(reportdt)
                                        && f.FormCode == formCode && f.Branch == branch.ToString())
                                    .CountAsync();
                            if (lockData > 0)
                            {
                                throw new CustomException(ErrorMessages.BranchLocked);
                            }
                        }
                    }
                    jObject = null;
                }
                else if (checkLocalBranch == true)
                {
                    var jObject = ((Newtonsoft.Json.Linq.JObject)data);

                    //check branch AUTH
                    JToken branch;
                    jObject.TryGetValue("BRANCH", out branch);
                    if (branch != null)
                    {
                        int count = await _context.BranchGroupDetails.Where(f => f.GroupId == branchGroup
                           && f.BranchCode == branch.ToString()).CountAsync();
                        if (count == 0)
                        {
                            throw new CustomException(string.Format(ErrorMessages.NotAuthorizedBranch, branch.ToString()));
                        }
                    }

                    //check branch lock server side
                    JToken reportdt;
                    jObject.TryGetValue("REPORT_DT", out reportdt);
                    jObject.TryGetValue("RG_BRANCH", out branch);
                    if (branch != null && reportdt != null)
                    {
                        using (MODULEDbContext ctx = base.GetDbContext(module))
                        {
                            long lockData =
                                await ctx.LockDatas
                                    .Where(f => f.ReportDate == Convert.ToDateTime(reportdt)
                                        && f.FormCode == formCode && f.Branch == branch.ToString())
                                    .CountAsync();
                            if (lockData > 0)
                            {
                                throw new CustomException(ErrorMessages.BranchLocked);
                            }
                        }
                    }
                    jObject = null;
                }


                //validation for specified forms
                //validate user
                dynamic userData = await this.ValidateUser(formCode, data);
                if (userData != null)
                {
                    overwriteData = userData;
                }
                //validate ApprovalGroup
                await ValidateApprovalGroup(formCode, overwriteData, details);
                //validate BranchGroup
                await ValidateBranchGroup(formCode, overwriteData, details);

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return overwriteData;
        }
        private async System.Threading.Tasks.Task ValidateBranchGroup(string formCode, dynamic data, List<dynamic> details)
        {
            if (formCode == FormCodeCollection.F_GBBGR.ToString())
            {
                var jObject = ((Newtonsoft.Json.Linq.JObject)data);
                JToken groupId, uidApv;
                jObject.TryGetValue("GROUP_ID", out groupId);
                jObject.TryGetValue("UID", out uidApv);
                //QA Request.
                if (groupId != null)
                {

                    List<TMP_VAL_VALUE> listId = new List<TMP_VAL_VALUE>();
                    if (Convert.ToString(uidApv) != "" && Convert.ToString(uidApv) != "0")
                    {
                        listId = await (from a in _context.ApprovalHeaders
                                        join a1 in _context.ApprovalHeaders on a.UniqueId equals a1.LinkToHeaderId
                                        join d in _context.ApprovalDetails on a1.UniqueId equals d.HeaderId
                                        where a.UniqueId == Convert.ToInt64(uidApv)
                                         && d.FieldName == FieldCollection.BRANCH_CD.ToString()
                                        select new TMP_VAL_VALUE
                                        {
                                            id = a1.UniqueId,
                                            value = d.ValueAfter
                                        }).ToListAsync();
                    }

                    List<string> listErr = new List<string>();
                    if (details.Count > 0)
                    {
                        foreach (dynamic item in details)
                        {
                            if (item != null)
                            {
                                jObject = ((Newtonsoft.Json.Linq.JObject)item);
                                JToken branchCd, action, uid;
                                jObject.TryGetValue("BRANCH_CD", out branchCd);
                                jObject.TryGetValue("screenActionType", out action);
                                jObject.TryGetValue("UID", out uid);

                                string strBR = Convert.ToString(branchCd);
                                TMP_VAL_VALUE tmp = listId.Where(f => f.id == Convert.ToInt32(uid) && f.id != 0).FirstOrDefault();
                                if (tmp != null)
                                {
                                    tmp.value = strBR;
                                    if (Convert.ToString(action) == ScreenAction.REMOVE_DTL.ToString())
                                    {
                                        listId.Remove(tmp);
                                    }
                                }
                                else
                                {
                                    listId.Add(new TMP_VAL_VALUE { value = strBR, id = 0 });
                                }
                                jObject = null;
                            }
                        }
                    }

                    //List<TMP_VAL_VALUE> listId = await _context.BranchGroupDetails.Where(f => f.GroupId == Convert.ToString(groupId))
                    //    .Select(c => new TMP_VAL_VALUE
                    //    {
                    //        value = c.BranchCode,
                    //        id = c.UniqueId
                    //    }).ToListAsync();

                    string errs = "";
                    string flag = "";
                    List<TMP_VAL_VALUE> resultVal = listId.OrderBy(o => o.value).ToList();
                    foreach (TMP_VAL_VALUE item in resultVal)
                    {
                        if (flag == item.value)
                        {
                            if (!errs.Contains(item.value + ","))
                            {
                                errs += item.value + ",";
                            }
                        }
                        flag = item.value;
                    }
                    if (errs != "")
                    {
                        throw new CustomException(string.Format(ErrorMessages.DuplicateBranch, errs.Substring(0, errs.Length - 1)));
                    }
                    resultVal = null;
                }
            }
        }
        private async System.Threading.Tasks.Task ValidateApprovalGroup(string formCode, dynamic data, List<dynamic> details)
        {
            if (formCode == FormCodeCollection.F_GBAGR.ToString())
            {
                var jObject = ((Newtonsoft.Json.Linq.JObject)data);
                JToken groupId, uidApv;
                jObject.TryGetValue("GROUP_ID", out groupId);
                jObject.TryGetValue("UID", out uidApv);
                //QA Request.
                if (groupId != null)
                {
                    //List<TMP_VAL_VALUE> listId = await _context.ApprovalGroupDetails.Where(f => f.GroupId == Convert.ToString(groupId))
                    //    .Select(c => new TMP_VAL_VALUE { id = c.UniqueId, value = c.UserId }).ToListAsync();

                    List<TMP_VAL_VALUE> listId = new List<TMP_VAL_VALUE>();
                    if (Convert.ToString(uidApv) != "" && Convert.ToString(uidApv) != "0")
                    {
                        listId = await (from a in _context.ApprovalHeaders
                                        join a1 in _context.ApprovalHeaders on a.UniqueId equals a1.LinkToHeaderId
                                        join d in _context.ApprovalDetails on a1.UniqueId equals d.HeaderId
                                        where a.UniqueId == Convert.ToInt64(uidApv)
                                         && d.FieldName == FieldCollection.USR_ID.ToString()
                                        select new TMP_VAL_VALUE
                                        {
                                            id = a1.UniqueId,
                                            value = d.ValueAfter
                                        }).ToListAsync();
                    }

                    List<string> listErr = new List<string>();
                    if (details.Count > 0)
                    {
                        foreach (dynamic item in details)
                        {
                            if (item != null)
                            {
                                jObject = ((Newtonsoft.Json.Linq.JObject)item);
                                JToken userid, action, uid;
                                jObject.TryGetValue("USR_ID", out userid);
                                jObject.TryGetValue("screenActionType", out action);
                                jObject.TryGetValue("UID", out uid);

                                string strUser = Convert.ToString(userid);
                                TMP_VAL_VALUE tmp = listId.Where(f => f.id == Convert.ToInt32(uid) && f.id != 0).FirstOrDefault();
                                if (tmp != null)
                                {
                                    tmp.value = strUser;
                                    if (Convert.ToString(action) == ScreenAction.REMOVE_DTL.ToString())
                                    {
                                        listId.Remove(tmp);
                                    }
                                }
                                else
                                {
                                    listId.Add(new TMP_VAL_VALUE { value = strUser, id = 0 });
                                }
                                jObject = null;
                            }
                        }
                    }

                    string errs = "";
                    string flag = "";
                    List<TMP_VAL_VALUE> resultVal = listId.OrderBy(o => o.value).ToList();
                    foreach (TMP_VAL_VALUE item in resultVal)
                    {
                        if (flag == item.value)
                        {
                            if (!errs.Contains(item.value + ","))
                            {
                                errs += item.value + ",";
                            }
                        }
                        flag = item.value;
                    }

                    if (errs != "")
                    {
                        throw new CustomException(string.Format(ErrorMessages.DuplicateUser, errs.Substring(0, errs.Length - 1)));
                    }
                }

            }
        }
        private async Task<dynamic> ValidateUser(string formCode, dynamic data)
        {
            dynamic overwrite = data;
            if (formCode.ToUpper() == ARRA.Common.Enumerations.FormCodeCollection.F_GBUSR.ToString()) //screen user
            {
                var jObject = ((Newtonsoft.Json.Linq.JObject)overwrite);

                JToken pass, cpass, userid, locksts;
                jObject.TryGetValue("USR_ID", out userid);
                jObject.TryGetValue("PASSWD", out pass);
                jObject.TryGetValue("PASSWD_CRFM", out cpass);
                jObject.TryGetValue("LOCK_STS", out locksts);
                string strUser = Convert.ToString(userid);
                string strPass = Convert.ToString(pass);
                string strCPass = Convert.ToString(cpass);
                bool boolLocksts = ConvertValue.ToBoolean(locksts);

                ApplicationSettings sett = new ApplicationSettings(_configuration);
                string passBefore = await _context.Users.Where(f => f.UserId == strUser).Select(c => c.Password).FirstOrDefaultAsync();
                if (string.IsNullOrEmpty(passBefore)) passBefore = "";

                string IS_LDAP = _configuration.GetSection("LdapSettings:IS_LDAP").Value;

                if (IS_LDAP != "1")
                {
                    if (string.IsNullOrEmpty(strPass))
                    {
                        throw new CustomException(ErrorMessages.PasswordEmpty);
                    }
                    else if (strPass != passBefore || strCPass != passBefore)
                    {
                        if (!strPass.Equals(strCPass))
                        {
                            throw new CustomException(ErrorMessages.PasswordNotMatch);
                        }
                        else if (strPass.Length < sett.PasswordMinLength)
                        {
                            throw new CustomException(string.Format(ErrorMessages.PasswordLength, sett.PasswordMinLength.ToString()));
                        }
                        else if (!Regex.IsMatch(strPass, "[A-Z]") && sett.PasswordUppercase)
                        {
                            throw new CustomException(ErrorMessages.PasswordUppercaseLetter);
                        }
                        else if (!Regex.IsMatch(strPass, "[a-z]") && sett.PasswordLowercase)
                        {
                            throw new CustomException(ErrorMessages.PasswordLowercaseLetter);
                        }
                        else if (!Regex.IsMatch(strPass, "[0-9]") && sett.PasswordNumeric)
                        {
                            throw new CustomException(ErrorMessages.PasswordDigit);
                        }
                        else if (!Regex.IsMatch(strPass, "[^a-zA-Z0-9]") && sett.PasswordSpecialChar)
                        {
                            throw new CustomException(ErrorMessages.PasswordSpecialCharacter);
                        }
                        else //valid
                        {
                            strPass = _auth.Encrypt(strPass);
                            strCPass = strPass;

                            jObject.Properties().Where(f => f.Name == "PASSWD").FirstOrDefault().Value = strPass;
                            jObject.Properties().Where(f => f.Name == "PASSWD_CRFM").FirstOrDefault().Value = strPass;
                            if (!boolLocksts)
                            {
                                if(jObject.Property("LOCK_CTR") != null)
                                    jObject.Properties().Where(f => f.Name == "LOCK_CTR").FirstOrDefault().Value = "0";
                            }
                        }
                    }
                }

                //QA Request
                if (jObject.Property("MENU_GROUP") != null)
                {
                    int count = await _context.MenuGroupDetails.Where(f => f.GroupId == Convert.ToString(jObject.Property("MENU_GROUP").Value) && f.AllowApproval == true).Take(1).CountAsync();
                    if (count > 0)
                    {
                        throw new CustomException(ErrorMessages.ApprovalGroupRequired);
                    }
                }

                sett = null;
            }
            return overwrite;
        }
        #endregion

        #region Helper
        private string GetValue(string type,string value)
        {
            string result = value;
            if (type == ControlType.MULTICHOICE.ToString())
            {
                result = value.Replace(" ", "").Replace("]", "").Replace("[", "").Replace("\"", "").Replace("\r\n", "");
            }

            return result;
        }
        #endregion
    }
    public class TMP_VAL_VALUE
    {
        public long id { get; set; }
        public string value { get; set; }

    }
}