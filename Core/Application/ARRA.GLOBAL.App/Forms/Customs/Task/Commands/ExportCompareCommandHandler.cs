﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Commands
{
    public class ExportCompareCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportCompareCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly IMediator _mediator;
        public ExportCompareCommandHandler(GLOBALDbContext context, IMediator mediator, IExcelConnector excel, IConfiguration configuration) : base(configuration)
        {
            _context = context;
            _excel = excel;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportCompareCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long id = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    //get form info
                    FormHeader form = await globalCtx.FormHeaders.FindAsync(req.CommandModel.formCode);
                    IList<FormDetail> listColumns = await globalCtx.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);
                    string db = "";
                    if (string.IsNullOrEmpty(form.DatabaseName))
                    {
                        ARRA.GLOBAL.Domain.Entities.Module md = await globalCtx.Modules.FindAsync(form.Module);
                        db = md.DatabaseName;
                        md = null;
                    }
                    else
                    {
                        db = form.DatabaseName.Trim();
                    }
                    string tablename = db + ".DBO.TR_EXPORT_COMPARE_TEMP";

                    //Get Path Import
                    SystemParameterHeader parDetail = await globalCtx.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.IMPORT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string filePathImport = parDetail.ParamValue;

                    //Get Path Export
                    parDetail = await globalCtx.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string filePathExport = parDetail.ParamValue;
                    string expFileName = form.FormCode + "-" + req.CurrentDateTime.Ticks.ToString() + ".xlsx";

                    long totalRecord = 0;
                    //save to download list.
                    var exportlist = new ExportFileList
                    {
                        UserId = req.UserIdentity.UserId,
                        Module = form.Module,
                        FormCode = req.CommandModel.formCode,
                        JobQueueId = req.CommandModel.jobId,
                        Status = JobStatus.Progress.ToString(),
                        TotalRecord = null,
                        TotalInserted = null,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host,
                        ApprovalId = req.CommandModel.approvalId
                    };


                    globalCtx.ExportFileLists.Add(exportlist);
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    id = exportlist.UniqueId;

                    totalRecord = await _excel.WriteExcelCompare(globalCtx.Database.GetDbConnection(), listColumns, filePathImport + req.CommandModel.fileName, filePathExport + expFileName, tablename, req.CommandModel.formCode);

                    ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(exportlist.UniqueId);
                    explst.FileName = expFileName;
                    explst.TotalRecord = totalRecord;
                    explst.TotalInserted = totalRecord;
                    explst.Status = JobStatus.Success.ToString();
                    explst.ModifiedBy = req.UserIdentity.UserId;
                    explst.ModifiedDate = req.CurrentDateTime;
                    explst.ModifiedHost = req.UserIdentity.Host;
                    await globalCtx.SaveChangesAsync(cancellationToken);
                    explst = null;

                    form = null;
                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;
                    if (id > 0)
                    {
                        ExportFileList explst = await globalCtx.ExportFileLists.FindAsync(id);
                        explst.Status = JobStatus.Failed.ToString();
                        explst.Message = message;
                        explst.ModifiedBy = req.UserIdentity.UserId;
                        explst.ModifiedDate = req.CurrentDateTime;
                        explst.ModifiedHost = req.UserIdentity.Host;
                        await globalCtx.SaveChangesAsync(cancellationToken);
                        explst = null;
                    }
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = id
            };
        }

        //public async Task<StatusModel> Handle(CommandsModel<ExportCompareCommand, StatusModel> req, CancellationToken cancellationToken)
        //{

        //    string path = req.CommandModel.filePath;
        //    string expFileName = req.CommandModel.fileName;
        //    string fullPathFileName = path + req.CommandModel.fileName;

        //    string status = CommandStatus.Success.ToString();
        //    string message = "";
        //    long id = 0;
        //    try
        //    {
        //        var cmd = new GetFormSumCompare()
        //        {
        //            Module = req.CommandModel.module,
        //            FormCode = req.CommandModel.formCode,
        //            ReportDt = req.CommandModel.reportDt,
        //            ComparisonDt = req.CommandModel.comparisonDt
        //        };

        //        QueriesModel<GetFormSumCompare, FormSumCompareModel> qModel = new();
        //        qModel.UserIdentity = req.UserIdentity;
        //        qModel.AccessMatrix = req.AccessMatrix;
        //        qModel.CurrentDateTime = DateTime.Now;
        //        qModel.QueryModel = cmd;


        //        FormSumCompareModel masterData =  await _mediator.Send(qModel, cancellationToken);
        //        long totalRecord = 0;

        //        IList<dynamic> exportMasterData = new List<dynamic>();
        //        foreach(var data in masterData.Data)
        //        {
        //            exportMasterData.Add(new { id = data["id"], description = data["description"], rows = data["rows"], columns = getComparationColumns(data["columns"]) });
        //        }

        //        using (var _ModuledContext = base.GetDbContext(req.CommandModel.module)) 
        //        {
        //            //save to download list.
        //            var exportlist = new ExportFileList
        //            {
        //                UserId = req.UserIdentity.UserId,
        //                Module = req.CommandModel.module,
        //                FormCode = req.CommandModel.formCode,
        //                JobQueueId = req.CommandModel.jobId,
        //                FileName = expFileName,
        //                Status = JobStatus.Progress.ToString(),
        //                TotalRecord = null,
        //                TotalInserted = null,
        //                CreatedBy = req.UserIdentity.UserId,
        //                CreatedDate = req.CurrentDateTime,
        //                CreatedHost = req.UserIdentity.Host,
        //                ApprovalId = req.CommandModel.approvalId
        //            };

        //            _ModuledContext.ExportFileLists.Add(exportlist);
        //            await _ModuledContext.SaveChangesAsync(cancellationToken);

        //            totalRecord = await _excel.WriteExcelJObjectCompare(exportMasterData, fullPathFileName);

        //            exportlist.FileName = expFileName;
        //            exportlist.TotalRecord = totalRecord;
        //            exportlist.TotalInserted = totalRecord;
        //            exportlist.Status = JobStatus.Success.ToString();
        //            exportlist.ModifiedBy = req.UserIdentity.UserId;
        //            exportlist.ModifiedDate = req.CurrentDateTime;
        //            exportlist.ModifiedHost = req.UserIdentity.Host;
        //            await _ModuledContext.SaveChangesAsync(cancellationToken);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        using (var _ModuledContext = base.GetDbContext(req.CommandModel.module))
        //        {
        //            status = CommandStatus.Failed.ToString();
        //            message = ex.Message;
        //            if (id > 0)
        //            {
        //                ExportFileList explst = await _ModuledContext.ExportFileLists.Where(exp => exp.UniqueId == id).FirstOrDefaultAsync(cancellationToken);
        //                explst.Status = JobStatus.Failed.ToString();
        //                explst.Message = message;
        //                explst.ModifiedBy = req.UserIdentity.UserId;
        //                explst.ModifiedDate = req.CurrentDateTime;
        //                explst.ModifiedHost = req.UserIdentity.Host;
        //                await _ModuledContext.SaveChangesAsync(cancellationToken);
        //                explst = null;
        //            }
        //        }
        //    }

        //    return new StatusModel
        //    {
        //        status = status,
        //        message = message,
        //        id = id
        //    };
        //}

        //private List<dynamic> getComparationColumns(List<dynamic> columns)
        //{
        //    List<dynamic> newColumns = new();
        //    foreach (dynamic columnItem in columns)
        //    {
        //        if (Int64.Parse(columnItem.IS_HIDE) == 1) continue;

        //        var column = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(columnItem));
        //        newColumns.Add(column);

        //        if (Int64.Parse(columnItem.IS_COMPARE) != 1) continue;

        //        var _tmpColumnsPrev = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(columnItem));
        //        var columnPrev = _tmpColumnsPrev;
        //        columnPrev.COLUMN_ALIAS = columnPrev.COLUMN_ALIAS + "_PREV";
        //        columnPrev.COLUMN_DESC = columnPrev.COLUMN_DESC + " Prev";
        //        newColumns.Add(columnPrev);

        //        var _tmpColumnsDiff = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(columnItem));
        //        var columnDiff = _tmpColumnsDiff;
        //        columnDiff.COLUMN_ALIAS = columnDiff.COLUMN_ALIAS + "_DIFF";
        //        columnDiff.COLUMN_DESC = "Diff";
        //        newColumns.Add(columnDiff);
        //    }

        //    return newColumns;
        //}
    }
}