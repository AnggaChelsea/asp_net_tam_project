﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using ARRA.Common;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetExportedFileByAppvIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetExportedFileByAppvIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetExportedFileByAppvIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetExportedFileByAppvIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            ExportFileList exp = await _context.ExportFileLists.Where(f => f.ApprovalId == req.QueryModel.id).OrderByDescending(c=>c.UniqueId).Take(1).FirstOrDefaultAsync(cancellationToken);
            string fileName = "";
            string filePath = "";

            //this for check has authorized or not.
            //IList<ApprovalGroupDetail> apvDtl = await _context.ApprovalGroupDetails.Where(f => f.UserId == req.UserIdentity.UserId).Include(c=>c.GroupId).ToListAsync(cancellationToken);
            //User usr = await _context.Users.FindAsync(exp.UserId);
            //if(apvDtl.Contains(usr.ApprovalGroup))
            //{
            //}

            string err = "";
            if (exp != null)
            {
                if (!string.IsNullOrEmpty(exp.FileName))
                {
                    //Get File Path
                    string path = await _context.SystemParameterHeaders.Where(f => f.Module == exp.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);

                    fileName = exp.FileName;
                    filePath = path;
                }
                else if (exp.Status == JobStatus.Failed.ToString())
                {
                    err = exp.Message;
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath,
                error = err
            };
        }
    }
}