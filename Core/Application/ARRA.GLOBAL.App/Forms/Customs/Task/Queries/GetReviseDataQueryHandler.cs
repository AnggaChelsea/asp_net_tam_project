﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using Dapper;
using System.Data;
using System;
using ARRA.GLOBAL.Domain.Entities;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetReviseDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetReviseDataQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetReviseDataQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }


        public async Task<DataListModel> Handle(QueriesModel<GetReviseDataQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            int totalCounts = await _context.ApprovalHeaders.Where(f => f.LinkToHeaderId == req.QueryModel.uid && f.CreatedBy==req.UserIdentity.UserId).CountAsync(cancellationToken);
            IList<dynamic> result = new List<dynamic>();

            if (totalCounts > 0)
            {
                FilterConds mftr = ResolveReviseFilter(req.QueryModel.filter);
                SortConds msort = ResolveSorting(req.QueryModel.sort);
                DynamicParameters p = new DynamicParameters();
                p.Add("@APPV_ID", Convert.ToInt64(req.QueryModel.uid));
                p.Add("@FTR", mftr.filter);
                p.Add("@SORT", msort.sortconds);
                p.Add("@SORTASCDESC", msort.ascdesc);
                p.Add("@PAGE", req.QueryModel.start);
                p.Add("@PAGESIZE", req.QueryModel.pageSize);
                p.Add("@TOTAL_CONDS", mftr.totalftr);
                mftr = null;
                msort = null;

                IEnumerable<long> hdrs = await _context.Database.GetDbConnection().QueryAsync<long>("DBO.UDPS_GET_REVISE_DATA", p, null, null, CommandType.StoredProcedure);

                JObject objJs = null;
                IList<ApprovalDetail> dtls = null;
                foreach (long l in hdrs)
                {
                    dtls = await (from h in _context.ApprovalHeaders
                                  join d in _context.ApprovalDetails
                                      on h.UniqueId equals d.HeaderId
                                  where h.UniqueId == l
                                  select new ApprovalDetail
                                  {
                                      FieldName = d.FieldName,
                                      ValueAfter = d.ValueAfter,
                                      ValueBefore = d.ValueBefore,
                                      FormColName = h.ActionType
                                  }).ToListAsync(cancellationToken);

                    
                    if (dtls.Count > 0)
                    {
                        objJs = new JObject();
                        objJs.Add("ACTIONTPXX", dtls[0].FormColName);
                        foreach (ApprovalDetail dtl in dtls)
                        {
                            objJs.Add(dtl.FieldName, dtl.ValueAfter);
                            objJs.Add(dtl.FieldName + "_before", dtl.ValueBefore);
                        }

                        if (objJs.ContainsKey("UID"))
                        {
                            objJs["UID"] = l;
                        }
                        else
                        {
                            objJs.Add("UID", l);
                        }

                        result.Add(objJs.ToObject<dynamic>());
                        objJs = null;
                        dtls = null;
                    }

                }
            }

            return new DataListModel
            {
                totalRecord = totalCounts,
                data = result,
                aggregate = null
            };
        }

        private SortConds ResolveSorting(string sort)
        {
            string[] strs = sort.Replace("  ", " ").Split(' ');

            if (strs[0].Trim() == "UID")
            {
                return new SortConds();
            }

            return new SortConds
            {
                sortconds = "FIELD_NM='" + Utility.SafeSqlString(strs[0].Trim()) + "'",
                ascdesc = Utility.SafeSqlString(strs[1].Trim())
            };
        }

        private FilterConds ResolveReviseFilter(IList<FilterModel> list)
        {

            if (list == null) return new FilterConds();

            string result = "";
            FilterOperator opr;
            int ftrctr = 0;
            foreach (FilterModel m in list)
            {
                if (string.IsNullOrEmpty(m.optr))
                {
                    continue;
                }


                opr = Utility.ParseEnum<FilterOperator>(m.optr);
                if ((opr != FilterOperator.EMPTY && string.IsNullOrEmpty(m.value)) || string.IsNullOrEmpty(m.field) || string.IsNullOrEmpty(m.optr))
                    continue;
                string field = Utility.SafeSqlString(m.field.Trim());
                string value = Utility.SafeSqlString(m.value);
                ftrctr++;
                switch (m.optr)
                {
                    case "CONTAINS":
                        result += " (FIELD_NM='"+field + "' AND VAL_AFTER LIKE '%" + value + "%') ";
                        break;
                    case "INCLUDE":
                        string[] str = value.Split(',');
                        string conds = "";
                        for (int i = 0; i < str.Length; i++)
                        {
                            conds += "'" + str[i].Trim() + "',";
                        }
                        if (conds != "") conds = conds.Substring(0, conds.Length - 1);

                        result += " (FIELD_NM='" + field + "' AND VAL_AFTER IN (" + conds + ") ) ";
                        break;
                    case "EQUALS":
                        result += " (FIELD_NM='" + field + "' AND VAL_AFTER ='" + value + "' ) ";
                        break;
                    case "EMPTY":
                        result += " (FIELD_NM='" + field + "' AND TRIM(VAL_AFTER) = '') ";
                        break;
                    case "STARTWITH":
                        result += " (FIELD_NM='"+field + "' AND VAL_AFTER LIKE '" + value + "%') ";
                        break;
                    case "ENDWITH":
                        result += " (FIELD_NM='"+field + "' AND VAL_AFTER LIKE '%" + value + "') ";
                        break;
                    case "GREATER":
                        if (m.datatype == ControlType.MONEY.ToString() ||
                            m.datatype == ControlType.NUMBER.ToString() ||
                            m.datatype == ControlType.PERCENTAGE.ToString())
                        {
                            result += " (FIELD_NM='" + field + "' AND CAST(VAL_AFTER AS DECIMAL(32,2)) >'" + value + "') ";
                        }
                        else
                        {
                            result += " (FIELD_NM='" + field + "' AND VAL_AFTER >'" + value + "') ";
                        }
                        break;
                    case "GREATERTHAN":
                        if (m.datatype == ControlType.MONEY.ToString() ||
                            m.datatype == ControlType.NUMBER.ToString() ||
                            m.datatype == ControlType.PERCENTAGE.ToString())
                        {
                            result += " (FIELD_NM='" + field + "' AND CAST(VAL_AFTER AS DECIMAL(32,2)) >='" + value + "') ";
                        }
                        else
                        {
                            result += " (FIELD_NM='" + field + "' AND VAL_AFTER >= '" + value + "') ";
                        }
                        break;
                    case "LESS":
                        if (m.datatype == ControlType.MONEY.ToString() ||
                            m.datatype == ControlType.NUMBER.ToString() ||
                            m.datatype == ControlType.PERCENTAGE.ToString())
                        {
                            result += " (FIELD_NM='" + field + "' AND CAST(VAL_AFTER AS DECIMAL(32,2)) < '" + value + "') ";
                        }
                        else
                        {
                            result += " (FIELD_NM='" + field + "' AND VAL_AFTER < '" + value + "') ";
                        }
                        break;
                    case "LESSTHAN":
                        if (m.datatype == ControlType.MONEY.ToString() ||
                            m.datatype == ControlType.NUMBER.ToString() ||
                            m.datatype == ControlType.PERCENTAGE.ToString())
                        {
                            result += " (FIELD_NM='" + field + "' AND CAST(VAL_AFTER AS DECIMAL(32,2)) <='" + value + "') ";
                        }
                        else
                        {
                            result += " (FIELD_NM='" + field + "' AND VAL_AFTER <='" + value + "') ";
                        }
                        break;
                }
                result += " OR ";
            }

            if (!string.IsNullOrEmpty(result))
            {
                result = result.Substring(0, result.Length - 3);
            }

            return new FilterConds
            {
                filter = result,
                totalftr = ftrctr
            };
        }
    }
}

public class FilterConds
{
    public string filter { get; set; }
    public int totalftr { get; set; }
}
public class SortConds
{
    public string sortconds { get; set; }
    public string ascdesc { get; set; }
}