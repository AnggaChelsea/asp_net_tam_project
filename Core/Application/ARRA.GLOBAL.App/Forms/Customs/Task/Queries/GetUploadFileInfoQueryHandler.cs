﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Forms.Customs.Task.Models;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetUploadFileInfoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetUploadFileInfoQuery, FileInfoModel>, FileInfoModel>
    {
        private readonly GLOBALDbContext _context;

        public GetUploadFileInfoQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<FileInfoModel> Handle(QueriesModel<GetUploadFileInfoQuery, FileInfoModel> req, CancellationToken cancellationToken)
        {
            FileInfoModel vm = new FileInfoModel();

            vm.fileName =
                    await (from a in _context.ApprovalHeaders
                           join b in _context.ApprovalDetails
                            on a.UniqueId equals b.HeaderId
                           where a.LinkToHeaderId == req.QueryModel.id
                            && b.FormSeq == 8 //original filename
                           select b.ValueAfter
                           ).FirstOrDefaultAsync(cancellationToken);
            
            return vm;
        }
    }
}