﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetUploadFileByAppvIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetUploadFileByAppvIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetUploadFileByAppvIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetUploadFileByAppvIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            string fileName = "";
            string filePath = "";

            string createdBy = await _context.ApprovalHeaders.Where(f => f.UniqueId == req.QueryModel.id)
                .Select(c => c.CreatedBy).FirstOrDefaultAsync(cancellationToken);

            bool hasAccess = false;
            if (createdBy == req.UserIdentity.UserId)
            {
                hasAccess = true;//himself
            }
            else
            {
                IList<string> joinGroup =
                    await (from a in _context.ApprovalGroupHeaders
                           join b in _context.ApprovalGroupDetails
                            on a.GroupId equals b.GroupId
                           where a.Status == true
                            && b.UserId == req.UserIdentity.UserId
                           select a.GroupId
                           ).Distinct().ToListAsync(cancellationToken);
                string createdReportTo = await _context.Users.Where(f => f.UserId == createdBy)
                    .Select(c => c.ApprovalGroup).FirstOrDefaultAsync(cancellationToken);
                if (joinGroup.Contains(createdReportTo))
                {
                    hasAccess = true;
                }
                joinGroup = null;
            }

            if (hasAccess)
            {
                TMP_GET_FILE file =
                    await (from a in _context.ApprovalHeaders
                           join b in _context.ApprovalDetails
                            on a.UniqueId equals b.HeaderId
                           where a.LinkToHeaderId == req.QueryModel.id
                            && b.FormSeq == 6 //filename
                           select new TMP_GET_FILE
                           {
                               fileName = b.ValueAfter,
                               module = a.Module
                           }
                           ).FirstOrDefaultAsync(cancellationToken);

                if (file != null)
                {
                    fileName = file.fileName;
                    filePath =
                        await _context.SystemParameterHeaders.Where(f => f.Module == file.module && f.ParamName == SystemParameterType.MANUAL_FILE_PATH.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);

                }
            }
            return new FileModel
            {
                fileName = fileName,
                path = filePath
            };
        }
    }

    public class TMP_GET_FILE
    {
        public string fileName { get; set; }
        public string module { get; set; }
    }
}