﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetReviseDataQuery
    {
        public long uid { get; set; }
        public string formCode { get; set; }
        public int start { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public IList<FilterModel> filter { get; set; }
    }

}