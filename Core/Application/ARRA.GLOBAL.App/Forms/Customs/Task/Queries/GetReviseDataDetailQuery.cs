﻿namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetReviseDataDetailQuery
    {
        public string formCode { get; set; }
        public long uid { get; set; }
    }
}