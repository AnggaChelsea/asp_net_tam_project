﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using Dapper;
using System.Data;
using ARRA.GLOBAL.Domain.Entities;
using Newtonsoft.Json.Linq;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetReviseDataDetailQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetReviseDataDetailQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetReviseDataDetailQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetReviseDataDetailQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            IList<ApprovalDetail> dtls = await (from h in _context.ApprovalHeaders
                                                join d in _context.ApprovalDetails
                                                    on h.UniqueId equals d.HeaderId
                                                where h.UniqueId == req.QueryModel.uid
                                                    && h.CreatedBy == req.UserIdentity.UserId
                                                select new ApprovalDetail
                                                {
                                                    FieldName = d.FieldName,
                                                    ValueAfter = d.ValueAfter,
                                                    ValueBefore = d.ValueBefore,
                                                    HeaderId = d.HeaderId
                                                }).ToListAsync(cancellationToken);
            JObject objJs = new JObject();
            foreach (ApprovalDetail dtl in dtls)
            {
                objJs.Add(dtl.FieldName, dtl.ValueAfter);
                objJs.Add(dtl.FieldName+"_before", dtl.ValueBefore);
            }

            if (objJs.ContainsKey("UID"))
            {
                objJs["UID"] = dtls[0].HeaderId;
            }
            else
            {
                objJs.Add("UID", dtls[0].HeaderId);
            }

            dtls = null;

            IList<dynamic> result = new List<dynamic>();
            result.Add(objJs.ToObject<dynamic>());

            return new DataListModel
            {
                totalRecord = 1,
                data = result
            };

        }
    }
}