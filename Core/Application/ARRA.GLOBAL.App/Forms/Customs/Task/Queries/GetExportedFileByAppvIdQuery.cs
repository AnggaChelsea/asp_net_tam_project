﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Task.Queries
{
    public class GetExportedFileByAppvIdQuery
    {
        public long id { get; set; }
    }
}