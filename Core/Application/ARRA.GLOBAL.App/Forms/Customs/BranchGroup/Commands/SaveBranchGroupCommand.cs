﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.BranchGroup.Commands
{
    public class SaveBranchGroupCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }
    }
}