﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using ARRA.Common.Enumerations;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.ApprovalGroup.Commands
{
    public class SaveApprovalGroupCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveApprovalGroupCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public SaveApprovalGroupCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveApprovalGroupCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel sts = new StatusModel();

            var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
            JToken groupId;
            jObject.TryGetValue("GROUP_ID", out groupId);

            //QA Request.
            if (groupId != null) {
                if (req.CommandModel.action == ScreenAction.REMOVE.ToString())
                {
                    if (await _context.Users.Where(f => f.ApprovalGroup == groupId.ToString() && f.Status == true).CountAsync(cancellationToken) > 0)
                    {
                        throw new CustomException(ErrorMessages.ApprovalGroupActiveDelete);
                    }
                }

                List<TMP_APV_GROUP_VAL> listId = await _context.ApprovalGroupDetails.Where(f => f.GroupId == Convert.ToString(groupId))
                    .Select(c => new TMP_APV_GROUP_VAL { id = c.UniqueId, userid = c.UserId }).ToListAsync(cancellationToken);

                List<string> listErr = new List<string>();
                if (req.CommandModel.details.Count > 0)
                {
                    foreach (dynamic item in req.CommandModel.details)
                    {
                        if (item != null)
                        {
                            jObject = ((Newtonsoft.Json.Linq.JObject)item);
                            JToken userid, action, uid;
                            jObject.TryGetValue("USR_ID", out userid);
                            jObject.TryGetValue("screenActionType", out action);
                            jObject.TryGetValue("UID", out uid);

                            string strUser = Convert.ToString(userid);
                            TMP_APV_GROUP_VAL tmp = listId.Where(f => f.id == Convert.ToInt32(uid) && f.id != 0).FirstOrDefault();
                            if (tmp != null)
                            {
                                tmp.userid = strUser;
                                if (Convert.ToString(action) == ScreenAction.REMOVE_DTL.ToString())
                                {
                                    listId.Remove(tmp);
                                }
                            }
                            else
                            {
                                listId.Add(new TMP_APV_GROUP_VAL { userid = strUser, id = 0 });
                            }
                            jObject = null;
                        }
                    }
                }

                string errs = "";
                string flag = "";
                List<TMP_APV_GROUP_VAL> resultVal = listId.OrderBy(o => o.userid).ToList();
                foreach (TMP_APV_GROUP_VAL item in resultVal)
                {
                    if (flag == item.userid)
                    {
                        if (!errs.Contains(item.userid + ","))
                        {
                            errs += item.userid + ",";
                        }
                    }
                    flag = item.userid;
                }

                if (errs != "")
                {
                    throw new CustomException(string.Format(ErrorMessages.DuplicateUser, errs.Substring(0, errs.Length - 1)));
                }
            }

            //run base function
            CommandsModel<SaveDataCommand, StatusModel> cmd = new CommandsModel<SaveDataCommand, StatusModel>();
            cmd.UserIdentity = new UserIdentityModel
            {
                UserId = req.UserIdentity.UserId,
                Host = req.UserIdentity.Host,
                BranchGroup = req.UserIdentity.BranchGroup,
                GroupId = req.UserIdentity.GroupId,
                Modules = req.UserIdentity.Modules
            };
            cmd.AccessMatrix = req.AccessMatrix;
            cmd.CurrentDateTime = req.CurrentDateTime;
            cmd.CommandModel = new SaveDataCommand
            {
                action = req.CommandModel.action,
                formCode = req.CommandModel.formCode,
                data = req.CommandModel.data,
                details = req.CommandModel.details
            };

            await _mediator.Send(cmd);

            sts.status = CommandStatus.Success.ToString();


            return sts;
        }

    }
    public class TMP_APV_GROUP_VAL
    {
        public int id { get; set; }
        public string userid { get; set; }
    }
}