﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.ApprovalGroup.Commands
{
    public class SaveApprovalGroupCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }
    }
}