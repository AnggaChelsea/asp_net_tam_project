﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.ProcessMonitor.Commands
{
    public class CancelProcessCommandHandler : AppBase, IRequestHandler<CommandsModel<CancelProcessCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public CancelProcessCommandHandler(
            GLOBALDbContext context,IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<CancelProcessCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "DATA_DT").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.select) == false)
                    {
                        string[] select = req.CommandModel.select.Split('$');
                        if (select.Length > 0)
                        {

                            try
                            {
                                string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode)
                                    .Select(c => c.Module).FirstOrDefaultAsync<string>(cancellationToken);

                                IList<string> audit = new List<string>();
                                using (MODULEDbContext ctx = base.GetDbContext(module))
                                {
                                    for (int i = 0; i < select.Length; i++)
                                    {
                                        string procNo = select[i];
                                        if (!string.IsNullOrEmpty(procNo))
                                        {
                                            var hist = await ctx.ProcessLogHeaders.FindAsync(Convert.ToInt64(procNo));
                                            if (hist.ProcessStatus == JobStatus.Progress.ToString() || hist.ProcessStatus == JobStatus.Pending.ToString())
                                            {
                                                hist.ProcessStatus = JobStatus.Cancelling.ToString();
                                                hist.ModifiedBy = req.UserIdentity.UserId;
                                                hist.ModifiedDate = req.CurrentDateTime;
                                                hist.ModifiedHost = req.UserIdentity.Host;

                                                var prevJob = await ctx.JobQueueHeaders.FindAsync(hist.QueueId);
                                                string threadId = "";
                                                if (prevJob != null)
                                                {
                                                    prevJob.Status = JobStatus.Cancelled.ToString();
                                                    threadId = prevJob.ThreadId;
                                                }

                                                //add job queue
                                                ARRA.MODULE.Domain.Entities.JobQueueHeader hdrQueue = new ARRA.MODULE.Domain.Entities.JobQueueHeader
                                                {
                                                    JobType = QueueJobType.CANCEL_PROCESS.ToString(),
                                                    PeriodType = hist.PeriodType,
                                                    Status = JobStatus.Pending.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueHeaders.Add(hdrQueue);

                                                await ctx.SaveChangesAsync(cancellationToken);

                                                ARRA.MODULE.Domain.Entities.JobQueueDetail hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "DATA_DATE",
                                                    ParamValue = ftr.value,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(hdrQueueD);

                                                hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "JOB_ID",
                                                    ParamValue = hist.UniqueId.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(hdrQueueD);

                                                hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "JOB_QUEUE_ID",
                                                    ParamValue = hist.QueueId.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(hdrQueueD);

                                                hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "THREAD_ID",
                                                    ParamValue = threadId==null?"":threadId,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };

                                                ctx.JobQueueDetails.Add(hdrQueueD);

                                                await ctx.SaveChangesAsync(cancellationToken);

                                                audit.Add("DATA_DATE=" + ftr.value + ", JOB_ID=" + hist.UniqueId.ToString());
                                                hdrQueue = null;
                                                hdrQueueD = null;
                                            }
                                        }
                                    }
                                }

                                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync();
                                AuditTrailHeader hdrAudit = new AuditTrailHeader
                                {
                                    MenuCode = menu.MenuCode,
                                    MenuName = menu.MenuName,
                                    Module = ModuleType.GLOBAL.ToString(),
                                    WithApproval = false,
                                    ActivityAction = AuditType.CANCEL_PROCESS.ToString(),
                                    UserId = req.UserIdentity.UserId,
                                    ActivityDate = req.CurrentDateTime,
                                    ActivityHost = req.UserIdentity.Host,
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                };

                                _context.AuditTrailHeaders.Add(hdrAudit);

                                await _context.SaveChangesAsync(cancellationToken);

                                foreach (string str in audit)
                                {
                                    AuditTrailDetail dtlAudit = new AuditTrailDetail
                                    {
                                        FormColName = "PARAMETER",
                                        FieldName = "PARAMETER",
                                        FormSeq = 1,
                                        HeaderId = hdrAudit.UniqueId,
                                        ValueAfter = str,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    _context.AuditTrailDetails.Add(dtlAudit);
                                }

                                await _context.SaveChangesAsync(cancellationToken);

                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        err = "Please chooose process to cancel";
                    }
                }
                else
                {
                    err = "Please add date filter before run cancel";
                }
            }
            else
            {
                err = "Please add date filter before run cancel";
            }
            ftr = null;


            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}