﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.ProcessMonitor.Commands
{
    public class CancelProcessCommand
    {
        public string formCode { get; set; }
        public string select { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}