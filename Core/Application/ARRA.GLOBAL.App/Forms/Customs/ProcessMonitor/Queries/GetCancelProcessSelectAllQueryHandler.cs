﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using System.Linq.Dynamic.Core;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetCancelProcessSelectAllQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetCancelProcessSelectAllQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetCancelProcessSelectAllQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetCancelProcessSelectAllQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "DATA_DT").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), "");
                    string module = await _context.FormHeaders.Where(f => f.FormCode == req.QueryModel.formCode)
                                    .Select(c => c.Module).FirstOrDefaultAsync<string>(cancellationToken);

                    using (MODULEDbContext ctx = base.GetDbContext(module))
                    {
                        listSelect = await ctx.ProcessLogHeaders
                            .Select(c => new
                            {
                                UID = c.UniqueId,
                                DATA_DT = c.DataDate,
                                PROC_TP = c.ProcessType,
                                PROC_NO = c.ProcessNo,
                                PROC_NM = c.ProcessName,
                                PROC_STS = c.ProcessStatus,
                                CREATED_BY = c.CreatedBy,
                                CREATED_DT = c.CreatedDate,
                                PERIOD_TP = c.PeriodType,
                                BRANCH_CD = c.Branch
                            })
                            .Where(objFtr.filterParams, objFtr.values)
                            .Where(f => f.PROC_STS == JobStatus.Pending.ToString()
                                || f.PROC_STS == JobStatus.Progress.ToString()
                            ).Select(c => c.UID.ToString())
                            .ToListAsync(cancellationToken);
                        objFtr = null;
                    }
                }
            }

            return new SelectionListModel
            {
                data = listSelect
            };
        }

    }
}