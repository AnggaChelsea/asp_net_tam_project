﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class ExportTFDataCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportTFDataCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly ITextFileConnector _textfile;
        //public ExportTFDataCommandHandler(
        //    GLOBALDbContext context, ITextFileConnector textfile, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //    _textfile = textfile;
        //}
        public ExportTFDataCommandHandler(
            ITextFileConnector textfile, IConfiguration configuration) : base(configuration)
        {
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportTFDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    TMP_FORMINFO fInfo = await globalCtx.FormHeaders
                        .Where(f => f.FormCode == req.CommandModel.formCode)
                        .Select(c => new TMP_FORMINFO
                        {
                            Module = c.Module,
                            PeriodType = c.PeriodType,
                            PeriodCode = c.TFPeriodCode,
                            OjkFormCode = c.OJKFormCode,
                            FormName = c.FormName,
                            TFSpecialCase = c.TFSpecialCase,
                            FilePeriodType = c.TFPeriodCode
                        }).FirstOrDefaultAsync(cancellationToken);
                    string module = fInfo.Module;
                    string period = fInfo.PeriodType;
                    string periodCode = fInfo.PeriodCode;
                    string ojkFormCode = fInfo.OjkFormCode;
                    string formName = fInfo.FormName;
                    string tFSpecialCase = fInfo.TFSpecialCase;
                    string tfPeriodType = fInfo.FilePeriodType;

                    fInfo = null;

                    bool hasHeader = true;

                    IList<FormDetail> listColumns = await globalCtx.FormDetails
                        .Where(f => f.FormCode == req.CommandModel.formCode && f.TFShow == true)
                        .OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                    //Get File Path
                    SystemParameterHeader parDetail = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.TEXT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string filePath = parDetail.ParamValue;
                    parDetail = null;

                    parDetail = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.TEXT_FILE_TYPE.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string fileType = parDetail.ParamValue.ToUpper();

                    string delimiter = await globalCtx.SystemParameterDetails.Where(f => f.ParamCode == parDetail.ParamCode)
                        .Select(c => c.ParamMapping).FirstOrDefaultAsync(cancellationToken);

                    string metadataVersion = ojkFormCode;

                    string sandiBank = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.SANDI_BANK.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);

                    string strHasHeader = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.BI_TF_HEADER.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);
                    hasHeader = ARRA.Common.ConvertValue.ToBoolean(strHasHeader.ToLower());

                    string rulePeriodCode = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.TEXT_FILE_PERIOD_RULE.ToString())
                        .Select(c => c.ParamCode)
                        .FirstOrDefaultAsync(cancellationToken);

                    IList<string> idops = await globalCtx.BusinessParamDetails.Where(f => f.ParamCode == "AN047").OrderBy(o => o.ParamSeq).Select(c => c.ParamValue).ToListAsync(cancellationToken);

                    IList<string> periodRule = await globalCtx.SystemParameterDetails.Where(f => f.ParamCode == rulePeriodCode && f.ParamValue == PeriodType.WEEKLY.ToString())
                        .OrderBy(o => o.ParamSeq)
                        .Select(c => c.ParamMapping).ToListAsync(cancellationToken);

                    filePath = filePath + req.CommandModel.reportDate.ToString("yyyyMMdd") + @"\";
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    parDetail = null;
                    using (MODULEDbContext ctx = base.GetDbContext(module))
                    {
                        IList<string> branches = await ctx.JobQueueDetails.Where(f => f.HeaderId == req.CommandModel.jobId && f.ParamName == FieldCollection.BRANCH.ToString())
                            .Select(c => c.ParamValue).ToListAsync(cancellationToken);

                        if (string.IsNullOrEmpty(tFSpecialCase))
                        {
                            List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await ctx.GenerateTextFiles
                                .Where(f => f.ReportDate == req.CommandModel.reportDate
                                    && f.FormCode == req.CommandModel.formCode
                                    && branches.Contains(f.BranchCode))
                                .ToListAsync(cancellationToken);

                            if (listTF.Count > 0)
                            {
                                foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile item in listTF)
                                {
                                    item.Active = false;
                                }
                                ctx.UpdateRange(listTF);
                                await ctx.SaveChangesAsync(cancellationToken);
                            }
                            listTF = null;

                            //ID_OPRSNL
                            foreach (string branch in branches)
                            {
                                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                        await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.jobId
                                            && f.Branch == branch && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                                hdrLog.ModifiedBy = "ARRA-SYSTEM";
                                hdrLog.ModifiedDate = req.CurrentDateTime;
                                try
                                {
                                    foreach (string idop in idops)
                                    {
                                        IList<FilterModel> listFtr = new List<FilterModel>();
                                        listFtr.Add(new FilterModel
                                        {
                                            field = ReportingDateField.REPORT_DT.ToString(),
                                            datatype = ControlType.DATE.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = req.CommandModel.reportDate.ToString("yyyy-MM-dd")
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.RG_BRANCH.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = branch
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.ID_OPRSNL.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = idop
                                        });

                                        //get total record
                                        DynamicParameters p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);
                                        IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                                        long totalRecord = records.FirstOrDefault();
                                        records = null;

                                        //get data
                                        p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@SORT", "");
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);

                                        listFtr = null;
                                        string fileName = "";
                                        string originalFileName = "";
                                        if (totalRecord > 0 || idop == "1")
                                        {
                                            using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                                            {
                                                string tfDate = "";
                                                //for adhoc case textfile using current date
                                                if (PeriodType.ADHOC.ToString() == period.ToUpper())
                                                {
                                                    tfDate = req.CurrentDateTime.ToString("yyyy-MM-dd");
                                                }
                                                //else if(PeriodType.MONTHLY.ToString() == period.ToUpper() || PeriodType.QUARTERLY.ToString() == period.ToUpper() || PeriodType.YEARLY.ToString() == period.ToUpper() || PeriodType.SEMESTER.ToString() == period.ToUpper())
                                                //{ // get last date of month
                                                //    int reportDateYear = req.CommandModel.reportDate.Year;
                                                //    int reportDateMonth = req.CommandModel.reportDate.Month;
                                                //    int lastDay = DateTime.DaysInMonth(reportDateYear, reportDateMonth);
                                                //    tfDate = new DateTime(reportDateYear, reportDateMonth, lastDay).ToString("yyyy-MM-dd");
                                                //}
                                                else
                                                { //common case
                                                    tfDate = req.CommandModel.reportDate.ToString("yyyy-MM-dd");
                                                }

                                                fileName = sandiBank.Substring(0, 3) + branch + sandiBank.Substring(sandiBank.Length - 3, 3) + "_" + metadataVersion + "_" + tfDate + "_" + this.GetPeriodFile(req.CommandModel.formCode, period, periodCode, req.CommandModel.reportDate, periodRule) + "_" + idop;
                                                originalFileName = fileName;// + "_" + req.CurrentDateTime.Ticks.ToString();
                                                if (fileType == FileType.CSV.ToString())
                                                {
                                                    fileName += ".csv";
                                                    originalFileName += ".csv";
                                                    _textfile.WriteCSVRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.DELIMITED.ToString())
                                                {
                                                    fileName += ".txt";
                                                    originalFileName += ".txt";
                                                    _textfile.WriteDelimitedRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.FIXLENGTH.ToString())
                                                {
                                                    fileName += ".txt";
                                                    originalFileName += ".txt";
                                                    _textfile.WriteFixLengthRegulator(rd, listColumns, filePath + fileName);
                                                }
                                                rd.Close();
                                            }

                                            p = null;

                                            //save to download list.
                                            var generatelist = new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                            {
                                                BranchCode = branch,
                                                BranchName = branch,
                                                FormCode = req.CommandModel.formCode,
                                                OjkFormCode = ojkFormCode,
                                                FileName = fileName,
                                                OriginalFileName = originalFileName,
                                                FormName = formName,
                                                TotalRecord = totalRecord,
                                                PeriodType = period,
                                                ReportDate = req.CommandModel.reportDate,
                                                FilePeriodType = tfPeriodType,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                Active = true
                                            };

                                            ctx.GenerateTextFiles.Add(generatelist);
                                            generatelist = null;
                                        }
                                    }

                                    hdrLog.ProcessStatus = JobStatus.Success.ToString();
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                catch (Exception ex)
                                {
                                    hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                                    hdrLog.Remark = ex.Message;
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                hdrLog = null;
                            }
                        }
                        else if (tFSpecialCase == TFSpecialCase.PUABPUASDOC.ToString())
                        {
                            BusinessParamHeader puabpagisore = await globalCtx.BusinessParamHeaders.Where(f => f.ParamCode == ParamCodeCollection.GB211.ToString())
                                .FirstOrDefaultAsync(cancellationToken);
                            BusinessParamHeader puabpagisoreContent = await globalCtx.BusinessParamHeaders.Where(f => f.ParamCode == ParamCodeCollection.GB214.ToString())
                                .FirstOrDefaultAsync(cancellationToken);

                            List<string> periods = new List<string>();
                            if (req.CurrentDateTime <= Convert.ToDateTime(Convert.ToDateTime(req.CurrentDateTime).ToString("yyyy-MM-dd") + " " + puabpagisore.ParamValue))
                            {
                                periods.Add("D1"); //PUAB pagi
                            }
                            else if (req.CurrentDateTime > Convert.ToDateTime(Convert.ToDateTime(req.CurrentDateTime).ToString("yyyy-MM-dd") + " " + puabpagisore.ParamValue))
                            {
                                periods.Add("D2"); //PUAB sore
                            }

                            periods.Add("D3"); //PUAB VALS
                            periods.Add("D4"); //PUAB LN
                            periods.Add("D5"); //PUAS/DOC

                            List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await ctx.GenerateTextFiles
                                .Where(f => f.ReportDate == req.CommandModel.reportDate
                                    && f.FormCode == req.CommandModel.formCode
                                    && branches.Contains(f.BranchCode)
                                    && periods.Contains(f.FilePeriodType))
                                .ToListAsync(cancellationToken);

                            if (listTF.Count > 0)
                            {
                                foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile item in listTF)
                                {
                                    item.Active = false;
                                }
                                ctx.UpdateRange(listTF);
                                await ctx.SaveChangesAsync(cancellationToken);
                            }

                            listTF = null;

                            //ID_OPRSNL
                            foreach (string branch in branches)
                            {
                                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                        await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.jobId
                                            && f.Branch == branch && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                                hdrLog.ModifiedBy = "ARRA-SYSTEM";
                                hdrLog.ModifiedDate = req.CurrentDateTime;
                                try
                                {
                                    foreach (string periodfile in periods)
                                    {
                                        foreach (string idop in idops)
                                        {
                                            IList<FilterModel> listFtr = new List<FilterModel>();
                                            listFtr.Add(new FilterModel
                                            {
                                                field = ReportingDateField.REPORT_DT.ToString(),
                                                datatype = ControlType.DATE.ToString(),
                                                optr = FilterOperator.EQUALS.ToString(),
                                                value = req.CommandModel.reportDate.ToString("yyyy-MM-dd")
                                            });

                                            listFtr.Add(new FilterModel
                                            {
                                                field = FieldCollection.RG_BRANCH.ToString(),
                                                datatype = ControlType.TEXT.ToString(),
                                                optr = FilterOperator.EQUALS.ToString(),
                                                value = branch
                                            });

                                            listFtr.Add(new FilterModel
                                            {
                                                field = FieldCollection.ID_OPRSNL.ToString(),
                                                datatype = ControlType.TEXT.ToString(),
                                                optr = FilterOperator.EQUALS.ToString(),
                                                value = idop
                                            });

                                            if (periodfile == "D1") //perlu ada flag mana yang sudah di laporkan.
                                            {
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_TR_ANTAR_BANK.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "PUAB"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.PERAN_PELPOR_.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.NOTEQUALS.ToString(),
                                                    value = "LN"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_VAL.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "IDR"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = FieldCollection.JAM_TR.ToString(),
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.LESSTHAN.ToString(),
                                                    value = puabpagisoreContent.ParamValue
                                                });

                                            }
                                            else if (periodfile == "D2") //perlu ada flag mana yang sudah di laporkan.
                                            {
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_TR_ANTAR_BANK.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "PUAB"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.PERAN_PELPOR_.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.NOTEQUALS.ToString(),
                                                    value = "LN"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_VAL.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "IDR"
                                                });

                                                listFtr.Add(new FilterModel
                                                {
                                                    field = FieldCollection.JAM_TR.ToString(),
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.GREATER.ToString(),
                                                    value = puabpagisoreContent.ParamValue
                                                });
                                            }
                                            else if (periodfile == "D3") //valas
                                            {
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_TR_ANTAR_BANK.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "PUAB"
                                                });

                                                //confirm lagi.
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.PERAN_PELPOR_.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.NOTEQUALS.ToString(),
                                                    value = "LN"
                                                });

                                                //confirm lagi
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_VAL.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.NOTEQUALS.ToString(),
                                                    value = "IDR"
                                                });
                                            }
                                            else if (periodfile == "D4")
                                            {
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_TR_ANTAR_BANK.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "PUAB"
                                                });
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.PERAN_PELPOR_.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.EQUALS.ToString(),
                                                    value = "LN"
                                                });
                                            }
                                            else if (periodfile == "D5")
                                            {
                                                listFtr.Add(new FilterModel
                                                {
                                                    field = "ISNULL(" + FieldCollection.JNS_TR_ANTAR_BANK.ToString() + ",'')",
                                                    datatype = ControlType.TEXT.ToString(),
                                                    optr = FilterOperator.INCLUDE.ToString(),
                                                    value = "PUAS,DOC"
                                                });
                                            }

                                            //get total record
                                            DynamicParameters p = new DynamicParameters();
                                            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                            p.Add("@FTR", ResolveFilter(listFtr));
                                            p.Add("@USR_ID", req.UserIdentity.UserId);
                                            p.Add("@TF", true);
                                            IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                                            long totalRecord = records.FirstOrDefault();
                                            records = null;

                                            //get data
                                            p = new DynamicParameters();
                                            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                            p.Add("@FTR", ResolveFilter(listFtr));
                                            p.Add("@SORT", "");
                                            p.Add("@USR_ID", req.UserIdentity.UserId);
                                            p.Add("@TF", true);

                                            listFtr = null;
                                            string fileName = "";
                                            string originalFileName = "";
                                            if (totalRecord > 0 || idop == "1")
                                            {
                                                using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                                                {
                                                    fileName = sandiBank.Substring(0, 3) + branch + sandiBank.Substring(sandiBank.Length - 3, 3) + "_" + metadataVersion + "_" + req.CommandModel.reportDate.ToString("yyyy-MM-dd") + "_" + periodfile + "_" + idop;
                                                    originalFileName = fileName;// + "_" + req.CurrentDateTime.Ticks.ToString();
                                                    if (fileType == FileType.CSV.ToString())
                                                    {
                                                        fileName += ".csv";
                                                        originalFileName += ".csv";
                                                        _textfile.WriteCSVRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                    }
                                                    else if (fileType == FileType.DELIMITED.ToString())
                                                    {
                                                        fileName += ".txt";
                                                        originalFileName += ".txt";
                                                        _textfile.WriteDelimitedRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                    }
                                                    else if (fileType == FileType.FIXLENGTH.ToString())
                                                    {
                                                        fileName += ".txt";
                                                        originalFileName += ".txt";
                                                        _textfile.WriteFixLengthRegulator(rd, listColumns, filePath + fileName);
                                                    }
                                                    rd.Close();
                                                }

                                                p = null;

                                                //save to download list.
                                                var generatelist = new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                                {
                                                    BranchCode = branch,
                                                    BranchName = branch,
                                                    FormCode = req.CommandModel.formCode,
                                                    OjkFormCode = ojkFormCode,
                                                    FileName = fileName,
                                                    OriginalFileName = originalFileName,
                                                    FormName = formName,
                                                    TotalRecord = totalRecord,
                                                    PeriodType = period,
                                                    ReportDate = req.CommandModel.reportDate,
                                                    FilePeriodType = periodfile,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host,
                                                    Active = true
                                                };

                                                ctx.GenerateTextFiles.Add(generatelist);
                                                generatelist = null;
                                            }
                                        }
                                    }

                                    hdrLog.ProcessStatus = JobStatus.Success.ToString();
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                catch (Exception ex)
                                {
                                    hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                                    hdrLog.Remark = ex.Message;
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                hdrLog = null;
                            }

                            puabpagisore = null;
                            puabpagisoreContent = null;
                        }
                        else if (tFSpecialCase == TFSpecialCase.ISREPORTED.ToString())
                        {
                            List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await ctx.GenerateTextFiles
                                .Where(f => f.ReportDate == req.CommandModel.reportDate
                                    && f.FormCode == req.CommandModel.formCode
                                    && branches.Contains(f.BranchCode))
                                .ToListAsync(cancellationToken);

                            if (listTF.Count > 0)
                            {
                                foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile item in listTF)
                                {
                                    item.Active = false;
                                }
                                ctx.UpdateRange(listTF);
                                await ctx.SaveChangesAsync(cancellationToken);
                            }
                            listTF = null;

                            //ID_OPRSNL
                            foreach (string branch in branches)
                            {
                                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                        await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.jobId
                                            && f.Branch == branch && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                                hdrLog.ModifiedBy = "ARRA-SYSTEM";
                                hdrLog.ModifiedDate = req.CurrentDateTime;
                                try
                                {
                                    foreach (string idop in idops)
                                    {
                                        IList<FilterModel> listFtr = new List<FilterModel>();
                                        listFtr.Add(new FilterModel
                                        {
                                            field = ReportingDateField.REPORT_DT.ToString(),
                                            datatype = ControlType.DATE.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = req.CommandModel.reportDate.ToString("yyyy-MM-dd")
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.RG_BRANCH.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = branch
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.ID_OPRSNL.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = idop
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = "ISNULL(" + FieldCollection.IS_REPORTED.ToString() + ",'')",
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.NOTEQUALS.ToString(),
                                            value = "YES"
                                        }); //yang belum pernah generate saja.

                                        //get total record
                                        DynamicParameters p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);
                                        IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                                        long totalRecord = records.FirstOrDefault();
                                        records = null;

                                        //get data
                                        p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@SORT", "");
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);

                                        listFtr = null;
                                        string fileName = "";
                                        string originalFileName = "";
                                        if (totalRecord > 0 || idop == "1")
                                        {
                                            using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                                            {
                                                string tfDate = "";
                                                //for adhoc case textfile using current date
                                                if (PeriodType.ADHOC.ToString() == period.ToUpper())
                                                {
                                                    tfDate = req.CurrentDateTime.ToString("yyyy-MM-dd");
                                                }
                                                //else if(PeriodType.MONTHLY.ToString() == period.ToUpper() || PeriodType.QUARTERLY.ToString() == period.ToUpper() || PeriodType.YEARLY.ToString() == period.ToUpper() || PeriodType.SEMESTER.ToString() == period.ToUpper())
                                                //{ // get last date of month
                                                //    int reportDateYear = req.CommandModel.reportDate.Year;
                                                //    int reportDateMonth = req.CommandModel.reportDate.Month;
                                                //    int lastDay = DateTime.DaysInMonth(reportDateYear, reportDateMonth);
                                                //    tfDate = new DateTime(reportDateYear, reportDateMonth, lastDay).ToString("yyyy-MM-dd");
                                                //}
                                                else
                                                { //common case
                                                    tfDate = req.CommandModel.reportDate.ToString("yyyy-MM-dd");
                                                }

                                                fileName = sandiBank.Substring(0, 3) + branch + sandiBank.Substring(sandiBank.Length - 3, 3) + "_" + metadataVersion + "_" + tfDate + "_" + this.GetPeriodFile(req.CommandModel.formCode, period, periodCode, req.CommandModel.reportDate, periodRule) + "_" + idop;
                                                originalFileName = fileName;// + "_" + req.CurrentDateTime.Ticks.ToString();
                                                if (fileType == FileType.CSV.ToString())
                                                {
                                                    fileName += ".csv";
                                                    originalFileName += ".csv";
                                                    _textfile.WriteCSVRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.DELIMITED.ToString())
                                                {
                                                    fileName += ".txt";
                                                    originalFileName += ".txt";
                                                    _textfile.WriteDelimitedRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.FIXLENGTH.ToString())
                                                {
                                                    fileName += ".txt";
                                                    originalFileName += ".txt";
                                                    _textfile.WriteFixLengthRegulator(rd, listColumns, filePath + fileName);
                                                }
                                                rd.Close();
                                            }

                                            p = null;

                                            //save to download list.
                                            var generatelist = new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                            {
                                                BranchCode = branch,
                                                BranchName = branch,
                                                FormCode = req.CommandModel.formCode,
                                                OjkFormCode = ojkFormCode,
                                                FileName = fileName,
                                                OriginalFileName = originalFileName,
                                                FormName = formName,
                                                TotalRecord = totalRecord,
                                                PeriodType = period,
                                                ReportDate = req.CommandModel.reportDate,
                                                FilePeriodType = tfPeriodType,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                Active = true
                                            };

                                            ctx.GenerateTextFiles.Add(generatelist);
                                            generatelist = null;
                                        }
                                    }

                                    hdrLog.ProcessStatus = JobStatus.Success.ToString();
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);

                                    DynamicParameters p1 = new DynamicParameters();
                                    p1.Add("@REPORT_DT", req.CommandModel.reportDate);
                                    p1.Add("@FORM_CD", req.CommandModel.formCode);
                                    await ctx.Database.GetDbConnection().ExecuteAsync("DBO.SP_UPDATE_REPORTED", p1, null, null, CommandType.StoredProcedure);
                                    p1 = null;

                                }
                                catch (Exception ex)
                                {
                                    hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                                    hdrLog.Remark = ex.Message;
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                hdrLog = null;
                            }
                        }
                        else if (tFSpecialCase == TFSpecialCase.TIMEFLAG.ToString())
                        {
                            List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await ctx.GenerateTextFiles
                                .Where(f => f.ReportDate == req.CommandModel.reportDate
                                    && f.FormCode == req.CommandModel.formCode
                                    && branches.Contains(f.BranchCode))
                                .ToListAsync(cancellationToken);

                            if (listTF.Count > 0)
                            {
                                foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile item in listTF)
                                {
                                    item.Active = false;
                                }
                                ctx.UpdateRange(listTF);
                                await ctx.SaveChangesAsync(cancellationToken);
                            }
                            listTF = null;

                            //ID_OPRSNL
                            foreach (string branch in branches)
                            {
                                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                        await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.jobId
                                            && f.Branch == branch && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                                hdrLog.ModifiedBy = "ARRA-SYSTEM";
                                hdrLog.ModifiedDate = req.CurrentDateTime;
                                try
                                {
                                    foreach (string idop in idops)
                                    {
                                        IList<FilterModel> listFtr = new List<FilterModel>();
                                        listFtr.Add(new FilterModel
                                        {
                                            field = ReportingDateField.REPORT_DT.ToString(),
                                            datatype = ControlType.DATE.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = req.CommandModel.reportDate.ToString("yyyy-MM-dd")
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.RG_BRANCH.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = branch
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = FieldCollection.ID_OPRSNL.ToString(),
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = idop
                                        });

                                        listFtr.Add(new FilterModel
                                        {
                                            field = "ISNULL(" + FieldCollection.TIME_FLAG.ToString() + ",'')",
                                            datatype = ControlType.TEXT.ToString(),
                                            optr = FilterOperator.EQUALS.ToString(),
                                            value = req.CommandModel.timeFlag
                                        }); // generate berdasarkan time flag

                                        //get total record
                                        DynamicParameters p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);
                                        IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                                        long totalRecord = records.FirstOrDefault();
                                        records = null;

                                        //get data
                                        p = new DynamicParameters();
                                        p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                                        p.Add("@FTR", ResolveFilter(listFtr));
                                        p.Add("@SORT", "");
                                        p.Add("@USR_ID", req.UserIdentity.UserId);
                                        p.Add("@TF", true);

                                        listFtr = null;
                                        string fileName = "";
                                        string originalFileName = "";
                                        if (totalRecord > 0 || idop == "1")
                                        {
                                            using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                                            {
                                                string tfDate = "";
                                                //for adhoc case textfile using current date
                                                if (PeriodType.ADHOC.ToString() == period.ToUpper())
                                                {
                                                    tfDate = req.CurrentDateTime.ToString("yyyy-MM-dd");
                                                }
                                                //else if(PeriodType.MONTHLY.ToString() == period.ToUpper() || PeriodType.QUARTERLY.ToString() == period.ToUpper() || PeriodType.YEARLY.ToString() == period.ToUpper() || PeriodType.SEMESTER.ToString() == period.ToUpper())
                                                //{ // get last date of month
                                                //    int reportDateYear = req.CommandModel.reportDate.Year;
                                                //    int reportDateMonth = req.CommandModel.reportDate.Month;
                                                //    int lastDay = DateTime.DaysInMonth(reportDateYear, reportDateMonth);
                                                //    tfDate = new DateTime(reportDateYear, reportDateMonth, lastDay).ToString("yyyy-MM-dd");
                                                //}
                                                else
                                                { //common case
                                                    tfDate = req.CommandModel.reportDate.ToString("yyyy-MM-dd");
                                                }

                                                fileName = sandiBank.Substring(0, 3) + branch + sandiBank.Substring(sandiBank.Length - 3, 3) + "_" + metadataVersion + "_" + tfDate + "_" + this.GetPeriodFile(req.CommandModel.formCode, period, periodCode, req.CommandModel.reportDate, periodRule) + "_" + idop;
                                                originalFileName = fileName;// + "_" + req.CurrentDateTime.Ticks.ToString();
                                                if (fileType == FileType.CSV.ToString())
                                                {
                                                    fileName += "_" + req.CommandModel.timeFlag + ".csv";
                                                    originalFileName += "_" + req.CommandModel.timeFlag + ".csv";
                                                    _textfile.WriteCSVRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.DELIMITED.ToString())
                                                {
                                                    fileName += "_" + req.CommandModel.timeFlag + ".txt";
                                                    originalFileName += "_" + req.CommandModel.timeFlag + ".txt";
                                                    _textfile.WriteDelimitedRegulator(rd, listColumns, filePath + fileName, delimiter, hasHeader);
                                                }
                                                else if (fileType == FileType.FIXLENGTH.ToString())
                                                {
                                                    fileName += "_" + req.CommandModel.timeFlag + ".txt";
                                                    originalFileName += "_" + req.CommandModel.timeFlag + ".txt";
                                                    _textfile.WriteFixLengthRegulator(rd, listColumns, filePath + fileName);
                                                }
                                                rd.Close();
                                            }

                                            p = null;

                                            //save to download list.
                                            var generatelist = new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                            {
                                                BranchCode = branch,
                                                BranchName = branch,
                                                FormCode = req.CommandModel.formCode,
                                                OjkFormCode = ojkFormCode,
                                                FileName = fileName,
                                                OriginalFileName = originalFileName,
                                                FormName = formName,
                                                TotalRecord = totalRecord,
                                                PeriodType = period,
                                                ReportDate = req.CommandModel.reportDate,
                                                FilePeriodType = tfPeriodType,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                Active = true
                                            };

                                            ctx.GenerateTextFiles.Add(generatelist);
                                            generatelist = null;
                                        }
                                    }

                                    hdrLog.ProcessStatus = JobStatus.Success.ToString();
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);

                                }
                                catch (Exception ex)
                                {
                                    hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                                    hdrLog.Remark = ex.Message;
                                    ctx.ProcessLogHeaders.Update(hdrLog);

                                    await ctx.SaveChangesAsync(cancellationToken);
                                }
                                hdrLog = null;
                            }
                        }
                        branches = null;
                    }

                    periodRule = null;
                    listColumns = null;
                    //downloadId = exportlist.UniqueId;

                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
        private string GetPeriodFile(string formCode, string periodType, string periodCode, DateTime reportDate, IList<string> listRule)
        {
            string result = "";

            result = periodCode;

            //tidak pakai aturan w1/w2/w3/w4 lagi.
            //if (periodCode.ToUpper() == PeriodType.WEEKLY.ToString().Substring(0, 1))
            //{
            //    int day = reportDate.Day;
            //    foreach (string rule in listRule)
            //    {
            //        string[] strs = rule.Split('=');
            //        if (day <= Convert.ToInt32(strs[1]))
            //        {
            //            result = strs[0];
            //        }
            //    }
            //}
            //else
            //{
            //    result = periodCode;
            //}

            return result;
        }
    }

    public class TMP_FORMINFO
    {
        public string Module { get; set; }
        public string PeriodType { get; set; }
        public string PeriodCode { get; set; }
        public string OjkFormCode { get; set; }
        public string FormName { get; set; }
        public string TFSpecialCase { get; set; }
        public string FilePeriodType { get; set; }

    }
}