﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class GenerateTFWoBRCommand
    {
        public string formCode { get; set; }
        public string form { get; set; }
        public string branch { get; set; }
        public string idOps { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}