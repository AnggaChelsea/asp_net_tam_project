﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.Common;
using ARRA.Common.Enumerations;
using System.Security.Principal;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class GenerateTFCommandHandler : AppBase, IRequestHandler<CommandsModel<GenerateTFCommand, FormBranchFailListModel>, FormBranchFailListModel>
    {
        private readonly GLOBALDbContext _context;
        public GenerateTFCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FormBranchFailListModel> Handle(CommandsModel<GenerateTFCommand, FormBranchFailListModel> req, CancellationToken cancellationToken)
        {
            FormBranchFailListModel vm = new FormBranchFailListModel();
            vm.errors = new List<FormBranchFailModel>();

            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString());

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.form) == false &&
                        string.IsNullOrEmpty(req.CommandModel.branch) == false
                        )
                    {
                        string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode).Select(c => c.Module).FirstOrDefaultAsync(cancellationToken);
                        ARRA.GLOBAL.Domain.Entities.Module mInfo = await _context.Modules.Where(f => f.ModuleId == module).FirstOrDefaultAsync(cancellationToken);

                        // switch check validation, add 20200723 rizwan
                        bool isCheckVal = false;
                        try { isCheckVal = Convert.ToBoolean(await _context.SystemParameterHeaders.Where(f => f.ParamCode == "GB213").Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken)); }
                        catch { isCheckVal = false; }
                        string validationList = await _context.SystemParameterDetails.Where(f => f.ParamCode == "GB213" && f.ParamMapping == module).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);
                        IList<TMP_FORM> lsitReconForm = await _context.SystemParameterDetails.Where(f => f.ParamCode == "GB215").Select(c => new TMP_FORM { FormCode = c.ParamValue }).ToListAsync(cancellationToken);

                        using (MODULEDbContext ctx = base.GetDbContext(module))
                        {
                            string[] branches = req.CommandModel.branch.Split(',');
                            string[] forms = req.CommandModel.form.Split(',');
                            string timeFlag = req.CommandModel.timeFlag;
                            List<string> listSelectedBranch = new List<string>();
                            List<string> listSelectedForm = new List<string>();
                            if (branches.Length > 0 && forms.Length > 0)
                            {
                                IList<TMP_FORM> listForm =
                                               await _context.FormHeaders.Where(f => forms.Contains(f.FormCode))
                                                .Select(c => new TMP_FORM
                                                {
                                                    FormCode = c.FormCode,
                                                    PeriodType = c.PeriodType,
                                                    OJKCode = c.OJKFormCode,
                                                    FormName = c.FormName
                                                }).ToListAsync(cancellationToken);

                                //IList<TMP_BRANCHCOUNT> listBranchCheck =
                                //    await _context.Branches.Where(f => f.Unit == mInfo.BusinessType)
                                //        .GroupBy(c => c.RegulatorBranch)
                                //        .Select(c => new TMP_BRANCHCOUNT
                                //        {
                                //            Branch = c.Key,
                                //            Count = c.Count()
                                //        }).ToListAsync(cancellationToken);

                                IList<TMP_FORMBRANCH> listLock =
                                    await (from a in ctx.LockDatas
                                               //join b in ctx.SN_Branches
                                               // on a.Branch equals b.RegulatorBranch
                                               //on a.Branch equals b.BranchCode
                                           where a.ReportDate == Convert.ToDateTime(ftr.value)
                                            && forms.Contains(a.FormCode)
                                           group a by new
                                           {
                                               a.FormCode,
                                               a.Branch
                                               ///b.RegulatorBranch
                                           } into gp
                                           select new TMP_FORMBRANCH
                                           {
                                               FormCode = gp.Key.FormCode,
                                               //Branch = gp.Key.RegulatorBranch,
                                               Branch = gp.Key.Branch//,
                                               //Count = gp.Count()
                                           }).ToListAsync(cancellationToken);

                                //foreach (TMP_FORMBRANCH item in listLock)
                                //{
                                //    if (listBranchCheck.Where(f => f.Branch == item.Branch
                                //         && f.Count == item.Count).Count() > 0)
                                //    {
                                //        item.IsValid = true;
                                //    }
                                //    else
                                //    {
                                //        item.IsValid = false;
                                //    }
                                //}
                                //listBranchCheck = null;

                                IList<TMP_FORMBRANCH> listExists =
                                    await ctx.ProcessLogHeaders.Where(f => f.ProcessType == QueueJobType.GENERATE_TF.ToString()
                                        && f.DataDate == Convert.ToDateTime(ftr.value)
                                        && (f.ProcessStatus == JobStatus.Progress.ToString() || f.ProcessStatus == JobStatus.Pending.ToString())
                                        )
                                    .Select(c => new TMP_FORMBRANCH
                                    {
                                        FormCode = c.ProcessNo,
                                        Branch = c.Branch
                                    }).ToListAsync(cancellationToken);

                                //IList <TMP_FORMBRANCH> listExists =
                                //                await (from f in ctx.JobQueueDetails
                                //                       join b in ctx.JobQueueDetails
                                //                        on f.HeaderId equals b.HeaderId
                                //                       join c in ctx.JobQueueDetails
                                //                        on f.HeaderId equals c.HeaderId
                                //                       join h in ctx.JobQueueHeaders
                                //                        on f.HeaderId equals h.UniqueId
                                //                       where f.ParamName == "FORM_CODE"
                                //                        && h.JobType == QueueJobType.GENERATE_TF.ToString()
                                //                        && (h.Status == JobStatus.Progress.ToString() || h.Status == JobStatus.Pending.ToString())
                                //                        && b.ParamName == "BRANCH"
                                //                        && c.ParamName == "REPORT_DATE"
                                //                        && c.ParamValue == Convert.ToDateTime(ftr.value).ToString("yyyy-MM-dd")
                                //                       select new TMP_FORMBRANCH
                                //                       {
                                //                           FormCode = f.ParamValue,
                                //                           Branch = b.ParamValue
                                //                       }).ToListAsync(cancellationToken);

                                IList<TMP_FORMBRANCH> listSuccessVal = null;
                                IList<TMP_RUNNINGRECON> listRunningRecon = null;
                                IList<TMP_FORMBRANCH> listErrorReconScenario = null;
                                IList<TMP_FORMBRANCH> listErrorReconInterForm = null;
                                IList<TMP_FORMBRANCH> listErrorRecon = null;

                                if (isCheckVal)
                                {
                                    // add 20200723 rizwan
                                    // start check success validation
                                    listSuccessVal =
                                        await ctx.ProcessLogHeaders.Where(f => f.ProcessType == QueueJobType.VALIDATION.ToString()
                                            && f.DataDate == Convert.ToDateTime(ftr.value)
                                            && forms.Contains(f.ProcessNo)
                                            && f.ProcessStatus == JobStatus.SuccessValidation.ToString()
                                            )
                                        .Select(c => new TMP_FORMBRANCH
                                        {
                                            FormCode = c.ProcessNo,
                                            Branch = c.Branch
                                        }).ToListAsync(cancellationToken);
                                    // end check success validation

                                    if (validationList != null && validationList.Contains("RECONCILIATION"))
                                    {
                                        // add 20200723 rizwan
                                        // start check running recon
                                        listRunningRecon =
                                            await (from a in ctx.ProcessLogHeaders
                                                   join b in listForm
                                                    on a.PeriodType equals b.PeriodType
                                                   where a.DataDate == Convert.ToDateTime(ftr.value)
                                                    && a.ProcessType == QueueJobType.RECONCILIATION.ToString()
                                                    && (a.ProcessStatus == JobStatus.SuccessValidation.ToString()
                                                    || a.ProcessStatus == JobStatus.FailedValidation.ToString())
                                                   select new TMP_RUNNINGRECON
                                                   {
                                                       FormCode = b.FormCode
                                                   }).ToListAsync(cancellationToken);
                                        // end check running recon

                                        // add 20200723 rizwan
                                        // start check error reconciliation
                                        listErrorReconScenario =
                                            await ctx.ValidationScenarioResults.Where(f => f.ReportingDate == Convert.ToDateTime(ftr.value)
                                                && forms.Contains(f.FormCode)
                                                )
                                            .Select(c => new TMP_FORMBRANCH
                                            {
                                                FormCode = c.FormCode,
                                                Branch = c.Branch
                                            }).ToListAsync(cancellationToken);

                                        listErrorReconInterForm =
                                            await ctx.InterFormValidationResults.Where(f => f.DataDate == Convert.ToDateTime(ftr.value)
                                                && forms.Contains(f.SrcFormCode)
                                                )
                                            .Select(c => new TMP_FORMBRANCH
                                            {
                                                FormCode = c.SrcFormCode,
                                                Branch = c.Branch
                                            }).ToListAsync(cancellationToken);

                                        listErrorRecon = listErrorReconScenario.Union(listErrorReconInterForm).ToList();
                                        // end check error reconciliation
                                    }
                                }

                                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);

                                for (int i = 0; i < forms.Length; i++)
                                {
                                    string strForm = forms[i].Trim();
                                    if (strForm != "")
                                    {
                                        for (int j = 0; j < branches.Length; j++)
                                        {
                                            string strBranch = branches[j].Trim();
                                            if (strBranch != "")
                                            {
                                                //if (listLock.Where(f => f.FormCode == strForm && f.Branch == strBranch && f.IsValid==true).Count() == 0)
                                                if (listLock.Where(f => f.FormCode == strForm && f.Branch == strBranch).Count() == 0)
                                                {
                                                    vm.errors.Add(new FormBranchFailModel
                                                    {
                                                        form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                        branch = strBranch,
                                                        message = ErrorMessages.LockDataBeforeGenerateTF
                                                    });
                                                }
                                                else if (listExists.Where(f => f.FormCode == strForm && f.Branch == strBranch).Count() > 0)
                                                {
                                                    vm.errors.Add(new FormBranchFailModel
                                                    {
                                                        form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                        branch = strBranch,
                                                        message = ErrorMessages.GenerateTFExists
                                                    });
                                                }
                                                else if (isCheckVal
                                                    && listSuccessVal.Where(f => f.FormCode == strForm && f.Branch == strBranch).Count() == 0
                                                    ) //add 20200723 rizwan
                                                {
                                                    vm.errors.Add(new FormBranchFailModel
                                                    {
                                                        form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                        branch = strBranch,
                                                        message = ErrorMessages.CompleteValidationBeforeGenerateTF
                                                    });
                                                }
                                                else if (isCheckVal
                                                    && validationList != null && validationList.Contains("RECONCILIATION")
                                                    && lsitReconForm.Where(f => f.FormCode == strForm).Count() > 0
                                                    && (
                                                        listRunningRecon.Where(f => f.FormCode == strForm).Count() == 0
                                                        ||
                                                        (
                                                            listRunningRecon.Where(f => f.FormCode == strForm).Count() > 0
                                                            &&
                                                            listErrorRecon.Where(f => f.FormCode == strForm && f.Branch == strBranch).Count() > 0)
                                                        )
                                                    ) //add 20200723 rizwan
                                                {
                                                    vm.errors.Add(new FormBranchFailModel
                                                    {
                                                        form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                        branch = strBranch,
                                                        message = ErrorMessages.CompleteReconBeforeGenerateTF
                                                    });
                                                }
                                                else
                                                {
                                                    listSelectedBranch.Add(strBranch);
                                                    listSelectedForm.Add(strForm);
                                                }
                                            }
                                        }
                                    }
                                }
                                listExists = null;

                                if (listSelectedForm.Count > 0 && listSelectedBranch.Count > 0)
                                {
                                    using (var trans = ctx.Database.BeginTransaction())
                                    {
                                        try
                                        {
                                            IList<long> tmpQueueId = new List<long>();
                                            foreach (string strForm in listSelectedForm.Distinct())
                                            {
                                                TMP_FORM fInfo = listForm.Where(f => f.FormCode == strForm).FirstOrDefault();

                                                ARRA.MODULE.Domain.Entities.JobQueueHeader hdr = new ARRA.MODULE.Domain.Entities.JobQueueHeader
                                                {
                                                    JobType = QueueJobType.GENERATE_TF.ToString(),
                                                    PeriodType = fInfo.PeriodType,
                                                    Status = JobStatus.Temp.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };

                                                ctx.JobQueueHeaders.Add(hdr);
                                                await ctx.SaveChangesAsync(cancellationToken);

                                                tmpQueueId.Add(hdr.UniqueId);

                                                ARRA.MODULE.Domain.Entities.JobQueueDetail dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "FORM_CODE",
                                                    ParamValue = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "REPORT_DATE",
                                                    ParamValue = ftr.value,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "TIME_FLAG",
                                                    ParamValue = timeFlag,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                AuditTrailHeader hdrAudit = new AuditTrailHeader
                                                {
                                                    MenuCode = menu.MenuCode,
                                                    MenuName = menu.MenuName,
                                                    Module = module,
                                                    WithApproval = false,
                                                    ActivityAction = AuditType.ADD.ToString(),
                                                    UserId = req.UserIdentity.UserId,
                                                    ActivityDate = req.CurrentDateTime,
                                                    ActivityHost = req.UserIdentity.Host,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailHeaders.Add(hdrAudit);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                AuditTrailDetail dtlAudit = new AuditTrailDetail
                                                {
                                                    FormColName = "FORM",
                                                    FieldName = "FORM",
                                                    FormSeq = 1,
                                                    HeaderId = hdrAudit.UniqueId,
                                                    ValueAfter = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailDetails.Add(dtlAudit);

                                                foreach (string strBranch in listSelectedBranch.Distinct())
                                                {
                                                    dtl = new MODULE.Domain.Entities.JobQueueDetail()
                                                    {
                                                        HeaderId = hdr.UniqueId,
                                                        ParamName = "BRANCH",
                                                        ParamValue = strBranch,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(dtl);

                                                    ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLogs = new MODULE.Domain.Entities.ProcessLogHeader
                                                    {
                                                        DataDate = Convert.ToDateTime(ftr.value),
                                                        PeriodType = fInfo.PeriodType,
                                                        Branch = strBranch,
                                                        ProcessNo = strForm,
                                                        ProcessName = fInfo.FormName,
                                                        ProcessStatus = JobStatus.Progress.ToString(),
                                                        ProcessType = QueueJobType.GENERATE_TF.ToString(),
                                                        QueueId = hdr.UniqueId,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.ProcessLogHeaders.Add(hdrLogs);

                                                    dtlAudit = new AuditTrailDetail
                                                    {
                                                        FormColName = "BRANCH",
                                                        FieldName = "BRANCH",
                                                        FormSeq = 2,
                                                        HeaderId = hdrAudit.UniqueId,
                                                        ValueAfter = strBranch,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    _context.AuditTrailDetails.Add(dtlAudit);

                                                    dtl = null;
                                                }
                                                fInfo = null;
                                            }
                                            listForm = null;
                                            listExists = null;

                                            IList<ARRA.MODULE.Domain.Entities.GenerateTextFile> listGTF =
                                                await ctx.GenerateTextFiles.Where(f => listSelectedForm.Distinct().Contains(f.FormCode)
                                                    && listSelectedBranch.Distinct().Contains(f.BranchCode)
                                                    && f.ReportDate == Convert.ToDateTime(ftr.value)
                                                    && f.Active == true).ToListAsync(cancellationToken);
                                            foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile tf in listGTF)
                                            {
                                                tf.Active = false;
                                            }
                                            ctx.GenerateTextFiles.UpdateRange(listGTF);
                                            listGTF = null;

                                            IList<ARRA.MODULE.Domain.Entities.JobQueueHeader> listUpd =
                                                await ctx.JobQueueHeaders.Where(f => tmpQueueId.Contains(f.UniqueId)).ToListAsync(cancellationToken);
                                            foreach (ARRA.MODULE.Domain.Entities.JobQueueHeader hdrupd in listUpd)
                                            {
                                                hdrupd.Status = JobStatus.Pending.ToString();
                                            }
                                            ctx.JobQueueHeaders.UpdateRange(listUpd);
                                            listUpd = null;

                                            await ctx.SaveChangesAsync(cancellationToken);

                                            await _context.SaveChangesAsync(cancellationToken);

                                            trans.Commit();

                                            tmpQueueId = null;
                                        }
                                        catch (Exception ex)
                                        {
                                            trans.Rollback();

                                            throw ex;
                                        }
                                    }

                                }
                                menu = null;
                            }
                            branches = null;
                            forms = null;

                            listSelectedForm = null;
                            listSelectedBranch = null;
                        }//ctx

                        mInfo = null;
                    }
                }
                else
                {
                    err = ErrorMessages.AddFilterDate;
                }
            }
            else
            {
                err = ErrorMessages.AddFilterDate;
            }
            ftr = null;

            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return vm;
        }
    }

    public class TMP_FORM
    {
        public string FormCode { get; set; }
        public string OJKCode { get; set; }
        public string PeriodType { get; set; }
        public string FormName { get; set; }
    }

    public class TMP_FORMBRANCH
    {
        public string FormCode { get; set; }
        public string Branch { get; set; }
        public int Count { get; set; }
        public bool IsValid { get; set; }
    }
    public class TMP_BRANCHCOUNT
    {
        public string Branch { get; set; }
        public int Count { get; set; }

    }

    public class TMP_RUNNINGRECON
    {
        public string FormCode { get; set; }
    }
}