﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class ExportTFDataSLIKCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportTFDataSLIKCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly ITextFileConnector _textfile;
        //public ExportTFDataSLIKCommandHandler(
        //    GLOBALDbContext context, ITextFileConnector textfile, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //    _textfile = textfile;
        //}
        public ExportTFDataSLIKCommandHandler(
            ITextFileConnector textfile, IConfiguration configuration) : base(configuration)
        {
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportTFDataSLIKCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                try
                {
                    TMP_FORMINFO_SLIK fInfo = await globalCtx.FormHeaders
                        .Where(f => f.FormCode == req.CommandModel.formCode)
                        .Select(c => new TMP_FORMINFO_SLIK
                        {
                            Module = c.Module,
                            PeriodType = c.PeriodType,
                            PeriodCode = c.TFPeriodCode,
                            OjkFormCode = c.OJKFormCode,
                            FormName = c.FormName
                        }).FirstOrDefaultAsync(cancellationToken);

                    string module = fInfo.Module;
                    string period = fInfo.PeriodType;
                    string ojkFormCode = fInfo.OjkFormCode;
                    string formName = fInfo.FormName;
                    fInfo = null;

                    IList<FormDetail> listColumns = await globalCtx.FormDetails
                        .Where(f => f.FormCode == req.CommandModel.formCode && f.TFShow == true)
                        .OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                    //Get File Path
                    SystemParameterHeader parDetail = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.TEXT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string filePath = parDetail.ParamValue;
                    parDetail = null;

                    string sandiBank = await globalCtx.SystemParameterHeaders.Where(f => f.Module == module && f.ParamName == SystemParameterType.SANDI_BANK.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);
                    string kodeJenisPelapor = await globalCtx.SystemParameterHeaders.Where(f => f.ParamCode == "GB046")
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);
                    int maxRow = Convert.ToInt32(await globalCtx.SystemParameterHeaders.Where(f => f.ParamCode == "GB045")
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken));

                    filePath = filePath + req.CommandModel.reportDate.ToString("yyyyMMdd") + @"\";
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    parDetail = null;
                    using (MODULEDbContext ctx = base.GetDbContext(module))
                    {
                        List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await (from a in ctx.GenerateTextFiles
                                         select new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                         {
                                             UniqueId = a.UniqueId,
                                             ReportDate = a.ReportDate,
                                             PeriodType = a.PeriodType,
                                             FormCode = a.FormCode,
                                             FormName = a.FormName,
                                             BranchCode = a.BranchCode,
                                             BranchName = a.BranchName,
                                             FileName = a.FileName,
                                             OriginalFileName = a.OriginalFileName,
                                             TotalRecord = a.TotalRecord,
                                             IdOperational = a.IdOperational,
                                             Active = a.Active,
                                             CreatedBy = a.CreatedBy,
                                             CreatedDate = a.CreatedDate,
                                             CreatedHost = a.CreatedHost,
                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedDate = a.ModifiedDate,
                                             ModifiedHost = a.ModifiedHost,
                                             OjkFormCode = a.OjkFormCode == null ? string.Empty : a.OjkFormCode,
                                             FilePeriodType = a.FilePeriodType
                                         }).Where(f => f.ReportDate == req.CommandModel.reportDate
                                                && f.FormCode == req.CommandModel.formCode)
                                         .ToListAsync(cancellationToken);

                        //List<ARRA.MODULE.Domain.Entities.GenerateTextFile> listTF = await ctx.GenerateTextFiles
                        //    .Where(f => f.ReportDate == req.CommandModel.reportDate
                        //        && f.FormCode == req.CommandModel.formCode)
                        //    .ToListAsync(cancellationToken);

                        if (listTF.Count > 0)
                        {
                            foreach (ARRA.MODULE.Domain.Entities.GenerateTextFile item in listTF)
                            {
                                item.Active = false;
                            }
                            ctx.UpdateRange(listTF);
                            await ctx.SaveChangesAsync(cancellationToken);
                        }
                        listTF = null;

                        ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLog =
                                    await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.jobId
                                        && f.ProcessNo == req.CommandModel.formCode).FirstOrDefaultAsync(cancellationToken);
                        hdrLog.ModifiedBy = "ARRA-SYSTEM";
                        hdrLog.ModifiedDate = req.CurrentDateTime;

                        try
                        {
                            IList<FilterModel> listFtr = new List<FilterModel>();
                            listFtr.Add(new FilterModel
                            {
                                field = ReportingDateField.REPORT_DT.ToString(),
                                datatype = ControlType.DATE.ToString(),
                                optr = FilterOperator.EQUALS.ToString(),
                                value = req.CommandModel.reportDate.ToString("yyyy-MM-dd")
                            });
                            
                            //get total record
                            DynamicParameters p = new DynamicParameters();
                            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                            p.Add("@FTR", ResolveFilter(listFtr));
                            p.Add("@USR_ID", req.UserIdentity.UserId);
                            p.Add("@TF", true);
                            IEnumerable<Int64> records = await globalCtx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT_TFSLIK", p, null, null, CommandType.StoredProcedure);
                            long totalRecord = records.FirstOrDefault();
                            records = null;

                            // get header
                            p = new DynamicParameters();
                            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                            p.Add("@REPORT_DT", req.CommandModel.reportDate.ToString("yyyy-MM-dd"));
                            IEnumerable<String> headers = await globalCtx.Database.GetDbConnection().QueryAsync<String>("DBO.UDPS_GET_FORM_DATA_TF_HDR", p, null, null, CommandType.StoredProcedure);

                            // get tf name
                            IEnumerable<String> fileNames = await globalCtx.Database.GetDbConnection().QueryAsync<String>("DBO.UDPS_GET_FORM_DATA_TF_NAME", p, null, null, CommandType.StoredProcedure);
                            string fileName = fileNames.FirstOrDefault();
                            fileNames = null;

                            //get data
                            p = new DynamicParameters();
                            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                            p.Add("@FTR", ResolveFilter(listFtr));
                            p.Add("@SORT", "");
                            p.Add("@USR_ID", req.UserIdentity.UserId);
                            p.Add("@TF", true);

                            //string fileName = "";                            
                            string originalFileName = "";
                            List<string> files = new List<string>();
                            using (IDataReader rd = await globalCtx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA_TFSLIK", p, null, null, CommandType.StoredProcedure))
                            {
                                listColumns.Add(new FormDetail
                                {
                                    TFShow = true,
                                    FieldName = "TYPE",
                                    EdrControlType = "TEXT",
                                    TFDefaultValue = "D01",
                                    GrdColumnSeq = -1
                                });

                                listColumns = listColumns.OrderBy(c => c.GrdColumnSeq).ToList();

                                //fileName = kodeJenisPelapor + "." + sandiBank + "." + req.CommandModel.reportDate.ToString("yyyy") + "." + req.CommandModel.reportDate.ToString("MM") + "." + ojkFormCode + "." + "{seq}.txt";
                                originalFileName = fileName;

                                //string header = "H|" + kodeJenisPelapor + "|" + sandiBank + "|" + req.CommandModel.reportDate.ToString("yyyy") + "|" + req.CommandModel.reportDate.ToString("MM") + "|" + ojkFormCode + "|{totalRow}|" + totalRecord.ToString();
                                string header = headers.FirstOrDefault();
                                headers = null;
                                header = header.Replace("{totalRecord}", totalRecord.ToString());

                                files = _textfile.WriteDelimitedRegulatorWithHdrAndSplit(rd, listColumns, filePath + fileName, "|", header, maxRow, totalRecord);

                                rd.Close();
                            }

                            p = null;

                            foreach (string file in files)
                            {
                                string[] strs = file.Split("|");
                                //save to download list.
                                var generatelist = new ARRA.MODULE.Domain.Entities.GenerateTextFile
                                {
                                    FormCode = ojkFormCode,
                                    FileName = strs[0],
                                    BranchCode = "",
                                    BranchName = "",
                                    OriginalFileName = originalFileName,
                                    FormName = formName,
                                    TotalRecord = Convert.ToInt64(strs[1]),
                                    PeriodType = period,
                                    ReportDate = req.CommandModel.reportDate,
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host,
                                    Active = true
                                };
                                ctx.GenerateTextFiles.Add(generatelist);
                                generatelist = null;
                            }

                            hdrLog.ProcessStatus = JobStatus.Success.ToString();
                            ctx.ProcessLogHeaders.Update(hdrLog);

                            await ctx.SaveChangesAsync(cancellationToken);
                        }
                        catch (Exception ex)
                        {
                            hdrLog.ProcessStatus = JobStatus.Failed.ToString();
                            hdrLog.Remark = ex.Message;
                            ctx.ProcessLogHeaders.Update(hdrLog);

                            await ctx.SaveChangesAsync(cancellationToken);
                        }
                        hdrLog = null;

                    }
                    listColumns = null;
                    //downloadId = exportlist.UniqueId;

                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;
                }

            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
        private string GetPeriodFile(string formCode, string periodType, string periodCode, DateTime reportDate, IList<string> listRule)
        {
            string result = "";
            if (periodCode.ToUpper() == PeriodType.WEEKLY.ToString().Substring(0, 1))
            {
                int day = reportDate.Day;
                foreach (string rule in listRule)
                {
                    string[] strs = rule.Split('=');
                    if (day <= Convert.ToInt32(strs[1]))
                    {
                        result = strs[0];
                    }
                }
            }
            else
            {
                result = periodCode;
            }

            return result;
        }
    }

    public class TMP_FORMINFO_SLIK
    {
        public string Module { get; set; }
        public string PeriodType { get; set; }
        public string PeriodCode { get; set; }
        public string OjkFormCode { get; set; }
        public string FormName { get; set; }
    }
}