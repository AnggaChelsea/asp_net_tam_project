﻿using System;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class ExportTFDataSLIKCommand
    {
        public string formCode { get; set; }
        public DateTime reportDate { get; set; }
        public long jobId { get; set; }
    }
}