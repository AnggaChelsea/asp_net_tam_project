﻿using System;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands
{
    public class ExportTFDataCommand
    {
        public string formCode { get; set; }
        public string idOperational { get; set; }
        public DateTime reportDate { get; set; }
        public long jobId { get; set; }
        public string timeFlag { get; set; }
    }
}