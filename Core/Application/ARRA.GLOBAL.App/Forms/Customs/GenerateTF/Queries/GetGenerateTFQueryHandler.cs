﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Queries
{
    public class GetGenerateTFDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetGenerateTFDataQuery, FormBranchListModel>, FormBranchListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetGenerateTFDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<FormBranchListModel> Handle(QueriesModel<GetGenerateTFDataQuery, FormBranchListModel> req, CancellationToken cancellationToken)
        {
            FormBranchListModel vm = new FormBranchListModel();

            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "REPORT_DT").FirstOrDefault();
            if (ftr != null)
            {

                // 20221109: req user permata, bisa input date lain
                var ftrs = req.QueryModel.filter.Where(f => f.field == "REPORT_DT");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(ftr.value))
                {
                    FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
                    ARRA.GLOBAL.Domain.Entities.Module module = await _context.Modules.FindAsync(form.Module);

                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), form.FilterConditionEx);

                    IList<TMP_Branch> branches =
                        await (from br in _context.Branches
                               join br_a in _context.BranchGroupDetails
                                on br.BranchCode equals br_a.BranchCode
                               join cy in _context.BusinessParamDetails
                                on br.CountryCode equals cy.ParamValue
                               join rg in _context.RegulatorBranchs
                                on br.RegulatorBranch equals rg.BranchCode
                               where br_a.GroupId == req.UserIdentity.BranchGroup
                                && cy.ParamCode == "AN004"
                                && br.Unit == module.BusinessType
                                && rg.Unit == module.BusinessType
                               orderby rg.BranchCode
                               select new TMP_Branch
                               {
                                   branchCode = rg.BranchCode,
                                   branchName = rg.BranchCode+" - "+ rg.BranchName,
                                   country = cy.ParamDesc
                               }).Distinct().ToListAsync(cancellationToken);

                    IList<TMP_Form> forms =
                        await (from mg in _context.MenuGroupDetails
                               join m in _context.Menus
                                on mg.MenuCode equals m.MenuCode
                               join f in _context.FormHeaders
                                on m.FormCode equals f.FormCode
                               join pm in _context.Menus
                                on m.ParentMenuCode equals pm.MenuCode into t_menuparent
                               from xpm in t_menuparent.DefaultIfEmpty()
                               where mg.GroupId == req.UserIdentity.GroupId
                                && string.IsNullOrEmpty(f.OJKFormCode) == false
                                && f.Module == form.Module
                                && mg.AllowView == true
                               select new TMP_Form
                               {
                                   formCode = f.FormCode,
                                   formName = f.OJKFormCode+" - "+ f.FormName,
                                   menuName = m.MenuName,
                                   parentName = xpm.MenuName,
                                   parentCode = xpm.MenuCode,
                                   REPORT_DT = Convert.ToDateTime(ftr.value),
                                   PERIOD_TP = f.PeriodType
                               }
                               ).Where(objFtr.filterParams,objFtr.values).ToListAsync(cancellationToken);
    
                    vm.branch = this.GetBranchFrontEnd(branches);
                    vm.form = this.GetFormFrontEnd(forms);

                    branches = null;
                    forms = null;
                    objFtr = null;
                    form = null;
                }
            }
            ftr = null;

            return vm;
        }


        private IList<TreeModel> GetBranchFrontEnd(IList<TMP_Branch> branches)
        {
            IList<TreeModel> tree = new List<TreeModel>();
            IList<string> countries = branches.Select(c => c.country).Distinct().ToList();
            foreach (string cy in countries) //get country
            {
                tree.Add(new TreeModel
                {
                    value = cy,
                    name = cy,
                    isValue = false,
                    parent = "",
                    state = TreeState.show.ToString(),
                });
            }

            foreach (TreeModel m in tree)
            {
                IList<TMP_Branch> c1 = branches.Where(f => f.country == m.value).ToList();//branch
                if (c1.Count > 0)
                {
                    m.child = c1.Select(c => new TreeModel
                    {
                        value = c.branchCode,
                        name = c.branchName,
                        isValue = true,
                        state = TreeState.show.ToString(),
                        parent = m.value,
                    }).ToList();
                }
                c1 = null;
            }
            return tree;
        }

        private IList<TreeModel> GetFormFrontEnd(IList<TMP_Form> forms)
        {
            IList<TreeModel> tree = new List<TreeModel>();

            List<string> period = forms.Select(c => c.PERIOD_TP).Distinct().OrderBy(c=>c).ToList();
            foreach (string p in period) //get parent menu
            {
                tree.Add(new TreeModel
                {
                    value = p,
                    name = p,
                    isValue = false,
                    parent = "",
                    state = TreeState.show.ToString(),
                });
            }

            foreach (TreeModel m in tree)
            {
                IList<dynamic> c1 = forms.Where(f => f.PERIOD_TP == m.value)
                    .Select(c => new { c.parentName, c.parentCode })
                    .Distinct()
                    .OrderBy(c=>c.parentName)
                    .ToDynamicList();

                if (c1.Count > 0)
                {
                    m.child = c1.Select(c => new TreeModel
                    {
                        value = c.parentCode,
                        name = c.parentName,
                        isValue = false,
                        state = TreeState.hide.ToString(),
                        parent = m.value,
                    }).ToList();

                    foreach (TreeModel m1 in m.child)
                    {
                        IList<TMP_Form> c2 = forms.Where(f => f.parentCode == m1.value).OrderBy(o=>o.formCode).ToList();//area
                        m1.child = c2.Select(c => new TreeModel
                        {
                            value = c.formCode,
                            name = c.formName,
                            isValue = true,
                            state = TreeState.hide.ToString(),
                            parent = m1.value,
                        }).ToList();

                        c2 = null;
                    }
                }
                c1 = null;
            }
            period = null;

            return tree;
        }
    }

    public class TMP_Branch
    {
        public string branchCode { get; set; }
        public string branchName { get; set; }
        public string country { get; set; }
    }

    public class TMP_Form
    {
        public string menuName { get; set; }
        public string parentCode { get; set; }
        public string parentName { get; set; }
        public string formCode { get; set; }
        public string formName { get; set; }
        public string PERIOD_TP { get; set; }
        public DateTime REPORT_DT { get; set; }
        public short? sequence { get; set; }
        public short? parentSequence { get; set; }
        public short? parentSequence2 { get; set; }
    }
}
