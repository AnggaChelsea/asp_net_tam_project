﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Queries
{
    public class GetGenerateTFByPeriodDataQuery
    {
        public string formCode { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}
