﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.Common;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.Validation.Commands
{
    public class ValidationWoBRCommandHandler : AppBase, IRequestHandler<CommandsModel<ValidationWoBRCommand, FormBranchFailListModel>, FormBranchFailListModel>
    {
        private readonly GLOBALDbContext _context;
        public ValidationWoBRCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<FormBranchFailListModel> Handle(CommandsModel<ValidationWoBRCommand, FormBranchFailListModel> req, CancellationToken cancellationToken)
        {
            FormBranchFailListModel vm = new FormBranchFailListModel();
            vm.errors = new List<FormBranchFailModel>();

            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString());

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.form) == false)
                    {
                        string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode).Select(c => c.Module).FirstOrDefaultAsync(cancellationToken);
                        using (MODULEDbContext ctx = base.GetDbContext(module))
                        {
                            string[] forms = req.CommandModel.form.Split(',');
                            List<string> listSelectedForm = new List<string>();
                            if (forms.Length > 0)
                            {
                                IList<TMP_FORM_WO_BR> listForm =
                                               await _context.FormHeaders.Where(f => forms.Contains(f.FormCode))
                                                .Select(c => new TMP_FORM_WO_BR
                                                {
                                                    FormCode = c.FormCode,
                                                    PeriodType = c.PeriodType,
                                                    OJKCode = c.OJKFormCode,
                                                    FormName = c.FormName
                                                }).ToListAsync(cancellationToken);

                                IList<string> listExists =
                                    await ctx.ProcessLogHeaders.Where(f => f.DataDate == Convert.ToDateTime(ftr.value) && f.ProcessType == QueueJobType.VALIDATION.ToString() && f.ProcessStatus == JobStatus.Progress.ToString())
                                        .Select(c => c.ProcessNo).ToListAsync(cancellationToken);        

                                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);

                                for (int i = 0; i < forms.Length; i++)
                                {
                                    string strForm = forms[i].Trim();
                                    if (strForm != "")
                                    {
                                        if (listExists.Where(f => f == strForm).Count() > 0)
                                        {
                                            vm.errors.Add(new FormBranchFailModel
                                            {
                                                form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                message = ErrorMessages.ValidationExists
                                            });
                                        }
                                        else
                                        {
                                            listSelectedForm.Add(strForm);
                                        }
                                    }
                                }
                                listExists = null;

                                if (listSelectedForm.Count > 0)
                                {
                                    using (var trans = ctx.Database.BeginTransaction())
                                    {
                                        try
                                        {
                                            IList<long> tmpQueueId = new List<long>();
                                            foreach (string strForm in listSelectedForm.Distinct())
                                            {
                                                TMP_FORM_WO_BR fInfo = listForm.Where(f => f.FormCode == strForm).FirstOrDefault();

                                                ARRA.MODULE.Domain.Entities.JobQueueHeader hdr = new ARRA.MODULE.Domain.Entities.JobQueueHeader
                                                {
                                                    JobType = QueueJobType.VALIDATION.ToString(),
                                                    PeriodType = fInfo.PeriodType,
                                                    Status = JobStatus.Temp.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };

                                                ctx.JobQueueHeaders.Add(hdr);
                                                await ctx.SaveChangesAsync(cancellationToken);

                                                tmpQueueId.Add(hdr.UniqueId);

                                                ARRA.MODULE.Domain.Entities.JobQueueDetail dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "FORM_CODE",
                                                    ParamValue = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "REPORT_DATE",
                                                    ParamValue = ftr.value,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLogs = new MODULE.Domain.Entities.ProcessLogHeader
                                                {
                                                    DataDate = Convert.ToDateTime(ftr.value),
                                                    PeriodType = fInfo.PeriodType,
                                                    Branch = null,
                                                    ProcessNo = strForm,
                                                    ProcessName = fInfo.FormName,
                                                    ProcessStatus = JobStatus.Progress.ToString(),
                                                    ProcessType = QueueJobType.VALIDATION.ToString(),
                                                    QueueId = hdr.UniqueId,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.ProcessLogHeaders.Add(hdrLogs);


                                                AuditTrailHeader hdrAudit = new AuditTrailHeader
                                                {
                                                    MenuCode = menu.MenuCode,
                                                    MenuName = menu.MenuName,
                                                    Module = module,
                                                    WithApproval = false,
                                                    ActivityAction = AuditType.ADD.ToString(),
                                                    UserId = req.UserIdentity.UserId,
                                                    ActivityDate = req.CurrentDateTime,
                                                    ActivityHost = req.UserIdentity.Host,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailHeaders.Add(hdrAudit);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                AuditTrailDetail dtlAudit = new AuditTrailDetail
                                                {
                                                    FormColName = "FORM",
                                                    FieldName = "FORM",
                                                    FormSeq = 1,
                                                    HeaderId = hdrAudit.UniqueId,
                                                    ValueAfter = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailDetails.Add(dtlAudit);
                                                
                                                fInfo = null;
                                            }
                                            listForm = null;
                                            listExists = null;

                                            IList<ARRA.MODULE.Domain.Entities.JobQueueHeader> listUpd =
                                                await ctx.JobQueueHeaders.Where(f => tmpQueueId.Contains(f.UniqueId)).ToListAsync(cancellationToken);
                                            foreach(ARRA.MODULE.Domain.Entities.JobQueueHeader hdrupd in listUpd)
                                            {
                                                hdrupd.Status = JobStatus.Pending.ToString();
                                            }
                                            ctx.JobQueueHeaders.UpdateRange(listUpd);

                                            await ctx.SaveChangesAsync(cancellationToken);
                                            await _context.SaveChangesAsync(cancellationToken);

                                            trans.Commit();
                                        }
                                        catch (Exception ex)
                                        {
                                            trans.Rollback();

                                            throw ex;
                                        }
                                    }

                                }

                                menu = null;

                            }
                            forms = null;

                            listSelectedForm = null;
                        }//ctx
                    }
                }
                else
                {
                    err = ErrorMessages.AddFilterDate;
                }
            }
            else
            {
                err = ErrorMessages.AddFilterDate;
            }
            ftr = null;

            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return vm;
        }
    }
    
    public class TMP_FORM_WO_BR
    {
        public string FormCode { get; set; }
        public string OJKCode { get; set; }
        public string PeriodType { get; set; }
        public string FormName { get; set; }
    }

    public class TMP_FORMBRANCH_WO_BR
    {
        public string FormCode { get; set; }
        public string Branch { get; set; }

    }
}