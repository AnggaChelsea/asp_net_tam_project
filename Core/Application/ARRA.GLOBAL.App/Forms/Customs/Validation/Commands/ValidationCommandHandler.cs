﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.Common;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.Validation.Commands
{
    public class ValidationCommandHandler : AppBase, IRequestHandler<CommandsModel<ValidationCommand, FormBranchFailListModel>, FormBranchFailListModel>
    {
        private readonly GLOBALDbContext _context;
        public ValidationCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<FormBranchFailListModel> Handle(CommandsModel<ValidationCommand, FormBranchFailListModel> req, CancellationToken cancellationToken)
        {
            FormBranchFailListModel vm = new FormBranchFailListModel();
            vm.errors = new List<FormBranchFailModel>();

            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "REPORT_DT").FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == "REPORT_DT");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.form) == false &&
                        string.IsNullOrEmpty(req.CommandModel.branch) == false
                        )
                    {
                        string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode).Select(c => c.Module).FirstOrDefaultAsync(cancellationToken);
                        using (MODULEDbContext ctx = base.GetDbContext(module))
                        {
                            string[] branches = req.CommandModel.branch.Split(',');
                            string[] forms = req.CommandModel.form.Split(',');
                            List<string> listSelectedBranch = new List<string>();
                            List<string> listSelectedForm = new List<string>();
                            if (branches.Length > 0 && forms.Length > 0)
                            {
                                IList<TMP_FORM> listForm =
                                               await _context.FormHeaders.Where(f => forms.Contains(f.FormCode))
                                                .Select(c => new TMP_FORM
                                                {
                                                    FormCode = c.FormCode,
                                                    PeriodType = c.PeriodType,
                                                    OJKCode = c.OJKFormCode,
                                                    FormName = c.FormName
                                                }).ToListAsync(cancellationToken);

                                IList<TMP_FORMBRANCH> listExists =
                                                await (from f in ctx.JobQueueDetails
                                                       join b in ctx.JobQueueDetails
                                                        on f.HeaderId equals b.HeaderId
                                                       join c in ctx.JobQueueDetails
                                                        on f.HeaderId equals c.HeaderId
                                                       join h in ctx.JobQueueHeaders
                                                        on f.HeaderId equals h.UniqueId
                                                       where f.ParamName == "FORM_CODE"
                                                        && h.JobType == QueueJobType.VALIDATION.ToString()
                                                        && (h.Status == JobStatus.Progress.ToString() || h.Status == JobStatus.Pending.ToString())
                                                        && b.ParamName == "BRANCH"
                                                        && c.ParamName == "REPORT_DATE"
                                                        && c.ParamValue == Convert.ToDateTime(ftr.value).ToString("yyyy-MM-dd")
                                                       select new TMP_FORMBRANCH
                                                       {
                                                           FormCode = f.ParamValue,
                                                           Branch = b.ParamValue
                                                       }).ToListAsync(cancellationToken);

                                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);

                                for (int i = 0; i < forms.Length; i++)
                                {
                                    string strForm = forms[i].Trim();
                                    if (strForm != "")
                                    {
                                        for (int j = 0; j < branches.Length; j++)
                                        {
                                            string strBranch = branches[j].Trim();
                                            if (strBranch != "")
                                            {
                                                if (listExists.Where(f => f.FormCode == strForm && f.Branch == strBranch).Count() > 0)
                                                {
                                                    vm.errors.Add(new FormBranchFailModel
                                                    {
                                                        form = listForm.Where(f => f.FormCode == strForm).FirstOrDefault().OJKCode,
                                                        branch = strBranch,
                                                        message = ErrorMessages.ValidationExists
                                                    });
                                                }
                                                else
                                                {
                                                    listSelectedBranch.Add(strBranch);
                                                    listSelectedForm.Add(strForm);
                                                }
                                            }
                                        }
                                    }
                                }
                                listExists = null;

                                if (listSelectedForm.Count > 0 && listSelectedBranch.Count > 0)
                                {
                                    using (var trans = ctx.Database.BeginTransaction())
                                    {
                                        try
                                        {
                                            IList<long> tmpQueueId = new List<long>();
                                            foreach (string strForm in listSelectedForm.Distinct())
                                            {
                                                TMP_FORM fInfo = listForm.Where(f => f.FormCode == strForm).FirstOrDefault();

                                                ARRA.MODULE.Domain.Entities.JobQueueHeader hdr = new ARRA.MODULE.Domain.Entities.JobQueueHeader
                                                {
                                                    JobType = QueueJobType.VALIDATION.ToString(),
                                                    PeriodType = fInfo.PeriodType,
                                                    Status = JobStatus.Temp.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };

                                                ctx.JobQueueHeaders.Add(hdr);
                                                await ctx.SaveChangesAsync(cancellationToken);

                                                tmpQueueId.Add(hdr.UniqueId);

                                                ARRA.MODULE.Domain.Entities.JobQueueDetail dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "FORM_CODE",
                                                    ParamValue = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                dtl = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                {
                                                    HeaderId = hdr.UniqueId,
                                                    ParamName = "REPORT_DATE",
                                                    ParamValue = ftr.value,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                ctx.JobQueueDetails.Add(dtl);

                                                
                                                AuditTrailHeader hdrAudit = new AuditTrailHeader
                                                {
                                                    MenuCode = menu.MenuCode,
                                                    MenuName = menu.MenuName,
                                                    Module = module,
                                                    WithApproval = false,
                                                    ActivityAction = AuditType.ADD.ToString(),
                                                    UserId = req.UserIdentity.UserId,
                                                    ActivityDate = req.CurrentDateTime,
                                                    ActivityHost = req.UserIdentity.Host,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailHeaders.Add(hdrAudit);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                AuditTrailDetail dtlAudit = new AuditTrailDetail
                                                {
                                                    FormColName = "FORM",
                                                    FieldName = "FORM",
                                                    FormSeq = 1,
                                                    HeaderId = hdrAudit.UniqueId,
                                                    ValueAfter = strForm,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.AuditTrailDetails.Add(dtlAudit);

                                                foreach (string strBranch in listSelectedBranch.Distinct())
                                                {
                                                    dtl = new MODULE.Domain.Entities.JobQueueDetail()
                                                    {
                                                        HeaderId = hdr.UniqueId,
                                                        ParamName = "BRANCH",
                                                        ParamValue = strBranch,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(dtl);

                                                    
                                                    ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLogs = new MODULE.Domain.Entities.ProcessLogHeader
                                                    {
                                                        DataDate = Convert.ToDateTime(ftr.value),
                                                        PeriodType = fInfo.PeriodType,
                                                        Branch = strBranch,
                                                        ProcessNo = strForm,
                                                        ProcessName = fInfo.FormName,
                                                        ProcessStatus = JobStatus.Progress.ToString(),
                                                        ProcessType = QueueJobType.VALIDATION.ToString(),
                                                        QueueId = hdr.UniqueId,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.ProcessLogHeaders.Add(hdrLogs);

                                                    dtlAudit = new AuditTrailDetail
                                                    {
                                                        FormColName = "BRANCH",
                                                        FieldName = "BRANCH",
                                                        FormSeq = 2,
                                                        HeaderId = hdrAudit.UniqueId,
                                                        ValueAfter = strBranch,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    _context.AuditTrailDetails.Add(dtlAudit);

                                                    
                                                    hdrLogs = null;
                                                    dtl = null;

                                                }
                                                fInfo = null;
                                            }
                                            listForm = null;
                                            listExists = null;

                                            IList<ARRA.MODULE.Domain.Entities.JobQueueHeader> listUpd =
                                                await ctx.JobQueueHeaders.Where(f => tmpQueueId.Contains(f.UniqueId)).ToListAsync(cancellationToken);
                                            foreach(ARRA.MODULE.Domain.Entities.JobQueueHeader hdrupd in listUpd)
                                            {
                                                hdrupd.Status = JobStatus.Pending.ToString();
                                            }
                                            ctx.JobQueueHeaders.UpdateRange(listUpd);

                                            await ctx.SaveChangesAsync(cancellationToken);
                                            await _context.SaveChangesAsync(cancellationToken);

                                            trans.Commit();
                                        }
                                        catch (Exception ex)
                                        {
                                            trans.Rollback();

                                            throw ex;
                                        }
                                    }

                                }

                                menu = null;

                            }

                            branches = null;
                            forms = null;

                            listSelectedForm = null;
                            listSelectedBranch = null;
                        }//ctx
                    }
                }
                else
                {
                    err = ErrorMessages.AddFilterDate;
                }
            }
            else
            {
                err = ErrorMessages.AddFilterDate;
            }
            ftr = null;

            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return vm;
        }
    }
    
    public class TMP_FORM
    {
        public string FormCode { get; set; }
        public string OJKCode { get; set; }
        public string PeriodType { get; set; }
        public string FormName { get; set; }
    }

    public class TMP_FORMBRANCH
    {
        public string FormCode { get; set; }
        public string Branch { get; set; }

    }
}