﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Validation.Commands
{
    public class ValidationWoBRCommand
    {
        public string formCode { get; set; }
        public string form { get; set; }
        public string branch { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}