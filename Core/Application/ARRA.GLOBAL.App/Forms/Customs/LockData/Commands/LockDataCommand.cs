﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.LockData.Commands
{
    public class LockDataCommand
    {
        public string formCode { get; set; }
        public string form { get; set; }
        public string branch { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}