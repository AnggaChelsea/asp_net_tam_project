﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.MODULE.Domain.Entities;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Forms.Customs.LockData.Commands
{
    public class LockDataCommandHandler : AppBase, IRequestHandler<CommandsModel<LockDataCommand, FormBranchFailListModel>, FormBranchFailListModel>
    {
        private readonly GLOBALDbContext _context;
        public LockDataCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<FormBranchFailListModel> Handle(CommandsModel<LockDataCommand, FormBranchFailListModel> req, CancellationToken cancellationToken)
        {
            FormBranchFailListModel vm = new FormBranchFailListModel();
            vm.errors = new List<FormBranchFailModel>();

            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "REPORT_DT").FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == "REPORT_DT");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.form) == false &&
                        string.IsNullOrEmpty(req.CommandModel.branch) == false
                        )
                    {
                        string[] branches = req.CommandModel.branch.Split(',');
                        string[] forms = req.CommandModel.form.Split(',');
                        if (branches.Length > 0 && forms.Length>0)
                        {
                            string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode).Select(c => c.Module).FirstOrDefaultAsync(cancellationToken);
                            using (MODULEDbContext ctx = base.GetDbContext(module))
                            {
                                using (var trans = ctx.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        IList<TMP_LOCK> listLock =
                                            await ctx.LockDatas
                                            .Where(f => branches.Contains(f.Branch)
                                                && forms.Contains(f.FormCode)
                                                && f.ReportDate == Convert.ToDateTime(ftr.value)
                                                ).Select(c => new TMP_LOCK
                                                {
                                                    Branch = c.Branch,
                                                    FormCode = c.FormCode
                                                }).ToListAsync(cancellationToken);
                                        IList<TMP_FORM> listForm =
                                           await _context.FormHeaders.Where(f => forms.Contains(f.FormCode))
                                            .Select(c => new TMP_FORM
                                            {
                                                FormCode  = c.FormCode,
                                                PeriodType = c.PeriodType,
                                                OJKCode = c.OJKFormCode
                                            }).ToListAsync(cancellationToken);

                                        string auditForm = "";
                                        string auditBranch = "";
                                        for (int i = 0; i < forms.Length; i++)
                                        {
                                            string strForm = forms[i].Trim();
                                            if (strForm != "")
                                            {
                                                for (int j = 0; j < branches.Length; j++)
                                                {
                                                    string strBranch = branches[j].Trim();
                                                    if (strBranch != "")
                                                    {
                                                        TMP_LOCK lockdata = listLock.Where(f => f.Branch == strBranch && f.FormCode == strForm).FirstOrDefault();
                                                        TMP_FORM fInfo = listForm.Where(f => f.FormCode == strForm).FirstOrDefault();

                                                        if (lockdata == null)
                                                        {
                                                            ARRA.MODULE.Domain.Entities.LockData lck = new MODULE.Domain.Entities.LockData
                                                            {
                                                                ReportDate = Convert.ToDateTime(ftr.value),
                                                                Branch = strBranch,
                                                                FormCode = strForm,
                                                                PeriodType = fInfo.PeriodType,
                                                                CreatedBy = req.UserIdentity.UserId,
                                                                CreatedDate = req.CurrentDateTime,
                                                                CreatedHost = req.UserIdentity.Host
                                                            };

                                                            ctx.LockDatas.Add(lck);
                                                            auditForm += strForm + ", ";
                                                            auditBranch += strBranch + ", ";
                                                            lck = null;
                                                        }
                                                        else
                                                        {
                                                            vm.errors.Add(new FormBranchFailModel
                                                            {
                                                                form = fInfo.OJKCode,
                                                                branch = strBranch,
                                                                message = ErrorMessages.AlreadyLock
                                                            });
                                                        }
                                                    }//
                                                }
                                            }
                                        }

                                        listLock = null;
                                        listForm = null;
                                        await ctx.SaveChangesAsync(cancellationToken);

                                        if (auditBranch != "" && auditForm != "")
                                        {
                                            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                                            AuditTrailHeader hdrAudit = new AuditTrailHeader
                                            {
                                                MenuCode = menu.MenuCode,
                                                MenuName = menu.MenuName,
                                                Module = module,
                                                WithApproval = false,
                                                ActivityAction = AuditType.ADD.ToString(),
                                                UserId = req.UserIdentity.UserId,
                                                ActivityDate = req.CurrentDateTime,
                                                ActivityHost = req.UserIdentity.Host,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            };
                                            _context.AuditTrailHeaders.Add(hdrAudit);

                                            await _context.SaveChangesAsync(cancellationToken);

                                            AuditTrailDetail dtlAudit = new AuditTrailDetail
                                            {
                                                FormColName = "FORM",
                                                FieldName = "FORM",
                                                FormSeq = 1,
                                                HeaderId = hdrAudit.UniqueId,
                                                ValueAfter = auditForm,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            };
                                            _context.AuditTrailDetails.Add(dtlAudit);

                                            dtlAudit = new AuditTrailDetail
                                            {
                                                FormColName = "BRANCH",
                                                FieldName = "BRANCH",
                                                FormSeq = 2,
                                                HeaderId = hdrAudit.UniqueId,
                                                ValueAfter = auditBranch,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            };
                                            _context.AuditTrailDetails.Add(dtlAudit);

                                            await _context.SaveChangesAsync(cancellationToken);
                                        }

                                        trans.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trans.Rollback();

                                        throw ex;
                                    }
                                }
                            }//ctx
                        }
                    }
                }
                else
                {
                    err = ErrorMessages.AddFilterDate;
                }
            }
            else
            {
                err = ErrorMessages.AddFilterDate;
            }
            ftr = null;


            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});

            }

            return vm;
        }
    }

    public class TMP_LOCK
    {
        public string Branch { get; set; }
        public string FormCode { get; set; }
    }
    public class TMP_FORM
    {
        public string FormCode { get; set; }
        public string OJKCode { get; set; }
        public string PeriodType { get; set; }
    }
}