﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.LockData.Commands
{
    public class LockDataCommandWoBR
    {
        public string formCode { get; set; }
        public string form { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}
