﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.LockData.Queries
{
    public class GetFormBIBranchDataQuery
    {
        public string formCode { get; set; }
        public IList<FilterModel> filter { get; set; }
    }

}