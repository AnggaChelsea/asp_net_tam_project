﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Models;
using Dapper;
using System.Data;
using System;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Queries
{
    public class GetValDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetValDataQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetValDataQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }


        public async Task<DataListModel> Handle(QueriesModel<GetValDataQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
            p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
            p.Add("@USR_ID", req.UserIdentity.UserId);
            IEnumerable<Int64> records = await _context.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
            long totalRecord = records.FirstOrDefault();

            dynamic formData = new List<dynamic>();
            dynamic aggregatedata = "";
            dynamic invalid = new List<dynamic>();

            if (totalRecord > 0)
            {
                p = new DynamicParameters();
                p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
                p.Add("@SORT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.sort));
                p.Add("@PAGE", req.QueryModel.start);
                p.Add("@PAGESIZE", req.QueryModel.pageSize);
                p.Add("@USR_ID", req.UserIdentity.UserId);
                formData = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure);

                p = new DynamicParameters();
                p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                p.Add("@FTR", ResolveFilter(req.QueryModel.filter));
                p.Add("@USR_ID", req.UserIdentity.UserId);
                aggregatedata = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORM_DATA_AGGREGATE", p, null, null, CommandType.StoredProcedure);

                DateTime? dtRpt = null;
                Forms.Models.FilterModel ftr = req.QueryModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();
                if (ftr != null)
                {
                    if (!string.IsNullOrEmpty(ftr.value))
                    {
                        dtRpt = Convert.ToDateTime(ftr.value);
                    }
                }
                ftr = null;

                string uidStr = "";
                foreach(dynamic item in formData)
                {
                    uidStr += item.UID.ToString() + ",";
                }
                uidStr = uidStr.Substring(0, uidStr.Length - 1);
                p = new DynamicParameters();
                p.Add("@REPORT_DT", dtRpt);
                p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                p.Add("@UID_STR", uidStr);

                IEnumerable<InvalidDataModel> listInvalid = await _context.Database.GetDbConnection().QueryAsync<InvalidDataModel>("DBO.UDPS_GET_INVALID_DATA", p, null, null, CommandType.StoredProcedure);
                string prevField = "";
                string field = "";
                string msgs = "";
                bool isErr = false;
                foreach(dynamic item in formData)
                {
                    long id = item.UID;
                    field = "";
                    msgs = "";
                    prevField = "";
                    isErr = false;
                    IList<InvalidDataModel> invs = listInvalid.Where(f => f.UID == id).OrderBy(c=>c.FIELD).ToList();
                    foreach(InvalidDataModel i in invs)
                    {
                        string typeError = Convert.ToString(i.TP).Substring(0, 1);
                        if (typeError == "E") isErr = true;
                        if (prevField == "")
                        {
                            field += i.FIELD + "^";
                        }
                        else if (i.FIELD != prevField)
                        {
                            field += i.FIELD + "^";
                            msgs += "^";
                        }

                        if (i.VALNO != 0)
                        {
                            msgs += "Rule Id (" + i.VALNO + ") (" + typeError + ") : " + i.MSG + "\n";
                        }
                        else
                        {
                            msgs += "BI : " + i.MSG + "\n";
                        }

                        prevField = i.FIELD;
                    }
                    invs = null;
                    item.ERR_F = field;
                    item.ERR_M = msgs;
                    item.ERR_T = isErr ? "E" : "W";
                }

                listInvalid = null;
                p = null;
            }

            return new DataListModel
            {
                totalRecord = records.FirstOrDefault(),
                data = formData,
                aggregate = aggregatedata
            };
        }

    }
}