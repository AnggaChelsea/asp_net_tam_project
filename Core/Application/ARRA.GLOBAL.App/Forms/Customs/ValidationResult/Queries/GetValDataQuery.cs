﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Queries
{
    public class GetValDataQuery
    {
        public string formCode { get; set; }
        public int start { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public IList<FilterModel> filter { get; set; }
    }

}