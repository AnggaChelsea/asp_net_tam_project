﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands
{
    public class ExportValDataCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportValDataCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;
        //public ExportValDataCommandHandler(
        //    GLOBALDbContext context, IExcelConnector excel, ITextFileConnector textfile) : base(context)
        //{
        //    _context = context;
        //    _excel = excel;
        //    _textfile = textfile;
        //}
        public ExportValDataCommandHandler(
            IExcelConnector excel, ITextFileConnector textfile,IConfiguration configuration) : base(configuration)
        {
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportValDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            using (GLOBALDbContext globalctx = base.GetGlobalDbContext())
            {
                try
                {
                    IList<FilterModel> listFtr = new List<FilterModel>();
                    if (!String.IsNullOrEmpty(req.CommandModel.filter))
                    {
                        listFtr = JsonConvert.DeserializeObject<List<FilterModel>>(req.CommandModel.filter);
                    }

                    //get total record
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    p.Add("@FTR", ResolveFilter(listFtr));
                    p.Add("@USR_ID", req.UserIdentity.UserId);
                    IEnumerable<Int64> records = await globalctx.Database.GetDbConnection().QueryAsync<Int64>("DBO.UDPS_GET_FORM_DATA_COUNT", p, null, null, CommandType.StoredProcedure);
                    long totalRecord = records.FirstOrDefault();

                    //get form info
                    FormHeader form = await globalctx.FormHeaders.FindAsync(req.CommandModel.formCode);
                    IList<FormDetail> listColumns = await globalctx.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                    //Get File Path
                    SystemParameterHeader parDetail = await globalctx.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                    string filePath = parDetail.ParamValue;

                    //get data
                    p = new DynamicParameters();
                    p.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    p.Add("@FTR", ResolveFilter(listFtr));
                    p.Add("@SORT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.sort));
                    p.Add("@USR_ID", req.UserIdentity.UserId);

                    List<dynamic> rdVal = new List<dynamic>();
                    if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                    {
                        DynamicParameters dp = new DynamicParameters();
                        dp.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                        dp.Add("@SORT", "");
                        dp.Add("@PAGE", "0");
                        dp.Add("@PAGESIZE", "0");
                        dp.Add("@FTR", ResolveFilter(listFtr));
                        dp.Add("@IS_VALIDATION", "1");
                        var validationData = await globalctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_VALIDATION_DATA", dp, null, null, CommandType.StoredProcedure);
                        rdVal = validationData.ToList();
                        validationData = null;
                    }

                    string fileName = "";
                    //using (IDataReader rd = await _context.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA_INVALID", p, null, null, CommandType.StoredProcedure))
                    using (IDataReader rd = await globalctx.Database.GetDbConnection().ExecuteReaderAsync("DBO.UDPS_GET_FORM_DATA", p, null, null, CommandType.StoredProcedure))
                    {
                        fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                        if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                        {
                            fileName += ".xlsx";
                            _excel.WriteExcelValidation(rd, rdVal, listColumns, filePath + fileName);
                        }
                        else if (req.CommandModel.fileType == FileType.CSV.ToString())
                        {
                            fileName += ".csv";
                            _textfile.WriteCSV(rd, listColumns, filePath + fileName);
                        }
                        else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                        {
                            fileName += ".txt";
                            _textfile.WriteDelimited(rd, listColumns, filePath + fileName, form.TFDelimtedChar);
                        }
                        else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                        {
                            fileName += ".txt";
                            _textfile.WriteFixLength(rd, listColumns, filePath + fileName);
                        }

                        await globalctx.SaveChangesAsync(cancellationToken);

                        rd.Close();
                    }

                    //save to download list.
                    var exportlist = new ExportFileList
                    {
                        UserId = req.UserIdentity.UserId,
                        Module = form.Module,
                        FormCode = req.CommandModel.formCode,
                        JobQueueId = req.CommandModel.jobId,
                        FileName = fileName,
                        TotalRecord = totalRecord,
                        TotalInserted = totalRecord,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host
                    };
                    globalctx.ExportFileLists.Add(exportlist);
                    await globalctx.SaveChangesAsync(cancellationToken);

                    downloadId = exportlist.UniqueId;

                }
                catch (Exception ex)
                {
                    status = CommandStatus.Failed.ToString();
                    message = ex.Message;
                }
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
    }
}