﻿namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands
{
    public class CreateExportValTaskCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
    }
}