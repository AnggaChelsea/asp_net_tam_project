﻿namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands
{
    public class AutoFixDataCommand
    {
        public string formCode { get; set; }
        public string reportDt { get; set; }
        public string uid { get; set; }
    }
}