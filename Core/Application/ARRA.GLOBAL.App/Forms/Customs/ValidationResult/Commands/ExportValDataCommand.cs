﻿namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands
{
    public class ExportValDataCommand
    {
        public string formCode { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
        public string fileType { get; set; }
        public long jobId { get; set; }
    }
}