﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Exceptions;

namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands
{
    public class AutoFixDataCommandHandler : AppBase, IRequestHandler<CommandsModel<AutoFixDataCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public AutoFixDataCommandHandler(
            GLOBALDbContext context, IMediator mediator, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<AutoFixDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }

            try
            {
                using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                {
                    DynamicParameters dpTable = new DynamicParameters();                    
                    dpTable.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                    dpTable.Add("@REPORT_DT", req.CommandModel.reportDt);
                    dpTable.Add("@USR_ID", req.UserIdentity.UserId);
                    dpTable.Add("@HOST", req.UserIdentity.Host);
                    dpTable.Add("@UID", req.CommandModel.uid);
                    await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPP_AUTO_FIX", dpTable, null, null, CommandType.StoredProcedure);
                }
            } catch(Exception ex)
            {
                status = CommandStatus.Failed.ToString();
                message = ex.Message;
            }
            
            return new StatusModel
            {
                status = status,
                message = message
            };
        }
    }
}