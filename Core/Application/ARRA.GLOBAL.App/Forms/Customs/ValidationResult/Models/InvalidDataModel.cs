﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Models
{
    public class InvalidDataModel
    {
        public string FIELD { get; set; }
        public long UID { get; set; }
        public int VALNO { get; set; }
        public string MSG { get; set; }
        public string TP { get; set; }

    }
}
