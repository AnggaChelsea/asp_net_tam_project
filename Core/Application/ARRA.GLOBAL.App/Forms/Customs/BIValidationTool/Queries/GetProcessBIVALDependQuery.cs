﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALDependQuery
    {
        public string formCode { get; set; }
        public string procNo { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}