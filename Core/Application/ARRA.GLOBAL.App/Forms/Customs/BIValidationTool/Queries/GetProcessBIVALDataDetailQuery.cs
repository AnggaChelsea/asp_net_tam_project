﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALDataDetailQuery
    {
        public string formCode { get; set; }
        public long uid { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}