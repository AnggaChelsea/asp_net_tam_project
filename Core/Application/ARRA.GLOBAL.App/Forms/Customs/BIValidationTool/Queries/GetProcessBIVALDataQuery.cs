﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALDataQuery
    {
        public string formCode { get; set; }
        public int start { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public IList<FilterModel> filter { get; set; }
    }

}