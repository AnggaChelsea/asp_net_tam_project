﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALDataDetailQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessBIVALDataDetailQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetProcessBIVALDataDetailQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetProcessBIVALDataDetailQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            dynamic formData = new List<dynamic>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    //FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
                    using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
                    {
                        string procno = await ctx.BIValidationTools.Where(f => f.UniqueId == req.QueryModel.uid).Select(c => c.ProcessNo).FirstOrDefaultAsync(cancellationToken);

                        long uidlog = await ctx.ProcessLogHeaders
                                        .Where(f => f.ProcessNo == procno
                                            && f.DataDate == Convert.ToDateTime(ftr.value)
                                            && f.ProcessType == QueueJobType.BI_VALIDATION.ToString()
                                        ).OrderByDescending(o => o.UniqueId).Take(1)
                                        .Select(s => s.UniqueId).FirstOrDefaultAsync(cancellationToken);

                        formData = await (from tf in ctx.BIValidationTools
                                          join hs in ctx.ProcessLogHeaders.Where(f => f.UniqueId == uidlog)
                                           on tf.ProcessNo equals hs.ProcessNo into t_log
                                          from xhs in t_log.DefaultIfEmpty()
                                          join us in ctx.SN_Users
                                           on xhs.CreatedBy equals us.UserId into t_usr
                                          from xus in t_usr.DefaultIfEmpty()
                                          where tf.UniqueId == req.QueryModel.uid
                                          select new
                                          {
                                              uid = tf.UniqueId,
                                              date = ftr.value,
                                              period = tf.PeriodType,
                                              proc_no = tf.ProcessNo,
                                              proc_nm = tf.ProcessName,
                                              sts = xhs.ProcessStatus,
                                              run_by = xus.FirstName,
                                              run_dt = xhs.CreatedDate,
                                              note = xhs.Remark
                                          }).ToListAsync<dynamic>(cancellationToken);
                    }
                }
            }
            return new DataListModel
            {
                totalRecord = 1,
                data = formData
            };
        }
    }

    public class StatusApproval
    {
        public string SubmitBy { get; set; }
        public DateTime? SubmitDate { get; set; }
        public string Action { get; set; }
    }
}