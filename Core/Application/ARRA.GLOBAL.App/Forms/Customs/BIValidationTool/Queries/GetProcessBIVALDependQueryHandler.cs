﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using ARRA.Common.Enumerations;
using System.IO;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALDependQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessBIVALDependQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetProcessBIVALDependQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetProcessBIVALDependQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    //FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);

                    using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
                    {
                        IList<long> listid = await ctx.ProcessLogHeaders
                                        .Where(
                                            f => f.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                            && f.ProcessType == QueueJobType.BI_VALIDATION.ToString()
                                         )
                                        .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                                    ).ToListAsync<long>(cancellationToken);

                        _resultDepend = new List<string>();
                        _resultDepend.Add(req.QueryModel.procNo);
                        IList<TempDependModel> list =
                            await (from d in ctx.ProcessDependenciess
                                   join p in ctx.BIValidationTools
                                    on d.ProcessNo equals p.ProcessNo
                                   join h in ctx.ProcessLogHeaders.Where(f => listid.Contains(f.UniqueId))
                                    on d.ProcessNo equals h.ProcessNo into t_log
                                   from xhs in t_log.DefaultIfEmpty()
                                   where d.ProcessType == QueueJobType.BI_VALIDATION.ToString()
                                   select new TempDependModel
                                   {
                                       procNo = d.ProcessNo,
                                       depend = d.DependProcessNo,
                                       sts = xhs.ProcessStatus,
                                   }
                                   ).ToListAsync(cancellationToken);

                        RecursiveCheck(req.QueryModel.procNo, list);
                        list = null;
                    }

                }
            }

            return new SelectionListModel
            {
                data = _resultDepend
            };
        }

        int _maxDependCounter = 0;
        IList<string> _resultDepend;

        private void RecursiveCheck(string procNo, IList<TempDependModel> ls)
        {
            if (_maxDependCounter > 100) return; //maximum-100 to prevent invalid setting/loop forever.
            IList<TempDependModel> list = 
                ls.Where(f => f.depend == procNo).ToList();

            if (list.Count == 0)
            {
                return; //end of recursive.
            }
            else
            {
                foreach (TempDependModel d in list)
                {
                    if(d.sts!=JobStatus.Pending.ToString() 
                        && d.sts != JobStatus.Progress.ToString()
                        && d.sts != JobStatus.Cancelling.ToString())
                    {
                        _resultDepend.Add(d.procNo);
                    }
                    _maxDependCounter++;
                    RecursiveCheck(d.procNo, ls);
                }
            }
        }

    }

    public class TempDependModel
    {
        public string procNo { get; set; }
        public string depend { get; set; }
        public string sts { get; set; }
    }
}