﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries
{
    public class GetProcessBIVALSelectAllQuery
    {
        public string formCode { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}