﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Newtonsoft.Json;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Commands
{
    public class ExportDataCommandBIVALHandler : AppBase, IRequestHandler<CommandsModel<ExportDataBIVALCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;
        public ExportDataCommandBIVALHandler(
            GLOBALDbContext context, IExcelConnector excel, ITextFileConnector textfile, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportDataBIVALCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            try
            {
                IList<FilterModel> listFtr = new List<FilterModel>();
                if (!String.IsNullOrEmpty(req.CommandModel.filter))
                {
                    listFtr = JsonConvert.DeserializeObject<List<FilterModel>>(req.CommandModel.filter);
                }

                FilterModel ftr = listFtr.Where(f => f.field == "date").FirstOrDefault();
                if (ftr != null)
                {
                    if (ftr.value != null)
                    {
                        //get form info
                        FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);

                        DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(listFtr), form.FilterConditionEx);

                        using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                        {
                            IList<long> listid = await ctx.ProcessLogHeaders
                                    .Where(
                                        f => f.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                        && f.ProcessType == QueueJobType.BI_VALIDATION.ToString()
                                        )
                                    .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                                ).ToListAsync<long>(cancellationToken);

                            var query = (from tf in ctx.BIValidationTools
                                         join hs in ctx.ProcessLogHeaders.Where(f => listid.Contains(f.UniqueId))
                                          on tf.ProcessNo equals hs.ProcessNo into t_log
                                         from xhs in t_log.DefaultIfEmpty()
                                         join us in ctx.SN_Users
                                          on xhs.CreatedBy equals us.UserId into t_usr
                                         from xus in t_usr.DefaultIfEmpty()
                                         select new
                                         {
                                             uid = tf.UniqueId,
                                             date = ftr.value,
                                             period = tf.PeriodType,
                                             proc_no = tf.ProcessNo,
                                             proc_nm = tf.ProcessName,
                                             sts = xhs.ProcessStatus,
                                             run_by = xus.FirstName,
                                             run_dt = xhs.CreatedDate,
                                             note = xhs.Remark
                                         }
                                ).Where(objFtr.filterParams, objFtr.values);

                            objFtr = null;

                            int totalRecord = await query.CountAsync(cancellationToken);

                            List<dynamic> listdy = await query
                                .OrderBy(ARRA.Common.Utility.SafeSqlString(req.CommandModel.sort))
                                .ToDynamicListAsync();

                            IList<FormDetail> listColumns = await _context.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                            //Get File Path
                            SystemParameterHeader parDetail = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).FirstOrDefaultAsync(cancellationToken);
                            string filePath = parDetail.ParamValue;

                            string fileName = form.FormCode + "-" + DateTime.Now.Ticks.ToString();
                            if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                            {
                                fileName += ".xlsx";
                                _excel.WriteExcel(listdy, listColumns, filePath + fileName);
                            }
                            else if (req.CommandModel.fileType == FileType.CSV.ToString())
                            {
                                fileName += ".csv";
                                _textfile.WriteCSV(listdy, listColumns, filePath + fileName);
                            }
                            else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                            {
                                fileName += ".txt";
                                _textfile.WriteDelimited(listdy, listColumns, filePath + fileName, form.TFDelimtedChar);
                            }
                            else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                            {
                                fileName += ".txt";
                                _textfile.WriteFixLength(listdy, listColumns, filePath + fileName);
                            }

                            //save to download list.
                            var exportlist = new ExportFileList
                            {
                                UserId = req.UserIdentity.UserId,
                                Module = form.Module,
                                FormCode = req.CommandModel.formCode,
                                JobQueueId = req.CommandModel.jobId,
                                FileName = fileName,
                                TotalRecord = totalRecord,
                                TotalInserted = totalRecord,
                                CreatedBy = req.UserIdentity.UserId,
                                CreatedDate = req.CurrentDateTime,
                                CreatedHost = req.UserIdentity.Host
                            };
                            _context.ExportFileLists.Add(exportlist);
                            await _context.SaveChangesAsync(cancellationToken);

                            downloadId = exportlist.UniqueId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                status = CommandStatus.Failed.ToString();
                message = ex.Message;
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
    }
}