﻿namespace ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Commands
{
    public class CreateExportTaskBIVALCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
    }
}