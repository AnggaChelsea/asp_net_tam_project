﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using ARRA.Common.Enumerations;
using System.IO;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Queries
{
    public class GetProcessTransformSelectAllQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessTransformSelectAllQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        public GetProcessTransformSelectAllQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetProcessTransformSelectAllQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);

                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), form.FilterConditionEx);

                    using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                    {
                        IList<long> listid = await ctx.ProcessLogHeaders
                                .Where(f =>
                                    f.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                    && f.ProcessType == QueueJobType.TRANSFORM.ToString()
                                    )
                                .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                            ).ToListAsync<long>(cancellationToken);

                        listSelect = await (from tf in ctx.TransformProcesses
                                            join hs in ctx.ProcessLogHeaders.Where(f => listid.Contains(f.UniqueId))
                                             on tf.ProcessNo equals hs.ProcessNo into t_log
                                            from xhs in t_log.DefaultIfEmpty()
                                            join us in ctx.SN_Users
                                             on xhs.CreatedBy equals us.UserId into t_usr
                                            from xus in t_usr.DefaultIfEmpty()
                                            select new
                                            {
                                                uid = tf.UniqueId,
                                                date = ftr.value,
                                                period = tf.PeriodType,
                                                proc_no = tf.ProcessNo,
                                                proc_nm = tf.ProcessName,
                                                sts = xhs.ProcessStatus == null ? "" : xhs.ProcessStatus,
                                                run_by = xus.FirstName,
                                                run_dt = xhs.CreatedDate,
                                                note = xhs.Remark
                                            }
                                            ).Where(objFtr.filterParams, objFtr.values)
                                            .Where(
                                                f => f.sts != JobStatus.Pending.ToString()
                                                && f.sts != JobStatus.Progress.ToString()
                                                && f.sts != JobStatus.Cancelling.ToString()
                                                )
                                            .Select(c => c.proc_no)
                                            .ToListAsync<string>(cancellationToken);

                        objFtr = null;
                    }
                }
            }

            return new SelectionListModel
            {
                data = listSelect
            };
        }

    }
}