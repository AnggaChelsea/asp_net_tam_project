﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Queries
{
    public class GetProcessTransformSelectAllQuery
    {
        public string formCode { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}