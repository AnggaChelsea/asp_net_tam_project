﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using System.IO;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Queries
{
    public class GetProcessTransformDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessTransformDataQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetProcessTransformDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetProcessTransformDataQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            int totalRecord = 0;
            dynamic formData = new List<dynamic>();

            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {

                // 20221109: req user permata, bisa input date lain
                var ftrs = req.QueryModel.filter.Where(f => f.field == "date");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(ftr.value))
                {
                    FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);

                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), form.FilterConditionEx);

                    using(MODULEDbContext ctx = base.GetDbContext(form.Module))
                    {
                        IList<long> listid = await ctx.ProcessLogHeaders
                            .Where(ff =>
                                ff.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                && ff.ProcessType == QueueJobType.TRANSFORM.ToString()
                                )
                            .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                        ).ToListAsync<long>(cancellationToken);

                        // hide finish job jika masih pending dan progress
                        List<string> pendStatus = new List<string>() { "Pending", "Progress"};

                        var query = (from tf in ctx.TransformProcesses
                                     join hs in ctx.ProcessLogHeaders.Where(ff => listid.Contains(ff.UniqueId))
                                      on tf.ProcessNo equals hs.ProcessNo into t_log
                                     from xhs in t_log.DefaultIfEmpty()
                                     join us in ctx.SN_Users
                                      on xhs.CreatedBy equals us.UserId into t_usr
                                     from xus in t_usr.DefaultIfEmpty()
                                     select new
                                     {
                                         uid = tf.UniqueId,
                                         date = ftr.value,
                                         period = tf.PeriodType,
                                         proc_no = tf.ProcessNo,
                                         proc_nm = tf.ProcessName,
                                         sts = xhs.ProcessStatus,
                                         run_by = xus.FirstName,
                                         run_dt = xhs.CreatedDate,
                                         note = xhs.Remark,
                                         startjob = xhs.CreatedDate,
                                         finishjob = pendStatus.Contains(xhs.ProcessStatus) ? null : xhs.ModifiedDate
                                     }
                            ).Where(objFtr.filterParams, objFtr.values);

                        objFtr = null;

                        totalRecord = await query.CountAsync(cancellationToken);

                        if (totalRecord > 0)
                        {
                            formData = await query
                                .OrderBy(Utility.SafeSqlString(req.QueryModel.sort))
                                .Skip(req.QueryModel.start)
                                .Take(req.QueryModel.pageSize)
                                .ToDynamicListAsync();

                            listid = null;
                        }
                        //
                    }
                    form = null;
                }
            }

            return new DataListModel
            {
                totalRecord = totalRecord,
                data = formData
            };

        }

    }
}