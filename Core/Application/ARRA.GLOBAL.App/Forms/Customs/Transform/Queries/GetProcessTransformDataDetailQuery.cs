﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Queries
{
    public class GetProcessTransformDataDetailQuery
    {
        public string formCode { get; set; }
        public long uid { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}