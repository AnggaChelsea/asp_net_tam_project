﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.MODULE.Domain.Entities;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Commands
{
    public class RunProcessTransformCommandHandler : AppBase, IRequestHandler<CommandsModel<RunProcessTransformCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RunProcessTransformCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RunProcessTransformCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.select) == false)
                    {
                        string[] select = req.CommandModel.select.Split('$');
                        if (select.Length > 0)
                        {
                            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);

                            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                            {
                                // check lock transform
                                string errorLocked = await CheckLockFormCode(req, ftr, ctx, cancellationToken);

                                if (!errorLocked.Equals(string.Empty))
                                    throw new CustomException(errorLocked);
                                // end check lock transform

                                using (var trans = ctx.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        IList<string> audit = new List<string>();
                                        IList<TransformProcess> dsList =
                                            await ctx.TransformProcesses.Where(
                                                f => select.Contains(f.ProcessNo))
                                            .ToListAsync(cancellationToken);
                                        //+period

                                        for (int i = 0; i < select.Length; i++)
                                        {
                                            string procNo = select[i];
                                            if (!string.IsNullOrEmpty(procNo))
                                            {
                                                IList<ARRA.MODULE.Domain.Entities.ProcessLogHeader> list =
                                                    await ctx.ProcessLogHeaders.Where(f =>
                                                    f.DataDate == Convert.ToDateTime(ftr.value)
                                                    && f.ProcessNo == procNo
                                                    ).ToListAsync(cancellationToken);

                                                if (list.Where(f => f.ProcessStatus == JobStatus.Pending.ToString()
                                                     || f.ProcessStatus == JobStatus.Progress.ToString()
                                                     || f.ProcessStatus == JobStatus.Cancelling.ToString()
                                                ).FirstOrDefault() == null)
                                                {
                                                    //remove exists hist
                                                    ctx.ProcessLogHeaders.RemoveRange(list);

                                                    ARRA.MODULE.Domain.Entities.TransformProcess tf = dsList.Where(f => f.ProcessNo == procNo).FirstOrDefault();

                                                    //add job queue
                                                    ARRA.MODULE.Domain.Entities.JobQueueHeader hdrQueue = new ARRA.MODULE.Domain.Entities.JobQueueHeader
                                                    {
                                                        JobType = QueueJobType.TRANSFORM.ToString(),
                                                        PeriodType = tf.PeriodType,
                                                        Status = JobStatus.Pending.ToString(),
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueHeaders.Add(hdrQueue);

                                                    await ctx.SaveChangesAsync(cancellationToken);

                                                    //add hist
                                                    ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrHist = new ARRA.MODULE.Domain.Entities.ProcessLogHeader
                                                    {
                                                        DataDate = Convert.ToDateTime(ftr.value),
                                                        ProcessNo = procNo,
                                                        ProcessName = tf.ProcessName,
                                                        ProcessStatus = JobStatus.Progress.ToString(),
                                                        ProcessType = QueueJobType.TRANSFORM.ToString(),
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host,
                                                        QueueId = hdrQueue.UniqueId,
                                                        PeriodType = tf.PeriodType
                                                    };

                                                    ctx.ProcessLogHeaders.Add(hdrHist);
                                                    await ctx.SaveChangesAsync(cancellationToken);

                                                    ARRA.MODULE.Domain.Entities.JobQueueDetail hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                    {
                                                        HeaderId = hdrQueue.UniqueId,
                                                        ParamName = "DATA_DATE",
                                                        ParamValue = ftr.value,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(hdrQueueD);

                                                    hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                    {
                                                        HeaderId = hdrQueue.UniqueId,
                                                        ParamName = "PROCESS_NO",
                                                        ParamValue = procNo,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(hdrQueueD);

                                                    hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                    {
                                                        HeaderId = hdrQueue.UniqueId,
                                                        ParamName = "JOB_ID",
                                                        ParamValue = hdrHist.UniqueId.ToString(),
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(hdrQueueD);

                                                    hdrQueueD = new ARRA.MODULE.Domain.Entities.JobQueueDetail
                                                    {
                                                        HeaderId = hdrQueue.UniqueId,
                                                        ParamName = "MODULE",
                                                        ParamValue = form.Module,
                                                        CreatedBy = req.UserIdentity.UserId,
                                                        CreatedDate = req.CurrentDateTime,
                                                        CreatedHost = req.UserIdentity.Host
                                                    };
                                                    ctx.JobQueueDetails.Add(hdrQueueD);

                                                    await ctx.SaveChangesAsync(cancellationToken);

                                                    audit.Add("DATA_DATE=" + ftr.value + ", PROCESS_NO=" + procNo + ", JOB_ID=" + hdrHist.UniqueId.ToString());
                                                    hdrHist = null;
                                                    hdrQueue = null;
                                                    hdrQueueD = null;
                                                }

                                                list = null;
                                            }
                                        }

                                        Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                                        AuditTrailHeader hdrAudit = new AuditTrailHeader
                                        {
                                            MenuCode = menu.MenuCode,
                                            MenuName = menu.MenuName,
                                            Module = form.Module,
                                            WithApproval = false,
                                            ActivityAction = AuditType.PROCESS.ToString(),
                                            UserId = req.UserIdentity.UserId,
                                            ActivityDate = req.CurrentDateTime,
                                            ActivityHost = req.UserIdentity.Host,
                                            CreatedBy = req.UserIdentity.UserId,
                                            CreatedDate = req.CurrentDateTime,
                                            CreatedHost = req.UserIdentity.Host
                                        };
                                        _context.AuditTrailHeaders.Add(hdrAudit);

                                        await _context.SaveChangesAsync(cancellationToken);

                                        foreach (string str in audit)
                                        {
                                            AuditTrailDetail dtlAudit = new AuditTrailDetail
                                            {
                                                FormColName = "PARAMETER",
                                                FieldName = "PARAMETER",
                                                FormSeq = 1,
                                                HeaderId = hdrAudit.UniqueId,
                                                ValueAfter = str,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            };
                                            _context.AuditTrailDetails.Add(dtlAudit);
                                        }
                                        await _context.SaveChangesAsync(cancellationToken);

                                        trans.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trans.Rollback();

                                        throw ex;
                                    }
                                }
                            }//ctx
                        }
                    }
                    else
                    {
                        err = "Please chooose process to run";
                    }
                }
                else
                {
                    err = "Please add date filter before run process";
                }
            }
            else
            {
                err = "Please add date filter before run process";
            }
            ftr = null;


            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});

            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

        private async Task<string> CheckLockFormCode(CommandsModel<RunProcessTransformCommand, StatusModel> req, FilterModel ftr, MODULEDbContext ctx, CancellationToken cancellationToken)
        {
            string errorLocked = string.Empty;

            var listProcNo = req.CommandModel.select.Split('$').ToList();
            listProcNo.RemoveAt(listProcNo.Count - 1);
            listProcNo.RemoveAt(0);

            //var lockedProcess = await (from a in ctx.TransformFormMappings
            //                           join b in ctx.LockDatas on a.FormCode equals b.FormCode
            //                           where
            //                           listProcNo.Contains(a.ProcessNo)
            //                           && b.ReportDate == Convert.ToDateTime(ftr.value)
            //                           select new
            //                           {
            //                               a.FormCode,
            //                               a.ProcessNo,
            //                               FormName = this.GetFormName(a.FormCode)
            //                           })
            //                               .ToListAsync(cancellationToken);
            var lockedProcess = await (from a in ctx.TransformFormMappings
                                       join b in ctx.LockDatas on a.FormCode equals b.FormCode
                                       where
                                       listProcNo.Contains(a.ProcessNo)
                                       && b.ReportDate == Convert.ToDateTime(ftr.value)
                                       select new
                                       {
                                           a.FormCode,
                                           a.ProcessNo,
                                           FormName = ""
                                       })
                                           .ToListAsync(cancellationToken);

            foreach (var item in lockedProcess)
            {
                errorLocked += "Process No " + item.ProcessNo + ", " + item.FormCode + " - " + this.GetFormName(item.FormCode) + " (Cannot transform locked data) \r\n";
            }

            return errorLocked;
        }

        private string GetFormName(string formCode)
        {
            return _context.FormHeaders.Where(f => f.FormCode == formCode).Select(f => f.FormName).FirstOrDefault();
        }
    }
}