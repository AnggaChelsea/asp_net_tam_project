﻿namespace ARRA.GLOBAL.App.Forms.Customs.Transform.Commands
{
    public class ExportDataTransformCommand
    {
        public string formCode { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
        public string fileType { get; set; }
        public long jobId { get; set; }
    }
}