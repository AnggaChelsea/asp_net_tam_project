﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Queries
{
    public class GetMenuGroupReviseDataQuery
    {
        public long apvId { get; set; }
        public string formCode { get; set; }
    }
}