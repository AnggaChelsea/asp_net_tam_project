﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Models;
using ARRA.Common.Enumerations;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Queries
{
    public class GetMenuGroupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetMenuGroupQuery, MenuGroupListModel>, MenuGroupListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetMenuGroupQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<MenuGroupListModel> Handle(QueriesModel<GetMenuGroupQuery, MenuGroupListModel> req, CancellationToken cancellationToken)
        {
            string groupId = req.QueryModel.filter.Where(f => f.field == FieldCollection.GROUP_ID.ToString()).Select(c=>c.value).FirstOrDefault();
            string module = req.QueryModel.filter.Where(f => f.field == FieldCollection.MODULE.ToString()).Select(c => c.value).FirstOrDefault();
            MenuGroupListModel vm = new MenuGroupListModel();
            if (!string.IsNullOrEmpty(module))
            {
                IList<MenuGroupModel> list =
                    await (from mu in _context.Menus
                           join mg in _context.MenuGroupDetails.Where(f=>f.GroupId==groupId)
                            on mu.MenuCode equals mg.MenuCode
                           into t_menu_gp
                           from xmg in t_menu_gp.DefaultIfEmpty()
                           where mu.Module == module && mu.Status==true
                           select new MenuGroupModel
                           {
                               uid = xmg.UniqueId,
                               menuCode = mu.MenuCode,
                               menuName = mu.MenuName,
                               parentId = string.IsNullOrEmpty(mu.ParentMenuCode) ? "" : mu.ParentMenuCode,
                               view = xmg.AllowView,
                               add = xmg.AllowInsert,
                               edit = xmg.AllowUpdate,
                               remove = xmg.AllowDelete,
                               export = xmg.AllowExport,
                               import = xmg.AllowImport,
                               approval = xmg.AllowApproval,
                               enview = true,
                               enadd = mu.FeatureInsert,
                               enedit = mu.FeatureUpdate,
                               enremove = mu.FeatureDelete,
                               enimport = mu.FeatureImport,
                               enexport = mu.FeatureExport,
                               enapproval = mu.FeatureApproval,
                               seq = mu.MenuSeq,
                               hasurl = string.IsNullOrEmpty(mu.MenuUrl) ? "0":"1"
                           }).ToListAsync(cancellationToken);
                vm.data = generateTree(list);

                list = null;
            }
            else
            {
                throw new Exceptions.CustomException(ErrorMessages.GroupIdModuleRequired);
            }

            return vm;
        }

        private IList<MenuGroupModel> generateTree(IList<MenuGroupModel> list)
        {
            IList<MenuGroupModel> p = list.Where(f => f.parentId == "").OrderBy(o=>o.seq).ToList();
            foreach (MenuGroupModel i in p)
            {
                IList<MenuGroupModel> v1 = list.Where(f => f.parentId == i.menuCode).OrderBy(o=>o.seq).ToList();
                i.childs = v1;
                //i.state = TreeState.show.ToString();
                foreach (MenuGroupModel i1 in v1)
                {
                    IList<MenuGroupModel> v2 = list.Where(f => f.parentId == i1.menuCode).OrderBy(o => o.seq).ToList();
                    i1.childs = v2;
                    v2 = null;
                }
                v1 = null;
            }

            return p;
        }
    }
}
