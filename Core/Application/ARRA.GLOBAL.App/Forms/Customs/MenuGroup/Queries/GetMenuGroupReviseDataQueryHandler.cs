﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Queries
{
    public class GetMenuGroupReviseDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetMenuGroupReviseDataQuery, MenuGroupReviseModel>, MenuGroupReviseModel>
    {
        private readonly GLOBALDbContext _context;

        public GetMenuGroupReviseDataQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<MenuGroupReviseModel> Handle(QueriesModel<GetMenuGroupReviseDataQuery, MenuGroupReviseModel> req, CancellationToken cancellationToken)
        {
            IList<dynamic> result = new List<dynamic>();
            IList<TMP_MG_APV> hdrs = await _context.ApprovalHeaders.Where(f => f.LinkToHeaderId == req.QueryModel.apvId && f.CreatedBy==req.UserIdentity.UserId)
                .Select(c=>new TMP_MG_APV{
                    UID = c.UniqueId,
                    RefId = c.RefferalId
                }).ToListAsync(cancellationToken);
            int totalCount = hdrs.Count;
            if (totalCount > 0) //has data and access
            {
                JObject objJs = null;
                IList<ApprovalDetail> dtls = null;
                foreach (TMP_MG_APV l in hdrs)
                {
                    dtls = await _context.ApprovalDetails.Where(f => f.HeaderId == l.UID).OrderBy(o => o.FormSeq)
                        .Select(c => new ApprovalDetail
                        {
                            FieldName = c.FieldName,
                            ValueBefore = c.ValueBefore,
                            ValueAfter = c.ValueAfter
                        }).ToListAsync(cancellationToken);

                    if (dtls.Count > 0)
                    {
                        objJs = new JObject();
                        objJs.Add("uid", l.RefId);
                        foreach (ApprovalDetail dtl in dtls)
                        {
                            string col = GetFrontEndJsonModel(dtl.FieldName);
                            if (col != "")
                            {
                                objJs.Add(col, dtl.ValueAfter);
                            }
                        }
                        result.Add(objJs.ToObject<dynamic>());
                        objJs = null;
                    }
                    dtls = null;
                }
            }
            hdrs = null;

            return new MenuGroupReviseModel
            {
                data = result,
            };
        }

        private string GetFrontEndJsonModel(string dbfield)
        {
            string result = "";
            if(dbfield== "MENU_CD")
            {
                result = "menu";
            }
            else if (dbfield == "ALLOW_VIEW")
            {
                result = "view";
            }
            else if (dbfield == "ALLOW_INS")
            {
                result = "add";
            }
            else if (dbfield == "ALLOW_UPD")
            {
                result = "edit";
            }
            else if (dbfield == "ALLOW_DEL")
            {
                result = "remove";
            }
            else if (dbfield == "ALLOW_EXP")
            {
                result = "export";
            }
            else if (dbfield == "ALLOW_IMP")
            {
                result = "import";
            }
            else if (dbfield == "ALLOW_APVL")
            {
                result = "approval";
            }
            return result;
        }
    }
    public class TMP_MG_APV
    {
        public long UID { get; set; }
        public long? RefId { get; set; }

    }
}

