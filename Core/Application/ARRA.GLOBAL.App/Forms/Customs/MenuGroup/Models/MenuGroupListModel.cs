﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Models
{
    public class MenuGroupListModel
    {
        public IList<MenuGroupModel> data { get; set; }
    }
}