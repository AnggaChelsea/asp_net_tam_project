﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Models
{
    public class MenuGroupModel
    {
        public int? uid { get; set; }
        public string menuCode { get; set; }
        public string menuName { get; set; }
        public string parentId { get; set; }
        public int? seq { get; set; }
        public bool? view { get; set; }
        public bool? add { get; set; }
        public bool? edit { get; set; }
        public bool? remove { get; set; }
        public bool? import { get; set; }
        public bool? export { get; set; }
        public bool? approval { get; set; }
        public bool? enadd { get; set; }
        public bool? enview { get; set; }
        public bool? enedit { get; set; }
        public bool? enremove { get; set; }
        public bool? enimport { get; set; }
        public bool? enexport { get; set; }
        public bool? enapproval { get; set; }
        public string state { get; set; }
        public string hasurl { get; set; }
        public IList<MenuGroupModel> childs { get; set; }
    }
}