﻿namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class CreateMGRvsExportTaskCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string groupId { get; set; }
    }
}