﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class SaveMenuGroupCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }
        public string groupId { get; set; }
    }
}