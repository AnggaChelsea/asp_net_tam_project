﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Newtonsoft.Json;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class ExportMGRvsDataCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportMGRvsDataCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        private readonly ITextFileConnector _textfile;
        public ExportMGRvsDataCommandHandler(
            GLOBALDbContext context, IExcelConnector excel, ITextFileConnector textfile) : base(context)
        {
            _context = context;
            _excel = excel;
            _textfile = textfile;
        }

        public async Task<StatusModel> Handle(CommandsModel<ExportMGRvsDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string status = CommandStatus.Success.ToString();
            string message = "";
            long downloadId = 0;
            try
            {
                IList<dynamic> exps = await (from mu in _context.Menus
                                                join xmg in _context.MenuGroupDetails
                                                    on mu.MenuCode equals xmg.MenuCode
                                                //into t_menu_gp
                                                //from xmg in t_menu_gp.DefaultIfEmpty()
                                                where mu.Status == true
                                                    && xmg.GroupId == req.CommandModel.groupId
                                                orderby mu.Module, mu.ParentMenuCode, mu.MenuSeq
                                                select new TMP_MG_EXP
                                                {
                                                    UID = mu.UniqueId,
                                                    MODULE = mu.Module,
                                                    MENU_CD = mu.MenuCode,
                                                    PARENT_MENU_CD = mu.ParentMenuCode,
                                                    MENU_NM = mu.MenuName,
                                                    GROUP_ID = xmg.GroupId,
                                                    ALLOW_VW = xmg.AllowView==true?"Yes":"No",
                                                    ALLOW_INS = xmg.AllowInsert == true? "Yes" : "No",
                                                    ALLOW_UPD = xmg.AllowUpdate == true? "Yes" : "No",
                                                    ALLOW_DEL = xmg.AllowDelete == true? "Yes" : "No",
                                                    ALLOW_EXP = xmg.AllowExport == true? "Yes" : "No",
                                                    ALLOW_IMP = xmg.AllowImport == true? "Yes" : "No",
                                                    ALLOW_APVL = xmg.AllowApproval == true? "Yes" : "No"
                                                }).ToListAsync<dynamic>(cancellationToken);
                long totalRecord = exps.Count;

                //get form info
                TMP_FORM_INFO form = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode)
                    .Select(c => new TMP_FORM_INFO
                    {
                        Module = c.Module,
                        Delimited = c.TFDelimtedChar
                    }).FirstOrDefaultAsync(cancellationToken);
                IList<FormDetail> listColumns = await _context.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && (f.GrdColumnShow == true || f.EdrShow == true)).OrderBy(s => s.GrdColumnSeq).ToListAsync(cancellationToken);

                //Get File Path
                string filePath = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                    .Select(c=>c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                string fileName = req.CommandModel.formCode + "-" + DateTime.Now.Ticks.ToString();
                if (req.CommandModel.fileType == FileType.EXCEL.ToString())
                {
                    fileName += ".xlsx";
                    _excel.WriteExcel(exps, listColumns, filePath + fileName);
                }
                else if (req.CommandModel.fileType == FileType.CSV.ToString())
                {
                    fileName += ".csv";
                    _textfile.WriteCSV(exps, listColumns, filePath + fileName);
                }
                else if (req.CommandModel.fileType == FileType.DELIMITED.ToString())
                {
                    fileName += ".txt";
                    _textfile.WriteDelimited(exps, listColumns, filePath + fileName, form.Delimited);
                }
                else if (req.CommandModel.fileType == FileType.FIXLENGTH.ToString())
                {
                    fileName += ".txt";
                    _textfile.WriteFixLength(exps, listColumns, filePath + fileName);
                }
                listColumns = null;

                //save to download list.
                var exportlist = new ExportFileList
                {
                    UserId = req.UserIdentity.UserId,
                    Module = form.Module,
                    FormCode = req.CommandModel.formCode,
                    JobQueueId = 0,
                    FileName = fileName,
                    TotalRecord = totalRecord,
                    TotalInserted = totalRecord,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                _context.ExportFileLists.Add(exportlist);
                await _context.SaveChangesAsync(cancellationToken);
                form = null;
                exps = null;

                downloadId = exportlist.UniqueId;

            }
            catch (Exception ex)
            {
                status = CommandStatus.Failed.ToString();
                message = ex.Message;
            }

            return new StatusModel
            {
                status = status,
                message = message,
                id = downloadId
            };
        }
    }
    public class TMP_MG_EXP
    {

        public long UID { get; set; }
        public string GROUP_ID { get; set; }
        public string MENU_CD { get; set; }
        public string MENU_NM { get; set; }
        public string ALLOW_VW { get; set; }
        public string ALLOW_INS { get; set; }
        public string ALLOW_UPD { get; set; }
        public string ALLOW_DEL { get; set; }
        public string ALLOW_EXP { get; set; }
        public string ALLOW_IMP { get; set; }
        public string ALLOW_APVL { get; set; }
        public string MODULE { get; set; }
        public string PARENT_MENU_CD { get; set; }
    }
    public class TMP_FORM_INFO
    {
        public string Module { get; set; }
        public string Delimited { get; set; }
    }
}