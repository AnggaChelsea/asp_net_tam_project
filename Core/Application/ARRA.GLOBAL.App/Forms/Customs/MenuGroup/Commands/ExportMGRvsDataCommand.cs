﻿namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class ExportMGRvsDataCommand
    {
        public string formCode { get; set; }
        public string groupId { get; set; }
        public string fileType { get; set; }
    }
}