﻿using System.Collections.Generic;
namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class SaveMenuGroupReviseCommand
    {
        public string formCode { get; set; }
        public string action { get; set; }
        public dynamic data { get; set; }
        public List<dynamic> details { get; set; }
        public string groupId { get; set; }
        public long uid { get; set; }
    }
}