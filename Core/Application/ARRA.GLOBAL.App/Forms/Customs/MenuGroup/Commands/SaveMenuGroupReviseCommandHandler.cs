﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class SaveMenuGroupReviseCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveMenuGroupReviseCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveMenuGroupReviseCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveMenuGroupReviseCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if (menu != null)
            {
                FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
                if (form == null)
                {
                    throw new NotFoundException(nameof(FormHeader), req.CommandModel.formCode);
                }

                IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == req.CommandModel.formCode && f.EdrShow == true).ToListAsync(cancellationToken);
                if (formDetail.Count > 0)
                {
                    ApprovalHeader apvHdr = await _context.ApprovalHeaders.Where(f => f.UniqueId == req.CommandModel.uid).FirstOrDefaultAsync(cancellationToken);
                    if (apvHdr.CreatedBy == req.UserIdentity.UserId) //is valid user
                    {
                        using (var trans = _context.Database.BeginTransaction())
                        {
                            try
                            {
                                apvHdr.TaskType = TaskType.Approval.ToString();
                                apvHdr.ModifiedBy = req.UserIdentity.UserId;
                                apvHdr.ModifiedDate = req.CurrentDateTime;
                                apvHdr.ModifiedHost = req.UserIdentity.Host;

                                IList<ApprovalDetail> apvDtl = await _context.ApprovalDetails.Where(f => f.HeaderId == apvHdr.UniqueId).ToListAsync(cancellationToken);
                                _context.ApprovalDetails.RemoveRange(apvDtl);

                                //save header
                                var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
                                Int64 headerid = apvHdr.UniqueId;
                                foreach (FormDetail item in formDetail)
                                {
                                    JToken value, valueBefore;
                                    jObject.TryGetValue(item.FieldName, out value);
                                    jObject.TryGetValue(item.FieldName + "_before", out valueBefore);
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = Utility.SafeSqlString(Convert.ToString(value)),
                                        ValueBefore = Convert.ToString(valueBefore),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }
                                apvDtl = null;
                                await _context.SaveChangesAsync(cancellationToken);

                                //save detail
                                IList<ApprovalHeader> dApvHdr = await _context.ApprovalHeaders.Where(f => f.LinkToHeaderId == req.CommandModel.uid).ToListAsync(cancellationToken);
                                IList<long> hdrs = dApvHdr.Select(c => c.UniqueId).ToList();
                                IList<ApprovalDetail> dApvDtl = await _context.ApprovalDetails.Where(f => hdrs.Contains(f.HeaderId)).ToListAsync(cancellationToken);
                                _context.ApprovalDetails.RemoveRange(dApvDtl);
                                _context.ApprovalHeaders.RemoveRange(dApvHdr);
                                await _context.SaveChangesAsync(cancellationToken);
                                hdrs = null;
                                dApvHdr = null;
                                dApvDtl = null;
                                if (!string.IsNullOrEmpty(form.LinkToFormCode))
                                {
                                    IList<MenuGroupDetail> ls = await _context.MenuGroupDetails.Where(f => f.GroupId == req.CommandModel.groupId).ToListAsync(cancellationToken);
                                    if (req.CommandModel.details.Count > 0)
                                    {
                                        foreach (dynamic item in req.CommandModel.details)
                                        {
                                            if (item != null)
                                            {
                                                jObject = ((Newtonsoft.Json.Linq.JObject)item);

                                                JToken view, refid;
                                                jObject.TryGetValue("view", out view);
                                                jObject.TryGetValue("uid", out refid);
                                                ApprovalHeader mHeader = new ApprovalHeader
                                                {
                                                    MenuCode = menu.MenuCode,
                                                    MenuName = menu.MenuName,
                                                    ActionType = Convert.ToBoolean(view) ? OperationType.REPLACE.ToString() : OperationType.REMOVE.ToString(),
                                                    ApprovalStatus = ApprovalStatus.Pending.ToString(),
                                                    Module = menu.Module,
                                                    RefferalId = refid.ToString() == "" ? 0 : Convert.ToInt64(refid),
                                                    TaskType = TaskType.Approval.ToString(),
                                                    FormCode = form.LinkToFormCode,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host,
                                                    LinkToHeaderId = headerid,
                                                };
                                                _context.ApprovalHeaders.Add(mHeader);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                Int64 headeriddetail = mHeader.UniqueId;
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "GROUP_ID",
                                                    FormColName = "Group Id",
                                                    FormSeq = 1,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.groupId)),
                                                    ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.groupId)),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host,
                                                    KeyConds = true
                                                });

                                                JToken value;
                                                jObject.TryGetValue("menu", out value);

                                                MenuGroupDetail valBefore = ls.Where(f => f.GroupId == req.CommandModel.groupId && f.MenuCode == Convert.ToString(value)).FirstOrDefault();
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "MENU_CD",
                                                    FormColName = "Menu Code",
                                                    FormSeq = 2,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)),
                                                    ValueBefore = Utility.SafeSqlString(Convert.ToString(value)),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host,
                                                    KeyConds = true
                                                });

                                                jObject.TryGetValue("view", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_VIEW",
                                                    FormColName = "Allow View",
                                                    FormSeq = 3,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowView).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("add", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_INS",
                                                    FormColName = "Allow Add",
                                                    FormSeq = 4,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowInsert).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("edit", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_UPD",
                                                    FormColName = "Allow Edit",
                                                    FormSeq = 5,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowUpdate).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("remove", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_DEL",
                                                    FormColName = "Allow Remove",
                                                    FormSeq = 6,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowDelete).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("export", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_EXP",
                                                    FormColName = "Allow Export",
                                                    FormSeq = 7,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowExport).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("import", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_IMP",
                                                    FormColName = "Allow Import",
                                                    FormSeq = 8,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowImport).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });
                                                jObject.TryGetValue("approval", out value);
                                                _context.ApprovalDetails.Add(new ApprovalDetail
                                                {
                                                    FieldName = "ALLOW_APVL",
                                                    FormColName = "Allow Approval",
                                                    FormSeq = 9,
                                                    HeaderId = headeriddetail,
                                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                    ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowApproval).ToLower(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                });

                                                await _context.SaveChangesAsync(cancellationToken);
                                            }
                                        }
                                    }
                                    ls = null;
                                }
                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        throw new CustomException(ErrorMessages.NotAllowSave);
                    }
                    apvHdr = null;
                }
                else
                {
                    throw new NotFoundException("Menu not found", "not found");
                }
                formDetail = null;
                form = null;
            }
            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }     
    }
}