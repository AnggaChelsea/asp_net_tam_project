﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class CreateMGRvsExportTaskCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateMGRvsExportTaskCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public CreateMGRvsExportTaskCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateMGRvsExportTaskCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }

            long downloadId = 0;
            CommandsModel<ExportMGRvsDataCommand, StatusModel> cmd = new CommandsModel<ExportMGRvsDataCommand, StatusModel>();
            cmd.UserIdentity = new UserIdentityModel
            {
                UserId = req.UserIdentity.UserId,
                Host = req.UserIdentity.Host
            };
            cmd.CurrentDateTime = req.CurrentDateTime;
            cmd.CommandModel = new ExportMGRvsDataCommand
            {
                fileType = req.CommandModel.fileType,
                groupId = req.CommandModel.groupId,
                formCode = req.CommandModel.formCode
            };
            StatusModel stsM = await _mediator.Send(cmd);
            if (stsM != null)
            {
                downloadId = stsM.id;
            }
            cmd = null;
            stsM = null;
            form = null;
            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
                id = downloadId
            };
        }
    }
}