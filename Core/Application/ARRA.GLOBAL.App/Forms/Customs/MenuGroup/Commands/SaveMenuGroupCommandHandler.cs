﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;

namespace ARRA.GLOBAL.App.Forms.Customs.MenuGroup.Commands
{
    public class SaveMenuGroupCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveMenuGroupCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveMenuGroupCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveMenuGroupCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if (menu != null)
            {
                FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
                if (form == null)
                {
                    throw new NotFoundException(nameof(FormHeader), req.CommandModel.formCode);
                }

                IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                if (formDetail.Count > 0)
                {
                    Int64 headerid = 0;
                    using (var trans = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            //save header
                            var jObject = ((Newtonsoft.Json.Linq.JObject)req.CommandModel.data);
                            JToken action, uid;
                            jObject.TryGetValue("screenActionType", out action);
                            
                            //QA request.
                            if (base.GetActionType(Convert.ToString(action)) == OperationType.REMOVE.ToString())
                            {
                                JToken groupId;
                                jObject.TryGetValue("GROUP_ID", out groupId);
                                if (groupId != null)
                                {
                                    int exists = await _context.Users.Where(f => f.Status == true && f.MenuGroup == Convert.ToString(groupId)).CountAsync(cancellationToken);
                                    if (exists > 0)
                                    {
                                        throw new CustomException(ErrorMessages.GroupIsUsed);
                                    }
                                }
                            }

                            jObject.TryGetValue("UID", out uid);
                            if (Convert.ToString(uid) == "") { uid = "0"; }
                            ApprovalHeader mHeader = new ApprovalHeader
                            {
                                MenuCode = menu.MenuCode,
                                MenuName = menu.MenuName,
                                ActionType = base.GetActionType(Convert.ToString(action)),
                                ApprovalStatus = ApprovalStatus.Pending.ToString(),
                                Module = menu.Module,
                                RefferalId = Convert.ToInt64(uid),
                                TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                                FormCode = req.CommandModel.formCode,
                                CreatedBy = req.UserIdentity.UserId,
                                CreatedDate = req.CurrentDateTime,
                                CreatedHost = req.UserIdentity.Host
                            };

                            _context.ApprovalHeaders.Add(mHeader);

                            await _context.SaveChangesAsync(cancellationToken);

                            headerid = mHeader.UniqueId;
                            foreach (FormDetail item in formDetail)
                            {
                                JToken value, valueBefore;
                                jObject.TryGetValue(item.FieldName, out value);
                                jObject.TryGetValue(item.FieldName + "_before", out valueBefore);

                                _context.ApprovalDetails.Add(new ApprovalDetail
                                {
                                    FieldName = item.FieldName,
                                    FormColName = item.GrdColumnName,
                                    FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                    HeaderId = headerid,
                                    ValueAfter = Utility.SafeSqlString(Convert.ToString(value)),
                                    ValueBefore = Convert.ToString(valueBefore),
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                });
                            }

                            await _context.SaveChangesAsync(cancellationToken);

                            if (!string.IsNullOrEmpty(form.LinkToFormCode))
                            {
                                IList<MenuGroupDetail> ls = await _context.MenuGroupDetails.Where(f => f.GroupId == req.CommandModel.groupId).ToListAsync(cancellationToken);
                                //detail
                                if (req.CommandModel.details.Count > 0)
                                {
                                    foreach (dynamic item in req.CommandModel.details)
                                    {
                                        if (item != null)
                                        {
                                            jObject = ((Newtonsoft.Json.Linq.JObject)item);

                                            JToken view, refid;
                                            jObject.TryGetValue("view", out view);
                                            jObject.TryGetValue("uid", out refid);

                                            mHeader = new ApprovalHeader
                                            {
                                                MenuCode = menu.MenuCode,
                                                MenuName = menu.MenuName,
                                                //ActionType = ARRA.Common.ConvertValue.ToBoolean(view) ? OperationType.REPLACE.ToString() : OperationType.REMOVE.ToString(),
                                                ActionType = OperationType.REPLACE.ToString(),
                                                ApprovalStatus = ApprovalStatus.Pending.ToString(),
                                                Module = menu.Module,
                                                RefferalId = refid.ToString() == "" ? 0 : Convert.ToInt64(refid),
                                                TaskType = TaskType.Approval.ToString(),
                                                FormCode = form.LinkToFormCode,
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                LinkToHeaderId = headerid,
                                            };
                                            _context.ApprovalHeaders.Add(mHeader);

                                            await _context.SaveChangesAsync(cancellationToken);

                                            Int64 headeriddetail = mHeader.UniqueId;

                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "GROUP_ID",
                                                FormColName = "Group Id",
                                                FormSeq = 1,
                                                HeaderId = headeriddetail,
                                                ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.groupId)),
                                                ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.groupId)),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                KeyConds = true
                                            });

                                            JToken value;
                                            jObject.TryGetValue("menu", out value);

                                            MenuGroupDetail valBefore = ls.Where(f => f.GroupId == req.CommandModel.groupId && f.MenuCode == Convert.ToString(value)).FirstOrDefault();
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "MENU_CD",
                                                FormColName = "Menu Code",
                                                FormSeq = 2,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)),
                                                ValueBefore = Utility.SafeSqlString(Convert.ToString(value)),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host,
                                                KeyConds = true
                                            });

                                            jObject.TryGetValue("view", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_VIEW",
                                                FormColName = "Allow View",
                                                FormSeq = 3,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowView).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("add", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_INS",
                                                FormColName = "Allow Add",
                                                FormSeq = 4,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowInsert).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("edit", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_UPD",
                                                FormColName = "Allow Edit",
                                                FormSeq = 5,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowUpdate).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("remove", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_DEL",
                                                FormColName = "Allow Remove",
                                                FormSeq = 6,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowDelete).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("export", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_EXP",
                                                FormColName = "Allow Export",
                                                FormSeq = 7,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowExport).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("import", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_IMP",
                                                FormColName = "Allow Import",
                                                FormSeq = 8,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowImport).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });
                                            jObject.TryGetValue("approval", out value);
                                            _context.ApprovalDetails.Add(new ApprovalDetail
                                            {
                                                FieldName = "ALLOW_APVL",
                                                FormColName = "Allow Approval",
                                                FormSeq = 9,
                                                HeaderId = headeriddetail,
                                                ValueAfter = Utility.SafeSqlString(Convert.ToString(value)).ToLower(),
                                                ValueBefore = valBefore == null ? "" : Convert.ToString(valBefore.AllowApproval).ToLower(),
                                                CreatedBy = req.UserIdentity.UserId,
                                                CreatedDate = req.CurrentDateTime,
                                                CreatedHost = req.UserIdentity.Host
                                            });

                                            await _context.SaveChangesAsync(cancellationToken);
                                        }
                                    }
                                }
                                ls = null;
                            }

                            


                            trans.Commit();
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            throw ex;
                        }

                        try
                        {
                            //cek has approval atau tidak.
                            if (!req.AccessMatrix.AllowApproval)
                            {
                                DynamicParameters p = new DynamicParameters();
                                p.Add("@APPV_ID", headerid);
                                p.Add("@USR_ID", req.UserIdentity.UserId);
                                p.Add("@HOST", req.UserIdentity.Host);
                                p.Add("@APPROVAL", req.AccessMatrix.AllowApproval);
                                await _context.Database.GetDbConnection().ExecuteAsync("DBO.UDPU_EXECUTE_CRUD", p, null, null, CommandType.StoredProcedure);
                                p = null;
                            }
                        }
                        catch (SqlException ex)
                        {
                            throw new DatabaseException(ex.Number, ex.Message);
                        }
                    }
                }
            }
            else
            {
                throw new NotFoundException("Menu not found", "not found");
            }


            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }

    }
}