﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.Common;
using ARRA.Common.Enumerations;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.Forms.Customs.Reconcilition.Commands
{
    public class ReconCommandHandler : AppBase, IRequestHandler<CommandsModel<ReconCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public ReconCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<ReconCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            FormBranchFailListModel vm = new FormBranchFailListModel();
            vm.errors = new List<FormBranchFailModel>();

            //get filter
            string strDate = "";
            string strPeriod = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString());

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                strDate = ftr.value;
            }
            ftr = req.CommandModel.filter.Where(f => f.field == FieldCollection.PERIOD_TP.ToString()).FirstOrDefault();
            if (ftr != null)
            {
                strPeriod = ftr.value;
            }
            ftr = null;

            if (!string.IsNullOrEmpty(strDate) && !string.IsNullOrEmpty(strPeriod))
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
                {
                    DateTime rptDate = Convert.ToDateTime(strDate);
                    int exists = await ctx.ProcessLogHeaders.Where(f => f.DataDate == rptDate &&
                        f.PeriodType == strPeriod && f.ProcessType == QueueJobType.RECONCILIATION.ToString()
                        && (f.ProcessStatus == JobStatus.Progress.ToString() || f.ProcessStatus == JobStatus.Cancelling.ToString())
                    ).CountAsync(cancellationToken);

                    if (exists == 0)
                    {
                        ARRA.GLOBAL.Domain.Entities.FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
                        ARRA.GLOBAL.Domain.Entities.Module module = await _context.Modules.FindAsync(form.Module);
                        List<string> listAllBranch = await (from br in _context.Branches
                                                            join br_a in _context.BranchGroupDetails
                                                             on br.BranchCode equals br_a.BranchCode
                                                            join cy in _context.BusinessParamDetails
                                                             on br.CountryCode equals cy.ParamValue
                                                            join rg in _context.RegulatorBranchs
                                                             on br.RegulatorBranch equals rg.BranchCode
                                                            where br_a.GroupId == req.UserIdentity.BranchGroup
                                                             && cy.ParamCode == "AN004"
                                                             && br.Unit == module.BusinessType
                                                             && rg.Unit == module.BusinessType
                                                            orderby rg.BranchCode
                                                            select rg.BranchCode
                                                            ).Distinct().ToListAsync(cancellationToken);

                        //List<string> listAllBranch = new List<string>();

                        using (var trans = ctx.Database.BeginTransaction())
                        {
                            try
                            {
                                ARRA.MODULE.Domain.Entities.JobQueueHeader jobHdr = new MODULE.Domain.Entities.JobQueueHeader
                                {
                                    JobType = QueueJobType.RECONCILIATION.ToString(),
                                    PeriodType = strPeriod,
                                    Status = JobStatus.Temp.ToString(),
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                };
                                ctx.JobQueueHeaders.Add(jobHdr);
                                await ctx.SaveChangesAsync(cancellationToken);

                                ARRA.MODULE.Domain.Entities.JobQueueDetail jobDtl = new MODULE.Domain.Entities.JobQueueDetail()
                                {
                                    HeaderId = jobHdr.UniqueId,
                                    ParamName = "REPORT_DATE",
                                    ParamValue = strDate,
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                };
                                ctx.JobQueueDetails.Add(jobDtl);

                                jobDtl = new MODULE.Domain.Entities.JobQueueDetail()
                                {
                                    HeaderId = jobHdr.UniqueId,
                                    ParamName = "PERIOD_TYPE",
                                    ParamValue = strPeriod,
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                };
                                ctx.JobQueueDetails.Add(jobDtl);

                                foreach (string branch in listAllBranch)
                                {
                                    jobDtl = new MODULE.Domain.Entities.JobQueueDetail()
                                    {
                                        HeaderId = jobHdr.UniqueId,
                                        ParamName = "BRANCH",
                                        ParamValue = branch,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    ctx.JobQueueDetails.Add(jobDtl);

                                    ARRA.MODULE.Domain.Entities.ProcessLogHeader hdrLogs = new MODULE.Domain.Entities.ProcessLogHeader()
                                    {
                                        DataDate = rptDate,
                                        PeriodType = strPeriod,
                                        Branch = branch,
                                        ProcessNo = "",
                                        ProcessType = QueueJobType.RECONCILIATION.ToString(),
                                        ProcessName = ProcessName.Reconciliation.ToString(),
                                        QueueId = jobHdr.UniqueId,
                                        ProcessStatus = JobStatus.Progress.ToString(),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    ctx.ProcessLogHeaders.Add(hdrLogs);
                                }

                                jobHdr.Status = JobStatus.Pending.ToString();

                                await ctx.SaveChangesAsync(cancellationToken);

                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();

                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        throw new CustomException(ErrorMessages.ReconciliationExists);
                    }
                }//ctx
            }
            else
            {
                throw new CustomException(ErrorMessages.AddFilterDatePeriod);
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}