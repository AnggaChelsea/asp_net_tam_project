﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Commands
{
    public class DownloadTFCommandHandler : AppBase, IRequestHandler<CommandsModel<DownloadTFCommand, FileListModel>, FileListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public DownloadTFCommandHandler(
            GLOBALDbContext context,IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<FileListModel> Handle(CommandsModel<DownloadTFCommand, FileListModel> req, CancellationToken cancellationToken)
        {
            FileListModel vm = new FileListModel();

            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();

            // 20221109: req user permata, bisa input date lain
            var ftrs = req.CommandModel.filter.Where(f => f.field == "REPORT_DT");

            foreach (var item in ftrs)
            {
                if (item.value != null && item.value != "")
                {
                    ftr = item;
                    break;
                }
            }
            
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.select) == false)
                    {
                        List<Int64> select = req.CommandModel.select.Split('$').Where(f=>f!="").Select(Int64.Parse).ToList();
                        if (select.Count > 0)
                        {
                            try
                            {
                                using (MODULEDbContext ctx = base.GetDbContext(req.AccessMatrix.Module))
                                {
                                    vm.files = await ctx.GenerateTextFiles.Where(f => select.Contains(f.UniqueId) && f.Active==true)
                                        .Select(c=>c.FileName)
                                        .ToListAsync(cancellationToken);

                                    string path = await _context.SystemParameterHeaders
                                        .Where(f => f.Module == req.AccessMatrix.Module 
                                            && f.ParamName == SystemParameterType.TEXT_FILE_LOCATION.ToString())
                                        .Select(c=>c.ParamValue)
                                        .FirstOrDefaultAsync(cancellationToken);

                                    vm.filePath = path + Convert.ToDateTime(ftr.value).ToString("yyyyMMdd") + @"\";

                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        err = "Please chooose process to cancel";
                    }
                }
                else
                {
                    err = "Please add date filter before run cancel";
                }
            }
            else
            {
                err = "Please add date filter before run cancel";
            }
            ftr = null;

            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return vm;
        }
    }
}