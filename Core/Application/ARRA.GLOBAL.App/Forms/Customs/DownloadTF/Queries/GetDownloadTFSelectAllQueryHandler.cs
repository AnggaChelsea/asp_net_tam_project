﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using System.Linq.Dynamic.Core;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Queries
{
    public class GetDownloadTFSelectAllQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDownloadTFSelectAllQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetDownloadTFSelectAllQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetDownloadTFSelectAllQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();

            FilterModel ftr_ctr = null;

            var ctr_filter = req.QueryModel.filter;
            int index;
            List<int> IndexList = new List<int>();

            foreach (var a in ctr_filter)
            {
                if (a.field != ReportingDateField.REPORT_DT.ToString() || !string.IsNullOrEmpty(a.value))
                {
                    ftr_ctr = a;
                }

                index = req.QueryModel.filter.IndexOf(a);

                if (string.IsNullOrEmpty(a.value))
                {
                    IndexList.Add(index);
                }
            }

            foreach (var f in IndexList)
            {
                req.QueryModel.filter.RemoveAt(f);
            }

            ftr_ctr = req.QueryModel.filter.Where(f => f.field == ReportingDateField.REPORT_DT.ToString()).FirstOrDefault();

            if (ftr_ctr != null)
            {
                if (!string.IsNullOrEmpty(ftr_ctr.value))
                {
                    TMP_Form form = await _context.FormHeaders.Where(f => f.FormCode == req.QueryModel.formCode)
                                    .Select(c => new TMP_Form
                                    {
                                        Module = c.Module,
                                        Filter = c.FilterConditionEx
                                    })
                                    .FirstOrDefaultAsync(cancellationToken);

                    //get active only
                    req.QueryModel.filter.Add(new FilterModel { field = "ACTIVE", datatype = "BOOL", value = "1" });

                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), form.Filter);
                    //DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq((IList<FilterModel>)ftr_ctr), form.Filter);

                    using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                    {
                        listSelect = await ctx.GenerateTextFiles
                            .Select(c => new
                            {
                                uid = c.UniqueId,
                                REPORT_DT = c.ReportDate,
                                PERIOD_TP = c.PeriodType,
                                FORM_CD = c.OjkFormCode,
                                OJK_FORM_CD = c.OjkFormCode,
                                BRANCH_CD = c.BranchCode,
                                FILE_NM = c.FileName,
                                TOTAL_RECORD = c.TotalRecord,
                                CREATED_BY = c.CreatedBy,
                                CREATED_DT = c.CreatedDate,
                                ACTIVE = c.Active
                            })
                            .Where(objFtr.filterParams, objFtr.values)
                            .Select(c => c.uid.ToString())
                            .ToListAsync(cancellationToken);
                        objFtr = null;
                    }
                    form = null;
                    objFtr = null;
                }
            }

            return new SelectionListModel
            {
                data = listSelect
            };
        }
    }
    public class TMP_Form
    {
        public string Module { get; set; }
        public string Filter { get; set; }
    }
}