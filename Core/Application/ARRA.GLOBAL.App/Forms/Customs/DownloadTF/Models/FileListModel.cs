﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Models
{
    public class FileListModel
    {
        public IList<string> files { get; set; }
        public string filePath { get; set; }
    }
}