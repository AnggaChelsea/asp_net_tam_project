﻿namespace ARRA.GLOBAL.App.Forms.Customs.Process.Commands
{
    public class ExportDataDsCommand
    {
        public string formCode { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
        public string fileType { get; set; }
        public long jobId { get; set; }
    }
}