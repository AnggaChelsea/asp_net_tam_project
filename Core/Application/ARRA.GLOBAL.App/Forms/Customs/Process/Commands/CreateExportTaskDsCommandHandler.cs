﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Commands
{
    public class CreateExportTaskDsCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateExportTaskDsCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public CreateExportTaskDsCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<CreateExportTaskDsCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            long downloadId = 0;
            CommandsModel<ExportDataDsCommand, StatusModel> cmd = new CommandsModel<ExportDataDsCommand, StatusModel>();
            cmd.UserIdentity = new UserIdentityModel
            {
                UserId = req.UserIdentity.UserId,
                Host = req.UserIdentity.Host
            };
            cmd.CurrentDateTime = req.CurrentDateTime;
            cmd.CommandModel = new ExportDataDsCommand
            {
                fileType = req.CommandModel.fileType,
                filter = req.CommandModel.filter,
                formCode = req.CommandModel.formCode,
                sort = req.CommandModel.sort,
                jobId = 0
            };

            StatusModel stsM = await _mediator.Send(cmd);
            if (stsM != null)
            {
                downloadId = stsM.id;
            }
            cmd = null;
            stsM = null;

            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
                id = downloadId
            };
        }
    }
}