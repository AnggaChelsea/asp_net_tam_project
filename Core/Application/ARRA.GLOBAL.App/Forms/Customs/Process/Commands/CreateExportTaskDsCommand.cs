﻿namespace ARRA.GLOBAL.App.Forms.Customs.Process.Commands
{
    public class CreateExportTaskDsCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
    }
}