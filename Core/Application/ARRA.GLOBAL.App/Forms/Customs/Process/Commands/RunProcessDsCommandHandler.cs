﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Commands
{
    public class RunProcessDsCommandHandler : AppBase, IRequestHandler<CommandsModel<RunProcessDsCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RunProcessDsCommandHandler(
            GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RunProcessDsCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.select) == false)
                    {
                        string[] select = req.CommandModel.select.Split('$');
                        if (select.Length > 0)
                        {
                            using (var trans = _context.Database.BeginTransaction())
                            {
                                try
                                {
                                    IList<string> audit = new List<string>();
                                    IList<ARRA.GLOBAL.Domain.Entities.ProcessDataSource> dsList =
                                        await _context.ProcessDataSources.Where(f => select.Contains(f.ProcessNo)).ToListAsync(cancellationToken);

                                    for (int i = 0; i < select.Length; i++)
                                    {
                                        string procNo = select[i];
                                        if (!string.IsNullOrEmpty(procNo))
                                        {
                                            IList<ProcessLogHeader> list =
                                                await _context.ProcessLogHeaders.Where(f =>
                                                f.DataDate == Convert.ToDateTime(ftr.value)
                                                && f.ProcessNo == procNo
                                                ).ToListAsync(cancellationToken);

                                            if (list.Where(f => f.ProcessStatus == JobStatus.Pending.ToString()
                                                 || f.ProcessStatus == JobStatus.Progress.ToString()
                                                 || f.ProcessStatus == JobStatus.Cancelling.ToString()
                                            ).FirstOrDefault() == null)
                                            {
                                                //remove exists hist
                                                _context.ProcessLogHeaders.RemoveRange(list);

                                                ARRA.GLOBAL.Domain.Entities.ProcessDataSource ds = dsList.Where(f => f.ProcessNo == procNo).FirstOrDefault();
                                                
                                                //add job queue
                                                JobQueueHeader hdrQueue = new JobQueueHeader
                                                {
                                                    JobType = QueueJobType.PROCESS_DATA_SOURCE.ToString(),
                                                    PeriodType = ds.PeriodType,
                                                    Status = JobStatus.Pending.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.JobQueueHeaders.Add(hdrQueue);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                //add hist
                                                ProcessLogHeader hdrHist = new ProcessLogHeader
                                                {
                                                    DataDate = Convert.ToDateTime(ftr.value),
                                                    ProcessNo = procNo,
                                                    ProcessName = ds.ProcessName,
                                                    PeriodType = ds.PeriodType,
                                                    ProcessStatus = JobStatus.Progress.ToString(),
                                                    ProcessType = QueueJobType.PROCESS_DATA_SOURCE.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host,
                                                    QueueId = hdrQueue.UniqueId
                                                };

                                                _context.ProcessLogHeaders.Add(hdrHist);
                                                await _context.SaveChangesAsync(cancellationToken);

                                                JobQueueDetail hdrQueueD = new JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "DATA_DATE",
                                                    ParamValue = ftr.value,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.JobQueueDetails.Add(hdrQueueD);

                                                hdrQueueD = new JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "PROCESS_NO",
                                                    ParamValue = procNo,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.JobQueueDetails.Add(hdrQueueD);

                                                string filename = ds.FileName == null ? "" : ds.FileName.Replace("$", Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat));
                                                hdrQueueD = new JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "FILE_NAME",
                                                    ParamValue = filename,
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };

                                                _context.JobQueueDetails.Add(hdrQueueD);

                                                hdrQueueD = new JobQueueDetail
                                                {
                                                    HeaderId = hdrQueue.UniqueId,
                                                    ParamName = "JOB_ID",
                                                    ParamValue = hdrHist.UniqueId.ToString(),
                                                    CreatedBy = req.UserIdentity.UserId,
                                                    CreatedDate = req.CurrentDateTime,
                                                    CreatedHost = req.UserIdentity.Host
                                                };
                                                _context.JobQueueDetails.Add(hdrQueueD);

                                                await _context.SaveChangesAsync(cancellationToken);

                                                audit.Add("DATA_DATE=" + ftr.value + ", PROCESS_NO=" + procNo + ", FILE_NAME="+ filename + ", JOB_ID=" + hdrHist.UniqueId.ToString());
                                                hdrHist = null;
                                                hdrQueue = null;
                                                hdrQueueD = null;
                                            }

                                            list = null;
                                        }
                                    }

                                    AuditTrailHeader hdrAudit = new AuditTrailHeader
                                    {
                                        MenuCode = "GB_PROCESS_DATA_SOURCE",
                                        MenuName = "Process Data Source",
                                        Module = ModuleType.GLOBAL.ToString(),
                                        WithApproval = false,
                                        ActivityAction = AuditType.PROCESS.ToString(),
                                        UserId = req.UserIdentity.UserId,
                                        ActivityDate = req.CurrentDateTime,
                                        ActivityHost = req.UserIdentity.Host,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    _context.AuditTrailHeaders.Add(hdrAudit);

                                    await _context.SaveChangesAsync(cancellationToken);

                                    foreach (string str in audit)
                                    {
                                        AuditTrailDetail dtlAudit = new AuditTrailDetail
                                        {
                                            FormColName = "PARAMETER",
                                            FieldName = "PARAMETER",
                                            FormSeq = 1,
                                            HeaderId = hdrAudit.UniqueId,
                                            ValueAfter = str,
                                            CreatedBy = req.UserIdentity.UserId,
                                            CreatedDate = req.CurrentDateTime,
                                            CreatedHost = req.UserIdentity.Host
                                        };
                                        _context.AuditTrailDetails.Add(dtlAudit);
                                    }

                                    await _context.SaveChangesAsync(cancellationToken);

                                    trans.Commit();
                                }
                                catch(Exception ex)
                                {
                                    trans.Rollback();

                                    throw ex;
                                }
                            }
                        }
                    }
                    else
                    {
                        err = "Please chooose process to run";
                    }
                }
                else
                {
                    err = "Please add date filter before run process";
                }
            }
            else
            {
                err = "Please add date filter before run process";
            }
            ftr = null;


            if (err != "")
            {
                throw new CustomException(err);

                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});

            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}