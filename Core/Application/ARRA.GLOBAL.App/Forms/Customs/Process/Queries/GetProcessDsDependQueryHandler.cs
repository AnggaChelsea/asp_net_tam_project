﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using ARRA.Common.Enumerations;
using System.IO;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetProcessDsDependQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessDsDependQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetProcessDsDependQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetProcessDsDependQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    List<string> pars = new List<string>();
                    pars.Add("GB014_N");
                    pars.Add("GB015");
                    IList<SystemParameterHeader> hdrs = await _context.SystemParameterHeaders
                        .Where(ff => pars.Contains(ff.ParamCode)).ToListAsync(cancellationToken);
                    var find = hdrs.Where(ff => ff.ParamCode == "GB015").FirstOrDefault();
                    string folder = "";
                    if (find != null)
                    {
                        folder = find.ParamValue;
                    }
                    if (!string.IsNullOrEmpty(folder))
                    {
                        folder = Convert.ToDateTime(ftr.value).ToString(folder);
                    }
                    string path = hdrs.Where(ff => ff.ParamCode == "GB014_N").FirstOrDefault().ParamValue + folder + @"\";
                    pars = null;


                    IList<long> listid = await _context.ProcessLogHeaders
                                    .Where(
                                        f => f.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                        && f.ProcessType == QueueJobType.PROCESS_DATA_SOURCE.ToString()
                                     )
                                    .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                                ).ToListAsync<long>(cancellationToken);

                    _resultDepend = new List<string>();
                    _resultDepend.Add(req.QueryModel.procNo);
                    IList<TempDependModel> list =
                        await (from d in _context.ProcessDependeciess
                               join p in _context.ProcessDataSources
                                on d.ProcessNo equals p.ProcessNo
                               join h in _context.ProcessLogHeaders.Where(f=>listid.Contains(f.UniqueId))
                                on d.ProcessNo equals h.ProcessNo into t_log
                               from xhs in t_log.DefaultIfEmpty()
                               where d.ProcessType == QueueJobType.PROCESS_DATA_SOURCE.ToString()
                               select new TempDependModel
                               {
                                   procNo = d.ProcessNo,
                                   depend = d.DependProcessNo,
                                   sts = xhs.ProcessStatus,
                                   exists = p.UsingTF?File.Exists(path+ p.FileName.Replace("$",Convert.ToDateTime(ftr.value).ToString(p.FileNameDateFormat))):false,
                                   tf = p.UsingTF
                               }
                               ).ToListAsync(cancellationToken);

                    RecursiveCheck(req.QueryModel.procNo, list);
                    list = null;
                }
            }

            return new SelectionListModel
            {
                data = _resultDepend
            };
        }

        int _maxDependCounter = 0;
        IList<string> _resultDepend;

        private void RecursiveCheck(string procNo, IList<TempDependModel> ls)
        {
            if (_maxDependCounter > 100) return; //maximum-100 to prevent invalid setting/loop forever.
            IList<TempDependModel> list = 
                ls.Where(f => f.depend == procNo).ToList();

            if (list.Count == 0)
            {
                return; //end of recursive.
            }
            else
            {
                foreach (TempDependModel d in list)
                {
                    if(d.sts!=JobStatus.Pending.ToString() 
                        && d.sts != JobStatus.Progress.ToString()
                        && d.sts != JobStatus.Cancelling.ToString()
                        && (d.exists==true || d.tf == false)
                        )
                    {
                        _resultDepend.Add(d.procNo);
                    }
                    _maxDependCounter++;
                    RecursiveCheck(d.procNo, ls);
                }
            }
        }

    }

    public class TempDependModel
    {
        public string procNo { get; set; }
        public bool exists { get; set; }
        public string depend { get; set; }
        public string sts { get; set; }
        public bool tf { get; set; }
    }
}