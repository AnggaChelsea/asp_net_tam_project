﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using Newtonsoft.Json;
using System.IO;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetProcessDsDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessDsDataQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetProcessDsDataQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetProcessDsDataQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            int totalRecord = 0;
            dynamic formData = new List<dynamic>();

            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                // 20221109: req user permata, bisa input date lain
                var ftrs = req.QueryModel.filter.Where(f => f.field == "date");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(ftr.value))
                {
                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), "");

                    List<string> pars = new List<string>();
                    pars.Add("GB014_N");
                    pars.Add("GB015");
                    IList<SystemParameterHeader> hdrs = await _context.SystemParameterHeaders
                        .Where(ff => pars.Contains(ff.ParamCode)).ToListAsync(cancellationToken);
                    var f = hdrs.Where(ff => ff.ParamCode == "GB015").FirstOrDefault();
                    string folder = "";
                    if (f != null)
                    {
                        folder = f.ParamValue;
                    }

                    if (!string.IsNullOrEmpty(folder))
                    {
                        folder = Convert.ToDateTime(ftr.value).ToString(folder);
                    }
                    string path = hdrs.Where(ff => ff.ParamCode == "GB014_N").FirstOrDefault().ParamValue+folder+@"\";

                    IList<long> listid = await _context.ProcessLogHeaders
                            .Where(ff =>
                                ff.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                && ff.ProcessType == QueueJobType.PROCESS_DATA_SOURCE.ToString()
                                )
                            .GroupBy(g => g.ProcessNo).Select(s => s.Max(m => m.UniqueId)
                        ).ToListAsync<long>(cancellationToken);

                    var query = (from ds in _context.ProcessDataSources
                                 join hs in _context.ProcessLogHeaders.Where(ff => listid.Contains(ff.UniqueId))
                                  on ds.ProcessNo equals hs.ProcessNo into t_log
                                 from xhs in t_log.DefaultIfEmpty()
                                 join us in _context.Users
                                  on xhs.CreatedBy equals us.UserId into t_usr
                                 from xus in t_usr.DefaultIfEmpty()
                                 select new
                                 {
                                     uid = ds.UniqueId,
                                     date = ftr.value,
                                     proc_no = ds.ProcessNo,
                                     proc_nm = ds.ProcessName,
                                     sts = xhs.ProcessStatus,
                                     run_by = xus.FirstName,
                                     run_dt = xhs.CreatedDate,
                                     note = xhs.Remark,
                                     start_job = xhs.CreatedDate,
                                     finish_job = xhs.ModifiedDate,
                                     exists = ds.UsingTF ? File.Exists(path + ds.FileName.Replace("$", Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat))) ? "Y" : "N" : "",
                                     file = ds.UsingTF ? ds.FileName.Replace("$", Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat)) : "",
                                     tf = ds.UsingTF
                                 }
                        ).Where(objFtr.filterParams, objFtr.values).ToListAsync(cancellationToken);

                    objFtr = null;

                    totalRecord = query.Result.Count();

                    if (totalRecord > 0)
                    {
                        formData = query.Result.AsQueryable()
                            .OrderBy(Utility.SafeSqlString(req.QueryModel.sort))
                            .Skip(req.QueryModel.start)
                            .Take(req.QueryModel.pageSize)
                            .ToDynamicList();

                        listid = null;
                    }
                }
            }

            return new DataListModel
            {
                totalRecord = totalRecord,
                data = formData
            };

        }

    }
}