﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq.Dynamic.Core;
using ARRA.Common;
using ARRA.Common.Enumerations;
using System.IO;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetProcessDsSelectAllQueryHandler
        : AppBase,
            IRequestHandler<
                QueriesModel<GetProcessDsSelectAllQuery, SelectionListModel>,
                SelectionListModel
            >
    {
        private readonly GLOBALDbContext _context;

        public GetProcessDsSelectAllQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<SelectionListModel> Handle(
            QueriesModel<GetProcessDsSelectAllQuery, SelectionListModel> req,
            CancellationToken cancellationToken
        )
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(
                        base.ResolveFilterDynamicLinq(req.QueryModel.filter),
                        ""
                    );

                    List<string> pars = new List<string>();
                    pars.Add("GB014_N");
                    pars.Add("GB015");
                    IList<SystemParameterHeader> hdrs = await _context.SystemParameterHeaders
                        .Where(ff => pars.Contains(ff.ParamCode))
                        .ToListAsync(cancellationToken);
                    var find = hdrs.Where(ff => ff.ParamCode == "GB015").FirstOrDefault();
                    string folder = "";
                    if (find != null)
                    {
                        folder = find.ParamValue;
                    }

                    if (!string.IsNullOrEmpty(folder))
                    {
                        folder = Convert.ToDateTime(ftr.value).ToString(folder);
                    }
                    string path =
                        hdrs.Where(ff => ff.ParamCode == "GB014_N").FirstOrDefault().ParamValue
                        + folder
                        + @"\";

                    IList<long> listid = await _context.ProcessLogHeaders
                        .Where(
                            f =>
                                f.DataDate == Convert.ToDateTime(Utility.SafeSqlString(ftr.value))
                                && f.ProcessType == QueueJobType.PROCESS_DATA_SOURCE.ToString()
                        )
                        .GroupBy(g => g.ProcessNo)
                        .Select(s => s.Max(m => m.UniqueId))
                        .ToListAsync<long>(cancellationToken);

                    var listSelectAll = await (
                        from ds in _context.ProcessDataSources
                        join hs in _context.ProcessLogHeaders.Where(
                            f => listid.Contains(f.UniqueId)
                        )
                            on ds.ProcessNo equals hs.ProcessNo
                            into t_log
                        from xhs in t_log.DefaultIfEmpty()
                        join us in _context.Users on xhs.CreatedBy equals us.UserId into t_usr
                        from xus in t_usr.DefaultIfEmpty()
                        select new
                        {
                            uid = ds.UniqueId,
                            date = ftr.value,
                            proc_no = ds.ProcessNo,
                            proc_nm = ds.ProcessName,
                            sts = xhs.ProcessStatus == null ? "" : xhs.ProcessStatus,
                            run_by = xus.FirstName,
                            run_dt = xhs.CreatedDate,
                            note = xhs.Remark,
                            start_job = ds.CreatedDate,
                            finish_job = ds.ModifiedDate,
                            exists = ds.UsingTF
                                ? File.Exists(
                                    path
                                        + ds.FileName.Replace(
                                            "$",
                                            Convert
                                                .ToDateTime(ftr.value)
                                                .ToString(ds.FileNameDateFormat)
                                        )
                                )
                                    ? "Y"
                                    : "N"
                                : "",
                            file = ds.UsingTF
                                ? ds.FileName.Replace(
                                    "$",
                                    Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat)
                                )
                                : "",
                            tf = ds.UsingTF
                        }
                    ).ToListAsync(cancellationToken);

                    listSelect = listSelectAll
                        .AsQueryable()
                        .Where(objFtr.filterParams, objFtr.values)
                        .Where(
                            f =>
                                f.sts != JobStatus.Pending.ToString()
                                && f.sts != JobStatus.Progress.ToString()
                                && f.sts != JobStatus.Cancelling.ToString()
                                && (f.exists == "Y" || f.tf == false)
                        )
                        .Select(s => s.proc_no.ToString())
                        .ToList();

                    //listSelect = await (from ds in _context.ProcessDataSources
                    //                    join hs in _context.ProcessLogHeaders.Where(f => listid.Contains(f.UniqueId))
                    //                     on ds.ProcessNo equals hs.ProcessNo into t_log
                    //                    from xhs in t_log.DefaultIfEmpty()
                    //                    join us in _context.Users
                    //                     on xhs.CreatedBy equals us.UserId into t_usr
                    //                    from xus in t_usr.DefaultIfEmpty()
                    //                    select new
                    //                    {
                    //                        uid = ds.UniqueId,
                    //                        date = ftr.value,
                    //                        proc_no = ds.ProcessNo,
                    //                        proc_nm = ds.ProcessName,
                    //                        sts = xhs.ProcessStatus==null?"":xhs.ProcessStatus,
                    //                        run_by = xus.FirstName,
                    //                        run_dt = xhs.CreatedDate,
                    //                        note = xhs.Remark,
                    //                        exists = ds.UsingTF?File.Exists(path + ds.FileName.Replace("$", Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat))) ? "Y" : "N":"",
                    //                        file = ds.UsingTF?ds.FileName.Replace("$", Convert.ToDateTime(ftr.value).ToString(ds.FileNameDateFormat)):"",
                    //                        tf = ds.UsingTF
                    //                    }
                    //                    ).Where(objFtr.filterParams, objFtr.values)
                    //                    .Where(
                    //                        f=>f.sts!=JobStatus.Pending.ToString()
                    //                        && f.sts!=JobStatus.Progress.ToString()
                    //                        && f.sts!= JobStatus.Cancelling.ToString()
                    //                        && (f.exists=="Y" || f.tf==false))
                    //                    .Select(c => c.proc_no)
                    //                    .ToListAsync<string>(cancellationToken);
                    objFtr = null;
                }
            }

            return new SelectionListModel { data = listSelect };
        }
    }
}
