﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using System;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Linq.Dynamic;
using System.IO;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetProcessDsDataDetailQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetProcessDsDataDetailQuery, DataListModel>, DataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetProcessDsDataDetailQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<DataListModel> Handle(QueriesModel<GetProcessDsDataDetailQuery, DataListModel> req, CancellationToken cancellationToken)
        {
            dynamic formData = new List<dynamic>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "date").FirstOrDefault();
            if (ftr != null)
            {
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    string procno = await _context.ProcessDataSources.Where(f => f.UniqueId == req.QueryModel.uid).Select(c => c.ProcessNo).FirstOrDefaultAsync(cancellationToken);
                    long uidlog = await _context.ProcessLogHeaders
                                    .Where(f => f.ProcessNo == procno
                                        && f.DataDate == Convert.ToDateTime(ftr.value)
                                    ).OrderByDescending(o => o.UniqueId).Take(1).Select(s => s.UniqueId).FirstOrDefaultAsync(cancellationToken);
                    //if (uidlog != 0)
                    //{
                        List<string> pars = new List<string>();
                        pars.Add("GB014_N");
                        pars.Add("GB015");
                        IList<SystemParameterHeader> hdrs = await _context.SystemParameterHeaders
                            .Where(ff => pars.Contains(ff.ParamCode)).ToListAsync(cancellationToken);
                        var find = hdrs.Where(ff => ff.ParamCode == "GB015").FirstOrDefault();
                        string folder = "";
                        if (find != null)
                        {
                            folder = find.ParamValue;
                        }

                        string path = hdrs.Where(ff => ff.ParamCode == "GB014_N").FirstOrDefault().ParamValue;// + folder + @"\";
                        pars = null;

                        formData = await (from ds in _context.ProcessDataSources
                                          join hs in _context.ProcessLogHeaders.Where(f => f.UniqueId == uidlog)
                                           on ds.ProcessNo equals hs.ProcessNo into t_log
                                          from xhs in t_log.DefaultIfEmpty()
                                          join us in _context.Users
                                           on xhs.CreatedBy equals us.UserId into t_usr
                                          from xus in t_usr.DefaultIfEmpty()
                                          where ds.UniqueId == req.QueryModel.uid
                                          select new
                                          {
                                              uid = ds.UniqueId,
                                              date = ftr.value,
                                              proc_no = ds.ProcessNo,
                                              proc_nm = ds.ProcessName,
                                              sts = xhs.ProcessStatus,
                                              run_by = xus.FirstName,
                                              run_dt = xhs.CreatedDate,
                                              note = xhs.Remark,
                                              start_job = xhs.CreatedDate,
                                              finish_job = xhs.ModifiedDate,
                                              exists = ds.UsingTF?File.Exists(path + ds.FileName.Replace("$", Convert.ToDateTime(xhs.DataDate).ToString(ds.FileNameDateFormat))) ? "Y" : "N":"",
                                              file = ds.UsingTF?ds.FileName.Replace("$", Convert.ToDateTime(xhs.DataDate).ToString(ds.FileNameDateFormat)):"",
                                              tf = ds.UsingTF
                                          }).ToListAsync<dynamic>(cancellationToken);

                    }
                //}
            }
            return new DataListModel
            {
                totalRecord = 1,
                data = formData
            };
        }
    }

    public class StatusApproval
    {
        public string SubmitBy { get; set; }
        public DateTime? SubmitDate { get; set; }
        public string Action { get; set; }
    }
}