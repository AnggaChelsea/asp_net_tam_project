﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Customs.Process.Queries
{
    public class GetProcessDsDependQuery
    {
        public string formCode { get; set; }
        public string procNo { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}