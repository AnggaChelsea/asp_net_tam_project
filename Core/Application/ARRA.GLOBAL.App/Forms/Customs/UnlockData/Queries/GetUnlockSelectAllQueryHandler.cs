﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Forms.Models;
using System.Linq.Dynamic.Core;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;

namespace ARRA.GLOBAL.App.Forms.Customs.UnlockData.Queries
{
    public class GetUnlockSelectAllQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetUnlockSelectAllQuery, SelectionListModel>, SelectionListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetUnlockSelectAllQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<SelectionListModel> Handle(QueriesModel<GetUnlockSelectAllQuery, SelectionListModel> req, CancellationToken cancellationToken)
        {
            List<string> listSelect = new List<string>();
            FilterModel ftr = req.QueryModel.filter.Where(f => f.field == "REPORT_DT").FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.QueryModel.filter.Where(f => f.field == "REPORT_DT");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    TMP_Form form = await _context.FormHeaders.Where(f => f.FormCode == req.QueryModel.formCode)
                                    .Select(c => new TMP_Form
                                    {
                                        Module = c.Module,
                                        Filter = c.FilterConditionEx
                                    })
                                    .FirstOrDefaultAsync(cancellationToken);
                    DynamicFilterModel objFtr = Shared.Function.GetFilterConds(base.ResolveFilterDynamicLinq(req.QueryModel.filter), form.Filter);

                    // check apakah user memiliki akses delete untuk menu unlock
                    if (req.AccessMatrix.AllowDelete)
                    {
                        using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                        {
                            listSelect = await ctx.LockDatas
                                .Select(c => new
                                {
                                    uid = c.UniqueId,
                                    REPORT_DT = c.ReportDate,
                                    PERIOD_TP = c.PeriodType,
                                    FORM_CD = c.FormCode,
                                    BRANCH = c.Branch,
                                    CREATED_BY = c.CreatedBy,
                                    CREATED_DT = c.CreatedDate
                                })
                                .Where(objFtr.filterParams, objFtr.values)
                                .Select(c => c.uid.ToString())
                                .ToListAsync(cancellationToken);
                            objFtr = null;
                        }
                    }

                    form = null;
                    objFtr = null;
                }
            }

            return new SelectionListModel
            {
                data = listSelect
            };
        }
    }
    public class TMP_Form
    {
        public string Module { get; set; }
        public string Filter { get; set; }
    }
}