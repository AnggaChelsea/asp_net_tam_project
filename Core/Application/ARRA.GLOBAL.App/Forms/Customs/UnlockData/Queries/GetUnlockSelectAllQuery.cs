﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.UnlockData.Queries
{
    public class GetUnlockSelectAllQuery
    {
        public string formCode { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}