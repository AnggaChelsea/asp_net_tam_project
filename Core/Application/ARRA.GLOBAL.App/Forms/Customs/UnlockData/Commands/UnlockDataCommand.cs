﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Forms.Customs.UnlockData.Commands
{
    public class UnlockDataCommand
    {
        public string formCode { get; set; }
        public string select { get; set; }
        public IList<FilterModel> filter { get; set; }
    }
}