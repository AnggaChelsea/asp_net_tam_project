﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using System.Linq;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Forms.Customs.UnlockData.Commands
{
    public class UnlockDataCommandHandler : AppBase, IRequestHandler<CommandsModel<UnlockDataCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public UnlockDataCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<UnlockDataCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            string err = "";
            FilterModel ftr = req.CommandModel.filter.Where(f => f.field == "REPORT_DT").FirstOrDefault();
            if (ftr != null)
            {
                var ftrs = req.CommandModel.filter.Where(f => f.field == "REPORT_DT");

                foreach (var item in ftrs)
                {
                    if (item.value != null && item.value != "")
                    {
                        ftr = item;
                        break;
                    }
                }
                
                if (!string.IsNullOrEmpty(ftr.value))
                {
                    if (string.IsNullOrEmpty(req.CommandModel.select) == false)
                    {
                        List<Int64> select = req.CommandModel.select.Split('$').Where(f => f != "").Select(Int64.Parse).ToList();
                        if (select.Count > 0)
                        {
                            try
                            {
                                string module = await _context.FormHeaders.Where(f => f.FormCode == req.CommandModel.formCode)
                                    .Select(c => c.Module).FirstOrDefaultAsync<string>(cancellationToken);

                                using (MODULEDbContext ctx = base.GetDbContext(module))
                                {
                                    IList<ARRA.MODULE.Domain.Entities.LockData> LockData =
                                        await ctx.LockDatas.Where(f => select.Contains(f.UniqueId)).ToListAsync(cancellationToken);

                                    ctx.RemoveRange(LockData);

                                    await ctx.SaveChangesAsync(cancellationToken);

                                    Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync();
                                    AuditTrailHeader hdrAudit = new AuditTrailHeader
                                    {
                                        MenuCode = menu.MenuCode,
                                        MenuName = menu.MenuName,
                                        Module = ModuleType.GLOBAL.ToString(),
                                        WithApproval = false,
                                        ActivityAction = AuditType.REMOVE.ToString(),
                                        UserId = req.UserIdentity.UserId,
                                        ActivityDate = req.CurrentDateTime,
                                        ActivityHost = req.UserIdentity.Host,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    menu = null;

                                    _context.AuditTrailHeaders.Add(hdrAudit);

                                    await _context.SaveChangesAsync(cancellationToken);

                                    string auditForm = "";
                                    string auditBranch = "";
                                    foreach (ARRA.MODULE.Domain.Entities.LockData val in LockData)
                                    {
                                        auditForm += val.FormCode + ", ";
                                        auditBranch += val.Branch + ", ";
                                    }
                                    LockData = null;

                                    AuditTrailDetail dtlAudit = new AuditTrailDetail
                                    {
                                        FormColName = "FORM",
                                        FieldName = "FORM",
                                        FormSeq = 1,
                                        HeaderId = hdrAudit.UniqueId,
                                        ValueBefore = auditForm,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    _context.AuditTrailDetails.Add(dtlAudit);

                                    dtlAudit = new AuditTrailDetail
                                    {
                                        FormColName = "BRANCH",
                                        FieldName = "BRANCH",
                                        FormSeq = 2,
                                        HeaderId = hdrAudit.UniqueId,
                                        ValueBefore = auditBranch,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    };
                                    _context.AuditTrailDetails.Add(dtlAudit);

                                    auditBranch = "";
                                    auditForm = "";
                                    dtlAudit = null;
                                    await _context.SaveChangesAsync(cancellationToken);
                                }
                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }
                        }
                    }
                    else
                    {
                        err = "Please chooose process to cancel";
                    }
                }
                else
                {
                    err = "Please add date filter before run cancel";
                }
            }
            else
            {
                err = "Please add date filter before run cancel";
            }
            ftr = null;


            if (err != "")
            {
                throw new CustomException(err);
                //throw new ValidationException(new List<FluentValidation.Results.ValidationFailure>()
                //{
                //    new FluentValidation.Results.ValidationFailure("PROCESS",err)
                //});
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}