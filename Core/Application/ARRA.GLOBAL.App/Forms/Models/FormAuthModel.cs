﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class FormAuthModel
    {
        public string code { get; set; }
        public string ojkCode { get; set; }
        public string name { get; set; }
    }
}
