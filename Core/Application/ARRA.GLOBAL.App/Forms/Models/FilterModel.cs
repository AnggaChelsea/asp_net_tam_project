﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class FilterModel
    {
        public string field { get; set; }
        public string optr { get; set; }
        public string value { get; set; }
        public object valuedt { get; set; }
        public string valuetype { get; set; }
        public bool wlookup { get; set; }
        public dynamic lookup { get; set; }
        public string datatype { get; set; }
        public Boolean? defaultftr { get; set; }

        public string orifield { get; set; }
        public string orioptr { get; set; }
        public string orivalue { get; set; }
        public string oridatatp { get; set; }
        public Boolean? autorefresh { get; set; }
    }
}