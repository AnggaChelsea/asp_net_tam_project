﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class ReportStaticCellValueModel
    {
        public long UniqueId { get; set; }
        public DateTime? ReportDate { get; set; }
        public int RowSequence { get; set; }
        public string CellPosition { get; set; }
        public string CellType { get; set; }
        public string CellValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedHost { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedHost { get; set; }
    }
}
