﻿using System;
using System.Collections.Generic;
namespace ARRA.GLOBAL.App.Forms.Models
{
    public class FormAuthListModel
    {
        public IList<FormAuthModel> data { get; set; }
    }
}
