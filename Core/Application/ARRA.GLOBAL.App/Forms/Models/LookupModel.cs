﻿namespace ARRA.GLOBAL.App.Forms.Models
{
    public class LookupModel
    {
        public string partype { get; set; }
        public string code { get; set; }
        public string desc { get; set; }

    }
}