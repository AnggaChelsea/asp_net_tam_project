﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class EditorListModel
    {
        public IList<EditorModel> data { get; set; }

        public IList<LookupModel> lookup { get; set; }
    }
}