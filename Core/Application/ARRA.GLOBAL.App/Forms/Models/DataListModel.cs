﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class DataListModel 
    {
        public Int64 totalRecord { get; set; }
        public IList<dynamic> data { get; set; }
        public dynamic aggregate { get; set; }
        public dynamic sts { get; set; } //status pending approval
    }
}