﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class GridModel
    {
        public string field { get; set; }
        public string caption { get; set; }
        public string datatype { get; set; }
        public string format { get; set; }
        public string aggregate { get; set; }
        public string lCode { get; set; }
    }
}