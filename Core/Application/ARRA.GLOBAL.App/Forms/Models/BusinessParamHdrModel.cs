﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class BusinessParamHdrModel
    {
        public string module { get; set; }
        public string paramCode { get; set; }
        public string paramName { get; set; }
        public string paramDesc { get; set; }
        public string paramValue { get; set; }
    }
}