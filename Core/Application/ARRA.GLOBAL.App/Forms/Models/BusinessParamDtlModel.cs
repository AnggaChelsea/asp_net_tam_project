﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class BusinessParamDtlModel
    {
        public string paramCode { get; set; }
        public string paramValue { get; set; }
        public string paramDesc { get; set; }
        public Int32? paramSeq { get; set; }
        public string paramValue2 { get; set; }
        public string paramValue3 { get; set; }
    }
}