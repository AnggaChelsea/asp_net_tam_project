﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class EditorModel
    {
        public string field { get; set; }
        public string caption { get; set; }
        public string editor { get; set; }
        public string lookup { get; set; }
        public string nested { get; set; }
        public bool? readOnly { get; set; }
        public bool? mandatory { get; set; }
        public string defaultvalue { get; set; }
        //public int? maxLength { get; set; } //ubah jadi double 20200424
        public double? maxLength { get; set; }
        public string exprs { get; set; }
        public bool? show { get; set; }
        public string dylookup { get; set; }
        public string defaultNestedField { get; set; } // add 20200505 rizwan
        public string defaultNestedSrc { get; set; } // add 20200506 rizwan
        public string defaultNestedMap { get; set; } // add 20200506 rizwan
        public string edrCalculation { get; set; } // add 20200527 rizwan
    }
}