﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class ActionRowModel
    {
        public string title { get; set; }
        public string icon { get; set; }
        public string isNeedPermission { get; set; }
        public string permission { get; set; }
        public string type { get; set; }
        public int sequence { get; set; }
    }
}