﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class FileModel
    {
        public string fileName { get; set; }
        public string path { get; set; }
        public string error { get; set; }
    }
}
