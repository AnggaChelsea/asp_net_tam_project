﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class BusinessParamModel
    {
        public IList<BusinessParamHdrModel> headers { get; set; }
        public IList<BusinessParamDtlModel> details { get; set; }
    }
}