﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Forms.Models
{
    public class FormListModel
    {
        public string linkDetail { get; set; }
        public string formName { get; set; }
        public string linkPK { get; set; }
        public string linkFK { get; set; }
        public bool? expDirect { get; set; }
        public string sort { get; set; }
        public IList<FilterModel> filter { get; set; }
        public IList<GridModel> grid { get; set; }
        public IList<ActionRowModel> actionRow { get; set; }
        public IList<NavbarModel> navbar { get; set; }
        public bool allowIns { get; set; }
        public bool allowUpd { get; set; }
        public bool allowDel { get; set; }
        public bool allowExp { get; set; }
        public bool allowImp { get; set; }
        public bool allowApv { get; set; }

        public string editorAPI { get; set; }
        public string lookupAPI { get; set; }
        public string dataAPI { get; set; }
        public string dataDetailAPI { get; set; }
        public string saveFilterAPI { get; set; }
        public string saveDataAPI { get; set; }
        public string exportTaskAPI { get; set; }
        public string downloadAPI { get; set; }
        public string uploadAPI { get; set; }
        public string selectAllAPI { get; set; }
        public string selectDependAPI { get; set; }
        public string actionAPI { get; set; }
        public string linkValRes { get; set; }
        public IList<ReportStaticCellModel> reportStaticCells { get; set; }
    }
}