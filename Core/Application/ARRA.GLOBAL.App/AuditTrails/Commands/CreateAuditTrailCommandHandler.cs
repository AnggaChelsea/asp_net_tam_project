﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System;
using ARRA.Common;
using System.Reflection;

namespace ARRA.GLOBAL.App.AuditTrails.Commands
{
    public class CreateAuditTrailCommandHandler
        : IRequestHandler<CreateAuditTrailCommand>
    {
        private readonly GLOBALDbContext _context;
        private readonly IDateTime _curdatetime;

        public CreateAuditTrailCommandHandler(GLOBALDbContext context, IDateTime curdatetime)
        {
            _context = context;
            _curdatetime = curdatetime;
        }

        public async Task<Unit> Handle(CreateAuditTrailCommand request, CancellationToken cancellationToken)
        {
            //_context.Cities.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private void AddAuditLog<T>(object model)
        {
            foreach (PropertyInfo p in model.GetType().GetProperties())
            {
                //string aa = p.GetValue(model).ToString();
                //if required a db column name
                //string a = _context.Model.FindEntityType(model.GetType()).GetProperties().Select(x => x.Relational().ColumnName).FirstOrDefault();
            }
        }

        private void AddAuditLog<T>(List<T> listmodel)
        {
            foreach (T item in listmodel)
            {
                foreach (PropertyInfo pc in item.GetType().GetProperties())
                {
                    //string aa = pc.GetValue(item).ToString();
                    //using description attribute for field descriptions.
                    //object[] obj = pc.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    //string bb = (obj[0] as DescriptionAttribute).Description;
                }
            }

        }

    }
}