﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AuditTrails.Commands
{
    public class CreateAuditTrailCommand : IRequest
    {
        public string uuid { get; set; }
        public string FieldName { get; set; }
        public string FieldDesc { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}