﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Menus.Models;
using ARRA.GLOBAL.App.Authentications.Models;
using System;

namespace ARRA.GLOBAL.App.Authentications.Queries
{
    public class GetAmxByRouteUrlQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetAmxByRouteUrlQuery, AccessMatrixModel>, AccessMatrixModel>
    {
        private readonly GLOBALDbContext _context;

        public GetAmxByRouteUrlQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<AccessMatrixModel> Handle(QueriesModel<GetAmxByRouteUrlQuery, AccessMatrixModel> req, CancellationToken cancellationToken)
        {
            AccessMatrixModel amx = await (from a in _context.MenuGroupDetails
                                     join m in _context.Menus on a.MenuCode equals m.MenuCode
                                     where a.GroupId == req.QueryModel.MenuGroup
                                       && string.IsNullOrEmpty(m.MenuUrl) == false
                                       && m.MenuUrl == req.QueryModel.MenuUrl
                                       && m.Status == true
                                     select new AccessMatrixModel
                                     {
                                         MenuCode = m.MenuCode,
                                         Module = m.Module,
                                         AllowInsert = Convert.ToBoolean(a.AllowInsert),
                                         AllowUpdate = Convert.ToBoolean(a.AllowUpdate),
                                         AllowDelete = Convert.ToBoolean(a.AllowDelete),
                                         AllowExport = Convert.ToBoolean(a.AllowExport),
                                         AllowImport = Convert.ToBoolean(a.AllowImport),
                                         AllowApproval = Convert.ToBoolean(a.AllowApproval)
                                     }).FirstOrDefaultAsync(cancellationToken);

            return amx;
        }

    }
}