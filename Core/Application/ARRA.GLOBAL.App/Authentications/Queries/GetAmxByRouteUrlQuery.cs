﻿using MediatR;
namespace ARRA.GLOBAL.App.Authentications.Queries
{
    public class GetAmxByRouteUrlQuery
    {
        public string MenuGroup { get; set; }
        public string MenuUrl { get; set; }
    }
}