﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Menus.Models;
using ARRA.GLOBAL.App.Authentications.Models;
using System;

namespace ARRA.GLOBAL.App.Authentications.Queries
{
    public class GetAmxByRoute
    {
        private readonly GLOBALDbContext _context;

        public GetAmxByRoute(GLOBALDbContext context)
        {
            _context = context;
        }

        public AccessMatrixModel GetAmx(string role, string url)
        {
            // by pass validation result detail
            // rizwan 20200826
            if (url.Contains("F_SLVLD") || url.Contains("F_ANVLD_"))
            {
                return new AccessMatrixModel
                {
                    MenuCode = "F_SLVLD",
                    Module = "SLIK",
                    AllowInsert = false,
                    AllowUpdate = false,
                    AllowDelete = false,
                    AllowExport = false,
                    AllowImport = false,
                    AllowApproval = false
                };
            }

            AccessMatrixModel amx = (from a in _context.MenuGroupDetails
                                     join m in _context.Menus on a.MenuCode equals m.MenuCode
                                     where a.GroupId == role
                                       && string.IsNullOrEmpty(m.MenuUrl) == false
                                       && m.MenuUrl == url
                                       && m.Status == true
                                     select new AccessMatrixModel
                                     {
                                         MenuCode = m.MenuCode,
                                         Module = m.Module,
                                         AllowInsert = Convert.ToBoolean(a.AllowInsert),
                                         AllowUpdate = Convert.ToBoolean(a.AllowUpdate),
                                         AllowDelete = Convert.ToBoolean(a.AllowDelete),
                                         AllowExport = Convert.ToBoolean(a.AllowExport),
                                         AllowImport = Convert.ToBoolean(a.AllowImport),
                                         AllowApproval = Convert.ToBoolean(a.AllowApproval)
                                     }).FirstOrDefault();


            return amx;
        }

    }
}