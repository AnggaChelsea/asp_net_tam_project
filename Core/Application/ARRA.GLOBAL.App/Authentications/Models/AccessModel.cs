﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Authentications.Models
{
    public class AccessModel
    {
        public string menuCode { get; set; }
        public string menuUrl { get; set; }
        public string module { get; set; }
        public Boolean? allowView { get; set; }
        public Boolean? allowInsert { get; set; }
        public Boolean? allowUpdate { get; set; }
        public Boolean? allowDelete { get; set; }
        public Boolean? allowExport { get; set; }
        public Boolean? allowImport { get; set; }
        public Boolean? allowApproval { get; set; }
    }
}
