﻿using FluentValidation;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.Common;
using System.Text.RegularExpressions;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator(
            IConfiguration configuration)
        {
            RuleFor(x => x).Custom((x, context) => {
                ApplicationSettings sett = new ApplicationSettings(configuration);
                if (string.IsNullOrEmpty(x.Password))
                {
                    context.AddFailure("password", ErrorMessages.PasswordEmpty);
                }
                else if (string.IsNullOrEmpty(x.newPassword))
                {
                    context.AddFailure("password", ErrorMessages.NewPasswordEmpty);
                }
                else if (x.newPassword.Equals(x.Password))
                {
                    context.AddFailure("password", ErrorMessages.OldNewPasswordSame);
                }
                else if (!x.newPassword.Equals(x.confirmNewPassword))
                {
                    context.AddFailure("password", ErrorMessages.PasswordNotMatch);
                }
                else if (x.newPassword.Length < sett.PasswordMinLength)
                {
                    context.AddFailure("password", string.Format(ErrorMessages.PasswordLength, sett.PasswordMinLength.ToString()));
                }
                else if (!Regex.IsMatch(x.newPassword, "[A-Z]") && sett.PasswordUppercase)
                {
                    context.AddFailure("password", ErrorMessages.PasswordUppercaseLetter);
                }
                else if (!Regex.IsMatch(x.newPassword, "[a-z]") && sett.PasswordLowercase)
                {
                    context.AddFailure("password", ErrorMessages.PasswordLowercaseLetter);
                }
                else if (!Regex.IsMatch(x.newPassword, "[0-9]") && sett.PasswordNumeric)
                {
                    context.AddFailure("password", ErrorMessages.PasswordDigit);
                }
                else if (!Regex.IsMatch(x.newPassword, "[^a-zA-Z0-9]") && sett.PasswordSpecialChar)
                {
                    context.AddFailure("password", ErrorMessages.PasswordSpecialCharacter);
                }
                sett = null;
            });
        }
    }
}