﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Persistence;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        private readonly GLOBALDbContext _context;
        private readonly IAuthenticationService _authenticationService;
        public LoginCommandValidator(GLOBALDbContext context, IAuthenticationService authenticationService)
        {
            _context = context;
            _authenticationService = authenticationService;

            RuleFor(x => x.UserName).NotEmpty().WithMessage("Username is required");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password is required");
     
            //RuleFor(x => x).Must(x => x.Equals(ValidatePassword(x.UserName, x.Password))).WithMessage("Invalid Username or Password");
        }
    }
}


//public static class RuleBuilderExtensions
//{
//    public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder, int minimumLength = 14)
//    {
//        var options = ruleBuilder
//            .NotEmpty().WithMessage(ErrorMessages.PasswordEmpty)
//            .MinimumLength(minimumLength).WithMessage(ErrorMessages.PasswordLength)
//            .Matches("[A-Z]").WithMessage(ErrorMessages.PasswordUppercaseLetter)
//            .Matches("[a-z]").WithMessage(ErrorMessages.PasswordLowercaseLetter)
//            .Matches("[0-9]").WithMessage(ErrorMessages.PasswordDigit)
//            .Matches("[^a-zA-Z0-9]").WithMessage(ErrorMessages.PasswordSpecialCharacter);
//        return options;
//    }
//}
//RuleFor(x => x.Password).Password();