﻿namespace ARRA.GLOBAL.App.Authentications.Commands
{
   public class ChangePasswordCommand
    {
        public string Password { get; set; }
        public string newPassword { get; set; }
        public string confirmNewPassword { get; set; }
    }
}