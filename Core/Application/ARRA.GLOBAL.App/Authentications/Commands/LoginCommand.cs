﻿using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ARRA.GLOBAL.App.Authentications.Commands;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class LoginCommand 
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Browser { get; set; }
    }
}