﻿using MediatR;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class CleanActiveSessionCommandHandler : AppBase, IRequestHandler<CommandsModel<CleanActiveSessionCommand, StatusModel>, StatusModel>
    {
        private readonly IConfiguration _configuration;

        public CleanActiveSessionCommandHandler(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<CleanActiveSessionCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext _context = base.GetGlobalDbContext())
            {
                try
                {
                    var sessions = _context.ActiveSession.Where(a => a.SessionDate.Value.Date < req.CurrentDateTime.Date).ToList();
                    _context.ActiveSession.RemoveRange(sessions);

                    await _context.SaveChangesAsync(cancellationToken);
                }
                catch
                {
                    return new StatusModel
                    {
                        status = CommandStatus.Failed.ToString()
                    };
                }
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };

        }
    }
}
