﻿using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ARRA.GLOBAL.App.Authentications.Commands;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class LogoutCommand
    {
        public string UserName { get; set; }
    }
}
