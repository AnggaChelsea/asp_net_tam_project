﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class InsertJobCleanSessionCommandHandler : AppBase, IRequestHandler<CommandsModel<InsertJobCleanSessionCommand, StatusModel>, StatusModel>
    {
        private readonly IConfiguration _configuration;
        public InsertJobCleanSessionCommandHandler(IConfiguration configuration) : base(configuration)
        {

            _configuration = configuration;
        }
        
        public async Task<StatusModel> Handle(CommandsModel<InsertJobCleanSessionCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext _context = base.GetGlobalDbContext())
            {
                var jobQueued = await _context.JobQueueHeaders.Where(a => a.JobType.Equals(QueueJobType.CLEAN_ACT_SESSION.ToString())
                 && a.CreatedDate.Value.Date == req.CurrentDateTime.Date
             ).ToListAsync(cancellationToken);


                if (jobQueued.Count == 0)
                {
                    JobQueueHeader jobQueueHeader = new JobQueueHeader()
                    {
                        JobType = QueueJobType.CLEAN_ACT_SESSION.ToString(),
                        Status = "Pending",
                        CreatedBy = "SYSTEM",
                        CreatedDate = req.CurrentDateTime
                    };
                    _context.JobQueueHeaders.Add(jobQueueHeader);

                    await _context.SaveChangesAsync(cancellationToken);
                }
            }
            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}
