﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using ARRA.Common;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class ChangePasswordCommandHandler : AppBase, IRequestHandler<CommandsModel<ChangePasswordCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IAuthenticationService _authenticationService;
        private readonly IConfiguration _configuration;
        public ChangePasswordCommandHandler(
            GLOBALDbContext context, IAuthenticationService authenticationService,
            IConfiguration configuration) : base(context)
        {
            _context = context;
            _authenticationService = authenticationService;
            _configuration = configuration;

        }

        public async Task<StatusModel> Handle(CommandsModel<ChangePasswordCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            ApplicationSettings set = new ApplicationSettings(_configuration);
            int count = set.PasswordHistoryCount;
            set = null;

            StatusModel m = new StatusModel();
            var entity = await _context.Users.FindAsync(req.UserIdentity.UserId);

            if (_authenticationService.Encrypt(req.CommandModel.Password).Equals(entity.Password))
            {
                string newPass = _authenticationService.Encrypt(req.CommandModel.newPassword);
                IList<string> histPass = await _context.UserPasswordHistories
                    .Where(f => f.UserId == req.UserIdentity.UserId)
                    .OrderByDescending(c => c.UniqueId)
                    .Take(count)
                    .Select(c => c.Password)
                    .ToListAsync<string>(cancellationToken);

                if (histPass.Contains(newPass))
                {
                    m.status = CommandStatus.Failed.ToString();
                    m.message = string.Format(ErrorMessages.PasswordSameWithPrevPass,count.ToString());
                }
                else
                {
                    entity.Password = newPass;
                    entity.ConfirmPassword = newPass;
                    entity.PasswordExpired = false;
                    entity.ModifiedBy = req.UserIdentity.UserId;
                    entity.ModifiedDate = req.CurrentDateTime;
                    entity.ModifiedHost = req.UserIdentity.Host;

                    UserPasswordHistory hist = new UserPasswordHistory
                    {
                        UserId = req.UserIdentity.UserId,
                        Password = newPass,
                        CreatedBy = req.UserIdentity.UserId,
                        CreatedDate = req.CurrentDateTime,
                        CreatedHost = req.UserIdentity.Host,
                    };

                    _context.UserPasswordHistories.Add(hist);
                    hist = null;

                    await _context.SaveChangesAsync(cancellationToken);

                    m.status = CommandStatus.Success.ToString();
                }
                histPass = null;
            }
            else
            {
                m.status = CommandStatus.Failed.ToString();
                m.message = ErrorMessages.OldPasswordInvalid;
            }
            entity = null;

            return m;
        }
    }
}
