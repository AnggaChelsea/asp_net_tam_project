﻿using MediatR;
using ARRA.Common.Model;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Authentications.Models;
using System.Linq;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class LogoutCommandHandler : AppBase, IRequestHandler<CommandsModel<LogoutCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public LogoutCommandHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<LogoutCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            try
            {
                _context.LogonActivities.Add(new LogonActivity()
                {
                    UserId = req.UserIdentity.UserId.ToLower(),
                    LoginStatus = LoginStatus.Logout.ToString(),
                    CreatedBy = req.UserIdentity.UserId.ToLower(),
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                ActiveSession listActSession = _context.ActiveSession.Where(w => w.UserId == req.UserIdentity.UserId.ToLower()).FirstOrDefault();
                _context.RemoveRange(listActSession);

                await _context.SaveChangesAsync(cancellationToken);

            }
            catch
            {

            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}
