﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Authentications.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Authentications.Commands
{
    public class LoginCommandHandler : AppBase, IRequestHandler<CommandsModel<LoginCommand, LoginModel>, LoginModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IAuthenticationService _authenticationService;
        public LoginCommandHandler(
            GLOBALDbContext context, IAuthenticationService authenticationService,
            IConfiguration configuration) : base(context)
        {
            _context = context;
            _authenticationService = authenticationService;
            _configuration = configuration;
        }


        public async Task<LoginModel> Handle(CommandsModel<LoginCommand, LoginModel> req, CancellationToken cancellationToken)
        {

    //        "AuthenticationSchema": "ARRA.APP",
    //"JWT_SECRET": "tkn.R0b1n.7788!#",
    //"PASS_SECRET": "pass.R0b1n.8899!#",
    //"SESSION_TIMEOUT": "60",
    //"ALLOW_CLIENT_URL": "*", //ADDITIONAL URL SEPERATE WITH COMMA
    //"UPLOAD_MAX_SIZE": "500", //IN MEGABYTES
    //"PASSWORD_POLICY": {
    //            "MINIMUM_LENGTH": 8,
    //  "UPPERCASE_CHARS": true,
    //  "LOWERCASE_CHARS": true,
    //  "NUMERIC_CHARS": true,
    //  "SPECIAL_CHARS": true,
    //  "PASS_HISTORY_COUNT": 8, //not allow same with previous password
    //  "PASS_EXPIRED": 0 //in days, 0 = never expire.
    //},
    //"LOGIN_POLICY": {
    //            "AUTOMATIC_UNLOCK": 1, //in minutes//0=never
    //  "MAXIMUM_INVALID_LOGIN": 3 // 3times // 0=never check
    //},

            LoginModel loginInfo = new LoginModel();
            bool logged = false;
            var session_timeout = _configuration.GetSection("ApplicationSettings:SESSION_TIMEOUT").Value;
            //Convert.ToInt32(await _context.SystemParameterHeaders.Where(f => f.ParamCode == "GB041").Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken));
           
            //retrive user info.
            var entity = await _context.Users.FindAsync(req.CommandModel.UserName);
            if (entity != null)
            {
                if (entity.Status) //active user only.
                {
                    ApplicationSettings sett = new ApplicationSettings(_configuration);

                    //lock checking.
                    if (entity.LockStatus == true)
                    {
                        if (sett.LoginAutomaticUnlock > 0)
                        {
                            if((req.CurrentDateTime-entity.LastLoginDateTime).Value.TotalMinutes > sett.LoginAutomaticUnlock)
                            {
                                entity.LockCounter = 0;
                                entity.LockStatus = false;
                            }
                            else
                            {
                                loginInfo.sts = string.Format(ErrorMessages.LoginLockedAuto, sett.LoginAutomaticUnlock.ToString());
                                return loginInfo; //not continue.
                            }
                        }
                        else
                        {
                            loginInfo.sts = ErrorMessages.LoginLocked;
                            return loginInfo; //not continue.
                        }
                    }

                    var loginMaxFailed = _configuration.GetSection("ApplicationSettings:LOGIN_POLICY:MAXIMUM_INVALID_LOGIN").Value;
                    //Convert.ToInt32(await _context.SystemParameterHeaders.Where(f => f.ParamCode == "GB042").Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken));

                    //active session checking
                    string usingMS = "0"; 
                        //await _context.SystemParameterHeaders.Where(f => f.ParamCode == "GB_MS").Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);
                    ActiveSession cekSession = _context.ActiveSession.Where(f => f.UserId == req.CommandModel.UserName.ToLower()).FirstOrDefault();
                    double totDiffSessionMinues = 0;
                    bool isSuperADmin = false;

                    if (cekSession != null)
                        totDiffSessionMinues = (DateTime.Now - cekSession.SessionDate.Value).TotalMinutes;

                    if (entity.MenuGroup.Equals("SUPER_ADMIN"))
                        isSuperADmin = true;

                    //if (cekSession != null && totDiffSessionMinues < session_timeout && usingMS == "1" && !isSuperADmin)
                    if (cekSession != null 
                        && usingMS == "1" 
                        && !isSuperADmin 
                        && !(cekSession.CreatedHost == req.UserIdentity.Host 
                            && cekSession.Browser == req.CommandModel.Browser))
                    {
                        loginInfo.sts = ErrorMessages.UserActiveSession;
                    }
                    else
                    {
                        //Check IF LDAP
                        string IS_LDAP = _configuration.GetSection("LdapSettings:IS_LDAP").Value;
                        string USE_DS = _configuration.GetSection("LdapSettings:USE_DIRECTORY_SEARCHER").Value;
                        int flag_stat = 0;

                        /**
                         * if super admin ignore ldap setting
                         **/
                     
                        if (IS_LDAP == "1" && !isSuperADmin)
                        {


                            if (USE_DS == "1")
                            {
                                //Flag_stat 1 Success 2 Invalid User 3 Wrong Server Settings
                                flag_stat = _authenticationService.LDAP_Authenticate_DS(req.CommandModel.UserName.ToLower(), req.CommandModel.Password);
                            }
                            else
                            {
                                flag_stat = _authenticationService.LDAP_Authenticate(req.CommandModel.UserName.ToLower(), req.CommandModel.Password);
                            }

                            if (flag_stat == 1)
                            {
                                loginInfo = _authenticationService.GenerateToken(req.CommandModel.UserName.ToLower(), entity.MenuGroup, entity.ModuleAccess, entity.BranchGroup);
                                loginInfo.userName = entity.FirstName;
                                string group = await _context.MenuGroupHeaders.Where(f => f.GroupId == entity.MenuGroup)
                                    .Select(c => c.GroupName).FirstOrDefaultAsync(cancellationToken);

                                loginInfo.groupName = string.IsNullOrEmpty(group) ? "" : group.ToUpper();

                                _context.LogonActivities.Add(new LogonActivity()
                                {
                                    UserId = req.CommandModel.UserName.ToLower(),
                                    LoginStatus = LoginStatus.Success.ToString(),
                                    CreatedBy = req.CommandModel.UserName,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                });

                                //last login.
                                entity.LockCounter = 0; //reset lock.

                                logged = true;
                            }
                            else // fail login
                            {

                                logged = false;

                                if (flag_stat == 2)
                                {
                                    loginInfo.sts = ErrorMessages.LDAPUserInvalid;
                                }
                                else
                                {
                                    loginInfo.sts = ErrorMessages.LDAPSettingsInvalid;
                                }
                            }



                        }
                        else // NON LDAP
                        {
                            if (_authenticationService.Encrypt(req.CommandModel.Password).Equals(entity.Password) && !string.IsNullOrEmpty(req.CommandModel.Password))
                            {
                                //get access matrix.
                                //IList<AccessModel> amx = await (from a in _context.MenuGroupDetails
                                //                                      join m in _context.Menus on a.MenuCode equals m.MenuCode
                                //                                      where a.GroupId == entity.MenuGroup
                                //                                        && string.IsNullOrEmpty(m.MenuUrl) == false
                                //                                        && m.Status == true
                                //                                      select new AccessModel
                                //                                      {
                                //                                          menuUrl = m.MenuUrl,
                                //                                          module = m.Module,
                                //                                          matrix = Convert.ToInt32(a.AllowInsert).ToString() + "|" +
                                //                                          Convert.ToInt32(a.AllowUpdate).ToString() + "|" +
                                //                                          Convert.ToInt32(a.AllowDelete).ToString() + "|" +
                                //                                          Convert.ToInt32(a.AllowExport).ToString() + "|" +
                                //                                          Convert.ToInt32(a.AllowImport).ToString() + "|" +
                                //                                          Convert.ToInt32(a.AllowApproval).ToString()
                                //                                      }).ToListAsync(cancellationToken);

                                //foreach (AccessModel item in amx)
                                //{
                                //    menulist += item.menuUrl.ToLower() + "^";
                                //    access += item.matrix + "^";
                                //    modulelist += item.module + "^";
                                //}
                                //amx = null;

                                loginInfo = _authenticationService.GenerateToken(req.CommandModel.UserName.ToLower(), entity.MenuGroup, entity.ModuleAccess, entity.BranchGroup);
                                loginInfo.userName = entity.FirstName;
                                string group = await _context.MenuGroupHeaders.Where(f => f.GroupId == entity.MenuGroup)
                                    .Select(c => c.GroupName).FirstOrDefaultAsync(cancellationToken);

                                loginInfo.groupName = string.IsNullOrEmpty(group) ? "" : group.ToUpper();

                                _context.LogonActivities.Add(new LogonActivity()
                                {
                                    UserId = req.CommandModel.UserName.ToLower(),
                                    LoginStatus = LoginStatus.Success.ToString(),
                                    CreatedBy = req.CommandModel.UserName,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                });

                                //last login.
                                entity.LockCounter = 0; //reset lock.

                                //check password expired
                                loginInfo.expired = false;
                                int dayExpired = sett.PasswordExpiredDay;
                                if (dayExpired > 0) //check password expired
                                {
                                    if (entity.PasswordExpired == true)
                                    {
                                        loginInfo.expired = true;
                                    }
                                    else
                                    {
                                        DateTime? lastChangePassword = await _context.UserPasswordHistories.Where(f => f.UserId == entity.UserId).Select(c => c.CreatedDate).MaxAsync(cancellationToken);
                                        if (lastChangePassword != null)
                                        {
                                            if (req.CurrentDateTime > Convert.ToDateTime(lastChangePassword).AddDays(dayExpired))
                                            {
                                                loginInfo.expired = true;
                                            }
                                        }
                                        else
                                        {
                                            if (req.CurrentDateTime > Convert.ToDateTime(entity.CreatedDate).AddDays(dayExpired))
                                            {
                                                loginInfo.expired = true;
                                            }
                                        }
                                    }
                                }

                                loginInfo.sts = LoginStatus.Success.ToString();

                                logged = true;
                            }
                            else  //login failed.
                            {

                                logged = false;

                                //lock checking.                        
                                if (sett.LoginMaximumFailed > 0)
                                {
                                    //login lock counter.
                                    Int16? ctr = entity.LockCounter == null ? 0 : entity.LockCounter;
                                    entity.LockCounter = Convert.ToInt16(ctr + 1);

                                    if (entity.LockCounter > sett.LoginMaximumFailed)
                                    {
                                        entity.LockStatus = true;

                                        if (sett.LoginAutomaticUnlock > 0) //when automatic unlock.
                                        {
                                            loginInfo.sts = string.Format(ErrorMessages.LoginLockedAuto, sett.LoginAutomaticUnlock.ToString());
                                        }
                                        else
                                        {
                                            loginInfo.sts = ErrorMessages.LoginLocked;
                                        }
                                    }
                                    else
                                    {
                                        loginInfo.sts = ErrorMessages.LoginInvalid;
                                    }
                                }
                                else
                                {
                                    loginInfo.sts = ErrorMessages.LoginInvalid;
                                }

                            }
                            entity.LastLoginDateTime = req.CurrentDateTime;
                        }

                        if (logged && usingMS == "1")
                        {
                            if (cekSession == null)
                            {
                                _context.ActiveSession.Add(new ActiveSession()
                                {
                                    UserId = req.CommandModel.UserName.ToLower(),
                                    SessionDate = req.CurrentDateTime,
                                    Remarks = null,
                                    CreatedBy = req.CommandModel.UserName,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host,
                                    Browser = req.CommandModel.Browser,
                                });
                            }
                            else
                            {
                                ActiveSession listActSession = _context.ActiveSession.Where(w => w.UserId == req.CommandModel.UserName.ToLower()).FirstOrDefault();
                                listActSession.SessionDate = DateTime.Now;
                                _context.UpdateRange(listActSession);
                            }
                        }
                    }

                    
                }
                else
                {
                    loginInfo.sts = ErrorMessages.UserInactive;
                }
            }
            else //user not found
            {
                loginInfo.sts = ErrorMessages.LoginInvalid;
            }

            //fail login
            if (string.IsNullOrEmpty(loginInfo.token))
            {
                _context.LogonActivities.Add(new LogonActivity()
                {
                    UserId = req.CommandModel.UserName.ToLower(),
                    LoginStatus = LoginStatus.Failed.ToString(),
                    CreatedBy = req.CommandModel.UserName,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
            }

            await _context.SaveChangesAsync(cancellationToken);

            return loginInfo;
        }
    }
}
