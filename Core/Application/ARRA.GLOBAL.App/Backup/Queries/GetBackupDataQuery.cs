﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Backup.Queries
{
    public class GetBackupDataQuery
    {
        public string reportDt { get; set; }
    }

}