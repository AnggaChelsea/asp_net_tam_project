﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Backup.Models;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;

namespace ARRA.GLOBAL.App.Backup.Queries
{
    public class GetBackupDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBackupDataQuery, BackupListModel>, BackupListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetBackupDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<BackupListModel> Handle(QueriesModel<GetBackupDataQuery, BackupListModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters spParam = new DynamicParameters();
            spParam.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.reportDt));
            BackupListModel output = new BackupListModel();
            var spOutput = await _context.Database.GetDbConnection().QueryAsync<BackupModel>("DBO.UDPS_BACKUP", spParam, null, null, CommandType.StoredProcedure);
            output.data = spOutput.ToList();
            return output;
        }
    }
}