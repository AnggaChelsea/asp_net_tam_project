﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Backup.Models
{
    public class BackupListModel
    {
        public IList<BackupModel> data { get; set; }
    }
}
