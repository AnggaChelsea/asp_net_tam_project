﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Backup.Commands
{
    public class BackupCommand
    {
        public long jobId { get; set; }
        public string createdBy { get; set; }
        public string reportDt { get; set; }
        public string uids { get; set; }
    }
}