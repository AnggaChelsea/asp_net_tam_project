﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.Domain.Entities;
using System.Collections.Generic;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace ARRA.GLOBAL.App.Backup.Commands
{
    public class BackupCommandHandler : AppBase, IRequestHandler<CommandsModel<BackupCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public BackupCommandHandler(
            GLOBALDbContext context, IMediator mediator) : base(context)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<BackupCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters spParam = new DynamicParameters();
            spParam.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.CommandModel.reportDt));
            spParam.Add("@UIDS", ARRA.Common.Utility.SafeSqlString(req.CommandModel.uids));
            spParam.Add("@CREATED_BY", ARRA.Common.Utility.SafeSqlString(req.CommandModel.createdBy));
            await _context.Database.GetDbConnection().QueryAsync("DBO.UDPP_BACKUP", spParam, null, null, CommandType.StoredProcedure);

            return new StatusModel
            {
                status = CommandStatus.Success.ToString(),
            };
        }
    }
}