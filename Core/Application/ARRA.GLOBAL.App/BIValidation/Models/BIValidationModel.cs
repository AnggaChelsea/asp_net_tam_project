﻿namespace ARRA.GLOBAL.App.BIValidation.Models
{
    public class BIValidationModel
    {
        public string processType { get; set; }
        public string processNo { get; set; }
        public string processName { get; set; }
        public string etlPackage { get; set; }
    }
}