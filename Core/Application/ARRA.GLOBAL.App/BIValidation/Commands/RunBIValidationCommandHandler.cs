﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Collections.Generic;
using ARRA.MODULE.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System.Data;
using Dapper;

namespace ARRA.GLOBAL.App.BIValidation.Commands
{
    public class ExportReportProgressCommandHandler : AppBase, IRequestHandler<CommandsModel<RunBIValidationCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IWebServiceConnector _webService;
        private readonly IConfiguration _configuration;
        //public ExportReportProgressCommandHandler(
        //    GLOBALDbContext context, IWebServiceConnector webService,IConfiguration configuration) : base(context,configuration)
        //{
        //    _context = context;
        //    _webService = webService;
        //    _configuration = configuration;
        //}
        public ExportReportProgressCommandHandler(
            IWebServiceConnector webService, IConfiguration configuration) : base(configuration)
        {
            _webService = webService;
            _configuration = configuration;
        }

        public async Task<StatusModel> Handle(CommandsModel<RunBIValidationCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel vm = new StatusModel();
            try
            {
                bool isSuccess = true;
                using (GLOBALDbContext ctxGlobal = base.GetGlobalDbContext())
                {
                    string executionDateTime = Convert.ToDateTime(req.CurrentDateTime).ToString("yyyy-MM-dd HH:mm:ss");
                    FormHeader form = await ctxGlobal.FormHeaders.FindAsync(req.CommandModel.formCode);
                    string periodType = form.PeriodType;

                    string periodLaporan = await ctxGlobal.BusinessParamDetails.Where(f => f.ParamCode == ParamCodeCollection.GB205.ToString() && f.ParamValue == form.PeriodType)
                        .Select(c => c.ParamMapping).FirstOrDefaultAsync(cancellationToken);

                    string idPelapor = await ctxGlobal.SystemParameterHeaders.Where(f => f.ParamName == ParamNameCollection.SANDI_BANK.ToString() && f.Module == form.Module)
                        .Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    string ojkFormCode = form.OJKFormCode;

                    string licenceKey = await ctxGlobal.SystemParameterHeaders.Where(f => f.ParamName == ParamNameCollection.BI_VALIDATOR_LICENSE.ToString() && f.Module == form.Module)
                        .Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    string db = "";
                    if (string.IsNullOrEmpty(form.DatabaseName))
                    {
                        db = await ctxGlobal.Modules.Where(f => f.ModuleId == form.Module).Select(c => c.DatabaseName).FirstOrDefaultAsync(cancellationToken);
                    }
                    else
                    {
                        db = form.DatabaseName.Trim();
                    }
                    string tablename = db + ".DBO." + form.TableName;

                    DynamicParameters p = new DynamicParameters();
                    p.Add("@REPORT_DT", req.CommandModel.reportingDate);
                    IEnumerable<string> listBranch = await ctxGlobal.Database.GetDbConnection().QueryAsync<string>("SELECT DISTINCT RG_BRANCH FROM " + tablename + " (NOLOCK) WHERE REPORT_DT=@REPORT_DT", p, null, null, CommandType.Text);
                    p = null;

                    form = null;
                    List<validationReport> result = await _webService.RunBIValidation(executionDateTime, licenceKey, req.CommandModel.reportingDate, periodLaporan, idPelapor, ojkFormCode,listBranch);
                    if (result != null)
                    {
                        if (result.Count > 0)
                        {
                            using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                            {
                                //remove frist
                                List<BIValidationException> listDel = await ctx.BIValidationExceptions.Where(f => f.FormCode == req.CommandModel.formCode && f.ReportDate == Convert.ToDateTime(req.CommandModel.reportingDate)).ToListAsync(cancellationToken);
                                ctx.BIValidationExceptions.RemoveRange(listDel);
                                await ctx.SaveChangesAsync(cancellationToken);
                                listDel = null;

                                foreach (validationReport item in result)
                                {
                                    isSuccess = false;

                                    ctx.BIValidationExceptions.Add(new MODULE.Domain.Entities.BIValidationException
                                    {
                                        ValidationStatus = item.validationStatus,
                                        ValidationTest = item.validationTest,
                                        FormCode = req.CommandModel.formCode,
                                        ReportDate = Convert.ToDateTime(req.CommandModel.reportingDate),
                                        PeriodType = periodType,
                                        IdPelapor = idPelapor,
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }
                                await ctx.SaveChangesAsync(cancellationToken);
                            }
                        }
                    }
                    result = null;
                    listBranch = null;

                    if (isSuccess)
                    {
                        vm.status = CommandStatus.Success.ToString();
                    }
                    else
                    {
                        vm.status = CommandStatus.Failed.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            
            return vm;
        }
    }
}