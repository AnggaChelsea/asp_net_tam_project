﻿namespace ARRA.GLOBAL.App.BIValidation.Commands
{
    public class RunBIValidationCommand
    {
        public string reportingDate { get; set; }
        public string formCode { get; set; }
        public string module { get; set; }
    }
}