﻿namespace ARRA.GLOBAL.App.BIValidation.Queries
{
    public class GetBIValidationByNoQuery
    {
        public string processNo { get; set; }
        public string module { get; set; }
    }
}