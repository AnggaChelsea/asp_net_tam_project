﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.BIValidation.Models;
using System;

namespace ARRA.GLOBAL.App.BIValidation.Queries
{
    public class GetBIValidationByNoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBIValidationByNoQuery, BIValidationModel>, BIValidationModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetBIValidationByNoQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}
        public GetBIValidationByNoQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<BIValidationModel> Handle(QueriesModel<GetBIValidationByNoQuery, BIValidationModel> req, CancellationToken cancellationToken)
        {
            BIValidationModel m = new BIValidationModel();
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                m = await (from p in ctx.BIValidationTools
                           where p.ProcessNo == req.QueryModel.processNo
                           select new BIValidationModel
                           {
                               processNo = p.ProcessNo,
                               processName = p.ProcessName,
                               etlPackage = p.ETLPackage
                           }).FirstOrDefaultAsync(cancellationToken);
            }

            //_context.Dispose();
            return m;
        }
    }
}