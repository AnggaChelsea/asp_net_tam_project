﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Tasks.Models;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.Tasks.Queries
{
    public class GetTotalRejectedQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetTotalRejectedQuery, TotalItemModel>, TotalItemModel>
    {
        private readonly GLOBALDbContext _context;

        public GetTotalRejectedQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<TotalItemModel> Handle(QueriesModel<GetTotalRejectedQuery, TotalItemModel> req, CancellationToken cancellationToken)
        {
            int total = await (from a in _context.ApprovalHeaders
                               where a.LinkToHeaderId == null
                               && a.CreatedBy == req.UserIdentity.UserId
                               && a.ApprovalStatus == ApprovalStatus.Rejected.ToString()
                               select a.UniqueId).CountAsync(cancellationToken);

            return new TotalItemModel
            {
                records = total
            };
        }
    }
}