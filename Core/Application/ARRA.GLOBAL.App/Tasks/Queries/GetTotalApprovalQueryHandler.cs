﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Tasks.Models;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.App.Tasks.Queries
{
    public class GetTotalApprovalQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetTotalApprovalQuery, TotalItemModel>, TotalItemModel>
    {
        private readonly GLOBALDbContext _context;

        public GetTotalApprovalQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<TotalItemModel> Handle(QueriesModel<GetTotalApprovalQuery, TotalItemModel> req, CancellationToken cancellationToken)
        {
            int total = await (from a in _context.ApprovalHeaders
                               join u in _context.Users
                                on a.CreatedBy equals u.UserId
                               join ua in _context.ApprovalGroupDetails
                                on u.ApprovalGroup equals ua.GroupId
                               where ua.UserId == req.UserIdentity.UserId
                                && a.LinkToHeaderId == null
                                && a.ApprovalStatus == ApprovalStatus.Pending.ToString()
                                && a.TaskType == TaskType.Approval.ToString()
                               select a.UniqueId
                               ).Distinct().CountAsync(cancellationToken);

            return new TotalItemModel
            {
                records = total
            };

        }
    }
}