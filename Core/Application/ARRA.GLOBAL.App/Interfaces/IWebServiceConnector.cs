﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.App.Interfaces
{
    public interface IWebServiceConnector
    {
        Task<List<validationReport>> RunBIValidation(string executionDateTime, string licenceKey,
            string reportingDate, string periodLaporan, string idPelapor, string ojkFormCode, IEnumerable<string> branches);
    }
}