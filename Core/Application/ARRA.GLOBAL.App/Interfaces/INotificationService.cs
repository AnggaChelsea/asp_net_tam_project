﻿using System;
using System.Collections.Generic;
using System.Text;
using ARRA.GLOBAL.App.Notifications.Models;

namespace ARRA.GLOBAL.App.Interfaces
{
    public interface INotificationService
    {
        void Send(Message message);
    }
}
