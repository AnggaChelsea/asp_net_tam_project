﻿using ARRA.GLOBAL.App.Authentications.Models;

namespace ARRA.GLOBAL.App.Interfaces
{
    public interface IAuthenticationService
    {
        LoginModel GenerateToken(string userid, string role,string moduleaccess, string branchgroup);
        string Encrypt(string text);
        int LDAP_Authenticate(string distinguishedName, string password);
        int LDAP_Authenticate_DS(string distinguishedName, string password);
    }
}
