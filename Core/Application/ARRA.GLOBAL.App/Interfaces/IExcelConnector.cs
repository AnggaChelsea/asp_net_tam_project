﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;

namespace ARRA.GLOBAL.App.Interfaces
{
    public interface IExcelConnector
    {
        void WriteExcel(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath);
        void WriteExcelValidation(IDataReader rd, List<dynamic> rdVal, IList<FormDetail> listColumn, string fileFullPath);
        void WriteExcel(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath);
        void WriteExcelJObject(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath);
        Task<long> WriteExcelJObjectCompare(IList<dynamic> data, string fileFullPath);
        Task<long> WriteExcelCompare(IDbConnection con, IList<FormDetail> listColumn, string fileFullPathImport, string fileFullPathExport,string tableName,string formCode);
        Task<long> ImportData(IDbConnection con, IConfiguration conf, UserIdentityModel user, string tableName, IList<FormDetail> listColumn, string fileFullPath, long logId, string formCode, string fieldPK, string detailTableName, string detailFKField, string parentTable, string parentKey, string parentFkKey, bool? CheckBIBranch, bool? CheckLocalBranch, List<string> listAuthBranch);
        Task<long> ManualUpload(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId,string actionType);
        void GenerateManualUploadTemplate(IList<ManualUploadDetail> listColumn, string fileFullPath);
        void WriteExcelTemplate(string templatePath, string outputPath, string sheetName, List<dynamic> cellValueList);
        void WriteExcelDtlBatch(IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn, string formCode, string outputPath, string outputName, long totalRecord);
        void WriteExcelBatch(IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn, string formCode, string outputPath, string outputName, long totalRecord, int maxRow, int maxPage);
    }
}