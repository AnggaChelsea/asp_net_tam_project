﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Notifications.Models;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.App.Interfaces
{
    public interface ITextFileConnector
    {
        void WriteCSV(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath);
        void WriteFixLength(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath);
        void WriteDelimited(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath,string delimiter);
        void WriteCSV(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath);
        void WriteFixLength(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath);
        void WriteDelimited(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath, string delimiter);

        void WriteCSVRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath,string delimiter,bool hasHeader);
        void WriteDelimitedRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter, bool hasHeader);
        void WriteFixLengthRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath);
        Task<long> ManualUpload(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId, string actionType, string delimiter);
        Task<long> ManualUploadFixLength(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId, string actionType);
        Task<long> UploadErrorUJK(IDbConnection con, UserIdentityModel user, string fileFullPath, string reportDate, string formCode);
        Task<long> UploadDbl(IDbConnection con, UserIdentityModel user, string fileFullPath, string reportDate, string formCode);
        Task<long> UploadErrorAntasena(IDbConnection con, UserIdentityModel user, string fileFullPath);
        void GenerateTemplate(IList<ManualUploadDetail> listColumns, string fileFullPath, string fileType, string delimiter);
        List<string> WriteDelimitedRegulatorWithHdrAndSplit(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter, string header, int maxRow, long totalRecord);
        void WriteCSVBatch(IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn, string formCode, string outputPath, string outputName, long totalRecord, int maxRow, int maxPage);
    }
}