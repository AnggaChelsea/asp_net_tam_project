﻿namespace RR.Application.GenerateTF.Queries
{
    public class GetReportProgressQuery
    {
        public string module { get; set; }
        public string periodType { get; set; }
    }
}