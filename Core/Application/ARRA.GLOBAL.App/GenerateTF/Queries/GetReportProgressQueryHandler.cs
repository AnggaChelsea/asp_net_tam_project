﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Menus.Models;
using RR.Application.GenerateTF.Queries;
using ARRA.GLOBAL.App.GenerateTF.Models;
using Microsoft.Extensions.Configuration;
using System;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.GenerateTF.Queries
{
    public class GetReportProgressQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetReportProgressQuery, ReportProgressModel>, ReportProgressModel>
    {
        private readonly GLOBALDbContext _context;

        public GetReportProgressQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<ReportProgressModel> Handle(QueriesModel<GetReportProgressQuery, ReportProgressModel> req, CancellationToken cancellationToken)
        {
            ReportProgressModel vm = new ReportProgressModel();

            IList<TMP_FORM> forms = await _context.FormHeaders.Where(f => f.Module == req.QueryModel.module
                    && f.PeriodType == req.QueryModel.periodType)
                    .Select(c => new TMP_FORM
                    {
                        formCode = c.FormCode,
                        ojkCode = c.OJKFormCode,
                        formName = c.FormName
                    }).OrderBy(o => o.ojkCode).ToListAsync(cancellationToken);

            string but = await _context.Modules.Where(f => f.ModuleId == req.QueryModel.module).Select(c => c.BusinessType).FirstOrDefaultAsync(cancellationToken);

            IList<BranchModel> branches = null;

            if (req.QueryModel.periodType == PeriodType.DAILY.ToString()
                    || req.QueryModel.periodType == PeriodType.WEEKLY.ToString()
                    || req.QueryModel.periodType == PeriodType.QUARTERLY.ToString())
            { //lapor konsol.
                string konsolBranch = await _context.SystemParameterHeaders.Where(f => f.ParamName == SystemParameterType.KONSOLIDASI_BR.ToString()
                    && f.Module == req.QueryModel.module).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);
                branches = await _context.RegulatorBranchs
                    .Where(f => f.Unit == but && f.BranchCode == konsolBranch)
                    .Select(c => new BranchModel
                    {
                        branch = c.BranchCode,
                        name = c.BranchName
                    }
                    ).ToListAsync(cancellationToken);
            }
            else
            {
                branches = await _context.RegulatorBranchs
                    .Where(f => f.Unit == but).OrderBy(o => o.BranchCode)
                    .Select(c => new BranchModel
                    {
                        branch = c.BranchCode,
                        name = c.BranchName
                    }
                    ).ToListAsync(cancellationToken);
            }
            DateTime rptDt;
            IList<TMP_TF> txts = new List<TMP_TF>();

            
            if (req.QueryModel.periodType.ToUpper() == PeriodType.ADHOC.ToString() ||
                req.QueryModel.periodType.ToUpper() == PeriodType.DAILY.ToString())
            {
                rptDt = await _context.ReportingDates.Where(f => f.Module == req.QueryModel.module && f.PeriodType == PeriodType.DAILY.ToString()).Select(c => c.ReportDate).FirstOrDefaultAsync(cancellationToken);
                using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
                {
                    txts = await ctx.GenerateTextFiles
                            .Where(f => f.PeriodType == req.QueryModel.periodType && f.Active == true 
                                // impact migration
                                //&& Convert.ToDateTime(f.CreatedDate).ToString("yyyyMMdd") == rptDt.ToString("yyyyMMdd"))
                                && f.CreatedDate.Value.Year == rptDt.Year
                                && f.CreatedDate.Value.Month == rptDt.Month
                                && f.CreatedDate.Value.Day == rptDt.Day)
                            .Select(c => new TMP_TF
                            {
                                formCode = c.FormCode,
                                branch = c.BranchCode
                            }).ToListAsync(cancellationToken);

                }
            }
            else
            {
                rptDt = await _context.ReportingDates.Where(f => f.Module == req.QueryModel.module && f.PeriodType == req.QueryModel.periodType).Select(c => c.ReportDate).FirstOrDefaultAsync(cancellationToken);
                using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
                {
                    txts = await ctx.GenerateTextFiles
                            .Where(f => f.PeriodType == req.QueryModel.periodType && f.Active == true && f.ReportDate == rptDt)
                            .Select(c => new TMP_TF
                            {
                                formCode = c.FormCode,
                                branch = c.BranchCode
                            }).ToListAsync(cancellationToken);

                }

            }

            IList<dynamic> result = new List<dynamic>();
            foreach (TMP_FORM fr in forms)
            {
                dynamic item = new System.Dynamic.ExpandoObject();
                IDictionary<string, object> dic = item;
                dic.Add("FORM", fr.formName);
                foreach (BranchModel br in branches)
                {
                    //txts.Where(f => f.formCode == fr.formCode && f.branch == br.branch).FirstOrDefault(c => c.reportDt)
                    dic.Add(br.branch, txts.Where(f => f.formCode == fr.formCode && f.branch == br.branch).Count() > 0 ? 1 : 0);
                }
                
                result.Add(item);
                item = null;
                dic = null;
            }
            forms = null;
            txts = null;

            vm.branch = branches;
            vm.data = result;
            vm.reportDate = rptDt.ToString("d MMM yyyy");
            result = null;
            branches = null;

            return vm;
        }
    }
    public class TMP_FORM
    {
        public string formCode { get; set; }
        public string ojkCode { get; set; }
        public string formName { get; set; }
    }
    public class TMP_TF
    {
        public DateTime? reportDt { get; set; }
        public string formCode { get; set; }
        public string branch { get; set; }
    }
}