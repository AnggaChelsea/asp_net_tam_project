﻿using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.GenerateTF.Models
{
    public class ReportProgressModel
    {
        public IList<dynamic> data { get; set; }
        public IList<BranchModel> branch { get; set; }
        public string reportDate { get; set; }
    }
}
