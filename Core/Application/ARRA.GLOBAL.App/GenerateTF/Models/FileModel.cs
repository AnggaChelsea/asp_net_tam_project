﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.GenerateTF.Models
{
    public class FileModel
    {
        public string fileName { get; set; }
        public string fullPath { get; set; }
    }
}