﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.App.GenerateTF.Models;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using Newtonsoft.Json.Linq;

namespace ARRA.GLOBAL.App.GenerateTF.Commands
{
    public class ExportReportProgressCommandHandler : AppBase, IRequestHandler<CommandsModel<ExportReportProgressCommand, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IExcelConnector _excel;
        public ExportReportProgressCommandHandler(
            GLOBALDbContext context, IExcelConnector excel,IConfiguration configuration) : base(context,configuration)
        {
            _context = context;
            _excel = excel;
        }

        public async Task<FileModel> Handle(CommandsModel<ExportReportProgressCommand, FileModel> req, CancellationToken cancellationToken)
        {
            FileModel vm = new FileModel();
            try
            {
                string filePath = await _context.SystemParameterHeaders.Where(f => f.Module == ModuleType.GLOBAL.ToString() && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString()).Select(c=>c.ParamValue).FirstOrDefaultAsync(cancellationToken);
                string fileName = "ReportProgress_"+req.CurrentDateTime.Ticks.ToString()+".xlsx";

                IList<TMP_FORM> forms = await _context.FormHeaders.Where(f => f.Module == req.CommandModel.module && f.PeriodType == req.CommandModel.periodType)
                .Select(c => new TMP_FORM
                {
                    formCode = c.FormCode,
                    ojkCode = c.OJKFormCode,
                    formName = c.FormName
                }).OrderBy(o => o.ojkCode).ToListAsync(cancellationToken);

                string but = await _context.Modules.Where(f => f.ModuleId == req.CommandModel.module).Select(c => c.BusinessType).FirstOrDefaultAsync(cancellationToken);
                IList<BranchModel> branches = null;

                if (req.CommandModel.periodType == PeriodType.DAILY.ToString()
                   || req.CommandModel.periodType == PeriodType.WEEKLY.ToString()
                   || req.CommandModel.periodType == PeriodType.QUARTERLY.ToString())
                { //lapor konsol.
                    string konsolBranch = await _context.SystemParameterHeaders.Where(f => f.ParamName == SystemParameterType.KONSOLIDASI_BR.ToString()
                        && f.Module == req.CommandModel.module).Select(c => c.ParamValue).FirstOrDefaultAsync(cancellationToken);

                    branches = await _context.RegulatorBranchs
                        .Where(f => f.Unit == but && f.BranchCode == konsolBranch)
                        .Select(c => new BranchModel
                        {
                            branch = c.BranchCode,
                            name = c.BranchName
                        }
                        ).ToListAsync(cancellationToken);
                }
                else
                {
                    branches = await _context.RegulatorBranchs
                        .Where(f => f.Unit == but).OrderBy(o => o.BranchCode)
                        .Select(c => new BranchModel
                        {
                            branch = c.BranchCode,
                            name = c.BranchName
                        }
                        ).ToListAsync(cancellationToken);
                }

                DateTime rptDt;

                IList<TMP_TF> txts = new List<TMP_TF>();
                if (req.CommandModel.periodType.ToUpper() == PeriodType.ADHOC.ToString() ||
                req.CommandModel.periodType.ToUpper() == PeriodType.DAILY.ToString())
                {
                    rptDt = await _context.ReportingDates.Where(f => f.Module == req.CommandModel.module && f.PeriodType == PeriodType.DAILY.ToString()).Select(c => c.ReportDate).FirstOrDefaultAsync(cancellationToken);
                    using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                    {
                        txts = await ctx.GenerateTextFiles
                                .Where(f => f.PeriodType == req.CommandModel.periodType && f.Active == true
                                    && Convert.ToDateTime(f.CreatedDate).ToString("yyyyMMdd") == rptDt.ToString("yyyyMMdd"))
                                .Select(c => new TMP_TF
                                {
                                    formCode = c.FormCode,
                                    branch = c.BranchCode
                                }).ToListAsync(cancellationToken);

                    }
                }
                else
                {
                    rptDt = await _context.ReportingDates.Where(f => f.Module == req.CommandModel.module && f.PeriodType == req.CommandModel.periodType).Select(c => c.ReportDate).FirstOrDefaultAsync(cancellationToken);
                    using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                    {
                        txts = await ctx.GenerateTextFiles
                            .Where(f => f.PeriodType == req.CommandModel.periodType && f.Active == true && f.ReportDate == rptDt)
                            .Select(c => new TMP_TF
                            {
                                formCode = c.FormCode,
                                branch = c.BranchCode
                            }).ToListAsync(cancellationToken);
                    }

                }

                IList<FormDetail> listColumns = new List<FormDetail>();
                listColumns.Add(new FormDetail { GrdColumnName = "Form Code", FieldName = "FORM_CD", EdrControlType=ControlType.TEXT.ToString() });
                listColumns.Add(new FormDetail { GrdColumnName = "Form", FieldName = "FORM", EdrControlType = ControlType.TEXT.ToString() });
                IList<dynamic> result = new List<dynamic>();
                bool isFirst = true;
                foreach (TMP_FORM fr in forms)
                {
                    JObject item = new JObject();
                    item.Add("FORM_CD", fr.ojkCode);
                    item.Add("FORM", fr.formName);
                    foreach (BranchModel br in branches)
                    {
                        if (isFirst)
                        {
                            listColumns.Add(new FormDetail { GrdColumnName = br.branch+ " - "+ br.name, FieldName = br.branch, EdrControlType=ControlType.TEXT.ToString() });
                        }
                        item.Add(br.branch, txts.Where(f => f.formCode == fr.formCode && f.branch == br.branch).Count() > 0 ? ReportStatus.DONE.ToString() : ReportStatus.PROGRESS.ToString());
                    }
                    isFirst = false;
                    result.Add(item);
                    item = null;
                }

                forms = null;
                branches = null;
                txts = null;

                _excel.WriteExcelJObject(result, listColumns, filePath + fileName);

                listColumns = null;
                result = null;
                vm.fileName = fileName;
                vm.fullPath = filePath + fileName;
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return vm;
        }
    }
    public class TMP_FORM
    {
        public string formCode { get; set; }
        public string ojkCode { get; set; }
        public string formName { get; set; }
    }
    public class TMP_TF
    {
        public string formCode { get; set; }
        public string branch { get; set; }
    }
}