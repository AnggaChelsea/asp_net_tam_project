﻿namespace ARRA.GLOBAL.App.GenerateTF.Commands
{
    public class ExportReportProgressCommand
    {
        public string module { get; set; }
        public string periodType { get; set; }
    }
}