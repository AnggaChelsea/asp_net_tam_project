﻿using Dapper;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Jurnal.Models;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Commands
{
    public class ProcessGLCommandHandler : AppBase, IRequestHandler<CommandsModel<ProcessGLCommand, JurnalStatusModel>, JurnalStatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;

        public ProcessGLCommandHandler(
            GLOBALDbContext context, IMediator mediator, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<JurnalStatusModel> Handle(CommandsModel<ProcessGLCommand, JurnalStatusModel> req, CancellationToken cancellationToken)
        {
            Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
            if( menu != null)
            {
                ApprovalHeader mHeader = new ApprovalHeader
                {
                    MenuCode = menu.MenuCode,
                    MenuName = menu.MenuName,
                    ActionType = "PROCESS",
                    ApprovalStatus = ApprovalStatus.Pending.ToString(),
                    Module = menu.Module,
                    RefferalId = Convert.ToInt64(0),
                    TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                    FormCode = "F_ANGLADJ",
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host,
                    PeriodType = ""
                };

                _context.ApprovalHeaders.Add(mHeader);

                await _context.SaveChangesAsync(cancellationToken);
                Int64 headerid = mHeader.UniqueId;

                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "TANGGAL",
                    FormColName = "TANGGAL",
                    FormSeq = Convert.ToInt16(1),
                    HeaderId = headerid,
                    ValueAfter = req.CommandModel.tanggal,
                    ValueBefore = String.Empty,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "PERIOD",
                    FormColName = "PERIOD",
                    FormSeq = Convert.ToInt16(2),
                    HeaderId = headerid,
                    ValueAfter = req.CommandModel.period,
                    ValueBefore = String.Empty,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                // create excel 
                long downloadId = 0;
                CommandsModel<ExportDataCommand, StatusModel> cmd = new CommandsModel<ExportDataCommand, StatusModel>();
                cmd.UserIdentity = new UserIdentityModel
                {
                    UserId = req.UserIdentity.UserId,
                    Host = req.UserIdentity.Host
                };
                cmd.CurrentDateTime = req.CurrentDateTime;
                cmd.CommandModel = new ExportDataCommand
                {
                    fileType = "EXCEL",
                    filter = "[{'field':'TANGGAL','optr':'EQUALS','value':'2020-11-30','datatype':'DATE'}]",
                    formCode = "F_ANGLADJ",
                    sort = "UID asc",
                    jobId = 0
                };
                StatusModel stsM = await _mediator.Send(cmd);
                if (stsM != null)
                {
                    downloadId = stsM.id;
                }

                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "EXCELID",
                    FormColName = "EXCELID",
                    FormSeq = Convert.ToInt16(3),
                    HeaderId = headerid,
                    ValueAfter = downloadId.ToString(),
                    ValueBefore = String.Empty,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "BERITAACARA",
                    FormColName = "BERITAACARA",
                    FormSeq = Convert.ToInt16(4),
                    HeaderId = headerid,
                    ValueAfter = req.CommandModel.filepath,
                    ValueBefore = String.Empty,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                await _context.SaveChangesAsync(cancellationToken);

                try
                {
                    if (!req.AccessMatrix.AllowApproval)
                    {
                        DynamicParameters p = new DynamicParameters();
                        p.Add("@APPV_ID", headerid);
                        p.Add("@USR_ID", req.UserIdentity.UserId);
                        p.Add("@HOST", req.UserIdentity.Host);
                        p.Add("@APPROVAL", req.AccessMatrix.AllowApproval);

                        await _context.Database.GetDbConnection().ExecuteAsync("DBO.UDPU_EXECUTE_GL_ADJUSTMENT", p, null, null, CommandType.StoredProcedure);
                    }

                }
                catch (SqlException ex)
                {
                    throw new DatabaseException(ex.Number, ex.Message);
                }
            }
            else
            {
                throw new NotFoundException("Menu not found", "not found");
            }

            return new JurnalStatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}
