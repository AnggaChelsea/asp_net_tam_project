﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Jurnal.Models
{
    public class BeritaAcaraModel
    {
        public int UID { get; set; }
        public string Tanggal { get; set; }
        public string Period { get; set; }
        public string FilePath { get; set; }
    }
}
