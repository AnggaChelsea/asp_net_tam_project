﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Jurnal.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Queries
{
    public class GetBeritaAcaraFileByAppvIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBeritaAcaraFileByAppvIdQuery, FileModel>, FileModel>
    {
        public GetBeritaAcaraFileByAppvIdQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public async Task<FileModel> Handle(QueriesModel<GetBeritaAcaraFileByAppvIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            string fileName = "";
            string filePath = "";
            string err = "";
            string ext = "";

            IEnumerable<BeritaAcaraModel> beritaAcara;

            using (MODULEDbContext ctx = base.GetDbContext("ANTASENA_BUS"))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@APPVID", req.QueryModel.id);
                filePath = await ctx.Database.GetDbConnection().QueryFirstOrDefaultAsync<string>("DBO.UDPS_GET_BERITA_ACARA_FILE_BY_APPV", p, null, null, CommandType.StoredProcedure);
            }

            if (filePath != null)
            {
                fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1, filePath.Length - (filePath.LastIndexOf('\\') + 1));
                ext = fileName.Substring(fileName.LastIndexOf('.') + 1, fileName.Length - (fileName.LastIndexOf('.') + 1));
                fileName = fileName.Substring(0, fileName.LastIndexOf('_'));
                fileName = fileName + "." + ext;
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath
            };
        }
    }
}
