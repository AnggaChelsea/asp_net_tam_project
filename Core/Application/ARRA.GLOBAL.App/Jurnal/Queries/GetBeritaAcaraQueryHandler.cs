﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jurnal.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Queries
{
    public class GetBeritaAcaraQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBeritaAcaraQuery, BeritaAcaraListModel>, BeritaAcaraListModel>
    {
        public GetBeritaAcaraQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public async Task<BeritaAcaraListModel> Handle(QueriesModel<GetBeritaAcaraQuery, BeritaAcaraListModel> req, CancellationToken cancellationToken)
        {
            IEnumerable<BeritaAcaraModel> beritaAcara;

            using (MODULEDbContext ctx = base.GetDbContext("ANTASENA_BUS"))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@TANGGAL", req.QueryModel.tanggal);
                p.Add("@PERIOD", req.QueryModel.period);
                beritaAcara = await ctx.Database.GetDbConnection().QueryAsync<BeritaAcaraModel>("DBO.UDPS_GET_BERITA_ACARA", p, null, null, CommandType.StoredProcedure);
            }

            //foreach (var item in pivotData)
            //{
            //    vm.Pivot.Add(new PivotModel
            //    {
            //        GlNumber = item.GlNumber,
            //        Debit = item.Debit,
            //        Credit = item.Credit,
            //        Total = item.Total
            //    });
            //}

            //return vm;

            return new BeritaAcaraListModel
            {
                BeritaAcara = beritaAcara.AsList()
            };
        }
    }
}
