﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jurnal.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Queries
{
    public class GetGLPivotQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetGLPivotQuery, PivotListModel>, PivotListModel>
    {
        public GetGLPivotQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public async Task<PivotListModel> Handle(QueriesModel<GetGLPivotQuery, PivotListModel> req, CancellationToken cancellationToken)
        {
            //PivotListModel vm = new PivotListModel();
            IEnumerable<PivotModel> pivotData;

            using (MODULEDbContext ctx = base.GetDbContext("ANTASENA_BUS"))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@TANGGAL", req.QueryModel.tanggal);
                pivotData = await ctx.Database.GetDbConnection().QueryAsync<PivotModel>("DBO.UDPS_GET_GL_PIVOT", p, null, null, CommandType.StoredProcedure);
            }

            //foreach (var item in pivotData)
            //{
            //    vm.Pivot.Add(new PivotModel
            //    {
            //        GlNumber = item.GlNumber,
            //        Debit = item.Debit,
            //        Credit = item.Credit,
            //        Total = item.Total
            //    });
            //}

            //return vm;

            return new PivotListModel
            {
                Pivot = pivotData.AsList()
            };
        }
    }
}
