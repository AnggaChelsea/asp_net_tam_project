﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jurnal.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Queries
{
    public class GetMissingGLQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetMissingGLQuery, GLListModel>, GLListModel>, IDisposable
    {
        public GetMissingGLQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public void Dispose()
        {
            //_context.Dispose();
        }

        public async Task<GLListModel> Handle(QueriesModel<GetMissingGLQuery, GLListModel> req, CancellationToken cancellationToken)
        {
            IEnumerable<string> missingGl;

            using (MODULEDbContext ctx = base.GetDbContext("ANTASENA_BUS"))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@TANGGAL", req.QueryModel.tanggal);
                p.Add("@PERIOD", req.QueryModel.period);
                missingGl = await ctx.Database.GetDbConnection().QueryAsync<string>("DBO.UDPS_GET_MISSING_GL", p, null, null, CommandType.StoredProcedure);
            }

            return new GLListModel
            {
                gl = missingGl.AsList()
            };
        }
    }
}
