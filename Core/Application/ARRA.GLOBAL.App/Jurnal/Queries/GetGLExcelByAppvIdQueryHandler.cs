﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ARRA.Common;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Jurnal.Queries
{
    public class GetGLExcelByAppvIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetGLExcelByAppvIdQuery, FileModel>, FileModel>
    {
        private readonly GLOBALDbContext _context;

        public GetGLExcelByAppvIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<FileModel> Handle(QueriesModel<GetGLExcelByAppvIdQuery, FileModel> req, CancellationToken cancellationToken)
        {
            string dtlId = "0";
            string fieldValue = "EXCELID";

            dtlId = await (from a in _context.ApprovalHeaders
                           join b in _context.ApprovalDetails
                            on a.UniqueId equals b.HeaderId
                           where a.UniqueId == req.QueryModel.id
                            && b.FieldName == fieldValue
                           select b.ValueAfter).FirstOrDefaultAsync(cancellationToken);

            ExportFileList exp;
            FormHeader form = await _context.FormHeaders.FindAsync("F_ANGLADJ");
            if (form.ExportDirect == true)
            {
                exp = await _context.ExportFileLists.FindAsync(Convert.ToInt64(dtlId));
            }
            else
            {
                exp = await _context.ExportFileLists.Where(f => f.JobQueueId == Convert.ToInt64(dtlId)).FirstOrDefaultAsync(cancellationToken);
            }

            string fileName = "";
            string filePath = "";
            string err = "";
            if (exp != null)
            {
                if (exp.CreatedBy == req.UserIdentity.UserId)
                {
                    if (exp != null)
                    {
                        if (!string.IsNullOrEmpty(exp.FileName))
                        {
                            //Get File Path
                            string path = await _context.SystemParameterHeaders.Where(f => f.Module == form.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                                .Select(c => c.ParamValue)
                                .FirstOrDefaultAsync(cancellationToken);

                            fileName = exp.FileName;
                            filePath = path;
                        }
                        else if (exp.Status == JobStatus.Failed.ToString())
                        {
                            err = exp.Message;
                        }
                    }
                }
                else
                {
                    //prevent unaothorized action 
                    throw new Exception(ErrorMessages.NotAUthorizedToAction);
                }
            }

            return new FileModel
            {
                fileName = fileName,
                path = filePath
            };
        }
    }
}
