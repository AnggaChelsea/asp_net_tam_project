﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Approvals.Models;

namespace ARRA.GLOBAL.App.Approvals.Queries
{
    public class GetApprovalByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetApprovalByIdQuery, ApprovalModel>, ApprovalModel>
    {
        private readonly GLOBALDbContext _context;

        public GetApprovalByIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<ApprovalModel> Handle(QueriesModel<GetApprovalByIdQuery, ApprovalModel> req, CancellationToken cancellationToken)
        {
            return await (from a in _context.ApprovalHeaders
                                               where a.UniqueId == req.QueryModel.id
                                               select new ApprovalModel
                                               {
                                                   module = a.Module,
                                                   taskType = a.TaskType,
                                                   actionType = a.ActionType,
                                                   menuCode = a.MenuCode,
                                                   status = a.ApprovalStatus
                                               }).FirstOrDefaultAsync(cancellationToken);

        }
    }
}