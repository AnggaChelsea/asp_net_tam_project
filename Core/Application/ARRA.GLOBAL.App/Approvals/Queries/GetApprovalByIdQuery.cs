﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Approvals.Queries
{
    public class GetApprovalByIdQuery
    {
        public long id { get; set; }
    }
}