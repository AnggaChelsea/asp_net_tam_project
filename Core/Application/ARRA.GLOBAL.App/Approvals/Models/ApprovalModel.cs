﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Approvals.Models
{
    public class ApprovalModel
    {
        public string module { get; set; }
        public string menuCode { get; set; }
        public string taskType { get; set; }
        public string actionType { get; set; }
        public string status { get; set; }
    }
}
