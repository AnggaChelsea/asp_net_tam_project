﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.ProcessDataSource.Queries
{
    public class GetETLVariableByProcessNoQuery
    {
        public string module { get; set; }
        public string processNo { get; set; }
    }
}
