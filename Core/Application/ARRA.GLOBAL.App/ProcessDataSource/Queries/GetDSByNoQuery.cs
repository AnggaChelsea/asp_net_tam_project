﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessDataSource.Queries
{
    public class GetDSByNoQuery
    {
        public string processNo { get; set; }
    }
}