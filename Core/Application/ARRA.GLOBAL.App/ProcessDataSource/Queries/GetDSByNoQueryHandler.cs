﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ProcessDataSource.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.ProcessDataSource.Queries
{
    public class GetDSByNoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDSByNoQuery, DataSourceModel>, DataSourceModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetDSByNoQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetDSByNoQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<DataSourceModel> Handle(QueriesModel<GetDSByNoQuery, DataSourceModel> req, CancellationToken cancellationToken)
        {
            DataSourceModel m = new DataSourceModel();
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                m = await (from p in globalCtx.ProcessDataSources
                           where p.ProcessNo == req.QueryModel.processNo
                           select new DataSourceModel
                           {
                               processNo = p.ProcessNo,
                               processName = p.ProcessName,
                               fileName = p.FileName,
                               fileNameFormat = p.FileNameDateFormat,
                               etlPackage = p.ETLPackage,
                               usingTF = p.UsingTF,
                               as32Bit = ARRA.Common.ConvertValue.ToBoolean(p.As32Bit)
                           }).FirstOrDefaultAsync(cancellationToken);
            }
            return m;
        }
    }
}