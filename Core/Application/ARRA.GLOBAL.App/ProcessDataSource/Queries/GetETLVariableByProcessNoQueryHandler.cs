﻿using ARRA.GLOBAL.App.ProcessDataSource.Models;
using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.ProcessDataSource.Queries
{
    public class GetETLVariableByProcessNoQueryHandler
        : AppBase,
            IRequestHandler<
                QueriesModel<GetETLVariableByProcessNoQuery, IList<ETLVariableModel>>,
                IList<ETLVariableModel>
            >
    {
        public GetETLVariableByProcessNoQueryHandler(IConfiguration configuration)
            : base(configuration) { }

        public async Task<IList<ETLVariableModel>> Handle(
            QueriesModel<GetETLVariableByProcessNoQuery, IList<ETLVariableModel>> req,
            CancellationToken cancellationToken
        )
        {
            var listVariables = new List<ETLVariableModel>();

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@procNo", req.QueryModel.processNo);

                string queryVariableModel = string.Format(
                    "SELECT mepv.PROC_NO as ProcNo "
                        + "     , mepv.VARIABLE_NM AS Name "
                        + "     , mepv.VARIABLE_VAL AS Value "
                        + " FROM dbo.MS_ETL_PROCESS_VARIABLE AS mepv "
                        + " WHERE mepv.PROC_NO = @procNo"
                );

                var variableModel = await ctx.Database
                    .GetDbConnection()
                    .QueryAsync<ETLVariableModel>(
                        queryVariableModel,
                        p,
                        null,
                        null,
                        CommandType.Text
                    );

                listVariables = variableModel.ToList();
            }

            return listVariables;
        }
    }
}
