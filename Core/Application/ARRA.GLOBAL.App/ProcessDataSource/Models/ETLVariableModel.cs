﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.ProcessDataSource.Models
{
    public class ETLVariableModel
    {
        public string ProcNo { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
