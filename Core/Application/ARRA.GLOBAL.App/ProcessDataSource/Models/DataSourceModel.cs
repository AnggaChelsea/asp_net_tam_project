﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessDataSource.Models
{
    public class DataSourceModel
    {
        public string processNo { get; set; }
        public string processName { get; set; }
        public string fileName { get; set; }
        public string fileNameFormat { get; set; }
        public string etlPackage { get; set; }
        public bool usingTF { get; set; }
        public bool as32Bit { get; set; }
    }
}
