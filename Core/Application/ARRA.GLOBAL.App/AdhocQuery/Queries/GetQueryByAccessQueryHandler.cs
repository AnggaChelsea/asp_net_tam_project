﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.AdhocQuery.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetQueryByAccessQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetQueryByAccessQuery, QueryListModel>, QueryListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetQueryByAccessQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<QueryListModel> Handle(QueriesModel<GetQueryByAccessQuery, QueryListModel> req, CancellationToken cancellationToken)
        {
            QueryListModel vm = new QueryListModel();
            if (req.AccessMatrix.AllowInsert || req.AccessMatrix.AllowUpdate || req.AccessMatrix.AllowDelete)
            {
                vm.data =
                await (from a in _context.AdhocQueries
                       orderby a.QueryName
                       select new QueryLookupModel
                       {
                           id = a.UniqueId,
                           name = a.QueryName,
                           desc = a.QueryDesc
                       }).ToListAsync(cancellationToken);
            }
            else
            {
                vm.data =
                await (from a in _context.AdhocQueries
                       join b in _context.AdhocQueryAccesses
                             on a.UniqueId equals b.QueryId
                       where b.GroupId == req.UserIdentity.GroupId
                       orderby a.QueryName
                       select new QueryLookupModel
                       {
                           id = a.UniqueId,
                           name = a.QueryName,
                           desc = a.QueryDesc
                       }).ToListAsync(cancellationToken);
            }

            return vm;
        }
    }
}