﻿namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetQueryResultQuery
    {
        public string query { get; set; }
        public string isServerSide { get; set; }
        public string page { get; set; }
        public string pageSize { get; set; }
        public string sort { get; set; }
    }
}