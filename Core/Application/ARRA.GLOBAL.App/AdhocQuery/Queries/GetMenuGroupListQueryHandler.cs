﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.AdhocQuery.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetMenuGroupListQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetMenuGroupListQuery, MenuGroupListModel>, MenuGroupListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetMenuGroupListQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<MenuGroupListModel> Handle(QueriesModel<GetMenuGroupListQuery, MenuGroupListModel> req, CancellationToken cancellationToken)
        {
            MenuGroupListModel vm = new MenuGroupListModel();

            vm.data = await _context.MenuGroupHeaders.Select(c => new MenuGroupModel
            {
                groupId = c.GroupId,
                name = c.GroupName
            }).OrderBy(o => o.name).ToListAsync(cancellationToken);

            return vm;
        }
    }
}