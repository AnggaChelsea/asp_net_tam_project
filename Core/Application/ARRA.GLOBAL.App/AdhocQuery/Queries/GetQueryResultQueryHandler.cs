﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.AdhocQuery.Models;
using System.Collections.Generic;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using System;


namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetQueryResultQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetQueryResultQuery, QueryResultModel>, QueryResultModel>
    {
        private readonly GLOBALDbContext _context;

        public GetQueryResultQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<QueryResultModel> Handle(QueriesModel<GetQueryResultQuery, QueryResultModel> req, CancellationToken cancellationToken)
        {
            QueryResultModel vm = new QueryResultModel();
            if(req.QueryModel.query.ToUpper().Contains("INSERT ") || req.QueryModel.query.ToUpper().Contains("UPDATE ") || req.QueryModel.query.ToUpper().Contains("DELETE "))
            {
                throw new System.Exception("You dont have authorization to perform insert, update and delete");
            }

            long totalRecord = 0;
            using (GLOBALDbContext ctx = base.GetGlobalReadDbContext())
            {
                if (req.QueryModel.isServerSide == "YES")
                {
                    string queryCount = "select count(1) TOTAL_RECORD from (" + req.QueryModel.query + ") a";
                    dynamic recordsCounter = await ctx.Database.GetDbConnection().QueryAsync(queryCount, null, null, null, CommandType.Text);
                    totalRecord = (long) recordsCounter[0].TOTAL_RECORD;
                    recordsCounter = null;
                    vm.totalRecord = totalRecord;

                    req.QueryModel.query = "select * from (" + req.QueryModel.query + ") a";
                    if (req.QueryModel.sort != "")
                    {
                        req.QueryModel.query += " order by " + req.QueryModel.sort;
                    } else
                    {
                        req.QueryModel.query += " order by 1";
                    }                    
                    req.QueryModel.query += " offset " + req.QueryModel.page + " rows fetch next " + req.QueryModel.pageSize + " rows only";
                }
            }

            using (GLOBALDbContext ctx = base.GetGlobalReadDbContext())
            {
                vm.result = await ctx.Database.GetDbConnection().QueryAsync(req.QueryModel.query, null, null, null, CommandType.Text);
                //var tmpRes = await ctx.Database.GetDbConnection().QueryAsync(req.QueryModel.query, null, null, null, CommandType.Text);
                //vm.result = tmpRes.AsQueryable().Take(20).ToList();
            }

            return vm;
        }
    }
}