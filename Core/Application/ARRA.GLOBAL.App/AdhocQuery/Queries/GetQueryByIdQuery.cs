﻿namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetQueryByIdQuery
    {
        public long id { get; set; }
    }
}