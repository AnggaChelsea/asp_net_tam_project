﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.AdhocQuery.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.AdhocQuery.Queries
{
    public class GetQueryByIdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetQueryByIdQuery, QueryInfoModel>, QueryInfoModel>
    {
        private readonly GLOBALDbContext _context;

        public GetQueryByIdQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<QueryInfoModel> Handle(QueriesModel<GetQueryByIdQuery, QueryInfoModel> req, CancellationToken cancellationToken)
        {
            QueryInfoModel vm = new QueryInfoModel();
            if (req.AccessMatrix.AllowInsert || req.AccessMatrix.AllowUpdate || req.AccessMatrix.AllowDelete)
            {
                vm.qinfo =
                await (from a in _context.AdhocQueries
                       where a.UniqueId == req.QueryModel.id
                       select new QueryModel
                       {
                           id = a.UniqueId,
                           name = a.QueryName,
                           desc = a.QueryDesc,
                           query = a.Query
                       }).FirstOrDefaultAsync(cancellationToken);                
            }
            else
            {
                vm.qinfo =
                await (from a in _context.AdhocQueries
                       join b in _context.AdhocQueryAccesses
                             on a.UniqueId equals b.QueryId
                       where b.GroupId == req.UserIdentity.GroupId && a.UniqueId == req.QueryModel.id
                       select new QueryModel
                       {
                           id = a.UniqueId,
                           name = a.QueryName,
                           desc = a.QueryDesc,
                           query = a.Query
                       }).FirstOrDefaultAsync(cancellationToken);
            }

            if (vm.qinfo != null)
            {
                vm.param = await _context.AdhocQueryParams.Where(f => f.QueryId == vm.qinfo.id).Select(c =>
                new QueryParamModel
                {
                    id = c.ParamId,
                    pname = c.ParamName,
                    ptype = c.ParamType,
                    pvalue = c.DefaultValue
                }).ToListAsync(cancellationToken);

                vm.access = await _context.AdhocQueryAccesses.Where(f => f.QueryId == vm.qinfo.id).Select(c =>
                new QueryAccessModel
                {
                    groupId = c.GroupId
                }).ToListAsync(cancellationToken);
            }

            return vm;
        }
    }
}