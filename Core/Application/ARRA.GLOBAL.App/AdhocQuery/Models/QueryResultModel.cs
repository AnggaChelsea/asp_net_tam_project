﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryResultModel
    {
        public dynamic result { get; set; }
        public long totalRecord { get; set; }
        public string errmessage { get; set; }
    }
}