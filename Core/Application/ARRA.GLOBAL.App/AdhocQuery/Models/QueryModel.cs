﻿namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryModel
    {
        public long id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public string query { get; set; }
    }
}
