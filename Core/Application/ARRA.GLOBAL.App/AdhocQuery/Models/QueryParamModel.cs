﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryParamModel
    {
        public string id { get; set; }
        public string pname { get; set; }
        public string ptype { get; set; }
        public string pvalue { get; set; }
    }
}