﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class MenuGroupListModel
    {
        public IList<MenuGroupModel> data { get; set; }
    }
}
