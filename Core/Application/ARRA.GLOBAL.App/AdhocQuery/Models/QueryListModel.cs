﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryListModel
    {
        public IList<QueryLookupModel> data { get; set; }
    }
}
