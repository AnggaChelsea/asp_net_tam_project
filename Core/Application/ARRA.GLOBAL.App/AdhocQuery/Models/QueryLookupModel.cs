﻿namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryLookupModel
    {
        public long id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
    }
}