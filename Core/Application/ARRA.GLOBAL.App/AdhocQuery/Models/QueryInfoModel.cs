﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class QueryInfoModel
    {
        public QueryModel qinfo { get; set; }
        public IList<QueryParamModel> param { get; set; }
        public IList<QueryAccessModel> access { get; set; }
    }
}