﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AdhocQuery.Models
{
    public class MenuGroupModel
    {
        public string groupId { get; set; }
        public string name { get; set; }
    }
}
