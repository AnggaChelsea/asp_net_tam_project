﻿using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace ARRA.GLOBAL.App.AdhocQuery.Commands
{
    public class RemoveQueryCommandHandler : AppBase, IRequestHandler<CommandsModel<RemoveQueryCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RemoveQueryCommandHandler(
            GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RemoveQueryCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    // remove query
                    Domain.Entities.AdhocQuery qry = _context.AdhocQueries.Where(f => f.UniqueId == req.CommandModel.id).FirstOrDefault();
                    _context.AdhocQueries.Remove(qry);

                    IList<Domain.Entities.AdhocQueryAccess> accQry = _context.AdhocQueryAccesses.Where(a => a.QueryId == req.CommandModel.id).ToList();
                    foreach (var item in accQry)
                    {
                        _context.AdhocQueryAccesses.Remove(item);
                    }

                    await _context.SaveChangesAsync(cancellationToken);

                    scope.Complete();

                    return new StatusModel
                    {
                        status = CommandStatus.Success.ToString()
                    };
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
