﻿using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace ARRA.GLOBAL.App.AdhocQuery.Commands
{
    public class SaveQueryCommandHandler : AppBase, IRequestHandler<CommandsModel<SaveQueryCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveQueryCommandHandler(
            GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveQueryCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            using(TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    // save or edit query
                    if (req.CommandModel.id == 0)
                    {
                        Domain.Entities.AdhocQuery adqry = new Domain.Entities.AdhocQuery
                        {
                            QueryName = req.CommandModel.name,
                            QueryDesc = req.CommandModel.desc,
                            Query = req.CommandModel.query,
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host
                        };

                        _context.AdhocQueries.Add(adqry);

                        await _context.SaveChangesAsync(cancellationToken);

                        var uniqId = adqry.UniqueId;

                        foreach (var item in req.CommandModel.groups)
                        {
                            _context.AdhocQueryAccesses.Add(new Domain.Entities.AdhocQueryAccess
                            {
                                QueryId = uniqId,
                                GroupId = item,
                                CreatedBy = req.UserIdentity.UserId,
                                CreatedDate = req.CurrentDateTime,
                                CreatedHost = req.UserIdentity.Host
                            });
                        }
                    }
                    else
                    {
                        Domain.Entities.AdhocQuery qry = _context.AdhocQueries.Where(f => f.UniqueId == req.CommandModel.id).FirstOrDefault();
                        qry.QueryName = req.CommandModel.name;
                        qry.QueryDesc = req.CommandModel.desc;
                        qry.Query = req.CommandModel.query;
                        qry.ModifiedBy = req.UserIdentity.UserId;
                        qry.ModifiedDate = req.CurrentDateTime;
                        qry.ModifiedHost = req.UserIdentity.Host;

                        IList<Domain.Entities.AdhocQueryAccess> accs = _context.AdhocQueryAccesses.Where(f => f.QueryId == req.CommandModel.id).ToList();

                        // delete access
                        foreach (var item in accs)
                        {
                            if (!req.CommandModel.groups.Contains(item.GroupId))
                            {
                                _context.AdhocQueryAccesses.Remove(item);
                            }
                        }

                        // add access
                        foreach (var item in req.CommandModel.groups)
                        {
                            if (!(accs.Where(a => a.GroupId == item).Count() > 0))
                            {
                                _context.AdhocQueryAccesses.Add(new Domain.Entities.AdhocQueryAccess
                                {
                                    QueryId = req.CommandModel.id,
                                    GroupId = item,
                                    CreatedBy = req.UserIdentity.UserId,
                                    CreatedDate = req.CurrentDateTime,
                                    CreatedHost = req.UserIdentity.Host
                                });
                            }
                        }
                    }

                    await _context.SaveChangesAsync(cancellationToken);

                    scope.Complete();

                    return new StatusModel
                    {
                        status = CommandStatus.Success.ToString()
                    };
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
