﻿using ARRA.GLOBAL.App.AdhocQuery.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.AdhocQuery.Commands
{
    public class SaveQueryCommand
    {
        public long id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public string query { get; set; }
        public string[] groups { get; set; }
    }
}
