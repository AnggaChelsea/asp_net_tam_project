﻿using System;

namespace ARRA.GLOBAL.App.Exceptions
{
    public class CustomException : Exception
    {
        public string CustomMessage { get; set; }
        public CustomException(string message) : base(message)
        {
            CustomMessage = message;
        }
    }
}