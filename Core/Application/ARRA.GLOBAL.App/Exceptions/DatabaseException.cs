﻿using System;

namespace ARRA.GLOBAL.App.Exceptions
{
    public class DatabaseException : Exception
    {
        public string CustomMessage { get; set; }
        public DatabaseException(int errorNumber, string message) : base("Data Save Failed")
        {
            CustomMessage = message;
            switch (errorNumber)
            {
                case 2627:
                    CustomMessage = "Data with this key already exists, please input another one";
                    break;
                case 241:
                    CustomMessage = "Invalid date/time format, please check the data";
                    break;
                case 8114:
                    CustomMessage = "Invalid number/money format, please check the data";
                    break;
            }
        }
    }
}