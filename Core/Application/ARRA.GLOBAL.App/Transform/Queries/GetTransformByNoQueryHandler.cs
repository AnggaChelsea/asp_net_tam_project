﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Transform.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Transform.Queries
{
    public class GetTransformByNoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetTransformByNoQuery, TransformModel>, TransformModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetTransformByNoQueryHandler(GLOBALDbContext context,IConfiguration configuration) : base(context,configuration)
        //{
        //    _context = context;
        //}
        public GetTransformByNoQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<TransformModel> Handle(QueriesModel<GetTransformByNoQuery, TransformModel> req, CancellationToken cancellationToken)
        {
            TransformModel m = new TransformModel();
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                m = await (from p in ctx.TransformProcesses
                           where p.ProcessNo == req.QueryModel.processNo
                           select new TransformModel
                           {
                               processNo = p.ProcessNo,
                               processName = p.ProcessName,
                               etlPackage = p.ETLPackage,
                               as32Bit = ARRA.Common.ConvertValue.ToBoolean(p.As32Bit)
                           }).FirstOrDefaultAsync(cancellationToken);
            }
            return m;
        }
    }
}