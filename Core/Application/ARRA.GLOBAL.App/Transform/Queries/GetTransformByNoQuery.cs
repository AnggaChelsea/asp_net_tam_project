﻿namespace ARRA.GLOBAL.App.Transform.Queries
{
    public class GetTransformByNoQuery
    {
        public string processNo { get; set; }
        public string module { get; set; }
    }
}