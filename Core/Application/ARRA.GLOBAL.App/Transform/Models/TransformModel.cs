﻿namespace ARRA.GLOBAL.App.Transform.Models
{
    public class TransformModel
    {
        public string processType { get; set; }
        public string processNo { get; set; }
        public string processName { get; set; }
        public string etlPackage { get; set; }
        public bool as32Bit { get; set; }
    }
}