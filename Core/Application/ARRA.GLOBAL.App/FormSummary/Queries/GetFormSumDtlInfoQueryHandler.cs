﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Models;
using Microsoft.Extensions.Configuration;
using System.Linq;
using ARRA.GLOBAL.App.FormSummary.Models;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDtlInfoQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumDtlInfoQuery, FormSumDataListModel>, FormSumDataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumDtlInfoQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FormSumDataListModel> Handle(QueriesModel<GetFormSumDtlInfoQuery, FormSumDataListModel> req, CancellationToken cancellationToken)
        {
            var form = await _context.FormHeaders.FindAsync(req.QueryModel.FormCode);

            if (form == null)
            {
                throw new NotFoundException(nameof(FormHeader), req.QueryModel.FormCode);
            }

            MODULEDbContext moduleCtx = base.GetDbContext(form.Module);
            var reportRowCol = await moduleCtx.ReportRowColumns.Where(f => 
                f.ReportCd == req.QueryModel.FormCode && 
                f.RowSeq == req.QueryModel.Row && 
                f.ColumnSeq == req.QueryModel.Column).FirstOrDefaultAsync(cancellationToken);

            var reportRow = await moduleCtx.ReportRows.Where(f =>
                f.ReportCd == req.QueryModel.FormCode &&
                f.RowSeq == req.QueryModel.Row).FirstOrDefaultAsync(cancellationToken);
            
            string formCdDtl = reportRow.FormCodeDetail;

            if (string.IsNullOrEmpty(formCdDtl))
            {
                var reportMaster = await moduleCtx.ReportMasters.FindAsync(req.QueryModel.FormCode);
                formCdDtl = reportMaster.FormCdDtl;
            }

            IList<GridModel> grid = await (from a in _context.FormDetails
                                           where a.FormCode == formCdDtl && a.GrdColumnShow == true 
                                           orderby a.GrdColumnSeq
                                           select new GridModel
                                           {
                                               caption = a.GrdColumnName,
                                               field = a.FieldName,
                                               datatype = a.EdrControlType,
                                               format = a.EdrFormatExpression,
                                               aggregate = a.GrdAggregate,
                                               lCode = a.EdrLookupCode
                                           }).ToListAsync(cancellationToken);
            return new FormSumDataListModel
            {
                data = grid,
                reportRowCol = reportRowCol
            };
        }
    }
}