﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDataQuery
    {
        public string formCode { get; set; }
        public String reportDt { get; set; }
        public String tableCd { get; set; }
    }

}