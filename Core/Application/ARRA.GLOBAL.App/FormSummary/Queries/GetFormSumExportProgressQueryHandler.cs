﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.Domain.Entities;
using System.Linq;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumExportProgressQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumExportProgressQuery, FormSumExportProgressModel>, FormSumExportProgressModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumExportProgressQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumExportProgressModel> Handle(QueriesModel<GetFormSumExportProgressQuery, FormSumExportProgressModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
            string status = "";
            string message = "";
            string percentage = "";

            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                //MODULE.Domain.Entities.ExportFileList explst = await ctx.ExportFileLists.FindAsync(req.QueryModel.exportId);
                var explst = ctx.ExportFileLists.Where(f => f.JobQueueId == req.QueryModel.exportId).FirstOrDefault();
                if (explst != null)
                {
                    status = explst.Status != null ? explst.Status : "";
                    message = explst.Message != null ? explst.Message : "";
                    if(explst.TotalRecord > 0)
                    {
                        percentage = ((explst.TotalInserted / explst.TotalRecord) * 100).ToString();

                    } else
                    {
                        percentage = "0";
                    }
                } else
                {
                    status = "Pending";
                    message = "";
                    percentage = "0";
                }
            }
            
            return new FormSumExportProgressModel
            {
                status = status,
                message = message,
                percentage = percentage
            };
        }
    }
}