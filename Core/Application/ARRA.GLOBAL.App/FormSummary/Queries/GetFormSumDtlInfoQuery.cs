﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDtlInfoQuery
    {
        public string FormCode { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
    }

}