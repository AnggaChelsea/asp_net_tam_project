﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using ARRA.Common.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ARRA.GLOBAL.App.Forms.Models;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumDataQuery, FormSumDataListModel>, FormSumDataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumDataListModel> Handle(QueriesModel<GetFormSumDataQuery, FormSumDataListModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);
            var tblSttic = await (from a in _context.SystemParameterDetails
                                     where a.ParamCode.Equals("RWASTCL") && a.ParamValue == req.QueryModel.formCode
                                     select a.ParamMapping).FirstOrDefaultAsync(cancellationToken);

            List < dynamic > data = new List<dynamic>();
            IEnumerable<ReportStaticCellValueModel> staticCellsValue = new List<ReportStaticCellValueModel> { };

            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                DynamicParameters dpTable = new DynamicParameters();
                dpTable.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                var tables = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE", dpTable, null, null, CommandType.StoredProcedure);
                if (tables.ToList().Count() == 0)
                {
                    DynamicParameters dpTableCol= new DynamicParameters();
                    dpTableCol.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                    dpTableCol.Add("@TBL_CD", "");
                    var columns = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_COL", dpTableCol, null, null, CommandType.StoredProcedure);

                    DynamicParameters dpTableRow = new DynamicParameters();
                    dpTableRow.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                    dpTableRow.Add("@REPORT_DT", req.QueryModel.reportDt);
                    dpTableRow.Add("@TBL_CD", "");
                    var rows = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_ROW", dpTableRow, null, null, CommandType.StoredProcedure);

                    Dictionary<string, dynamic> tmpDict = new Dictionary<string, dynamic>();
                    tmpDict.Add("id", "tblDefault");
                    tmpDict.Add("rows", rows);
                    tmpDict.Add("columns", columns);

                    data.Add(tmpDict);
                } else
                {
                    // get table row col
                    DynamicParameters dpTableRowCol = new DynamicParameters();
                    dpTableRowCol.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                    var rowCols = await ctx.Database.GetDbConnection().QueryAsync<TABLE_ROW_COL>("DBO.UDPS_GET_TABLE_ROW_COL", dpTableRowCol, null, null, CommandType.StoredProcedure);

                    for (int i = 0;i < tables.ToList().Count();i++)
                    {
                        dynamic table = tables.ToList()[i];
                        string tableCd = table.TBL_CD;
                        DynamicParameters dpTableCol = new DynamicParameters();
                        dpTableCol.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                        dpTableCol.Add("@TBL_CD", tableCd);
                        var columns = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_COL", dpTableCol, null, null, CommandType.StoredProcedure);

                        DynamicParameters dpTableRow = new DynamicParameters();
                        dpTableRow.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                        dpTableRow.Add("@REPORT_DT", req.QueryModel.reportDt);
                        dpTableRow.Add("@TBL_CD", tableCd);
                        var rows = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_ROW", dpTableRow, null, null, CommandType.StoredProcedure);

                        var dtRows = new DataTable();

                        if (rows.Count() != 0)
                        {
                            dtRows = ConvertExtensions.ToDataTable(rows.ToList());

                            // foreach rows
                            for (int j = 0; j < rows.ToList().Count(); j++)
                            {
                                dynamic row = rows.ToList()[j];
                                int tr_seq = row.TR_SEQ;
                                // foreach columns
                                for (int k = 0; k < columns.ToList().Count(); k++)
                                {
                                    dynamic column = columns.ToList()[k];
                                    string column_alias = column.COLUMN_ALIAS;

                                    // skip if column_alis is NO, COMPONENT_CD, TR_DESCRIPTION and TR_SEQ
                                    if (column_alias == "NO" || column_alias == "COMPONENT_CD" || column_alias == "TR_DESCRIPTION" || column_alias == "TR_SEQ")
                                    {
                                        continue;
                                    }

                                    var curRowCOl = rowCols.Where(x => x.ROW_SEQ == Convert.ToString(tr_seq) && x.COLUMN_DEST == column_alias).FirstOrDefault();
                                    if (!dtRows.Columns.Contains(column_alias + "_HAS_DETAIL"))
                                        dtRows.Columns.Add(new DataColumn(column_alias + "_HAS_DETAIL"));

                                    if (curRowCOl != null)
                                    {
                                        if (string.IsNullOrEmpty(curRowCOl.COLUMN_STATIC) && string.IsNullOrEmpty(curRowCOl.RECALCULATE_TYPE))
                                        {
                                            dtRows.Rows[j][column_alias + "_HAS_DETAIL"] = "1";
                                        }
                                        else
                                        {
                                            dtRows.Rows[j][column_alias + "_HAS_DETAIL"] = "0";
                                        }
                                    }
                                }
                            }
                        }


                        Dictionary<string, dynamic> tmpDict = new Dictionary<string, dynamic>();
                        tmpDict.Add("id", tableCd);
                        tmpDict.Add("description", table.TBL_DESC);
                        tmpDict.Add("rows", rows);
                        tmpDict.Add("dtRows", dtRows);
                        tmpDict.Add("columns", columns);

                        data.Add(tmpDict);
                    }
                }

                // get static cell value
                string qryStaticCell = @"SELECT UID AS UniqueId " +
	                                    " ,REPORT_DT AS ReportDate " +
	                                    " ,ROW_SEQ AS RowSequence " +
                                        " ,CELL_POSITION AS CellPosition " +
                                        " ,CELL_TYPE AS CellType " +
                                        " ,CELL_VALUE AS CellValue " +
                                    "FROM dbo." + tblSttic + " " +
                                    "WHERE REPORT_DT = '" + req.QueryModel.reportDt + "'";
                staticCellsValue = await ctx.Database.GetDbConnection().QueryAsync<ReportStaticCellValueModel>(qryStaticCell, null, null, null, CommandType.Text);
            }

            return new FormSumDataListModel
            {
                data = data,
                reportStaticCellValue = staticCellsValue,
            };
        }
    }

    public class TABLE_ROW_COL
    {
        public int UID { get; set; }
        public string REPORT_CD { get; set; }
        public string ROW_SEQ { get; set; }
        public string COLUMN_SEQ { get; set; }
        public string COLUMN_FORMULA { get; set; }
        public string COLUMN_STATIC { get; set; }
        public string COLUMN_AGGREGATE { get; set; }
        public string IS_RECALCULATE { get; set; }
        public string RECALCULATE_SEQ { get; set; }
        public string RECALCULATE_TYPE { get; set; }
        public string DIVISOR_AFTER_AGGREGATE { get; set; }
        public string ROUNDED_AFTER_AGGREGATE { get; set; }
        public string ABSOLUTE_AFTER_AGGREGATE { get; set; }
        public string ZERO_WHEN_NEGATIVE { get; set; }
        public string COLUMN_DEST { get; set; }
    }
}