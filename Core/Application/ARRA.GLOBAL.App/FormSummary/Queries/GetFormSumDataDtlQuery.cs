﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDataDtlQuery
    {
        public string formCode { get; set; }
        public String reportDt { get; set; }
        public String row { get; set; }
        public String column { get; set; }
        public String start { get; set; }
        public String pageSize { get; set; }
        public String sort { get; set; }
        public String filter { get; set; }
    }

}