﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.FormSummary.Queries
{
    public class GetFormSumDataDtlQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumDataDtlQuery, FormSumDataListModel>, FormSumDataListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumDataDtlQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumDataListModel> Handle(QueriesModel<GetFormSumDataDtlQuery, FormSumDataListModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.QueryModel.formCode);

            dynamic data = null;
            dynamic recordsTotal = null;

            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                DynamicParameters dpTableDtl = new DynamicParameters();
                dpTableDtl.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.formCode));
                dpTableDtl.Add("@REPORT_DT", req.QueryModel.reportDt);
                dpTableDtl.Add("@ROW", req.QueryModel.row);
                dpTableDtl.Add("@COLUMN", req.QueryModel.column);
                dpTableDtl.Add("@PAGE_SIZE", req.QueryModel.pageSize);
                dpTableDtl.Add("@PAGE", req.QueryModel.start);
                dpTableDtl.Add("@SORT", req.QueryModel.sort);
                dpTableDtl.Add("@FILTER", req.QueryModel.filter);
                dpTableDtl.Add("@IS_COUNT", "1");
                var countList = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_DETAIL", dpTableDtl, null, null, CommandType.StoredProcedure);
                recordsTotal = countList.ToList()[0].RECORD_TOTAL;

                dpTableDtl.Add("@IS_COUNT", "0");
                data = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_DETAIL", dpTableDtl, null, null, CommandType.StoredProcedure);
            }

            return new FormSumDataListModel
            {
                data = data,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsTotal
            };
        }
    }
}