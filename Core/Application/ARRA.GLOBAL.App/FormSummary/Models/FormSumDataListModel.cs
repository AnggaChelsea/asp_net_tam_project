﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Models
{
    public class FormSumDataListModel 
    {
        public dynamic data { get; set; }
        public dynamic reportRowCol { get; set; }
        public dynamic recordsTotal { get; set; }
        public dynamic recordsFiltered { get; set; }
        public dynamic reportStaticCellValue { get; set; }
    }
}