﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummary.Models
{
    public class FormSumExportProgressModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public string percentage { get; set; }
    }
}