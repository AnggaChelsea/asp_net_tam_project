﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using JobQueueDetail = ARRA.MODULE.Domain.Entities.JobQueueDetail;

namespace ARRA.GLOBAL.App.FormSummary.Commands
{
    public class CreateExportTaskFormSummaryDtlCommandHandler : AppBase, IRequestHandler<CommandsModel<CreateExportTaskFormSummaryDtlCommand, ExportProgressModel>, ExportProgressModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;

        public CreateExportTaskFormSummaryDtlCommandHandler(
            GLOBALDbContext context,  IMediator mediator, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ExportProgressModel> Handle(CommandsModel<CreateExportTaskFormSummaryDtlCommand, ExportProgressModel> req, CancellationToken cancellationToken)
        {
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }
            long jobQueueId = 0;

            using (MODULEDbContext ctx = base.GetDbContext(form.Module))
            {
                MODULE.Domain.Entities.JobQueueHeader header = new MODULE.Domain.Entities.JobQueueHeader
                {
                    JobType = QueueJobType.EXPORT_TEMPLATE_DTL.ToString(),
                    PeriodType = form.PeriodType,
                    Status = JobStatus.Temp.ToString(),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                };

                ctx.JobQueueHeaders.Add(header);
                await ctx.SaveChangesAsync(cancellationToken);

                jobQueueId = header.UniqueId;

                List<JobQueueDetail> listDetail = new List<JobQueueDetail>();
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FORM_CD",
                    ParamValue = req.CommandModel.formCode,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILTER",
                    ParamValue = req.CommandModel.filter,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "SORT",
                    ParamValue = req.CommandModel.sort,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                listDetail.Add(new JobQueueDetail
                {
                    HeaderId = jobQueueId,
                    ParamName = "FILE_TYPE",
                    ParamValue = req.CommandModel.fileType,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });

                ctx.JobQueueDetails.AddRange(listDetail);
                header.Status = JobStatus.Pending.ToString();

                await ctx.SaveChangesAsync(cancellationToken);
            }

            return new ExportProgressModel
            {
                status = CommandStatus.Success.ToString(),
                jobId = jobQueueId.ToString()
            };
        }

    }
}