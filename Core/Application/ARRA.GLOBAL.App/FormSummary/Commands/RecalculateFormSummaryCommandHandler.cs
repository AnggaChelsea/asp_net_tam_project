﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using JobQueueDetail = ARRA.MODULE.Domain.Entities.JobQueueDetail;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Linq;

namespace ARRA.GLOBAL.App.FormSummary.Commands
{
    public class RecalculateFormSummaryCommandHandler : AppBase, IRequestHandler<CommandsModel<RecalculateFormSummaryCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;

        public RecalculateFormSummaryCommandHandler(
            GLOBALDbContext context,  IMediator mediator, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<RecalculateFormSummaryCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };
            FormHeader form = await _context.FormHeaders.FindAsync(req.CommandModel.formCode);
            if (form == null)
            {
                throw new NotFoundException(typeof(FormHeader).Name, form);
            }
            Menu menu = await _context.Menus.Where(m => m.FormCode == form.FormCode).FirstOrDefaultAsync(cancellationToken);

            try
            {
                ApprovalHeader mHeader = new ApprovalHeader
                {
                    MenuCode = menu.MenuCode,
                    MenuName = menu.MenuName,
                    ActionType = "RECALC_PROCESS",
                    ApprovalStatus = ApprovalStatus.Pending.ToString(),
                    Module = form.Module,
                    RefferalId = Convert.ToInt64(0),
                    TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                    FormCode = req.CommandModel.formCode,
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host,
                    PeriodType = form.PeriodType
                };

                await _context.ApprovalHeaders.AddAsync(mHeader);
                await _context.SaveChangesAsync();

                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "MODULE",
                    FormColName = "MODULE",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = Convert.ToString(form.Module),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "REPORT_DT",
                    FormColName = "REPORT_DT",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = Convert.ToString(req.CommandModel.reportDt),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "FORM_CD",
                    FormColName = "FORM_CD",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "ROW_SEQ",
                    FormColName = "ROW_SEQ",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = Convert.ToString(req.CommandModel.row),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "COLUMN_SEQ",
                    FormColName = "COLUMN_SEQ",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = Convert.ToString(req.CommandModel.column),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                _context.ApprovalDetails.Add(new ApprovalDetail
                {
                    FieldName = "NEW_VALUE",
                    FormColName = "NEW_VALUE",
                    HeaderId = mHeader.UniqueId,
                    ValueAfter = Convert.ToString(req.CommandModel.value),
                    ValueBefore = Convert.ToString(""),
                    CreatedBy = req.UserIdentity.UserId,
                    CreatedDate = req.CurrentDateTime,
                    CreatedHost = req.UserIdentity.Host
                });
                await _context.SaveChangesAsync();

                statusModel.message = "Recalculate process has submitted and wait for approval.";
                if (!req.AccessMatrix.AllowApproval)
                {
                    statusModel.message = "Recalculate process has submitted.";
                    mHeader.ApprovalStatus = ApprovalStatus.Approved.ToString();
                    mHeader.ModifiedBy = req.UserIdentity.UserId;
                    mHeader.ModifiedDate = req.CurrentDateTime;
                    mHeader.ModifiedHost = req.UserIdentity.Host;

                    using (MODULEDbContext ctx = base.GetDbContext(form.Module))
                    {
                        DynamicParameters dpTable = new DynamicParameters();
                        dpTable.Add("@REPORT_DT", req.CommandModel.reportDt);
                        dpTable.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.CommandModel.formCode));
                        dpTable.Add("@ROW_SEQ", req.CommandModel.row);
                        dpTable.Add("@COLUMN_SEQ", req.CommandModel.column);
                        dpTable.Add("@NEW_VALUE", req.CommandModel.value);
                        await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPP_REPORT_MANUAL_RECALCULATE", dpTable, null, null, CommandType.StoredProcedure);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return statusModel;
        }

    }
}