﻿using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummary.Commands
{
    public class CreateExportTaskFormSummaryCommand
    {
        public string formCode { get; set; }
        public string reportDt { get; set; }
        public IList<ReportStaticCellModel> staticCell { get; set; }
    }
}