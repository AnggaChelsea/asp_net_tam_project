﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummary.Commands
{
    public class CreateExportTaskFormSummaryDtlCommand
    {
        public string formCode { get; set; }
        public string fileType { get; set; }
        public string filter { get; set; }
        public string sort { get; set; }
    }
}