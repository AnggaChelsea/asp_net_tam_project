﻿using ARRA.GLOBAL.App.Forms.Models;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummary.Commands
{
    public class RecalculateFormSummaryCommand
    {
        public string formCode { get; set; }
        public string reportDt { get; set; }
        public string row { get; set; }
        public string column { get; set; }
        public string value { get; set; }
    }
}