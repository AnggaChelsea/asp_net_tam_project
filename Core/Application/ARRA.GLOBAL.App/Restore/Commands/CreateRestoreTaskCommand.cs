﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Restore.Commands
{
    public class CreateRestoreTaskCommand
    {
        public string reportDt { get; set; }
        public List<string> uids { get; set; }
    }
}