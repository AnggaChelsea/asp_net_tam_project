﻿namespace ARRA.GLOBAL.App.Restore.Commands
{
    public class RestoreCommand
    {
        public long jobId { get; set; }
        public string createdBy { get; set; }
        public string reportDt { get; set; }
        public string uids { get; set; }
    }
}