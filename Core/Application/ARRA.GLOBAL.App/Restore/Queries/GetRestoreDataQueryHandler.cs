﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Restore.Models;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;

namespace ARRA.GLOBAL.App.Restore.Queries
{
    public class GetRestoreDataQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetRestoreDataQuery, RestoreListModel>, RestoreListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetRestoreDataQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<RestoreListModel> Handle(QueriesModel<GetRestoreDataQuery, RestoreListModel> req, CancellationToken cancellationToken)
        {
            DynamicParameters spParam = new DynamicParameters();
            spParam.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.reportDt));
            RestoreListModel output = new RestoreListModel();
            var spOutput = await _context.Database.GetDbConnection().QueryAsync<RestoreModel>("DBO.UDPS_RESTORE", spParam, null, null, CommandType.StoredProcedure);
            output.data = spOutput.ToList();
            return output;
        }
    }
}