﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Restore.Models
{
    public class RestoreModel
    {
        public string UID { get; set; }
        public string dbNm { get; set; }
        public string tableNm { get; set; }
        public string isAvailable { get; set; }
        public string periodColumnNm { get; set; }
        public string dbNmDestination { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string createdBy { get; set; }
        public string createdDt { get; set; }
    }
}
