﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Restore.Models
{
    public class RestoreListModel
    {
        public IList<RestoreModel> data { get; set; }
    }
}
