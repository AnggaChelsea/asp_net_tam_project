﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.Domain.Enumerations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Entities;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.ProcessHistory.Commands
{
    public class UpdateProcHistCommandByQueueHandler : AppBase, IRequestHandler<CommandsModel<UpdateProcHistByQueueCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        //public UpdateProcHistCommandByQueueHandler(
        //    GLOBALDbContext context, IMediator mediator,IConfiguration configuration) : base(context,configuration)
        //{
        //    _context = context;
        //    _mediator = mediator;
        //}
        public UpdateProcHistCommandByQueueHandler(
            IMediator mediator, IConfiguration configuration) : base(configuration)
        {
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<UpdateProcHistByQueueCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            try
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    ctx.ChangeTracker.LazyLoadingEnabled = false;

                    IList<ARRA.MODULE.Domain.Entities.ProcessLogHeader> list = await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.queueId).ToListAsync(cancellationToken);
                    foreach (ARRA.MODULE.Domain.Entities.ProcessLogHeader item in list)
                    {
                        item.ProcessStatus = req.CommandModel.status;
                        item.Remark = req.CommandModel.remarks;
                        item.ModifiedBy = SystemName.SYSTEM.ToString();
                        item.ModifiedDate = req.CurrentDateTime;
                    }
                    ctx.UpdateRange(list);

                    await ctx.SaveChangesAsync(cancellationToken);

                    list = null;
                    ctx.Dispose();
                }

            }
            catch(Exception ex)
            {
                string a = ex.Message;
            }

            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}