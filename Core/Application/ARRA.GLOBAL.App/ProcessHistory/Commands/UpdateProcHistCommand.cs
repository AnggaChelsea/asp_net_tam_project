﻿using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.ProcessHistory.Commands
{
    public class UpdateProcHistCommand
    {
        public long jobId { get; set; }
        public string status { get; set; }
        public string module { get; set; }
        public string remarks { get; set; }
    }
}