﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Persistence;
using ARRA.Common.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.ProcessHistory.Commands
{
    public class UpdateProcHistCommandHandler : AppBase, IRequestHandler<CommandsModel<UpdateProcHistCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        //public UpdateProcHistCommandHandler(
        //    GLOBALDbContext context, IMediator mediator,IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //    _mediator = mediator;
        //}
        public UpdateProcHistCommandHandler(
            IMediator mediator, IConfiguration configuration) : base(configuration)
        {
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<UpdateProcHistCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            try
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    ctx.ChangeTracker.LazyLoadingEnabled = false;

                    var entity = await ctx.ProcessLogHeaders.FindAsync(req.CommandModel.jobId);
                    if (entity.ProcessStatus == req.CommandModel.status &&
                        entity.Remark == req.CommandModel.remarks)
                    {
                        //noaction needed
                    }
                    else
                    {
                        entity.ProcessStatus = req.CommandModel.status;
                        entity.Remark = req.CommandModel.remarks;
                        entity.ModifiedBy = SystemName.SYSTEM.ToString();
                        entity.ModifiedDate = req.CurrentDateTime;
                        ctx.Update(entity);
                        await ctx.SaveChangesAsync(cancellationToken);
                    }
                    entity = null;
                    ctx.Dispose();
                }
            }
            catch(Exception ex)
            {
                string a = ex.Message;
            }

            //_context.Dispose();
            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}