﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.Domain.Enumerations;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Collections.Generic;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.GLOBAL.App.ProcessHistory.Commands
{
    public class UpdateProcHistValidationCommandHandler : AppBase, IRequestHandler<CommandsModel<UpdateProcHistValidationCommand, StatusModel>, StatusModel>
    {
        //private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        public UpdateProcHistValidationCommandHandler(
            IMediator mediator, IConfiguration configuration) : base(configuration)
        {
            _mediator = mediator;
        }

        public async Task<StatusModel> Handle(CommandsModel<UpdateProcHistValidationCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            try
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    ctx.ChangeTracker.LazyLoadingEnabled = false;

                    IList<string> listBranch = await ctx.ValidationExceptionSummaries.Where(f => f.ReportingDate == req.CommandModel.reportingDate
                        && f.FormCode == req.CommandModel.formCode).Select(c => c.Branch).Distinct().ToListAsync(cancellationToken);

                    IList<ProcessLogHeader> list = await ctx.ProcessLogHeaders.Where(f => f.QueueId == req.CommandModel.queueId).ToListAsync(cancellationToken);
                    foreach(ProcessLogHeader item in list)
                    {
                        if (req.CommandModel.status == CommandStatus.Failed.ToString())
                        {
                            item.ProcessStatus = req.CommandModel.status;
                        }
                        else
                        {
                            if (listBranch.Contains(item.Branch))
                            {
                                item.ProcessStatus = ValidationStatus.FailedValidation.ToString();
                            }
                            else
                            {
                                item.ProcessStatus = ValidationStatus.SuccessValidation.ToString();
                            }
                        }

                        item.Remark = req.CommandModel.remarks;
                        item.ModifiedBy = SystemName.SYSTEM.ToString();
                        item.ModifiedDate = req.CurrentDateTime;
                        ctx.Update(item);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);

                    listBranch = null;
                    list = null;
                    ctx.Dispose();
                }
            }
            catch(Exception ex)
            {
                string a = ex.Message;
            }

            //_context.Dispose();
            return new StatusModel
            {
                status = CommandStatus.Success.ToString()
            };
        }
    }
}