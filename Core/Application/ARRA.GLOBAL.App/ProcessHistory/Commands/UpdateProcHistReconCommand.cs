﻿using System;

namespace ARRA.GLOBAL.App.ProcessHistory.Commands
{
    public class UpdateProcHistReconCommand
    {
        public long queueId { get; set; }
        public DateTime reportingDate { get; set; }
        public string formCode { get; set; }
        public string periodType { get; set; }
        public string status { get; set; }
        public string module { get; set; }
        public string remarks { get; set; }

    }
}