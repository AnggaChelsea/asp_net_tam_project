﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessHistory.Models
{
    public class HistLogHdrModel
    {
        public DateTime? dataDate { get; set; }
        public string processType { get; set; }
        public string processNo { get; set; }
        public string status { get; set; }
        public DateTime? createdDate { get; set; }
    }
}
