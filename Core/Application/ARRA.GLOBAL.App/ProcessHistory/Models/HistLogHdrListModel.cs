﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessHistory.Models
{
    public class HistLogHdrListModel
    {
        public IList<HistLogHdrModel> HistLogs { get; set; }
    }
}
