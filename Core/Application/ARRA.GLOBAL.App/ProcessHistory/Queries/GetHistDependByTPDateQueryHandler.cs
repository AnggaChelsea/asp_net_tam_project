﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using System.Collections.Generic;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.ProcessHistory.Models;
using Microsoft.Extensions.Configuration;
using System;

namespace ARRA.GLOBAL.App.ProcessHistory.Queries
{
    public class GetHistDependByTPDateQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetHistDependByTPDateQuery, HistLogHdrListModel>, HistLogHdrListModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetHistDependByTPDateQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}
        public GetHistDependByTPDateQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<HistLogHdrListModel> Handle(QueriesModel<GetHistDependByTPDateQuery, HistLogHdrListModel> req, CancellationToken cancellationToken)
        {
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                ctx.ChangeTracker.LazyLoadingEnabled = false;

                IList<long> dependsts = await ctx.ProcessLogHeaders
                    .Where(f => f.DataDate == req.QueryModel.date && f.ProcessType == req.QueryModel.procType
                        && req.QueryModel.depend.Contains(f.ProcessNo)).GroupBy(g => g.ProcessNo)
                    .Select(c => c.Max(m => m.UniqueId)).ToListAsync(cancellationToken);

                IList<HistLogHdrModel> histLogs = await ctx.ProcessLogHeaders
                    .Where(f => dependsts.Contains(f.UniqueId))
                    .Select(c => new HistLogHdrModel
                    {
                        processNo = c.ProcessNo,
                        processType = c.ProcessType,
                        dataDate = c.DataDate,
                        status = c.ProcessStatus,
                        createdDate = c.CreatedDate
                    }).ToListAsync(cancellationToken);

                dependsts = null;
                ctx.Dispose();

                //_context.Dispose();
                return new HistLogHdrListModel()
                {
                    HistLogs = histLogs
                };
            }
        }
    }
}