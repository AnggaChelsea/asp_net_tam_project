﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessHistory.Queries
{
    public class GetHistDependByTPDateQuery
    {
        public DateTime date { get; set; }
        public string procType { get; set; }
        public string module { get; set; }
        public IList<string> depend { get; set; }
    }
}