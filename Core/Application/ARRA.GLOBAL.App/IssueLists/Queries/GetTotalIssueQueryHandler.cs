﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.IssueLists.Models;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.IssueLists.Queries
{
    public class GetTotalIssueQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetTotalIssueQuery, TotalItemModel>, TotalItemModel>
    {
        private readonly GLOBALDbContext _context;

        public GetTotalIssueQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<TotalItemModel> Handle(QueriesModel<GetTotalIssueQuery, TotalItemModel> req, CancellationToken cancellationToken)
        {
            string access = await _context.Users.Where(f => f.UserId == req.UserIdentity.UserId).Select(c => c.ModuleAccess).FirstOrDefaultAsync(cancellationToken);
            int total = 0;

            if (!string.IsNullOrEmpty(access))
            {
                string[] strs = access.Split(',');

                total = await (from i in _context.IssueLists
                               where strs.Contains(i.Module)
                               && i.IssueStatus == IssueStatus.Open.ToString()
                               select i.UniqueId).CountAsync(cancellationToken);
            }

            return new TotalItemModel
            {
                records = total
            };
        }
    }
}