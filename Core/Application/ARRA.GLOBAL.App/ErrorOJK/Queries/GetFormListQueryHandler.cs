﻿using MediatR;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.ErrorOJK.Models;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.ErrorOJK.Queries
{
    public class GetFormListQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormListQuery, FormListModel>, FormListModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IConfiguration _configuration;
        public GetFormListQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<FormListModel> Handle(QueriesModel<GetFormListQuery, FormListModel> req, CancellationToken cancellationToken)
        {
            FormListModel mdl = new FormListModel();

            mdl.formList =
                await (from mg in _context.MenuGroupDetails
                       join m in _context.Menus
                        on mg.MenuCode equals m.MenuCode
                       join f in _context.FormHeaders
                        on m.FormCode equals f.FormCode
                       join pm in _context.Menus
                        on m.ParentMenuCode equals pm.MenuCode into t_menuparent
                       from xpm in t_menuparent.DefaultIfEmpty()
                       where mg.GroupId == req.UserIdentity.GroupId
                        && string.IsNullOrEmpty(f.OJKFormCode) == false
                        && f.Module == req.QueryModel.module
                        && mg.AllowView == true
                       select new FormModel
                       {
                           FormCode = f.FormCode,
                           FormDesc = f.OJKFormCode + " - " + f.FormName,
                       }
                    ).ToListAsync(cancellationToken);

            return mdl;
        }
    }
}
