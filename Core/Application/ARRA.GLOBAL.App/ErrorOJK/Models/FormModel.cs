﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ErrorOJK.Models
{
    public class FormModel
    {
        public string FormCode { get; set; }
        public string FormDesc { get; set; }
    }
}
