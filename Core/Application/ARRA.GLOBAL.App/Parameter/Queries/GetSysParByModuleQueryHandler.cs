﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParByModuleQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetSysParByModuleQuery, ParamHeaderModel>, ParamHeaderModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetSysParByModuleQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetSysParByModuleQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<ParamHeaderModel> Handle(QueriesModel<GetSysParByModuleQuery, ParamHeaderModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                FormHeader form = await globalCtx.FormHeaders.FindAsync(req.QueryModel.formCode);

                return await (from a in globalCtx.SystemParameterHeaders
                              where a.Module == form.Module && a.ParamName == req.QueryModel.parName
                              select new ParamHeaderModel
                              {
                                  parName = a.ParamName,
                                  parValue = a.ParamValue
                              }).FirstOrDefaultAsync(cancellationToken);
            }
        }
    }
}