﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParDtlByCodeQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetSysParDtlByCodeQuery, ParamDetailListModel>, ParamDetailListModel>
    {
        public GetSysParDtlByCodeQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<ParamDetailListModel> Handle(QueriesModel<GetSysParDtlByCodeQuery, ParamDetailListModel> req, CancellationToken cancellationToken)
        {
            IList<ParamDetailModel> parDetails = new List<ParamDetailModel>();
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                parDetails = await globalCtx.SystemParameterDetails
                    .Where(f => f.ParamCode == req.QueryModel.code)
                    .Select(f => new ParamDetailModel
                    {
                        parDesc = f.ParamDesc,
                        parValue = f.ParamValue,
                        parValMap = f.ParamMapping,
                        parSeq = f.ParamSeq
                    }).ToListAsync(cancellationToken);
            }

            return new ParamDetailListModel
            {
                data = parDetails
            };
        }
    }
}
