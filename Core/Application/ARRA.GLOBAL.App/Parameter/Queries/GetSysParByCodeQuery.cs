﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParByCodeQuery
    {
        public string code { get; set; }
    }
}