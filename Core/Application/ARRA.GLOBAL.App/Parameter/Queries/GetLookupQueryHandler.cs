﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetLookupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetLookupQuery, LookupListModel>, LookupListModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetLookupQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetLookupQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<LookupListModel> Handle(QueriesModel<GetLookupQuery, LookupListModel> req, CancellationToken cancellationToken)
        {
            IList<LookupModel> lookup = new List<LookupModel>();
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                lookup = await (from a in globalCtx.BusinessParamDetails
                                                   where a.ParamCode == req.QueryModel.code
                                                   orderby a.ParamSeq
                                                   select new LookupModel
                                                   {
                                                       code = a.ParamValue,
                                                       desc = a.ParamDesc
                                                   }).ToListAsync(cancellationToken);
            }
            return new LookupListModel
            {
                data = lookup
            };
        }
    }
}