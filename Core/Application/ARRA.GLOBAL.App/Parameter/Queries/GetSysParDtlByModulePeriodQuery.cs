﻿namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParDtlByModulePeriodQuery
    {
        public string module { get; set; }
        public string period { get; set; }
        public string parName { get; set; }
    }
}