﻿namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParDtlByFormCdQuery
    {
        public string formCode { get; set; }
        public string parName { get; set; }
    }
}