﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetDynamicNestedLookupQuery
    {
        public string formCode { get; set; }
        public string formField { get; set; }
        public string mapField { get; set; }
        public string mapValue { get; set; }
    }
}
