﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParByModuleQuery
    {
        public string formCode { get; set; }
        public string parName { get; set; }
    }
}