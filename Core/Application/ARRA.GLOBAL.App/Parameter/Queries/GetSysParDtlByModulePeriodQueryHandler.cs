﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParDtlByModulePeriodQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetSysParDtlByModulePeriodQuery, ParamHeaderModel>, ParamHeaderModel>
    {
        //private readonly GLOBALDbContext _context;
        //public GetSysParDtlByModulePeriodQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetSysParDtlByModulePeriodQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<ParamHeaderModel> Handle(QueriesModel<GetSysParDtlByModulePeriodQuery, ParamHeaderModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                return await (from a in globalCtx.SystemParameterHeaders
                              join b in globalCtx.SystemParameterDetails
                                on a.ParamCode equals b.ParamCode
                              where a.Module == req.QueryModel.module
                                && a.ParamName == req.QueryModel.parName
                                && b.ParamMapping == req.QueryModel.period
                              select new ParamHeaderModel
                              {
                                  parName = b.ParamMapping,
                                  parValue = b.ParamValue
                              }).FirstOrDefaultAsync(cancellationToken);
            }

        }
    }
}