﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParByCodeQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetSysParByCodeQuery, ParamHeaderModel>, ParamHeaderModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetSysParByCodeQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}

        public GetSysParByCodeQueryHandler(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<ParamHeaderModel> Handle(QueriesModel<GetSysParByCodeQuery, ParamHeaderModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                SystemParameterHeader hdr = await globalCtx.SystemParameterHeaders.FindAsync(req.QueryModel.code);

                return new ParamHeaderModel
                {
                    parName = hdr.ParamName,
                    parValue = hdr.ParamValue
                };
            }//
        }
    }
}