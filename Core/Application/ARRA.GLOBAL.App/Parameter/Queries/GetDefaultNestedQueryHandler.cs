﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetDefaultNestedQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDefaultNestedQuery, DefaultNestedModel>, DefaultNestedModel>
    {
        public GetDefaultNestedQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<DefaultNestedModel> Handle(QueriesModel<GetDefaultNestedQuery, DefaultNestedModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                DefaultNestedModel defNest;

                DynamicParameters p = new DynamicParameters();
                p.Add("@CUSTOM_SRC", req.QueryModel.customrSrc);
                p.Add("@MAP_CD", req.QueryModel.mapCode);
                p.Add("@VAL", req.QueryModel.val);
                defNest = await globalCtx.Database.GetDbConnection().QuerySingleOrDefaultAsync<DefaultNestedModel>("DBO.UDPS_GET_DEFAULT_NESTED", p, null, null, CommandType.StoredProcedure);

                return defNest;
            }
        }
    }
}
