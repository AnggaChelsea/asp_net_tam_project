﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetDynamicNestedLookupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDynamicNestedLookupQuery, LookupListModel>, LookupListModel>
    {
        public GetDynamicNestedLookupQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<LookupListModel> Handle(QueriesModel<GetDynamicNestedLookupQuery, LookupListModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                IEnumerable<LookupModel> newLookup;

                DynamicParameters p = new DynamicParameters();
                p.Add("@FORM_CD", req.QueryModel.formCode);
                p.Add("@FORM_FIELD", req.QueryModel.formField);
                p.Add("@MAPFIELD", req.QueryModel.mapField);
                p.Add("@MAPVALUE", req.QueryModel.mapValue);
                IEnumerable<LookupModel> iDyLookup = await globalCtx.Database.GetDbConnection().QueryAsync<LookupModel>("DBO.UDPS_GET_NESTED_DYNAMIC_LOOKUP", p, null, null, CommandType.StoredProcedure);
                newLookup = iDyLookup.ToList<LookupModel>();

                return new LookupListModel { data = newLookup.ToList() };
            }
        }
    }
}
