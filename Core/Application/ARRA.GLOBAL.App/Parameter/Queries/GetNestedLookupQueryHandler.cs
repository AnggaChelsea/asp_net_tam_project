﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Parameter.Models;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetNestedLookupQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetNestedLookupQuery, LookupListModel>, LookupListModel>
    {
        //private readonly GLOBALDbContext _context;
        //public GetNestedLookupQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetNestedLookupQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<LookupListModel> Handle(QueriesModel<GetNestedLookupQuery, LookupListModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                if (string.IsNullOrEmpty(req.QueryModel.mapvalue))
                {
                    return new LookupListModel
                    {
                        data = await (from a in globalCtx.BusinessParamDetails
                                      where a.ParamCode == req.QueryModel.code
                                      orderby a.ParamSeq
                                      select new LookupModel
                                      {
                                          code = a.ParamValue,
                                          desc = a.ParamDesc
                                      }).ToListAsync(cancellationToken)
                    };
                }
                else
                {
                    return new LookupListModel
                    {
                        data = await (from a in globalCtx.BusinessParamDetails
                                      where a.ParamCode == req.QueryModel.code
                                       && a.ParamMapping == req.QueryModel.mapvalue.Trim()
                                      orderby a.ParamSeq
                                      select new LookupModel
                                      {
                                          code = a.ParamValue,
                                          desc = a.ParamDesc
                                      }).ToListAsync(cancellationToken)
                    };
                }
            }

        }
    }
}