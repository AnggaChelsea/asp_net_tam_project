﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetDefaultNestedQuery
    {
        public string customrSrc { get; set; }
        public string mapCode { get; set; }
        public string val { get; set; }
    }
}
