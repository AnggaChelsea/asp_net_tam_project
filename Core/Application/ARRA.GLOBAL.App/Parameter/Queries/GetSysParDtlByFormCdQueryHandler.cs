﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetSysParDtlByFormCdQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetSysParDtlByFormCdQuery, ParamHeaderModel>, ParamHeaderModel>
    {
        //private readonly GLOBALDbContext _context;
        //public GetSysParDtlByFormCdQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetSysParDtlByFormCdQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<ParamHeaderModel> Handle(QueriesModel<GetSysParDtlByFormCdQuery, ParamHeaderModel> req, CancellationToken cancellationToken)
        {
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                FormHeader form = await globalCtx.FormHeaders.FindAsync(req.QueryModel.formCode);

                return await (from a in globalCtx.SystemParameterHeaders
                              join b in globalCtx.SystemParameterDetails
                                on a.ParamCode equals b.ParamCode
                              where a.Module == form.Module
                                && a.ParamName == req.QueryModel.parName
                                && b.ParamMapping == form.PeriodType
                              select new ParamHeaderModel
                              {
                                  parName = b.ParamMapping,
                                  parValue = b.ParamValue
                              }).FirstOrDefaultAsync(cancellationToken);
            }

        }
    }
}