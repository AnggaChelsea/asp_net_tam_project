﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Parameter.Models;
using Microsoft.Extensions.Configuration;

namespace ARRA.GLOBAL.App.Parameter.Queries
{
    public class GetLookupsQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetLookupsQuery, LookupsListModel>, LookupsListModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetLookupsQueryHandler(GLOBALDbContext context) : base(context)
        //{
        //    _context = context;
        //}
        public GetLookupsQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<LookupsListModel> Handle(QueriesModel<GetLookupsQuery, LookupsListModel> req, CancellationToken cancellationToken)
        {
            IList<LookupsModel> lookup = new List<LookupsModel>();
            using (GLOBALDbContext globalCtx = base.GetGlobalDbContext())
            {
                string[] codes = req.QueryModel.codelist.Split(',');
                lookup = await globalCtx.BusinessParamDetails
                    .Where(f => codes.Contains(f.ParamCode))
                    .OrderBy(o => o.ParamSeq)
                    .Select(c => new LookupsModel
                    {
                        code = c.ParamValue,
                        desc = c.ParamDesc,
                        parType = c.ParamCode
                    }).ToListAsync(cancellationToken);
                codes = null;
            }

            return new LookupsListModel
            {
                data = lookup
            };
        }
    }
}