﻿using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Parameter.Models
{
    public class LookupsListModel
    {
        public IList<LookupsModel> data { get; set; }

    }
}
