﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Models
{
    public class ParamDetailListModel
    {
        public IList<ParamDetailModel> data { get; set; }
    }
}
