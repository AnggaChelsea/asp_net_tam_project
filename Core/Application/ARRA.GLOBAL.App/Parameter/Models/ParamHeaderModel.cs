﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Models
{
    public class ParamHeaderModel
    {
        public string parName { get; set; }
        public string parValue { get; set; }
    }
}