﻿namespace ARRA.GLOBAL.App.Parameter.Models
{
    public class LookupsModel
    {
        public string code { get; set; }
        public string desc { get; set; }
        public string parType { get; set; }
    }
}
