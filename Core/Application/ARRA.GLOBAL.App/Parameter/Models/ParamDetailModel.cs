﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Parameter.Models
{
    public class ParamDetailModel
    {
        public string parValue { get; set; }
        public string parValMap { get; set; }
        public string parDesc { get; set; }
        public int parSeq { get; set; }
    }
}
