﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuAccessModel
    {
        public bool? AllowView { get; set; }
        public bool? AllowInsert { get; set; }
        public bool? AllowUpdate { get; set; }
        public bool? AllowDelete { get; set; }
        public bool? AllowExport { get; set; }
        public bool? AllowImport { get; set; }
    }
}
