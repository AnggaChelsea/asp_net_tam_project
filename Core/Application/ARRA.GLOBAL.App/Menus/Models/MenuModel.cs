﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuModel
    {
        public string id { get; set; }
        public string module { get; set; }
        public string url { get; set; }
        public IList<MenuItem> items { get; set; }
    }
}