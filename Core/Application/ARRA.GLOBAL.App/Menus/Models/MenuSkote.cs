﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuSkote
    {
        public int Id { get; set; }
        public bool IsTitle { get; set; }
        public bool IsLayout { get; set; }
        public string Label { get; set; }
        public string Icon { get; set; }
        public string Link { get; set; }
        public int ParentId { get; set; }
        public string Key { get; set; }
        public IList<MenuSkote> SubItems { get; set; }
    }
}
