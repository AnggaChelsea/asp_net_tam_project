﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuListSkoteModel
    {
        public IList<MenuSkote> data { get; set; }
        public MenuListSkoteModel()
        {
            data = new List<MenuSkote>();
        }
    }
}
