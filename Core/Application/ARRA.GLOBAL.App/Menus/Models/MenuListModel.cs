﻿using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuListModel
    {
        public IList<MenuModel> data { get; set; }
        public MenuListModel()
        {
            data = new List<MenuModel>();
        }
    }
}