﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Menus.Models
{
    public class MenuItem
    {
        public string module { get; set; }
        public string menuid { get; set; }
        public string menuname { get; set; }
        public string pagetitle { get; set; }
        public string url { get; set; }
        public string parentid { get; set; }
        public string icon { get; set; }
        public Int16? seq { get; set; }

    }
}