﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Menus.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuSkoteByUserQueryHandler
        : AppBase,
            IRequestHandler<
                QueriesModel<GetMenuSkoteByUserQuery, MenuListSkoteModel>,
                MenuListSkoteModel
            >
    {
        private readonly GLOBALDbContext _context;

        public GetMenuSkoteByUserQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<MenuListSkoteModel> Handle(
            QueriesModel<GetMenuSkoteByUserQuery, MenuListSkoteModel> req,
            CancellationToken cancellationToken
        )
        {
            // UPDATE SA MENU IF USER LOGIN IS SUPER ADMIN
            if (req.QueryModel.MenuGroup.Equals("SUPER_ADMIN"))
            {
                await _context.Database
                    .GetDbConnection()
                    .ExecuteAsync(
                        "UDPU_SET_MENU_SA",
                        null,
                        null,
                        null,
                        System.Data.CommandType.StoredProcedure
                    );
            }

            MenuListSkoteModel vm1 = new MenuListSkoteModel();
            MenuListModel vm = new MenuListModel();
            IList<MenuItem> menuItems = await (
                from a in _context.MenuGroupDetails
                join b in _context.MenuGroupHeaders on a.GroupId equals b.GroupId
                join m in _context.Menus on a.MenuCode equals m.MenuCode
                where
                    a.GroupId == req.QueryModel.MenuGroup
                    && m.Status == true
                    && m.IsMenu == true
                    && b.Status == true
                    && a.AllowView == true
                orderby m.Module, m.ParentMenuCode, m.MenuSeq
                select new MenuItem
                {
                    menuid = m.MenuCode,
                    menuname = m.MenuName,
                    pagetitle = m.PageTitle,
                    parentid = m.ParentMenuCode == null ? "" : m.ParentMenuCode,
                    url = m.MenuUrl,
                    module = m.Module,
                    icon = m.Icon,
                    seq = m.MenuSeq
                }
            ).ToListAsync(cancellationToken);

            var moduleItems = await (
                from m in _context.Modules
                orderby m.Sequence
                select new { id = m.ModuleId, module = m.ModuleName, url = m.BaseUrl }
            ).ToListAsync(cancellationToken);

            var userAccess = req.UserIdentity.Modules;
            if (!string.IsNullOrEmpty(userAccess))
            {
                int index = 0;
                string[] usrModule = userAccess.Split(',');
                moduleItems = moduleItems.Where(f => usrModule.Contains(f.id)).ToList();
                for (int i = 0; i < moduleItems.Count; i++)
                {
                    IList<MenuItem> listItem = menuItems
                        .Where(f => f.module == moduleItems[i].id && f.parentid == string.Empty)
                        .OrderBy(c => c.seq)
                        .ToList();
                    if (listItem.Count > 0)
                    {
                        index++;
                        vm1.data.Add(
                            new MenuSkote
                            {
                                Id = index,
                                Label = moduleItems[i].module,
                                //IsTitle = true,
                                Icon = "fas fa-grip-horizontal",
                                Key = index.ToString(),
                                SubItems = this.GetSubItems(
                                    String.Empty,
                                    moduleItems[i].id,
                                    menuItems,
                                    index,
                                    index.ToString()
                                )
                            }
                        );

                        //index++;
                        //vm1.data.Add(new MenuSkote
                        //{
                        //    Id = index,
                        //    IsTitle = true,
                        //    IsLayout = true,
                        //});

                        //foreach (var item in listItem)
                        //{
                        //    index++;
                        //    vm1.data.Add(new MenuSkote
                        //    {
                        //        Id = index,
                        //        Icon = (item.icon == null || item.icon == String.Empty) ? "fa fa-circle-o" : "fas fa-" + item.icon,
                        //        Label = item.menuname,
                        //        Link = item.url,
                        //        ParentId = 0,
                        //        SubItems = this.GetSubItems(item.menuid, item.module, menuItems, index)
                        //    });
                        //}
                    }
                    listItem = null;
                }
            }
            moduleItems = null;
            menuItems = null;

            return vm1;
        }

        private List<MenuSkote> GetSubItems(
            string parentMenu,
            string module,
            IList<MenuItem> menuItems,
            int parentIdIndex,
            string parentKey
        )
        {
            List<MenuSkote> subItems = new List<MenuSkote>();
            var listItem = menuItems
                .Where(f => f.parentid == parentMenu && f.module == module)
                .OrderBy(c => c.seq)
                .ToList();
            if (listItem.Count > 0)
            {
                int index = 1;
                foreach (var item in listItem)
                {
                    subItems.Add(
                        new MenuSkote
                        {
                            Id = index,
                            Icon =
                                (item.icon == null || item.icon == string.Empty)
                                    ? "fa fa-circle-o"
                                    : "fas fa-" + item.icon,
                            Label = item.menuname,
                            Link = item.url,
                            ParentId = parentIdIndex,
                            Key = parentKey + "_" + index.ToString(),
                            SubItems = this.GetSubItems(
                                item.menuid,
                                module,
                                menuItems,
                                index,
                                parentKey + "_" + index.ToString()
                            )
                        }
                    );

                    index++;
                }
            }
            listItem = null;
            return subItems;
        }
    }
}
