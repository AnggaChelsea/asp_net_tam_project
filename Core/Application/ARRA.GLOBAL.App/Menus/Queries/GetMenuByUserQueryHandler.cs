﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.Menus.Models;
using Dapper;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuByUserQueryHandler
        : AppBase,
            IRequestHandler<QueriesModel<GetMenuByUserQuery, MenuListModel>, MenuListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetMenuByUserQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<MenuListModel> Handle(
            QueriesModel<GetMenuByUserQuery, MenuListModel> req,
            CancellationToken cancellationToken
        )
        {
            // UPDATE SA MENU IF USER LOGIN IS SUPER ADMIN
            if (req.QueryModel.MenuGroup.Equals("SUPER_ADMIN"))
            {
                await _context.Database
                    .GetDbConnection()
                    .ExecuteAsync(
                        "UDPU_SET_MENU_SA",
                        null,
                        null,
                        null,
                        System.Data.CommandType.StoredProcedure
                    );
            }

            MenuListModel vm = new MenuListModel();
            IList<MenuItem> menuItems = await (
                from a in _context.MenuGroupDetails
                join b in _context.MenuGroupHeaders on a.GroupId equals b.GroupId
                join m in _context.Menus on a.MenuCode equals m.MenuCode
                where
                    a.GroupId == req.QueryModel.MenuGroup
                    && m.Status == true
                    && m.IsMenu == true
                    && b.Status == true
                    && a.AllowView == true
                orderby m.Module, m.ParentMenuCode, m.MenuSeq
                select new MenuItem
                {
                    menuid = m.MenuCode,
                    menuname = m.MenuName,
                    pagetitle = m.PageTitle,
                    parentid = m.ParentMenuCode == null ? "" : m.ParentMenuCode,
                    url = m.MenuUrl,
                    module = m.Module,
                    icon = m.Icon,
                    seq = m.MenuSeq
                }
            ).ToListAsync(cancellationToken);

            var moduleItems = await (
                from m in _context.Modules
                orderby m.Sequence
                select new { id = m.ModuleId, module = m.ModuleName, url = m.BaseUrl }
            ).ToListAsync(cancellationToken);

            var userAccess = req.UserIdentity.Modules;
            if (!string.IsNullOrEmpty(userAccess))
            {
                string[] usrModule = userAccess.Split(',');
                moduleItems = moduleItems.Where(f => usrModule.Contains(f.id)).ToList();
                for (int i = 0; i < moduleItems.Count; i++)
                {
                    IList<MenuItem> listItem = menuItems
                        .Where(f => f.module == moduleItems[i].id)
                        .OrderBy(c => c.seq)
                        .ToList();
                    if (listItem.Count > 0)
                    {
                        vm.data.Add(
                            new MenuModel
                            {
                                id = moduleItems[i].id,
                                module = moduleItems[i].module,
                                url = moduleItems[i].url,
                                items = listItem
                            }
                        );
                    }
                    listItem = null;
                }
            }
            moduleItems = null;
            menuItems = null;

            return vm;
        }
    }
}
