﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.AdhocQuery.Models;
using ARRA.GLOBAL.App.Menus.Models;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuAccessByUrlQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetMenuAccessByUrlQuery, MenuAccessModel>, MenuAccessModel>
    {
        private readonly GLOBALDbContext _context;

        public GetMenuAccessByUrlQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<MenuAccessModel> Handle(QueriesModel<GetMenuAccessByUrlQuery, MenuAccessModel> request, CancellationToken cancellationToken)
        {
            MenuAccessModel vm = new MenuAccessModel();

            //string groupId = await _context.Users.Where(f => f.FirstName == request.QueryModel.UserName).Select(f => f.MenuGroup).FirstOrDefaultAsync(cancellationToken);

            vm = await (from a in _context.Menus
                        join b in _context.MenuGroupDetails on a.MenuCode equals b.MenuCode
                        join c in _context.MenuGroupHeaders on b.GroupId equals c.GroupId
                        where a.MenuUrl == WebUtility.UrlDecode(request.QueryModel.Url)
                            && c.GroupName == request.QueryModel.Group
                        select new MenuAccessModel
                        {
                            AllowDelete = b.AllowDelete,
                            AllowExport = b.AllowExport,
                            AllowImport = b.AllowImport,
                            AllowInsert = b.AllowInsert,
                            AllowUpdate = b.AllowUpdate,
                            AllowView = b.AllowView
                        }).FirstOrDefaultAsync(cancellationToken);

            return vm;
        }
    }
}
