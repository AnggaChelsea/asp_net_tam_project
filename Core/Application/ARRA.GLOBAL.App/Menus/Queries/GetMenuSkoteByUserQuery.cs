﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuSkoteByUserQuery
    {
        public string UserId { get; set; }
        public string MenuGroup { get; set; }
    }
}
