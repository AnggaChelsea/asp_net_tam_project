﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuByUserQuery
    {
        public string UserId { get; set; }
        public string MenuGroup { get; set; }
    }
}