﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.Menus.Queries
{
    public class GetMenuAccessByUrlQuery
    {
        public string Group { get; set; }
        public string Url { get; set; }
    }
}
