﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummaryCompare.Models;
using ARRA.GLOBAL.Domain.Entities;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumCompareHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumCompare, FormSumCompareModel>, FormSumCompareModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumCompareHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumCompareModel> Handle(QueriesModel<GetFormSumCompare, FormSumCompareModel> req, CancellationToken cancellationToken)
        {
            //FormHeader form = await _context.FormHeaders.Where(hdr => hdr.FormCode == req.QueryModel.FormCode).FirstOrDefaultAsync(cancellationToken);

            List<dynamic> data = new List<dynamic>();

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.Module))
            {
                DynamicParameters dpTable = new DynamicParameters();
                dpTable.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.FormCode));
                var tables = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE", dpTable, null, null, CommandType.StoredProcedure);
                if (tables.ToList().Count() == 0)
                {
                    DynamicParameters dpTableCol = new DynamicParameters();
                    dpTableCol.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.FormCode));
                    dpTableCol.Add("@TBL_CD", "");
                    var columns = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_COL", dpTableCol, null, null, CommandType.StoredProcedure);

                    DynamicParameters dpReportCompare = new DynamicParameters();
                    dpReportCompare.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.ReportDt));
                    dpReportCompare.Add("@REPORT_DT_PREV", ARRA.Common.Utility.SafeSqlString(req.QueryModel.ComparisonDt));
                    dpReportCompare.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.FormCode));
                    dpReportCompare.Add("@TBL_CD", "");
                    var rows = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_REPORT_COMPARE", dpReportCompare, null, null, CommandType.StoredProcedure);

                    Dictionary<string, dynamic> tmpDict = new Dictionary<string, dynamic>();
                    tmpDict.Add("id", "tblDefault");
                    tmpDict.Add("columns", columns);
                    tmpDict.Add("rows", rows);                    

                    data.Add(tmpDict);
                }
                else
                {
                    for (int i = 0; i < tables.ToList().Count(); i++)
                    {
                        dynamic table = tables.ToList()[i];
                        string tableCd = table.TBL_CD;
                        DynamicParameters dpTableCol = new DynamicParameters();
                        dpTableCol.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.FormCode));
                        dpTableCol.Add("@TBL_CD", tableCd);
                        var columns = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_TABLE_COL", dpTableCol, null, null, CommandType.StoredProcedure);

                        DynamicParameters dpReportCompare = new DynamicParameters();
                        dpReportCompare.Add("@REPORT_DT", ARRA.Common.Utility.SafeSqlString(req.QueryModel.ReportDt));
                        dpReportCompare.Add("@REPORT_DT_PREV", ARRA.Common.Utility.SafeSqlString(req.QueryModel.ComparisonDt));
                        dpReportCompare.Add("@FORM_CD", ARRA.Common.Utility.SafeSqlString(req.QueryModel.FormCode));
                        dpReportCompare.Add("@TBL_CD", tableCd);
                        var rows = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_REPORT_COMPARE", dpReportCompare, null, null, CommandType.StoredProcedure);

                        Dictionary<string, dynamic> tmpDict = new Dictionary<string, dynamic>();
                        tmpDict.Add("id", tableCd);
                        tmpDict.Add("description", table.TBL_DESC);
                        tmpDict.Add("rows", rows);
                        tmpDict.Add("columns", columns);

                        data.Add(tmpDict);
                    }
                }
            }

            return new FormSumCompareModel
            {
                Data = data,
            };
        }
    }
}