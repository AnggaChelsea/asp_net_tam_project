﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumCompare
    {
        public string Module { get; set; }
        public string FormCode { get; set; }
        public string ReportDt { get; set; }
        public string ComparisonDt { get; set; }
    }

}