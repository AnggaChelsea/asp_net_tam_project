﻿using Dapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.FormSummaryCompare.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.MODULE.Domain.Entities;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryCompare.Queries
{
    public class ExportTaskCompareQuery : IRequest<StatusModel>
    {
        public string ReportCompareCode { get; set; }
        public string ReportExportFileName { get; set; }
        public GetFormSumCompare FilterModel { get; set; }

    }
    public class ExportTaskCompareQueryHandler : AppBase, IRequestHandler<QueriesModel<ExportTaskCompareQuery, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        private readonly IMediator _mediator;
        private readonly IConfiguration _config; 

        public ExportTaskCompareQueryHandler(GLOBALDbContext context, IMediator mediator, IConfiguration config) : base(context, config)
        {
            _context = context;
            _mediator = mediator;
            _config = config;
        }
        public async Task<StatusModel> Handle(QueriesModel<ExportTaskCompareQuery, StatusModel> req, CancellationToken cancellationToken)
        {
            string path = await _context.SystemParameterHeaders
                        .Where(f => f.Module == req.QueryModel.FilterModel.Module && f.ParamName == SystemParameterType.EXPORT_FILE_LOCATION.ToString())
                        .Select(c => c.ParamValue)
                        .FirstOrDefaultAsync(cancellationToken);
            string fileName = req.QueryModel.ReportExportFileName;

            using var ModuleContext = base.GetDbContext(req.QueryModel.FilterModel.Module);
            JobQueueHeader header = new JobQueueHeader()
            {
                JobType = QueueJobType.EXPORT_SUM_COMPARE.ToString(),
                Status = JobStatus.Pending.ToString(),
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            };

            ModuleContext.JobQueueHeaders.Add(header);
            await ModuleContext.SaveChangesAsync(cancellationToken);

            long jobQueueId = header.UniqueId;
            List<JobQueueDetail> listDetail = new();

            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "FILE_PATH",
                ParamValue = path,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "FILE_NAME",
                ParamValue = fileName,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "MODULE",
                ParamValue = req.QueryModel.FilterModel.Module,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "FORM_CD",
                ParamValue = req.QueryModel.FilterModel.FormCode,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "REPORT_DATE",
                ParamValue = req.QueryModel.FilterModel.ReportDt,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });
            listDetail.Add(new JobQueueDetail
            {
                HeaderId = jobQueueId,
                ParamName = "COMPARISON_DATE",
                ParamValue = req.QueryModel.FilterModel.ComparisonDt,
                CreatedBy = req.UserIdentity.UserId,
                CreatedDate = req.CurrentDateTime,
                CreatedHost = req.UserIdentity.Host
            });

            ModuleContext.JobQueueDetails.AddRange(listDetail);
            header.Status = JobStatus.Pending.ToString();
            await ModuleContext.SaveChangesAsync(cancellationToken);


            return new StatusModel()
            {
                status = CommandStatus.Success.ToString(),
                id = Convert.ToInt64(jobQueueId)
            };

        }
    }
}
