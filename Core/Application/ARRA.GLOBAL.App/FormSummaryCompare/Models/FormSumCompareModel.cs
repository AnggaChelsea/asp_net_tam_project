﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummaryCompare.Models
{
    public class FormSumCompareModel
    {
        public dynamic Data { get; set; }
    }

    public class ExportCompareProgressModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public string percentage { get; set; }
    }
}