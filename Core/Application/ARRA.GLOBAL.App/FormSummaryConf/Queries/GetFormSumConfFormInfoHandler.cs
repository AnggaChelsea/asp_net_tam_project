﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Dapper;
using ARRA.MODULE.Domain.Entities;
using System.Collections.Generic;
using ARRA.GLOBAL.App;
using System.Data;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfFormInfoHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumConfFormInfo, FormSumConfFormInfoModel>, FormSumConfFormInfoModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfFormInfoHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumConfFormInfoModel> Handle(QueriesModel<GetFormSumConfFormInfo, FormSumConfFormInfoModel> req, CancellationToken cancellationToken)
        {

            dynamic rows;
            var usr_id = req.UserIdentity.UserId;
            var modules = req.QueryModel.module;
            //var allow_ap = req.AccessMatrix.AllowApproval;
            //var allow_add = req.AccessMatrix.AllowInsert;

            DynamicParameters spParam = new DynamicParameters();
            spParam.Add("@p_module", ARRA.Common.Utility.SafeSqlString(modules));
            spParam.Add("@p_user", ARRA.Common.Utility.SafeSqlString(usr_id));            
            rows = await _context.Database.GetDbConnection().QueryAsync("DBO.UDPS_GET_FORSUMCONF_GRANT", spParam, null, null, CommandType.StoredProcedure);



            return new FormSumConfFormInfoModel
            {
                rows = rows              
            };
        }
    }
}
