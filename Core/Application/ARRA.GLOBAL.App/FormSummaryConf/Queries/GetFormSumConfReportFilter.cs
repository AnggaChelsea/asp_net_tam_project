﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportFilter
    {
        public string module { get; set; }
        public string reportCd { get; set; }
        public int rowSeq { get; set; }
        public int columnSeq { get; set; }
    }

}