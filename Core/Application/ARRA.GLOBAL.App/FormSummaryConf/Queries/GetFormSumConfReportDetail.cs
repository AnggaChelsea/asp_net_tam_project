﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportDetail
    {
        public string module { get; set; }
        public string reportCd { get; set; }
    }

}