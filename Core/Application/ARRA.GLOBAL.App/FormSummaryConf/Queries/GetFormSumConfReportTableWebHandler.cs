﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using ARRA.GLOBAL.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ARRA.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportTableWebHandler : AppBase, IRequestHandler<QueriesModel<GetFormSumConfReportTableWeb, FormSumConfReportTableModel>, FormSumConfReportTableModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfReportTableWebHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FormSumConfReportTableModel> Handle(QueriesModel<GetFormSumConfReportTableWeb, FormSumConfReportTableModel> req, CancellationToken cancellationToken)
        {
            dynamic reportTableList = null;
            dynamic reportTableDataList = null;

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                reportTableList = await (from a in ctx.ReportTableWeb select a).Where(x => x.ReportCd == req.QueryModel.reportCd).OrderBy(x => x.TableSequence).ToListAsync(cancellationToken);
                reportTableDataList = await (from a in ctx.ReportTableDataWeb select a).Where(x => x.ReportCd == req.QueryModel.reportCd).OrderBy(x => x.ReportCd).ToListAsync(cancellationToken);
            }

            return new FormSumConfReportTableModel
            {
                reportTable = reportTableList,
                reportTableData = reportTableDataList,
            };
        }
    }
}
