﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Dapper;
using ARRA.MODULE.Domain.Entities;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportDetailHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumConfReportDetail, FormSumConfListModel>, FormSumConfListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfReportDetailHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumConfListModel> Handle(QueriesModel<GetFormSumConfReportDetail, FormSumConfListModel> req, CancellationToken cancellationToken)
        {
            IList<ReportRow> reportRows = null;
            dynamic reportColumns = null;
            dynamic reportRowColumns = null;

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                reportRows = await ctx.ReportRows.Where(f => f.ReportCd == req.QueryModel.reportCd ).OrderBy(s => s.RowSeq).ToListAsync(cancellationToken);
                reportColumns = await ctx.ReportColumns.Where(f => f.ReportCd == req.QueryModel.reportCd ).OrderBy(s => s.ColumnSeq).ToListAsync(cancellationToken);
                reportRowColumns = await ctx.ReportRowColumns.Where(f => f.ReportCd == req.QueryModel.reportCd ).ToListAsync(cancellationToken);
            }

            return new FormSumConfListModel
            {
                reportRow = reportRows,
                reportCol = reportColumns,
                reportRowCol = reportRowColumns
            };
        }
    }
}