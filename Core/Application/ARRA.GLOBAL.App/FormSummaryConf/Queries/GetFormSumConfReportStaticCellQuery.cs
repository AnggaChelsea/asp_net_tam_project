﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportStaticCellQuery
    {
        public string module { get; set; }
        public string reportCd { get; set; }
    }
}
