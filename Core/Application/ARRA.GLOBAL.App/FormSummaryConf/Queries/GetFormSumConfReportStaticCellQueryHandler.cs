﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using ARRA.GLOBAL.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using System.Threading;
using Microsoft.Extensions.Configuration;
using ARRA.Persistence;
using ARRA.MODULE.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportStaticCellQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumConfReportStaticCellQuery, FormSumConfReportStaticModel>, FormSumConfReportStaticModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfReportStaticCellQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<FormSumConfReportStaticModel> Handle(QueriesModel<GetFormSumConfReportStaticCellQuery, FormSumConfReportStaticModel> req, CancellationToken cancellationToken)
        {
            dynamic reportStaticCells = null;

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                reportStaticCells = await ctx.ReportStaticCell.Where(f => f.ReportCd == req.QueryModel.reportCd).ToListAsync(cancellationToken);
            }

            return new FormSumConfReportStaticModel
            {
                reportStaticCells = reportStaticCells,
            };
        }
    }
}
