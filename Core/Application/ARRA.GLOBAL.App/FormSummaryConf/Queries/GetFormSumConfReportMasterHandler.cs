﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportMasterHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumConfReportMaster, FormSumConfListModel>, FormSumConfListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfReportMasterHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumConfListModel> Handle(QueriesModel<GetFormSumConfReportMaster, FormSumConfListModel> req, CancellationToken cancellationToken)
        {
            dynamic reportMasterList = null;

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                reportMasterList = await (from a in ctx.ReportMasters select a).OrderBy(x => x.ReportDesc).ToListAsync(cancellationToken);
            }

            return new FormSumConfListModel
            {
                reportMaster = reportMasterList,
            };
        }
    }
}