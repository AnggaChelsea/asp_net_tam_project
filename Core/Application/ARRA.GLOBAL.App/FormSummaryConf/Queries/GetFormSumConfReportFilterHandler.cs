﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.FormSummaryConf.Queries
{
    public class GetFormSumConfReportFilterHandler
        : AppBase, IRequestHandler<QueriesModel<GetFormSumConfReportFilter, FormSumConfListModel>, FormSumConfListModel>
    {
        private readonly GLOBALDbContext _context;

        public GetFormSumConfReportFilterHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<FormSumConfListModel> Handle(QueriesModel<GetFormSumConfReportFilter, FormSumConfListModel> req, CancellationToken cancellationToken)
        {
            dynamic reportFilterList = null;

            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                reportFilterList = await ctx.ReportFilters.Where(reportRowCol => reportRowCol.ReportCd == req.QueryModel.reportCd && reportRowCol.RowSeq == req.QueryModel.rowSeq && reportRowCol.ColumnSeq == req.QueryModel.columnSeq).ToListAsync(cancellationToken);
            }

            return new FormSumConfListModel
            {
                reportFilter = reportFilterList,
            };
        }
    }
}