﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportMaster
    {
        public string module { get; set; }
        public string reportCode { get; set; }
    }
}