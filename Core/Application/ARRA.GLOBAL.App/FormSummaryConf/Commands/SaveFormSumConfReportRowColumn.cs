﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportRowColumn
    {
        public long uniqueId { get; set; }
        public string module { get; set; }
        public string reportCd { get; set; }
        public int rowSeq { get; set; }
        public int columnSeq { get; set; }
        public string columnFormula { get; set; }
        public string columnStatic { get; set; }
        public string isRecalculate { get; set; }
        public int recalculateSeq { get; set; }
        public string recalculateType { get; set; }
        public string columnAggregate { get; set; }
        public string divisorAfterAggregate { get; set; }
        public string roundedAfterAggregate { get; set; }
        public string absoluteAfterAggregate { get; set; }
    }
}