﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportRowHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportRow, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportRowHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportRow, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportRow reportRow_ctr = new ReportRow();
            ReportRow tmpReportRow_ctr = null;

            if (!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportRow reportRow = new ReportRow();
                    ReportRow tmpReportRow = null;
                    if (req.CommandModel.uniqueId != 0)
                    {
                        tmpReportRow = await ctx.ReportRows.FindAsync(req.CommandModel.uniqueId);
                    }

                    if (tmpReportRow != null)
                    {
                        reportRow = tmpReportRow;
                        reportRow.ModifiedBy = req.UserIdentity.UserId;
                        reportRow.ModifiedDate = req.CurrentDateTime;
                        reportRow.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportRow.ReportCd = req.CommandModel.reportCd;
                        reportRow.CreatedBy = req.UserIdentity.UserId;
                        reportRow.CreatedDate = req.CurrentDateTime;
                        reportRow.CreatedHost = req.UserIdentity.Host;
                    }
                    reportRow.RowSeq = req.CommandModel.rowSeq;
                    reportRow.RowDesc = req.CommandModel.rowDesc;
                    reportRow.ComponentCd = req.CommandModel.componentCd;
                    reportRow.DbNm = req.CommandModel.dbNm;
                    reportRow.TableNm = req.CommandModel.tableNm;
                    reportRow.ParentRowSeq = req.CommandModel.parentRowSeq;
                    reportRow.Text = req.CommandModel.text;

                    if (tmpReportRow == null)
                    {
                        ctx.ReportRows.Add(reportRow);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_ROW");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_ROW");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                    
                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.module))
                    {
                        if (req.CommandModel.uniqueId != 0)
                        {
                            tmpReportRow_ctr = await ctx_ctr.ReportRows.FindAsync(req.CommandModel.uniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportRow_ctr != null ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportRow_ctr != null ? req.CommandModel.uniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_ROW",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        ValueBefore = tmpReportRow_ctr != null? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "ROW_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSeq)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.RowSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "ROW_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowDesc)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.RowDesc)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COMPONENT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.componentCd)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.ComponentCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.componentCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "DB_NM")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.dbNm)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.DbNm)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.dbNm)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TABLE_NM")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.tableNm)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.TableNm)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.tableNm)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "PARENT_ROW_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.parentRowSeq)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.ParentRowSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.parentRowSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "IS_TF")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.text)),
                                        ValueBefore = tmpReportRow_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRow_ctr.Text)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.text)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }


            return statusModel;
        }
    }
}