﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportStaticCellCommand
    {
        public long uniqueId { get; set; }
        public string module { get; set; }
        public string reportCd { get; set; }
        public int rowSequence { get; set; }
        public string cellPosition { get; set; }
        public string cellType { get; set; }
        public string cellValue { get; set; }
    }
}
