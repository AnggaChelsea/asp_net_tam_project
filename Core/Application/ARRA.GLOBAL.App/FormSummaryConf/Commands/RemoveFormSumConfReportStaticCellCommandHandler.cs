﻿using ARRA.MODULE.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App;
using ARRA.GLOBAL.App.Exceptions;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.MODULE.Domain.Entities;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportStaticCellCommandHandler : AppBase, IRequestHandler<CommandsModel<RemoveFormSumConfReportStaticCellCommand, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RemoveFormSumConfReportStaticCellCommandHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RemoveFormSumConfReportStaticCellCommand, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportStaticCell reportStatic_ctr = new ReportStaticCell();
            ReportStaticCell tmpReportStatic_ctr = null;

            if (!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportStaticCell reportStatic = new ReportStaticCell();
                    ReportStaticCell tmpReportStatic = null;
                    
                    tmpReportStatic = await ctx.ReportStaticCell.FindAsync(req.CommandModel.uniqueId);
                    
                    ctx.ReportStaticCell.Remove(tmpReportStatic);
                    
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPTSTCEL");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPTSTCEL");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);

                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.module))
                    {
                        if (req.CommandModel.uniqueId != 0)
                        {
                            tmpReportStatic_ctr = await ctx_ctr.ReportStaticCell.FindAsync(req.CommandModel.uniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = base.GetActionType("REMOVE"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportStatic_ctr != null ? req.CommandModel.uniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPTSTCEL",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        ValueBefore = tmpReportStatic_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportStatic_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "ROW_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSequence)),
                                        ValueBefore = tmpReportStatic_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportStatic_ctr.RowSequence)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSequence)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "CELL_POSITION")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellPosition)),
                                        ValueBefore = tmpReportStatic_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportStatic_ctr.CellPosition)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellPosition)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "CELL_TYPE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellType)),
                                        ValueBefore = tmpReportStatic_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportStatic_ctr.CellType)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellType)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "CELL_VALUE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellValue)),
                                        ValueBefore = tmpReportStatic_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportStatic_ctr.CellValue)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.cellValue)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
            }

            return statusModel;
        }
    }
}
