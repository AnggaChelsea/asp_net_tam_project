﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportMaster
    {
        public string module { get; set; }
        public string reportCode { get; set; }
        public string reportDescription { get; set; }
        public string reportSheet { get; set; }
        public string formCodeDtl { get; set; }
        public long uniqueId { get; set; }
        public string reportTemplate { get; set; }

    }
}