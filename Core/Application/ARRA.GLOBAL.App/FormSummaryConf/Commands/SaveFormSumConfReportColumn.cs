﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportColumn
    {
        public long uniqueId { get; set; }
        public string module { get; set; }
        public string reportCd { get; set; }
        public int columnSeq { get; set; }
        public string columnExcelPos { get; set; }
        public string columnDest { get; set; }
    }
}