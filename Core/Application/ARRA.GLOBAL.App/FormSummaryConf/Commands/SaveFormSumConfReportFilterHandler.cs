﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;


namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportFilterHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportFilter, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportFilterHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportFilter, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportFilter reportFilter_ctr = new ReportFilter();
            ReportFilter tmpReportFilter_ctr = null;


            if (!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.Module))
                {
                    ReportFilter reportFilter = new ReportFilter();
                    ReportFilter tmpReportFilter = null;
                    if (req.CommandModel.UniqueId != 0)
                    {
                        tmpReportFilter = await ctx.ReportFilters.FindAsync(req.CommandModel.UniqueId);
                    }

                    if (tmpReportFilter != null)
                    {
                        reportFilter = tmpReportFilter;
                        reportFilter.ModifiedBy = req.UserIdentity.UserId;
                        reportFilter.ModifiedDate = req.CurrentDateTime;
                        reportFilter.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportFilter.ReportCd = req.CommandModel.ReportCd;
                        reportFilter.CreatedBy = req.UserIdentity.UserId;
                        reportFilter.CreatedDate = req.CurrentDateTime;
                        reportFilter.CreatedHost = req.UserIdentity.Host;
                    }
                    reportFilter.RowSeq = req.CommandModel.RowSeq;
                    reportFilter.ColumnSeq = req.CommandModel.ColumnSeq;
                    reportFilter.Prefix = req.CommandModel.Prefix;
                    reportFilter.ColumnName = req.CommandModel.ColumnName;
                    reportFilter.Expression = req.CommandModel.Expression;
                    reportFilter.ColumnVal = req.CommandModel.ColumnVal;
                    reportFilter.Operator = req.CommandModel.Operator;
                    reportFilter.Suffix = req.CommandModel.Suffix;
                    reportFilter.FilterSeq = req.CommandModel.FilterSeq;

                    if (tmpReportFilter == null)
                    {
                        ctx.ReportFilters.Add(reportFilter);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_FILTER");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_FILTER");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.Module))
                    {
                        if (req.CommandModel.UniqueId != 0)
                        {
                            tmpReportFilter_ctr = await ctx_ctr.ReportFilters.FindAsync(req.CommandModel.UniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportFilter_ctr != null ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportFilter_ctr != null ? req.CommandModel.UniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_FILTER",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "ROW_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.RowSeq)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.RowSeq)) :ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.RowSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnSeq)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.ColumnSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "PREFIX")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Prefix)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.Prefix)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Prefix)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_NAME")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnName)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.ColumnName)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnName)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "EXPRESSION")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Expression)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.Expression)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Expression)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_VAL")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnVal)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.ColumnVal)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnVal)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "OPERATOR")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Operator)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.Operator)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Operator)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "SUFFIX")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Suffix)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.Suffix)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.Suffix)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "FILTER_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.FilterSeq)),
                                        ValueBefore = tmpReportFilter_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportFilter_ctr.FilterSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.FilterSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }

            return statusModel;
        }
    }
}