﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportFilter
    {
        public long UniqueId { get; set; }
        public string Module { get; set; }
        public string ReportCd { get; set; }
        public int RowSeq { get; set; }
        public int ColumnSeq { get; set; }
        public string Prefix { get; set; }
        public string ColumnName { get; set; }
        public string Expression { get; set; }
        public string ColumnVal { get; set; }
        public string Operator { get; set; }
        public string Suffix { get; set; }
        public int FilterSeq { get; set; }
    }
}