﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportTableWeb
    {
        public string Module { get; set; }
        public long UniqueId { get; set; }
        public string ReportCd { get; set; }
        public string TableCd { get; set; }
        public string TableDesc { get; set; }
        public string TableFilter { get; set; }
        public int TableSequence { get; set; }
    }
}
