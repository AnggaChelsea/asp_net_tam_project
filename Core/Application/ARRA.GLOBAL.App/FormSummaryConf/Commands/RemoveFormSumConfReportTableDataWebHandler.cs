﻿using MediatR;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.MODULE.Domain.Entities;
using ARRA.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.App.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportTableDataWebHandler : AppBase, IRequestHandler<CommandsModel<RemoveFormSumConfReportTableDataWeb, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RemoveFormSumConfReportTableDataWebHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RemoveFormSumConfReportTableDataWeb, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };

            if (req.AccessMatrix.AllowApproval)
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_TBL_DT_WEB");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_TBL_DT_WEB");
                    }

                    ReportTableDataWeb reportTableDataWeb_ctr = new ReportTableDataWeb();
                    ReportTableDataWeb tmpReportTableDataWeb_ctr = null;

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);

                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.Module))
                    {
                        if (req.CommandModel.UniqueId != 0)
                        {
                            tmpReportTableDataWeb_ctr = await ctx_ctr.ReportTableDataWeb.FindAsync(req.CommandModel.UniqueId);
                        }
                    }

                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = base.GetActionType("REMOVE"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = req.CommandModel.UniqueId,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_TBL_DT_WEB",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.TableCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_DATA")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnData)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_ALIAS")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnAlias)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "IS_HIDE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.IsHide)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "IS_COMPARE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.IsCompare)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }                    
                }
            }
            else {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.Module))
                {
                    ReportTableDataWeb reportTableDataWeb = await ctx.ReportTableDataWeb.FindAsync(req.CommandModel.UniqueId);
                    ctx.Remove(reportTableDataWeb);
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }    
            
            return statusModel;
        }
    }
}
