﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportTableDataWebHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportTableDataWeb, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportTableDataWebHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportTableDataWeb, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportTableDataWeb reportTableDataWeb_ctr = new ReportTableDataWeb();
            ReportTableDataWeb tmpReportTableDataWeb_ctr = null;

            if (!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.Module))
                {
                    ReportTableDataWeb reportTableDataWeb = new ReportTableDataWeb();
                    ReportTableDataWeb tmpReportTableDataWeb = null;
                    if (req.CommandModel.UniqueId != 0)
                    {
                        tmpReportTableDataWeb = await ctx.ReportTableDataWeb.FindAsync(req.CommandModel.UniqueId);
                    }

                    if (tmpReportTableDataWeb != null)
                    {
                        reportTableDataWeb = tmpReportTableDataWeb;
                        reportTableDataWeb.ModifiedBy = req.UserIdentity.UserId;
                        reportTableDataWeb.ModifiedDate = req.CurrentDateTime;
                        reportTableDataWeb.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportTableDataWeb.ReportCd = req.CommandModel.ReportCd;
                        reportTableDataWeb.CreatedBy = req.UserIdentity.UserId;
                        reportTableDataWeb.CreatedDate = req.CurrentDateTime;
                        reportTableDataWeb.CreatedHost = req.UserIdentity.Host;
                    }
                    reportTableDataWeb.ColumnSeq = req.CommandModel.ColumnSeq;
                    reportTableDataWeb.ColumnDesc = req.CommandModel.ColumnDesc;
                    reportTableDataWeb.TableCd = req.CommandModel.TableCd;
                    reportTableDataWeb.ColumnData = req.CommandModel.ColumnData;
                    reportTableDataWeb.ColumnAlias = req.CommandModel.ColumnAlias;
                    reportTableDataWeb.IsHide = req.CommandModel.IsHide;
                    reportTableDataWeb.IsCompare = req.CommandModel.IsCompare;

                    if (tmpReportTableDataWeb == null)
                    {
                        ctx.ReportTableDataWeb.Add(reportTableDataWeb);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_TBL_DT_WEB");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_TBL_DT_WEB");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);

                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.Module))
                    {
                        if (req.CommandModel.UniqueId != 0)
                        {
                            tmpReportTableDataWeb_ctr = await ctx_ctr.ReportTableDataWeb.FindAsync(req.CommandModel.UniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportTableDataWeb_ctr != null ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportTableDataWeb_ctr != null ? req.CommandModel.UniqueId: null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_TBL_DT_WEB",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnSeq)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnDesc)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnDesc)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableCd)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.TableCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_DATA")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnData)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnData)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnData)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_ALIAS")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnAlias)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.ColumnAlias)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ColumnAlias)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "IS_HIDE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.IsHide)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.IsHide)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.IsHide)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "IS_COMPARE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.IsCompare)),
                                        ValueBefore = tmpReportTableDataWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableDataWeb_ctr.IsCompare)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.IsCompare)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }

            return statusModel;
        }
    }
}
