﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.FormSummaryConf.Commands;
using ARRA.GLOBAL.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Enumerations;
using ARRA.MODULE.Domain.Entities;
using ARRA.Persistence;
using System.Threading;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.App.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportTableWebHandler : AppBase, IRequestHandler<CommandsModel<RemoveFormSumConfReportTableWeb, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RemoveFormSumConfReportTableWebHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RemoveFormSumConfReportTableWeb, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };

            if (req.AccessMatrix.AllowApproval)
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_TBL_WEB");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_TBL_WEB");
                    }

                    ReportTableWeb reportTableWeb_ctr = new ReportTableWeb();
                    ReportTableWeb tmpReportTableWeb_ctr = null;

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);

                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.Module))
                    {
                        if (req.CommandModel.UniqueId != 0)
                        {
                            tmpReportTableWeb_ctr = await ctx_ctr.ReportTableWeb.FindAsync(req.CommandModel.UniqueId);
                        }
                    }

                    if (formDetail.Count > 0)
                    {

                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = base.GetActionType("REMOVE"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = req.CommandModel.UniqueId,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_TBL_WEB",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_FILTER")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableFilter)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableSequence)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
            }
            else
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.Module))
                {
                    ReportTableWeb reportTableWeb = await ctx.ReportTableWeb.FindAsync(req.CommandModel.UniqueId);
                    ctx.Remove(reportTableWeb);
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            

            return statusModel;
        }
    }
}
