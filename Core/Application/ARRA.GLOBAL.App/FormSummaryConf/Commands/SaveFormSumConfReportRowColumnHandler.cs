﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportRowColumnHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportRowColumn, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportRowColumnHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportRowColumn, StatusModel> req, CancellationToken cancellationToken)
        {
            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportRowColumn reportRowColumn_ctr = new ReportRowColumn();
            ReportRowColumn tmpReportRowColumn_ctr = null;

            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };
                          
            if (allow_aprv) {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_ROW_COL");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_ROW_COL");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.module))
                    {
                        if (req.CommandModel.uniqueId != 0)
                        {
                            tmpReportRowColumn_ctr = await ctx_ctr.ReportRowColumns.FindAsync(req.CommandModel.uniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportRowColumn_ctr != null? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportRowColumn_ctr != null ? req.CommandModel.uniqueId: null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_ROW_COL",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        ValueBefore = tmpReportRowColumn_ctr != null? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,                                                                                
                                    });
                                }

                                if (item.FieldName == "ROW_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSeq)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.RowSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.rowSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "COLUMN_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnSeq)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.ColumnSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "COLUMN_FORMULA")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnFormula)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.ColumnFormula)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnFormula)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "COLUMN_STATIC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnStatic)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.ColumnStatic)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnStatic)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "COLUMN_AGGREGATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnAggregate)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.ColumnAggregate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnAggregate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "IS_RECALCULATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.isRecalculate)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.IsRecalculate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.isRecalculate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "RECALCULATE_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.recalculateSeq)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.RecalculateSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.recalculateSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "RECALCULATE_TYPE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.recalculateType)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.RecalculateType)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.recalculateType)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "DIVISOR_AFTER_AGGREGATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.divisorAfterAggregate)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.DivisorAfterAggregate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.divisorAfterAggregate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "ROUNDED_AFTER_AGGREGATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.roundedAfterAggregate)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.RoundedAfterAggregate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.roundedAfterAggregate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "ABSOLUTE_AFTER_AGGREGATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.absoluteAfterAggregate)),
                                        ValueBefore = tmpReportRowColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportRowColumn_ctr.AbsoluteAfterAggregate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.absoluteAfterAggregate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }
            else {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportRowColumn reportRowColumn = new ReportRowColumn();
                    ReportRowColumn tmpReportRowColumn = null;
                    if (req.CommandModel.uniqueId != 0)
                    {
                        tmpReportRowColumn = await ctx.ReportRowColumns.FindAsync(req.CommandModel.uniqueId);
                    }

                    if (tmpReportRowColumn != null)
                    {
                        reportRowColumn = tmpReportRowColumn;
                        reportRowColumn.ModifiedBy = req.UserIdentity.UserId;
                        reportRowColumn.ModifiedDate = req.CurrentDateTime;
                        reportRowColumn.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportRowColumn.ReportCd = req.CommandModel.reportCd;
                        reportRowColumn.CreatedBy = req.UserIdentity.UserId;
                        reportRowColumn.CreatedDate = req.CurrentDateTime;
                        reportRowColumn.CreatedHost = req.UserIdentity.Host;
                    }
                    reportRowColumn.RowSeq = req.CommandModel.rowSeq;
                    reportRowColumn.ColumnSeq = req.CommandModel.columnSeq;
                    reportRowColumn.ColumnFormula = req.CommandModel.columnFormula;
                    reportRowColumn.ColumnStatic = req.CommandModel.columnStatic;
                    reportRowColumn.IsRecalculate = req.CommandModel.isRecalculate;
                    reportRowColumn.RecalculateSeq = req.CommandModel.recalculateSeq;
                    reportRowColumn.RecalculateType = req.CommandModel.recalculateType;
                    reportRowColumn.ColumnAggregate = req.CommandModel.columnAggregate;
                    reportRowColumn.DivisorAfterAggregate = req.CommandModel.divisorAfterAggregate;
                    reportRowColumn.RoundedAfterAggregate = req.CommandModel.roundedAfterAggregate;
                    reportRowColumn.AbsoluteAfterAggregate = req.CommandModel.absoluteAfterAggregate;

                    if (tmpReportRowColumn == null)
                    {
                        ctx.ReportRowColumns.Add(reportRowColumn);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }

            return statusModel;
        }
    }
}