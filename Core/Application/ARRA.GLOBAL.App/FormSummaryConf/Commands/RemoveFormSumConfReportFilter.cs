﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportFilter
    {
        public string Module { get; set; }
        public string ReportCode { get; set; }
        public long UniqueId { get; set; }
    }
}