﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportTableWebHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportTableWeb, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportTableWebHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportTableWeb, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel()
            {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportTableWeb reportTableWeb_ctr = new ReportTableWeb();
            ReportTableWeb tmpReportTableWeb_ctr = null;


            if (!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.Module))
                {
                    ReportTableWeb reportTableWeb = new ReportTableWeb();
                    ReportTableWeb tmpReportTableWeb = null;
                    if (req.CommandModel.UniqueId != 0)
                    {
                        tmpReportTableWeb = await ctx.ReportTableWeb.FindAsync(req.CommandModel.UniqueId);
                    }

                    if (tmpReportTableWeb != null)
                    {
                        reportTableWeb = tmpReportTableWeb;
                        reportTableWeb.ModifiedBy = req.UserIdentity.UserId;
                        reportTableWeb.ModifiedDate = req.CurrentDateTime;
                        reportTableWeb.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportTableWeb.ReportCd = req.CommandModel.ReportCd;
                        reportTableWeb.CreatedBy = req.UserIdentity.UserId;
                        reportTableWeb.CreatedDate = req.CurrentDateTime;
                        reportTableWeb.CreatedHost = req.UserIdentity.Host;
                    }
                    reportTableWeb.TableCd = req.CommandModel.TableCd;
                    reportTableWeb.TableDesc = req.CommandModel.TableDesc;
                    reportTableWeb.TableFilter = req.CommandModel.TableFilter;
                    reportTableWeb.TableSequence = req.CommandModel.TableSequence;

                    if (tmpReportTableWeb == null)
                    {
                        ctx.ReportTableWeb.Add(reportTableWeb);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_TBL_WEB");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_TBL_WEB");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                    
                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.Module))
                    {
                        if (req.CommandModel.UniqueId != 0)
                        {
                            tmpReportTableWeb_ctr = await ctx_ctr.ReportTableWeb.FindAsync(req.CommandModel.UniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportTableWeb_ctr != null ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportTableWeb_ctr != null ? req.CommandModel.UniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_TBL_WEB",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        ValueBefore = tmpReportTableWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.ReportCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableCd)),
                                        ValueBefore = tmpReportTableWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableCd)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableDesc)),
                                        ValueBefore = tmpReportTableWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableDesc)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_FILTER")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableFilter)),
                                        ValueBefore = tmpReportTableWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableFilter)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableFilter)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "TBL_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableSequence)),
                                        ValueBefore = tmpReportTableWeb_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportTableWeb_ctr.TableSequence)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.TableSequence)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }

            return statusModel;
        }
    }
}
