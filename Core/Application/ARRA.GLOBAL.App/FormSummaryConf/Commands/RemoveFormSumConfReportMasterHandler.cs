﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportMasterHandler : AppBase, IRequestHandler<CommandsModel<RemoveFormSumConfReportMaster, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public RemoveFormSumConfReportMasterHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<RemoveFormSumConfReportMaster, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };

            if (req.AccessMatrix.AllowApproval)
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_MASTER");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_MASTER");
                    }

                    ReportMaster reportMaster = new ReportMaster();
                    ReportMaster tmpReportMaster = null;

                    using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                    {
                        tmpReportMaster = await ctx.ReportMasters.FindAsync(req.CommandModel.reportCode);
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = base.GetActionType("REMOVE"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportMaster.UniqueId,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_MASTER",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportDesc)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_TEMPLATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportTemplate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_SHEET")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportSheet)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "FORM_CD_DTL")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = null,
                                        ValueBefore = ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.FormCdDtl)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }
            else
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportMaster reportMaster = await ctx.ReportMasters.FindAsync(req.CommandModel.reportCode);
                    ctx.Remove(reportMaster);
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }

            return statusModel;
        }
    }
}