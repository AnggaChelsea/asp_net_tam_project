﻿using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportRow
    {
        public long uniqueId { get; set; }
        public string module { get; set; }
        public string reportCd { get; set; }
        public int rowSeq { get; set; }
        public string rowDesc { get; set; }
        public string componentCd { get; set; }
        public string dbNm { get; set; }
        public string tableNm { get; set; }
        public int parentRowSeq { get; set; }
        public string text { get; set; }
    }
}