﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportColumnHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportColumn, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportColumnHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportColumn, StatusModel> req, CancellationToken cancellationToken)
        {
            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };

            var allow_aprv = req.AccessMatrix.AllowApproval;
            ReportColumn reportColumn_ctr = new ReportColumn();
            ReportColumn tmpReportColumn_ctr = null;


            if(!allow_aprv)
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportColumn reportColumn = new ReportColumn();
                    ReportColumn tmpReportColumn = null;
                    if (req.CommandModel.uniqueId != 0)
                    {
                        tmpReportColumn = await ctx.ReportColumns.FindAsync(req.CommandModel.uniqueId);
                    }

                    if (tmpReportColumn != null)
                    {
                        reportColumn = tmpReportColumn;
                        reportColumn.ModifiedBy = req.UserIdentity.UserId;
                        reportColumn.ModifiedDate = req.CurrentDateTime;
                        reportColumn.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportColumn.ReportCd = req.CommandModel.reportCd;
                        reportColumn.CreatedBy = req.UserIdentity.UserId;
                        reportColumn.CreatedDate = req.CurrentDateTime;
                        reportColumn.CreatedHost = req.UserIdentity.Host;
                    }
                    reportColumn.ColumnSeq = req.CommandModel.columnSeq;
                    reportColumn.ColumnExcelPos = req.CommandModel.columnExcelPos;
                    reportColumn.ColumnDest = req.CommandModel.columnDest;

                    if (tmpReportColumn == null)
                    {
                        ctx.ReportColumns.Add(reportColumn);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }
            else
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_COL");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_COL");
                    }

                    IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);
                    using (MODULEDbContext ctx_ctr = base.GetDbContext(req.CommandModel.module))
                    {
                        if (req.CommandModel.uniqueId != 0)
                        {
                            tmpReportColumn_ctr = await ctx_ctr.ReportColumns.FindAsync(req.CommandModel.uniqueId);
                        }
                    }


                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = tmpReportColumn_ctr != null ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = tmpReportColumn_ctr != null ? req.CommandModel.uniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_COL",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST" && item.FieldName != "UID")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        ValueBefore = tmpReportColumn_ctr != null? tmpReportColumn_ctr.ReportCd : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCd)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_SEQ")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnSeq)),
                                        ValueBefore = tmpReportColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportColumn_ctr.ColumnSeq)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnSeq)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_EXCEL_POS")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnExcelPos)),
                                        ValueBefore = tmpReportColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportColumn_ctr.ColumnExcelPos)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnExcelPos)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }

                                if (item.FieldName == "COLUMN_DEST")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnDest)),
                                        ValueBefore = tmpReportColumn_ctr != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportColumn_ctr.ColumnDest)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.columnDest)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host,
                                    });
                                }
                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }
                //end of approval
            }

            return statusModel;
        }
    }
}