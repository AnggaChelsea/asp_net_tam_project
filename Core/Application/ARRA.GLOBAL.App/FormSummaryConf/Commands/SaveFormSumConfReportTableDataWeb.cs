﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportTableDataWeb
    {
        public string Module { get; set; }
        public long UniqueId { get; set; }
        public string ReportCd { get; set; }
        public int ColumnSeq { get; set; }
        public string ColumnDesc { get; set; }
        public string TableCd { get; set; }
        public string ColumnData { get; set; }
        public string ColumnAlias { get; set; }
        public string IsHide { get; set; }
        public string IsCompare { get; set; }
    }
}
