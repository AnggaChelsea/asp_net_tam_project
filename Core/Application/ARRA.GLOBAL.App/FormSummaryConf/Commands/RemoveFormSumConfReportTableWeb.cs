﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class RemoveFormSumConfReportTableWeb
    {
        public string Module { get; set; }
        public string ReportCode { get; set; }
        public string TableCd { get; set; }
        public long UniqueId { get; set; }
    }
}
