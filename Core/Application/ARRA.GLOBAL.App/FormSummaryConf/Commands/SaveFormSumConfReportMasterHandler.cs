﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Persistence;
using ARRA.Common.Model;
using ARRA.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using ARRA.MODULE.Domain.Entities;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.GLOBAL.App.Exceptions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using ARRA.GLOBAL.Domain.Enumerations;

namespace ARRA.GLOBAL.App.FormSummaryConf.Commands
{
    public class SaveFormSumConfReportMasterHandler : AppBase, IRequestHandler<CommandsModel<SaveFormSumConfReportMaster, StatusModel>, StatusModel>
    {
        private readonly GLOBALDbContext _context;
        public SaveFormSumConfReportMasterHandler(
            GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }

        public async Task<StatusModel> Handle(CommandsModel<SaveFormSumConfReportMaster, StatusModel> req, CancellationToken cancellationToken)
        {
            var allow_aprv = req.AccessMatrix.AllowApproval;            
            
            StatusModel statusModel = new StatusModel() {
                status = CommandStatus.Success.ToString()
            };


            if (allow_aprv)
            {
                //Approval
                Menu menu = await _context.Menus.Where(f => f.MenuCode == req.AccessMatrix.MenuCode).FirstOrDefaultAsync(cancellationToken);
                if (menu != null)
                {
                    FormHeader form = await _context.FormHeaders.FindAsync("F_RPT_MASTER");

                    if (form == null)
                    {
                        throw new NotFoundException(nameof(FormHeader), "F_RPT_MASTER");
                    }

                    ReportMaster reportMaster = new ReportMaster();
                    ReportMaster tmpReportMaster = null;

                    using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                    {
                         tmpReportMaster = await ctx.ReportMasters.FindAsync(req.CommandModel.reportCode);
                    }

                        IList<FormDetail> formDetail = await _context.FormDetails.Where(f => f.FormCode == form.FormCode && f.EdrShow == true).ToListAsync(cancellationToken);

                    if (formDetail.Count > 0)
                    {
                        ApprovalHeader mHeader = new ApprovalHeader
                        {
                            MenuCode = menu.MenuCode,
                            MenuName = menu.MenuName,
                            ActionType = req.CommandModel.uniqueId != 0 ? base.GetActionType("EDIT") : base.GetActionType("ADD"),
                            ApprovalStatus = ApprovalStatus.Pending.ToString(),
                            Module = menu.Module,
                            RefferalId = req.CommandModel.uniqueId != 0 ? req.CommandModel.uniqueId : null,
                            TaskType = req.AccessMatrix.AllowApproval ? TaskType.Approval.ToString() : TaskType.Temp.ToString(),
                            FormCode = "F_RPT_MASTER",
                            CreatedBy = req.UserIdentity.UserId,
                            CreatedDate = req.CurrentDateTime,
                            CreatedHost = req.UserIdentity.Host,
                            PeriodType = form.PeriodType
                        };
                        _context.ApprovalHeaders.Add(mHeader);
                        await _context.SaveChangesAsync(cancellationToken);

                        Int64 headerid = mHeader.UniqueId;
                        foreach (FormDetail item in formDetail)
                        {
                            if (item.FieldName != "CREATED_DT" && item.FieldName != "CREATED_BY" && item.FieldName != "CREATED_HOST"
                               && item.FieldName != "MODIFIED_DT" && item.FieldName != "MODIFIED_BY" && item.FieldName != "MODIFIED_HOST")
                            {
                                if (item.FieldName == "REPORT_CD")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCode)),
                                        ValueBefore = tmpReportMaster != null? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportCd)) :  ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportCode)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_DESC")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportDescription)),
                                        ValueBefore = tmpReportMaster != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportDesc)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportDescription)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_TEMPLATE")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportTemplate)),
                                        ValueBefore = tmpReportMaster != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportTemplate)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportTemplate)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "REPORT_SHEET")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportSheet)),
                                        ValueBefore = tmpReportMaster != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.ReportSheet)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.reportSheet)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                                if (item.FieldName == "FORM_CD_DTL")
                                {
                                    _context.ApprovalDetails.Add(new ApprovalDetail
                                    {
                                        FieldName = item.FieldName,
                                        FormColName = item.GrdColumnName,
                                        FormSeq = Convert.ToInt16(item.GrdColumnSeq),
                                        HeaderId = headerid,
                                        ValueAfter = ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.formCodeDtl)),
                                        ValueBefore = tmpReportMaster != null ? ARRA.Common.Utility.SafeSqlString(Convert.ToString(tmpReportMaster.FormCdDtl)) : ARRA.Common.Utility.SafeSqlString(Convert.ToString(req.CommandModel.formCodeDtl)),
                                        CreatedBy = req.UserIdentity.UserId,
                                        CreatedDate = req.CurrentDateTime,
                                        CreatedHost = req.UserIdentity.Host
                                    });
                                }

                            }
                        }

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                }

                //end of approval
            }
            else
            {
                using (MODULEDbContext ctx = base.GetDbContext(req.CommandModel.module))
                {
                    ReportMaster reportMaster = new ReportMaster();
                    ReportMaster tmpReportMaster = await ctx.ReportMasters.FindAsync(req.CommandModel.reportCode);
                    if (tmpReportMaster != null)
                    {
                        reportMaster = tmpReportMaster;
                        reportMaster.ModifiedBy = req.UserIdentity.UserId;
                        reportMaster.ModifiedDate = req.CurrentDateTime;
                        reportMaster.ModifiedHost = req.UserIdentity.Host;
                    }
                    else
                    {
                        reportMaster.ReportCd = req.CommandModel.reportCode;
                        reportMaster.CreatedBy = req.UserIdentity.UserId;
                        reportMaster.CreatedDate = req.CurrentDateTime;
                        reportMaster.CreatedHost = req.UserIdentity.Host;
                    }
                    reportMaster.ReportCd = req.CommandModel.reportCode;
                    reportMaster.ReportDesc = req.CommandModel.reportDescription;
                    reportMaster.ReportSheet = req.CommandModel.reportSheet;
                    reportMaster.FormCdDtl = req.CommandModel.formCodeDtl;


                    if (tmpReportMaster == null)
                    {
                        ctx.ReportMasters.Add(reportMaster);
                    }
                    await ctx.SaveChangesAsync(cancellationToken);
                }
            }                       
            return statusModel;
        }
    }
}