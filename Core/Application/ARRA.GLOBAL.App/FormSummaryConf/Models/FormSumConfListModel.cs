﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.FormSummaryConf.Models
{
    public class FormSumConfListModel
    {
        public dynamic reportMaster { get; set; }
        public dynamic reportRow { get; set; }
        public dynamic reportCol { get; set; }
        public dynamic reportRowCol { get; set; }
        public dynamic reportFilter{ get; set; }
    }
}