﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.App.FormSummaryConf.Models
{
    public class FormSumConfReportTableModel
    {
        public dynamic reportTable{ get; set; }
        public dynamic reportTableData{ get; set; }
    }
}
