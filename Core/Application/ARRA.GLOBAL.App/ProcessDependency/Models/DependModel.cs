﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessDependency.Models
{
    public class DependModel
    {
        public string ProcessNo { get; set; }
        public string DependNo { get; set; }
    }
}
