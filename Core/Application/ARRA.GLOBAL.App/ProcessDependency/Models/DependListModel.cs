﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.ProcessDependency.Models
{
    public class DependListModel
    {
        public IList<DependModel> Depends { get; set; }
    }
}
