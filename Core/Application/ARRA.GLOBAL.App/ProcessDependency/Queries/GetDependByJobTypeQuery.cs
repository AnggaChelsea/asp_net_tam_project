﻿namespace ARRA.GLOBAL.App.ProcessDependency.Queries
{
    public class GetDependByJobTypeQuery
    {
        public string processType { get; set; }
        public string module { get; set; }
    }
}