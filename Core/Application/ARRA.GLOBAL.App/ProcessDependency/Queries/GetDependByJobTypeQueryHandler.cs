﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.ProcessDataSource.Models;
using System.Collections.Generic;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.ProcessDependency.Models;
using Microsoft.Extensions.Configuration;
using System;

namespace ARRA.GLOBAL.App.ProcessDependency.Queries
{
    public class GetDependByJobTypeQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetDependByJobTypeQuery, DependListModel>, DependListModel>
    {
        //private readonly GLOBALDbContext _context;

        //public GetDependByJobTypeQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        //{
        //    _context = context;
        //}

        public GetDependByJobTypeQueryHandler(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<DependListModel> Handle(QueriesModel<GetDependByJobTypeQuery, DependListModel> req, CancellationToken cancellationToken)
        {
            DependListModel vm = new DependListModel();
            using (MODULEDbContext ctx = base.GetDbContext(req.QueryModel.module))
            {
                ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                ctx.ChangeTracker.LazyLoadingEnabled = false;

                vm = new DependListModel
                {
                    Depends =
                        await ctx.ProcessDependenciess
                        .Where(f => f.ProcessType == req.QueryModel.processType)
                        .Select(c => new DependModel
                        {
                            DependNo = c.DependProcessNo,
                            ProcessNo = c.ProcessNo
                        }).ToListAsync(cancellationToken)
                };
            }

            return vm;
        }
    }
}