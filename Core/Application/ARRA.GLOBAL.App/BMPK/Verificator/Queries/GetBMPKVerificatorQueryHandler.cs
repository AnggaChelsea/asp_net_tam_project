﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ARRA.Common.Model;
using ARRA.Persistence;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.BMPK.BMPKVerificator.Models;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace ARRA.GLOBAL.App.BMPK.BMPKVerificator.Queries
{
    public class GetBMPKVerificatorQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetBMPKVerificatorQuery, BMPKVerificatorModel>, BMPKVerificatorModel>
    {
        private readonly GLOBALDbContext _context;

        public GetBMPKVerificatorQueryHandler(GLOBALDbContext context, IConfiguration configuration) : base(context, configuration)
        {
            _context = context;
        }


        public async Task<BMPKVerificatorModel> Handle(QueriesModel<GetBMPKVerificatorQuery, BMPKVerificatorModel> req, CancellationToken cancellationToken)
        {
            dynamic rows;
            using (MODULEDbContext ctx = base.GetDbContext("BMPK"))
            {
                DynamicParameters dpTable = new DynamicParameters();
                dpTable.Add("@CIF", req.QueryModel.cif);
                dpTable.Add("@NIK", req.QueryModel.nik);
                rows = await ctx.Database.GetDbConnection().QueryAsync("DBO.UDPS_BMPK_VERIFICATOR", dpTable, null, null, CommandType.StoredProcedure);
            }
            
            return new BMPKVerificatorModel
            {
                rows = rows
            };
        }
    }
}