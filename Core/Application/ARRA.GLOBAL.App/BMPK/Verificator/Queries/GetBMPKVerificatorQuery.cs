﻿using MediatR;
using ARRA.GLOBAL.App.Forms.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.GLOBAL.App.BMPK.BMPKVerificator.Queries
{
    public class GetBMPKVerificatorQuery
    {
        public dynamic cif { get; set; }
        public dynamic nik { get; set; }
    }

}