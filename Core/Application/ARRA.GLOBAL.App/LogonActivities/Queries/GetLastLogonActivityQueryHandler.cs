﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ARRA.Common.Model;
using ARRA.Persistence;
using ARRA.GLOBAL.App.LogonActivites.Models;

namespace ARRA.GLOBAL.App.LogonActivites.Queries
{
    public class GetLastLogonActivityQueryHandler
        : AppBase, IRequestHandler<QueriesModel<GetLastLogonActivityQuery, LogonListActivityModel>, LogonListActivityModel>
    {
        private readonly GLOBALDbContext _context;

        public GetLastLogonActivityQueryHandler(GLOBALDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<LogonListActivityModel> Handle (QueriesModel<GetLastLogonActivityQuery, LogonListActivityModel> req, CancellationToken cancellationToken)
        {
            IList<LogonActivityModel> lookup = await (from b in _context.LogonActivities
                                                        where b.UserId == req.UserIdentity.UserId
                                                        orderby  b.CreatedDate descending                                                      
                                                        select new LogonActivityModel
                                                        {
                                                            user = b.UserId,
                                                            login = b.LoginStatus,
                                                            date = b.CreatedDate,
                                                            host = b.CreatedHost
                                                        }).Take(5).ToListAsync(cancellationToken);

            return new LogonListActivityModel
            {
                data = lookup
            };
        }
    }
}