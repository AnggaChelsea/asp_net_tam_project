﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ARRA.GLOBAL.App.LogonActivites.Models
{
    public class LogonActivityModel
    {
        public string user { get; set; }

        public string login { get; set; }

        public DateTime? date { get; set; }

        public string host { get; set; }
    }
}
