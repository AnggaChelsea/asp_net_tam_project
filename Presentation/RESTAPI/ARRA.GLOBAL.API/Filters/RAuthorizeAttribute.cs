﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Authentications.Queries;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ARRA.GLOBAL.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly string _routeUrl;
        private const string _notAuthorizedMsg = ARRA.Common.ErrorMessages.NotAuthorized;
        //protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());

        //private readonly IAccessMatrix _matrix = new ARRA.GLOBAL.App.Authentications.Queries.GetAmxByRoute();
        public RAuthorizeAttribute()
        {

        }

        public RAuthorizeAttribute(string RouteUrl)
        {
            _routeUrl = RouteUrl.ToLower();
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                if (context.HttpContext.User.Identity.IsAuthenticated)
                {
                    string ctrl = Convert.ToString(context.RouteData.Values["controller"]);
                    string form = Convert.ToString(context.RouteData.Values["code"]);
                    string routeUrl = "";
                    if (string.IsNullOrEmpty(_routeUrl))
                    {
                        routeUrl = "/" + ctrl + "/" + form;
                    }
                    else
                    {
                        routeUrl = _routeUrl;
                    }

                    if (routeUrl == "")
                    {
                        context.Result = new BadRequestObjectResult(new JsonResult(new { errors = _notAuthorizedMsg }));
                    }
                    else
                    {
                        Claim usrmdl = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "UserModule").FirstOrDefault();
                        Claim role = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "GroupAccess").FirstOrDefault();

                        if (usrmdl != null && role != null)
                        {
                            ARRA.Persistence.GLOBALDbContext db = context.HttpContext.RequestServices.GetService<ARRA.Persistence.GLOBALDbContext>();

                            AccessMatrixModel amx = new ARRA.GLOBAL.App.Authentications.Queries.GetAmxByRoute(db).GetAmx(role.Value, routeUrl);
                            if (amx == null)
                            {
                                context.HttpContext.Response.ContentType = "application/json";
                                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                                context.Result = new JsonResult(new
                                {
                                    errors = _notAuthorizedMsg
                                }
                                );
                            }
                            else
                            {
                                if (usrmdl.Value.Contains(amx.Module + ",")) //check by module
                                {
                                    context.HttpContext.Items.Add("screeninfo", amx);
                                }
                                else
                                {
                                    context.HttpContext.Response.ContentType = "application/json";
                                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                                    context.Result = new JsonResult(new
                                    {
                                        errors = _notAuthorizedMsg
                                    }
                                    );
                                }
                            }
                            amx = null;
                            db = null;
                        }
                        else
                        {
                            context.HttpContext.Response.ContentType = "application/json";
                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            context.Result = new JsonResult(new
                            {
                                errors = _notAuthorizedMsg
                            }
                            );
                        }
                    }
                }
                else
                {
                    context.HttpContext.Response.ContentType = "application/json";
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Result = new JsonResult(new
                    {
                        errors = "Session Expired"
                    });
                }
            }
            catch (Exception ex)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new
                {
                    errors = ex.Message
                }
                );
            }

            #region Backup Old
            /*
            string ctrl = Convert.ToString(context.RouteData.Values["controller"]).ToLower();
            string form = Convert.ToString(context.RouteData.Values["code"]);
            Claim usrmdl = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "UserModule").FirstOrDefault();
            Claim menu = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "Menu").FirstOrDefault();
            Claim amx = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "AMX").FirstOrDefault();
            Claim mdl = ((ClaimsIdentity)context.HttpContext.User.Identity).Claims.Where(x => x.Type == "ModuleList").FirstOrDefault();

            if (usrmdl != null && role != null)
            {
                string[] strs = menu.Value.Split('^');
                int idx;
                if (string.IsNullOrEmpty(_routeUrl))
                {
                    idx = Array.IndexOf(strs, "/" + ctrl + "/" + form.ToLower());
                }
                else
                {
                    idx = Array.IndexOf(strs, ctrl == _routeUrl);
                }

                if (idx > 0)
                {
                    string formModule = mdl.Value.Split('^')[idx];
                    if (usrmdl.Value.Contains(formModule + ",")) //check by module
                    {
                        //insert/update/delete/export/import/approval
                        //0     /1     /2     /3     /4     /5
                        string[] access = amx.Value.Split('^')[idx].Split('|');
                        AccessMatrixModel ScreenAccess = new AccessMatrixModel();
                        ScreenAccess.Menu = strs[idx]; strs = null;
                        ScreenAccess.AllowInsert = Convert.ToBoolean(Convert.ToInt32(access[0]));
                        ScreenAccess.AllowUpdate = Convert.ToBoolean(Convert.ToInt32(access[1]));
                        ScreenAccess.AllowDelete = Convert.ToBoolean(Convert.ToInt32(access[2]));
                        ScreenAccess.AllowExport = Convert.ToBoolean(Convert.ToInt32(access[3]));
                        ScreenAccess.AllowImport = Convert.ToBoolean(Convert.ToInt32(access[4]));
                        ScreenAccess.AllowApproval = Convert.ToBoolean(Convert.ToInt32(access[5]));
                        access = null;

                        context.HttpContext.Items.Add("screeninfo", ScreenAccess);
                        //success authorized.
                    }
                    else
                    {
                        context.Result = new UnauthorizedObjectResult(_notAuthorizedMsg);
                    }
                }
                else
                {
                    context.Result = new UnauthorizedObjectResult(_notAuthorizedMsg);
                }
            }
            else
            {
                context.Result = new UnauthorizedObjectResult(_notAuthorizedMsg);
            }
            */
            #endregion
        }
    }
}