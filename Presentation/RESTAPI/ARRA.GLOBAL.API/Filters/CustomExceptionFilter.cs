﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ARRA.GLOBAL.App.Exceptions;

namespace ARRA.GLOBAL.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ValidationException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(
                    ((ValidationException)context.Exception).Failures);

                return;
            }


            //20190710 JJ: for other should err says please contact admin, and write log file.
            var code = HttpStatusCode.InternalServerError;
            string errMsg = "";
            if (context.Exception is NotFoundException)
            {
                code = HttpStatusCode.NotFound;
                
            }
            else if (context.Exception is DeleteFailureException)
            {
                code = HttpStatusCode.BadRequest;
            }
            else if (context.Exception is DatabaseException)
            {
                errMsg = ((DatabaseException)context.Exception).CustomMessage;
                code = HttpStatusCode.BadRequest;
            }
            else if (context.Exception is CustomException)
            {
                errMsg = ((CustomException)context.Exception).CustomMessage;
                code = HttpStatusCode.BadRequest;
            }
            else
            {
                code = HttpStatusCode.InternalServerError;
            }

            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = (int)code;

            if (errMsg == "")
            {
                errMsg = context.Exception.Message;
            }

            if(code == HttpStatusCode.BadRequest)
            {
                context.Result = new JsonResult(new
                {
                    errors = new[] { errMsg }
                });
            }
            else
            {
                context.Result = new JsonResult(new
                {
                    errors = new[] { errMsg },
                    stackTrace = context.Exception.StackTrace
                });
            }
            
        }
    }
}
