﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Restore.Commands;
using ARRA.GLOBAL.App.Restore.Models;
using ARRA.GLOBAL.App.Restore.Queries;
using ARRA.GLOBAL.Filters;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize("/restore")]
    public class RestoreController : BaseController
    {
        private readonly IConfiguration __configuration;
        public RestoreController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpPost]
        public async Task<IActionResult> GetData(GetRestoreDataQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetRestoreDataQuery, RestoreListModel>(cmd)));
        }
        #endregion

        #region restore
        [HttpPost]
        public async Task<IActionResult> CreateRestoreTask(CreateRestoreTaskCommand cmd)
        {
            StatusModel m = await Mediator.Send(GetCommand<CreateRestoreTaskCommand, StatusModel>(cmd));
            return Ok(m);
        }
        #endregion
    }
}