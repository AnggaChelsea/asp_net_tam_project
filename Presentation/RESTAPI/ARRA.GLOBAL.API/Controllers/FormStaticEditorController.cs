﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ARRA.Common;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Approvals.Models;
using ARRA.GLOBAL.App.Approvals.Queries;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Task.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Task.Queries;
using ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Commands;
using ARRA.GLOBAL.App.Forms.Customs.ValidationResult.Queries;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize]
    public class FormStaticEditorController : BaseController
    {
        private readonly IConfiguration __configuration;
        public FormStaticEditorController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDetailGridQuery, FormListModel>(new GetFormDetailGridQuery { FormCode = code })));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormDataQuery, DataListModel>(cmd)));
        }
        [HttpGet("{code}/{uid}")]
        public async Task<IActionResult> GetDataDetail(string code, int uid)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDataDetailQuery, DataListModel>(new GetFormDataDetailQuery { formCode = code, uid = uid })));
        }
        [HttpGet("{code}")]
        public async Task<IActionResult> GetEditor(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormEditorQuery, EditorListModel>(new GetFormEditorQuery { FormCode = code })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetBusParHdr(string code, GetBusinessParamQuery cmd)
        {
            cmd.FormCode = cmd.FormCode;
            cmd.isHeaderOnly = true;
            cmd.isDetailOnly = false;
            return Ok(await Mediator.Send(GetQuery<GetBusinessParamQuery, BusinessParamModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetBusParDtl(string code, GetBusinessParamQuery cmd)
        {
            cmd.FormCode = cmd.FormCode;
            cmd.isHeaderOnly = false;
            cmd.isDetailOnly = true;
            return Ok(await Mediator.Send(GetQuery<GetBusinessParamQuery, BusinessParamModel>(cmd)));
        }
        #endregion

        #region CRUD
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveFilter(string code, SaveFilterCommand cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetCommand<SaveFilterCommand, StatusModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> SaveData(string code, SaveDataCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action;
            jObject.TryGetValue("screenActionType", out action);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                return Ok(await Mediator.Send(GetCommand<SaveDataCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }

        }
        #endregion

        #region export
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTask(string code, CreateExportTaskCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileByIdQuery, FileModel>(new GetExportedFileByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region import
        [HttpPost("{code}")]
        public async Task<IActionResult> Upload(string code, Microsoft.AspNetCore.Http.IFormFile file)
        {
            if (AccessMatrix.AllowImport)
            {
                string checkresult = "1";
                try
                {
                    //if (file.ContentType == Utility.GetMimeTypes()[".xlsx"]) //allowed type
                    //{
                        int idx = file.FileName.LastIndexOf(".");
                        if (idx > 0)
                        {
                            string ext = file.FileName.Substring(idx + 1, file.FileName.Length - idx - 1).ToLower();
                            if (ext == "xlsx")
                            {
                                if (file.Length <= (new ApplicationSettings(__configuration).UploadMaxFileSize * (int)1048576)) //compare in bytes
                                {
                                    ParamHeaderModel m = await Mediator.Send(GetQuery<GetSysParByModuleQuery, ParamHeaderModel>(new GetSysParByModuleQuery { formCode = code, parName = SystemParameterType.IMPORT_FILE_LOCATION.ToString() }));
                                    if (m != null)
                                    {
                                        var path = m.parValue;
                                        if (file.Length > 0)
                                        {
                                            string originalName = file.FileName;
                                            if (originalName.Contains("\\"))
                                            {
                                                originalName = originalName.Substring(originalName.LastIndexOf('\\') + 1, originalName.Length - originalName.LastIndexOf('\\') - 1);
                                            }

                                            string newName = code + "-" + _currDate.Now.Ticks.ToString() + ".xlsx";
                                            var filePath = Path.Combine(path, newName);
                                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                                            {
                                                await file.CopyToAsync(fileStream);
                                            }

                                            await Mediator.Send(GetCommand<CreateImportTaskCommand, StatusModel>(new CreateImportTaskCommand { formCode = code, fileName = newName, originalFileName = originalName }));
                                        }
                                    }
                                    else
                                    {
                                        checkresult = "Server Path Not Found";
                                    }
                                }
                                else
                                {
                                    checkresult = "File Exceeds the Maximum Size (" + new ApplicationSettings(__configuration).UploadMaxFileSize.ToString() + "MB)";
                                }
                            }
                        }
                        else
                        {
                            checkresult = "Invalid File Type, Please Upload Excel (XLSX) file";
                        }
                    //}
                    //else
                    //{
                    //    checkresult = "Invalid File Type, Please Upload Excel (XLSX) file";
                    //}

                }
                catch (Exception ex)
                {
                    checkresult = ex.Message;
                }

                if (checkresult == "1")
                {
                    return Ok(new JsonResult(new
                    {
                        status = checkresult
                    }));
                }
                else
                {
                    return BadRequestRequest(new List<string>() { checkresult });
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region Custom User
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveDataUser(string code, ARRA.GLOBAL.App.Forms.Customs.User.Commands.SaveUserCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action,pass,confirmpass;
            jObject.TryGetValue("screenActionType", out action);
            jObject.TryGetValue("PASSWD", out pass);
            jObject.TryGetValue("PASSWD_CRFM", out confirmpass);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                cmd.action = Convert.ToString(action);
                cmd.password = Convert.ToString(pass);
                cmd.confirmPassword = Convert.ToString(confirmpass);
                return Ok(await Mediator.Send(GetCommand<ARRA.GLOBAL.App.Forms.Customs.User.Commands.SaveUserCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region Custom Approval Group
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveDataApprovalGroup(string code, ARRA.GLOBAL.App.Forms.Customs.ApprovalGroup.Commands.SaveApprovalGroupCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action;
            jObject.TryGetValue("screenActionType", out action);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                cmd.action = Convert.ToString(action);
                return Ok(await Mediator.Send(GetCommand<ARRA.GLOBAL.App.Forms.Customs.ApprovalGroup.Commands.SaveApprovalGroupCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region Custom Branh Group
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveDataBranchGroup(string code, ARRA.GLOBAL.App.Forms.Customs.BranchGroup.Commands.SaveBranchGroupCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action;
            jObject.TryGetValue("screenActionType", out action);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                cmd.action = Convert.ToString(action);
                return Ok(await Mediator.Send(GetCommand<ARRA.GLOBAL.App.Forms.Customs.BranchGroup.Commands.SaveBranchGroupCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region revise custom
        [HttpPost("{code}")]
        public async Task<IActionResult> GetReviseData(string code, GetReviseDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetReviseDataQuery, DataListModel>(cmd)));
        }

        [HttpGet("{code}/{uid}")]
        public async Task<IActionResult> GetReviseDataDetail(string code, int uid)
        {
            return Ok(await Mediator.Send(GetQuery<GetReviseDataDetailQuery, DataListModel>(new GetReviseDataDetailQuery { formCode = code, uid = uid })));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveReviseData(string code, SaveReviseDataCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken uid;
            jObject.TryGetValue("UID", out uid);
            jObject = null;
            bool process = false;

            //check authorize
            ApprovalModel m = await Mediator.Send(GetQuery<GetApprovalByIdQuery, ApprovalModel>(new GetApprovalByIdQuery { id= Convert.ToInt64(uid) }));
            if (m != null)
            {

                if (m.actionType == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
                {
                    process = true;
                }
                if (m.actionType == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
                {
                    process = true;
                }
                if (m.actionType == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
                {
                    process = true;
                }
            }
            m = null;

            if (process)
            {
                cmd.formCode = code;
                return Ok(await Mediator.Send(GetCommand<SaveReviseDataCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }

        }
        #endregion revise

        #region Validation Result
        [HttpPost("{code}")]
        public async Task<IActionResult> GetDataValidation(string code, GetValDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetValDataQuery, DataListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportValTask(string code, CreateExportValTaskCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportValTaskCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

    }
}