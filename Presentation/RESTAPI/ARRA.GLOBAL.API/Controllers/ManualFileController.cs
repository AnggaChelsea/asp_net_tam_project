﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.ManualUpload.Commands;
using ARRA.GLOBAL.App.ManualUpload.Models;
using ARRA.GLOBAL.App.ManualUpload.Queries;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    [RAuthorize]
    public class ManualFileController : BaseController
    {
        private readonly IConfiguration __configuration;
        public ManualFileController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetModule(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetAuthModuleLookupQuery, ARRA.GLOBAL.App.ManualUpload.Models.LookupListModel>(new GetAuthModuleLookupQuery { })));
        }
        [HttpGet("{code}/{module}")]
        public async Task<IActionResult> GetManualFile(string code, string module)
        {
            return Ok(await Mediator.Send(GetQuery<GetManualUploadLookupQuery, ManualFileListModel>(new GetManualUploadLookupQuery { formCode = code, module = module })));
        }
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormInfoQuery, ManualFileScreenInfoModel>(new GetFormInfoQuery { })));
        }
        #endregion

        #region export
        [HttpGet("{code}/{processNo}")]
        public async Task<IActionResult> Download(string code, string processNo)
        {
            if (AccessMatrix.AllowExport)
            {
                TemplateFileModel file = await Mediator.Send(GetQuery<GetTemplateFileQuery, TemplateFileModel>(new GetTemplateFileQuery { processNo = processNo }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region upload
        [HttpPost("{code}/{processNo}")]
        public async Task<IActionResult> Upload(string code,string processNo, Microsoft.AspNetCore.Http.IFormFile file)
        {
            if (AccessMatrix.AllowImport)
            {
                string checkresult = "1";
                if (file.ContentType == Utility.GetMimeTypes()[".xlsx"]
                    || file.ContentType == Utility.GetMimeTypes()[".txt"]
                    || file.ContentType == Utility.GetMimeTypes()[".csv"]
                    || file.ContentType == Utility.GetMimeTypes()[".csv1"]) //allowed type
                {
                    int idx = file.FileName.LastIndexOf(".");
                    if (idx > 0)
                    {
                        string ext = file.FileName.Substring(idx + 1, file.FileName.Length - idx - 1).ToLower();
                        if (ext == "xlsx" || ext=="txt" || ext == "csv")
                        {
                            if (file.Length <= (new ApplicationSettings(__configuration).UploadMaxFileSize * (int)1048576)) //compare in bytes
                            {
                                string path = await Mediator.Send(GetQuery<GetUploadPathByProcNoQuery, string>(new GetUploadPathByProcNoQuery { procNo = processNo }));
                                if (!string.IsNullOrEmpty(path))
                                {
                                    if (file.Length > 0)
                                    {
                                        string originalName = file.FileName;
                                        string newName = code + "-" + _currDate.Now.Ticks.ToString() + "." + ext;
                                        var filePath = Path.Combine(path, newName);
                                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                                        {
                                            await file.CopyToAsync(fileStream);
                                        }

                                        await Mediator.Send(GetCommand<CreateUploadTaskCommand, StatusModel>(new CreateUploadTaskCommand
                                        {
                                            formCode = code,
                                            fileName = newName,
                                            originalFileName = originalName,
                                            processNo = processNo
                                        }));
                                    }
                                }
                                else
                                {
                                    checkresult = "Server Path Not Found";
                                }
                            }
                            else
                            {
                                checkresult = "File Exceeds the Maximum Size (" + new ApplicationSettings(__configuration).UploadMaxFileSize.ToString() + "MB)";
                            }
                        }
                    }
                    else
                    {
                        checkresult = "Invalid File Type, Please Upload Excel/Text (XLSX/CSV/TXT) file";
                    }
                }
                else
                {
                    checkresult = "Invalid File Type, Please Upload Excel/Text (XLSX/CSV/TXT) file";
                }


                if (checkresult == "1")
                {
                    return Ok(new JsonResult(new
                    {
                        status = checkresult
                    }));
                }
                else
                {
                    return BadRequestRequest(new List<string>() { checkresult });
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

    }
}