﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ARRA.Common;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Shared.Models;
using ARRA.GLOBAL.App.Forms.Customs.Process.Queries;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;
using ARRA.GLOBAL.Domain.Enumerations;
using ARRA.GLOBAL.Filters;
using ARRA.GLOBAL.App.Forms.Customs.Process.Commands;
using ARRA.GLOBAL.App.Forms.Customs.ProcessMonitor.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Transform.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Transform.Queries;
using ARRA.GLOBAL.App.Forms.Customs.UnlockData.Queries;
using ARRA.GLOBAL.App.Forms.Customs.UnlockData.Commands;
using ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Queries;
using ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Commands;
using System.IO.Compression;
using ARRA.GLOBAL.App.Forms.Customs.DownloadTF.Models;
using ARRA.GLOBAL.App.Forms.Customs.Reconcilition.Commands;
using ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Queries;
using ARRA.GLOBAL.App.Forms.Customs.BIValidationTool.Commands;
using ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Commands;
using ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Models;
using ARRA.GLOBAL.App.Forms.Customs.DownloadEF.Queries;

namespace ARRA.GLOBAL.API.Controllers
{
    [RAuthorize]
    public class ProcessController : BaseController
    {
        private readonly IConfiguration __configuration;
        public ProcessController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region Dynamic Form
        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDetailGridQuery, FormListModel>(new GetFormDetailGridQuery { FormCode = code })));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormDataQuery, DataListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetDataDetail(string code, int uid, GetFormDataDetailQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDataDetailQuery, DataListModel>(cmd)));
        }
        [HttpGet("{code}")]
        public async Task<IActionResult> GetEditor(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormEditorQuery, EditorListModel>(new GetFormEditorQuery { FormCode = code })));
        }
        #endregion

        #region CRUD
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveFilter(string code, SaveFilterCommand cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetCommand<SaveFilterCommand, StatusModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> SaveData(string code, SaveDataCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action;
            jObject.TryGetValue("screenActionType", out action);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                return Ok(await Mediator.Send(GetCommand<SaveDataCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }

        }
        #endregion

        #region export
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTask(string code, CreateExportTaskCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileByIdQuery, FileModel>(new GetExportedFileByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region import
        [HttpPost("{code}")]
        public async Task<IActionResult> Upload(string code, Microsoft.AspNetCore.Http.IFormFile file)
        {
            if (AccessMatrix.AllowImport)
            {
                string checkresult = "1";
                if (file.ContentType == Utility.GetMimeTypes()[".xlsx"]) //allowed type
                {
                    int idx = file.FileName.LastIndexOf(".");
                    if (idx > 0)
                    {
                        string ext = file.FileName.Substring(idx + 1, file.FileName.Length - idx - 1).ToLower();
                        if (ext == "xlsx")
                        {
                            if (file.Length <= (new ApplicationSettings(__configuration).UploadMaxFileSize * (int)1048576)) //compare in bytes
                            {
                                ParamHeaderModel m = await Mediator.Send(GetQuery<GetSysParByModuleQuery, ParamHeaderModel>(new GetSysParByModuleQuery { formCode = code, parName = SystemParameterType.IMPORT_FILE_LOCATION.ToString() }));
                                if (m != null)
                                {
                                    var path = m.parValue;
                                    if (file.Length > 0)
                                    {
                                        string originalName = file.FileName;
                                        string newName = code + "-" + _currDate.Now.Ticks.ToString() + ".xlsx";
                                        var filePath = Path.Combine(path, newName);
                                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                                        {
                                            await file.CopyToAsync(fileStream);
                                        }

                                        await Mediator.Send(GetCommand<CreateImportTaskCommand, StatusModel>(new CreateImportTaskCommand { formCode = code, fileName = newName, originalFileName = originalName }));
                                    }
                                }
                                else
                                {
                                    checkresult = "Server Path Not Found";
                                }
                            }
                            else
                            {
                                checkresult = "File Exceeds the Maximum Size (" + new ApplicationSettings(__configuration).UploadMaxFileSize.ToString() + "MB)";
                            }
                        }
                    }
                    else
                    {
                        checkresult = "Invalid File Type, Please Upload Excel (XLSX) file";
                    }
                }
                else
                {
                    checkresult = "Invalid File Type, Please Upload Excel (XLSX) file";
                }


                if (checkresult == "1")
                {
                    return Ok(new JsonResult(new
                    {
                        status = checkresult
                    }));
                }
                else
                {
                    return BadRequestRequest(new List<string>() { checkresult });
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion
        #endregion

        #region Custom Process Data Source
        [HttpPost("{code}")]
        public async Task<IActionResult> GetProcessSelectAll(string code, GetProcessDsSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessDsSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetProcessDepend(string code, GetProcessDsDependQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessDsDependQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetProcessDsData(string code, GetProcessDsDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessDsDataQuery, DataListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetProcessDsDataDetail(string code,long uid, GetProcessDsDataDetailQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetProcessDsDataDetailQuery, DataListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTaskDs(string code, CreateExportTaskDsCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskDsCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> RunProcess(string code, RunProcessDsCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<RunProcessDsCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region ProcessMonitor
        [HttpPost("{code}")]
        public async Task<IActionResult> GetCancelSelectAll(string code, GetCancelProcessSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetCancelProcessSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> RunCancel(string code, CancelProcessCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CancelProcessCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region Custom Transform
        [HttpPost("{code}")]
        public async Task<IActionResult> GetTransformSelectAll(string code, GetProcessTransformSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessTransformSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetTransformDepend(string code, GetProcessTransformDependQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessTransformDependQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetTransformData(string code, GetProcessTransformDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessTransformDataQuery, DataListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetTransformDataDetail(string code, long uid, GetProcessTransformDataDetailQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetProcessTransformDataDetailQuery, DataListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTaskTransform(string code, CreateExportTaskTransformCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskTransformCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> RunTransform(string code, RunProcessTransformCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<RunProcessTransformCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region Custom Unlock
        [HttpPost("{code}")]
        public async Task<IActionResult> GetUnlockSelectAll(string code, GetUnlockSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetUnlockSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> UnlockData(string code, UnlockDataCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<UnlockDataCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region Custom Download TF
        [HttpPost("{code}")]
        public async Task<IActionResult> GetDownloadSelectAll(string code, GetDownloadTFSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetDownloadTFSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> DownloadFile( string code,[FromBody] DownloadTFCommand cmd)
        {
            try
            {
                cmd.formCode = code;
                FileListModel file = await Mediator.Send(GetCommand<DownloadTFCommand, FileListModel>(cmd));
                using (var ms = new MemoryStream())
                {
                    using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        foreach (var f in file.files)
                        {
                            var entry = zipArchive.CreateEntry(f);

                            using (var fileStream = new FileStream(file.filePath + f, FileMode.Open))
                            using (var entryStream = entry.Open())
                            {
                                fileStream.CopyTo(entryStream);
                            }
                        }
                    }
                    ms.Position = 0;
                    return File(ms.ToArray(), "application/zip");
                }
            }
            catch(Exception ex)
            {
                return BadRequestRequest(new List<string> { ErrorMessages.ZipFileFailed });
            }
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> DownloadExportedFile(string code, [FromBody] DownloadEFCommand cmd)
        {
            try
            {
                cmd.formCode = code;
                //FileExportedListModel file = await Mediator.Send(GetCommand<DownloadEFCommand, FileExportedListModel>(cmd));
                ListFileExportedModel file = await Mediator.Send(GetCommand<DownloadEFCommand, ListFileExportedModel>(cmd));
                using (var ms = new MemoryStream())
                {
                    using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        foreach (var f in file.files)
                        {
                            var entry = zipArchive.CreateEntry(f.file);

                            using (var fileStream = new FileStream(f.filePath + f.file, FileMode.Open))
                            using (var entryStream = entry.Open())
                            {
                                fileStream.CopyTo(entryStream);
                            }
                        }
                    }
                    ms.Position = 0;
                    return File(ms.ToArray(), "application/zip");
                }
            }
            catch (Exception ex)
            {
                return BadRequestRequest(new List<string> { ErrorMessages.ZipFileFailed });
            }
        }

        #endregion

        #region Custom Reconciliation
        [HttpPost("{code}")]
        public async Task<IActionResult> RunRecon(string code, ReconCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<ReconCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region Custom BI Validation Tool
        [HttpPost("{code}")]
        public async Task<IActionResult> GetBIVALSelectAll(string code, GetProcessBIVALSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessBIVALSelectAllQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetBIVALDepend(string code, GetProcessBIVALDependQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessBIVALDependQuery, SelectionListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetBIVALData(string code, GetProcessBIVALDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetProcessBIVALDataQuery, DataListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetBIVALDataDetail(string code, long uid, GetProcessBIVALDataDetailQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetProcessBIVALDataDetailQuery, DataListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTaskBIVAL(string code, CreateExportTaskBIVALCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskBIVALCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> RunBIVAL(string code, RunProcessBIVALCommand cmd)
        {
            if (AccessMatrix.AllowInsert)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<RunProcessBIVALCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion

        #region Custom Download EF
        [HttpPost("{code}")]
        public async Task<IActionResult> GetDownloadEFSelectAll(string code, GetDownloadEFSelectAllQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetDownloadEFSelectAllQuery, SelectionListModel>(cmd)));
        }

        #endregion
    }
}