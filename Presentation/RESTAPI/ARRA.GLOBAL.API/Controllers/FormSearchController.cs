﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.App.FormSummary.Queries;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize]
    public class FormSearchController : BaseController
    {
        private readonly IConfiguration __configuration;
        public FormSearchController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetForm(string code)
        {           
            return Ok(await Mediator.Send(GetQuery<GetFormSearchQuery, FormSearchModel>(new GetFormSearchQuery { formCode = code })));
        }

       #endregion

    }
}