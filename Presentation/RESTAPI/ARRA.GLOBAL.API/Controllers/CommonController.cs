﻿using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.Menus.Models;
using ARRA.GLOBAL.App.Menus.Queries;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;

namespace ARRA.GLOBAL.API.Controllers
{
    [Authorize]
    public class CommonController : BaseController
    {

        [HttpGet]
        public async Task<IActionResult> GetMenu()
        {
            return Ok(await Mediator.Send(GetQuery<GetMenuByUserQuery, MenuListModel>(new GetMenuByUserQuery { UserId = LoginId, MenuGroup = LoginRole })));
        }

        [HttpGet]
        public async Task<IActionResult> GetMenuSkote()
        {
            return Ok(await Mediator.Send(GetQuery<GetMenuSkoteByUserQuery, MenuListSkoteModel>(new GetMenuSkoteByUserQuery { UserId = LoginId, MenuGroup = LoginRole })));
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetLookup(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetLookupQuery, LookupListModel>(new GetLookupQuery { code = code })));
        }
        [HttpGet("{codelist}")]
        public async Task<IActionResult> GetLookups(string codelist)
        {
            return Ok(await Mediator.Send(GetQuery<GetLookupsQuery, LookupsListModel>(new GetLookupsQuery { codelist = codelist })));
        }
        [HttpGet("{code}/{map}")]
        public async Task<IActionResult> GetLookup(string code,string map)
        {
            return Ok(await Mediator.Send(GetQuery<GetNestedLookupQuery, LookupListModel>(new GetNestedLookupQuery { code = code, mapvalue = map })));
        }

        [HttpGet("{formcode}/{formfield}/{mapfield}/{mapvalue}")]
        public async Task<IActionResult> GetLookup(string formcode, string formfield, string mapfield, string mapvalue)
        {
            return Ok(await Mediator.Send(GetQuery<GetDynamicNestedLookupQuery, LookupListModel>(new GetDynamicNestedLookupQuery { formCode = formcode, formField = formfield, mapField = mapfield, mapValue = mapvalue })));
        }

        [HttpGet("{mapcode}/{val}")]
        public async Task<IActionResult> GetDefaultNested(string mapcode, string val)
        {
            return Ok(await Mediator.Send(GetQuery<GetDefaultNestedQuery, DefaultNestedModel>(new GetDefaultNestedQuery { customrSrc = string.Empty, mapCode = mapcode, val = val })));
        }

        [HttpGet("{customsrc}/{val}")]
        public async Task<IActionResult> GetDefaultNestedSrc(string customsrc, string val)
        {
            return Ok(await Mediator.Send(GetQuery<GetDefaultNestedQuery, DefaultNestedModel>(new GetDefaultNestedQuery { customrSrc = customsrc, mapCode = string.Empty, val = val })));
        }

        [HttpGet("{formcode}/{period}")]
        public async Task<IActionResult> GetFormAuthList(string formcode,string period)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormAuthListQuery, FormAuthListModel>(new GetFormAuthListQuery { periodType=period, formCode=formcode })));
        }

        [HttpGet("{group}/{url}")]
        public async Task<IActionResult> GetMenuAccessByUrl(string group, string url)
        {
            return Ok(await Mediator.Send(GetQuery<GetMenuAccessByUrlQuery, MenuAccessModel>(new GetMenuAccessByUrlQuery { Group = group, Url = url })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetSysParDetail(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetSysParDtlByCodeQuery, ParamDetailListModel>(new GetSysParDtlByCodeQuery { code = code })));
        }
    }
}