﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ARRA.Common;
//using ARRA.GLOBAL.App.GenerateTF.Commands;
//using ARRA.GLOBAL.App.GenerateTF.Models;
using ARRA.GLOBAL.App.IssueLists.Queries;
using ARRA.GLOBAL.App.LogonActivites.Models;
using ARRA.GLOBAL.App.LogonActivites.Queries;
using ARRA.GLOBAL.App.Tasks.Models;
using ARRA.GLOBAL.App.Tasks.Queries;
//using RR.Application.GenerateTF.Queries;

namespace ARRA.GLOBAL.API.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {

        [HttpGet]
        public async Task<IActionResult> GetLastLogon()
        {
            return Ok(await Mediator.Send(GetQuery<GetLastLogonActivityQuery, LogonListActivityModel>(null)));
        }

        [HttpGet]
        public async Task<IActionResult> GetTotalIssueOpen()
        {
            return Ok(await Mediator.Send(GetQuery<GetTotalIssueQuery, ARRA.GLOBAL.App.IssueLists.Models.TotalItemModel>(null)));
        }
        [HttpGet]
        public async Task<IActionResult> GetTotalRejected()
        {
            return Ok(await Mediator.Send(GetQuery<GetTotalRejectedQuery, TotalItemModel>(null)));
        }

        [HttpGet]
        public async Task<IActionResult> GetTotalApproval()
        {
            return Ok(await Mediator.Send(GetQuery<GetTotalApprovalQuery, TotalItemModel>(null)));
        }

        [HttpGet]
        public async Task<IActionResult> GetTotalRevise()
        {
            return Ok(await Mediator.Send(GetQuery<GetTotalReviseQuery, TotalItemModel>(null)));
        }

        //[HttpGet("{module}/{period}")]
        //public async Task<IActionResult> GetReportProgress(string module, string period)
        //{
        //    GetReportProgressQuery cmd = new GetReportProgressQuery();
        //    cmd.module = module;
        //    cmd.periodType = period;
        //    return Ok(await Mediator.Send(GetQuery<GetReportProgressQuery, ReportProgressModel>(cmd)));
        //}

        //[HttpGet("{module}/{period}")]
        //public async Task<IActionResult> DownloadProgress(string module, string period)
        //{
        //    FileModel file = await Mediator.Send(GetCommand<ExportReportProgressCommand, FileModel>(new ExportReportProgressCommand { module = module, periodType = period }));
        //    if (file.fileName != "")
        //    {
        //        var memory = new MemoryStream();
        //        using (var stream = new FileStream(file.fullPath, FileMode.Open))
        //        {
        //            await stream.CopyToAsync(memory);
        //        }
        //        memory.Position = 0;
        //        return File(memory, Utility.GetContentType(file.fullPath), Path.GetFileName(file.fullPath));
        //    }
        //    else
        //    {
        //        return NoContent();
        //    }
        //}
    }
}