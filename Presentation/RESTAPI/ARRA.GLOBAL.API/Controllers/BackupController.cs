﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Backup.Commands;
using ARRA.GLOBAL.App.Backup.Models;
using ARRA.GLOBAL.App.Backup.Queries;
using ARRA.GLOBAL.Filters;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize("/backup")]
    public class BackupController : BaseController
    {
        private readonly IConfiguration __configuration;
        public BackupController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpPost]
        public async Task<IActionResult> GetData(GetBackupDataQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetBackupDataQuery, BackupListModel>(cmd)));
        }
        #endregion

        #region backup
        [HttpPost]
        public async Task<IActionResult> CreateBackupTask(CreateBackupTaskCommand cmd)
        {
            StatusModel m = await Mediator.Send(GetCommand<CreateBackupTaskCommand, StatusModel>(cmd));
            return Ok(m);
        }
        #endregion
    }
}