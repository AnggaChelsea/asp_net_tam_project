﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.FormSummaryCompare.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.FormSummaryCompare.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    [RAuthorize("/formsumcompare/rwa")]
    public class FormSumCompareController : BaseController
    {
        private readonly IConfiguration __configuration;
        public FormSumCompareController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportMaster(string code)
        {           
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportMaster, FormSumConfListModel>(new GetFormSumConfReportMaster { module = code })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormSumCompare cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormSumCompare, FormSumCompareModel>(cmd)));
        }

        [HttpPost("{code}/{fileName}")]
        public async Task<IActionResult> ExportReportCompareTask(string code, string fileName, GetFormSumCompare filter)
        {
            return Ok(await Mediator.Send(GetQuery<ExportTaskCompareQuery, StatusModel>(new ExportTaskCompareQuery() { ReportExportFileName = fileName, ReportCompareCode = code, FilterModel = filter })));
        }

        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> DownloadContentType(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileModuleByIdQuery, FileModel>(new GetExportedFileModuleByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    return Ok(Utility.GetContentType(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileModuleByIdQuery, FileModel>(new GetExportedFileModuleByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        #endregion
    }
}