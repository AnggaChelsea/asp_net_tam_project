﻿using System.Threading.Tasks;
using ARRA.GLOBAL.App.FormSummaryConf.Commands;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.FormSummaryConf.Commands;
using ARRA.GLOBAL.App.FormSummaryConf.Models;
using ARRA.GLOBAL.App.FormSummaryConf.Queries;
using ARRA.GLOBAL.Filters;


namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize]
    public class FormSumConfController : BaseController
    {
        private readonly IConfiguration __configuration;
        public FormSumConfController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfFormInfo, FormSumConfFormInfoModel>(new GetFormSumConfFormInfo { module = code })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportMaster(string code)
        {           
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportMaster, FormSumConfListModel>(new GetFormSumConfReportMaster { module = code })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportDetail(string code, GetFormSumConfReportDetail cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportDetail, FormSumConfListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportStaticCell(string code, GetFormSumConfReportStaticCellQuery cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportStaticCellQuery, FormSumConfReportStaticModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportFilter(string code, GetFormSumConfReportFilter cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportFilter, FormSumConfListModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetReportTableWeb(string code, GetFormSumConfReportFilter cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportFilter, FormSumConfListModel>(cmd)));
        }
        #endregion

        #region crud
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateReportMaster(string code, IFormCollection data, IFormFile file)
        {
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportMaster, StatusModel>(new SaveFormSumConfReportMaster()
            {
                module = code,
                reportCode = data["reportCode"],
                reportDescription = data["reportDescription"],
                reportSheet = data["reportSheet"],
                formCodeDtl = data["formCodeDtl"],
                uniqueId = long.Parse(data["uniqueId"]),
                reportTemplate = data["file"]
            }))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportMaster(string code, IFormCollection data, IFormFile file)
        {
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportMaster, StatusModel>(new SaveFormSumConfReportMaster()
            {
                module = code,
                reportCode = data["reportCode"],
                reportDescription = data["reportDescription"],
                reportSheet = data["reportSheet"],
                formCodeDtl = data["formCodeDtl"],
                uniqueId = long.Parse(data["uniqueId"]),
                reportTemplate = data["file"]
            }))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportRow(string code, SaveFormSumConfReportRow cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportRow, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateReportRow(string code, SaveFormSumConfReportRow cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportRow, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateReportStaticCell(string code, SaveFormSumConfReportStaticCellCommand cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportStaticCellCommand, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportStaticCell(string code, SaveFormSumConfReportStaticCellCommand cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportStaticCellCommand, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> DeleteReportStaticCell(string code, RemoveFormSumConfReportStaticCellCommand cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<RemoveFormSumConfReportStaticCellCommand, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportColumn(string code, SaveFormSumConfReportColumn cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportColumn, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateReportColumn(string code, SaveFormSumConfReportColumn cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportColumn, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportRowColumn(string code, SaveFormSumConfReportRowColumn cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportRowColumn, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateReportRowColumn(string code, SaveFormSumConfReportRowColumn cmd)
        {
            cmd.module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportRowColumn, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportFilter(string code, SaveFormSumConfReportFilter cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportFilter, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> RemoveReportMaster(string code, IFormCollection data, IFormFile file)
        {
            return Ok(await Mediator.Send(GetCommand<RemoveFormSumConfReportMaster, StatusModel>(new RemoveFormSumConfReportMaster()
            {
                module = code,
                reportCode = data["reportCode"],
            }))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> RemoveReportFilter(string code, RemoveFormSumConfReportFilter cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<RemoveFormSumConfReportFilter, StatusModel>(cmd))); ;
        }
        #endregion

        #region report table
        [HttpPost("{code}/{reportCd}")]
        public async Task<IActionResult> GetReportTableWeb(string code, string reportCd)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormSumConfReportTableWeb, FormSumConfReportTableModel>(new GetFormSumConfReportTableWeb { module = code, reportCd = reportCd })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportTableWeb(string code, SaveFormSumConfReportTableWeb cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportTableWeb, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> RemoveReportTableWeb(string code, RemoveFormSumConfReportTableWeb cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<RemoveFormSumConfReportTableWeb, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> UpdateReportTableDataWeb(string code, SaveFormSumConfReportTableDataWeb cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<SaveFormSumConfReportTableDataWeb, StatusModel>(cmd))); ;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> RemoveReportTableDataWeb(string code, RemoveFormSumConfReportTableDataWeb cmd)
        {
            cmd.Module = code;
            return Ok(await Mediator.Send(GetCommand<RemoveFormSumConfReportTableDataWeb, StatusModel>(cmd))); ;
        }
        #endregion
    }
}