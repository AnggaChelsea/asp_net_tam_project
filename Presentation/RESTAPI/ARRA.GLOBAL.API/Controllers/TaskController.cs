﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ARRA.Common;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Task.Commands;
using ARRA.GLOBAL.App.Forms.Customs.Task.Queries;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    public class TaskController : BaseController
    {
        [RAuthorize]
        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDetailGridQuery, FormListModel>(new GetFormDetailGridQuery { FormCode = code })));
        }
        [RAuthorize]
        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormDataQuery, DataListModel>(cmd)));
        }
        

        [RAuthorize]
        [HttpGet("{code}/{uid}")]
        public async Task<IActionResult> GetDataDetail(string code, int uid)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDataDetailQuery, DataListModel>(new GetFormDataDetailQuery { formCode = code, uid = uid })));
        }
        
        [RAuthorize]
        [HttpGet("{code}")]
        public async Task<IActionResult> GetEditor(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormEditorQuery, EditorListModel>(new GetFormEditorQuery { FormCode = code })));
        }
        [Authorize]
        [HttpGet("{appvid}")]
        public async Task<IActionResult> GetFileInfo(long appvid)
        {
            return Ok(await Mediator.Send(GetQuery<GetUploadFileInfoQuery, App.Forms.Customs.Task.Models.FileInfoModel>(new GetUploadFileInfoQuery { id = appvid })));
        }
        #endregion

        #region CRUD
        [RAuthorize]
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveFilter(string code, SaveFilterCommand cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetCommand<SaveFilterCommand, StatusModel>(cmd)));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Approval(ApprovalCommand cmd)
        {
            StatusModel sts = await Mediator.Send(GetCommand<ApprovalCommand, StatusModel>(cmd));
            if (sts.status == CommandStatus.Success.ToString())
            {
                return Ok(sts);
            }
            else
            {
                return BadRequestRequest(new List<string>() { sts.message });
            }
        }

        #endregion

        #region export
        [RAuthorize]
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTask(string code, CreateExportTaskCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [Authorize]
        [HttpPost("{appvid}")]
        public async Task<IActionResult> CreateExportCompareTask(string appvid, CreateExportCompareTaskCommand cmd)
        {
            cmd.approvalId = appvid;
            StatusModel m = await Mediator.Send(GetCommand<CreateExportCompareTaskCommand, StatusModel>(cmd));
            return Ok(m);
        }

        [Authorize]
        [HttpGet("{appvid}")]
        public async Task<IActionResult> Download(long appvid)
        {
            FileModel file = await Mediator.Send(GetQuery<GetExportedFileByAppvIdQuery, FileModel>(new GetExportedFileByAppvIdQuery { id = appvid }));
            if (file.fileName != "")
            {
                string path = file.path + file.fileName;

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }

                memory.Position = 0;
                return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
            }
            else if (!string.IsNullOrEmpty(file.error))
            {
                return BadRequestRequest(new List<string> { file.error });
            }
            else
            {
                return NoContent();
            }
        }
        [Authorize]
        [HttpGet("{appvid}")]
        public async Task<IActionResult> DownloadUpload(long appvid)
        {
            FileModel file = await Mediator.Send(GetQuery<GetUploadFileByAppvIdQuery, FileModel>(new GetUploadFileByAppvIdQuery { id = appvid }));
            if (file.fileName != "")
            {
                string path = file.path + file.fileName;

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }

                memory.Position = 0;
                return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
            }
            else
            {
                return NoContent();
            }
        }

        [Authorize]
        [HttpGet("{id}/{module}")]
        public async Task<IActionResult> DownloadAuditImportFile(long id, string module)
        {
            //if (AccessMatrix.AllowExport)
            //{
            FileModel file = await Mediator.Send(GetQuery<GetAuditImportFileByIdQuery, FileModel>(new GetAuditImportFileByIdQuery { id = id, module = module }));
            if (file.fileName != "")
            {
                string path = file.path + file.fileName;

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }

                memory.Position = 0;
                return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
            }
            else if (!string.IsNullOrEmpty(file.error))
            {
                return BadRequestRequest(new List<string> { file.error });
            }
            else
            {
                return NoContent();
            }
            //}
            //else
            //{
            //    return UnAuthorizedRequest();
            //}
        }

        [Authorize]
        [HttpGet("{id}/{module}")]
        public async Task<IActionResult> DownloadAuditImportFileName(long id, string module)
        {
            return Ok(await Mediator.Send(GetQuery<GetAuditImportFileByIdQuery, FileModel>(new GetAuditImportFileByIdQuery { id = id, module = module })));
        }

        [RAuthorize]
        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileByIdQuery, FileModel>(new GetExportedFileByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

    }
}