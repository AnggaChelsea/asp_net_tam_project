﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ROBIN.GLOBAL.Filters;
using ROBIN.Common;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.ManualUpload.Commands;
using ROBIN.GLOBAL.App.Parameter.Models;
using ROBIN.GLOBAL.App.Parameter.Queries;
using System.IO;
using ROBIN.GLOBAL.App.Forms.Queries;
using ROBIN.GLOBAL.App.Forms.Commands;
using Newtonsoft.Json.Linq;
using ROBIN.GLOBAL.Domain.Enumerations;
using System.IO.Compression;

namespace ROBIN.GLOBAL.API.Controllers
{
    [RAuthorize("/errorantasena")]
    public class ErrorAntasenaController : BaseController
    {
        private readonly IConfiguration __configuration;
        public ErrorAntasenaController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(Microsoft.AspNetCore.Http.IFormFile file)
        {
            if (AccessMatrix.AllowImport)
            {
                string checkresult = "1";
                if (file.ContentType == Utility.GetMimeTypes()[".txt"]
                    || file.ContentType == Utility.GetMimeTypes()[".csv"]
                    //|| file.ContentType == Utility.GetMimeTypes()[".zip"]
                    || file.ContentType == Utility.GetMimeTypes()[".csv1"]) //allowed type
                {
                    int idx = file.FileName.LastIndexOf(".");
                    if (idx > 0)
                    {
                        string ext = file.FileName.Substring(idx + 1, file.FileName.Length - idx - 1).ToLower();
                        if (ext == "txt" || ext == "csv")
                        {
                            if (file.Length <= (new ApplicationSettings(__configuration).UploadMaxFileSize * (int)1048576)) //compare in bytes
                            {
                                ParamHeaderModel mdl = await Mediator.Send(GetQuery<GetSysParByCodeQuery, ParamHeaderModel>(new GetSysParByCodeQuery { code = "GB036" }));
                                string path = mdl.parValue;
                                if (!string.IsNullOrEmpty(path))
                                {
                                    if (file.Length > 0)
                                    {
                                        string originalName = file.FileName;
                                        string newName = "ErrorAntasena-" + _currDate.Now.Ticks.ToString() + "." + ext;
                                        var filePath = Path.Combine(path, newName);
                                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                                        {
                                            await file.CopyToAsync(fileStream);
                                        }

                                        await Mediator.Send(GetCommand<CreateUploadErrorAntasenaTaskCommand, StatusModel>(new CreateUploadErrorAntasenaTaskCommand
                                        {
                                            formCode = "F_ANERR_BS",
                                            fileName = newName,
                                            originalFileName = originalName,
                                            //processNo = processNo
                                            //reportDate = reportDate
                                        }));
                                    }
                                }
                                else
                                {
                                    checkresult = "Server Path Not Found";
                                }
                            }
                            else
                            {
                                checkresult = "File Exceeds the Maximum Size (" + new ApplicationSettings(__configuration).UploadMaxFileSize.ToString() + "MB)";
                            }
                        }
                        else if (ext == "zip")
                        {
                            if (file.Length <= (new ApplicationSettings(__configuration).UploadMaxFileSize * (int)1048576)) //compare in bytes
                            {
                                ParamHeaderModel mdl = await Mediator.Send(GetQuery<GetSysParByCodeQuery, ParamHeaderModel>(new GetSysParByCodeQuery { code = "GB036" }));
                                string path = mdl.parValue;
                                if (!string.IsNullOrEmpty(path))
                                {
                                    if (file.Length > 0)
                                    {
                                        string originalName = file.FileName;
                                        string newName = "ErrorAntasena-" + _currDate.Now.Ticks.ToString() + "." + ext;
                                        var filePath = Path.Combine(path, newName);
                                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                                        {
                                            await file.CopyToAsync(fileStream);
                                        }

                                        //string zipErr = Path.Combine(filePath, "ZIP" + _currDate.Now.Ticks.ToString());
                                        //ZipFile.ExtractToDirectory(filePath, zipErr);

                                        using (ZipArchive archive = ZipFile.OpenRead(filePath))
                                        {
                                            foreach (ZipArchiveEntry entry in archive.Entries)
                                            {
                                                int zipIdx = entry.FullName.LastIndexOf(".");
                                                string zipExt = entry.FullName.Substring(zipIdx + 1, entry.FullName.Length - zipIdx - 1).ToLower();
                                                if (zipExt == "txt" || zipExt == "csv")
                                                {
                                                    string zipNewName = "ZipErrorAntasena-" + _currDate.Now.Ticks.ToString() + "." + zipExt;
                                                    var zipFilePath = Path.Combine(path, zipNewName);

                                                    //// Gets the full path to ensure that relative segments are removed.
                                                    //string destinationPath = Path.GetFullPath(Path.Combine(filePath, entry.FullName));

                                                    //// Ordinal match is safest, case-sensitive volumes can be mounted within volumes that
                                                    //// are case-insensitive.
                                                    //if (destinationPath.StartsWith(filePath, StringComparison.Ordinal))
                                                    //    entry.ExtractToFile(destinationPath);

                                                    entry.ExtractToFile(zipFilePath);

                                                    await Mediator.Send(GetCommand<CreateUploadErrorAntasenaTaskCommand, StatusModel>(new CreateUploadErrorAntasenaTaskCommand
                                                    {
                                                        formCode = "F_ANERR_BS",
                                                        fileName = zipNewName,
                                                        originalFileName = entry.FullName,
                                                        //processNo = processNo
                                                        //reportDate = reportDate
                                                    }));

                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    checkresult = "Server Path Not Found";
                                }
                            }
                            else
                            {
                                checkresult = "File Exceeds the Maximum Size (" + new ApplicationSettings(__configuration).UploadMaxFileSize.ToString() + "MB)";
                            }
                        }
                    }
                    else
                    {
                        checkresult = "Invalid File Type, Please Upload Text/ZIP (ZIP/CSV/TXT) file";
                    }
                }
                else
                {
                    checkresult = "Invalid File Type, Please Upload Text/ZIP (ZIP/CSV/TXT) file";
                }


                if (checkresult == "1")
                {
                    return Ok(new JsonResult(new
                    {
                        status = checkresult
                    }));
                }
                else
                {
                    return BadRequestRequest(new List<string>() { checkresult });
                }

            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDetailGridQuery, App.Forms.Models.FormListModel>(new GetFormDetailGridQuery { FormCode = code })));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormDataQuery, App.Forms.Models.DataListModel>(cmd)));
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetEditor(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormEditorQuery, App.Forms.Models.EditorListModel>(new GetFormEditorQuery { FormCode = code })));
        }

        [HttpGet("{code}/{uid}")]
        public async Task<IActionResult> GetDataDetail(string code, int uid)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDataDetailQuery, App.Forms.Models.DataListModel>(new GetFormDataDetailQuery { formCode = code, uid = uid })));
        }

        #region CRUD
        [HttpPost("{code}")]
        public async Task<IActionResult> SaveFilter(string code, SaveFilterCommand cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetCommand<SaveFilterCommand, StatusModel>(cmd)));
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> SaveData(string code, SaveDataCommand cmd)
        {
            var jObject = ((Newtonsoft.Json.Linq.JObject)cmd.data);
            JToken action;
            jObject.TryGetValue("screenActionType", out action);
            jObject = null;
            bool process = false;
            if (Convert.ToString(action) == ScreenAction.ADD.ToString() && AccessMatrix.AllowInsert)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.EDIT.ToString() && AccessMatrix.AllowUpdate)
            {
                process = true;
            }
            if (Convert.ToString(action) == ScreenAction.REMOVE.ToString() && AccessMatrix.AllowDelete)
            {
                process = true;
            }

            if (process)
            {
                cmd.formCode = code;
                return Ok(await Mediator.Send(GetCommand<SaveDataCommand, StatusModel>(cmd)));
            }
            else
            {
                return UnAuthorizedRequest();
            }

        }
        #endregion

        #region export
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTask(string code, CreateExportTaskCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                StatusModel m = await Mediator.Send(GetCommand<CreateExportTaskCommand, StatusModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                App.Forms.Models.FileModel file = await Mediator.Send(GetQuery<GetExportedFileByIdQuery, App.Forms.Models.FileModel>(new GetExportedFileByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion
    }
}
