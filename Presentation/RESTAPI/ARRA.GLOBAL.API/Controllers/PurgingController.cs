﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Purging.Commands;
using ARRA.GLOBAL.App.Purging.Models;
using ARRA.GLOBAL.App.Purging.Queries;
using ARRA.GLOBAL.Filters;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.API.Controllers
{
    //[Authorize]
    [RAuthorize("/purging")]
    public class PurgingController : BaseController
    {
        private readonly IConfiguration __configuration;
        public PurgingController(IConfiguration configuration)
        {
            __configuration = configuration;
        }

        #region view
        [HttpPost]
        public async Task<IActionResult> GetData(GetPurgingDataQuery cmd)
        {
            return Ok(await Mediator.Send(GetQuery<GetPurgingDataQuery, PurgingListModel>(cmd)));
        }
        #endregion

        #region Purging
        [HttpPost]
        public async Task<IActionResult> CreatePurgingTask(CreatePurgingTaskCommand cmd)
        {
            StatusModel m = await Mediator.Send(GetCommand<CreatePurgingTaskCommand, StatusModel>(cmd));
            return Ok(m);
        }
        #endregion
    }
}