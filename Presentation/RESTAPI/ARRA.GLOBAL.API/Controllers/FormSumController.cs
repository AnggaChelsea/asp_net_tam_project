﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ARRA.Common;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Forms.Models;
using ARRA.GLOBAL.App.Forms.Queries;
using ARRA.GLOBAL.App.FormSummary.Commands;
using ARRA.GLOBAL.App.FormSummary.Models;
using ARRA.GLOBAL.App.FormSummary.Queries;
using ARRA.GLOBAL.Filters;

namespace ARRA.GLOBAL.API.Controllers
{
    [RAuthorize]
    public class FormSumController : BaseController
    {
        private readonly IConfiguration __configuration;
        public FormSumController(IConfiguration configuration)
        {
            __configuration = configuration;
        }
        
        #region view
        [HttpGet("{code}")]
        public async Task<IActionResult> GetFormInfo(string code)
        {
            return Ok(await Mediator.Send(GetQuery<GetFormDetailGridQuery, FormListModel>(new GetFormDetailGridQuery { FormCode = code })));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetData(string code, GetFormSumDataQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumDataQuery, App.FormSummary.Models.FormSumDataListModel>(cmd)));
        }
        #endregion

        #region export & download (template / detail)
        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportTask(string code, CreateExportTaskFormSummaryCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                ExportProgressModel m = await Mediator.Send(GetCommand<CreateExportTaskFormSummaryCommand, ExportProgressModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> CreateExportDetailTask(string code, CreateExportTaskFormSummaryDtlCommand cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                ExportProgressModel m = await Mediator.Send(GetCommand<CreateExportTaskFormSummaryDtlCommand, ExportProgressModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> GetExportProgressTask(string code, GetFormSumExportProgressQuery cmd)
        {
            if (AccessMatrix.AllowExport)
            {
                cmd.formCode = code;
                FormSumExportProgressModel m = await Mediator.Send(GetQuery<GetFormSumExportProgressQuery, FormSumExportProgressModel>(cmd));
                return Ok(m);
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }

        [HttpGet("{code}/{id}")]
        public async Task<IActionResult> Download(string code, long id)
        {
            if (AccessMatrix.AllowExport)
            {
                FileModel file = await Mediator.Send(GetQuery<GetExportedFileTemplateByIdQuery, FileModel>(new GetExportedFileTemplateByIdQuery { formCode = code, id = id }));
                if (file.fileName != "")
                {
                    string path = file.path + file.fileName;

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }

                    memory.Position = 0;
                    return File(memory, Utility.GetContentType(path), Path.GetFileName(path));
                }
                else if (!string.IsNullOrEmpty(file.error))
                {
                    return BadRequestRequest(new List<string> { file.error });
                }
                else
                {
                    return NoContent();
                }
            }
            else
            {
                return UnAuthorizedRequest();
            }
        }
        #endregion

        #region view detail
        [HttpPost("{code}")]
        public async Task<IActionResult> GetFormDetailInfo(string code, GetFormSumDtlInfoQuery cmd)
        {
            cmd.FormCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumDtlInfoQuery, FormSumDataListModel>(cmd)));
        }
        [HttpPost("{code}")]
        public async Task<IActionResult> GetDataDetail(string code, GetFormSumDataDtlQuery cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetQuery<GetFormSumDataDtlQuery, App.FormSummary.Models.FormSumDataListModel>(cmd)));
        }
        #endregion

        #region crud
        [HttpPost("{code}")]
        public async Task<IActionResult> Recalculate(string code, RecalculateFormSummaryCommand cmd)
        {
            cmd.formCode = code;
            return Ok(await Mediator.Send(GetCommand<RecalculateFormSummaryCommand, StatusModel>(cmd))); ;
        }
        #endregion
    }
}