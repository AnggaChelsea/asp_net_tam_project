﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Authentications.Commands;
using ARRA.GLOBAL.App.Authentications.Models;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;

namespace ARRA.GLOBAL.API.Controllers
{
    [ApiController]
    public class AuthController : BaseController
    {

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginCommand command)
        {
            command.Browser = HttpContext.Request.Headers["User-Agent"].ToString();
            LoginModel mLogin = await Mediator.Send(GetCommand<LoginCommand, LoginModel>(command));
            if (!string.IsNullOrEmpty(mLogin.token))
            {
                return Ok(mLogin);
            }
            else
            {
                return BadRequest(new { errors = mLogin.sts });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand cmd)
        {
            StatusModel mLogin = await Mediator.Send(GetCommand<ChangePasswordCommand, StatusModel>(cmd));
            if (mLogin.status == CommandStatus.Success.ToString())
            {
                return Ok(mLogin);
            }
            else
            {
                return BadRequestRequest(new List<string>() { mLogin.message });
            }
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            return Ok(await Mediator.Send(GetCommand<LogoutCommand, StatusModel>(null)));
        }

        [HttpGet]
        public async Task<IActionResult> GetIdleSession()
        {
            ParamHeaderModel mdl = await Mediator.Send(GetQuery<GetSysParByCodeQuery, ParamHeaderModel>(new GetSysParByCodeQuery { code = "GB300" }));
            return Ok(mdl);
        }
    }
}