﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using System.Linq;
using System.Net;
using ARRA.Common.Model;
using ARRA.Common;
using System.Collections.Generic;
using System.Net.Http;

using System.Web;

namespace ARRA.GLOBAL.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public abstract class BaseController : Controller
    {
        private IMediator _mediator;
        protected IDateTime _currDate = new ARRA.Infrastructure.MachineDateTime();
        protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());

        protected CommandsModel<TReq, TRes> GetCommand<TReq, TRes>(TReq command) where TReq : class
            where TRes : class
        {
            CommandsModel<TReq, TRes> m = new CommandsModel<TReq, TRes>();
            m.UserIdentity = new UserIdentityModel
            {
                UserId = LoginId,
                GroupId = LoginRole,
                Host = LoginHost,
                BranchGroup = LoginBranchGroup,
                Modules = LoginUserModule
            };
            m.AccessMatrix = this.AccessMatrix;
            m.CurrentDateTime = _currDate.Now;
            m.CommandModel = command;

            return m;
        }

        protected QueriesModel<TReq, TRes> GetQuery<TReq, TRes>(TReq query) where TReq : class
            where TRes : class
        {
            QueriesModel<TReq, TRes> m = new QueriesModel<TReq, TRes>();
            m.UserIdentity = new UserIdentityModel
            {
                UserId = LoginId,
                GroupId = LoginRole,
                Host = LoginHost,
                BranchGroup = LoginBranchGroup,
                Modules = LoginUserModule
            };
            m.AccessMatrix = this.AccessMatrix;
            m.CurrentDateTime = _currDate.Now;
            m.QueryModel = query;
            return m;
        }

        protected AccessMatrixModel AccessMatrix
        {
            get { return (AccessMatrixModel)HttpContext.Items["screeninfo"]; }
        }

        protected IActionResult UnAuthorizedRequest()
        {
            HttpContext.Response.ContentType = "application/json";
            HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return new JsonResult(new
            {
                errors = "Unauthorized to Do This Action"
            }
            );
        }

        protected IActionResult BadRequestRequest(List<string> errors)
        {
            HttpContext.Response.ContentType = "application/json";
            HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return new JsonResult(new
            {
                errors = errors
            }
            );
        }

        protected string LoginId
        {
            get
            {
                //return "admin";
                Claim authInfo = ((ClaimsIdentity)User.Identity).Claims.Where(x => x.Type == "UserId").FirstOrDefault();
                if (authInfo != null)
                {
                    return authInfo.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        protected string LoginRole
        {
            get
            {
                Claim authInfo = ((ClaimsIdentity)User.Identity).Claims.Where(x => x.Type == "GroupAccess").FirstOrDefault();
                if (authInfo != null)
                {
                    return authInfo.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        
        protected string LoginUserModule
        {
            get
            {
                Claim authInfo = ((ClaimsIdentity)User.Identity).Claims.Where(x => x.Type == "UserModule").FirstOrDefault();
                if (authInfo != null)
                {
                    return authInfo.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        protected string LoginBranchGroup
        {
            get
            {
                Claim authInfo = ((ClaimsIdentity)User.Identity).Claims.Where(x => x.Type == "BranchGroup").FirstOrDefault();
                if (authInfo != null)
                {
                    return authInfo.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        protected string LoginHost
        {
            get
            {
                string str = HttpContext.Connection.RemoteIpAddress.ToString();

                return str;

                //string ip = System.Net.Dns.GetHostEntry
                //    (System.Net.Dns.GetHostName()).AddressList.GetValue(0).ToString();

                //var hostname = Request.us;
                //IPAddress ipAddress = IPAddress.Parse(hostname);
                //IPHostEntry ipHostEntry = Dns.GetHostEntry(ipAddress);

                //return ipHostEntry.AddressList.ToString();
                ////return Dns.GetHostName();
            }
        }


    }
}