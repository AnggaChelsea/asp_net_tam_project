﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;

using System;
using System.Text;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Infrastructure;
using ARRA.Persistence;
using Microsoft.EntityFrameworkCore;
using MediatR;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using ARRA.GLOBAL.App.Authentications.Commands;
using ARRA.Infrastructure;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Hosting;
using static ARRA.Common.Decrypt;
using ARRA.GLOBAL.Filters;
using Microsoft.OpenApi.Models;

namespace ARRA.GLOBAL.API
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            /**
             * ===================================================================================
             * START ADD SERVICES ARRA
             * ===================================================================================
             **/
            builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            builder.Services.AddMediatR(typeof(ARRA.GLOBAL.App.Authentications.Models.LoginModel).GetTypeInfo().Assembly);
            builder.Services.AddMediatR(typeof(ARRA.Common.Model.StatusModel).GetTypeInfo().Assembly);

            builder.Services.AddMediatR(typeof(ARRA.GLOBAL.App.Forms.Commands.ExportDataCommand).GetTypeInfo().Assembly);
            builder.Services.AddMediatR(typeof(ARRA.GLOBAL.App.Forms.Commands.ExportDataCommandHandler).GetTypeInfo().Assembly);
            builder.Services.AddMediatR(typeof(ARRA.Common.Model.CommandsModel<ARRA.GLOBAL.App.Forms.Commands.ExportDataCommand, ARRA.Common.Model.StatusModel>).GetTypeInfo().Assembly);

            builder.Services.AddTransient<ARRA.GLOBAL.App.Interfaces.IAuthenticationService, AuthenticationService>();
            builder.Services.AddTransient<ARRA.Common.IDateTime, ARRA.Infrastructure.MachineDateTime>();
            builder.Services.AddTransient<ARRA.GLOBAL.App.Interfaces.IExcelConnector, ARRA.Infrastructure.ExcelConnector>();
            builder.Services.AddTransient<ARRA.GLOBAL.App.Interfaces.ITextFileConnector, ARRA.Infrastructure.TextFileConnector>();
            builder.Services.AddTransient<ARRA.GLOBAL.App.Interfaces.IWebServiceConnector, ARRA.Infrastructure.WebServiceConnector>();

            // Add DbContext using SQL Server Provider
            builder.Services.AddDbContext<GLOBALDbContext>(options =>
                options.UseSqlServer(DecryptText(builder.Configuration.GetConnectionString("GLOBAL_ConStr"), builder.Configuration)));

            builder.Services
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<LoginCommandValidator>());

            //Jwt Authentication
            builder.Services.Configure<ApplicationSettings>(builder.Configuration.GetSection("ApplicationSettings"));

            var key = Encoding.UTF8.GetBytes(builder.Configuration["ApplicationSettings:JWT_Secret"].ToString());
            var authSchema = (builder.Configuration["ApplicationSettings:AuthenticationSchema"].ToString());

            builder.Services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero

                };
            });

            builder.Services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("en-US");
            });

            /**
             * ===================================================================================
             * END ADD SERVICES ARRA
             * ===================================================================================
             **/

            builder.Services.AddControllers(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute))).AddNewtonsoftJson();

            builder.Services.AddEndpointsApiExplorer();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RSwagger", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme (Example: 'Bearer 12345abcdef')",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RSwagger v1"));
            }

            string[] allowUrl = builder.Configuration["ApplicationSettings:ALLOW_CLIENT_URL"].ToString().Split(',');
            app.UseCors(builder =>
            builder.WithOrigins(allowUrl)
                .AllowAnyHeader()
                .AllowAnyOrigin()
                .AllowAnyMethod()
            );

            //app.UseHttpsRedirection();
            app.UseAuthentication();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
