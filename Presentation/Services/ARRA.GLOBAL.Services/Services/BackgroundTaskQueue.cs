//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using System;
//using System.Collections.Concurrent;
//using System.Threading;
//using System.Threading.Tasks;
//using ARRA.Common;

//namespace ARRA.GLOBAL.Services.Services
//{
//    #region snippet1
//    public interface IBackgroundTaskQueue
//    {
//        void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem);

//        Task<Func<CancellationToken, Task>> DequeueAsync(
//            CancellationToken cancellationToken);
//        Int32 GetItemCount();
//    }

//    public class BackgroundTaskQueue : IBackgroundTaskQueue
//    {
//        private IDateTime _currDate = new ARRA.Infrastructure.MachineDateTime();
//        private ConcurrentQueue<Func<CancellationToken, Task>> _workItems =
//            new ConcurrentQueue<Func<CancellationToken, Task>>();
//        private SemaphoreSlim _signal;// = new SemaphoreSlim(0,5);
//        private readonly ILogger _logger;

//        private readonly IConfiguration _configuration;

//        public BackgroundTaskQueue(IConfiguration configuration, ILogger<ROBINService> logger)
//        {
//            _logger = logger;
//            _configuration = configuration;
//            _signal = new SemaphoreSlim(0);
//        }

//        public void QueueBackgroundWorkItem(
//            Func<CancellationToken, Task> workItem)
//        {
//            if (workItem == null)
//            {
//                throw new ArgumentNullException(nameof(workItem));
//            }
//            _workItems.Enqueue(workItem);

//            _signal.Release();
//        }

//        public Int32 GetItemCount()
//        {
//            return _signal.CurrentCount;
//        }

//        public async Task<Func<CancellationToken, Task>> DequeueAsync(
//            CancellationToken cancellationToken)
//        {
//            await _signal.WaitAsync(cancellationToken);
//            _workItems.TryDequeue(out var workItem);
//            return workItem;
//        }
//    }

//    #endregion
//}
