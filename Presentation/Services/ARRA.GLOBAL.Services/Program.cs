﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ARRA.GLOBAL.Services.Services;
using MediatR;
using System.Reflection;
using ARRA.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq;
using ARRA.Infrastructure;
using Microsoft.Extensions.Logging;
using System.Globalization;
using static ARRA.Common.Decrypt;

namespace ARRA.GLOBAL.Services
{
    public class Program
    {
        private static async Task Main(string[] args)
        {
            var isService = !(Debugger.IsAttached || args.Contains("--console"));
            //var isService = false;

            CultureInfo.CurrentCulture = new CultureInfo("en-US");

            var builder = new HostBuilder()
                .ConfigureLogging((hostContext, config) =>
                {
                    //config.AddConsole();
                    //config.AddDebug();
                    //config.AddEventLog();
                })
            .ConfigureHostConfiguration(config =>
            {
                //config.AddEnvironmentVariables();
                //config.SetBasePath(Directory.GetCurrentDirectory());
                config.AddJsonFile("appsettings.json", optional: false);

            })
            .ConfigureAppConfiguration((hostContext, config) =>
            {
                //config.SetBasePath(Directory.GetCurrentDirectory());
                config.AddJsonFile("appsettings.json", optional: false);
                //config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                config.AddCommandLine(args);
            })
            .ConfigureServices((hostContext, services) =>
            {
                services.AddLogging();

                services.AddHostedService<ARRAService>();
                //services.AddHostedService<IDXService>();
                //services.AddHostedService<QueuedHostedService>();
                //services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();

                services.AddMediatR(typeof(ARRA.GLOBAL.App.Jobs.Queries.GetJobDtlByHdrQuery).GetTypeInfo().Assembly);
                services.AddTransient<ARRA.GLOBAL.App.Interfaces.IExcelConnector, ExcelConnector>();
                services.AddTransient<ARRA.GLOBAL.App.Interfaces.ITextFileConnector, TextFileConnector>();
                services.AddTransient<ARRA.GLOBAL.App.Interfaces.IWebServiceConnector, WebServiceConnector>();

                // Add DbContext using SQL Server Provider
                services.AddDbContext<GLOBALDbContext>( options =>
                    options.UseSqlServer(DecryptText(hostContext.Configuration.GetConnectionString("GLOBAL_ConStr"), hostContext.Configuration))
                    //,
                    //ServiceLifetime.Scoped
                    ,ServiceLifetime.Transient
                );

            });

            
            if (isService)
            {
                
                    await builder.RunAsServiceAsync();
                
            }
            else
            {
                await builder.RunConsoleAsync();
            }
        }

    //    public static async Task Main(string[] args)
    //{
    //    var host = new HostBuilder()
    //        .ConfigureLogging((hostContext, config) =>
    //        {
    //            //config.AddConsole();
    //            //config.AddDebug();
    //        })
    //        .ConfigureHostConfiguration(config =>
    //        {
    //            //config.AddEnvironmentVariables();
    //            config.SetBasePath(Directory.GetCurrentDirectory());
    //            config.AddJsonFile("appsettings.json", optional: false, true);

    //        })
    //        .ConfigureAppConfiguration((hostContext, config) =>
    //        {
    //            //config.SetBasePath(Directory.GetCurrentDirectory());
    //            config.AddJsonFile("appsettings.json", optional: false, true);
    //            //config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
    //            config.AddCommandLine(args);
    //        })
    //        .ConfigureServices((hostContext, services) =>
    //        {
    //            services.AddLogging();

    //            services.AddHostedService<ROBINService>();
    //            services.AddHostedService<QueuedHostedService>();
    //            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();

    //            services.AddMediatR(typeof(ARRA.GLOBAL.App.Jobs.Queries.GetJobDtlByHdrQuery).GetTypeInfo().Assembly);

    //            // Add DbContext using SQL Server Provider
    //            services.AddDbContext<GLOBALDbContext>(options =>
    //                options.UseSqlServer(hostContext.Configuration.GetConnectionString("GLOBAL_ConStr")));

    //        })

    //        .UseConsoleLifetime()
    //        .Build();

        

    //    using (host)
    //    {
    //        // Start the host
    //        await host.StartAsync();
    //        // Wait for the host to shutdown
    //        await host.WaitForShutdownAsync();
    //    }
    //}

   
        //public static IHostBuilder CreateHostBuilder(string[] args) =>
        //    Host.CreateDefaultBuilder(args)
        //        .UseWindowsService()
        //        .ConfigureAppConfiguration((context, config) =>
        //        {
        //            // Configure the app here.
        //        })
        //        .ConfigureServices((hostContext, services) =>
        //        {
        //            services.AddHostedService<ServiceA>();
        //            services.AddHostedService<ServiceB>();
        //        })
        //        // Only required if the service responds to requests.
        //        .ConfigureWebHostDefaults(webBuilder =>
        //        {
        //            webBuilder.UseStartup<Startup>();
        //        });

        //}

    }

}
