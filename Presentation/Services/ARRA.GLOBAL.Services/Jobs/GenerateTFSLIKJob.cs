﻿using System;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Jobs.Models;
using MediatR;
using ARRA.GLOBAL.Services.Jobs.Base;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using System.Threading.Tasks;
using System.Linq;
using ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class GenerateTFSLIKJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public GenerateTFSLIKJob(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CODE").FirstOrDefault().paramValue.Trim();
                string reportDate = _jobDetail.Where(f => f.paramName == "REPORT_DATE").FirstOrDefault().paramValue.Trim();
                StatusModel sts = await _mediator.Send(GetCommand<ExportTFDataSLIKCommand, StatusModel>(new ExportTFDataSLIKCommand
                {
                    formCode = formCode,
                    reportDate = Convert.ToDateTime(reportDate),
                    jobId = _jobHeader.uniqueId
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                    await UpdateHistoryLogStatusByQueue(_jobHeader.uniqueId, Common.Enumerations.JobStatus.Failed, sts.message, _jobHeader.module);
                }

                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
                await UpdateHistoryLogStatusByQueue(_jobHeader.uniqueId, Common.Enumerations.JobStatus.Failed, ex.Message, _jobHeader.module);
            }
        }
    }
}