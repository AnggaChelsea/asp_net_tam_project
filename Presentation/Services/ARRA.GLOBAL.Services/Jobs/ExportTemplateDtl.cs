﻿using System;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Jobs.Models;
using MediatR;
using ARRA.GLOBAL.Services.Jobs.Base;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using System.Linq;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class ExportTemplateDtl : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public ExportTemplateDtl(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }


        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CD").FirstOrDefault().paramValue.Trim();
                string filter = _jobDetail.Where(f => f.paramName == "FILTER").FirstOrDefault().paramValue.Trim();
                string sort = _jobDetail.Where(f => f.paramName == "SORT").FirstOrDefault().paramValue.Trim();
                string fileType = _jobDetail.Where(f => f.paramName == "FILE_TYPE").FirstOrDefault().paramValue.Trim();
                StatusModel sts = await _mediator.Send(GetCommand<ExportDataTemplateDtlCommand, StatusModel>(new ExportDataTemplateDtlCommand
                {
                    formCode = formCode,
                    filter = filter,
                    sort = sort,
                    fileType = fileType,
                    jobId = _jobHeader.uniqueId
                }));

                //Console.WriteLine("export process finish" + _jobHeader.uniqueId + "dT: " + DateTime.Now.ToString());

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
            }
        }
    }
}