﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Helper.Model;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.Parameter.Queries;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.Common;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class ValidationJob : JobBase, IJobBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private int _maxDependCounter;
        private IList<string> _resultDepend;

        public ValidationJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail, IConfiguration configuration) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
            _configuration = configuration;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CODE").FirstOrDefault().paramValue;
                string dataDate = _jobDetail.Where(f => f.paramName == "REPORT_DATE").FirstOrDefault().paramValue;

                ParamHeaderModel mPackage =
                    await _mediator.Send(GetQuery<GetSysParDtlByFormCdQuery, ParamHeaderModel>
                    (new GetSysParDtlByFormCdQuery
                    {
                        formCode = formCode,
                        parName = ParamNameCollection.VALIDATION_PACKAGE.ToString()
                    }));
                if (mPackage == null) throw new Exception(ErrorMessages.ValidationPackageNotFound);

                ApplicationSettings sett = new ApplicationSettings(_configuration);
                string result = await new Helper.ETLUtility(_mediator).RunETL(new ETLModel()
                {
                    PackageName = mPackage.parValue,
                    Password = sett.ETLPassword,
                    Location = sett.ETLLocation,
                    Parameters = new List<Parameter>(){
                        new Parameter { Name = "CON_STR_DATA_MART", Value = sett.ETLDataMartConStr.Replace(";","#")},
                        new Parameter { Name = "FORM_CD", Value = formCode },
                        new Parameter { Name = "DATA_DATE", Value = dataDate },
                        new Parameter { Name = "JOB_ID", Value = _jobHeader.uniqueId.ToString() }
                    }
                }, _jobHeader.uniqueId, _jobHeader.module);

                mPackage = null;
                sett = null;

                if (result.ToUpper().Contains("DTSER_SUCCESS"))
                {
                    await UpdateHistoryLogValidationByQueue(_jobHeader.uniqueId, Common.Enumerations.JobStatus.Success, "", _jobHeader.module, formCode, Convert.ToDateTime(dataDate));
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateHistoryLogValidationByQueue(_jobHeader.uniqueId, Common.Enumerations.JobStatus.Failed, result, _jobHeader.module, formCode, Convert.ToDateTime(dataDate));
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, result);
                }
            }
            catch (Exception ex)
            {
                await UpdateHistoryLogStatusByQueue(_jobHeader.uniqueId, Common.Enumerations.JobStatus.Failed, ex.Message, _jobHeader.module);
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
            }
        }
    }
}
