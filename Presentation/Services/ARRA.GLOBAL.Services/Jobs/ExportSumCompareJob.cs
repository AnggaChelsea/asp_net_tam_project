﻿using ARRA.GLOBAL.App.Forms.Customs.Task.Commands;
using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Customs.Task.Commands;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Jobs;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class ExportSumCompareJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public ExportSumCompareJob(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }


        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                try
                {
                    string filePath = _jobDetail.Where(f => f.paramName == "FILE_PATH").FirstOrDefault().paramValue.Trim();
                    string fileName = _jobDetail.Where(f => f.paramName == "FILE_NAME").FirstOrDefault().paramValue.Trim();
                    string module = _jobDetail.Where(f => f.paramName == "MODULE").FirstOrDefault().paramValue.Trim();
                    string formCode = _jobDetail.Where(f => f.paramName == "FORM_CD").FirstOrDefault().paramValue.Trim();
                    string reportDt = _jobDetail.Where(f => f.paramName == "REPORT_DATE").FirstOrDefault().paramValue.Trim();
                    string comparisonDt = _jobDetail.Where(f => f.paramName == "COMPARISON_DATE").FirstOrDefault().paramValue.Trim();

                    StatusModel sts = await _mediator.Send(GetCommand<ExportSumCompareCommand, StatusModel>(new ExportSumCompareCommand
                    {
                        filePath = filePath,
                        module = module,
                        formCode = formCode, 
                        fileName = fileName,
                        reportDt = reportDt,
                        comparisonDt = comparisonDt,
                        jobId = _jobHeader.uniqueId
                    }));

                    if (sts.status == JobStatus.Success.ToString())
                    {
                        await UpdateJobStatus(_jobHeader, JobStatus.Success);
                    }
                    else
                    {
                        await UpdateJobStatus(_jobHeader, JobStatus.Failed, sts.message);
                    }
                }
                catch (Exception ex)
                {
                    await UpdateJobStatus(_jobHeader, JobStatus.Failed, ex.Message);
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, JobStatus.Failed, ex.Message);
            }
        }
    }
}
