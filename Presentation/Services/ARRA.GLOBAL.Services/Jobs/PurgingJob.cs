﻿using System;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Jobs.Models;
using MediatR;
using ARRA.GLOBAL.Services.Jobs.Base;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using System.Threading.Tasks;
using System.Linq;
using ARRA.GLOBAL.App.Purging.Commands;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class PurgingJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public PurgingJob(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }


        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string uids = _jobDetail.Where(f => f.paramName == "UIDS").FirstOrDefault().paramValue.Trim();
                string reportDt = _jobDetail.Where(f => f.paramName == "REPORT_DT").FirstOrDefault().paramValue.Trim();

                StatusModel sts = await _mediator.Send(GetCommand<PurgingCommand, StatusModel>(new PurgingCommand
                {
                    reportDt = reportDt,
                    uids = uids,
                    jobId = _jobHeader.uniqueId,
                    createdBy = _jobHeader.createdBy,
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
            }
        }
    }
}