﻿using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Forms.Commands;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class ImportJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        public ImportJob(IMediator mediator,HeaderModel jobHeader, IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CD").FirstOrDefault().paramValue.Trim();
                string fileName = _jobDetail.Where(f => f.paramName == "FILE_NAME").FirstOrDefault().paramValue.Trim();
                string oriFileName = _jobDetail.Where(f => f.paramName == "FILE_NAME_ORIGINAL").FirstOrDefault().paramValue.Trim();
                string userId = _jobDetail.Where(f => f.paramName == "USER_ID").FirstOrDefault().paramValue.Trim();

                StatusModel sts = await _mediator.Send(GetCommand<ImportDataCommand, StatusModel>(new ImportDataCommand
                {
                    formCode = formCode,
                    fileName = fileName,
                    oriFileName = oriFileName,
                    queueId = _jobHeader.uniqueId,
                    userId = userId
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }

                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message+" ,INNER:"+ex.InnerException);
            }
        }

    }
}
