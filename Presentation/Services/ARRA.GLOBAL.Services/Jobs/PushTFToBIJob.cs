﻿using System;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Jobs.Models;
using MediatR;
using ARRA.GLOBAL.Services.Jobs.Base;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using System.Threading.Tasks;
using ARRA.GLOBAL.App.Forms.Commands;
using System.Linq;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class PushTFToBIJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;


        public PushTFToBIJob(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            //throw new NotImplementedException();
            return Task.FromResult(true);
        }

        public Task Run()
        {
            throw new NotImplementedException();
        }
    }
}
