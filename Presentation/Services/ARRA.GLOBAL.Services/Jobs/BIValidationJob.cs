﻿using MediatR;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Helper.Model;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ARRA.Common;
using ARRA.GLOBAL.App.ProcessDependency.Models;
using ARRA.GLOBAL.App.ProcessDependency.Queries;
using ARRA.Common.Enumerations;
using ARRA.GLOBAL.App.ProcessHistory.Queries;
using ARRA.GLOBAL.App.ProcessHistory.Models;
using Microsoft.Extensions.Configuration;
using ARRA.GLOBAL.App.BIValidation.Models;
using ARRA.GLOBAL.App.BIValidation.Queries;
using ARRA.GLOBAL.App.BIValidation.Commands;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class BIValidationJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private IConfiguration _configuration;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private int _maxDependCounter;
        private IList<string> _resultDepend;

        public BIValidationJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail, IConfiguration configuration) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
            _configuration = configuration;
        }

        /// <summary>
        /// false = waiting for dependecies process
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DependenciesChecking()
        {
            try
            {
                DependListModel m = await _mediator.Send(GetQuery<GetDependByJobTypeQuery, DependListModel>(new GetDependByJobTypeQuery { processType= QueueJobType.BI_VALIDATION.ToString(), module=_jobHeader.module }));
                if (m == null)
                {
                    return false;
                }
                else
                {
                    if (m.Depends.Count == 0)
                    {
                        m = null;
                        return false;
                    }
                    else
                    {
                        this._resultDepend = new List<string>();
                        this._maxDependCounter = 0;

                        string processNo = _jobDetail.Where(f => f.paramName == "PROCESS_NO").FirstOrDefault().paramValue;
                        
                        this.RecursiveCheck(processNo, m.Depends);

                        if (this._resultDepend.Count == 0)
                        {
                            m = null;
                            _resultDepend = null;
                            return false;//no dependencies
                        }
                        else
                        {
                            string dataDate = _jobDetail.Where(f => f.paramName == "DATA_DATE").FirstOrDefault().paramValue;
                            HistLogHdrListModel hists = await _mediator.Send(GetQuery<GetHistDependByTPDateQuery, HistLogHdrListModel>(
                                new GetHistDependByTPDateQuery
                                {
                                    date = Convert.ToDateTime(dataDate),
                                    depend = _resultDepend,
                                    procType = QueueJobType.BI_VALIDATION.ToString(),
                                    module = _jobHeader.module
                                }
                                ));

                            if (hists == null)
                            {
                                m = null;
                                return true;
                            }
                            else
                            {
                                if (hists.HistLogs.Count == 0)
                                {
                                    m = null;
                                    hists = null;
                                    return true;
                                }
                                else
                                {
                                    string wait = "";
                                    foreach(string str in this._resultDepend)
                                    {
                                        HistLogHdrModel find = hists.HistLogs.Where(f => f.processNo == str).FirstOrDefault();
                                        if (find == null)
                                        {
                                            wait += str + ",";
                                        }
                                        else
                                        {
                                            if (!find.status.Equals(JobStatus.Success.ToString()))
                                            {
                                                wait += str + ",";
                                            }
                                        }
                                        find = null;
                                    }

                                    if (wait == "")
                                    {
                                        m = null;
                                        return false;
                                    }
                                    else
                                    { 
                                        wait = wait.Substring(0, wait.Length - 1);

                                        long jobId = Convert.ToInt64(_jobDetail.Where(f => f.paramName == "JOB_ID").FirstOrDefault().paramValue);

                                        await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.Pending, string.Format(InfoMessages.WaitDependencies, wait),_jobHeader.module);

                                        m = null;
                                        return true;
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        private void RecursiveCheck(string procNo, IList<DependModel> ls)
        {
            if (_maxDependCounter > 100) return; //maximum-100 to prevent invalid setting/loop forever.
            IList<DependModel> list = ls.Where(f => f.ProcessNo == procNo).ToList();
            if (list.Count == 0)
            {
                return; //end of recursive.
            }
            else
            {
                foreach (DependModel d in list)
                {
                    _resultDepend.Add(d.DependNo);
                    _maxDependCounter++;
                    RecursiveCheck(d.DependNo, ls);
                }
            }
        }

        public async Task Run()
        {
            long jobId = 0;
            try
            {
                jobId = Convert.ToInt64(_jobDetail.Where(f => f.paramName == "JOB_ID").FirstOrDefault().paramValue);
                string processNo = _jobDetail.Where(f => f.paramName == "PROCESS_NO").FirstOrDefault().paramValue;
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CD").FirstOrDefault().paramValue;
                string dataDate = _jobDetail.Where(f => f.paramName == "DATA_DATE").FirstOrDefault().paramValue;
                string module = _jobDetail.Where(f => f.paramName == "MODULE").FirstOrDefault().paramValue;

                bool isService = true;
                if (_jobDetail.Where(f => f.paramName == "WEB_SERVICE").FirstOrDefault() == null)
                {
                    isService = false;
                }

                if (isService)
                {
                    RunBIValidationCommand cmd = new RunBIValidationCommand();
                    cmd.reportingDate = dataDate;
                    cmd.formCode = formCode;
                    cmd.module = module;
                    StatusModel sts = await _mediator.Send(GetCommand<RunBIValidationCommand, StatusModel>(cmd));
                    if (sts.status.Equals(CommandStatus.Success.ToString()))
                    {
                        await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.SuccessValidation, _jobHeader.module);
                        await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                    }
                    else
                    {
                        await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.FailedValidation, ErrorMessages.BIValidationFailed, _jobHeader.module);
                        await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed);
                    }
                    cmd = null;
                    sts = null;
                }
                else
                {
                    BIValidationModel tfModel = await _mediator.Send(GetQuery<GetBIValidationByNoQuery, BIValidationModel>(new GetBIValidationByNoQuery { module = module, processNo = processNo }));
                    ApplicationSettings sett = new ApplicationSettings(_configuration);
                    string result = await new Helper.ETLUtility(_mediator).RunETL(new ETLModel()
                    {
                        PackageName = tfModel.etlPackage,
                        Password = sett.ETLPassword,
                        Location = sett.ETLLocation,
                        Parameters = new List<Parameter>(){
                            new Parameter { Name = "CON_STR_DATA_MART", Value = sett.ETLDataMartConStr},
                            new Parameter { Name = "CON_STR_BI_VAL_STG_DATA", Value = sett.ETLBIVALSTagingConStr },
                            new Parameter { Name = "FORM_CD", Value = formCode },
                            new Parameter { Name = "DATA_DATE", Value = dataDate },
                            new Parameter { Name = "JOB_ID", Value = jobId.ToString() },
                            new Parameter { Name = "MODULE", Value = module }
                        }
                    }, _jobHeader.uniqueId, _jobHeader.module);

                    sett = null;
                    tfModel = null;

                    if (result.ToUpper().Contains("DTSER_SUCCESS"))
                    {
                        await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.Success, _jobHeader.module);
                        await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                    }
                    else
                    {
                        await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.Failed, result, _jobHeader.module);
                        await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, result);
                    }
                }
            }
            catch (Exception ex)
            {
                await UpdateHistoryLogStatus(jobId, Common.Enumerations.JobStatus.Failed, ex.Message,_jobHeader.module);
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
            }
        }

    }
}