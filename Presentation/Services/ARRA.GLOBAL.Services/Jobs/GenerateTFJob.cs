﻿using System;
using System.Collections.Generic;
using ARRA.GLOBAL.App.Jobs.Models;
using MediatR;
using ARRA.GLOBAL.Services.Jobs.Base;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using System.Threading.Tasks;
using System.Linq;
using ARRA.GLOBAL.App.Forms.Customs.GenerateTF.Commands;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class GenerateTFJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public GenerateTFJob(IMediator mediator, HeaderModel jobHeader,
            IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CODE").FirstOrDefault().paramValue.Trim();
                string reportDate = _jobDetail.Where(f => f.paramName == "REPORT_DATE").FirstOrDefault().paramValue.Trim();
                //string idOperational = _jobDetail.Where(f => f.paramName == "ID_OPERATIONAL").FirstOrDefault().paramValue.Trim();
                string timeFlag = _jobDetail.Where(f => f.paramName == "TIME_FLAG").FirstOrDefault().paramValue.Trim();
                StatusModel sts = await _mediator.Send(GetCommand<ExportTFDataCommand, StatusModel>(new ExportTFDataCommand
                {
                    formCode = formCode,
                    //idOperational = idOperational,
                    reportDate = Convert.ToDateTime(reportDate),
                    jobId = _jobHeader.uniqueId,
                    timeFlag = timeFlag
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }

                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message);
            }
        }
    }
}