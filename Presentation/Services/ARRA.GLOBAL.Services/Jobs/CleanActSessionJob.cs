﻿using ARRA.GLOBAL.App.Authentications.Commands;
using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Jobs;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class CleanActSessionJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;
        public CleanActSessionJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail)
            : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }
        
        public async Task Run()
        {
            try
            {
                StatusModel sts = await _mediator.Send(GetCommand<CleanActiveSessionCommand, StatusModel>(new CleanActiveSessionCommand()));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, ARRA.Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, ARRA.Common.Enumerations.JobStatus.Failed, sts.message);
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, ARRA.Common.Enumerations.JobStatus.Failed, ex.Message
                );
            }
        }
    }
}
