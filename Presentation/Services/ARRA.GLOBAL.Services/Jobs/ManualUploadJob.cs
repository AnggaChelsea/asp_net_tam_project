﻿using MediatR;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ARRA.GLOBAL.App.ManualUpload.Commands;

namespace ARRA.GLOBAL.Services.Jobs
{
    public class ManualUploadJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        public ManualUploadJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(false);
        }

        public async Task Run()
        {
            try
            {
                string processNo = _jobDetail.Where(f => f.paramName == "PROCESS_NO").FirstOrDefault().paramValue.Trim();
                string fileName = _jobDetail.Where(f => f.paramName == "FILE_NAME").FirstOrDefault().paramValue.Trim();
                string oriFileName = _jobDetail.Where(f => f.paramName == "FILE_NAME_ORIGINAL").FirstOrDefault().paramValue.Trim();

                StatusModel sts = await _mediator.Send(GetCommand<ManualUploadCommand, StatusModel>(new ManualUploadCommand
                {
                    processNo = processNo,
                    fileName = fileName,
                    oriFileName = oriFileName,
                    queueId = _jobHeader.uniqueId
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }
                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message+" ,INNER:"+ex.InnerException);
            }
        }
    }
}