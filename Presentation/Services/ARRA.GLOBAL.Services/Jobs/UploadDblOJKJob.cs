﻿using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Jobs.Models;
using ROBIN.GLOBAL.App.ManualUpload.Commands;
using ROBIN.GLOBAL.Services.Jobs.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBIN.GLOBAL.Services.Jobs
{
    public class UploadDblOJKJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        public UploadDblOJKJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(false);
        }

        public async Task Run()
        {
            try
            {
                string reportDate = _jobDetail.Where(f => f.paramName == "REPORT_DATE").FirstOrDefault().paramValue.Trim();
                string fileName = _jobDetail.Where(f => f.paramName == "FILE_NAME").FirstOrDefault().paramValue.Trim();
                string oriFileName = _jobDetail.Where(f => f.paramName == "FILE_NAME_ORIGINAL").FirstOrDefault().paramValue.Trim();
                string formCode = _jobDetail.Where(f => f.paramName == "FORM_CODE").FirstOrDefault().paramValue.Trim();

                StatusModel sts = await _mediator.Send(GetCommand<UploadDblCommand, StatusModel>(new UploadDblCommand
                {
                    formCode = formCode,
                    reportDate = reportDate,
                    fileName = fileName,
                    oriFileName = oriFileName,
                    queueId = _jobHeader.uniqueId
                }));

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, sts.message);
                }
                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(_jobHeader, Common.Enumerations.JobStatus.Failed, ex.Message + " ,INNER:" + ex.InnerException);
            }
        }
    }
}
