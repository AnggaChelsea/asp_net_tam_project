﻿using MediatR;
using ARRA.Common;
using ARRA.Common.Enumerations;
using ARRA.Common.Model;
using ARRA.GLOBAL.App.Jobs.Commands;
using ARRA.GLOBAL.App.Jobs.Models;
using ARRA.GLOBAL.App.Parameter.Models;
using ARRA.GLOBAL.App.Parameter.Queries;
using ARRA.GLOBAL.App.ProcessHistory.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.Services.Jobs.Base
{
    public class JobBase
    {
        //protected IMediator _mediator;
        private IMediator _mediator;
        protected IDateTime _currDate = new ARRA.Infrastructure.MachineDateTime();
        private HeaderModel _jobHeader;

        protected JobBase(IMediator mediator,HeaderModel jobHeader)
        {
            this._mediator = mediator;
            this._jobHeader = jobHeader;
        }

        protected async Task UpdateJobStatus(HeaderModel job, JobStatus status)
        {
            try
            {
                await UpdateJobStatus(job, status,"");
            }
            catch
            {

            }
        }

        protected async Task UpdateHistoryLogStatus(long jobId, JobStatus status,string module)
        {
            try
            {
                await UpdateHistoryLogStatus(jobId, status, "",module);
            }
            catch
            {

            }
        }
        protected async Task UpdateHistoryLogStatusByQueue(long jobId, JobStatus status,string module)
        {
            try
            {
                await UpdateHistoryLogStatusByQueue(jobId, status, "",module);
            }
            catch
            {

            }
        }
        protected async Task<string> GetSysParameterValue(string code)
        {
            string result = "";
            ParamHeaderModel m = await this.GetSysParameter(code);
            if (m != null)
            {
                result = m.parValue;
            }

            return result;
        }

        protected async Task<ParamHeaderModel> GetSysParameter(string code)
        {
            ParamHeaderModel vm = null;
            try
            {
                QueriesModel<GetSysParByCodeQuery, ParamHeaderModel> qry = new QueriesModel<GetSysParByCodeQuery, ParamHeaderModel>();
                qry.QueryModel = new GetSysParByCodeQuery
                {
                    code = code
                };
                qry.CurrentDateTime = _currDate.Now;
                vm = await _mediator.Send(qry);
            }
            catch
            {

            }

            return vm;
        }

        protected async Task UpdateHistoryLogStatusByQueue(long jobId, JobStatus status, string message,string module)
        {
            try
            {
                CommandsModel<UpdateProcHistByQueueCommand, StatusModel> cmd = new CommandsModel<UpdateProcHistByQueueCommand, StatusModel>();
                cmd.CommandModel = new UpdateProcHistByQueueCommand
                {
                    queueId = jobId,
                    status = status.ToString(),
                    remarks = message,
                    module = module
                };

                cmd.CurrentDateTime = _currDate.Now;
                await _mediator.Send(cmd);
            }
            catch
            {

            }
        }

        protected async Task UpdateHistoryLogValidationByQueue(long jobId, JobStatus status, string message, string module,string formCode,DateTime ReportDate)
        {
            try
            {
                CommandsModel<UpdateProcHistValidationCommand, StatusModel> cmd = new CommandsModel<UpdateProcHistValidationCommand, StatusModel>();
                cmd.CommandModel = new UpdateProcHistValidationCommand
                {
                    queueId = jobId,
                    status = status.ToString(),
                    remarks = message,
                    module = module,
                    formCode = formCode,
                    reportingDate = ReportDate
                };

                cmd.CurrentDateTime = _currDate.Now;
                await _mediator.Send(cmd);
            }
            catch
            {

            }
        }
        protected async Task UpdateHistoryLogReconByQueue(long jobId, JobStatus status, string message, string module, DateTime ReportDate,string periodType)
        {
            try
            {
                CommandsModel<UpdateProcHistReconCommand, StatusModel> cmd = new CommandsModel<UpdateProcHistReconCommand, StatusModel>();
                cmd.CommandModel = new UpdateProcHistReconCommand
                {
                    queueId = jobId,
                    status = status.ToString(),
                    remarks = message,
                    module = module,
                    reportingDate = ReportDate,
                    periodType = periodType
                };

                cmd.CurrentDateTime = _currDate.Now;
                await _mediator.Send(cmd);
            }
            catch
            {

            }
        }

        protected async Task UpdateHistoryLogStatus(long jobId, JobStatus status, string message,string module)
        {
            try
            {
                CommandsModel<UpdateProcHistCommand, StatusModel> cmd = new CommandsModel<UpdateProcHistCommand, StatusModel>();
                cmd.CommandModel = new UpdateProcHistCommand
                {
                    jobId = jobId,
                    status = status.ToString(),
                    remarks = message,
                    module = module
                };

                cmd.CurrentDateTime = _currDate.Now;
                await _mediator.Send(cmd);
                cmd = null;
            }
            catch
            {

            }
        }

        protected async Task UpdateJobStatus(HeaderModel job, JobStatus status,string message)
        {
            try
            {
                CommandsModel<UpdateHdrByIdCommand, StatusModel> cmd = new CommandsModel<UpdateHdrByIdCommand, StatusModel>();
                cmd.CommandModel = new UpdateHdrByIdCommand { uniqueId = job.uniqueId, status = status.ToString(), message = message, module = job.module };
                cmd.CurrentDateTime = _currDate.Now;
                await _mediator.Send(cmd);
            }
            catch
            {

            }
        }

        protected CommandsModel<TReq, TRes> GetCommand<TReq, TRes>(TReq command) where TReq : class
            where TRes : class
        {
            CommandsModel<TReq, TRes> m = new CommandsModel<TReq, TRes>();
            m.UserIdentity = new UserIdentityModel
            {
                UserId = _jobHeader.createdBy,
                Host = _jobHeader.createdHost
            };
            m.CurrentDateTime = _currDate.Now;
            m.CommandModel = command;

            return m;
        }

        protected QueriesModel<TReq, TRes> GetQuery<TReq, TRes>(TReq query) where TReq : class
            where TRes : class
        {
            QueriesModel<TReq, TRes> m = new QueriesModel<TReq, TRes>();
            m.UserIdentity = new UserIdentityModel
            {
                UserId = _jobHeader.createdBy,
                Host = _jobHeader.createdHost
            };
            m.CurrentDateTime = _currDate.Now;
            m.QueryModel = query;

            return m;
        }

    }
}