﻿using ARRA.GLOBAL.App.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.GLOBAL.Services.Jobs
{
    interface IJobBase
    {
        Task Run();
        Task<bool> DependenciesChecking();
    }
}