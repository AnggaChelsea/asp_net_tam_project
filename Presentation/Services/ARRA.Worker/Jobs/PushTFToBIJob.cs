﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.GLOBAL.App.Jobs.Models;

namespace ARRA.Worker.Jobs
{
    public class PushTFToBIJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public PushTFToBIJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            //throw new NotImplementedException();
            return Task.FromResult(true);
        }

        public Task Run()
        {
            throw new NotImplementedException();
        }
    }
}
