﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Forms.Commands;
using ROBIN.GLOBAL.App.Jobs.Models;

namespace ARRA.Worker.Jobs
{
    public class ExportAdhoc : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public ExportAdhoc(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail)
            : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string query = _jobDetail
                    .Where(f => f.paramName == "QUERY")
                    .FirstOrDefault()
                    .paramValue.Trim();
                string fileType = _jobDetail
                    .Where(f => f.paramName == "FILE_TYPE")
                    .FirstOrDefault()
                    .paramValue.Trim();
                StatusModel sts = await _mediator.Send(
                    GetCommand<ExportDataAdhocCommand, StatusModel>(
                        new ExportDataAdhocCommand
                        {
                            fileType = fileType,
                            jobId = _jobHeader.uniqueId,
                            queryVal = query
                        }
                    )
                );

                //Console.WriteLine("export process finish" + _jobHeader.uniqueId + "dT: " + DateTime.Now.ToString());

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, ROBIN.Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(
                        _jobHeader,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        sts.message
                    );
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
