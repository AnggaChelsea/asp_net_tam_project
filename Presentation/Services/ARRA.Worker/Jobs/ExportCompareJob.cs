﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Forms.Customs.Task.Commands;
using ROBIN.GLOBAL.App.Jobs.Models;

namespace ARRA.Worker.Jobs
{
    public class ExportCompareJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public ExportCompareJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                try
                {
                    string filePath = _jobDetail
                        .Where(f => f.paramName == "FILE_PATH")
                        .FirstOrDefault()
                        .paramValue.Trim();
                    string fileName = _jobDetail
                        .Where(f => f.paramName == "FILE_NAME")
                        .FirstOrDefault()
                        .paramValue.Trim();
                    string module = _jobDetail
                        .Where(f => f.paramName == "MODULE")
                        .FirstOrDefault()
                        .paramValue.Trim();
                    string formCode = _jobDetail
                        .Where(f => f.paramName == "FORM_CD")
                        .FirstOrDefault()
                        .paramValue.Trim();
                    string reportDt = _jobDetail
                        .Where(f => f.paramName == "REPORT_DATE")
                        .FirstOrDefault()
                        .paramValue.Trim();
                    string comparisonDt = _jobDetail
                        .Where(f => f.paramName == "COMPARISON_DATE")
                        .FirstOrDefault()
                        .paramValue.Trim();

                    StatusModel sts = await _mediator.Send(
                        GetCommand<ExportCompareCommand, StatusModel>(
                            new ExportCompareCommand
                            {
                                filePath = filePath,
                                module = module,
                                formCode = formCode,
                                fileName = fileName,
                                reportDt = reportDt,
                                comparisonDt = comparisonDt,
                                jobId = _jobHeader.uniqueId
                            }
                        )
                    );

                    if (sts.status == JobStatus.Success.ToString())
                    {
                        await UpdateJobStatus(
                            _jobHeader,
                            ROBIN.Common.Enumerations.JobStatus.Success
                        );
                    }
                    else
                    {
                        await UpdateJobStatus(
                            _jobHeader,
                            ROBIN.Common.Enumerations.JobStatus.Failed,
                            sts.message
                        );
                    }
                }
                catch (Exception ex)
                {
                    await UpdateJobStatus(
                        _jobHeader,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        ex.Message
                    );
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
