﻿using ARRA.GLOBAL.App.ProcessDataSource.Models;
using ARRA.GLOBAL.App.ProcessDataSource.Queries;
using ARRA.Worker.Helper.Model;
using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Jobs.Models;
using ROBIN.GLOBAL.App.ProcessDataSource.Models;
using ROBIN.GLOBAL.App.ProcessDataSource.Queries;
using ROBIN.GLOBAL.App.ProcessDependency.Models;
using ROBIN.GLOBAL.App.ProcessDependency.Queries;
using ROBIN.GLOBAL.App.ProcessHistory.Models;
using ROBIN.GLOBAL.App.ProcessHistory.Queries;

namespace ARRA.Worker.Jobs
{
    public class ProcessDatasourceJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private readonly IConfiguration _configuration;
        private int _maxDependCounter;
        private IList<string> _resultDepend;

        public ProcessDatasourceJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail,
            IConfiguration configuration
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
            _configuration = configuration;
        }

        /// <summary>
        /// false = waiting for dependecies process
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DependenciesChecking()
        {
            try
            {
                DependListModel m = await _mediator.Send(
                    GetQuery<GetDependByJobTypeQuery, DependListModel>(
                        new GetDependByJobTypeQuery
                        {
                            processType = QueueJobType.PROCESS_DATA_SOURCE.ToString(),
                            module = _jobHeader.module
                        }
                    )
                );
                if (m == null)
                {
                    return false;
                }
                else
                {
                    if (m.Depends.Count == 0)
                    {
                        m = null;
                        return false;
                    }
                    else
                    {
                        this._resultDepend = new List<string>();
                        this._maxDependCounter = 0;

                        string processNo = _jobDetail
                            .Where(f => f.paramName == "PROCESS_NO")
                            .FirstOrDefault()
                            .paramValue;

                        this.RecursiveCheck(processNo, m.Depends);

                        if (this._resultDepend.Count == 0)
                        {
                            m = null;
                            return false; //no dependencies
                        }
                        else
                        {
                            string dataDate = _jobDetail
                                .Where(f => f.paramName == "DATA_DATE")
                                .FirstOrDefault()
                                .paramValue;
                            HistLogHdrListModel hists = await _mediator.Send(
                                GetQuery<GetHistDependByTPDateQuery, HistLogHdrListModel>(
                                    new GetHistDependByTPDateQuery
                                    {
                                        date = Convert.ToDateTime(dataDate),
                                        depend = _resultDepend,
                                        procType = QueueJobType.PROCESS_DATA_SOURCE.ToString(),
                                        module = _jobHeader.module
                                    }
                                )
                            );

                            if (hists == null)
                            {
                                return true;
                            }
                            else
                            {
                                if (hists.HistLogs.Count == 0)
                                {
                                    return true;
                                }
                                else
                                {
                                    string wait = "";
                                    foreach (string str in this._resultDepend)
                                    {
                                        HistLogHdrModel find = hists.HistLogs
                                            .Where(f => f.processNo == str)
                                            .FirstOrDefault();
                                        if (find == null)
                                        {
                                            wait += str + ",";
                                        }
                                        else
                                        {
                                            if (!find.status.Equals(JobStatus.Success.ToString()))
                                            {
                                                wait += str + ",";
                                            }
                                        }
                                        find = null;
                                    }

                                    if (wait == "")
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        wait = wait.Substring(0, wait.Length - 1);

                                        long jobId = Convert.ToInt64(
                                            _jobDetail
                                                .Where(f => f.paramName == "JOB_ID")
                                                .FirstOrDefault()
                                                .paramValue
                                        );

                                        await UpdateHistoryLogStatus(
                                            jobId,
                                            JobStatus.Pending,
                                            string.Format(InfoMessages.WaitDependencies, wait),
                                            _jobHeader.module
                                        );

                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                m = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RecursiveCheck(string procNo, IList<DependModel> ls)
        {
            if (_maxDependCounter > 100)
                return; //maximum-100 to prevent invalid setting/loop forever.
            IList<DependModel> list = ls.Where(f => f.ProcessNo == procNo).ToList();
            if (list.Count == 0)
            {
                return; //end of recursive.
            }
            else
            {
                foreach (DependModel d in list)
                {
                    _resultDepend.Add(d.DependNo);
                    _maxDependCounter++;
                    RecursiveCheck(d.DependNo, ls);
                }
            }
        }

        public async Task Run()
        {
            long jobId = 0;
            try
            {
                jobId = Convert.ToInt64(
                    _jobDetail.Where(f => f.paramName == "JOB_ID").FirstOrDefault().paramValue
                );
                string processNo = _jobDetail
                    .Where(f => f.paramName == "PROCESS_NO")
                    .FirstOrDefault()
                    .paramValue;
                string fileName = _jobDetail
                    .Where(f => f.paramName == "FILE_NAME")
                    .FirstOrDefault()
                    .paramValue;
                string dataDate = _jobDetail
                    .Where(f => f.paramName == "DATA_DATE")
                    .FirstOrDefault()
                    .paramValue;
                DataSourceModel ds = await _mediator.Send(
                    GetQuery<GetDSByNoQuery, DataSourceModel>(
                        new GetDSByNoQuery { processNo = processNo }
                    )
                );

                bool as32bit = false;
                if (ds != null)
                {
                    as32bit = ds.as32Bit;
                }

                ApplicationSettings sett = new ApplicationSettings(_configuration);
                string dsConnection = "";
                bool process = false;
                if (ds.usingTF)
                {
                    string path = await base.GetSysParameterValue("GB014");
                    string folder = await base.GetSysParameterValue("GB015");
                    if (!string.IsNullOrEmpty(folder))
                    {
                        folder = Convert.ToDateTime(dataDate).ToString(folder);
                        path += folder + @"\";
                    }
                    string fileFullPath = path + fileName;

                    if (File.Exists(fileFullPath))
                    {
                        process = true;
                        dsConnection = fileFullPath;
                    }
                    else
                    {
                        await UpdateHistoryLogStatus(
                            jobId,
                            JobStatus.Failed,
                            string.Format(ErrorMessages.FileNotExists, fileFullPath)
                        );
                        await UpdateJobStatus(
                            _jobHeader,
                            JobStatus.Failed,
                            string.Format(ErrorMessages.FileNotExists, fileFullPath)
                        );
                    }
                }
                else
                {
                    process = true;
                    dsConnection = sett.ETLSourceDataConStr; //constring
                }

                string result = "";

                if (process)
                {
                    IList<Parameter> _parameters = new List<Parameter>()
                    {
                        new Parameter { Name = "CON_STR_SOURCE_DATA", Value = dsConnection },
                        new Parameter { Name = "CON_STR_STAGING", Value = sett.ETLStagingConStr },
                        new Parameter { Name = "CON_STR_DATA_MODEL", Value = sett.ETLRIMConStr },
                        new Parameter { Name = "DATA_DATE", Value = dataDate },
                        //new Parameter { Name = "ARCHIVE_MONTH", Value = "" }, //will seperate the archive process?
                        new Parameter { Name = "JOB_ID", Value = jobId.ToString() }
                    };

                    // get etl variable from database
                    IList<ETLVariableModel> etlVariables = await _mediator.Send(
                        GetQuery<GetETLVariableByProcessNoQuery, IList<ETLVariableModel>>(
                            new GetETLVariableByProcessNoQuery
                            {
                                module = "GLOBAL",
                                processNo = processNo
                            }
                        )
                    );

                    // foreach etlvariables insert into _parameters
                    foreach (var item in etlVariables)
                    {
                        _parameters.Add(new Parameter() { Name = item.Name, Value = item.Value });
                    }

                    result = await new Helper.ETLUtility(_mediator).RunETL(
                        new ETLModel()
                        {
                            PackageName = ds.etlPackage,
                            Password = sett.ETLPassword,
                            Location = sett.ETLLocation,
                            As32Bit = as32bit,
                            Path32Bit = sett.ETL32BitEXEPath,
                            Parameters = _parameters
                        },
                        _jobHeader.uniqueId,
                        _jobHeader.module
                    );
                }

                sett = null;
                ds = null;

                if (result.ToUpper().Contains("DTSER_SUCCESS"))
                {
                    await UpdateHistoryLogStatus(jobId, JobStatus.Success, _jobHeader.module);
                    await UpdateJobStatus(_jobHeader, JobStatus.Success);
                }
                else
                {
                    await UpdateHistoryLogStatus(
                        jobId,
                        JobStatus.Failed,
                        result,
                        _jobHeader.module
                    );
                    await UpdateJobStatus(_jobHeader, JobStatus.Failed, result);
                }
            }
            catch (Exception ex)
            {
                await UpdateHistoryLogStatus(
                    jobId,
                    JobStatus.Failed,
                    ex.Message,
                    _jobHeader.module
                );
                await UpdateJobStatus(_jobHeader, JobStatus.Failed, ex.Message);
            }
        }
    }
}

//SSIS for data staging & data model
//===parameter=====
//CON_STR_STAGING		| string
//CON_STR_DATA_MODEL	| string
//FILE_PATH		| string
//DATA_DATE		| date
//ARCHIVE_MONTH		| int
//JOB_ID    		| long


//SSIS for data mart transform
//==parameter===
//CON_STR_DATA_MODEL	| string
//CON_STR_DATA_MART	| string
//DATA_DATE		| date
//ARCHIVE_MONTH		| int
//JOB_ID			| long
