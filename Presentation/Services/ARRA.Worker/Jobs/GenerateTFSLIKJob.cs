﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Forms.Customs.GenerateTF.Commands;
using ROBIN.GLOBAL.App.Jobs.Models;

namespace ARRA.Worker.Jobs
{
    public class GenerateTFSLIKJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public GenerateTFSLIKJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string formCode = _jobDetail
                    .Where(f => f.paramName == "FORM_CODE")
                    .FirstOrDefault()
                    .paramValue.Trim();
                string reportDate = _jobDetail
                    .Where(f => f.paramName == "REPORT_DATE")
                    .FirstOrDefault()
                    .paramValue.Trim();
                StatusModel sts = await _mediator.Send(
                    GetCommand<ExportTFDataSLIKCommand, StatusModel>(
                        new ExportTFDataSLIKCommand
                        {
                            formCode = formCode,
                            reportDate = Convert.ToDateTime(reportDate),
                            jobId = _jobHeader.uniqueId
                        }
                    )
                );

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, ROBIN.Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(
                        _jobHeader,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        sts.message
                    );
                }

                sts = null;
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
