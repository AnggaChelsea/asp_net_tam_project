﻿using ARRA.Worker.Helper.Model;
using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Jobs.Models;
using ROBIN.GLOBAL.App.Parameter.Models;
using ROBIN.GLOBAL.App.Parameter.Queries;
using ROBIN.GLOBAL.Domain.Entities;

namespace ARRA.Worker.Jobs
{
    public class ReconciliationJob : JobBase, IJobBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private int _maxDependCounter;
        private IList<string> _resultDepend;

        public ReconciliationJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail,
            IConfiguration configuration
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
            _configuration = configuration;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string periodType = _jobDetail
                    .Where(f => f.paramName == "PERIOD_TYPE")
                    .FirstOrDefault()
                    .paramValue;
                string dataDate = _jobDetail
                    .Where(f => f.paramName == "REPORT_DATE")
                    .FirstOrDefault()
                    .paramValue;

                ParamHeaderModel mPackage = await _mediator.Send(
                    GetQuery<GetSysParDtlByModulePeriodQuery, ParamHeaderModel>(
                        new GetSysParDtlByModulePeriodQuery
                        {
                            module = _jobHeader.module,
                            period = periodType,
                            parName = ParamNameCollection.RECONCILIATION_PACKAGE.ToString()
                        }
                    )
                );

                if (mPackage == null)
                    throw new Exception(ErrorMessages.ReconciliationPackageNotFound);

                ApplicationSettings sett = new ApplicationSettings(_configuration);
                string result = await new Helper.ETLUtility(_mediator).RunETL(
                    new ETLModel()
                    {
                        PackageName = mPackage.parValue,
                        Password = sett.ETLPassword,
                        Location = sett.ETLLocation,
                        Parameters = new List<Parameter>()
                        {
                            new Parameter
                            {
                                Name = "CON_STR_DATA_MART",
                                Value = sett.ETLDataMartConStr.Replace(";", "#")
                            },
                            new Parameter { Name = "PERIOD_TP", Value = periodType },
                            new Parameter { Name = "DATA_DATE", Value = dataDate },
                            new Parameter
                            {
                                Name = "JOB_ID",
                                Value = _jobHeader.uniqueId.ToString()
                            }
                        }
                    },
                    _jobHeader.uniqueId,
                    _jobHeader.module
                );

                mPackage = null;
                sett = null;

                if (result.ToUpper().Contains("DTSER_SUCCESS"))
                {
                    await UpdateHistoryLogReconByQueue(
                        _jobHeader.uniqueId,
                        ROBIN.Common.Enumerations.JobStatus.Success,
                        "",
                        _jobHeader.module,
                        Convert.ToDateTime(dataDate),
                        periodType
                    );
                    await UpdateJobStatus(_jobHeader, ROBIN.Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateHistoryLogReconByQueue(
                        _jobHeader.uniqueId,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        result,
                        _jobHeader.module,
                        Convert.ToDateTime(dataDate),
                        periodType
                    );
                    await UpdateJobStatus(
                        _jobHeader,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        result
                    );
                }
            }
            catch (Exception ex)
            {
                await UpdateHistoryLogStatusByQueue(
                    _jobHeader.uniqueId,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message,
                    _jobHeader.module
                );
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
