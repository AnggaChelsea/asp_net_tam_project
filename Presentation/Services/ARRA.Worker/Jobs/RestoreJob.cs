﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Jobs.Models;
using ROBIN.GLOBAL.App.Restore.Commands;

namespace ARRA.Worker.Jobs
{
    public class RestoreJob : JobBase, IJobBase
    {
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;
        private IMediator _mediator;

        public RestoreJob(IMediator mediator, HeaderModel jobHeader, IList<DetailModel> jobDetail)
            : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            try
            {
                string uids = _jobDetail
                    .Where(f => f.paramName == "UIDS")
                    .FirstOrDefault()
                    .paramValue.Trim();
                string reportDt = _jobDetail
                    .Where(f => f.paramName == "REPORT_DT")
                    .FirstOrDefault()
                    .paramValue.Trim();

                StatusModel sts = await _mediator.Send(
                    GetCommand<RestoreCommand, StatusModel>(
                        new RestoreCommand
                        {
                            reportDt = reportDt,
                            uids = uids,
                            jobId = _jobHeader.uniqueId,
                            createdBy = _jobHeader.createdBy,
                        }
                    )
                );

                if (sts.status == JobStatus.Success.ToString())
                {
                    await UpdateJobStatus(_jobHeader, ROBIN.Common.Enumerations.JobStatus.Success);
                }
                else
                {
                    await UpdateJobStatus(
                        _jobHeader,
                        ROBIN.Common.Enumerations.JobStatus.Failed,
                        sts.message
                    );
                }
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
