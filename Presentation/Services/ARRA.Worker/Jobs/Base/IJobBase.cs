﻿namespace ARRA.Worker.Jobs
{
    interface IJobBase
    {
        Task Run();
        Task<bool> DependenciesChecking();
    }
}
