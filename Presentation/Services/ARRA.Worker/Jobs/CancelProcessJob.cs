﻿using ARRA.Worker.Jobs.Base;
using MediatR;
using ROBIN.Common.Enumerations;
using ROBIN.GLOBAL.App.Jobs.Models;
using System.Diagnostics;

namespace ARRA.Worker.Jobs
{
    public class CancelProcessJob : JobBase, IJobBase
    {
        private IMediator _mediator;
        private HeaderModel _jobHeader;
        private IList<DetailModel> _jobDetail;

        public CancelProcessJob(
            IMediator mediator,
            HeaderModel jobHeader,
            IList<DetailModel> jobDetail
        ) : base(mediator, jobHeader)
        {
            _jobHeader = jobHeader;
            _jobDetail = jobDetail;
            _mediator = mediator;
        }

        public Task<bool> DependenciesChecking()
        {
            return Task.FromResult(true);
        }

        public async Task Run()
        {
            long jobId = Convert.ToInt64(
                _jobDetail.Where(f => f.paramName == "JOB_ID").FirstOrDefault().paramValue
            );

            try
            {
                await base.UpdateHistoryLogStatus(jobId, JobStatus.Cancelled, _jobHeader.module);

                string threadId = Convert.ToString(
                    _jobDetail.Where(f => f.paramName == "THREAD_ID").FirstOrDefault().paramValue
                );
                if (!string.IsNullOrEmpty(threadId))
                {
                    using (Process prc = Process.GetProcessById(Convert.ToInt32(threadId)))
                        if (prc != null)
                        {
                            string name = prc.ProcessName;
                            if (name.ToLower() == "dtexec") //kill ssis, mMm
                            {
                                prc.Kill();
                            }
                        }
                }

                await UpdateJobStatus(_jobHeader, ROBIN.Common.Enumerations.JobStatus.Success);
            }
            catch (Exception ex)
            {
                await UpdateJobStatus(
                    _jobHeader,
                    ROBIN.Common.Enumerations.JobStatus.Failed,
                    ex.Message
                );
            }
        }
    }
}
