﻿using ARRA.Worker.Helper.Model;
using ROBIN.GLOBAL.App.Jobs.Commands;
using System.Diagnostics;

namespace ARRA.Worker.Helper
{
    public class ETLUtility
    {
        private ETLModel _etlModel;
        private MediatR.IMediator _mediator;
        private long _jobId;
        private string _module;

        public ETLUtility(MediatR.IMediator mediator)
        {
            _mediator = mediator;
        }

        public async System.Threading.Tasks.Task<string> RunETL(
            ETLModel m,
            long jobId,
            string module
        )
        {
            _etlModel = m;
            _jobId = jobId;
            _module = module;
            try
            {
                return await RunSSIS();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async System.Threading.Tasks.Task<string> RunETL(
            ETLModel m,
            bool use32bit,
            long jobId,
            string module
        )
        {
            _etlModel = m;
            _jobId = jobId;
            _module = module;

            try
            {
                return await RunSSIS();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async System.Threading.Tasks.Task<string> RunSSIS()
        {
            string result = "";
            try
            {
                string args = " /FILE \"" + _etlModel.Location + _etlModel.PackageName + "\" ";

                if (!string.IsNullOrEmpty(_etlModel.Password)) //using password
                {
                    args += "/DE " + _etlModel.Password + " ";
                }

                foreach (Parameter p in _etlModel.Parameters)
                {
                    if (string.IsNullOrEmpty(p.Value))
                    {
                        args +=
                            " /SET \u005cpackage.variables[" + p.Name + "].Value;\"" + " " + "\"";
                    }
                    else
                    {
                        args +=
                            " /SET \u005cpackage.variables["
                            + p.Name
                            + "].Value;\""
                            + p.Value
                            + "\"";
                    }
                }

                if (_etlModel.As32Bit)
                {
                    result = await RunCMD(_etlModel.Path32Bit, args);
                }
                else
                {
                    result = await RunCMD("DTEXEC.EXE", args);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async System.Threading.Tasks.Task<string> RunCMD(string exeName, string args)
        {
            string output = "";
            try
            {
                using (Process p = new Process())
                {
                    p.StartInfo.Arguments = args;
                    p.StartInfo.FileName = exeName;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.Start();

                    await UpdateThreadId(p.Id);

                    output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return output;
        }

        private async System.Threading.Tasks.Task UpdateThreadId(int threadId)
        {
            try
            {
                ROBIN.Common.Model.CommandsModel<
                    UpdateHdrByIdCommand,
                    ROBIN.Common.Model.StatusModel
                > cmd = new ROBIN.Common.Model.CommandsModel<
                    UpdateHdrByIdCommand,
                    ROBIN.Common.Model.StatusModel
                >();
                cmd.CommandModel = new UpdateHdrByIdCommand
                {
                    uniqueId = _jobId,
                    threadId = threadId.ToString(),
                    module = _module
                };
                cmd.CurrentDateTime = new ROBIN.Infrastructure.MachineDateTime().Now;
                await _mediator.Send(cmd);
                cmd = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
