﻿namespace ARRA.Worker.Helper.Model
{
    public class ETLModel
    {
        public string PackageName { get; set; }
        public string Location { get; set; }
        public string Password { get; set; }
        public bool As32Bit { get; set; }
        public string Path32Bit { get; set; }
        public IList<Parameter> Parameters { get; set; }
    }

    public class Parameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
