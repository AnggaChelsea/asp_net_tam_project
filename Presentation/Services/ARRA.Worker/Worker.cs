using ARRA.Worker.Jobs;
using MediatR;
using ROBIN.Common;
using ROBIN.Common.Enumerations;
using ROBIN.Common.Model;
using ROBIN.GLOBAL.App.Jobs.Commands;
using ROBIN.GLOBAL.App.Jobs.Models;
using ROBIN.GLOBAL.App.Jobs.Queries;

namespace ARRA.Worker
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private Timer _timer;
        private readonly CancellationToken _cancellationToken;
        private readonly IConfiguration _configuration;
        IMediator Mediator;
        private IList<TMP_QUEUE_CTR> _listCtr;
        private Int32 _maxThread;
        private IDateTime _currDate = new ROBIN.Infrastructure.MachineDateTime();
        private bool _allowRun;
        private string _module;
        private string[] _jobTypes;
        private int _treadCounter;

        public Worker(
            ILogger<Worker> logger,
            IConfiguration configuration,
            IApplicationLifetime applicationLifetime,
            IMediator mediator
        )
        {
            _logger = logger;
            _cancellationToken = applicationLifetime.ApplicationStopping;
            _configuration = configuration;
            Mediator = mediator;
            _treadCounter = 0;
            _listCtr = new List<TMP_QUEUE_CTR>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation(
                    "Timed ARRA Service is starting at: {time}",
                    DateTimeOffset.Now
                );

                ApplicationSettings set = new ApplicationSettings(_configuration);
                _module = set.ServiceModule;
                set = null;

                _allowRun = true;

                _maxThread = Convert.ToInt32(
                    _configuration.GetSection("ApplicationSettings:MaxThread").Value
                );
                string jobTypes = Convert.ToString(
                    _configuration.GetSection("ApplicationSettings:JobTypes").Value
                );
                if (!string.IsNullOrEmpty(jobTypes))
                {
                    _jobTypes = jobTypes.Split('#');
                }

                Int32 tickSeconds = Convert.ToInt32(
                    _configuration.GetSection("ApplicationSettings:TimerInSecond").Value
                );

                RunProcess();

                await Task.Delay(TimeSpan.FromSeconds(tickSeconds), stoppingToken);
            }
        }

        private async void RunProcess()
        {
            if (this._allowRun) //prevent duplicate
            {
                try
                {
                    this._allowRun = false;

                    QueriesModel<GetJobHdrByStatusQuery, HeaderListModel> m =
                        new QueriesModel<GetJobHdrByStatusQuery, HeaderListModel>();
                    m.QueryModel = new GetJobHdrByStatusQuery
                    {
                        status = JobStatus.Pending.ToString(),
                        module = _module,
                        jobTypes = _jobTypes
                    };
                    HeaderListModel hdrJob = await Mediator.Send(m);
                    IList<HeaderModel> listJobs = hdrJob.listJobs;
                    hdrJob = null;
                    m = null;
                    this.ResetCounter();

                    foreach (HeaderModel job in listJobs)
                    {
                        job.module = _module;
                        if (!_cancellationToken.IsCancellationRequested)
                        {
                            //special for cancel case.
                            if (job.jobType == QueueJobType.CANCEL_PROCESS.ToString())
                            {
                                QueriesModel<GetJobDtlByHdrQuery, DetailListModel> md =
                                    new QueriesModel<GetJobDtlByHdrQuery, DetailListModel>();
                                md.QueryModel = new GetJobDtlByHdrQuery
                                {
                                    headerId = job.uniqueId,
                                    module = _module
                                };
                                DetailListModel dtlJob = await Mediator.Send(md);
                                IList<DetailModel> jobDetail = dtlJob.listDetails;
                                dtlJob = null;
                                md = null;

                                CommandsModel<UpdateHdrByIdCommand, StatusModel> cmd =
                                    new CommandsModel<UpdateHdrByIdCommand, StatusModel>();
                                cmd.CommandModel = new UpdateHdrByIdCommand
                                {
                                    uniqueId = job.uniqueId,
                                    status = JobStatus.Progress.ToString(),
                                    module = _module
                                };
                                cmd.CurrentDateTime = _currDate.Now;
                                await Mediator.Send(cmd);
                                cmd = null;

                                new Thread(async () =>
                                {
                                    try
                                    {
                                        //_taskQueue.QueueBackgroundWorkItem(async token =>
                                        //{
                                        long queue_id = Convert.ToInt64(
                                            jobDetail
                                                .Where(f => f.paramName == "JOB_QUEUE_ID")
                                                .FirstOrDefault()
                                                .paramValue
                                        );
                                        try
                                        {
                                            await new CancelProcessJob(
                                                Mediator,
                                                job,
                                                jobDetail
                                            ).Run();
                                            //this.RemoveCounter(queue_id);
                                        }
                                        catch (Exception ex)
                                        {
                                            //this.RemoveCounter(queue_id);
                                            _logger.LogError(ex.Message);
                                        }
                                        //});
                                    }
                                    catch (Exception ex)
                                    {
                                        _logger.LogError(ex, $"Error occurred executing"); // {nameof(workItem)}.");
                                    }
                                    jobDetail = null;
                                }).Start();
                            }
                            else if (_treadCounter < _maxThread)
                            {
                                QueriesModel<GetJobDtlByHdrQuery, DetailListModel> md =
                                    new QueriesModel<GetJobDtlByHdrQuery, DetailListModel>();
                                md.QueryModel = new GetJobDtlByHdrQuery
                                {
                                    headerId = job.uniqueId,
                                    module = _module
                                };
                                DetailListModel dtlJob = await Mediator.Send(md);
                                IList<DetailModel> jobDetail = dtlJob.listDetails;
                                dtlJob = null;
                                md = null;

                                if (await this.IsDepend(job, jobDetail)) //is no dependencies.
                                {
                                    jobDetail = null;
                                }
                                else
                                {
                                    CommandsModel<UpdateHdrByIdCommand, StatusModel> cmd =
                                        new CommandsModel<UpdateHdrByIdCommand, StatusModel>();
                                    cmd.CommandModel = new UpdateHdrByIdCommand
                                    {
                                        uniqueId = job.uniqueId,
                                        status = JobStatus.Progress.ToString(),
                                        module = _module
                                    };
                                    cmd.CurrentDateTime = _currDate.Now;
                                    await Mediator.Send(cmd);
                                    cmd = null;

                                    new Thread(async () =>
                                    {
                                        try
                                        {
                                            //_taskQueue.QueueBackgroundWorkItem(async token =>
                                            //{
                                            _treadCounter++;

                                            //string tokenId = Guid.NewGuid().ToString();
                                            //_listCtr.Add(new TMP_QUEUE_CTR { JobType=job.jobType, QueueId=job.uniqueId, RunDate=_currDate.Now, token=tokenId });

                                            try
                                            {
                                                if (job.jobType == QueueJobType.EXPORT.ToString())
                                                {
                                                    await new ExportJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType == QueueJobType.IMPORT.ToString()
                                                )
                                                {
                                                    await new ImportJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.EXPORT_COMPARE.ToString()
                                                )
                                                {
                                                    await new ExportCompareJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.PROCESS_DATA_SOURCE.ToString()
                                                )
                                                {
                                                    await new ProcessDatasourceJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail,
                                                        _configuration
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType == QueueJobType.TRANSFORM.ToString()
                                                )
                                                {
                                                    await new TransformJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail,
                                                        _configuration
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.VALIDATION.ToString()
                                                )
                                                {
                                                    await new ValidationJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail,
                                                        _configuration
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.EXPORT_VALIDATION.ToString()
                                                )
                                                {
                                                    await new ExportValidationJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.GENERATE_TF.ToString()
                                                )
                                                {
                                                    await new GenerateTFJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.GENERATE_TF_SLIK.ToString()
                                                )
                                                {
                                                    await new GenerateTFSLIKJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.RECONCILIATION.ToString()
                                                )
                                                {
                                                    await new ReconciliationJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail,
                                                        _configuration
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.MANUAL_UPLOAD.ToString()
                                                )
                                                {
                                                    await new ManualUploadJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.BI_VALIDATION.ToString()
                                                )
                                                {
                                                    await new BIValidationJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail,
                                                        _configuration
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.UPLOAD_ERROROJK.ToString()
                                                )
                                                {
                                                    await new UploadErrorOJKJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.UPLOAD_DBLOJK.ToString()
                                                )
                                                {
                                                    // UploadDblOJKJob
                                                    await new UploadDblOJKJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.UPLOAD_ERRANTASENA.ToString()
                                                )
                                                {
                                                    // upload error antasena
                                                    await new UploadErrorAntasenaJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.EXPORT_TEMPLATE.ToString()
                                                )
                                                {
                                                    // export excel template
                                                    await new ExportTemplateJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.EXPORT_TEMPLATE_DTL.ToString()
                                                )
                                                {
                                                    // export excel template detail
                                                    await new ExportTemplateDtl(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType
                                                    == QueueJobType.EXPORT_ADHOC.ToString()
                                                )
                                                {
                                                    // export excel template detail
                                                    await new ExportAdhoc(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType == QueueJobType.BACKUP.ToString()
                                                )
                                                {
                                                    // export excel template detail
                                                    await new BackupJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType == QueueJobType.RESTORE.ToString()
                                                )
                                                {
                                                    // export excel template detail
                                                    await new RestoreJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else if (
                                                    job.jobType == QueueJobType.PURGING.ToString()
                                                )
                                                {
                                                    // export excel template detail
                                                    await new PurgingJob(
                                                        Mediator,
                                                        job,
                                                        jobDetail
                                                    ).Run();
                                                }
                                                else //unknow
                                                {
                                                    cmd =
                                                        new CommandsModel<
                                                            UpdateHdrByIdCommand,
                                                            StatusModel
                                                        >();
                                                    cmd.CommandModel = new UpdateHdrByIdCommand
                                                    {
                                                        uniqueId = job.uniqueId,
                                                        status = JobStatus.Failed.ToString(),
                                                        module = _module,
                                                        message = ErrorMessages.UnidentifiedJob
                                                    };
                                                    cmd.CurrentDateTime = _currDate.Now;
                                                    await Mediator.Send(cmd);
                                                    cmd = null;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                _logger.LogError(ex.Message);
                                            }
                                            finally
                                            {
                                                _treadCounter--;
                                                //this.RemoveCounterByToken(tokenId);
                                            }
                                            //});
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.LogError(ex, $"Error occurred executing"); // {nameof(workItem)}.");
                                        }

                                        jobDetail = null;
                                    }).Start();
                                }
                            }
                        }
                    }

                    listJobs = null;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
                finally
                {
                    this._allowRun = true;

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
            }
        }

        private async void ResetCounter()
        {
            try
            {
                QueriesModel<GetRecentJobProgressQuery, ProgressCountModel> m =
                    new QueriesModel<GetRecentJobProgressQuery, ProgressCountModel>();
                m.QueryModel = new GetRecentJobProgressQuery { module = _module };
                ProgressCountModel mCount = await Mediator.Send(m);
                _treadCounter = mCount.count;
                m = null;
                mCount = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveCounter(long queueId)
        {
            TMP_QUEUE_CTR mCtr = _listCtr.Where(f => f.QueueId == queueId).FirstOrDefault();
            if (mCtr != null)
            {
                _listCtr.Remove(mCtr);
            }
            mCtr = null;
        }

        private void RemoveCounterByToken(string token)
        {
            TMP_QUEUE_CTR mCtr = _listCtr.Where(f => f.token == token).FirstOrDefault();
            if (mCtr != null)
            {
                _listCtr.Remove(mCtr);
            }
            mCtr = null;
        }

        private async Task<bool> IsDepend(HeaderModel job, IList<DetailModel> jobDetail)
        {
            if (job.jobType == QueueJobType.PROCESS_DATA_SOURCE.ToString())
            {
                return await new ProcessDatasourceJob(
                    Mediator,
                    job,
                    jobDetail,
                    _configuration
                ).DependenciesChecking();
            }
            else if (job.jobType == QueueJobType.TRANSFORM.ToString())
            {
                return await new TransformJob(
                    Mediator,
                    job,
                    jobDetail,
                    _configuration
                ).DependenciesChecking();
            }
            else if (job.jobType == QueueJobType.MANUAL_UPLOAD.ToString())
            {
                return await new ManualUploadJob(Mediator, job, jobDetail).DependenciesChecking();
            }
            else if (job.jobType == QueueJobType.BI_VALIDATION.ToString())
            {
                return await new BIValidationJob(
                    Mediator,
                    job,
                    jobDetail,
                    _configuration
                ).DependenciesChecking();
            }

            return false;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed ROBIN Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }

    public class TMP_QUEUE_CTR
    {
        public string JobType { get; set; }
        public long QueueId { get; set; }
        public DateTime RunDate { get; set; }
        public string token { get; set; }
    }
}
