using ARRA.Worker;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ROBIN.Infrastructure;
using ROBIN.Persistence;
using Serilog;
using System.Reflection;
using static ARRA.Common.Decrypt;

IHost host = Host.CreateDefaultBuilder(args)
    .UseSystemd()
    .ConfigureLogging((hostContext, config) => { })
    .ConfigureHostConfiguration(config =>
    {
        config.AddJsonFile("appsettings.json", optional: false);
    })
    .ConfigureAppConfiguration(
        (hostContext, config) =>
        {
            config.AddJsonFile("appsettings.json", optional: false);
            config.AddCommandLine(args);
        }
    )
    .ConfigureServices(
        (hostContext, services) =>
        {
            services.AddLogging();

            services.AddHostedService<Worker>();

            services.AddTransient<
                ROBIN.GLOBAL.App.Interfaces.ITextFileConnector,
                TextFileConnector
            >();
            services.AddTransient<ROBIN.GLOBAL.App.Interfaces.IExcelConnector, ExcelConnector>();
            services.AddTransient<
                ROBIN.GLOBAL.App.Interfaces.IWebServiceConnector,
                WebServiceConnector
            >();
            services.AddTransient<
                ROBIN.GLOBAL.App.Interfaces.IAuthenticationService,
                AuthenticationService
            >();
            services.AddTransient<
                ROBIN.GLOBAL.App.Interfaces.IAuthenticationService,
                AuthenticationService
            >();
            services.AddTransient<ROBIN.Common.IDateTime, MachineDateTime>();
            services.AddMediatR(
                typeof(ROBIN.GLOBAL.App.Jobs.Queries.GetJobDtlByHdrQuery).GetTypeInfo().Assembly
            );

            var config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();

            //Initialize Logger
            Log.Logger = new LoggerConfiguration()
             .ReadFrom.Configuration(config)
              .MinimumLevel.Error()
              .CreateLogger();

            // Add DbContext using SQL Server Provider
            services.AddDbContext<GLOBALDbContext>(
                options =>
                    //options.UseSqlServer(
                    //    hostContext.Configuration.GetConnectionString("GLOBAL_ConStr")
                    //)
                    options.UseSqlServer(DecryptText(hostContext.Configuration.GetConnectionString("GLOBAL_ConStr"), hostContext.Configuration))
                //,
                //ServiceLifetime.Scoped
                ,
                ServiceLifetime.Transient
            );
        }
    )
    .UseSerilog()
    .Build();

await host.RunAsync();
