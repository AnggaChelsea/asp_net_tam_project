﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using ARRA.Common.Model;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ARRA.Common;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.IO.Compression;
using Microsoft.Data.SqlClient;
using System.Text;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;

namespace ARRA.Infrastructure
{
    public class ExcelConnector : IExcelConnector
    {
        #region Helper
        private static int CellReferenceToIndex(Cell cell)
        {
            if (cell.CellReference == null) return -1;
            int index = 0;
            string reference = cell.CellReference.ToString().ToUpper();
            bool isFirst = true;
            foreach (char ch in reference)
            {
                if (Char.IsLetter(ch))
                {
                    int value = (int)ch - (int)'A';
                    if (isFirst)
                    {
                        index = value;
                        isFirst = false;
                    }
                    else//handle 2 character 
                    {
                        index = ((index + 1) * 26) + value;
                    }
                }
                else
                    return index;
            }
            return index;
        }

        private string GetExcelTypeAttr(string ctlType)
        {
            string typeAttr = "";
            if (ctlType == ControlType.DATE.ToString() ||
                ctlType == ControlType.DATETIME.ToString())
            {
                //typeAttr = "d";
                typeAttr = "str";
            }
            else if (ctlType == ControlType.NUMBER.ToString() ||
                ctlType == ControlType.MONEY.ToString() ||
                ctlType == ControlType.PERCENTAGE.ToString()
                )
            {
                typeAttr = "n";
            }
            else
            {
                typeAttr = "str";
            }

            #region xml type
            //all attr type
            //cellvalues.
            //String
            //SharedString
            //Number
            //InlineString
            //Error
            //Date
            //Boolean
            #endregion
            return typeAttr;
        }

        private string FormatValue(string ctlType, object value)
        {
            string result = "";
            try
            {
                if (ctlType == ControlType.DATE.ToString())
                {
                    if (value != DBNull.Value && value != null)
                    {
                        //result = Convert.ToDateTime(value).ToString("M/d/yyyy");
                        result = Convert.ToDateTime(value).ToString("yyyy-MM-dd");
                    }
                }
                else
                {
                    result = Convert.ToString(value == DBNull.Value || value == null ? "" : value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                result = "";
            }

            return result;
        }

        #endregion

        private void CustomValidation(IDbConnection con, IConfiguration conf, string formCode,
            IList<FormDetail> listColumn, ref List<CellValueMap> values, string uid, string action,
            string parentTable, string parentKey, string parentFkKey,
            bool? CheckBIBranch, bool? CheckLocalBranch, List<string> listAuthBranch)
        {
            //special case for same screen.
            try
            {
                if (formCode.ToUpper() == ARRA.Common.Enumerations.FormCodeCollection.F_GBUSR.ToString()) //screen user
                {
                    if (action == ScreenAction.REMOVE.ToString())
                    {
                        CellValueMap valActive = values.Where(f => f.field.ToUpper() == "ACTIVE").FirstOrDefault();
                        if (valActive == null)
                        {
                            throw new Exception(ErrorMessages.ProvideActiveFlag);
                        }
                        else
                        {
                            if (valActive.value != null)
                            {
                                if (valActive.value.ToLower() == "true" || valActive.value.ToLower() == "1")
                                {
                                    throw new Exception(ErrorMessages.UserActiveDelete);
                                }
                            }

                            valActive = null;
                        }
                    }
                    else
                    {
                        ApplicationSettings sett = new ApplicationSettings(conf);
                        string pass = values.Where(f => f.field == "PASSWD").Select(c => c.value).FirstOrDefault();
                        string cpass = values.Where(f => f.field == "PASSWD_CRFM").Select(c => c.value).FirstOrDefault();
                        string passBefore = "";
                        if (uid != "" && uid != "0" & uid != null) //update operation
                        {
                            DynamicParameters p = new DynamicParameters();
                            p.Add("@UID", Convert.ToInt32(uid));
                            passBefore = Convert.ToString(con.ExecuteScalar(" SELECT PASSWD FROM MS_USER WHERE UID = @UID ", p));

                            con.Execute("UPDATE MS_USER SET LOCK_CTR=0 WHERE UID = @UID", p);

                            p = null;
                        }

                        string IS_LDAP = sett.ISLDAP;

                        if (IS_LDAP != "1")
                        {
                            if (string.IsNullOrEmpty(pass))
                            {
                                throw new Exception(ErrorMessages.PasswordEmpty);
                            }
                            else if (pass != passBefore || cpass != passBefore)
                            {
                                if (!pass.Equals(cpass))
                                {
                                    throw new Exception(ErrorMessages.PasswordNotMatch);
                                }
                                else if (pass.Length < sett.PasswordMinLength)
                                {
                                    throw new Exception(string.Format(ErrorMessages.PasswordLength, sett.PasswordMinLength.ToString()));
                                }
                                else if (!Regex.IsMatch(pass, "[A-Z]") && sett.PasswordUppercase)
                                {
                                    throw new Exception(ErrorMessages.PasswordUppercaseLetter);
                                }
                                else if (!Regex.IsMatch(pass, "[a-z]") && sett.PasswordLowercase)
                                {
                                    throw new Exception(ErrorMessages.PasswordLowercaseLetter);
                                }
                                else if (!Regex.IsMatch(pass, "[0-9]") && sett.PasswordNumeric)
                                {
                                    throw new Exception(ErrorMessages.PasswordDigit);
                                }
                                else if (!Regex.IsMatch(pass, "[^a-zA-Z0-9]") && sett.PasswordSpecialChar)
                                {
                                    throw new Exception(ErrorMessages.PasswordSpecialCharacter);
                                }
                                else //valid
                                {
                                    AuthenticationService auth = new AuthenticationService(conf);
                                    pass = auth.Encrypt(pass);
                                    cpass = pass;
                                    CellValueMap valupdate = values.Where(f => f.field == "PASSWD").FirstOrDefault();
                                    if (valupdate != null) valupdate.value = pass;
                                    valupdate = values.Where(f => f.field == "PASSWD_CRFM").FirstOrDefault();
                                    if (valupdate != null) valupdate.value = cpass;
                                    auth = null;
                                }
                            }
                        }

                        sett = null;
                    }
                }

                if (!string.IsNullOrEmpty(parentTable) && !string.IsNullOrEmpty(parentKey) && action == ScreenAction.ADD.ToString())
                {
                    object val = values.Where(f => f.field == parentFkKey).Select(c => c.value).FirstOrDefault();
                    if (val != null)
                    {
                        DynamicParameters p = new DynamicParameters();
                        p.Add("@KEY", val);
                        object returnVal = con.ExecuteScalar(" SELECT TOP 1 1 FROM " + parentTable + " WHERE " + parentKey + " = @KEY ", p);
                        p = null;

                        if (returnVal == null)
                        {
                            throw new Exception(string.Format(ErrorMessages.InvalidKeyValue, val));
                        }
                    }

                }

                if (CheckBIBranch == true)
                {
                    string branch = values.Where(f => f.field.ToUpper() == "RG_BRANCH").Select(c => c.value).FirstOrDefault();
                    if (string.IsNullOrEmpty(branch))
                    {
                        throw new Exception(ErrorMessages.BIBranchRequired);
                    }
                    else
                    {
                        if (!listAuthBranch.Contains(branch))
                        {
                            throw new Exception(string.Format(ErrorMessages.BIBranchAuth, branch));
                        }
                    }
                }
                else if (CheckLocalBranch == true)
                {
                    string branch = values.Where(f => f.field.ToUpper() == "BRANCH").Select(c => c.value).FirstOrDefault();
                    if (string.IsNullOrEmpty(branch))
                    {
                        throw new Exception(ErrorMessages.BranchRequired);
                    }
                    else
                    {
                        if (!listAuthBranch.Contains(branch))
                        {
                            throw new Exception(string.Format(ErrorMessages.BranchAuth, branch));
                        }
                    }
                }

                // add 20201001, rizwan
                // Check data reporting date sesuai bulan berjalan atau tidak
                if (!this.IsDataCurrentReportingDate(formCode, con, values))
                {
                    throw new Exception("Invalid Reporting Date.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsDataCurrentReportingDate(string formCode, IDbConnection con, List<CellValueMap> values)
        {
            if (!IsImportCheckReportingDate(con))
                return true;

            bool isCurrentReportingDate = true;

            IList<string> listFormCD = new List<string>();

            using (IDataReader rd = GetListFormCode(con))
            {
                while (rd.Read())
                {
                    listFormCD.Add(rd["PAR_VALUE"].ToString());
                }
            }

            if (listFormCD.Contains(formCode))
            {
                string reportDt = values.Where(f => f.field.ToUpper() == "REPORT_DT").Select(c => c.value).FirstOrDefault();

                double doubleVal;
                if (double.TryParse(reportDt, out doubleVal))
                {
                    if (reportDt.ToString().Length != 8)
                    {
                        reportDt = DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd");
                    }
                }

                int countDt = Convert.ToInt32(con.ExecuteScalar(string.Format("SELECT COUNT(1) " +
                                                        "FROM ARRA_TIMESHEET..MS_REPORTING_DATE RD " +
                                                        "JOIN ARRA_TIMESHEET..MS_FORM_HDR MS ON MS.MODULE = RD.MODULE " +
                                                        "    AND MS.PERIOD_TP = RD.PERIOD_TP " +
                                                        "WHERE MS.FORM_CD = '{0}' " +
                                                        "    AND RD.REPORTING_DT = '{1}'", formCode, reportDt)));

                if (countDt == 0)
                    isCurrentReportingDate = false;
            }

            return isCurrentReportingDate;
        }

        private IDataReader GetListFormCode(IDbConnection con)
        {
            return con.ExecuteReader(string.Format("SELECT PAR_VALUE " +
                                                    "FROM ARRA_TIMESHEET..MS_SYSTEM_PARAM_DTL " +
                                                    "WHERE PAR_CD = 'gb214' " +
                                                    "    AND PAR_VAL_MAP = 1"));
        }

        private bool IsImportCheckReportingDate(IDbConnection con)
        {
            return Convert.ToBoolean(con.ExecuteScalar(string.Format("SELECT ISNULL(PAR_VALUE, 'false') " +
                                                                    "FROM ARRA_TIMESHEET..MS_SYSTEM_PARAM_HDR " +
                                                                    "WHERE MODULE = 'global' " +
                                                                    "    AND PAR_CD = 'GB214'")));
        }

        //public async Task<long> ImportData(IDbConnection con, IConfiguration conf, UserIdentityModel user, 
        //    string tableName, IList<FormDetail> listColumn, string fileFullPath, long logId, 
        //    string formCode, string fieldPK, string detailTableName, string detailFKField,
        //    string parentTable, string parentKey, string parentFkKey, 
        //    bool? CheckBIBranch,bool? CheckLocalBranch, List<string> listAuthBranch)
        //{
        //    long totalRecord = 0;
        //    try
        //    {
        //        using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileFullPath, false))
        //        {
        //            WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
        //            Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Sheet1").FirstOrDefault();
        //            if (sheet == null)
        //            {
        //                throw new Exception("Excel sheet not found (sheet name must using \"Sheet1\")");
        //            }

        //            WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
        //            var stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

        //            using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
        //            {
        //                try
        //                {
        //                    int row = 0;
        //                    string text;
        //                    int col = 0;
        //                    string uid = "";
        //                    DynamicParameters p = new DynamicParameters();
        //                    string qryIns = "";
        //                    string qryInsVal = "";
        //                    string qryUpd = "";
        //                    string curDate = new MachineDateTime().Now.ToString();
        //                    string insert = "";
        //                    string update = "";
        //                    string delete = "";
        //                    int uidIdx = 0;
        //                    int refcell = 0;
        //                    long line = 1;
        //                    long errCtr = 0;
        //                    List<CellValueMap> listOri = new List<CellValueMap>();
        //                    List<CellValueMap> listValue = new List<CellValueMap>();
        //                    List<CalculateNeracaLabaRugi> listNeracaLabaRugi = new List<CalculateNeracaLabaRugi>();
        //                    decimal dec_num;
        //                    while (reader.Read())
        //                    {
        //                        if (reader.ElementType == typeof(Row))
        //                        {
        //                            if (row == 1)
        //                            {
        //                                if (listOri.Where(f => f.field.ToUpper() == "UID").Count() == 0)
        //                                {
        //                                    throw new Exception(ErrorMessages.ReferenceNotFound);
        //                                }
        //                            }

        //                            if ((col > 0 || refcell > 0) && row > 1) //is not header and contains value
        //                            {
        //                                try
        //                                {
        //                                    bool isDelete = false;
        //                                    CellValueMap mapDelFlag = listValue.Where(f => f.field == "IS_DELETE").FirstOrDefault();
        //                                    if (mapDelFlag != null)
        //                                    {
        //                                        if (mapDelFlag.value != null)
        //                                        {
        //                                            if (mapDelFlag.value.ToUpper() == "Y")
        //                                            {
        //                                                isDelete = true;
        //                                            }
        //                                        }
        //                                        mapDelFlag = null;
        //                                    }

        //                                    string action = "";
        //                                    if (isDelete)
        //                                    {
        //                                        action = ScreenAction.REMOVE.ToString();
        //                                    }
        //                                    else if (uid == "" || uid == "0")
        //                                    {
        //                                        action = ScreenAction.ADD.ToString();
        //                                    }
        //                                    else
        //                                    {
        //                                        action = ScreenAction.EDIT.ToString();
        //                                    }

        //                                    //custom validation for same form.
        //                                    this.CustomValidation(con, conf, formCode, listColumn, ref listValue, uid, action, parentTable, parentKey, parentFkKey,CheckBIBranch,CheckLocalBranch,listAuthBranch);

        //                                    // list report dt & branch
        //                                    if (IsNeracaLabaRugi(formCode.ToLower()))
        //                                    {
        //                                        AddListBranch(listValue, listNeracaLabaRugi);
        //                                    }

        //                                    insert = ""; update = "";delete = "";
        //                                    qryIns = ""; qryInsVal = ""; qryUpd = "";

        //                                    if (!isDelete)
        //                                    {
        //                                        p = new DynamicParameters();
        //                                        foreach (FormDetail dd in listColumn)
        //                                        {
        //                                            if (dd.EdrShow == true)
        //                                            {
        //                                                CellValueMap map = listValue.Where(f => f.field == dd.FieldName).FirstOrDefault();
        //                                                if (map != null)
        //                                                {
        //                                                    if (uid == "" || uid == "0")
        //                                                    {
        //                                                        qryIns += dd.FieldName + ",";
        //                                                        qryInsVal += "@" + dd.FieldName + ",";
        //                                                        if (dd.EdrControlType == ControlType.DATE.ToString())
        //                                                        {
        //                                                            double doubleVal;
        //                                                            if (double.TryParse(map.value, out doubleVal))
        //                                                            {
        //                                                                if (map.value.ToString().Length == 8)
        //                                                                {
        //                                                                    p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                                }
        //                                                                else
        //                                                                {
        //                                                                    p.Add("@" + dd.FieldName, map.value == null ? null : DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd"));
        //                                                                }
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                // comment 20200520, rizwan bug ' menjadi ''
        //                                                                //p.Add("@" + dd.FieldName, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
        //                                                                p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            // comment 20200520, rizwan bug ' menjadi ''
        //                                                            //p.Add("@" + dd.FieldName, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
        //                                                            p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        if (dd.EdrReadonly == null || dd.EdrReadonly == false)
        //                                                        {
        //                                                            qryUpd += dd.FieldName + "=@" + dd.FieldName + ",";

        //                                                            if (dd.EdrControlType == ControlType.DATE.ToString())
        //                                                            {
        //                                                                double doubleVal;

        //                                                                if (double.TryParse(map.value, out doubleVal))
        //                                                                {
        //                                                                    if (map.value.ToString().Length == 8)
        //                                                                    {
        //                                                                        p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                                    }
        //                                                                    else
        //                                                                    {
        //                                                                        p.Add("@" + dd.FieldName, map.value == null ? null : DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd"));
        //                                                                    }
        //                                                                }
        //                                                                else
        //                                                                {
        //                                                                    //p.Add("@" + dd.FieldName, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
        //                                                                    p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                                }
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                //p.Add("@" + dd.FieldName, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
        //                                                                p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                map = null;
        //                                            } //allow edit
        //                                        }

        //                                        //start operation
        //                                        if (qryIns != "")
        //                                        {
        //                                            insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
        //                                            await con.ExecuteAsync(insert, p);
        //                                        }
        //                                        else if (qryUpd != "")
        //                                        {
        //                                            update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE UID=" + uid;
        //                                            await con.ExecuteAsync(update, p);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (uid == "" || uid == "0")
        //                                        {
        //                                            throw new Exception("Please provide reference id to delete");
        //                                        }
        //                                        else
        //                                        {
        //                                            if (!string.IsNullOrEmpty(detailTableName) && !string.IsNullOrEmpty(detailFKField) && !string.IsNullOrEmpty(fieldPK))
        //                                            {
        //                                                CellValueMap pkfield = listValue.Where(f => f.field == fieldPK).FirstOrDefault();
        //                                                if (pkfield != null)
        //                                                {
        //                                                    if (pkfield.value == null)
        //                                                    {
        //                                                        throw new Exception("Delete detail, key value not found");
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        p = new DynamicParameters();
        //                                                        p.Add("@fk", pkfield.value);
        //                                                        delete = "DELETE FROM " + detailTableName + " WHERE " + detailFKField + "=@fk";
        //                                                        await con.ExecuteAsync(delete,p);
        //                                                        p = null;
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    throw new Exception("Delete detail, key not found");
        //                                                }
        //                                            }

        //                                            delete = "DELETE FROM " + tableName + " WHERE UID=" + uid;
        //                                            await con.ExecuteAsync(delete);
        //                                        }
        //                                    }

        //                                    if (line % 1000 == 0)
        //                                    {
        //                                        p = new DynamicParameters();
        //                                        p.Add("@UID", logId);
        //                                        p.Add("@TOTAL_PROCEED", line);
        //                                        p.Add("@TOTAL_FAILED", errCtr);
        //                                        await con.ExecuteAsync("UPDATE TR_IMPORT_FILE_LIST_HDR SET TOTAL_PROCEED=@TOTAL_PROCEED,TOTAL_FAILED=@TOTAL_FAILED WHERE UID=@UID ", p);
        //                                    }

        //                                    p = null;
        //                                }
        //                                catch (SqlException ex)
        //                                {
        //                                    p = new DynamicParameters();
        //                                    p.Add("@HDR_ID", logId);
        //                                    p.Add("@ROW_NUM", line + 1);
        //                                    p.Add("@ERR_DESC", ARRA.Common.ErrorMessages.GetSQLErrorMessage(ex.Number, ex.Message));
        //                                    p.Add("@CREATED_BY", user.UserId);
        //                                    p.Add("@CREATED_DT", curDate);
        //                                    p.Add("@CREATED_HOST", user.Host);
        //                                    await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
        //                                    p = null;
        //                                    errCtr++;
        //                                }
        //                                catch(Exception ex)
        //                                {
        //                                    p = new DynamicParameters();
        //                                    p.Add("@HDR_ID", logId);
        //                                    p.Add("@ROW_NUM", line + 1);
        //                                    p.Add("@ERR_DESC", ex.Message);
        //                                    p.Add("@CREATED_BY", user.UserId);
        //                                    p.Add("@CREATED_DT", curDate);
        //                                    p.Add("@CREATED_HOST", user.Host);
        //                                    await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
        //                                    p = null;
        //                                    errCtr++;
        //                                }

        //                                line++;
        //                            }

        //                            listOri.ForEach(x => x.value = null);
        //                            listValue = listOri;
        //                            col = 0;
        //                            uid = "";
        //                            refcell = 0;
        //                            row++;
        //                        }

        //                        if (reader.ElementType == typeof(Cell))
        //                        {                                                                       
        //                            var cell = (Cell)reader.LoadCurrentElement();
        //                            dec_num = 0;
        //                            if (cell.CellValue != null)
        //                            {
        //                                Decimal.TryParse(cell.CellValue.Text, System.Globalization.NumberStyles.Float, null, out dec_num);

        //                                if (dec_num != 0)
        //                                {
        //                                    text = dec_num.ToString();
        //                                }
        //                                else {
        //                                    text = cell.CellValue.Text;
        //                                }                                        
        //                            }
        //                            else
        //                            {
        //                                Decimal.TryParse(cell.InnerText, System.Globalization.NumberStyles.Float, null, out dec_num);

        //                                if (dec_num != 0)
        //                                {
        //                                    text = dec_num.ToString();
        //                                }
        //                                else
        //                                {
        //                                    text = cell.InnerText;
        //                                }

        //                                //text = cell.InnerText;
        //                            }


        //                            if (cell.DataType != null)
        //                            {
        //                                if (cell.DataType.Value == CellValues.SharedString)
        //                                {
        //                                    if (stringTable != null)
        //                                    {
        //                                        SharedStringItem ssi = workbookPart.SharedStringTablePart
        //                                            .SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
        //                                        text = ssi.Text.Text;
        //                                    }
        //                                }
        //                            }

        //                            refcell = CellReferenceToIndex(cell); //get ref index.

        //                            if ((refcell == -1 ? col : refcell) == uidIdx) //uid
        //                            {
        //                                uid = text;
        //                            }

        //                            if (row == 1)
        //                            {
        //                                FormDetail d = listColumn.Where(f => f.GrdColumnName.ToUpper() == text.ToUpper()).FirstOrDefault();
        //                                if (d == null)
        //                                {
        //                                    if (text.ToLower() == "is delete")
        //                                    {
        //                                        listOri.Add(new CellValueMap { field = "IS_DELETE", value = null });
        //                                    }
        //                                    else
        //                                    {
        //                                        throw new Exception("Column " + text + " Not Found, Please check your excel format.");
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (d.FieldName == "UID")
        //                                    {
        //                                        uidIdx = refcell == -1 ? col : refcell;
        //                                    }

        //                                    listOri.Add(new CellValueMap { field = d.FieldName, value = null });
        //                                }
        //                                d = null;
        //                            }

        //                            if (row > 1) //row data.
        //                            {
        //                                if (refcell == -1)
        //                                {
        //                                    if (listValue.Count > col) //prevent data column more than header.
        //                                    {
        //                                        listValue[col].value = text == "" ? null : text;
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (listValue.Count > refcell) //prevent data column more than header.
        //                                    {
        //                                        listValue[refcell].value = text == "" ? null : text;
        //                                    }
        //                                }

        //                            }

        //                            col++;
        //                        }
        //                    }

        //                    // jalanin kalkulasi kalau form neraca atau laba rugi
        //                    if(IsNeracaLabaRugi(formCode.ToLower()))
        //                    {
        //                        var dst = listNeracaLabaRugi.GroupBy(o => new { o.ReportDT, o.Branch }).Select(g => g.First()).ToList();
        //                        foreach (var item in dst)
        //                        {
        //                            ExecuteAutoCalculation(con, formCode.ToLower(), item);
        //                        }
        //                    }

        //                    totalRecord = line - 1;

        //                }
        //                catch (Exception ex)
        //                {
        //                    throw ex;
        //                }
        //                finally
        //                {
        //                    reader.Close();
        //                }
        //            }

        //            spreadsheetDocument.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return totalRecord;
        //}

        public async Task<long> ImportData(IDbConnection con, IConfiguration conf, UserIdentityModel user,
            string tableName, IList<FormDetail> listColumn, string fileFullPath, long logId,
            string formCode, string fieldPK, string detailTableName, string detailFKField,
            string parentTable, string parentKey, string parentFkKey,
            bool? CheckBIBranch, bool? CheckLocalBranch, List<string> listAuthBranch)
        {
            long totalRecord = 0;
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileFullPath, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                    Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Sheet1").FirstOrDefault();
                    if (sheet == null)
                    {
                        throw new Exception("Excel sheet not found (sheet name must using \"Sheet1\")");
                    }

                    // ADD 20210120 BY RIZWAN
                    // ADD UPDATE DELETE BY FORMKEY
                    // 1. GET FORMKEY => SYSPARDETAIL
                    int countExistingDt = 0;
                    IList<string> listFormKey = new List<string>();
                    using (IDataReader reader = con.ExecuteReader(string.Format("SELECT PAR_VAL_MAP " +
                                                    "FROM ARRA_TIMESHEET..MS_SYSTEM_PARAM_DTL " +
                                                    "WHERE PAR_CD = 'GB200' " +
                                                    "    AND PAR_VALUE = '{0}'", formCode)))
                    {
                        while (reader.Read())
                        {
                            listFormKey.Add(Convert.ToString(reader["PAR_VAL_MAP"]));
                        }
                    }
                    // =================================================================

                    WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                    var stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
                    {
                        try
                        {
                            int row = 0;
                            string text;
                            int col = 0;
                            string uid = "";
                            DynamicParameters p = new DynamicParameters();
                            string qryIns = "";
                            string qryInsVal = "";
                            string qryUpd = "";
                            string curDate = new MachineDateTime().Now.ToString();
                            string insert = "";
                            string update = "";
                            string delete = "";
                            int uidIdx = 0;
                            int refcell = 0;
                            long line = 1;
                            long errCtr = 0;
                            List<CellValueMap> listOri = new List<CellValueMap>();
                            List<CellValueMap> listValue = new List<CellValueMap>();
                            List<CalculateNeracaLabaRugi> listNeracaLabaRugi = new List<CalculateNeracaLabaRugi>();
                            while (reader.Read())
                            {
                                if (reader.ElementType == typeof(Row))
                                {
                                    if (row == 1)
                                    {
                                        // Check field reference exists or not
                                        if (listFormKey.Count > 0)
                                        {
                                            foreach (var item in listFormKey)
                                            {
                                                if (listOri.Where(f => f.field.ToUpper() == item.ToString()).Count() == 0)
                                                {
                                                    throw new Exception(ErrorMessages.ReferenceNotFound);
                                                }
                                            }
                                        }
                                        else if (listOri.Where(f => f.field.ToUpper() == "UID").Count() == 0)
                                        {
                                            throw new Exception(ErrorMessages.ReferenceNotFound);
                                        }
                                    }

                                    if ((col > 0 || refcell > 0) && row > 1) //is not header and contains value
                                    {
                                        try
                                        {
                                            // ADD 20210120 BY RIZWAN
                                            // ADD UPDATE DELETE BY FORMKEY
                                            // 2. GET VALUE FROM LISTVALUE BY SYSPARDTL FORMKEY
                                            Dictionary<string, string> keyValue = new Dictionary<string, string>();
                                            if (listFormKey.Count > 0)
                                            {
                                                foreach (var item in listFormKey)
                                                {
                                                    CellValueMap mapkeyValue = listValue.Where(f => f.field == item.ToString()).FirstOrDefault();
                                                    keyValue.Add(item.ToString(), mapkeyValue.value);
                                                }
                                            }
                                            // =================================================================

                                            bool isDelete = false;
                                            CellValueMap mapDelFlag = listValue.Where(f => f.field == "IS_DELETE").FirstOrDefault();
                                            if (mapDelFlag != null)
                                            {
                                                if (mapDelFlag.value != null)
                                                {
                                                    if (mapDelFlag.value.ToUpper() == "Y")
                                                    {
                                                        isDelete = true;
                                                    }
                                                }
                                                mapDelFlag = null;
                                            }

                                            string action = "";
                                            if (isDelete)
                                            {
                                                action = ScreenAction.REMOVE.ToString();
                                            }
                                            else if (uid == "" || uid == "0")
                                            {
                                                action = ScreenAction.ADD.ToString();
                                            }
                                            else
                                            {
                                                action = ScreenAction.EDIT.ToString();
                                            }

                                            // ADD 20210120 BY RIZWAN
                                            // ADD UPDATE DELETE BY FORMKEY
                                            // 3. IF FORMKEY NOT NULL, CONDITION ACTION CHECK TO DATABASE
                                            if (listFormKey.Count > 0)
                                            {
                                                countExistingDt = 0;
                                                string whereCondition = string.Empty;

                                                foreach (var item in listFormKey)
                                                {
                                                    whereCondition += " " + item + " = '" + keyValue[item] + "' AND";
                                                }

                                                countExistingDt = Convert.ToInt32(con.ExecuteScalar(string.Format("SELECT COUNT(1) " +
                                                                    "FROM {0} WHERE {1} ", tableName, whereCondition.Substring(0, whereCondition.Length - 3))));

                                                if (isDelete)
                                                {
                                                    action = ScreenAction.REMOVE.ToString();
                                                }
                                                else if (countExistingDt == 0)
                                                {
                                                    action = ScreenAction.ADD.ToString();
                                                }
                                                else
                                                {
                                                    action = ScreenAction.EDIT.ToString();
                                                }
                                            }
                                            // =================================================================

                                            //custom validation for same form.
                                            this.CustomValidation(con, conf, formCode, listColumn, ref listValue, uid, action, parentTable, parentKey, parentFkKey, CheckBIBranch, CheckLocalBranch, listAuthBranch);

                                            //list report dt & branch
                                            if (IsNeracaLabaRugi(formCode.ToLower()))
                                            {
                                                AddListBranch(listValue, listNeracaLabaRugi);
                                            }

                                            insert = ""; update = ""; delete = "";
                                            qryIns = ""; qryInsVal = ""; qryUpd = "";

                                            if (!isDelete)
                                            {
                                                // penjagaan line kosong excel, 20210120, rizwan
                                                string emptyLine = string.Empty;

                                                p = new DynamicParameters();
                                                listColumn = (listFormKey.Count > 0) ? listColumn.Where(a => a.FieldName != "UID").ToList() : listColumn;
                                                foreach (FormDetail dd in listColumn)
                                                {
                                                    if (dd.EdrShow == true)
                                                    {
                                                        CellValueMap map = listValue.Where(f => f.field == dd.FieldName).FirstOrDefault();
                                                        if (map != null)
                                                        {
                                                            // ADD 20210120 BY RIZWAN
                                                            // ADD UPDATE DELETE BY FORMKEY
                                                            // 4. GANTI PENGECEKAN BY action == ScreenAction.ADD.ToString()
                                                            if (action == ScreenAction.ADD.ToString())
                                                            //if (uid == "" || uid == "0")
                                                            {
                                                                qryIns += dd.FieldName + ",";
                                                                qryInsVal += "@" + dd.FieldName + ",";
                                                                if (dd.EdrControlType == ControlType.DATE.ToString())
                                                                {
                                                                    double doubleVal;
                                                                    if (double.TryParse(map.value, out doubleVal))
                                                                    {
                                                                        if (map.value.ToString().Length == 8)
                                                                        {
                                                                            p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                        }
                                                                        else
                                                                        {
                                                                            p.Add("@" + dd.FieldName, map.value == null ? null : DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd"));
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        // comment 20200520, rizwan bug ' menjadi ''
                                                                        //p.Add("@" + dd.FieldName, map.value == null ? null : ROBIN.Common.Utility.SafeSqlString(map.value));
                                                                        p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    // comment 20200520, rizwan bug ' menjadi ''
                                                                    //p.Add("@" + dd.FieldName, map.value == null ? null : ROBIN.Common.Utility.SafeSqlString(map.value));
                                                                    p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                }

                                                                emptyLine += map.value == null ? string.Empty : map.value;
                                                            }
                                                            else
                                                            {
                                                                if (dd.EdrReadonly == null || dd.EdrReadonly == false)
                                                                {
                                                                    qryUpd += dd.FieldName + "=@" + dd.FieldName + ",";

                                                                    if (dd.EdrControlType == ControlType.DATE.ToString())
                                                                    {
                                                                        double doubleVal;

                                                                        if (double.TryParse(map.value, out doubleVal))
                                                                        {
                                                                            if (map.value.ToString().Length == 8)
                                                                            {
                                                                                p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                            }
                                                                            else
                                                                            {
                                                                                p.Add("@" + dd.FieldName, map.value == null ? null : DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd"));
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            //p.Add("@" + dd.FieldName, map.value == null ? null : ROBIN.Common.Utility.SafeSqlString(map.value));
                                                                            p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        //p.Add("@" + dd.FieldName, map.value == null ? null : ROBIN.Common.Utility.SafeSqlString(map.value));
                                                                        p.Add("@" + dd.FieldName, map.value == null ? null : map.value);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        map = null;
                                                    } //allow edit
                                                }

                                                //start operation
                                                if (qryIns != "")
                                                {
                                                    // penjagaan line kosong excel, 20210120, rizwan
                                                    if (emptyLine.Trim() != string.Empty)
                                                    {
                                                        insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                                        await con.ExecuteAsync(insert, p);
                                                    }
                                                    else
                                                    {
                                                        line--;
                                                    }
                                                }
                                                else if (qryUpd != "")
                                                {
                                                    // ADD 20210120 BY RIZWAN
                                                    // ADD UPDATE DELETE BY FORMKEY
                                                    // 5. JIKA FORMKEY NOT NULL, KONDISI UPDATE BY FORMKEY
                                                    if (listFormKey.Count > 0)
                                                    {
                                                        string whereCondition = string.Empty;

                                                        foreach (var item in listFormKey)
                                                        {
                                                            whereCondition += " " + item + " = '" + keyValue[item] + "' AND";
                                                        }

                                                        update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE " + whereCondition.Substring(0, whereCondition.Length - 3);
                                                    }
                                                    else
                                                    {
                                                        update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE UID=" + uid;
                                                    }

                                                    await con.ExecuteAsync(update, p);
                                                }
                                            }
                                            else
                                            {
                                                // ADD 20210120 BY RIZWAN
                                                // ADD UPDATE DELETE BY FORMKEY
                                                // 6. JIKA FORMKEY NOT NULL, KONDISI DELETE BY FORMKEY
                                                //if (uid == "" || uid == "0")
                                                if (listFormKey.Count == 0 && (uid == "" || uid == "0"))
                                                {
                                                    throw new Exception("Please provide reference id to delete");
                                                }
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(detailTableName) && !string.IsNullOrEmpty(detailFKField) && !string.IsNullOrEmpty(fieldPK))
                                                    {
                                                        CellValueMap pkfield = listValue.Where(f => f.field == fieldPK).FirstOrDefault();
                                                        if (pkfield != null)
                                                        {
                                                            if (pkfield.value == null)
                                                            {
                                                                throw new Exception("Delete detail, key value not found");
                                                            }
                                                            else
                                                            {
                                                                p = new DynamicParameters();
                                                                p.Add("@fk", pkfield.value);
                                                                delete = "DELETE FROM " + detailTableName + " WHERE " + detailFKField + "=@fk";
                                                                await con.ExecuteAsync(delete, p);
                                                                p = null;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            throw new Exception("Delete detail, key not found");
                                                        }
                                                    }

                                                    // ADD 20210120 BY RIZWAN
                                                    // ADD UPDATE DELETE BY FORMKEY
                                                    // 7. JIKA FORMKEY NOT NULL, KONDISI DELETE BY FORMKEY
                                                    if (listFormKey.Count > 0)
                                                    {
                                                        string whereCondition = string.Empty;

                                                        foreach (var item in listFormKey)
                                                        {
                                                            whereCondition += " " + item + " = '" + keyValue[item] + "' AND";
                                                        }

                                                        delete = "DELETE FROM " + tableName + " WHERE " + whereCondition.Substring(0, whereCondition.Length - 3);
                                                    }
                                                    else
                                                    {
                                                        delete = "DELETE FROM " + tableName + " WHERE UID=" + uid;
                                                    }

                                                    await con.ExecuteAsync(delete);
                                                }
                                            }

                                            if (line % 1000 == 0)
                                            {
                                                p = new DynamicParameters();
                                                p.Add("@UID", logId);
                                                p.Add("@TOTAL_PROCEED", line);
                                                p.Add("@TOTAL_FAILED", errCtr);
                                                await con.ExecuteAsync("UPDATE TR_IMPORT_FILE_LIST_HDR SET TOTAL_PROCEED=@TOTAL_PROCEED,TOTAL_FAILED=@TOTAL_FAILED WHERE UID=@UID ", p);
                                            }

                                            p = null;
                                        }
                                        catch (SqlException ex)
                                        {
                                            p = new DynamicParameters();
                                            p.Add("@HDR_ID", logId);
                                            p.Add("@ROW_NUM", line + 1);
                                            p.Add("@ERR_DESC", ARRA.Common.ErrorMessages.GetSQLErrorMessage(ex.Number, ex.Message));
                                            p.Add("@CREATED_BY", user.UserId);
                                            p.Add("@CREATED_DT", curDate);
                                            p.Add("@CREATED_HOST", user.Host);
                                            await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                            p = null;
                                            errCtr++;
                                        }
                                        catch (Exception ex)
                                        {
                                            p = new DynamicParameters();
                                            p.Add("@HDR_ID", logId);
                                            p.Add("@ROW_NUM", line + 1);
                                            p.Add("@ERR_DESC", ex.Message);
                                            p.Add("@CREATED_BY", user.UserId);
                                            p.Add("@CREATED_DT", curDate);
                                            p.Add("@CREATED_HOST", user.Host);
                                            await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                            p = null;
                                            errCtr++;
                                        }

                                        line++;
                                    }

                                    listOri.ForEach(x => x.value = null);
                                    listValue = listOri;
                                    col = 0;
                                    uid = "";
                                    refcell = 0;
                                    row++;
                                }

                                if (reader.ElementType == typeof(Cell))
                                {
                                    var cell = (Cell)reader.LoadCurrentElement();
                                    if (cell.CellValue != null)
                                    {
                                        text = cell.CellValue.Text;
                                    }
                                    else
                                    {
                                        text = cell.InnerText;
                                    }


                                    if (cell.DataType != null)
                                    {
                                        if (cell.DataType.Value == CellValues.SharedString)
                                        {
                                            if (stringTable != null)
                                            {
                                                SharedStringItem ssi = workbookPart.SharedStringTablePart
                                                    .SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                                                text = ssi.Text.Text;
                                            }
                                        }
                                    }

                                    refcell = CellReferenceToIndex(cell); //get ref index.

                                    if ((refcell == -1 ? col : refcell) == uidIdx) //uid
                                    {
                                        uid = text;
                                    }

                                    if (row == 1)
                                    {
                                        FormDetail d = listColumn.Where(f => f.GrdColumnName.ToUpper() == text.ToUpper()).FirstOrDefault();
                                        if (d == null)
                                        {
                                            if (text.ToLower() == "is delete")
                                            {
                                                listOri.Add(new CellValueMap { field = "IS_DELETE", value = null });
                                            }
                                            else
                                            {
                                                throw new Exception("Column " + text + " Not Found, Please check your excel format.");
                                            }
                                        }
                                        else
                                        {
                                            if (d.FieldName == "UID")
                                            {
                                                uidIdx = refcell == -1 ? col : refcell;
                                            }

                                            listOri.Add(new CellValueMap { field = d.FieldName, value = null });
                                        }
                                        d = null;
                                    }

                                    if (row > 1) //row data.
                                    {
                                        if (refcell == -1)
                                        {
                                            if (listValue.Count > col) //prevent data column more than header.
                                            {
                                                listValue[col].value = text == "" ? null : text;
                                            }
                                        }
                                        else
                                        {
                                            if (listValue.Count > refcell) //prevent data column more than header.
                                            {
                                                listValue[refcell].value = text == "" ? null : text;
                                            }
                                        }

                                    }

                                    col++;
                                }
                            }

                            //jalanin kalkulasi kalau form neraca atau laba rugi
                            if (IsNeracaLabaRugi(formCode.ToLower()))
                            {
                                var dst = listNeracaLabaRugi.GroupBy(o => new { o.ReportDT, o.Branch }).Select(g => g.First()).ToList();
                                foreach (var item in dst)
                                {
                                    ExecuteAutoCalculation(con, formCode.ToLower(), item);
                                }
                            }

                            totalRecord = line - 1;

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }

                    spreadsheetDocument.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalRecord;
        }

        private void ExecuteAutoCalculation(IDbConnection con, string formCode, CalculateNeracaLabaRugi item)
        {
            switch (formCode)
            {
                case "nrc01_bs_d":
                    con.Execute(string.Format("exec ARRA_ANTASENA_BUS..SP_UPDATE_NRC01_BY_BRANCH '{0}', '{1}', '{2}'", item.ReportDT, "DAILY", item.Branch));
                    break;
                case "nrc01_bs_m":
                    con.Execute(string.Format("exec ARRA_ANTASENA_BUS..SP_UPDATE_NRC01_BY_BRANCH '{0}', '{1}', '{2}'", item.ReportDT, "MONTHLY", item.Branch));
                    break;
                case "nrc01_bs_t":
                    con.Execute(string.Format("exec ARRA_ANTASENA_BUS..SP_UPDATE_NRC01_BY_BRANCH '{0}', '{1}', '{2}'", item.ReportDT, "QUARTERLY", item.Branch));
                    break;
                case "lrg01_bs_m":
                    con.Execute(string.Format("exec ARRA_ANTASENA_BUS..SP_UPDATE_LRG01_BY_BRANCH '{0}', '{1}', '{2}'", item.ReportDT, "MONTHLY", item.Branch));
                    break;
                case "lrg01_bs_t":
                    con.Execute(string.Format("exec ARRA_ANTASENA_BUS..SP_UPDATE_LRG01_BY_BRANCH '{0}', '{1}', '{2}'", item.ReportDT, "QUARTERLY", item.Branch));
                    break;
                default:
                    break;
            }
        }

        private void AddListBranch(List<CellValueMap> listValue, List<CalculateNeracaLabaRugi> listNeracaLabaRugi)
        {
            string reportDT = listValue.Where(f => f.field.ToUpper() == "REPORT_DT").Select(c => c.value).FirstOrDefault();
            string branch = listValue.Where(f => f.field.ToUpper() == "RG_BRANCH").Select(c => c.value).FirstOrDefault(); ;

            double doubleVal;
            if (double.TryParse(reportDT, out doubleVal))
            {
                if (reportDT.ToString().Length != 8)
                {
                    reportDT = DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd");
                }
            }

            listNeracaLabaRugi.Add(new CalculateNeracaLabaRugi { ReportDT = reportDT, Branch = branch });
        }

        private bool IsNeracaLabaRugi(string formCode)
        {
            return (formCode == "nrc01_bs_d" || formCode == "nrc01_bs_m" || formCode == "nrc01_bs_t" || formCode == "lrg01_bs_m" || formCode == "lrg01_bs_t");
        }

        public void WriteExcel(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath,
                    SpreadsheetDocumentType.Workbook))
                {
                    List<OpenXmlAttribute> oxa;
                    OpenXmlWriter oxw;

                    xl.AddWorkbookPart();
                    WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                    oxw = OpenXmlWriter.Create(wsp);
                    oxw.WriteStartElement(new Worksheet());
                    oxw.WriteStartElement(new SheetData());

                    //set header column
                    oxa = new List<OpenXmlAttribute>();
                    oxa.Add(new OpenXmlAttribute("r", null, "1"));
                    oxw.WriteStartElement(new Row(), oxa);
                    foreach (FormDetail item in listColumn) //header column
                    {
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("t", null, "str"));
                        oxw.WriteStartElement(new Cell(), oxa);
                        oxw.WriteElement(new CellValue(item.GrdColumnName));
                        oxw.WriteEndElement();
                    }

                    //delete flag
                    oxa = new List<OpenXmlAttribute>();
                    oxa.Add(new OpenXmlAttribute("t", null, "str"));
                    oxw.WriteStartElement(new Cell(), oxa);
                    oxw.WriteElement(new CellValue("Is Delete"));
                    oxw.WriteEndElement();

                    oxw.WriteEndElement();

                    //bool hasData = false;
                    int i = 2;
                    while (rd.Read())
                    {
                        //hasData = true;
                        oxa = new List<OpenXmlAttribute>();

                        // this is the row index
                        oxa.Add(new OpenXmlAttribute("r", null, i.ToString()));

                        oxw.WriteStartElement(new Row(), oxa);

                        int j = 0;
                        foreach (FormDetail item in listColumn)
                        {
                            oxa = new List<OpenXmlAttribute>();

                            //t = data type.
                            oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));

                            // sample of cell reference.
                            //oxa.Add(new OpenXmlAttribute("r", null, (i - 1).ToString() + j.ToString()));

                            oxw.WriteStartElement(new Cell(), oxa);

                            oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, rd[item.FieldName])));

                            // this is for Cell
                            oxw.WriteEndElement();
                            j++;
                        }

                        // this is for Row
                        oxw.WriteEndElement();

                        i++;
                    }

                    //if (!hasData)//prevent invalid
                    //{
                    //    oxa = new List<OpenXmlAttribute>();
                    //    oxa.Add(new OpenXmlAttribute("r", null, i.ToString()));
                    //    oxw.WriteStartElement(new Row(), oxa);
                    //    foreach (FormDetail item in listColumn)
                    //    {
                    //        oxa = new List<OpenXmlAttribute>();
                    //        //t = data type.
                    //        oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));
                    //        oxw.WriteStartElement(new Cell(), oxa);
                    //        oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, null)));
                    //        oxw.WriteEndElement();
                    //    }
                    //    // this is for Row
                    //    oxw.WriteEndElement();
                    //}

                    // this is for SheetData
                    oxw.WriteEndElement();
                    // this is for Worksheet
                    oxw.WriteEndElement();
                    oxw.Close();

                    oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                    oxw.WriteStartElement(new Workbook());
                    oxw.WriteStartElement(new Sheets());

                    oxw.WriteElement(new Sheet()
                    {
                        Name = "Sheet1",
                        SheetId = 1,
                        Id = xl.WorkbookPart.GetIdOfPart(wsp)
                    });

                    // this is for Sheets
                    oxw.WriteEndElement();
                    // this is for Workbook
                    oxw.WriteEndElement();
                    oxw.Close();

                    xl.Close();

                    oxw = null;
                    oxa = null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private Comments GenerateWorksheetCommentsPart1Content()
        {
            Comments comments1 = new Comments() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "xr" } };
            comments1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            comments1.AddNamespaceDeclaration("xr", "http://schemas.microsoft.com/office/spreadsheetml/2014/revision");

            Authors authors1 = new Authors();
            Author author1 = new Author();
            author1.Text = "ARRA";

            authors1.Append(author1);

            CommentList commentList1 = new CommentList();

            Comment comment1 = new Comment()
            {
                Reference = "A1",
                AuthorId = (UInt32Value)0U,
                ShapeId = (UInt32Value)0U
            };
            comment1.SetAttribute(new OpenXmlAttribute("xr", "uid", "http://schemas.microsoft.com/office/spreadsheetml/2014/revision", "{811649EF-4CB5-4311-BE14-228133003BE4}"));

            CommentText commentText1 = new CommentText();

            Run run1 = new Run();

            RunProperties runProperties1 = new RunProperties();
            FontSize fontSize3 = new FontSize() { Val = 9D };
            Color color3 = new Color() { Indexed = (UInt32Value)81U };
            RunFont runFont1 = new RunFont() { Val = "Tahoma" };
            RunPropertyCharSet runPropertyCharSet1 = new RunPropertyCharSet() { Val = 1 };

            runProperties1.Append(fontSize3);
            runProperties1.Append(color3);
            runProperties1.Append(runFont1);
            runProperties1.Append(runPropertyCharSet1);
            Text text1 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text1.Text = "This is my comment!\nThis is line 2!\n";

            run1.Append(runProperties1);
            run1.Append(text1);

            commentText1.Append(run1);

            comment1.Append(commentText1);

            commentList1.Append(comment1);

            comments1.Append(authors1);
            comments1.Append(commentList1);

            //return comments1;
            //worksheetCommentsPart1.Comments = comments1;

            return comments1;
        }
        public void WriteExcelValidation(IDataReader rd, List<dynamic> rdVal, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                // https://stackoverflow.com/questions/11116176/cell-styles-in-openxml-spreadsheet-spreadsheetml/11118442#11118442
                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath, SpreadsheetDocumentType.Workbook))
                {
                    Console.WriteLine("Creating workbook");
                    xl.AddWorkbookPart();
                    xl.WorkbookPart.Workbook = new Workbook();

                    Console.WriteLine("Creating worksheet");
                    var wsPart = xl.WorkbookPart.AddNewPart<WorksheetPart>();
                    wsPart.Worksheet = new Worksheet();

                    #region WorkbookStylesPart
                    var stylesPart = xl.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                    stylesPart.Stylesheet = new Stylesheet();

                    Console.WriteLine("Creating styles");
                    // blank font list
                    stylesPart.Stylesheet.Fonts = new Fonts();
                    stylesPart.Stylesheet.Fonts.Count = 1;
                    stylesPart.Stylesheet.Fonts.AppendChild(new Font());

                    // create fills
                    stylesPart.Stylesheet.Fills = new Fills();

                    // create a solid red fill
                    var solidRed = new PatternFill() { PatternType = PatternValues.Solid };
                    solidRed.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("FF7F7F") }; // red fill
                    solidRed.BackgroundColor = new BackgroundColor { Indexed = 64 };

                    stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
                    stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
                    stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = solidRed });
                    stylesPart.Stylesheet.Fills.Count = 3;

                    // blank border list
                    stylesPart.Stylesheet.Borders = new Borders();
                    stylesPart.Stylesheet.Borders.Count = 1;
                    stylesPart.Stylesheet.Borders.AppendChild(new Border());

                    // blank cell format list
                    stylesPart.Stylesheet.CellStyleFormats = new CellStyleFormats();
                    stylesPart.Stylesheet.CellStyleFormats.Count = 1;
                    stylesPart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

                    // cell format list
                    stylesPart.Stylesheet.CellFormats = new CellFormats();
                    stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat());
                    stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 0, BorderId = 0, FillId = 2, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });
                    stylesPart.Stylesheet.CellFormats.Count = 2;

                    stylesPart.Stylesheet.Save();
                    #endregion WorkbookStylesPart

                    #region
                    var commentXmlBuilder = new StringBuilder();
                    commentXmlBuilder.Append("<xml xmlns:v=\"urn:schemas-microsoft-com:vml\""
                        + "xmlns:o=\"urn:schemas-microsoft-com:office:office\""
                        + "xmlns:x=\"urn:schemas-microsoft-com:office:excel\">"
                        + "<o:shapelayout v:ext=\"edit\">"
                        + "<o:idmap v:ext=\"edit\" data=\"1\"/>"
                        + "</o:shapelayout><v:shapetype id=\"_x0000_t202\" coordsize=\"21600,21600\" o:spt=\"202\""
                        + "path=\"m,l,21600r21600,l21600,xe\">"
                        + "<v:stroke joinstyle=\"miter\"/>"
                        + "<v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>"
                        + "</v:shapetype>");

                    WorksheetCommentsPart worksheetCommentsPart = wsPart.AddNewPart<WorksheetCommentsPart>();
                    worksheetCommentsPart.Comments = new Comments();

                    Authors objAuthors = new Authors();
                    Author objAuthor = new Author();
                    objAuthor.Text = "ARRA";
                    objAuthors.Append(objAuthor);
                    worksheetCommentsPart.Comments.Append(objAuthors);
                    CommentList commentList = new CommentList();

                    #endregion

                    #region writing header & rows
                    var sheetData = wsPart.Worksheet.AppendChild(new SheetData());
                    var row = sheetData.AppendChild(new Row());
                    foreach (FormDetail item in listColumn)
                    {
                        row.AppendChild(new Cell() { CellValue = new CellValue(item.GrdColumnName), DataType = CellValues.String });
                    }

                    int rowIndex = 0;
                    while (rd.Read())
                    {
                        int columnIndex = 0;
                        row = sheetData.AppendChild(new Row());
                        var rdValData = rdVal[rowIndex] as IDictionary<string, object>;
                        foreach (FormDetail item in listColumn)
                        {
                            bool isError = rdValData[item.FieldName] != null && rdValData[item.FieldName].ToString() != "";
                            if (isError)
                            {
                                row.AppendChild(new Cell() { CellValue = new CellValue(FormatValue(item.EdrControlType, rd[item.FieldName])), DataType = CellValues.String, StyleIndex = 1 });

                                commentXmlBuilder.Append("<v:shape id=\"" + Guid.NewGuid().ToString().Replace("-", "") + "\" type=\"#_x0000_t202\" style=\'position:absolute;"
                                    + "margin-left:65.25pt;margin-top:1.5pt;width:270pt;height:59.25pt;z-index:1;"
                                    + "visibility:hidden;mso-wrap-style:tight\' fillcolor=\"#ffffe1\" o:insetmode=\"auto\">"
                                    + "<v:fill color2=\"#ffffe1\"/>"
                                    + "<v:shadow on=\"t\" color=\"black\" obscured=\"t\"/>"
                                    + "<v:path o:connecttype=\"none\"/>"
                                    + "<v:textbox style=\'mso-direction-alt:auto\'>"
                                    + "<div style=\'text-align:left\'></div>"
                                    + "</v:textbox>"
                                    + "<x:ClientData ObjectType=\"Note\">"
                                    + "<x:MoveWithCells/>"
                                    + "<x:SizeWithCells/>"
                                    + "<x:Anchor>1, 15, 0, 2, 6, 15, 4, 9</x:Anchor>"
                                    + "<x:AutoFill>False</x:AutoFill>"
                                    + "<x:Row>" + (rowIndex + 1).ToString() + "</x:Row>"
                                    + "<x:Column>" + columnIndex.ToString() + "</x:Column>"
                                    + "</x:ClientData>"
                                    + "</v:shape>");
                                Run runHeader = new Run();
                                Text textHeader = new Text() { Text = "ARRA:" };
                                runHeader.Append(textHeader);

                                Run runDetail = new Run();
                                Text textDetail = new Text()
                                {
                                    Text = rdValData[item.FieldName].ToString(),
                                    Space = SpaceProcessingModeValues.Preserve
                                };
                                runDetail.Append(textDetail);

                                CommentText commentText = new CommentText();
                                commentText.Append(runHeader);
                                commentText.Append(runDetail);

                                string cellColumnReference = getCellColumnReference(columnIndex) + (rowIndex + 2).ToString();
                                Comment comment = new Comment()
                                {
                                    Reference = cellColumnReference,
                                    AuthorId = (UInt32Value)0U
                                };
                                comment.Append(commentText);
                                commentList.Append(comment);
                            }
                            else
                            {
                                row.AppendChild(new Cell() { CellValue = new CellValue(FormatValue(item.EdrControlType, rd[item.FieldName])), DataType = CellValues.String });
                            }
                            columnIndex++;
                        }
                        rowIndex++;
                    }
                    //row.AppendChild(new Cell() { CellFormula = new CellFormula("B3"), DataType = CellValues.Number, StyleIndex = 1 });
                    #endregion writing header & rows

                    Console.WriteLine("Saving worksheet");
                    wsPart.Worksheet.Save();

                    commentXmlBuilder.Append("</xml>");

                    VmlDrawingPart vdp = wsPart.AddNewPart<VmlDrawingPart>();
                    XmlTextWriter writer = new XmlTextWriter(vdp.GetStream(FileMode.Create), Encoding.UTF8);
                    writer.WriteRaw(commentXmlBuilder.ToString());
                    writer.Flush();
                    writer.Close();

                    worksheetCommentsPart.Comments.Append(commentList);
                    string vmlrid = wsPart.GetIdOfPart(vdp);
                    LegacyDrawing ld = new LegacyDrawing() { Id = vmlrid };
                    wsPart.Worksheet.Append(ld);

                    Console.WriteLine("Creating sheet list");
                    var sheets = xl.WorkbookPart.Workbook.AppendChild(new Sheets());
                    sheets.AppendChild(new Sheet() { Id = xl.WorkbookPart.GetIdOfPart(wsPart), SheetId = 1, Name = "Sheet1" });

                    Console.WriteLine("Saving workbook");
                    xl.WorkbookPart.Workbook.Save();

                    Console.WriteLine("Done.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<long> WriteExcelCompare(IDbConnection con, IList<FormDetail> listColumn, string fileFullPathImport, string fileFullPathExport, string tableName, string formCode)
        {
            long totalRecord = 0;
            string guid = Guid.NewGuid().ToString();
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileFullPathImport, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                    Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Sheet1").FirstOrDefault();
                    if (sheet == null)
                    {
                        throw new Exception("Excel sheet not found (sheet name must using \"Sheet1\")");
                    }

                    WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                    var stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    List<CellValueMap> listOri = new List<CellValueMap>();


                    //#region insert into temp table
                    using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
                    {
                        try
                        {
                            long row = 0;
                            string text;
                            int col = 0;
                            string uid = "";
                            DynamicParameters p = new DynamicParameters();
                            string curDate = new MachineDateTime().Now.ToString();
                            int uidIdx = 0;
                            int refcell = 0;
                            long line = 1;
                            while (reader.Read())
                            {
                                if (reader.ElementType == typeof(Row))
                                {
                                    if ((col > 0 || refcell > 0) && row > 1) //is not header and contains value
                                    {
                                        try
                                        {
                                            if (uid != "" && uid != "0")
                                            {
                                                p = new DynamicParameters();
                                                p.Add("@P1", guid);
                                                p.Add("@P2", uid);
                                                p.Add("@P3", line);
                                                await con.ExecuteAsync("INSERT INTO " + tableName + "(UID,REF_ID,LINE) VALUES(@P1,@P2,@P3)", p);
                                            }

                                        }
                                        catch (SqlException ex)
                                        {
                                            throw ex;
                                        }

                                        line++;
                                    }

                                    col = 0;
                                    uid = "";
                                    refcell = 0;
                                    row++;
                                }

                                if (reader.ElementType == typeof(Cell))
                                {
                                    var cell = (Cell)reader.LoadCurrentElement();
                                    if (cell.CellValue != null)
                                    {
                                        text = cell.CellValue.Text;
                                    }
                                    else
                                    {
                                        text = cell.InnerText;
                                    }


                                    if (cell.DataType != null)
                                    {
                                        if (cell.DataType.Value == CellValues.SharedString)
                                        {
                                            if (stringTable != null)
                                            {
                                                SharedStringItem ssi = workbookPart.SharedStringTablePart
                                                    .SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                                                text = ssi.Text.Text;
                                            }
                                        }
                                    }

                                    refcell = CellReferenceToIndex(cell); //get ref index.
                                    if ((refcell == -1 ? col : refcell) == uidIdx) //uid
                                    {
                                        uid = text;

                                    }

                                    if (row == 1)
                                    {
                                        FormDetail d = listColumn.Where(f => f.GrdColumnName.ToUpper() == text.ToUpper()).FirstOrDefault();
                                        if (d == null)
                                        {
                                            if (text.ToLower() == "is delete")
                                            {
                                                listOri.Add(new CellValueMap { field = "IS_DELETE", value = null, caption = "Is Delete", type = ControlType.TEXT.ToString() });
                                            }
                                            else
                                            {
                                                throw new Exception("Column " + text + " Not Found, Please check your excel format.");
                                            }
                                        }
                                        else
                                        {
                                            if (d.FieldName == "UID")
                                            {
                                                uidIdx = refcell == -1 ? col : refcell;
                                            }

                                            listOri.Add(new CellValueMap { field = d.FieldName, value = null, caption = d.GrdColumnName, type = d.EdrControlType });
                                        }
                                        d = null;
                                    }


                                    col++;
                                }
                            }

                            totalRecord = line - 1;


                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }


                    ////////new step
                    string cols = "";
                    foreach (CellValueMap map in listOri)
                    {
                        if (map.field != "IS_DELETE")
                        {
                            cols += "D." + map.field + ",";
                        }
                    }

                    if (cols != "")
                    {
                        cols = cols.Substring(0, cols.Length - 1);

                        DynamicParameters p = new DynamicParameters();
                        p.Add("@FORM_CD", formCode);
                        p.Add("@COLS", cols);
                        p.Add("@GUID", guid);
                        using (IDataReader rd = await con.ExecuteReaderAsync("DBO.UDPS_EXPORT_COMPARE", p, null, null, CommandType.StoredProcedure))
                        {
                            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPathExport,
                                SpreadsheetDocumentType.Workbook))
                            {
                                List<OpenXmlAttribute> oxa;
                                OpenXmlWriter oxw;

                                xl.AddWorkbookPart();
                                WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                                oxw = OpenXmlWriter.Create(wsp);
                                oxw.WriteStartElement(new Worksheet());
                                oxw.WriteStartElement(new SheetData());

                                //set header column
                                oxa = new List<OpenXmlAttribute>();
                                oxa.Add(new OpenXmlAttribute("r", null, "1"));
                                oxw.WriteStartElement(new Row(), oxa);
                                foreach (CellValueMap item in listOri) //header column
                                {
                                    oxa = new List<OpenXmlAttribute>();
                                    oxa.Add(new OpenXmlAttribute("t", null, "str"));
                                    oxw.WriteStartElement(new Cell(), oxa);
                                    oxw.WriteElement(new CellValue(item.caption + " Before"));
                                    oxw.WriteEndElement();

                                    oxa = new List<OpenXmlAttribute>();
                                    oxa.Add(new OpenXmlAttribute("t", null, "str"));
                                    oxw.WriteStartElement(new Cell(), oxa);
                                    oxw.WriteElement(new CellValue(item.caption + " After"));
                                    oxw.WriteEndElement();
                                }

                                oxw.WriteEndElement();

                                List<CellValueMap> listValue = new List<CellValueMap>();
                                using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
                                {
                                    long row = 0;
                                    string text;
                                    int col = 0;
                                    int refcell = 0;
                                    int uidIdx = 0;
                                    long line = 2;
                                    string uid = "";
                                    while (reader.Read())
                                    {
                                        if (reader.ElementType == typeof(Row))
                                        {
                                            if ((col > 0 || refcell > 0) && row > 1) //is not header and contains value
                                            {
                                                try
                                                {
                                                    object valuebefore = null;
                                                    if (uid != "" && uid != "0")
                                                    {
                                                        rd.Read();
                                                    }

                                                    oxa = new List<OpenXmlAttribute>();

                                                    // this is the row index
                                                    oxa.Add(new OpenXmlAttribute("r", null, line.ToString()));
                                                    oxw.WriteStartElement(new Row(), oxa);
                                                    foreach (CellValueMap item in listValue)
                                                    {
                                                        valuebefore = null;
                                                        if (uid != "" && uid != "0")
                                                        {
                                                            if (item.field != "IS_DELETE")
                                                            {
                                                                valuebefore = rd[item.field];
                                                            }
                                                        }

                                                        //before
                                                        oxa = new List<OpenXmlAttribute>();
                                                        oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.type)));
                                                        oxw.WriteStartElement(new Cell(), oxa);
                                                        oxw.WriteElement(new CellValue(FormatValue(item.type, valuebefore)));
                                                        oxw.WriteEndElement();

                                                        string valueAfter = "";
                                                        if (item.type == ControlType.DATE.ToString())
                                                        {
                                                            double doubleVal;
                                                            if (double.TryParse(item.value, out doubleVal))
                                                            {
                                                                if (item.value.ToString().Length == 8)
                                                                {
                                                                    valueAfter = item.value;
                                                                }
                                                                else
                                                                {
                                                                    valueAfter = DateTime.FromOADate(doubleVal).ToString("yyyy-MM-dd");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                valueAfter = item.value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            valueAfter = item.value;
                                                        }

                                                        //after
                                                        oxa = new List<OpenXmlAttribute>();
                                                        oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.type)));
                                                        oxw.WriteStartElement(new Cell(), oxa);
                                                        //oxw.WriteElement(new CellValue(FormatValue(item.type, item.value)));
                                                        oxw.WriteElement(new CellValue(valueAfter));
                                                        oxw.WriteEndElement();
                                                    }

                                                    // this is for Row
                                                    oxw.WriteEndElement();

                                                }
                                                catch (SqlException ex)
                                                {
                                                    throw ex;
                                                }

                                                line++;
                                            }

                                            col = 0;
                                            uid = "";
                                            refcell = 0;
                                            row++;
                                            listOri.ForEach(x => x.value = null);
                                            listValue = listOri;
                                        }

                                        if (reader.ElementType == typeof(Cell))
                                        {
                                            var cell = (Cell)reader.LoadCurrentElement();
                                            if (cell.CellValue != null)
                                            {
                                                text = cell.CellValue.Text;
                                            }
                                            else
                                            {
                                                text = cell.InnerText;
                                            }

                                            if (cell.DataType != null)
                                            {
                                                if (cell.DataType.Value == CellValues.SharedString)
                                                {
                                                    if (stringTable != null)
                                                    {
                                                        SharedStringItem ssi = workbookPart.SharedStringTablePart
                                                            .SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                                                        text = ssi.Text.Text;
                                                    }
                                                }
                                            }

                                            refcell = CellReferenceToIndex(cell); //get ref index.
                                            if ((refcell == -1 ? col : refcell) == uidIdx) //uid
                                            {
                                                uid = text;
                                            }

                                            if (row == 1)
                                            {
                                                FormDetail d = listColumn.Where(f => f.GrdColumnName.ToUpper() == text.ToUpper()).FirstOrDefault();
                                                if (d == null)
                                                {
                                                    if (text.ToLower() != "is delete")
                                                    {
                                                        throw new Exception("Column " + text + " Not Found, Please check your excel format.");
                                                    }
                                                }
                                                else
                                                {
                                                    if (d.FieldName == "UID") //get uid index
                                                    {
                                                        uidIdx = refcell == -1 ? col : refcell;
                                                    }
                                                }
                                                d = null;
                                            }


                                            if (row > 1) //row data.
                                            {
                                                if (refcell == -1)
                                                {
                                                    if (listValue.Count > col) //prevent data column more than header.
                                                    {
                                                        listValue[col].value = text == "" ? null : text;
                                                    }
                                                }
                                                else
                                                {
                                                    if (listValue.Count > refcell) //prevent data column more than header.
                                                    {
                                                        listValue[refcell].value = text == "" ? null : text;
                                                    }
                                                }
                                            }

                                            col++;
                                        }
                                    }

                                }

                                // this is for SheetData
                                oxw.WriteEndElement();
                                // this is for Worksheet
                                oxw.WriteEndElement();
                                oxw.Close();

                                oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                                oxw.WriteStartElement(new Workbook());
                                oxw.WriteStartElement(new Sheets());

                                oxw.WriteElement(new Sheet()
                                {
                                    Name = "Sheet1",
                                    SheetId = 1,
                                    Id = xl.WorkbookPart.GetIdOfPart(wsp)
                                });

                                // this is for Sheets
                                oxw.WriteEndElement();
                                // this is for Workbook
                                oxw.WriteEndElement();
                                oxw.Close();

                                xl.Close();
                            }

                            rd.Close();
                        }

                        spreadsheetDocument.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                await con.ExecuteAsync("DELETE FROM " + tableName + " WHERE UID='" + guid + "'");
            }

            return totalRecord;

        }

        public async Task<long> WriteExcelJObjectCompare(IList<dynamic> data, string fileFullPath)
        {
            long result = 0;
            try
            {
                WorkbookPart workbookPart = null;
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Create(fileFullPath, SpreadsheetDocumentType.Workbook))
                {
                    uint idx = 0;

                    workbookPart = spreadSheetDocument.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();
                    spreadSheetDocument.WorkbookPart.Workbook.Sheets = new Sheets();

                    Sheets sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>();

                    foreach (dynamic dt in data)
                    {
                        idx++;
                        var _data = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(dt));
                        dynamic dataRowList = _data.rows;
                        dynamic listColumn = _data.columns;
                        string sheetName = _data.id;
                        result += dataRowList.Count;

                        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                        Sheet sheet = new Sheet()
                        {
                            Id = spreadSheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                            SheetId = idx,
                            Name = sheetName
                        };

                        sheets.Append(sheet);

                        SheetData sheetData = new SheetData();
                        worksheetPart.Worksheet = new Worksheet(sheetData);

                        Row headerRow = new();
                        foreach (var column in listColumn)
                        {
                            string headerText = column.COLUMN_DESC.ToString();
                            Cell cell = new();
                            cell.DataType = CellValues.String;
                            cell.CellValue = new CellValue(headerText);
                            headerRow.AppendChild(cell);
                        }
                        sheetData.AppendChild(headerRow);

                        for (int dataIdx = 0; dataIdx < dataRowList.Count; dataIdx++)
                        {
                            Row dataRow = new();
                            foreach (var column in listColumn)
                            {
                                string propName = column.COLUMN_ALIAS.ToString();
                                JObject currentData = dataRowList[dataIdx];
                                string dataText = currentData.GetValue(propName).ToString();

                                Cell cell = new();
                                cell.DataType = CellValues.String;
                                cell.CellValue = new CellValue(dataText);
                                dataRow.AppendChild(cell);
                            }
                            sheetData.AppendChild(dataRow);
                        }
                    }
                    spreadSheetDocument.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return await Task.FromResult(result);
        }

        public void WriteExcel(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath, SpreadsheetDocumentType.Workbook))
                {
                    List<OpenXmlAttribute> oxa;
                    OpenXmlWriter oxw;

                    xl.AddWorkbookPart();
                    WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                    oxw = OpenXmlWriter.Create(wsp);
                    oxw.WriteStartElement(new Worksheet());
                    oxw.WriteStartElement(new SheetData());

                    //set header column
                    oxa = new List<OpenXmlAttribute>();
                    oxa.Add(new OpenXmlAttribute("r", null, "1"));
                    oxw.WriteStartElement(new Row(), oxa);
                    foreach (FormDetail item in listColumn) //header column
                    {
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("t", null, "str"));
                        oxw.WriteStartElement(new Cell(), oxa);
                        oxw.WriteElement(new CellValue(item.GrdColumnName));
                        oxw.WriteEndElement();
                    }
                    oxw.WriteEndElement();


                    int i = 2;
                    for (int d = 0; d < ls.Count; d++)
                    {
                        oxa = new List<OpenXmlAttribute>();

                        // this is the row index
                        oxa.Add(new OpenXmlAttribute("r", null, i.ToString()));

                        oxw.WriteStartElement(new Row(), oxa);

                        int j = 0;
                        foreach (FormDetail item in listColumn)
                        {
                            oxa = new List<OpenXmlAttribute>();

                            //t = data type.
                            oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));

                            // sample of cell reference.
                            //oxa.Add(new OpenXmlAttribute("r", null, (i - 1).ToString() + j.ToString()));

                            oxw.WriteStartElement(new Cell(), oxa);

                            object val = ls[d].GetType().GetProperty(item.FieldName).GetValue(ls[d], null);

                            oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, val)));

                            // this is for Cell
                            oxw.WriteEndElement();
                            j++;
                        }

                        // this is for Row
                        oxw.WriteEndElement();

                        i++;
                    }

                    // this is for SheetData
                    oxw.WriteEndElement();
                    // this is for Worksheet
                    oxw.WriteEndElement();
                    oxw.Close();

                    oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                    oxw.WriteStartElement(new Workbook());
                    oxw.WriteStartElement(new Sheets());

                    oxw.WriteElement(new Sheet()
                    {
                        Name = "Sheet1",
                        SheetId = 1,
                        Id = xl.WorkbookPart.GetIdOfPart(wsp)
                    });

                    // this is for Sheets
                    oxw.WriteEndElement();
                    // this is for Workbook
                    oxw.WriteEndElement();
                    oxw.Close();

                    xl.Close();

                    oxw = null;
                    oxa = null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteExcelJObject(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {

                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath,
                    SpreadsheetDocumentType.Workbook))
                {
                    List<OpenXmlAttribute> oxa;
                    OpenXmlWriter oxw;

                    xl.AddWorkbookPart();
                    WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                    oxw = OpenXmlWriter.Create(wsp);
                    oxw.WriteStartElement(new Worksheet());
                    oxw.WriteStartElement(new SheetData());

                    //set header column
                    oxa = new List<OpenXmlAttribute>();
                    oxa.Add(new OpenXmlAttribute("r", null, "1"));
                    oxw.WriteStartElement(new Row(), oxa);
                    foreach (FormDetail item in listColumn) //header column
                    {
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("t", null, "str"));
                        oxw.WriteStartElement(new Cell(), oxa);
                        oxw.WriteElement(new CellValue(item.GrdColumnName));
                        oxw.WriteEndElement();
                    }
                    oxw.WriteEndElement();


                    int i = 2;
                    for (int d = 0; d < ls.Count; d++)
                    {
                        oxa = new List<OpenXmlAttribute>();

                        // this is the row index
                        oxa.Add(new OpenXmlAttribute("r", null, i.ToString()));

                        oxw.WriteStartElement(new Row(), oxa);

                        int j = 0;
                        foreach (FormDetail item in listColumn)
                        {
                            oxa = new List<OpenXmlAttribute>();

                            //t = data type.
                            oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));

                            // sample of cell reference.
                            //oxa.Add(new OpenXmlAttribute("r", null, (i - 1).ToString() + j.ToString()));

                            oxw.WriteStartElement(new Cell(), oxa);

                            object val = ls[d][item.FieldName];

                            oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, val)));

                            // this is for Cell
                            oxw.WriteEndElement();
                            j++;
                        }

                        // this is for Row
                        oxw.WriteEndElement();

                        i++;
                    }

                    // this is for SheetData
                    oxw.WriteEndElement();
                    // this is for Worksheet
                    oxw.WriteEndElement();
                    oxw.Close();

                    oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                    oxw.WriteStartElement(new Workbook());
                    oxw.WriteStartElement(new Sheets());

                    oxw.WriteElement(new Sheet()
                    {
                        Name = "Sheet1",
                        SheetId = 1,
                        Id = xl.WorkbookPart.GetIdOfPart(wsp)
                    });

                    // this is for Sheets
                    oxw.WriteEndElement();
                    // this is for Workbook
                    oxw.WriteEndElement();
                    oxw.Close();

                    xl.Close();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<long> ManualUpload(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId, string actionType)
        {
            long totalRecord = 0;
            try
            {

                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileFullPath, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                    Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Sheet1").FirstOrDefault();
                    WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                    var stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    using (OpenXmlReader reader = OpenXmlReader.Create(worksheetPart))
                    {
                        try
                        {
                            long row = 0;
                            string text;
                            int col = 0;
                            string uid = "";
                            DynamicParameters p = new DynamicParameters();
                            string qryIns = "";
                            string qryInsVal = "";
                            string qryUpd = "";
                            string qryDel = "";
                            string qryConds = "";
                            string curDate = new MachineDateTime().Now.ToString();
                            string insert = "";
                            string update = "";
                            string delete = "";
                            int refcell = 0;
                            long line = 1;
                            long errCtr = 0;
                            List<CellValueMap> listOri = new List<CellValueMap>();
                            List<CellValueMap> listValue = new List<CellValueMap>();
                            while (reader.Read())
                            {
                                if (reader.ElementType == typeof(Row))
                                {
                                    if ((col > 0 || refcell > 0) && row > 1) //is not header and contains value
                                    {
                                        insert = ""; update = "";
                                        qryIns = ""; qryInsVal = ""; qryUpd = ""; qryConds = "";
                                        p = new DynamicParameters();
                                        foreach (ManualUploadDetail dd in listColumn)
                                        {
                                            CellValueMap map = listValue.Where(f => f.field == dd.TargetColumn).FirstOrDefault();
                                            if (map != null)
                                            {

                                                if (actionType == OperationType.ADD.ToString())
                                                {
                                                    qryIns += dd.TargetColumn + ",";
                                                    qryInsVal += "@" + dd.TargetColumn + ",";

                                                    //drop logic for handle the datetime read as number here.

                                                    p.Add("@" + dd.TargetColumn, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
                                                }
                                                else if (actionType == OperationType.EDIT.ToString())
                                                {
                                                    if (dd.IsKey == true)
                                                    {
                                                        qryConds += dd.TargetColumn + "=@" + dd.TargetColumn + " AND ";
                                                    }
                                                    else
                                                    {
                                                        qryUpd += dd.TargetColumn + "=@" + dd.TargetColumn + ",";
                                                    }
                                                    //drop logic for handle the datetime read as number here.
                                                    p.Add("@" + dd.TargetColumn, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
                                                }
                                                else if (actionType == OperationType.REMOVE.ToString())
                                                {
                                                    if (dd.IsKey == true)
                                                    {
                                                        qryConds += dd.TargetColumn + "=@" + dd.TargetColumn + " AND ";

                                                        //drop logic for handle the datetime read as number here.
                                                        p.Add("@" + dd.TargetColumn, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
                                                    }
                                                }
                                                else if (actionType == OperationType.REPLACE.ToString())
                                                { //delete insert
                                                    qryIns += dd.TargetColumn + ",";
                                                    qryInsVal += "@" + dd.TargetColumn + ",";
                                                    p.Add("@" + dd.TargetColumn, map.value == null ? null : ARRA.Common.Utility.SafeSqlString(map.value));
                                                    if (dd.IsKey == true)
                                                    {
                                                        qryConds += dd.TargetColumn + "=@" + dd.TargetColumn + " AND ";
                                                    }
                                                    //drop logic for handle the datetime read as number here.
                                                }
                                            }
                                            map = null;
                                        }

                                        try
                                        {
                                            if (actionType == OperationType.ADD.ToString())
                                            {
                                                insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                                await con.ExecuteAsync(insert, p);
                                            }
                                            else if (actionType == OperationType.EDIT.ToString())
                                            {
                                                if (qryConds != "")
                                                {
                                                    update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                    await con.ExecuteAsync(update, p);
                                                }
                                            }
                                            else if (actionType == OperationType.REMOVE.ToString())
                                            {
                                                if (qryConds != "")
                                                {
                                                    delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                    await con.ExecuteAsync(delete, p);
                                                }
                                            }
                                            else if (actionType == OperationType.REPLACE.ToString())
                                            { //delete insert
                                                if (qryConds != "")
                                                {
                                                    delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                    insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                                    await con.ExecuteAsync(delete + " " + insert, p);
                                                }
                                            }

                                            if (line % 1000 == 0)
                                            {
                                                p = new DynamicParameters();
                                                p.Add("@UID", logId);
                                                p.Add("@TOTAL_PROCEED", line);
                                                p.Add("@TOTAL_FAILED", errCtr);
                                                await con.ExecuteAsync("UPDATE TR_IMPORT_FILE_LIST_HDR SET TOTAL_PROCEED=@TOTAL_PROCEED,TOTAL_FAILED=@TOTAL_FAILED WHERE UID=@UID ", p);
                                            }

                                            p = null;
                                        }
                                        catch (SqlException ex)
                                        {
                                            p = new DynamicParameters();
                                            p.Add("@HDR_ID", logId);
                                            p.Add("@ROW_NUM", line + 1);
                                            p.Add("@ERR_DESC", ex.Message);
                                            p.Add("@CREATED_BY", user.UserId);
                                            p.Add("@CREATED_DT", curDate);
                                            p.Add("@CREATED_HOST", user.Host);
                                            await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                            p = null;
                                            errCtr++;
                                        }

                                        line++;
                                    }

                                    listOri.ForEach(x => x.value = null);
                                    listValue = listOri;
                                    col = 0;
                                    uid = "";
                                    refcell = 0;
                                    row++;
                                }

                                if (reader.ElementType == typeof(Cell))
                                {
                                    var cell = (Cell)reader.LoadCurrentElement();
                                    if (cell.CellValue != null)
                                    {
                                        text = cell.CellValue.Text;
                                    }
                                    else
                                    {
                                        text = cell.InnerText;
                                    }

                                    if (cell.DataType != null)
                                    {
                                        if (cell.DataType.Value == CellValues.SharedString)
                                        {
                                            if (stringTable != null)
                                            {
                                                SharedStringItem ssi = workbookPart.SharedStringTablePart
                                                    .SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                                                text = ssi.Text.Text;
                                            }
                                        }
                                    }

                                    refcell = CellReferenceToIndex(cell); //get ref index.
                                    if (row == 1)
                                    {
                                        ManualUploadDetail d = listColumn.Where(f => f.SourceColumn.ToUpper() == text.ToUpper()).FirstOrDefault();
                                        if (d == null)
                                        {
                                            throw new Exception("Column " + text + " Not Found, Please check your excel format.");
                                        }
                                        else
                                        {
                                            listOri.Add(new CellValueMap { field = d.TargetColumn, value = null });
                                        }
                                        d = null;
                                    }

                                    if (row > 1) //row data.
                                    {
                                        if (refcell == -1)
                                        {
                                            if (listValue.Count > col) //prevent data column more than header.
                                            {
                                                listValue[col].value = text == "" ? null : text;
                                            }
                                        }
                                        else
                                        {
                                            if (listValue.Count > refcell) //prevent data column more than header.
                                            {
                                                // convert excel date
                                                text = await ConvertExcelDateToString(con, tableName, text, refcell, listValue);

                                                listValue[refcell].value = text == "" ? null : text;
                                            }
                                        }

                                    }

                                    col++;
                                }
                            }

                            totalRecord = line - 1;

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }

                    spreadsheetDocument.Close();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalRecord;
        }
        private async Task<string> ConvertExcelDateToString(IDbConnection con, string tableName, string text, int refcell, List<CellValueMap> listValue)
        {
            string colDataType = (await con.ExecuteScalarAsync("SELECT DATA_TYPE FROM " + tableName.Split('.')[0] + ".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName.Split('.')[2] + "' AND COLUMN_NAME = '" + listValue[refcell].field + "'")).ToString();
            string[] dateType = new string[2] { "date", "datetime" };
            int value;
            if (dateType.Contains(colDataType) && int.TryParse(text, out value))
            {
                text = DateTime.FromOADate(value).ToString();
            }

            return text;
        }

        public void GenerateManualUploadTemplate(IList<ManualUploadDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath,
                    SpreadsheetDocumentType.Workbook))
                {
                    List<OpenXmlAttribute> oxa;
                    OpenXmlWriter oxw;

                    xl.AddWorkbookPart();
                    WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                    oxw = OpenXmlWriter.Create(wsp);
                    oxw.WriteStartElement(new Worksheet());
                    oxw.WriteStartElement(new SheetData());

                    //set header column
                    oxa = new List<OpenXmlAttribute>();
                    oxa.Add(new OpenXmlAttribute("r", null, "1"));
                    oxw.WriteStartElement(new Row(), oxa);
                    foreach (ManualUploadDetail item in listColumn) //header column
                    {
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("t", null, "str"));
                        oxw.WriteStartElement(new Cell(), oxa);
                        oxw.WriteElement(new CellValue(item.SourceColumn));
                        oxw.WriteEndElement();
                    }
                    oxw.WriteEndElement();


                    // this is for SheetData
                    oxw.WriteEndElement();
                    // this is for Worksheet
                    oxw.WriteEndElement();
                    oxw.Close();

                    oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                    oxw.WriteStartElement(new Workbook());
                    oxw.WriteStartElement(new Sheets());

                    oxw.WriteElement(new Sheet()
                    {
                        Name = "Sheet1",
                        SheetId = 1,
                        Id = xl.WorkbookPart.GetIdOfPart(wsp)
                    });

                    // this is for Sheets
                    oxw.WriteEndElement();
                    // this is for Workbook
                    oxw.WriteEndElement();
                    oxw.Close();

                    xl.Close();

                    oxw = null;
                    oxa = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WriteExcelTemplate(string templatePath, string outputPath, string sheetName, List<dynamic> cellValueList)
        {
            byte[] byteArray = File.ReadAllBytes(templatePath);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                //using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileFullPath, false))
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, true))
                {
                    try
                    {
                        IEnumerable<Sheet> sheets = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == sheetName);
                        if (sheets.Count() == 0)
                        {
                            throw new Exception("Sheet Not Found");
                        }
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(relationshipId);

                        foreach (var cellValueDR in cellValueList)
                        {
                            int rowIndex = cellValueDR.ROW_SEQ;
                            string columnName = cellValueDR.COLUMN_EXCEL_POS;
                            string cellValue = cellValueDR.COLUMN_VALUE.ToString();

                            // sett cell datatype, cek jika cellvalue int atau bukan
                            var cellDataType = new EnumValue<CellValues>(CellValues.Number);
                            int output;
                            bool isNUmber = int.TryParse(cellValue, out output);
                            if (!isNUmber)
                                cellDataType = new EnumValue<CellValues>(CellValues.String);

                            try
                            {
                                Row row = worksheetPart.Worksheet.GetFirstChild<SheetData>().Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                                Cell cell = row.Elements<Cell>().Where(c => string.Compare(c.CellReference.Value, columnName + rowIndex, true) == 0).First();
                                cell.CellValue = new CellValue(cellValue);
                                cell.DataType = cellDataType;
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Equals("Sequence contains no elements"))
                                {
                                    uint ulastRowIndex = worksheetPart.Worksheet.GetFirstChild<SheetData>().Elements<Row>().Max(r => r.RowIndex);
                                    int lastRowIndex = Convert.ToInt32(ulastRowIndex);
                                    int selisihROw = rowIndex - lastRowIndex;

                                    SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                                    for (int i = 0; i < selisihROw; i++)
                                    {
                                        ulastRowIndex = ulastRowIndex + 1;
                                        sheetData.AppendChild(new Row() { RowIndex = ulastRowIndex });
                                    }

                                    Row newRow = new Row();
                                    newRow.RowIndex = ulastRowIndex;

                                    Cell newCell = new Cell() { CellReference = columnName + ulastRowIndex };
                                    newRow.Append(newCell);

                                    newCell.CellValue = new CellValue(cellValue);
                                    newCell.DataType = cellDataType;

                                    sheetData.Append(newRow);
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }

                        // set active sheet
                        //WorkbookView workbookView = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<BookViews>().GetFirstChild<WorkbookView>();
                        //workbookView.ActiveTab = UInt32.Parse(relationshipId);

                        worksheetPart.Worksheet.Save();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        spreadsheetDocument.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                        spreadsheetDocument.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                        spreadsheetDocument.Close();
                    }
                }
                File.WriteAllBytes(outputPath, stream.ToArray());
            }
        }

        public void WriteExcelDtlBatch(IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn, string formCode, string outputPath, string outputName, long totalRecord)
        {
            string guid = Guid.NewGuid().ToString();
            string tempPath = outputPath + "/" + guid;
            int maxRow = 0;
            int maxPage = 0;

            try
            {
                string query = "SELECT PAR_VALUE, PAR_DESC FROM SN_MS_SYSTEM_PARAM_DTL WHERE PAR_CD = 'GB217' AND PAR_DESC IN ('EXCEL_MAX_ROW', 'EXCEL_MAX_PAGE')";
                IDataReader rd = con.ExecuteReader(query);
                //using (IDataReader rd = ctx.Database.GetDbConnection().ExecuteReader(query))
                //{
                while (rd.Read())
                {
                    if (rd["PAR_DESC"].ToString() == "EXCEL_MAX_ROW")
                    {
                        maxRow = Int32.Parse(rd["PAR_VALUE"].ToString());
                    }
                    else
                    {
                        maxPage = Int32.Parse(rd["PAR_VALUE"].ToString());
                    }
                }
                rd.Close();
                rd.Dispose();
                rd = null;
                //}

                spParam.Add("@PAGE_SIZE", maxPage);

                Directory.CreateDirectory(tempPath);

                int maxLoop = (int)Math.Ceiling(((double)totalRecord / maxRow));
                if (totalRecord <= maxRow)
                {
                    maxLoop = 1;
                }
                int page = 0;
                for (int i = 0; i < maxLoop; i++)
                {
                    int maxLoopChild = (int)Math.Ceiling(((double)maxRow / maxPage));
                    if (totalRecord <= maxRow)
                    {
                        maxLoopChild = 1;
                    }
                    string fileFullPath = tempPath + "/" + Guid.NewGuid().ToString() + ".xlsx";
                    using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath, SpreadsheetDocumentType.Workbook))
                    {
                        List<OpenXmlAttribute> oxa;
                        OpenXmlWriter oxw;

                        xl.AddWorkbookPart();
                        WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                        oxw = OpenXmlWriter.Create(wsp);
                        oxw.WriteStartElement(new Worksheet());
                        oxw.WriteStartElement(new SheetData());

                        //set header column
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("r", null, "1"));
                        oxw.WriteStartElement(new Row(), oxa);
                        foreach (FormDetail item in listColumn) //header column
                        {
                            oxa = new List<OpenXmlAttribute>();
                            oxa.Add(new OpenXmlAttribute("t", null, "str"));
                            oxw.WriteStartElement(new Cell(), oxa);
                            oxw.WriteElement(new CellValue(item.GrdColumnName));
                            oxw.WriteEndElement();
                        }
                        oxw.WriteEndElement();

                        int rowIndex = 2;
                        for (int j = 0; j < maxLoopChild; j++)
                        {
                            spParam.Add("@PAGE", page);
                            IDataReader rows = con.ExecuteReader("DBO.UDPS_GET_TABLE_DETAIL", spParam, null, 6000, CommandType.StoredProcedure);
                            //using (IDataReader rows = await con.ExecuteReader("DBO.UDPS_GET_TABLE_DETAIL", spParam, null, null, CommandType.StoredProcedure))
                            //{
                            while (rows.Read())
                            {
                                oxa = new List<OpenXmlAttribute>();

                                // this is the row index
                                oxa.Add(new OpenXmlAttribute("r", null, rowIndex.ToString()));

                                oxw.WriteStartElement(new Row(), oxa);

                                foreach (FormDetail item in listColumn)
                                {
                                    oxa = new List<OpenXmlAttribute>();
                                    oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));
                                    oxw.WriteStartElement(new Cell(), oxa);
                                    object val = rows[item.FieldName];
                                    oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, val)));
                                    oxw.WriteEndElement();
                                }

                                // this is for Row
                                oxw.WriteEndElement();

                                page++;
                                rowIndex++;
                            }
                            rows.Close();
                            rows.Dispose();
                            rows = null;

                            if (page % maxPage != 0)
                            {
                                break;
                            }
                            //}
                        }

                        // this is for SheetData
                        oxw.WriteEndElement();
                        // this is for Worksheet
                        oxw.WriteEndElement();
                        oxw.Close();

                        oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                        oxw.WriteStartElement(new Workbook());
                        oxw.WriteStartElement(new Sheets());

                        oxw.WriteElement(new Sheet()
                        {
                            Name = "Sheet1",
                            SheetId = 1,
                            Id = xl.WorkbookPart.GetIdOfPart(wsp)
                        });

                        // this is for Sheets
                        oxw.WriteEndElement();
                        // this is for Workbook
                        oxw.WriteEndElement();
                        oxw.Close();

                        xl.Close();

                        oxw = null;
                        oxa = null;
                    }
                }

                ZipFile.CreateFromDirectory(
                    tempPath,
                    outputPath + outputName
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Directory.Delete(tempPath, true);
            }
        }

        public void WriteExcelBatch(
            IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn,
            string formCode, string outputPath, string outputName, long totalRecord,
            int maxRow, int maxPage)
        {
            string guid = Guid.NewGuid().ToString();
            string tempPath = outputPath + "/" + guid;

            try
            {
                //spParam.Add("@PAGESIZE", maxPage);

                Directory.CreateDirectory(tempPath);

                int maxLoop = (int)Math.Ceiling(((double)totalRecord / maxRow));
                if (totalRecord <= maxRow)
                {
                    maxLoop = 1;
                }
                int page = 0;
                for (int i = 0; i < maxLoop; i++)
                {
                    int maxLoopChild = (int)Math.Ceiling(((double)maxRow / maxPage));
                    if (totalRecord <= maxRow)
                    {
                        maxLoopChild = 1;
                    }
                    string fileFullPath = tempPath + "/" + Guid.NewGuid().ToString() + ".xlsx";
                    using (FileStream fs = new FileStream(fileFullPath, FileMode.OpenOrCreate))
                    {
                        using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fs, SpreadsheetDocumentType.Workbook))
                        {
                            List<OpenXmlAttribute> oxa;
                            OpenXmlWriter oxw;

                            xl.AddWorkbookPart();
                            WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                            oxw = OpenXmlWriter.Create(wsp);
                            oxw.WriteStartElement(new Worksheet());
                            oxw.WriteStartElement(new SheetData());

                            //set header column
                            oxa = new List<OpenXmlAttribute>();
                            oxa.Add(new OpenXmlAttribute("r", null, "1"));
                            oxw.WriteStartElement(new Row(), oxa);
                            foreach (FormDetail item in listColumn) //header column
                            {
                                oxa = new List<OpenXmlAttribute>();
                                oxa.Add(new OpenXmlAttribute("t", null, "str"));
                                oxw.WriteStartElement(new Cell(), oxa);
                                oxw.WriteElement(new CellValue(item.GrdColumnName));
                                oxw.WriteEndElement();
                            }
                            oxw.WriteEndElement();

                            int rowIndex = 2;
                            //for (int j = 0; j < maxLoopChild; j++)
                            //{
                            spParam.Add("@PAGE", i > 0 ? i * maxRow : 0);
                            spParam.Add("@PAGESIZE", maxRow);
                            IDataReader rows = con.ExecuteReader("DBO.UDPS_GET_FORM_DATA", spParam, null, 6000, CommandType.StoredProcedure);
                            while (rows.Read())
                            {
                                oxa = new List<OpenXmlAttribute>();

                                // this is the row index
                                oxa.Add(new OpenXmlAttribute("r", null, rowIndex.ToString()));

                                oxw.WriteStartElement(new Row(), oxa);

                                foreach (FormDetail item in listColumn)
                                {
                                    oxa = new List<OpenXmlAttribute>();
                                    oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));
                                    oxw.WriteStartElement(new Cell(), oxa);
                                    object val = rows[item.FieldName];
                                    oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, val)));
                                    oxw.WriteEndElement();
                                }

                                // this is for Row
                                oxw.WriteEndElement();

                                page++;
                                rowIndex++;
                            }
                            rows.Close();
                            rows.Dispose();
                            rows = null;

                            //if (page % maxPage != 0)
                            //{
                            //    break;
                            //}
                            //}
                            //}

                            // this is for SheetData
                            oxw.WriteEndElement();
                            // this is for Worksheet
                            oxw.WriteEndElement();
                            oxw.Close();

                            oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                            oxw.WriteStartElement(new Workbook());
                            oxw.WriteStartElement(new Sheets());

                            oxw.WriteElement(new Sheet()
                            {
                                Name = "Sheet1",
                                SheetId = 1,
                                Id = xl.WorkbookPart.GetIdOfPart(wsp)
                            });

                            // this is for Sheets
                            oxw.WriteEndElement();
                            // this is for Workbook
                            oxw.WriteEndElement();
                            oxw.Close();

                            xl.Close();

                            oxw = null;
                            oxa = null;
                        }
                    }
                }

                ZipFile.CreateFromDirectory(
                    tempPath,
                    outputPath + outputName
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Directory.Delete(tempPath, true);
            }
        }

        public void WriteExcelBatchOld(
            IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn,
            string formCode, string outputPath, string outputName, long totalRecord,
            int maxRow, int maxPage)
        {
            string guid = Guid.NewGuid().ToString();
            string tempPath = outputPath + "/" + guid;

            try
            {
                //spParam.Add("@PAGESIZE", maxPage);

                Directory.CreateDirectory(tempPath);

                int maxLoop = (int)Math.Ceiling(((double)totalRecord / maxRow));
                if (totalRecord <= maxRow)
                {
                    maxLoop = 1;
                }
                int page = 0;
                for (int i = 0; i < maxLoop; i++)
                {
                    int maxLoopChild = (int)Math.Ceiling(((double)maxRow / maxPage));
                    if (totalRecord <= maxRow)
                    {
                        maxLoopChild = 1;
                    }
                    string fileFullPath = tempPath + "/" + Guid.NewGuid().ToString() + ".xlsx";
                    using (SpreadsheetDocument xl = SpreadsheetDocument.Create(fileFullPath, SpreadsheetDocumentType.Workbook))
                    {
                        List<OpenXmlAttribute> oxa;
                        OpenXmlWriter oxw;

                        xl.AddWorkbookPart();
                        WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                        oxw = OpenXmlWriter.Create(wsp);
                        oxw.WriteStartElement(new Worksheet());
                        oxw.WriteStartElement(new SheetData());

                        //set header column
                        oxa = new List<OpenXmlAttribute>();
                        oxa.Add(new OpenXmlAttribute("r", null, "1"));
                        oxw.WriteStartElement(new Row(), oxa);
                        foreach (FormDetail item in listColumn) //header column
                        {
                            oxa = new List<OpenXmlAttribute>();
                            oxa.Add(new OpenXmlAttribute("t", null, "str"));
                            oxw.WriteStartElement(new Cell(), oxa);
                            oxw.WriteElement(new CellValue(item.GrdColumnName));
                            oxw.WriteEndElement();
                        }
                        oxw.WriteEndElement();

                        int rowIndex = 2;
                        for (int j = 0; j < maxLoopChild; j++)
                        {
                            spParam.Add("@PAGE", i > 0 ? i * maxRow : 0);
                            spParam.Add("@PAGESIZE", maxRow);
                            IDataReader rows = con.ExecuteReader("DBO.UDPS_GET_FORM_DATA", spParam, null, null, CommandType.StoredProcedure);
                            while (rows.Read())
                            {
                                oxa = new List<OpenXmlAttribute>();

                                // this is the row index
                                oxa.Add(new OpenXmlAttribute("r", null, rowIndex.ToString()));

                                oxw.WriteStartElement(new Row(), oxa);

                                foreach (FormDetail item in listColumn)
                                {
                                    oxa = new List<OpenXmlAttribute>();
                                    oxa.Add(new OpenXmlAttribute("t", null, GetExcelTypeAttr(item.EdrControlType)));
                                    oxw.WriteStartElement(new Cell(), oxa);
                                    object val = rows[item.FieldName];
                                    oxw.WriteElement(new CellValue(FormatValue(item.EdrControlType, val)));
                                    oxw.WriteEndElement();
                                }

                                // this is for Row
                                oxw.WriteEndElement();

                                page++;
                                rowIndex++;
                            }
                            rows.Close();
                            rows.Dispose();
                            rows = null;

                            if (page % maxPage != 0)
                            {
                                break;
                            }
                            //}
                        }

                        // this is for SheetData
                        oxw.WriteEndElement();
                        // this is for Worksheet
                        oxw.WriteEndElement();
                        oxw.Close();

                        oxw = OpenXmlWriter.Create(xl.WorkbookPart);
                        oxw.WriteStartElement(new Workbook());
                        oxw.WriteStartElement(new Sheets());

                        oxw.WriteElement(new Sheet()
                        {
                            Name = "Sheet1",
                            SheetId = 1,
                            Id = xl.WorkbookPart.GetIdOfPart(wsp)
                        });

                        // this is for Sheets
                        oxw.WriteEndElement();
                        // this is for Workbook
                        oxw.WriteEndElement();
                        oxw.Close();

                        xl.Close();

                        oxw = null;
                        oxa = null;
                    }
                }

                ZipFile.CreateFromDirectory(
                    tempPath,
                    outputPath + outputName
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Directory.Delete(tempPath, true);
            }
        }

        public string getCellColumnReference(int columnIndex)
        {
            string output = "A";
            if (columnIndex == 0) output = "A";
            if (columnIndex == 1) output = "B";
            if (columnIndex == 2) output = "C";
            if (columnIndex == 3) output = "D";
            if (columnIndex == 4) output = "E";
            if (columnIndex == 5) output = "F";
            if (columnIndex == 6) output = "G";
            if (columnIndex == 7) output = "H";
            if (columnIndex == 8) output = "I";
            if (columnIndex == 9) output = "J";
            if (columnIndex == 10) output = "K";
            if (columnIndex == 11) output = "L";
            if (columnIndex == 12) output = "M";
            if (columnIndex == 13) output = "N";
            if (columnIndex == 14) output = "O";
            if (columnIndex == 15) output = "P";
            if (columnIndex == 16) output = "Q";
            if (columnIndex == 17) output = "R";
            if (columnIndex == 18) output = "S";
            if (columnIndex == 19) output = "T";
            if (columnIndex == 20) output = "U";
            if (columnIndex == 21) output = "V";
            if (columnIndex == 22) output = "W";
            if (columnIndex == 23) output = "X";
            if (columnIndex == 24) output = "Y";
            if (columnIndex == 25) output = "Z";
            if (columnIndex == 26) output = "AA";
            if (columnIndex == 27) output = "AB";
            if (columnIndex == 28) output = "AC";
            if (columnIndex == 29) output = "AD";
            if (columnIndex == 30) output = "AE";
            if (columnIndex == 31) output = "AF";
            if (columnIndex == 32) output = "AG";
            if (columnIndex == 33) output = "AH";
            if (columnIndex == 34) output = "AI";
            if (columnIndex == 35) output = "AJ";
            if (columnIndex == 36) output = "AK";
            if (columnIndex == 37) output = "AL";
            if (columnIndex == 38) output = "AM";
            if (columnIndex == 39) output = "AN";
            if (columnIndex == 40) output = "AO";
            if (columnIndex == 41) output = "AP";
            if (columnIndex == 42) output = "AQ";
            if (columnIndex == 43) output = "AR";
            if (columnIndex == 44) output = "AS";
            if (columnIndex == 45) output = "AT";
            if (columnIndex == 46) output = "AU";
            if (columnIndex == 47) output = "AV";
            if (columnIndex == 48) output = "AW";
            if (columnIndex == 49) output = "AX";
            if (columnIndex == 50) output = "AY";
            if (columnIndex == 51) output = "AZ";
            return output;
        }

    }
}

public class CellValueMap
{
    public string field { get; set; }
    public string value { get; set; }
    public string caption { get; set; }
    public string type { get; set; }
}

public class CalculateNeracaLabaRugi
{
    public string ReportDT { get; set; }
    public string Branch { get; set; }
}