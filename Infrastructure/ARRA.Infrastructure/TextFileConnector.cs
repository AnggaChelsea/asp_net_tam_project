﻿using ARRA.GLOBAL.App.Interfaces;
using ARRA.GLOBAL.Domain.Entities;
using ARRA.GLOBAL.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using ARRA.Common.Model;
using Dapper;
using ARRA.Common.Enumerations;
using System.Transactions;
//using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators.Internal;
//using DocumentFormat.OpenXml.Drawing.Wordprocessing;

namespace ARRA.Infrastructure
{
    public class TextFileConnector : ITextFileConnector
    {
        #region Helper
        private string FormatValueCsv(string ctlType, object value)
        {
            string result = "";
            if (value != null)
            {
                if (ctlType == ControlType.DATE.ToString())
                {
                    result = Convert.ToDateTime(value).ToString("yyyy-MM-dd");
                }
                else
                {
                    result = Convert.ToString(value).Replace("\"", "\"\"");
                }
            }
            return "\"" + result + "\"";
        }
        private object GetValueDynamic(string field, dynamic d)
        {
            return d.GetType().GetProperty(field).GetValue(d, null);
        }
        #endregion
        #region Export Screen
        public void WriteCSV(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        foreach (FormDetail item in listColumn)
                        {
                            row += item.GrdColumnName + ",";
                        }

                        row = row.Substring(0, row.Length - 1);
                        wt.WriteLine(row);

                        while (rd.Read())
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                row += FormatValueCsv(item.EdrControlType, rd[item.FieldName] == DBNull.Value ? null : rd[item.FieldName]) + ",";
                            }

                            row = row.Substring(0, row.Length - 1);

                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteCSV(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        foreach (FormDetail item in listColumn)
                        {
                            row += item.GrdColumnName + ",";
                        }

                        row = row.Substring(0, row.Length - 1);
                        wt.WriteLine(row);

                        for (int d = 0; d < ls.Count; d++)
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                object val = this.GetValueDynamic(item.FieldName, ls[d]);
                                row += FormatValueCsv(item.EdrControlType, val == DBNull.Value ? null : val) + ",";
                            }

                            row = row.Substring(0, row.Length - 1);

                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WriteCSVBatch(IDbConnection con, DynamicParameters spParam, IList<FormDetail> listColumn, string formCode, string outputPath, string outputName, long totalRecord, int maxRow, int maxPage)
        {
            string guid = Guid.NewGuid().ToString();
            string tempPath = outputPath + "/" + guid;
            try
            {
                //spParam.Add("@PAGESIZE", maxPage);
                Directory.CreateDirectory(tempPath);
                int maxLoop = (int)Math.Ceiling(((double)totalRecord / maxRow));
                if (totalRecord <= maxRow)
                {
                    maxLoop = 1;
                }
                int page = 0;
                for (int i = 0; i < maxLoop; i++)
                {
                    int maxLoopChild = (int)Math.Ceiling(((double)maxRow / maxPage));
                    if (totalRecord <= maxRow)
                    {
                        maxLoopChild = 1;
                    }

                    string fileFullPath = tempPath + "/" + Guid.NewGuid().ToString() + ".csv";
                    using (StreamWriter wt = new StreamWriter(fileFullPath))
                    {
                        try
                        {
                            string row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                row += item.GrdColumnName + ",";
                            }

                            row = row.Substring(0, row.Length - 1);
                            wt.WriteLine(row);

                            int rowIndex = 2;
                            for (int j = 0; j < maxLoopChild; j++)
                            {
                                spParam.Add("@PAGE", i > 0 ? i * maxRow : 0);
                                spParam.Add("@PAGESIZE", maxRow);
                                IDataReader rows = con.ExecuteReader("DBO.UDPS_GET_FORM_DATA", spParam, null, null, CommandType.StoredProcedure);
                                while (rows.Read())
                                {
                                    row = "";
                                    foreach (FormDetail item in listColumn)
                                    {
                                        object val = rows[item.FieldName];
                                        row += FormatValueCsv(item.EdrControlType, val == DBNull.Value ? null : val) + ",";
                                    }

                                    row = row.Substring(0, row.Length - 1);

                                    wt.WriteLine(row);
                                    
                                    page++;
                                    rowIndex++;
                                }

                                rows.Close();
                                rows.Dispose();
                                rows = null;

                                if (page % maxPage != 0)
                                {
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            wt.Close();
                        }
                    }
                }

                ZipFile.CreateFromDirectory(
                    tempPath,
                    outputPath + outputName
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteFixLength(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                IList<FormDetail> ftr = listColumn.Where(f => f.TFShow == true).ToList();

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        if (ftr.Count > 0)
                        {
                            string row = "";
                            while (rd.Read())
                            {
                                row = "";
                                foreach (FormDetail item in ftr)
                                {
                                    row += FormatValueFixLength(item.EdrControlType, item.TFFormat,
                                        item.TFLength, item.TFReplicateChar, item.TFAbsolute, item.TFAlignment, rd[item.FieldName] == DBNull.Value ? null : rd[item.FieldName]);
                                }

                                row = row.Substring(0, row.Length - 1);

                                wt.WriteLine(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteFixLength(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                IList<FormDetail> ftr = listColumn.Where(f => f.TFShow == true).ToList();

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        if (ftr.Count > 0)
                        {
                            string row = "";
                            for (int d = 0; d < ls.Count; d++)
                            {
                                row = "";
                                foreach (FormDetail item in ftr)
                                {
                                    object val = this.GetValueDynamic(item.FieldName, ls[d]);
                                    row += FormatValueFixLength(item.EdrControlType, item.TFFormat,
                                        item.TFLength, item.TFReplicateChar, item.TFAbsolute, item.TFAlignment, val == DBNull.Value ? null : val);
                                }

                                row = row.Substring(0, row.Length - 1);

                                wt.WriteLine(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string FormatValueFixLength(string ctlType, string format, Int16? length,
            string replicateChar, bool? isAbsolute, string align, object value)
        {
            try
            {
                string result = Convert.ToString(value);

                if (Convert.ToBoolean(isAbsolute) && (
                    ctlType == ControlType.MONEY.ToString())
                    )
                {
                    result = Math.Abs(Convert.ToDecimal(value)).ToString();
                }


                //format
                if (value != null)
                {
                    if (ctlType == ControlType.DATE.ToString() ||
                        ctlType == ControlType.DATETIME.ToString())
                    {
                        if (!string.IsNullOrEmpty(format))
                        {
                            result = Convert.ToDateTime(value).ToString(format);
                        }
                        else
                        {
                            if (ctlType == ControlType.DATE.ToString())
                            {
                                result = Convert.ToDateTime(value).ToString("yyyy-MM-dd"); //default
                            }
                        }
                    }
                    else if (ctlType == ControlType.MONEY.ToString() ||
                        ctlType == ControlType.NUMBER.ToString() ||
                        ctlType == ControlType.PERCENTAGE.ToString()
                       )
                    {
                        if (!string.IsNullOrEmpty(format))
                        {
                            result = Convert.ToDecimal(value).ToString(format);
                        }
                    }
                }

                int len = Convert.ToInt32(length) - result.Length;
                if (len > 0)
                {
                    if (string.IsNullOrEmpty(align))
                    {
                        result = string.Concat(Enumerable.Repeat(replicateChar, len)) + result; //default
                    }
                    else
                    {
                        if (align.ToUpper() == "R")
                        {
                            result = string.Concat(Enumerable.Repeat(replicateChar, len)) + result;
                        }
                        else
                        {
                            result = result + string.Concat(Enumerable.Repeat(replicateChar, len));
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteDelimited(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter)
        {
            try
            {
                if (delimiter == null)
                {
                    delimiter = "|"; //default.
                }

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        foreach (FormDetail item in listColumn)
                        {
                            row += item.GrdColumnName + delimiter;
                        }

                        row = row.Substring(0, row.Length - delimiter.Length);
                        wt.WriteLine(row);

                        while (rd.Read())
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                if (item.EdrControlType == ControlType.DATE.ToString())
                                {
                                    row += Convert.ToString(rd[item.FieldName] == DBNull.Value ? null : Convert.ToDateTime(rd[item.FieldName]).ToString("yyyy-MM-dd")) + delimiter;
                                }
                                else
                                {
                                    row += Convert.ToString(rd[item.FieldName] == DBNull.Value ? null : rd[item.FieldName]) + delimiter;
                                }
                            }

                            row = row.Substring(0, row.Length - delimiter.Length);

                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteDelimited(IList<dynamic> ls, IList<FormDetail> listColumn, string fileFullPath, string delimiter)
        {
            try
            {
                if (delimiter == null)
                {
                    delimiter = "|"; //default.
                }

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        foreach (FormDetail item in listColumn)
                        {
                            row += item.GrdColumnName + delimiter;
                        }

                        row = row.Substring(0, row.Length - delimiter.Length);
                        wt.WriteLine(row);

                        for (int d = 0; d < ls.Count; d++)
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                object val = this.GetValueDynamic(item.FieldName, ls[d]);

                                if (item.EdrControlType == ControlType.DATE.ToString())
                                {
                                    row += Convert.ToString(val == DBNull.Value ? null : Convert.ToDateTime(val).ToString("yyyy-MM-dd")) + delimiter;
                                }
                                else
                                {
                                    row += Convert.ToString(val == DBNull.Value ? null : val) + delimiter;
                                }
                            }

                            row = row.Substring(0, row.Length - delimiter.Length);

                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Regulator Text File
        private string FormatValueRegulator(string ctlType, object value, string format)
        {
            string result = "";
            if (value != null && value != DBNull.Value)
            {
                if (ctlType == ControlType.DATE.ToString() && !string.IsNullOrEmpty(format))
                {
                    result = Convert.ToDateTime(value).ToString(format);
                }
                else if ((ctlType == ControlType.MONEY.ToString() || ctlType == ControlType.NUMBER.ToString()) && !string.IsNullOrEmpty(format))
                {
                    result = Convert.ToDecimal(value).ToString(format);
                }
                else
                {
                    result = Convert.ToString(value);
                }
            }
            return result;
        }

        public void WriteCSVRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter, bool hasHeader)
        {
            try
            {
                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        if (hasHeader)
                        {
                            foreach (FormDetail item in listColumn)
                            {
                                row += item.IdElement + delimiter;
                            }
                            row = row.Substring(0, row.Length - 1);
                            wt.WriteLine(row);
                        }

                        while (rd.Read())
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                row += FormatValueRegulator(item.EdrControlType, rd[item.FieldName], item.TFFormat) + delimiter;
                            }
                            row = row.Substring(0, row.Length - 1);
                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteDelimitedRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter, bool hasHeader)
        {
            try
            {
                if (delimiter == null)
                {
                    delimiter = "|"; //default.
                }

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        string row = "";
                        if (hasHeader)
                        {
                            foreach (FormDetail item in listColumn)
                            {
                                row += item.IdElement + delimiter;
                            }
                            row = row.Substring(0, row.Length - 1);
                            wt.WriteLine(row);
                        }

                        while (rd.Read())
                        {
                            row = "";
                            foreach (FormDetail item in listColumn)
                            {
                                row += FormatValueRegulator(item.EdrControlType, rd[item.FieldName], item.TFFormat) + delimiter;
                            }
                            row = row.Substring(0, row.Length - delimiter.Length);
                            wt.WriteLine(row);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void WriteFixLengthRegulator(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath)
        {
            try
            {
                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    try
                    {
                        if (listColumn.Count > 0)
                        {
                            string row = "";
                            while (rd.Read())
                            {
                                row = "";
                                foreach (FormDetail item in listColumn)
                                {
                                    row += FormatValueFixLength(item.EdrControlType, item.TFFormat,
                                        item.TFLength, item.TFReplicateChar, item.TFAbsolute, item.TFAlignment, rd[item.FieldName] == DBNull.Value ? null : rd[item.FieldName]);
                                }
                                row = row.Substring(0, row.Length - 1);

                                wt.WriteLine(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        wt.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> WriteDelimitedRegulatorWithHdrAndSplit(IDataReader rd, IList<FormDetail> listColumn, string fileFullPath, string delimiter, string header, int maxRow, long totalRecord)
        {
            List<string> fileName = new List<string>();
            try
            {
                if (delimiter == null)
                {
                    delimiter = "|"; //default.
                }
                int seq = 1;

                string file = fileFullPath.Replace("{seq}", seq.ToString());

                string row = "";
                StreamWriter wt = new StreamWriter(file);
                string hdr = "";

                if (totalRecord <= maxRow)
                {
                    hdr = header.Replace("{totalRow}", totalRecord.ToString());
                    fileName.Add(new FileInfo(file).Name + "|" + totalRecord.ToString());
                }
                else
                {
                    hdr = header.Replace("{totalRow}", maxRow.ToString());
                    fileName.Add(new FileInfo(file).Name + "|" + maxRow.ToString());
                }
                wt.WriteLine(hdr);
                long counter = 0;
                while (rd.Read())
                {
                    row = "";

                    if (counter != 0)
                    {
                        if (counter % maxRow == 0)
                        {
                            if (wt != null)//close prev file.
                            {
                                wt.Close();
                                wt.Dispose();
                            }

                            seq++;
                            file = fileFullPath.Replace("{seq}", seq.ToString());

                            wt = new StreamWriter(file);

                            long totRow = totalRecord - counter;
                            if (totRow <= maxRow)
                            {
                                hdr = header.Replace("{totalRow}", totRow.ToString());
                                fileName.Add(new FileInfo(file).Name + "|" + totRow.ToString());
                            }
                            else
                            {
                                hdr = header.Replace("{totalRow}", maxRow.ToString());
                                fileName.Add(new FileInfo(file).Name + "|" + maxRow.ToString());
                            }

                            wt.WriteLine(hdr);
                        }
                    }

                    foreach (FormDetail item in listColumn)
                    {
                        object val;
                        if (string.IsNullOrEmpty(item.TFDefaultValue))
                        {
                            val = rd[item.FieldName];
                        }
                        else
                        {
                            val = item.TFDefaultValue;
                        }

                        row += FormatValueRegulator(item.EdrControlType, val, item.TFFormat) + delimiter;
                    }
                    row = row.Substring(0, row.Length - delimiter.Length);
                    wt.WriteLine(row);

                    counter++;
                }

                if (wt != null)
                {
                    wt.Close();
                    wt.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fileName;
        }

        #endregion
        #region ManualUpload
        public async Task<long> ManualUpload(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId, string actionType, string delimiter)
        {
            delimiter = string.IsNullOrEmpty(delimiter) ? "|" : delimiter;
            long line = 1;
            try
            {
                int hdrRowCount = 0;
                long errCtr = 0;
                string curDate = new MachineDateTime().Now.ToString();
                List<TMP_FIELD_MAP> hdrFields = new List<TMP_FIELD_MAP>();
                using (StreamReader rd = new StreamReader(fileFullPath))
                {
                    string insert = "", update = "", delete = "", qryConds = "", qryInsVal = "", qryUpd = "", qryIns = "";
                    DynamicParameters p = new DynamicParameters();
                    while (!rd.EndOfStream)
                    {
                        insert = update = delete = qryConds = qryInsVal = qryUpd = qryIns = "";
                        string txt = rd.ReadLine();
                        if (!string.IsNullOrEmpty(txt))
                        {
                            string[] strs = txt.Split(delimiter);
                            if (line == 0)//header
                            {
                                strs = txt.ToLower().Split(delimiter);
                                hdrRowCount = strs.Length;
                                for (int i = 0; i < strs.Length; i++)
                                {
                                    ManualUploadDetail column = listColumn.Where(f => f.SourceColumn.ToLower() == strs[i]).FirstOrDefault();
                                    if (column == null)
                                    {
                                        throw new Exception("Column " + strs[i] + " Not Found, Please check your file format.");
                                    }
                                    else
                                    {
                                        hdrFields.Add(new TMP_FIELD_MAP { field = column.TargetColumn, isKey = column.IsKey });
                                    }
                                    column = null;
                                }

                                if (listColumn.Where(f => f.IsKey == true).Count() != listColumn.Where(f => f.IsKey == true && strs.Contains(f.SourceColumn.ToLower())).Count())
                                {
                                    throw new Exception("All key column required");
                                }
                            }
                            else
                            {
                                if (strs.Length == hdrRowCount)
                                {
                                    try
                                    {
                                        p = new DynamicParameters();
                                        for (int i = 0; i < hdrFields.Count; i++)
                                        {
                                            TMP_FIELD_MAP col = hdrFields[i];
                                            string val = strs[i].Trim();
                                            if (actionType == OperationType.ADD.ToString())
                                            {
                                                qryIns += col.field + ",";
                                                qryInsVal += "@" + col.field + ",";
                                                p.Add("@" + col.field, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                            }
                                            else if (actionType == OperationType.EDIT.ToString())
                                            {
                                                if (col.isKey == true)
                                                {
                                                    qryConds += col.field + "=@" + col.field + " AND ";
                                                }
                                                else
                                                {
                                                    qryUpd += col.field + "=@" + col.field + ",";
                                                }
                                                p.Add("@" + col.field, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                            }
                                            else if (actionType == OperationType.REMOVE.ToString())
                                            {
                                                if (col.isKey == true)
                                                {
                                                    qryConds += col.field + "=@" + col.field + " AND ";
                                                    p.Add("@" + col.field, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                                }
                                            }
                                            else if (actionType == OperationType.REPLACE.ToString())
                                            { //delete insert
                                                qryIns += col.field + ",";
                                                qryInsVal += "@" + col.field + ",";
                                                p.Add("@" + col.field, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                                if (col.isKey == true)
                                                {
                                                    qryConds += col.field + "=@" + col.field + " AND ";
                                                }
                                            }
                                        }

                                        if (actionType == OperationType.ADD.ToString())
                                        {
                                            insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                            await con.ExecuteAsync(insert, p);
                                        }
                                        else if (actionType == OperationType.EDIT.ToString())
                                        {
                                            if (qryConds != "")
                                            {
                                                update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                await con.ExecuteAsync(update, p);
                                            }
                                        }
                                        else if (actionType == OperationType.REMOVE.ToString())
                                        {
                                            if (qryConds != "")
                                            {
                                                delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                await con.ExecuteAsync(delete, p);
                                            }
                                        }
                                        else if (actionType == OperationType.REPLACE.ToString())
                                        { //delete insert
                                            if (qryConds != "")
                                            {
                                                delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                                insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                                await con.ExecuteAsync(delete + " " + insert, p);
                                            }
                                        }
                                        p = null;

                                        if (line % 1000 == 0)
                                        {
                                            p = new DynamicParameters();
                                            p.Add("@UID", logId);
                                            p.Add("@TOTAL_PROCEED", line);
                                            p.Add("@TOTAL_FAILED", errCtr);
                                            await con.ExecuteAsync("UPDATE TR_IMPORT_FILE_LIST_HDR SET TOTAL_PROCEED=@TOTAL_PROCEED,TOTAL_FAILED=@TOTAL_FAILED WHERE UID=@UID ", p);
                                            p = null;
                                            errCtr++;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        p = new DynamicParameters();
                                        p.Add("@HDR_ID", logId);
                                        p.Add("@ROW_NUM", line + 1);
                                        p.Add("@ERR_DESC", ex.Message);
                                        p.Add("@CREATED_BY", user.UserId);
                                        p.Add("@CREATED_DT", curDate);
                                        p.Add("@CREATED_HOST", user.Host);
                                        await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                        p = null;
                                        errCtr++;
                                    }
                                }
                                else
                                {
                                    p = new DynamicParameters();
                                    p.Add("@HDR_ID", logId);
                                    p.Add("@ROW_NUM", line + 1);
                                    p.Add("@ERR_DESC", "Column does not match with header column count");
                                    p.Add("@CREATED_BY", user.UserId);
                                    p.Add("@CREATED_DT", curDate);
                                    p.Add("@CREATED_HOST", user.Host);
                                    await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                    p = null;
                                    errCtr++;
                                }
                            }
                        }
                        else
                        {
                            p = new DynamicParameters();
                            p.Add("@HDR_ID", logId);
                            p.Add("@ROW_NUM", line + 1);
                            p.Add("@ERR_DESC", "Empty Line");
                            p.Add("@CREATED_BY", user.UserId);
                            p.Add("@CREATED_DT", curDate);
                            p.Add("@CREATED_HOST", user.Host);
                            await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                            p = null;
                            errCtr++;
                        }

                        line++;
                    }
                    rd.Close();
                    rd.Dispose();
                }

                hdrFields = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return line - 1;
        }

        public async Task<long> ManualUploadFixLength(IDbConnection con, UserIdentityModel user, string tableName, IList<ManualUploadDetail> listColumn, string fileFullPath, long logId, string actionType)
        {
            long line = 1;
            try
            {
                long errCtr = 0;
                string curDate = new MachineDateTime().Now.ToString();
                int colLength = 0;
                foreach (ManualUploadDetail d in listColumn)
                {
                    colLength += Convert.ToInt32(d.TFLength);
                }

                listColumn = listColumn.OrderBy(o => o.TFSeq).ToList();
                using (StreamReader rd = new StreamReader(fileFullPath))
                {
                    string insert = "", update = "", delete = "", qryConds = "", qryInsVal = "", qryUpd = "", qryIns = "";
                    DynamicParameters p = new DynamicParameters();
                    while (!rd.EndOfStream)
                    {
                        insert = update = delete = qryConds = qryInsVal = qryUpd = qryIns = "";
                        string txt = rd.ReadLine();
                        if (!string.IsNullOrEmpty(txt))
                        {
                            try
                            {
                                if (txt.Length == colLength)
                                {
                                    p = new DynamicParameters();
                                    int startPos = 0;

                                    for (int i = 0; i < listColumn.Count; i++)
                                    {
                                        ManualUploadDetail col = listColumn[i];
                                        string val = txt.Substring(startPos, Convert.ToInt32(col.TFLength)).Trim();
                                        startPos += Convert.ToInt32(col.TFLength);

                                        if (actionType == OperationType.ADD.ToString())
                                        {
                                            qryIns += col.TargetColumn + ",";
                                            qryInsVal += "@" + col.TargetColumn + ",";
                                            p.Add("@" + col.TargetColumn, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                        }
                                        else if (actionType == OperationType.EDIT.ToString())
                                        {
                                            if (col.IsKey == true)
                                            {
                                                qryConds += col.TargetColumn + "=@" + col.TargetColumn + " AND ";
                                            }
                                            else
                                            {
                                                qryUpd += col.TargetColumn + "=@" + col.TargetColumn + ",";
                                            }
                                            p.Add("@" + col.TargetColumn, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                        }
                                        else if (actionType == OperationType.REMOVE.ToString())
                                        {
                                            if (col.IsKey == true)
                                            {
                                                qryConds += col.TargetColumn + "=@" + col.TargetColumn + " AND ";
                                                p.Add("@" + col.TargetColumn, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                            }
                                        }
                                        else if (actionType == OperationType.REPLACE.ToString())
                                        { //delete insert
                                            qryIns += col.TargetColumn + ",";
                                            qryInsVal += "@" + col.TargetColumn + ",";
                                            p.Add("@" + col.TargetColumn, val == "" ? null : ARRA.Common.Utility.SafeSqlString(val));
                                            if (col.IsKey == true)
                                            {
                                                qryConds += col.TargetColumn + "=@" + col.TargetColumn + " AND ";
                                            }
                                        }
                                    }

                                    if (actionType == OperationType.ADD.ToString())
                                    {
                                        insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                        await con.ExecuteAsync(insert, p);
                                    }
                                    else if (actionType == OperationType.EDIT.ToString())
                                    {
                                        if (qryConds != "")
                                        {
                                            update = "UPDATE " + tableName + " SET " + qryUpd + "MODIFIED_BY='" + user.UserId + "',MODIFIED_DT='" + curDate + "',MODIFIED_HOST='" + user.Host + "' WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                            await con.ExecuteAsync(update, p);
                                        }
                                    }
                                    else if (actionType == OperationType.REMOVE.ToString())
                                    {
                                        if (qryConds != "")
                                        {
                                            delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                            await con.ExecuteAsync(delete, p);
                                        }
                                    }
                                    else if (actionType == OperationType.REPLACE.ToString())
                                    { //delete insert
                                        if (qryConds != "")
                                        {
                                            delete = "DELETE " + tableName + " WHERE " + qryConds.Substring(0, qryConds.Length - 4);
                                            insert = "INSERT INTO " + tableName + "(" + qryIns + "CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(" + qryInsVal + "'" + user.UserId + "','" + curDate + "','" + user.Host + "')";
                                            await con.ExecuteAsync(delete + " " + insert, p);
                                        }
                                    }
                                    p = null;

                                    if (line % 1000 == 0)
                                    {
                                        p = new DynamicParameters();
                                        p.Add("@UID", logId);
                                        p.Add("@TOTAL_PROCEED", line);
                                        p.Add("@TOTAL_FAILED", errCtr);
                                        await con.ExecuteAsync("UPDATE TR_IMPORT_FILE_LIST_HDR SET TOTAL_PROCEED=@TOTAL_PROCEED,TOTAL_FAILED=@TOTAL_FAILED WHERE UID=@UID ", p);
                                        p = null;
                                        errCtr++;
                                    }
                                }
                                else
                                {
                                    p = new DynamicParameters();
                                    p.Add("@HDR_ID", logId);
                                    p.Add("@ROW_NUM", line + 1);
                                    p.Add("@ERR_DESC", "Invalid row length");
                                    p.Add("@CREATED_BY", user.UserId);
                                    p.Add("@CREATED_DT", curDate);
                                    p.Add("@CREATED_HOST", user.Host);
                                    await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                    p = null;
                                    errCtr++;
                                }
                            }
                            catch (Exception ex)
                            {
                                p = new DynamicParameters();
                                p.Add("@HDR_ID", logId);
                                p.Add("@ROW_NUM", line + 1);
                                p.Add("@ERR_DESC", ex.Message);
                                p.Add("@CREATED_BY", user.UserId);
                                p.Add("@CREATED_DT", curDate);
                                p.Add("@CREATED_HOST", user.Host);
                                await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                                p = null;
                                errCtr++;
                            }

                        }
                        else
                        {
                            p = new DynamicParameters();
                            p.Add("@HDR_ID", logId);
                            p.Add("@ROW_NUM", line + 1);
                            p.Add("@ERR_DESC", "Empty Line");
                            p.Add("@CREATED_BY", user.UserId);
                            p.Add("@CREATED_DT", curDate);
                            p.Add("@CREATED_HOST", user.Host);
                            await con.ExecuteAsync("INSERT INTO TR_IMPORT_FILE_LIST_DTL(HDR_ID,ROW_NUM,ERR_DESC,CREATED_BY,CREATED_DT,CREATED_HOST) VALUES(@HDR_ID,@ROW_NUM,@ERR_DESC,@CREATED_BY,@CREATED_DT,@CREATED_HOST)", p);
                            p = null;
                            errCtr++;
                        }

                        line++;
                    }
                    rd.Close();
                    rd.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return line - 1;
        }
        #endregion

        public async Task<long> UploadErrorUJK(IDbConnection con, UserIdentityModel user, string filePath, string reportDate, string formCode)
        {
            long line = 1;
            string qryInsertError = string.Empty;

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (StreamReader rd = new StreamReader(filePath))
                    {
                        DataTable dtFormColumn = new DataTable();
                        string qryFormColumn = string.Format(@"SELECT FORM_CD
                                                            , FIELD_NM
                                                            , GRD_COL_NM
                                                            , EDR_DATA_TP
                                                        FROM SN_MS_FORM_DTL
                                                        WHERE FORM_CD = '{0}'
                                                            AND TF_SHOW = 1
                                                        ORDER BY GRD_COL_SEQ", formCode);
                        IDataReader drFormColumn = await con.ExecuteReaderAsync(qryFormColumn);
                        dtFormColumn.Load(drFormColumn);

                        int hdrId = 0;
                        int errBatch = 0;
                        int tblUid = 0;
                        string errFormTable = string.Empty;

                        // get table error form
                        string qryMsErrOjk = string.Format(@"SELECT TABLE_NM FROM MS_ERROR_OJK WHERE FORM_CD = '{0}';", formCode);
                        errFormTable = (await con.ExecuteScalarAsync(qryMsErrOjk)).ToString();

                        // get error batch
                        //string qryErrorBatch = string.Format(@"SELECT isnull(max(ERR_BATCH), 0) + 1 AS BATCH
                        //                                FROM TR_ERROR_OJK_HDR
                        //                                WHERE FORM_CD = '{0}'
                        //                                 AND REPORT_DT = '{1}'", formCode, reportDate);

                        string qryErrorBatch = string.Format(@"SELECT isnull(max(ERR_BATCH), 0) + 1 AS BATCH
                                                        FROM {0}
                                                        WHERE REPORT_DT = '{1}'", errFormTable, reportDate);

                        errBatch = Convert.ToInt32(await con.ExecuteScalarAsync(qryErrorBatch));

                        while (!rd.EndOfStream)
                        {
                            string txt = rd.ReadLine();
                            if (!string.IsNullOrEmpty(txt))
                            {
                                if (IsErrorHeader(txt))
                                {
                                    var arrayTxt = txt.Split('|');

                                    // insert error ojk header
                                    string qryInsertHdr = string.Format(@"INSERT INTO TR_ERROR_OJK_HDR (
	                                                                    FORM_CD
	                                                                    ,REPORT_DT
	                                                                    ,KEY_FIELD
	                                                                    ,KEY_VALUE
	                                                                    ,ERR_BATCH
	                                                                    ,CREATED_BY
	                                                                    ,CREATED_DT
	                                                                    ,CREATED_HOST
	                                                                    )
                                                                    OUTPUT inserted.UID
                                                                    VALUES (
	                                                                    '{0}'
	                                                                    ,'{1}'
	                                                                    ,'{2}'
	                                                                    ,'{3}'
	                                                                    ,{4}
	                                                                    ,'{5}'
	                                                                    ,GETDATE()
	                                                                    ,'{6}'
	                                                                    )", formCode, reportDate, dtFormColumn.Rows[0].ItemArray[1].ToString(), arrayTxt[2], errBatch, user.UserId, user.Host);

                                    hdrId = Convert.ToInt32(await con.ExecuteScalarAsync(qryInsertHdr));

                                    // insert error ojk detail value
                                    int arryDetailValue = 3;
                                    string detailValue = string.Empty;
                                    string fieldList = string.Empty;
                                    string valueList = string.Empty;
                                    foreach (DataRow item in dtFormColumn.Rows)
                                    {
                                        try
                                        {
                                            detailValue = arrayTxt[arryDetailValue].ToString();
                                        }
                                        catch
                                        {
                                            detailValue = string.Empty;
                                        }

                                        string qryInsertDtlValue = string.Format(@"INSERT INTO TR_ERROR_OJK_DTL (
	                                                                            HDR_ID
	                                                                            ,FIELD_NM
                                                                                ,FIELD_DESC
	                                                                            ,FIELD_VALUE
	                                                                            ,CREATED_BY
	                                                                            ,CREATED_DT
	                                                                            ,CREATED_HOST
	                                                                            )
                                                                            VALUES (
	                                                                            {0}
	                                                                            ,'{1}'
	                                                                            ,'{2}'
                                                                                ,'{3}'
	                                                                            ,'{4}'
	                                                                            ,getdate()
	                                                                            ,'{5}'
	                                                                            )", hdrId, item[1], item[2], detailValue, user.UserId, user.Host);
                                        await con.ExecuteAsync(qryInsertDtlValue);

                                        fieldList += item[1] + ",";
                                        if (item[3].ToString() == "NUMBER" || item[3].ToString() == "MONEY")
                                            valueList += "" + (detailValue == string.Empty ? "0" : detailValue) + ",";
                                        else if (item[3].ToString().Contains("DATE"))
                                        {
                                            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
                                            if (detailValue == string.Empty)
                                                valueList += "NULL,";
                                            else
                                                valueList += "'" + DateTime.ParseExact(detailValue, "yyyyMMdd", provider).ToString("yyyy-MM-dd") + "',";
                                        }
                                        else
                                            valueList += "'" + detailValue + "',";

                                        arryDetailValue++;
                                    }

                                    fieldList = "ERR_BATCH, REPORT_DT, " + fieldList.Substring(0, fieldList.Length - 1);
                                    valueList = errBatch + ", '" + reportDate + "', " + valueList.Substring(0, valueList.Length - 1);
                                    qryInsertError = string.Format(@"INSERT INTO {0} ({1}) OUTPUT inserted.UID VALUES ({2})", errFormTable, fieldList, valueList);
                                    tblUid = Convert.ToInt32(await con.ExecuteScalarAsync(qryInsertError));

                                    string qryUpdateErrHdr = string.Format(@"UPDATE TR_ERROR_OJK_HDR SET TBL_UID = {0} WHERE [UID] = {1}", tblUid, hdrId);
                                    await con.ExecuteAsync(qryUpdateErrHdr);
                                }
                                else if (IsErrorDetail(txt))
                                {
                                    var arrayDtl = txt.Split('|');

                                    string errorField = string.Empty;
                                    string errorValue = string.Empty;

                                    try { errorField = arrayDtl[0] + " " + arrayDtl[1]; }
                                    catch { errorField = string.Empty; }

                                    try { errorValue = arrayDtl[2]; } catch { errorValue = string.Empty; }

                                    // insert error ojk detail error
                                    string qryInsertDtlError = string.Format(@"INSERT INTO TR_ERROR_OJK_DTL (
	                                                                            HDR_ID
	                                                                            ,FIELD_NM
	                                                                            ,FIELD_DESC
	                                                                            ,FIELD_VALUE
	                                                                            ,CREATED_BY
	                                                                            ,CREATED_DT
	                                                                            ,CREATED_HOST
	                                                                            )
                                                                            VALUES (
	                                                                            {0}
	                                                                            ,'{1}'
                                                                                ,''
	                                                                            ,'{2}'
	                                                                            ,'{3}'
	                                                                            ,getdate()
	                                                                            ,'{4}'
	                                                                            )", hdrId, errorField, errorValue, user.UserId, user.Host);
                                    await con.ExecuteAsync(qryInsertDtlError);
                                }
                            }
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return line - 1;
        }

        public async Task<long> UploadDbl(IDbConnection con, UserIdentityModel user, string filePath, string reportDate, string formCode)
        {
            long line = 1;
            string qryInsertDbl = string.Empty;

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (StreamReader rd = new StreamReader(filePath))
                    {
                        int dblBatch = 0;

                        // get error batch
                        string qryDblBatch = string.Format(@"SELECT isnull(max(BATCH), 0) + 1 AS BATCH
                                                        FROM TR_DBL
                                                        WHERE FACILITY_TYPE_CODE = '{0}'
	                                                        AND REPORT_DT = '{1}'", formCode, reportDate);

                        dblBatch = Convert.ToInt32(await con.ExecuteScalarAsync(qryDblBatch));

                        // read first line (header)
                        string headerLine = rd.ReadLine();
                        var arrayHeader = headerLine.Split('|');

                        while (!rd.EndOfStream)
                        {
                            string txt = rd.ReadLine();
                            if (!string.IsNullOrEmpty(txt))
                            {
                                var arrayTxt = txt.Split('|');

                                qryInsertDbl = string.Format(@"INSERT INTO TR_DBL (
                                                                BATCH, 
                                                                REPORT_DT, 
                                                                CIF, 
                                                                ACCOUNT_NO, 
                                                                FACILITY_TYPE_CODE, 
                                                                FACILITY_TYPE_NAME, 
                                                                LAST_REPORT_YEAR_MONTH, 
                                                                LAST_MODIFIED_DATETIME
                                                                ) 
                                                            VALUES (
                                                                {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', CONVERT(DATETIME, '{7}', 103)
                                                            )", dblBatch, reportDate, arrayTxt[0], arrayTxt[1], arrayTxt[2], arrayTxt[3], arrayTxt[4], arrayTxt[5]);

                                await con.ExecuteAsync(qryInsertDbl);
                            }
                        }

                        //qryInsertDbl = string.Format(@"INSERT INTO TR_DBL (
                        //                                        BATCH, 
                        //                                        REPORT_DT, 
                        //                                        CIF, 
                        //                                        ACCOUNT_NO, 
                        //                                        FACILITY_TYPE_CODE, 
                        //                                        FACILITY_TYPE_NAME, 
                        //                                        LAST_REPORT_YEAR_MONTH, 
                        //                                        LAST_MODIFIED_DATETIME
                        //                                        ) 
                        //                                    VALUES (
                        //                                        {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', CONVERT(DATETIME, '{7}', 103)
                        //                                    )", dblBatch, reportDate, arrayHeader[0], arrayHeader[1], arrayHeader[2], arrayHeader[3], arrayHeader[4], arrayHeader[5]);

                        //await con.ExecuteAsync(qryInsertDbl);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return line - 1;
        }

        public async Task<long> UploadErrorAntasena(IDbConnection con, UserIdentityModel user, string filePath)
        {
            long line = 1;
            string qryInsert = string.Empty;

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (StreamReader rd = new StreamReader(filePath))
                    {
                        // read bi message
                        string biMessage = rd.ReadLine();
                        // read first line (header)
                        string headerLine = rd.ReadLine();

                        string qryDelete = string.Empty;
                        int countDel = 1;

                        // list column
                        string idPelapor = string.Empty;
                        string namaPelapor = string.Empty;
                        string kantorCabang = string.Empty;
                        string periodelaporan = string.Empty;
                        string kelompokInformasi = string.Empty;
                        string namaInformasi = string.Empty;
                        string periodeData = string.Empty;
                        string pesanKesalahan = string.Empty;
                        string pos = string.Empty;

                        while (!rd.EndOfStream)
                        {
                            string txt = rd.ReadLine();
                            if (!string.IsNullOrEmpty(txt))
                            {
                                var arrayTxt = txt.Split(',');
                                idPelapor = arrayTxt[0];
                                namaPelapor = arrayTxt[1];
                                kantorCabang = arrayTxt[2];
                                periodelaporan = arrayTxt[3];
                                kelompokInformasi = arrayTxt[4];
                                namaInformasi = arrayTxt[5];
                                periodeData = arrayTxt[6];

                                // delete data by period date, id pelapor & nama informasi
                                if (countDel == 1)
                                {
                                    qryDelete = string.Format(@"DELETE TR_ERROR_ANTASENA
                                                                    WHERE ID_PELAPOR = '{0}'
                                                                    AND PERIODE_DATA = '{1}'
                                                                    AND NAMA_INFORMASI = '{2}'", idPelapor, periodeData, namaInformasi);

                                    await con.ExecuteAsync(qryDelete);
                                }

                                // get pesan kesalahan dan pos jika ada
                                for (int i = 7; i < arrayTxt.Length; i++)
                                {
                                    pesanKesalahan = pesanKesalahan + arrayTxt[i] + ",";
                                }

                                pesanKesalahan = pesanKesalahan.Substring(0, pesanKesalahan.Length - 1);

                                string posLabaRugi = "Pos Laba Rugi (";
                                string posNeraca = "Pos Laporan Posisi Keuangan (";
                                string posAdministratif = "Pos Rekening Administratif (";

                                if (pesanKesalahan.Contains(posLabaRugi))
                                {
                                    int posisiPos = pesanKesalahan.IndexOf(posLabaRugi) + posLabaRugi.Length;
                                    if (pesanKesalahan.Substring(posisiPos, 1) != ")")
                                        pos = pesanKesalahan.Substring(posisiPos, 17);
                                }
                                else if (pesanKesalahan.Contains(posNeraca))
                                {
                                    int posisiPos = pesanKesalahan.IndexOf(posNeraca) + posNeraca.Length;
                                    if (pesanKesalahan.Substring(posisiPos, 1) != ")")
                                        pos = pesanKesalahan.Substring(posisiPos, 17);
                                }
                                else if (pesanKesalahan.Contains(posAdministratif))
                                {
                                    int posisiPos = pesanKesalahan.IndexOf(posAdministratif) + posAdministratif.Length;
                                    if (pesanKesalahan.Substring(posisiPos, 1) != ")")
                                        pos = pesanKesalahan.Substring(posisiPos, 17);
                                }

                                if (kantorCabang.Trim() == string.Empty)
                                {
                                    kantorCabang = idPelapor.Substring(3, 3);
                                }

                                // insert data error antasena
                                qryInsert = string.Format(@"INSERT INTO TR_ERROR_ANTASENA (ID_PELAPOR, NAMA_PELAPOR, KANTOR_CABANG, PERIODE_LAPORAN, KELOMPOK_INFORMASI, NAMA_INFORMASI, PERIODE_DATA, PESAN_KESALAHAN, POS)
                                                            VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                                                            idPelapor, namaPelapor, kantorCabang, periodelaporan, kelompokInformasi, namaInformasi, periodeData, pesanKesalahan, pos);

                                await con.ExecuteAsync(qryInsert);

                                countDel++;
                            }
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return line - 1;
        }

        private bool IsErrorHeader(string txt)
        {
            if (txt.Trim().Length < 7)
                return false;

            if (txt.Substring(0, 7) == "RECORD|")
                return true;
            else
                return false;
        }

        private bool IsErrorDetail(string txt)
        {
            if (txt.Trim().Length < 6)
                return false;

            if (txt.Substring(0, 6) == "ERROR|")
                return true;
            else
                return false;
        }

        #region Generate Manual Upload Template
        public void GenerateTemplate(IList<ManualUploadDetail> listColumns, string fileFullPath, string fileType, string delimiter)
        {
            if (fileType == FileType.FIXLENGTH.ToString())
            {
                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    string str = "";
                    int ctr = 1;
                    foreach (ManualUploadDetail col in listColumns)
                    {
                        str += col.SourceColumn + string.Concat(Enumerable.Repeat(" ", Convert.ToInt32(col.TFLength) - ctr.ToString().Length));
                        ctr++;
                    }
                    wt.WriteLine(str);

                    wt.Flush();
                    wt.Close();
                    wt.Dispose();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(delimiter))
                {
                    delimiter = "|";
                }

                using (StreamWriter wt = new StreamWriter(fileFullPath))
                {
                    string str = "";
                    foreach (ManualUploadDetail col in listColumns)
                    {
                        str += col.SourceColumn + delimiter;
                    }
                    if (str != "") str = str.Substring(0, str.Length - 1);

                    wt.WriteLine(str);

                    wt.Flush();
                    wt.Close();
                    wt.Dispose();
                }
            }
        }
        #endregion
    }
    public class TMP_FIELD_MAP
    {
        public string field { get; set; }
        public bool? isKey { get; set; }
    }
}