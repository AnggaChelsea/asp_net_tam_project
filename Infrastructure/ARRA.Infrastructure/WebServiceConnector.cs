﻿using ARRA.GLOBAL.App.Interfaces;
using System.Collections.Generic;
using BIValidationServices;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Linq;
using ARRA.Common.Model;
using System;

namespace ARRA.Infrastructure
{
    public class WebServiceConnector : IWebServiceConnector
    {
        public async Task<List<validationReport>> RunBIValidation(string executionDateTime,string licenceKey,
            string reportingDate,string periodLaporan,string idPelapor,string ojkFormCode, IEnumerable<string> branches)
        {
            List<validationReport> list = new List<validationReport>();
            try
            {
                //validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTimeRequest req = new validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTimeRequest();
                //req.fsExecutionDateTime = executionDateTime;  //yyyy-MM-dd HH:mm:ss
                //req.fsKeyCode = licenceKey; //text license
                //req.fsperiodeData = reportingDate; //data date
                //req.fsperiodeLaporan = periodLaporan; //D/W/M/Q/S/A
                //req.fsidFiler = idPelapor;//idpelapor
                //req.fsidPackage = "*"; //kode package / *
                //req.fsidVersion = ojkFormCode; //form
                //req.fsTOAssertion = "*"; //* semua

                validateDataByFilerIDByPackageIDByTOAssertionRequest req = new validateDataByFilerIDByPackageIDByTOAssertionRequest();

                foreach (string branch in branches)
                {
                    req = new validateDataByFilerIDByPackageIDByTOAssertionRequest();
                    req.fsperiodeData = reportingDate; //data date
                    req.fsperiodeLaporan = periodLaporan; //D/W/M/Q/S/A
                    req.fsidFiler = idPelapor.Substring(0,3)+branch+idPelapor.Substring(6,3);//idpelapor
                    req.fsidPackage = "*"; //kode package / *
                    req.fsidVersion = ojkFormCode; //form
                    req.fsTOAssertion = "*"; //* semua

                    BIValidationServices.IMServicesClient svc = new BIValidationServices.IMServicesClient();
                    //validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTimeResponse res =
                    //    await svc.validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTimeAsync(req);

                    validateDataByFilerIDByPackageIDByTOAssertionResponse res =
                        await svc.validateDataByFilerIDByPackageIDByTOAssertionAsync(req);
                    req = null;

                    if (res.fdsError.Nodes.Count > 0)
                    {
                        ArrayOfXElement result = res.fdsError;
                        List<XElement> els = result.Nodes.AsEnumerable().ToList();
                        IEnumerable<XElement> vals = els.Elements("NewDataSet").Elements("validationReport");

                        list = vals.Select(c => new validationReport
                        {
                            validationStatus = c.Elements("validationStatus").FirstOrDefault().Value,
                            validationTest = c.Elements("validationTest").FirstOrDefault().Value
                        }).ToList();

                        break;
                    }
                    res = null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
            //res.validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTimeResult;
            //validateDataByFilerIDByPackageIDByTOAssertionAuthByDateTime
            //    (ByVal fsExecutionDateTime As String, 
            //    ByVal fsKeyCode As String, 
            //    ByVal fsidFiler As String, 
            //    ByVal fsperiodeData As String, 
            //    ByVal fsperiodeLaporan As String, 
            //    ByVal fsidPackage As String, 
            //    ByVal fsTOAssertion As String, 
            //    ByVal fsidVersion As String, 
            //    ByRef fnElapsedTime As TimeSpan, 
            //    ByRef fdsError As DataSet
            //) 
        }
    }
}
