﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using ARRA.GLOBAL.App.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using Novell.Directory.Ldap;
using System.DirectoryServices;
using static ARRA.Common.Decrypt;

namespace ARRA.Infrastructure
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IConfiguration _configuration;     

        public AuthenticationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public GLOBAL.App.Authentications.Models.LoginModel GenerateToken(string userid, string groupaccess, string moduleaccess, string branchgroup)
        {           
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserId", userid),
                        new Claim("GroupAccess", groupaccess),
                        new Claim("BranchGroup", string.IsNullOrEmpty(branchgroup)?"":branchgroup),
                        new Claim("UserModule",string.IsNullOrEmpty(moduleaccess)?"":moduleaccess+",")
                    }),
                // 20221111 (Rizwan), disable expires diganti menggunakan angular idle
                //Expires = DateTime.UtcNow.AddMinutes(Convert.ToInt32(_configuration.GetSection("ApplicationSettings:SESSION_TIMEOUT").Value)),
                Expires = DateTime.UtcNow.AddDays(Convert.ToInt32(1440)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("ApplicationSettings:JWT_SECRET").Value)), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            string token = tokenHandler.WriteToken(securityToken);
            return new GLOBAL.App.Authentications.Models.LoginModel
            {
                userName = userid,
                token = token,
                expiry = tokenDescriptor.Expires
            };
        }

        public string Encrypt(string text)
        {
            string salt = _configuration.GetSection("ApplicationSettings:PASS_SECRET").Value;            
            return HashText(text, salt);
        }
        private string HashText(string text, string salt)
        {
            byte[] textWithSaltBytes = Encoding.UTF8.GetBytes(string.Concat(text, salt));

            byte[] hashedBytes = new SHA1CryptoServiceProvider().ComputeHash(textWithSaltBytes);

            return Convert.ToBase64String(hashedBytes);
        }


        public int LDAP_Authenticate(string distinguishedName, string password)
        {
            string LDAP_SERVER = _configuration.GetSection("LdapSettings:ServerName").Value;
            LDAP_SERVER = DecryptText(LDAP_SERVER, _configuration);

            int LDAP_PORT = int.Parse(_configuration.GetSection("LdapSettings:ServerPort").Value);
            LDAP_PORT = int.Parse(DecryptText(LDAP_PORT.ToString(), _configuration));

            bool useSSL = bool.Parse(_configuration.GetSection("LdapSettings:UseSSL").Value);

            using (var ldapConnection = new LdapConnection() { SecureSocketLayer = useSSL })
            {

                try
                {
                    ldapConnection.Connect(LDAP_SERVER, LDAP_PORT);
                }
                catch (Exception ex)
                {
                    //throw new Exception("Please Check LDAP Settings");
                    return 3;
                }


            try
                {                    
                    ldapConnection.Bind(distinguishedName, password);
                    return 3;
                }
                catch (Exception)
                {                    
                    return 1;
                }
            }
        }

        public int LDAP_Authenticate_DS(string distinguishedName, string password)
        {
            string LDAP_SERVER = _configuration.GetSection("LdapSettings:ServerName").Value;
            LDAP_SERVER = DecryptText(LDAP_SERVER, _configuration);

            string LDAP_PORT = _configuration.GetSection("LdapSettings:ServerPort").Value;
            LDAP_PORT = DecryptText(LDAP_PORT, _configuration);

            string pathServer = LDAP_SERVER + ":" + LDAP_PORT;

            try
            {
                DirectoryEntry de = new DirectoryEntry(pathServer, distinguishedName, password);
                DirectorySearcher ds = new DirectorySearcher(de);

                ds.FindOne();


                return 1;
            }
            catch (Exception ex)
            {
                //return false;
                if (ex.Message.ToLower().Contains("user"))
                {
                    return 2;
                }
                else
                    return 3;
            }
           
        }



    }
}