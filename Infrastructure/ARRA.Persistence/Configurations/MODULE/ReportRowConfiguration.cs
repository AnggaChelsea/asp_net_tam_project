﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportRowConfiguration : IEntityTypeConfiguration<ReportRow>
    {
        public void Configure(EntityTypeBuilder<ReportRow> builder)
        {
            builder.ToTable("MS_REPORT_ROW");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.RowSeq).HasColumnName("ROW_SEQ").HasMaxLength(255);
            builder.Property(e => e.RowDesc).HasColumnName("ROW_DESC").HasMaxLength(8000);
            builder.Property(e => e.ComponentCd).HasColumnName("COMPONENT_CD").HasMaxLength(255);
            builder.Property(e => e.DbNm).HasColumnName("DB_NM").HasMaxLength(255);
            builder.Property(e => e.TableNm).HasColumnName("TABLE_NM").HasMaxLength(255);
            builder.Property(e => e.ParentRowSeq).HasColumnName("PARENT_ROW_SEQ").HasMaxLength(255);
            builder.Property(e => e.Text).HasColumnName("IS_TF").HasMaxLength(1);
            builder.Property(e => e.FormCodeDetail).HasColumnName("FORM_CD_DTL").HasMaxLength(255);
        }

    }
}
