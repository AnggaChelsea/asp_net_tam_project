﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportTableDataWebConfiguration : IEntityTypeConfiguration<ReportTableDataWeb>
    {
        public void Configure(EntityTypeBuilder<ReportTableDataWeb> builder)
        {
            builder.ToTable("MS_REPORT_TBL_DATA_WEB");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.ColumnSeq).HasColumnName("COLUMN_SEQ");
            builder.Property(e => e.ColumnDesc).HasColumnName("COLUMN_DESC").HasMaxLength(255);
            builder.Property(e => e.TableCd).HasColumnName("TBL_CD").HasMaxLength(255);
            builder.Property(e => e.ColumnData).HasColumnName("COLUMN_DATA").HasMaxLength(255);
            builder.Property(e => e.ColumnAlias).HasColumnName("COLUMN_ALIAS").HasMaxLength(255);
            builder.Property(e => e.IsHide).HasColumnName("IS_HIDE").HasMaxLength(1);
            builder.Property(e => e.IsCompare).HasColumnName("IS_COMPARE").HasMaxLength(1);
        }
    }
}
