﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class GenerateTextFileConfiguration : IEntityTypeConfiguration<GenerateTextFile>
    {
        public void Configure(EntityTypeBuilder<GenerateTextFile> builder)
        {

            builder.ToTable("TR_GENERATE_TEXT_FILE");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ReportDate).HasColumnName("REPORT_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.OjkFormCode).HasColumnName("OJK_FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FormName).HasColumnName("FORM_NM").HasMaxLength(50);
            builder.Property(e => e.BranchCode).HasColumnName("BRANCH_CD").HasMaxLength(10);
            builder.Property(e => e.BranchName).HasColumnName("BRANCH_NM").HasMaxLength(50);
            builder.Property(e => e.FileName).HasColumnName("FILE_NM").HasMaxLength(100);
            builder.Property(e => e.OriginalFileName).HasColumnName("ORI_FILE_NM").HasMaxLength(100);
            builder.Property(e => e.TotalRecord).HasColumnName("TOTAL_RECORD");
            builder.Property(e => e.IdOperational).HasColumnName("ID_OPS").HasMaxLength(1);
            builder.Property(e => e.FilePeriodType).HasColumnName("FILE_PERIOD_TP").HasMaxLength(5);
            builder.Property(e => e.Active).HasColumnName("ACTIVE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
