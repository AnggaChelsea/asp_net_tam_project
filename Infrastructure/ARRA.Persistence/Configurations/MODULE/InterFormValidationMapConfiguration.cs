﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class InterFormValidationMapConfiguration : IEntityTypeConfiguration<InterFormValidationMap>
    {
        public void Configure(EntityTypeBuilder<InterFormValidationMap> builder)
        {
            builder.ToTable("MS_INTER_FORM_VAL_MAP");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            builder.Property(e => e.SrcFormCode).HasColumnName("SRC_FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.SrcSandiField).HasColumnName("SRC_SANDI_FIELD").HasMaxLength(30);
            builder.Property(e => e.SrcSandi).HasColumnName("SRC_SANDI").HasMaxLength(50);
            builder.Property(e => e.SrcAmoutField).HasColumnName("SRC_AMT_FIELD").HasMaxLength(30);
            builder.Property(e => e.SrcOtherCondition).HasColumnName("SRC_OTR_CONDS").HasMaxLength(100);
            builder.Property(e => e.CmpFormCode).HasColumnName("CMP_FORM_CD").HasMaxLength(20);
            builder.Property(e => e.CmpSandiField).HasColumnName("CMP_SANDI_FIELD").HasMaxLength(30);
            builder.Property(e => e.CmpSandi).HasColumnName("CMP_SANDI").HasMaxLength(50);
            builder.Property(e => e.CmpAmoutField).HasColumnName("CMP_AMT_FIELD").HasMaxLength(30);
            builder.Property(e => e.CmpOtherCondition).HasColumnName("CMP_OTR_CONDS").HasMaxLength(100);
            builder.Property(e => e.Message).HasColumnName("MSG").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
