﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportColumnConfiguration : IEntityTypeConfiguration<ReportColumn>
    {
        public void Configure(EntityTypeBuilder<ReportColumn> builder)
        {
            builder.ToTable("MS_REPORT_COL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.ColumnSeq).HasColumnName("COLUMN_SEQ").HasMaxLength(255);
            builder.Property(e => e.ColumnExcelPos).HasColumnName("COLUMN_EXCEL_POS").HasMaxLength(8000);
            builder.Property(e => e.ColumnDest).HasColumnName("COLUMN_DEST").HasMaxLength(255);
        }

    }
}
