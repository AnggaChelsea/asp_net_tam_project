﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportTableWebConfiguration : IEntityTypeConfiguration<ReportTableWeb>
    {
        public void Configure(EntityTypeBuilder<ReportTableWeb> builder)
        {
            builder.ToTable("MS_REPORT_TBL_WEB");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.TableCd).HasColumnName("TBL_CD").HasMaxLength(255);
            builder.Property(e => e.TableFilter).HasColumnName("TBL_FILTER").HasMaxLength(8000);
            builder.Property(e => e.TableSequence).HasColumnName("TBL_SEQ");
            builder.Property(e => e.TableDesc).HasColumnName("TBL_DESC").HasMaxLength(255);
        }

    }
}
