﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportFilterConfiguration : IEntityTypeConfiguration<ReportFilter>
    {
        public void Configure(EntityTypeBuilder<ReportFilter> builder)
        {
            builder.ToTable("MS_REPORT_FILTER");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.RowSeq).HasColumnName("ROW_SEQ").HasMaxLength(255);
            builder.Property(e => e.ColumnSeq).HasColumnName("COLUMN_SEQ").HasMaxLength(255);
            builder.Property(e => e.Prefix).HasColumnName("PREFIX").HasMaxLength(255);
            //builder.Property(e => e.ColumnName).HasColumnName("COLUMN_NAME").HasMaxLength(-1);
            builder.Property(e => e.ColumnName).HasColumnName("COLUMN_NAME").HasColumnType("varchar(MAX)");
            builder.Property(e => e.Expression).HasColumnName("EXPRESSION").HasMaxLength(255);
            //builder.Property(e => e.ColumnVal).HasColumnName("COLUMN_VAL").HasMaxLength(-1);
            builder.Property(e => e.ColumnVal).HasColumnName("COLUMN_VAL").HasColumnType("varchar(MAX)");
            builder.Property(e => e.Operator).HasColumnName("OPERATOR").HasMaxLength(255);
            builder.Property(e => e.Suffix).HasColumnName("SUFFIX").HasMaxLength(255);
            builder.Property(e => e.FilterSeq).HasColumnName("FILTER_SEQ").HasMaxLength(255);
        }

    }
}
