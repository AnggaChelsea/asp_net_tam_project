﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class BIValidationToolConfiguration : IEntityTypeConfiguration<BIValidationTool>
    {
        public void Configure(EntityTypeBuilder<BIValidationTool> builder)
        {
            builder.ToTable("MS_BI_VALIDATION_TOOL");
            builder.HasKey(e => e.ProcessNo);
            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO").HasMaxLength(20).IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            builder.Property(e => e.ProcessName).HasColumnName("PROC_NM").HasMaxLength(100).IsRequired();
            builder.Property(e => e.ETLPackage).HasColumnName("ETL_PACKAGE").HasMaxLength(100);
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}