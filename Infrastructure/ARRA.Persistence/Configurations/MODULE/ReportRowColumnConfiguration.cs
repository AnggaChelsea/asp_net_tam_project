﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportRowColumnConfiguration : IEntityTypeConfiguration<ReportRowColumn>
    {
        public void Configure(EntityTypeBuilder<ReportRowColumn> builder)
        {
            builder.ToTable("MS_REPORT_ROW_COL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.RowSeq).HasColumnName("ROW_SEQ").HasMaxLength(255);
            builder.Property(e => e.ColumnSeq).HasColumnName("COLUMN_SEQ").HasMaxLength(255);
            builder.Property(e => e.ColumnFormula).HasColumnName("COLUMN_FORMULA").HasMaxLength(8000);
            builder.Property(e => e.ColumnStatic).HasColumnName("COLUMN_STATIC").HasMaxLength(8000);
            builder.Property(e => e.IsRecalculate).HasColumnName("IS_RECALCULATE").HasMaxLength(1);
            builder.Property(e => e.RecalculateSeq).HasColumnName("RECALCULATE_SEQ").HasMaxLength(255);
            builder.Property(e => e.ColumnAggregate).HasColumnName("COLUMN_AGGREGATE").HasMaxLength(255);
            builder.Property(e => e.RecalculateType).HasColumnName("RECALCULATE_TYPE").HasMaxLength(1);
            builder.Property(e => e.DivisorAfterAggregate).HasColumnName("DIVISOR_AFTER_AGGREGATE").HasMaxLength(100);
            builder.Property(e => e.RoundedAfterAggregate).HasColumnName("ROUNDED_AFTER_AGGREGATE").HasMaxLength(100);
            builder.Property(e => e.AbsoluteAfterAggregate).HasColumnName("ABSOLUTE_AFTER_AGGREGATE").HasMaxLength(100);







        }

    }
}
