﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class InterFormValidationResultConfiguration : IEntityTypeConfiguration<InterFormValidationResult>
    {
        public void Configure(EntityTypeBuilder<InterFormValidationResult> builder)
        {
            builder.ToTable("TR_INTER_FORM_VAL_RESULT");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.DataDate).HasColumnName("REPORT_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            builder.Property(e => e.SrcFormCode).HasColumnName("SRC_FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.SrcSandi).HasColumnName("SRC_SANDI").HasMaxLength(50);
            builder.Property(e => e.SrcAmount).HasColumnName("SRC_AMT").HasColumnType("DECIMAL(32,2)");
            builder.Property(e => e.CmpFormCode).HasColumnName("CMP_FORM_CD").HasMaxLength(20);
            builder.Property(e => e.CmpSandi).HasColumnName("CMP_SANDI").HasMaxLength(50);
            builder.Property(e => e.CmpAmount).HasColumnName("CMP_AMT").HasColumnType("DECIMAL(32,2)");
            builder.Property(e => e.CmpValidSts).HasColumnName("VALID_STS").HasMaxLength(3);
            builder.Property(e => e.Difference).HasColumnName("DIFF").HasColumnType("DECIMAL(32,2)");
            builder.Property(e => e.Msg).HasColumnName("MSG").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
            builder.Property(e => e.Branch).HasColumnName("RG_BRANCH").HasMaxLength(10);

        }

    }
}
