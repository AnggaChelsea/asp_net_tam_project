﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ProcessLogHeaderConfiguration : IEntityTypeConfiguration<ProcessLogHeader>
    {
        public void Configure(EntityTypeBuilder<ProcessLogHeader> builder)
        {
            builder.ToTable("TR_PROCESS_LOG_HDR");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.DataDate).HasColumnName("DATA_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10);
            builder.Property(e => e.ProcessType).HasColumnName("PROC_TP").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ProcessName).HasColumnName("PROC_NM").HasMaxLength(100);
            builder.Property(e => e.Branch).HasColumnName("BRANCH_CD").HasMaxLength(10);
            builder.Property(e => e.ProcessStatus).HasColumnName("PROC_STS").HasMaxLength(20);
            builder.Property(e => e.Remark).HasColumnName("REMARKS").HasMaxLength(8000);
            builder.Property(e => e.QueueId).HasColumnName("QUEUE_ID");
            builder.Property(e => e.TotalRow).HasColumnName("TOTAL_ROW");
            builder.Property(e => e.TotalFailed).HasColumnName("TOTAL_FAILED");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}