﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class SN_UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("SN_MS_USER");
            builder.HasKey(e => e.UserId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.UserId).HasColumnName("USR_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.Password).HasColumnName("PASSWD").HasMaxLength(100);
            builder.Property(e => e.FirstName).HasColumnName("FIRST_NM").HasMaxLength(30).IsRequired();
            builder.Property(e => e.LastName).HasColumnName("LAST_NM").HasMaxLength(30);
            builder.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID").HasMaxLength(30);
            builder.Property(e => e.Position).HasColumnName("POSITION").HasMaxLength(50);
            builder.Property(e => e.MenuGroup).HasColumnName("MENU_GROUP").HasMaxLength(30);
            builder.Property(e => e.BranchGroup).HasColumnName("BRANCH_GROUP").HasMaxLength(30);
            builder.Property(e => e.ApprovalGroup).HasColumnName("APPROVAL_GROUP").HasMaxLength(30);
            builder.Property(e => e.ModuleAccess).HasColumnName("MODULE_ACCESS").HasMaxLength(200);
            builder.Property(e => e.LockStatus).HasColumnName("LOCK_STS");
            builder.Property(e => e.LockCounter).HasColumnName("LOCK_CTR");
            builder.Property(e => e.LastLoginDateTime).HasColumnName("LAST_LOGIN_DT");
            builder.Property(e => e.Status).HasColumnName("ACTIVE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
