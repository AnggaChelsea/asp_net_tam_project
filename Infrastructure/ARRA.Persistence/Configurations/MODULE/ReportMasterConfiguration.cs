﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportMasterConfiguration : IEntityTypeConfiguration<ReportMaster>
    {
        public void Configure(EntityTypeBuilder<ReportMaster> builder)
        {
            builder.ToTable("MS_REPORT_MASTER");
            builder.HasKey(e => e.ReportCd);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.ReportDesc).HasColumnName("REPORT_DESC").HasMaxLength(255);
            builder.Property(e => e.ReportTemplate).HasColumnName("REPORT_TEMPLATE").HasMaxLength(255);
            builder.Property(e => e.ReportSheet).HasColumnName("REPORT_SHEET").HasMaxLength(255);
            builder.Property(e => e.FormCdDtl).HasColumnName("FORM_CD_DTL").HasMaxLength(255);
        }

    }
}
