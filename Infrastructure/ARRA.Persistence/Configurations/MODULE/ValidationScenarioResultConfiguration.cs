﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ValidationScenarioResultConfiguration : IEntityTypeConfiguration<ValidationScenarioResult>
    {
        public void Configure(EntityTypeBuilder<ValidationScenarioResult> builder)
        {
            builder.ToTable("TR_VAL_SCENARIO_RESULT");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ReportingDate).HasColumnName("REPORT_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.Branch).HasColumnName("RG_BRANCH").HasMaxLength(10);
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            //builder.Property(e => e.ValidMessage).HasColumnName("VALID_MSG").HasMaxLength(-1);
            builder.Property(e => e.ValidMessage).HasColumnName("VALID_MSG").HasColumnType("varchar(MAX)");
            builder.Property(e => e.IdAssertion).HasColumnName("ID_ASSERTION").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
