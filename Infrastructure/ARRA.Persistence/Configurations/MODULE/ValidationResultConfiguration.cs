﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ValidationResultConfiguration : IEntityTypeConfiguration<ValidationResult>
    {
        public void Configure(EntityTypeBuilder<ValidationResult> builder)
        {
            builder.ToTable("TR_VALIDATION_RESULT");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.Branch).HasColumnName("BRANCH").HasMaxLength(10);
            builder.Property(e => e.TableName).HasColumnName("TABLE_NM").HasMaxLength(30);
            builder.Property(e => e.FieldName).HasColumnName("FIELD_NM").HasMaxLength(40);
            builder.Property(e => e.ValidNo).HasColumnName("VALID_NO");
            builder.Property(e => e.ValidMsg).HasColumnName("VALID_MSG").HasMaxLength(100);
            builder.Property(e => e.UniqueIdRef).HasColumnName("UID_DATA");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
