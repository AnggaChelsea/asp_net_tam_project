﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ValidationConfiguration : IEntityTypeConfiguration<Validation>
    {
        public void Configure(EntityTypeBuilder<Validation> builder)
        {
            builder.ToTable("MS_VALIDATION");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10).IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ValidationType).HasColumnName("VALID_TP").HasMaxLength(10);
            builder.Property(e => e.RuleType).HasColumnName("RULE_TP").HasMaxLength(30);
            builder.Property(e => e.FieldName).HasColumnName("FIELD_NM").HasMaxLength(40);
            builder.Property(e => e.ValueExpression).HasColumnName("VALUE_EXPRS").HasMaxLength(100);
            builder.Property(e => e.OtherCondsExpres).HasColumnName("OTR_CONDS_EXPRS").HasMaxLength(200);
            builder.Property(e => e.ValidMsg).HasColumnName("VALID_MSG").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
