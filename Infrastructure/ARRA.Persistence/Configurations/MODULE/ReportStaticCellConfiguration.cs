﻿using ARRA.MODULE.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class ReportStaticCellConfiguration : IEntityTypeConfiguration<ReportStaticCell>
    {
        public void Configure(EntityTypeBuilder<ReportStaticCell> builder)
        {
            builder.ToTable("MS_REPORT_STATIC_CELL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

            builder.Property(e => e.ReportCd).HasColumnName("REPORT_CD").HasMaxLength(255);
            builder.Property(e => e.RowSequence).HasColumnName("ROW_SEQ");
            builder.Property(e => e.CellPosition).HasColumnName("CELL_POSITION").HasMaxLength(255);
            builder.Property(e => e.CellType).HasColumnName("CELL_TYPE").HasMaxLength(255);
            builder.Property(e => e.CellValue).HasColumnName("CELL_VALUE").HasMaxLength(1000);
        }
    }
}
