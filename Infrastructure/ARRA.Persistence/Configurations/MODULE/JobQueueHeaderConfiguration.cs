﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class JobQueueHeaderConfiguration : IEntityTypeConfiguration<JobQueueHeader>
    {
        public void Configure(EntityTypeBuilder<JobQueueHeader> builder)
        {
            builder.ToTable("TR_JOB_QUEUED_HDR");
            builder.HasKey(e => e.UniqueId);
            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.JobType).HasColumnName("JOB_TP").HasMaxLength(20).IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10);
            builder.Property(e => e.Status).HasColumnName("STS").HasMaxLength(20).IsRequired();
            builder.Property(e => e.Message).HasColumnName("MESSAGE").HasMaxLength(8000);
            builder.Property(e => e.ThreadId).HasColumnName("THREAD_ID").HasMaxLength(50);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
