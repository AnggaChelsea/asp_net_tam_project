﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class TransformFormMappingConfiguration : IEntityTypeConfiguration<TransformFormMapping>
    {
        public void Configure(EntityTypeBuilder<TransformFormMapping> builder)
        {
            builder.ToTable("MS_TRANSFORM_FORM_MAPPING");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO");
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD");
        }
    }
}
