﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence.Configurations.MODULE
{
    public class BIValidationExceptionConfiguration : IEntityTypeConfiguration<BIValidationException>
    {
        public void Configure(EntityTypeBuilder<BIValidationException> builder)
        {
            builder.ToTable("TR_BI_VAL_EXCEPTION");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ReportDate).HasColumnName("REPORT_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.IdPelapor).HasColumnName("ID_PELAPOR").HasMaxLength(20);
            builder.Property(e => e.ValidationStatus).HasColumnName("VAL_STS").HasMaxLength(500);
            builder.Property(e => e.ValidationTest).HasColumnName("VAL_TEST").HasMaxLength(8000);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}