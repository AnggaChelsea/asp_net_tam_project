﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormSearchFieldConfiguration : IEntityTypeConfiguration<FormSearchField>
    {
        public void Configure(EntityTypeBuilder<FormSearchField> builder)
        {
            builder.ToTable("MS_FORM_SEARCH_FIELD");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(255).IsRequired();
            builder.Property(e => e.Key).HasColumnName("FIELD_KEY").HasMaxLength(255);
            builder.Property(e => e.Label).HasColumnName("FIELD_LABEL").HasMaxLength(255);
            builder.Property(e => e.Sequence).HasColumnName("FIELD_SEQ");

            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
