﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class MenuGroupHeaderConfiguration : IEntityTypeConfiguration<MenuGroupHeader>
    {
        public void Configure(EntityTypeBuilder<MenuGroupHeader> builder)
        {
            builder.ToTable("MS_MENU_GROUP_HDR");
            builder.HasKey(e => e.GroupId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.GroupId).HasColumnName("GROUP_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.GroupName).HasColumnName("GROUP_NM").HasMaxLength(30).IsRequired();
            builder.Property(e => e.GroupDesc).HasColumnName("GROUP_DESC").HasMaxLength(100);
            builder.Property(e => e.Status).HasColumnName("ACTIVE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);


        }
    }
}
