﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ManualFaqConfiguration : IEntityTypeConfiguration<ManualFaq>
    {
        public void Configure(EntityTypeBuilder<ManualFaq> builder)
        {
            builder.ToTable("MS_MANUAL_FAQ");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(20).IsRequired();
            builder.Property(e => e.DocumentType).HasColumnName("DOC_TYPE").HasMaxLength(50).IsRequired();
            builder.Property(e => e.ReleaseDate).HasColumnName("RELEASE_DT").IsRequired();
            builder.Property(e => e.DocumentName).HasColumnName("DOC_NAME").HasMaxLength(200).IsRequired();
            builder.Property(e => e.FileName).HasColumnName("FILE_NAME").HasMaxLength(200);
            builder.Property(e => e.FilePath).HasColumnName("FILE_PATH").HasMaxLength(200).IsRequired();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
