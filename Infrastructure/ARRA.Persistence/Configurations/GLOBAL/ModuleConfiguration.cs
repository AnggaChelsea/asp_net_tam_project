﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ModuleConfiguration : IEntityTypeConfiguration<Module>
    {
        public void Configure(EntityTypeBuilder<Module> builder)
        {
            builder.ToTable("MS_MODULE");
            builder.HasKey(e => e.ModuleId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ModuleId).HasColumnName("MODULE_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.ModuleName).HasColumnName("MODULE_NM").HasMaxLength(30).IsRequired();
            builder.Property(e => e.DatabaseName).HasColumnName("DB_NM").HasMaxLength(30);
            builder.Property(e => e.Prefix).HasColumnName("PREFIX").HasMaxLength(10);
            builder.Property(e => e.BaseUrl).HasColumnName("BASE_URL").HasMaxLength(100);
            builder.Property(e => e.BusinessType).HasColumnName("BUSINESS_TP").HasMaxLength(20);
            builder.Property(e => e.Sequence).HasColumnName("SEQUENCE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);


        }

    }
}
