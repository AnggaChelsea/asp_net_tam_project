﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class AdhocQueryConfiguration : IEntityTypeConfiguration<AdhocQuery>
    {
        public void Configure(EntityTypeBuilder<AdhocQuery> builder)
        {
            builder.ToTable("MS_ADHOCQUERY");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.QueryName).HasColumnName("QUERY_NM").HasMaxLength(100);
            builder.Property(e => e.QueryDesc).HasColumnName("QUERY_DESC").HasMaxLength(200);
            //builder.Property(e => e.Query).HasColumnName("QUERY").HasMaxLength(-1);
            builder.Property(e => e.Query).HasColumnName("QUERY").HasColumnType("varchar(MAX)");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
