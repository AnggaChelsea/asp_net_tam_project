﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormSearchListConfiguration : IEntityTypeConfiguration<FormSearchList>
    {
        public void Configure(EntityTypeBuilder<FormSearchList> builder)
        {
            builder.ToTable("MS_FORM_SEARCH_LIST");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(255).IsRequired();
            builder.Property(e => e.Value).HasColumnName("FORM_VALUE").HasMaxLength(255);
            builder.Property(e => e.FieldMapping).HasColumnName("FORM_FIELD_MAPPING").HasMaxLength(255);
            builder.Property(e => e.Sequence).HasColumnName("FORM_SEQ");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
