﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ManualUploadDetailConfiguration : IEntityTypeConfiguration<ManualUploadDetail>
    {
        public void Configure(EntityTypeBuilder<ManualUploadDetail> builder)
        {
            builder.ToTable("MS_MANUAL_UPLOAD_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO").HasMaxLength(20).IsRequired();
            builder.Property(e => e.SourceColumn).HasColumnName("SOURCE_COLUMN").HasMaxLength(50);
            builder.Property(e => e.TargetColumn).HasColumnName("TARGET_COLUMN").HasMaxLength(30);
            builder.Property(e => e.TFSeq).HasColumnName("TF_SEQ");
            builder.Property(e => e.TFLength).HasColumnName("TF_LENGTH");
            builder.Property(e => e.IsKey).HasColumnName("IS_KEY");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
