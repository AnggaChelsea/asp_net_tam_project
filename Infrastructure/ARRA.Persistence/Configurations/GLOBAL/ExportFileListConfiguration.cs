﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ExportFileListConfiguration : IEntityTypeConfiguration<ExportFileList>
    {
        public void Configure(EntityTypeBuilder<ExportFileList> builder)
        {
            builder.ToTable("TR_EXPORT_FILE_LIST");
            builder.HasKey(e => e.UniqueId);
            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.UserId).HasColumnName("USR_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.JobQueueId).HasColumnName("JOB_QUEUE_ID");
            builder.Property(e => e.FileName).HasColumnName("FILE_NM").HasMaxLength(100);
            builder.Property(e => e.TotalRecord).HasColumnName("TOTAL_RECORD");
            builder.Property(e => e.TotalInserted).HasColumnName("TOTAL_INSERTED");
            builder.Property(e => e.Status).HasColumnName("STS").HasMaxLength(20);
            builder.Property(e => e.Message).HasColumnName("MESSAGE").HasMaxLength(5000);
            builder.Property(e => e.ApprovalId).HasColumnName("APPV_ID");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}