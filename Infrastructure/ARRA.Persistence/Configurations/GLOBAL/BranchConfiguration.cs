﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {
        public void Configure(EntityTypeBuilder<Branch> builder)
        {
            builder.ToTable("MS_BRANCH");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.BranchCode).HasColumnName("BRANCH_CD").HasMaxLength(10).IsRequired();
            builder.Property(e => e.BranchName).HasColumnName("BRANCH_NM").HasMaxLength(50).IsRequired();
            builder.Property(e => e.CountryCode).HasColumnName("COUNTRY").HasColumnType("CHAR(2)");
            builder.Property(e => e.Region).HasColumnName("REGION").HasMaxLength(30);
            builder.Property(e => e.BranchArea).HasColumnName("AREA").HasMaxLength(30);
            builder.Property(e => e.Unit).HasColumnName("UNIT").HasMaxLength(20);
            builder.Property(e => e.RegulatorBranch).HasColumnName("REGULATOR_BRANCH_CD").HasMaxLength(10);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
