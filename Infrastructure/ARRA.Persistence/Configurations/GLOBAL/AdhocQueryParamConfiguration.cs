﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class AdhocQueryParamConfiguration : IEntityTypeConfiguration<AdhocQueryParam>
    {
        public void Configure(EntityTypeBuilder<AdhocQueryParam> builder)
        {
            builder.ToTable("MS_ADHOCQUERY_PARAM");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.QueryId).HasColumnName("QUERY_ID").IsRequired();
            builder.Property(e => e.ParamId).HasColumnName("PARAM_ID").HasMaxLength(30);
            builder.Property(e => e.ParamName).HasColumnName("PARAM_NM").HasMaxLength(50);
            builder.Property(e => e.ParamType).HasColumnName("PARAM_TP").HasMaxLength(50);
            builder.Property(e => e.DefaultValue).HasColumnName("DEFAULT_VAL").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
