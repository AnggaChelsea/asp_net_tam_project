﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ManualUploadHeaderConfiguration : IEntityTypeConfiguration<ManualUploadHeader>
    {
        public void Configure(EntityTypeBuilder<ManualUploadHeader> builder)
        {
            builder.ToTable("MS_MANUAL_UPLOAD_HDR");
            builder.HasKey(e => e.ProcessNo);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ProcessName).HasColumnName("PROC_NM").HasMaxLength(30);
            builder.Property(e => e.FileType).HasColumnName("FILE_TYPE").HasMaxLength(20);
            builder.Property(e => e.DelimitedChar).HasColumnName("DELIMITED_CHR").HasColumnType("CHAR(1)");
            builder.Property(e => e.TargetTable).HasColumnName("TARGET_TABLE").HasMaxLength(30);
            builder.Property(e => e.TemplateName).HasColumnName("TEMPLATE_NM").HasMaxLength(50);
            builder.Property(e => e.UploadType).HasColumnName("UPLOAD_TP").HasMaxLength(15);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);


        }

    }
}
