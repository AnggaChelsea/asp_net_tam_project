﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class IssueListDetailConfiguration : IEntityTypeConfiguration<IssueList>
    {
        public void Configure(EntityTypeBuilder<IssueList> builder)
        {
            builder.ToTable("TR_ISSUE_LIST");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.DateDate).HasColumnName("DATA_DT").HasColumnType("DATE").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10);
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.Problem).HasColumnName("PROBLEM").HasMaxLength(100);
            builder.Property(e => e.Solution).HasColumnName("SOLUTION").HasMaxLength(100);
            builder.Property(e => e.ActionTake).HasColumnName("ACTION_TK").HasMaxLength(20);
            builder.Property(e => e.IssueStatus).HasColumnName("ISSUE_STS").HasMaxLength(10);
        }
    }
}