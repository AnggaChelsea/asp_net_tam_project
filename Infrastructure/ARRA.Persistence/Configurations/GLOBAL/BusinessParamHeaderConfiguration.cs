﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class BusinessParamHeaderConfiguration : IEntityTypeConfiguration<BusinessParamHeader>
    {
        public void Configure(EntityTypeBuilder<BusinessParamHeader> builder)
        {
            builder.ToTable("MS_BUSINESS_PARAM_HDR");
            builder.HasKey(e => e.ParamCode);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ParamCode).HasColumnName("PAR_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ParamName).HasColumnName("PAR_NM").HasMaxLength(30).IsRequired();
            builder.Property(e => e.ParamDesc).HasColumnName("PAR_DESC").HasMaxLength(100);
            builder.Property(e => e.ParamValue).HasColumnName("PAR_VALUE").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);



        }

    }
}
