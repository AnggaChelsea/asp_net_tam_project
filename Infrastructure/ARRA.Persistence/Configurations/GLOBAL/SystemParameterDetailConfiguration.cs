﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class SystemParameterDetailConfiguration : IEntityTypeConfiguration<SystemParameterDetail>
    {
        public void Configure(EntityTypeBuilder<SystemParameterDetail> builder)
        {
            builder.ToTable("MS_SYSTEM_PARAM_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ParamCode).HasColumnName("PAR_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ParamValue).HasColumnName("PAR_VALUE").HasMaxLength(100).IsRequired();
            builder.Property(e => e.ParamMapping).HasColumnName("PAR_VAL_MAP").HasMaxLength(100);
            builder.Property(e => e.ParamDesc).HasColumnName("PAR_DESC").HasMaxLength(200);
            builder.Property(e => e.ParamSeq).HasColumnName("PAR_SEQ");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
