﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ApprovalDetailConfiguration : IEntityTypeConfiguration<ApprovalDetail>
    {
        public void Configure(EntityTypeBuilder<ApprovalDetail> builder)
        {
            builder.ToTable("TR_APPROVAL_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.HeaderId).HasColumnName("HDR_ID").IsRequired();
            builder.Property(e => e.FieldName).HasColumnName("FIELD_NM").HasMaxLength(40);
            builder.Property(e => e.FormColName).HasColumnName("FORM_COL_NM").HasMaxLength(50);
            builder.Property(e => e.FormSeq).HasColumnName("FORM_SEQ");
            builder.Property(e => e.ValueBefore).HasColumnName("VAL_BEFORE").HasMaxLength(500);
            builder.Property(e => e.ValueAfter).HasColumnName("VAL_AFTER").HasMaxLength(500);
            builder.Property(e => e.KeyConds).HasColumnName("KEY_CONDS");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
