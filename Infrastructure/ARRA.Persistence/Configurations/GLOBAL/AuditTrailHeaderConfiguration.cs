﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class AuditTrailHeaderConfiguration : IEntityTypeConfiguration<AuditTrailHeader>
    {
        public void Configure(EntityTypeBuilder<AuditTrailHeader> builder)
        {
            builder.ToTable("TR_AUDITTRAIL_HDR");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.UserId).HasColumnName("USR_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuCode).HasColumnName("MENU_CD").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuName).HasColumnName("MENU_NM").HasMaxLength(50).IsRequired();
            builder.Property(e => e.ActivityAction).HasColumnName("ACTION").HasMaxLength(30).IsRequired();
            builder.Property(e => e.WithApproval).HasColumnName("WITH_APPRVL");
            builder.Property(e => e.ActivityDate).HasColumnName("ACTION_DT");
            builder.Property(e => e.ActivityHost).HasColumnName("ACTION_HOST").HasMaxLength(20);
            builder.Property(e => e.LinkToAudit).HasColumnName("LINK_TO_AUDIT");
            builder.Property(e => e.RefferalId).HasColumnName("REF_ID");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
