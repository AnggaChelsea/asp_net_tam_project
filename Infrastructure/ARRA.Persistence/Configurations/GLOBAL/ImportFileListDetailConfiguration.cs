﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ImportFileListDetailConfiguration : IEntityTypeConfiguration<ImportFileListDetail>
    {
        public void Configure(EntityTypeBuilder<ImportFileListDetail> builder)
        {
            builder.ToTable("TR_IMPORT_FILE_LIST_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.HeaderId).HasColumnName("HDR_ID").IsRequired();
            builder.Property(e => e.RowNumber).HasColumnName("ROW_NUM");
            builder.Property(e => e.ErrDesc).HasColumnName("ERR_DESC").HasMaxLength(1000);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);

        }

    }
}
