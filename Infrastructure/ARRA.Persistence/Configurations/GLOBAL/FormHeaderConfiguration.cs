﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormHeaderConfiguration : IEntityTypeConfiguration<FormHeader>
    {
        public void Configure(EntityTypeBuilder<FormHeader> builder)
        {
            builder.ToTable("MS_FORM_HDR");
            builder.HasKey(e => e.FormCode);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FormName).HasColumnName("FORM_NM").HasMaxLength(50);
            builder.Property(e => e.OJKFormCode).HasColumnName("OJK_FORM_CD").HasMaxLength(20);
            builder.Property(e => e.FormDesc).HasColumnName("FORM_DESC").HasMaxLength(200);
            builder.Property(e => e.TableName).HasColumnName("TABLE_NM").HasMaxLength(50);
            builder.Property(e => e.OriginalTableName).HasColumnName("ORIGINAL_TABLE_NM").HasMaxLength(30);
            builder.Property(e => e.FilterConditionEx).HasColumnName("FTR_CONDS").HasMaxLength(500);
            builder.Property(e => e.LinkToFormCode).HasColumnName("LINK_TO_FORM_CD").HasMaxLength(20);
            builder.Property(e => e.LinkFieldPK).HasColumnName("LINK_FORM_PK").HasMaxLength(30);
            builder.Property(e => e.LinkFieldFK).HasColumnName("LINK_FORM_FK").HasMaxLength(30);
            builder.Property(e => e.TFType).HasColumnName("TF_TYPE").HasMaxLength(10);
            builder.Property(e => e.TFDelimtedChar).HasColumnName("TF_DELIMITED_CHAR").HasColumnType("CHAR(1)");
            builder.Property(e => e.FormOrder).HasColumnName("FORM_ORDER").HasMaxLength(50);
            builder.Property(e => e.DatabaseName).HasColumnName("DB_NM").HasMaxLength(30);
            builder.Property(e => e.ExportDirect).HasColumnName("EXPORT_DIRECT");
            builder.Property(e => e.CheckModule).HasColumnName("CHECK_MODULE");
            builder.Property(e => e.CheckBranch).HasColumnName("CHECK_BRANCH");
            builder.Property(e => e.CheckLocalBranch).HasColumnName("CHECK_LOCAL_BRANCH");
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(30);
            builder.Property(e => e.TFPeriodCode).HasColumnName("TF_PERIOD_CD").HasMaxLength(5);
            builder.Property(e => e.TFSpecialCase).HasColumnName("TF_SPECIAL_CASE").HasMaxLength(50);
            builder.Property(e => e.BusinessType).HasColumnName("BUSINESS_TP").HasMaxLength(30);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
            builder.Property(e => e.LinkValResult).HasColumnName("LINK_VAL_RSLT").HasMaxLength(20);
            builder.Property(e => e.Archive).HasColumnName("ARCHIVE");
            builder.Property(e => e.ArchiveTotalKeepInMonths).HasColumnName("ARC_TOTAL_KEEP_MONTH");
            builder.Property(e => e.ArchiveDateFilter).HasColumnName("ARC_DATE_FTR_COLUMN").HasMaxLength(30);
            builder.Property(e => e.ShowCopy).HasColumnName("ShowCopy");
        }

    }
}
