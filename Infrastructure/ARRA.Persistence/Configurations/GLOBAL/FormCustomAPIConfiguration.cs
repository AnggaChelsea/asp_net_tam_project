﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormCustomAPIConfiguration : IEntityTypeConfiguration<FormCustomAPI>
    {
        public void Configure(EntityTypeBuilder<FormCustomAPI> builder)
        {
            builder.ToTable("MS_FORM_CUSTOM_API");
            builder.HasKey(e => e.UniqueId);
            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ApiName).HasColumnName("API_NAME").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ApiRoute).HasColumnName("API_ROUTE").HasMaxLength(50).IsRequired();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
