﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class MenuGroupDetailConfiguration : IEntityTypeConfiguration<MenuGroupDetail>
    {
        public void Configure(EntityTypeBuilder<MenuGroupDetail> builder)
        {
            builder.ToTable("MS_MENU_GROUP_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.GroupId).HasColumnName("GROUP_ID").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuCode).HasColumnName("MENU_CD").HasMaxLength(30).IsRequired();
            builder.Property(e => e.AllowView).HasColumnName("ALLOW_VIEW");
            builder.Property(e => e.AllowInsert).HasColumnName("ALLOW_INS");
            builder.Property(e => e.AllowUpdate).HasColumnName("ALLOW_UPD");
            builder.Property(e => e.AllowDelete).HasColumnName("ALLOW_DEL");
            builder.Property(e => e.AllowExport).HasColumnName("ALLOW_EXP");
            builder.Property(e => e.AllowImport).HasColumnName("ALLOW_IMP");
            builder.Property(e => e.AllowApproval).HasColumnName("ALLOW_APVL");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
