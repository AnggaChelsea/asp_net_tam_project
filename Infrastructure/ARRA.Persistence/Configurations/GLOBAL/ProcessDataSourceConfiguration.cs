﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ProcessDataSourceConfiguration : IEntityTypeConfiguration<ProcessDataSource>
    {
        public void Configure(EntityTypeBuilder<ProcessDataSource> builder)
        {
            builder.ToTable("MS_PROCESS_SOURCE_DATA");
            builder.HasKey(e => e.ProcessNo);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ProcessNo).HasColumnName("PROC_NO").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ProcessName).HasColumnName("PROC_NM").HasMaxLength(50).IsRequired();
            builder.Property(e => e.FileName).HasColumnName("FILE_NM").HasMaxLength(50);
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10);
            builder.Property(e => e.FileNameDateFormat).HasColumnName("FILE_NM_DT_FRMT").HasMaxLength(20);
            builder.Property(e => e.FileDateGap).HasColumnName("FILE_DT_GAP");
            builder.Property(e => e.SourcePath).HasColumnName("SOURCE_PATH").HasMaxLength(200);
            builder.Property(e => e.TargetPath).HasColumnName("TARGET_PATH").HasMaxLength(200);
            builder.Property(e => e.StartTimeCopyFile).HasColumnName("START_TIME_COPY").HasMaxLength(5);
            builder.Property(e => e.EndTimeCopyFile).HasColumnName("END_TIME_COPY").HasMaxLength(5);
            builder.Property(e => e.ETLPackage).HasColumnName("ETL_PACKAGE").HasMaxLength(100);
            builder.Property(e => e.UsingTF).HasColumnName("USING_TF");
            builder.Property(e => e.As32Bit).HasColumnName("AS_32BIT");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
