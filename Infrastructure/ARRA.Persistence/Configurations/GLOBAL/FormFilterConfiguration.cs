﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormFilterConfiguration : IEntityTypeConfiguration<FormFilter>
    {
        public void Configure(EntityTypeBuilder<FormFilter> builder)
        {
            builder.ToTable("MS_FORM_FILTER");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormId).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FieldName).HasColumnName("FIELD_NM").HasMaxLength(40);
            builder.Property(e => e.FieldSeq).HasColumnName("FIELD_SEQ");
            builder.Property(e => e.Operator).HasColumnName("OPERATOR").HasMaxLength(15);
            builder.Property(e => e.DefaultValue).HasColumnName("DEFAULT_VAL").HasMaxLength(80);
            builder.Property(e => e.DefaultValueType).HasColumnName("DEFAULT_VAL_TP").HasMaxLength(20);
            builder.Property(e => e.UseLookup).HasColumnName("USE_LOOKUP");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
            builder.Property(e => e.AutoRefresh).HasColumnName("AUTO_REFRESH");
        }

    }
}
