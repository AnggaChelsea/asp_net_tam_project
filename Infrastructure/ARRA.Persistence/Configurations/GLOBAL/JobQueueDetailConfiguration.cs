﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class JobQueueDetailConfiguration : IEntityTypeConfiguration<JobQueueDetail>
    {
        public void Configure(EntityTypeBuilder<JobQueueDetail> builder)
        {
            builder.ToTable("TR_JOB_QUEUED_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.HeaderId).HasColumnName("HDR_ID").IsRequired();
            builder.Property(e => e.ParamName).HasColumnName("PAR_NM").HasMaxLength(30).IsRequired();
            builder.Property(e => e.ParamValue).HasColumnName("PAR_VAL").HasMaxLength(5000).IsRequired();
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }

    }
}
