﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormNavbarConfiguration : IEntityTypeConfiguration<FormNavbar>
    {
        public void Configure(EntityTypeBuilder<FormNavbar> builder)
        {
            builder.ToTable("MS_FORM_NAVBAR");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormId).HasColumnName("FORM_CD").HasMaxLength(255).IsRequired();

            builder.Property(e => e.IsParent).HasColumnName("IS_PARENT").HasMaxLength(1);
            builder.Property(e => e.ActionGroup).HasColumnName("ACTION_GROUP").HasMaxLength(255);
            builder.Property(e => e.NavbarType).HasColumnName("NAVBAR_TYPE").HasMaxLength(255);
            builder.Property(e => e.ActionType).HasColumnName("ACTION_TYPE").HasMaxLength(255);
            builder.Property(e => e.ActionIcon).HasColumnName("ACTION_ICON").HasMaxLength(255);
            builder.Property(e => e.ActionTitle).HasColumnName("ACTION_TITLE").HasMaxLength(255);
            builder.Property(e => e.IsNeedPermission).HasColumnName("IS_NEED_PERMISSION").HasMaxLength(255);
            builder.Property(e => e.ActionPermission).HasColumnName("ACTION_PERMISSION").HasMaxLength(255);
            builder.Property(e => e.ActionSeq).HasColumnName("ACTION_SEQ");

            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(255);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(255);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(255);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(255);
        }

    }
}
