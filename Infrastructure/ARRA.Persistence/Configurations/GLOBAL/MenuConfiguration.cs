﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class MenuConfiguration : IEntityTypeConfiguration<Menu>
    {
        public void Configure(EntityTypeBuilder<Menu> builder)
        {
            builder.ToTable("MS_MENU");
            builder.HasKey(e => e.MenuCode);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuCode).HasColumnName("MENU_CD").HasMaxLength(30).IsRequired();
            builder.Property(e => e.ParentMenuCode).HasColumnName("PARENT_MENU_CD").HasMaxLength(30);
            builder.Property(e => e.MenuName).HasColumnName("MENU_NM").HasMaxLength(50).IsRequired();
            builder.Property(e => e.MenuDesc).HasColumnName("MENU_DESC").HasMaxLength(200);
            builder.Property(e => e.MenuSeq).HasColumnName("MENU_SEQ");
            builder.Property(e => e.MenuUrl).HasColumnName("MENU_URL").HasMaxLength(100);
            builder.Property(e => e.PageTitle).HasColumnName("PAGE_TITLE").HasMaxLength(80);
            builder.Property(e => e.Icon).HasColumnName("ICON").HasMaxLength(20);
            builder.Property(e => e.MenuSeq).HasColumnName("MENU_SEQ");
            builder.Property(e => e.FeatureInsert).HasColumnName("FTR_INS");
            builder.Property(e => e.FeatureUpdate).HasColumnName("FTR_UPD");
            builder.Property(e => e.FeatureDelete).HasColumnName("FTR_DEL");
            builder.Property(e => e.FeatureExport).HasColumnName("FTR_EXP");
            builder.Property(e => e.FeatureImport).HasColumnName("FTR_IMP");
            builder.Property(e => e.FeatureApproval).HasColumnName("FTR_APV");
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20);
            builder.Property(e => e.IsMenu).HasColumnName("IS_MENU");
            builder.Property(e => e.WaitApproval).HasColumnName("WAIT_APPRVL");
            builder.Property(e => e.Status).HasColumnName("ACTIVE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
