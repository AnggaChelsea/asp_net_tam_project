﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ExportCompareTempConfiguration : IEntityTypeConfiguration<ExportCompareTemp>
    {
        public void Configure(EntityTypeBuilder<ExportCompareTemp> builder)
        {
            builder.ToTable("TR_EXPORT_COMPARE_TEMP");
            builder.HasKey(e => e.UID);
            builder.HasKey(e => e.Line);

            builder.Property(e => e.UID).HasColumnName("UID").HasMaxLength(36).IsRequired();
            builder.Property(e => e.RefId).HasColumnName("REF_ID");
            builder.Property(e => e.Line).HasColumnName("LINE").IsRequired();
        }
    }
}