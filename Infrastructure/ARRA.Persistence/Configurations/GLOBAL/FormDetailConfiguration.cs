﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class FormDetailConfiguration : IEntityTypeConfiguration<FormDetail>
    {
        public void Configure(EntityTypeBuilder<FormDetail> builder)
        {
            builder.ToTable("MS_FORM_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.FieldName).HasColumnName("FIELD_NM").HasMaxLength(40).IsRequired();
            builder.Property(e => e.GrdColumnName).HasColumnName("GRD_COL_NM").HasMaxLength(50);
            builder.Property(e => e.GrdColumnShow).HasColumnName("GRD_COL_SHOW");
            builder.Property(e => e.GrdColumnSeq).HasColumnName("GRD_COL_SEQ");
            builder.Property(e => e.GrdAggregate).HasColumnName("GRD_AGGREGATE").HasMaxLength(8);
            builder.Property(e => e.EdrControlType).HasColumnName("EDR_DATA_TP").HasMaxLength(20);
            builder.Property(e => e.EdrLookupCode).HasColumnName("EDR_LOOKUP_CD").HasMaxLength(50);
            builder.Property(e => e.EdrLookupNested).HasColumnName("EDR_LOOKUP_NESTED").HasMaxLength(300);
            builder.Property(e => e.EdrReadonly).HasColumnName("EDR_READONLY");
            builder.Property(e => e.EdrMandatory).HasColumnName("EDR_MANDATORY");
            builder.Property(e => e.EdrShow).HasColumnName("EDR_SHOW");
            builder.Property(e => e.EdrDefaultValue).HasColumnName("EDR_DEFAULT_VAL").HasMaxLength(100);
            builder.Property(e => e.EdrMaxLength).HasColumnName("EDR_MAX_LENGTH");
            builder.Property(e => e.EdrFormatExpression).HasColumnName("EDR_FORMAT_EXPS").HasMaxLength(30);
            builder.Property(e => e.EdrLookupSrcTable).HasColumnName("EDR_LOOKUP_SRC_TAB").HasMaxLength(30);
            builder.Property(e => e.EdrLookupSrcCondition).HasColumnName("EDR_LOOKUP_SRC_CONDS").HasMaxLength(200);  
            builder.Property(e => e.EdrLookupText).HasColumnName("EDR_LOOKUP_TEXT").HasMaxLength(100);
            builder.Property(e => e.EdrLookupValue).HasColumnName("EDR_LOOKUP_VAL").HasMaxLength(100);
            builder.Property(e => e.TFShow).HasColumnName("TF_SHOW");
            builder.Property(e => e.TFLength).HasColumnName("TF_LENGTH");
            builder.Property(e => e.TFAlignment).HasColumnName("TF_ALIGN").HasColumnType("CHAR(1)");
            builder.Property(e => e.TFReplicateChar).HasColumnName("TF_REPLICATE_CHR").HasColumnType("CHAR(1)");
            builder.Property(e => e.TFAbsolute).HasColumnName("TF_ABSOLUTE");
            builder.Property(e => e.TFFormat).HasColumnName("TF_FORMAT").HasMaxLength(10);
            builder.Property(e => e.TFDefaultValue).HasColumnName("TF_DEFAULT_VAL").HasMaxLength(50);
            builder.Property(e => e.ChangesNote).HasColumnName("CHANGES_NOTE").HasMaxLength(100);
            builder.Property(e => e.IdElement).HasColumnName("ID_ELEMENT").HasMaxLength(100);
            builder.Property(e => e.Status).HasColumnName("ACTIVE");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
            builder.Property(e => e.EdrDefaultNestedField).HasColumnName("EDR_DEFAULT_NESTED_FIELD").HasMaxLength(300);
            builder.Property(e => e.EdrDefaultNestedSrc).HasColumnName("EDR_DEFAULT_NESTED_CUSTOM_SRC").HasMaxLength(300);
            builder.Property(e => e.EdrDefaultNestedMap).HasColumnName("EDR_DEFAULT_NESTED_MAP_CD").HasMaxLength(300);
            builder.Property(e => e.EdrCalculation).HasColumnName("EDR_CALCULATION").HasMaxLength(1000);
        }

    }
}
