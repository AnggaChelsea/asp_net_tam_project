﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class BusinessParamDetailConfiguration : IEntityTypeConfiguration<BusinessParamDetail>
    {
        public void Configure(EntityTypeBuilder<BusinessParamDetail> builder)
        {
            builder.ToTable("MS_BUSINESS_PARAM_DTL");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.ParamCode).HasColumnName("PAR_CD").HasMaxLength(20).IsRequired();
            builder.Property(e => e.ParamValue).HasColumnName("PAR_VALUE").HasMaxLength(100).IsRequired();
            builder.Property(e => e.ParamValue2).HasColumnName("PAR_VALUE_2").HasMaxLength(100).IsRequired();
            builder.Property(e => e.ParamValue3).HasColumnName("PAR_VALUE_3").HasMaxLength(100).IsRequired();
            builder.Property(e => e.ParamMapping).HasColumnName("PAR_VAL_MAP").HasMaxLength(100);
            builder.Property(e => e.ParamMapping2).HasColumnName("PAR_VAL_MAP_2").HasMaxLength(100);
            builder.Property(e => e.ParamMapping3).HasColumnName("PAR_VAL_MAP_3").HasMaxLength(100);
            builder.Property(e => e.ParamDesc).HasColumnName("PAR_DESC").HasMaxLength(200);
            builder.Property(e => e.ParamSeq).HasColumnName("PAR_SEQ");
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);


        }

    }
}
