﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ARRA.GLOBAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Persistence.Configurations.GLOBAL
{
    public class ApprovalHeaderConfiguration : IEntityTypeConfiguration<ApprovalHeader>
    {
        public void Configure(EntityTypeBuilder<ApprovalHeader> builder)
        {
            builder.ToTable("TR_APPROVAL_HDR");
            builder.HasKey(e => e.UniqueId);

            builder.Property(e => e.UniqueId).HasColumnName("UID").IsRequired();
            builder.Property(e => e.Module).HasColumnName("MODULE").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuCode).HasColumnName("MENU_CD").HasMaxLength(30).IsRequired();
            builder.Property(e => e.MenuName).HasColumnName("MENU_NM").HasMaxLength(50).IsRequired();
            builder.Property(e => e.TaskType).HasColumnName("TASK_TP").HasMaxLength(20);
            builder.Property(e => e.ActionType).HasColumnName("ACTION_TP").HasMaxLength(15);
            builder.Property(e => e.ApprovalStatus).HasColumnName("APPRV_STS").HasMaxLength(10);
            builder.Property(e => e.LinkToHeaderId).HasColumnName("LINK_TO_HDR_ID");
            builder.Property(e => e.RefferalId).HasColumnName("REF_ID");
            builder.Property(e => e.FormCode).HasColumnName("FORM_CD").HasMaxLength(20);
            builder.Property(e => e.PeriodType).HasColumnName("PERIOD_TP").HasMaxLength(10);
            builder.Property(e => e.TableName).HasColumnName("TABLE_NM").HasMaxLength(30);
            builder.Property(e => e.Note).HasColumnName("NOTE").HasMaxLength(300);
            builder.Property(e => e.IdentityFK).HasColumnName("IDENTITY_FK");
            builder.Property(e => e.FileName).HasColumnName("FILE_NM").HasMaxLength(100);
            builder.Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(50);
            builder.Property(e => e.CreatedDate).HasColumnName("CREATED_DT");
            builder.Property(e => e.CreatedHost).HasColumnName("CREATED_HOST").HasMaxLength(20);
            builder.Property(e => e.ModifiedBy).HasColumnName("MODIFIED_BY").HasMaxLength(50);
            builder.Property(e => e.ModifiedDate).HasColumnName("MODIFIED_DT");
            builder.Property(e => e.ModifiedHost).HasColumnName("MODIFIED_HOST").HasMaxLength(20);
        }
    }
}
