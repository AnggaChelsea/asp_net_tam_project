﻿using Microsoft.EntityFrameworkCore;
using ARRA.Persistence.Infrastructure;

namespace ARRA.Persistence
{
    public class MODULEDbContextFactory : MODULEDesignTimeDbContextFactoryBase<MODULEDbContext>
    {
        protected override MODULEDbContext CreateNewInstance(DbContextOptions<MODULEDbContext> options)
        {
            return new MODULEDbContext(options);
        }
    }
}