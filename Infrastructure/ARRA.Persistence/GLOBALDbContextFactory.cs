﻿using Microsoft.EntityFrameworkCore;
using ARRA.Persistence.Infrastructure;

namespace ARRA.Persistence
{
    public class GLOBALDbContextFactory : GLOBALDesignTimeDbContextFactoryBase<GLOBALDbContext>
    {
        protected override GLOBALDbContext CreateNewInstance(DbContextOptions<GLOBALDbContext> options)
        {
            return new GLOBALDbContext(options);
        }
    }
}