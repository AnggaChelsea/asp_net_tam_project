﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ARRA.Persistence
{
    public class ROBINInitializer
    {
        //private readonly Dictionary<int, Customer> Customers = new Dictionary<int, Customer>();
        
        public static void Initialize(GLOBALDbContext ctxGlobal, MODULEDbContext ctxModule)
        {
            Console.WriteLine("Intial Data");

            var initializer = new ROBINInitializer();

            initializer.InitialDataAll(ctxGlobal, ctxModule);
        }

        public void InitialDataAll(GLOBALDbContext ctxGlobal,MODULEDbContext ctxModule)
        {
            ctxModule.Database.EnsureCreated();

            //if (context.Customers.Any())
            //{
            //    return; // Db has been filled
            //}

            FillData(ctxGlobal);
        }

        public void FillData(GLOBALDbContext context)
        {
            //var customers = new[]
            //{

            //    new Customer { CustomerId = "ALFKI", CustomerName = "Alfreds Futterkiste" },
            //};

            //context.Customers.AddRange(customers);

            //context.SaveChanges();
        }

    }
}
