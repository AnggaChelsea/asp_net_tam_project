﻿using System;
using Microsoft.EntityFrameworkCore;
using ARRA.Persistence.Extensions.GLOBAL;
using System.Linq;
using ARRA.GLOBAL.Domain.Entities;

namespace ARRA.Persistence
{
    public class GLOBALDbContext : DbContext
    {
        public GLOBALDbContext(DbContextOptions<GLOBALDbContext> options)
            : base(options)
        {
        }

        //table here
        public DbSet<AdhocQuery> AdhocQueries { get; set; }
        public DbSet<AdhocQueryParam> AdhocQueryParams { get; set; }
        public DbSet<AdhocQueryAccess> AdhocQueryAccesses { get; set; }
        public DbSet<ImportFileListHeader> ImportFileListHeaders { get; set; }
        public DbSet<ImportFileListDetail> ImportFileListDetails { get; set; }
        public DbSet<ExportCompareTemp> ExportCompareTemps { get; set; }
        public DbSet<FormCustomAPI> FormCustomAPIes { get; set; }
        public DbSet<ExportFileList> ExportFileLists { get; set; }
        public DbSet<JobQueueHeader> JobQueueHeaders { get; set; }
        public DbSet<JobQueueDetail> JobQueueDetails { get; set; }
        public DbSet<ReportingDate> ReportingDates { get; set; }
        public DbSet<ApprovalDetail> ApprovalDetails { get; set; }
        public DbSet<ApprovalGroupDetail> ApprovalGroupDetails { get; set; }
        public DbSet<ApprovalGroupHeader> ApprovalGroupHeaders { get; set; }
        public DbSet<ApprovalHeader> ApprovalHeaders { get; set; }
        public DbSet<AuditTrailDetail> AuditTrailDetails { get; set; }
        public DbSet<AuditTrailHeader> AuditTrailHeaders { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<BranchGroupDetail> BranchGroupDetails { get; set; }
        public DbSet<BranchGroupHeader> BranchGroupHeaders { get; set; }
        public DbSet<BusinessParamDetail> BusinessParamDetails { get; set; }
        public DbSet<BusinessParamHeader> BusinessParamHeaders { get; set; }
        public DbSet<FormDetail> FormDetails { get; set; }
        public DbSet<FormHeader> FormHeaders { get; set; }
        public DbSet<FormSearchField> FormSearchFields { get; set; }
        public DbSet<FormSearchList> FormSearchLists { get; set; }
        public DbSet<IssueList> IssueLists { get; set; }
        public DbSet<LogonActivity> LogonActivities { get; set; }
        public DbSet<ManualUploadDetail> ManualUploadDetails { get; set; }
        public DbSet<ManualUploadHeader> ManualUploadHeaders { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuGroupDetail> MenuGroupDetails { get; set; }
        public DbSet<MenuGroupHeader> MenuGroupHeaders { get; set; }
        public DbSet<ModuleAccess> ModuleAccesses { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ProcessDataSource> ProcessDataSources { get; set; }
        public DbSet<ProcessDependency> ProcessDependeciess { get; set; }
        public DbSet<ProcessLogDetail> ProcessLogDetails { get; set; }
        public DbSet<ProcessLogHeader> ProcessLogHeaders { get; set; }
        public DbSet<RegulatorBranch> RegulatorBranchs { get; set; }
        public DbSet<SystemParameterDetail> SystemParameterDetails { get; set; }
        public DbSet<SystemParameterHeader> SystemParameterHeaders { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserPasswordHistory> UserPasswordHistories { get; set; }

        public DbSet<FormFilter> FormFilters { get; set; }
        public DbSet<FormActionRow> FormActionRows { get; set; }
        public DbSet<FormNavbar> FormNavbars { get; set; }
        public DbSet<UserFormFilter> UserFormFilters { get; set; }
        public DbSet<BranchPeriod> BranchPeriods { get; set; }
        public DbSet<ManualFaq> ManualFaqs { get; set; }
        public DbSet<ActiveSession> ActiveSession { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))// || p.ClrType == typeof(string?)))
            {
                // impact migration
                //if (string.IsNullOrEmpty(property.Relational().ColumnType)){
                //    property.Relational().ColumnType = "varchar(" + property.GetMaxLength().ToString() + ")";
                //}
                if (string.IsNullOrEmpty(property.GetColumnType()))
                {
                    property.SetColumnType("varchar(" + property.GetMaxLength().ToString() + ")");
                }
            }

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(DateTime) || p.ClrType == typeof(DateTime?)))
            {
                // impact migration
                //if (string.IsNullOrEmpty(property.Relational().ColumnType)){
                //    property.Relational().ColumnType = "datetime";
                //}
                if (string.IsNullOrEmpty(property.GetColumnType()))
                {
                    property.SetColumnType("datetime");
                }
            }


        }
    }
}
