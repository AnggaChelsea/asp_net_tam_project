﻿using System;
using Microsoft.EntityFrameworkCore;
using ARRA.Persistence.Extensions.MODULE;
using System.Linq;
using ARRA.MODULE.Domain.Entities;
using ARRA.MODULE.Domain.Entities;

namespace ARRA.Persistence
{
    public class MODULEDbContext : DbContext
    {
        public MODULEDbContext(DbContextOptions<MODULEDbContext> options)
            : base(options)
        {
        }

        #region Table
        //table here
        public DbSet<ValidationScenarioResult> ValidationScenarioResults { get; set; }
        public DbSet<ValidationExceptionSummary> ValidationExceptionSummaries { get; set; }
        public DbSet<BIValidationException> BIValidationExceptions { get; set; }
        public DbSet<BIValidationTool> BIValidationTools { get; set; }
        public DbSet<LockData> LockDatas { get; set; }
        public DbSet<GenerateTextFile> GenerateTextFiles { get; set; }
        public DbSet<InterFormValidationMap> InterFormValidationMaps { get; set; }
        public DbSet<InterFormValidationResult> InterFormValidationResults { get; set; }
        public DbSet<ProcessDependencies> ProcessDependenciess { get; set; }
        public DbSet<TransformProcess> TransformProcesses { get; set; }
        public DbSet<Validation> Validations { get; set; }
        public DbSet<ValidationResult> ValidationResults { get; set; }
        public DbSet<ProcessLogDetail> ProcessLogDetails { get; set; }
        public DbSet<ProcessLogHeader> ProcessLogHeaders { get; set; }
        public DbSet<JobQueueHeader> JobQueueHeaders { get; set; }
        public DbSet<JobQueueDetail> JobQueueDetails { get; set; }
        public DbSet<TransformFormMapping> TransformFormMappings { get; set; }
        public DbSet<ReportMaster> ReportMasters { get; set; }
        public DbSet<ReportRow> ReportRows { get; set; }
        public DbSet<ReportRowColumn> ReportRowColumns { get; set; }
        public DbSet<ReportColumn> ReportColumns { get; set; }
        public DbSet<ReportFilter> ReportFilters { get; set; }
        public DbSet<ExportFileList> ExportFileLists { get; set; }
        public DbSet<ReportTableWeb> ReportTableWeb { get; set; }
        public DbSet<ReportTableDataWeb> ReportTableDataWeb { get; set; }
        public DbSet<ReportStaticCell> ReportStaticCell { get; set; }
        #endregion

        #region Link to Table
        public DbSet<ARRA.GLOBAL.Domain.Entities.User> SN_Users { get; set; }
        public DbSet<ARRA.GLOBAL.Domain.Entities.Branch> SN_Branches { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))// || p.ClrType == typeof(string?)))
            {
                // impact migration
                //if (string.IsNullOrEmpty(property.Relational().ColumnType)){
                //    property.Relational().ColumnType = "varchar(" + property.GetMaxLength().ToString() + ")";
                //}
                if (string.IsNullOrEmpty(property.GetColumnType()))
                {
                    property.SetColumnType("varchar(" + property.GetMaxLength().ToString() + ")");
                }
            }

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(DateTime) || p.ClrType == typeof(DateTime?)))
            {
                // impact migration
                //if (string.IsNullOrEmpty(property.Relational().ColumnType)){
                //    property.Relational().ColumnType = "datetime";
                //}
                if (string.IsNullOrEmpty(property.GetColumnType()))
                {
                    property.SetColumnType("datetime");
                }
            }


        }
    }
}
