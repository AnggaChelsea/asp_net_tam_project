﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ARRA.Persistence.Infrastructure
{

    public abstract class GLOBALDesignTimeDbContextFactoryBase<TContext> :
        IDesignTimeDbContextFactory<TContext> where TContext : DbContext
    {
        private const string GLOBAL_ConnectionStringName = "GLOBAL_ConStr";
        private const string AspNetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";

        public TContext CreateDbContext(string[] args)
        {
            return Create(Directory.GetCurrentDirectory(), Environment.GetEnvironmentVariable(AspNetCoreEnvironment));
        }

        protected abstract TContext CreateNewInstance(DbContextOptions<TContext> options);

        private TContext Create(string basePath,string environmentName)
        {
            var configuration = new ConfigurationBuilder()
                //.SetBasePath(basePath + "\\..\\Presentation\\RESTAPI\\RR.API")
                .SetBasePath(basePath + "\\")
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.Local.json", optional: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true)
                .Build();

            var GLOBAL_ConStr = configuration.GetConnectionString(GLOBAL_ConnectionStringName);
            
            return Create(GLOBAL_ConStr);
        }

        private TContext Create(string GLOBAL_ConStr)
        {
            if (string.IsNullOrEmpty(GLOBAL_ConStr))
            {
                throw new ArgumentException($"Connection string '{GLOBAL_ConStr}' is null or empty.", nameof(GLOBAL_ConStr));
            }


            Console.WriteLine($"DesignTimeDbContextFactoryBase.Create(string): Connection string: '{GLOBAL_ConStr}'.");

            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

            optionsBuilder.UseSqlServer(GLOBAL_ConStr);

            return CreateNewInstance(optionsBuilder.Options);
        }
    }
}