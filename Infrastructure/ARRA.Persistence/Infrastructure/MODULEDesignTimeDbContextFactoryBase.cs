﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ARRA.Persistence.Infrastructure
{
    public abstract class MODULEDesignTimeDbContextFactoryBase<TContext> :
        IDesignTimeDbContextFactory<TContext> where TContext : DbContext
    {
        private const string GLOBAL_ConnectionStringName = "GLOBAL_ConStr";
        private const string ANTASENA_ConnectionStringName = "ANTASENA_ConStr";
        private const string AspNetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";

        public TContext CreateDbContext(string[] args)
        {
            return Create(Directory.GetCurrentDirectory(), Environment.GetEnvironmentVariable(AspNetCoreEnvironment));
        }

        protected abstract TContext CreateNewInstance(DbContextOptions<TContext> options);

        private TContext Create(string basePath,string environmentName)
        {
            var configuration = new ConfigurationBuilder()
                //.SetBasePath(basePath + "\\..\\Presentation\\RESTAPI\\RR.API")
                .SetBasePath(basePath + "\\")
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.Local.json", optional: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true)
                .Build();

            var ANTASENA_ConStr = configuration.GetConnectionString(ANTASENA_ConnectionStringName);

            return Create(ANTASENA_ConStr);
        }

        private TContext Create(string ANTASENA_ConStr)
        {

            if (string.IsNullOrEmpty(ANTASENA_ConnectionStringName))
            {
                throw new ArgumentException($"Connection string '{ANTASENA_ConStr}' is null or empty.", nameof(ANTASENA_ConStr));
            }

            Console.WriteLine($"DesignTimeDbContextFactoryBase.Create(string): Connection string: '{ANTASENA_ConStr}'.");

            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

            optionsBuilder.UseSqlServer(ANTASENA_ConStr);

            return CreateNewInstance(optionsBuilder.Options);
        }
    }
}