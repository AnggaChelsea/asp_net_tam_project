﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Model
{
    public class AccessMatrixModel
    {
        public string MenuCode { get; set; }
        public string Module { get; set; }
        public bool AllowInsert { get; set; }
        public bool AllowUpdate { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowExport { get; set; }
        public bool AllowImport { get; set; }
        public bool AllowApproval { get; set; }

    }
}