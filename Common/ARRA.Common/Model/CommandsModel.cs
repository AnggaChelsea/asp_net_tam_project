﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Model
{
    public class CommandsModel<TRequest, TResponse> : IRequest<TResponse> where TRequest : class
        where TResponse : class
    {
        public UserIdentityModel UserIdentity { get; set; }
        public AccessMatrixModel AccessMatrix { get; set; }

        public TRequest CommandModel { get; set; }

        public DateTime CurrentDateTime { get; set; }
    }
}