﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ARRA.Common.Decrypt;

namespace ARRA.Common.Model
{
    public class ApplicationSettings
    {
        private const string _GLOBAL_ConnectionString = "GLOBAL_ConStr";
        private const string _GLOBAL_Read_ConnectionString = "GLOBAL_Read_ConStr";
        private const string _ANTASENA_ConnectionString = "ANTASENA_ConStr";
        private const string _RWA_ConnectionString = "RWA_ConStr";
        private const string _AspNetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";
        private const string _UploadFileSize = "ApplicationSettings:UPLOAD_MAX_SIZE";

        private const string _Password_MinLength = "ApplicationSettings:PASSWORD_POLICY:MINIMUM_LENGTH";
        private const string _Password_Uppercase = "ApplicationSettings:PASSWORD_POLICY:UPPERCASE_CHARS";
        private const string _Password_Lowercase = "ApplicationSettings:PASSWORD_POLICY:LOWERCASE_CHARS";
        private const string _Password_Numeric = "ApplicationSettings:PASSWORD_POLICY:NUMERIC_CHARS";
        private const string _Password_SpecialChar = "ApplicationSettings:PASSWORD_POLICY:SPECIAL_CHARS";
        private const string _Password_HistoryCount = "ApplicationSettings:PASSWORD_POLICY:PASS_HISTORY_COUNT";
        private const string _Password_Expired = "ApplicationSettings:PASSWORD_POLICY:PASS_EXPIRED";

        private const string _Login_AutomaticUnlock = "ApplicationSettings:LOGIN_POLICY:AUTOMATIC_UNLOCK";
        private const string _Login_MaximumFailed = "ApplicationSettings:LOGIN_POLICY:MAXIMUM_INVALID_LOGIN";

        private const string _ETL_Location = "ApplicationSettings:ETL:Location";
        private const string _ETL_Password = "ApplicationSettings:ETL:Password";
        private const string _ETL_RIM_ETLConString = "ApplicationSettings:ETL:RIM_ETLConStr";
        private const string _ETL_STAGING_ETLConString = "ApplicationSettings:ETL:STAGING_ETLConStr";
        private const string _ETL_DATA_MART_ETLConString = "ApplicationSettings:ETL:DATA_MART_ETLConStr";
        private const string _ETL_SOURCE_DATA_ETLConString = "ApplicationSettings:ETL:SOURCE_DATA_ETLConStr";
        private const string _ETL_BI_VAL_STG_DATA_ETLConString = "ApplicationSettings:ETL:BI_VAL_STG_DATA_ETLConStr";
        private const string _ETL_32BIT_EXE_PATH = "ApplicationSettings:ETL:ETL_32BIT_EXE_PATH";
        private const string _ServiceModule = "ApplicationSettings:ServiceModule";

        //For LDAP
        //      "LdapSettings": {
        //  "IS_LDAP": "1",
        //  "ServerName": "myserver",
        //  "ServerPort": 636,
        //  "UseSSL": "1",
        //  "Credentials": {
        //    "DomainUserName": "mydomain\\admin",
        //    "Password": "my_password"
        //  },
        //  "SearchBase": "CN=Users,DC=mydomain,DC=local",
        //  "ContainerName": "CN=Users,DC=mydomain,DC=local",
        //  "DomainName": "mydomain.local",
        //  "DomainDistinguishedName": "DC=mydomain,DC=local"
        //}


        private const string _IS_LDAP = "LdapSettings:IS_LDAP";
        private const string _LDAP_USE_DIRECTORY_SEARCHER = "LdapSettings:USE_DIRECTORY_SEARCHER";
        private const string _LDAP_ServerName = "LdapSettings:ServerName";
        private const string _LDAP_ServerPort = "LdapSettings:ServerPort";
        private const string _LDAP_UseSSL = "LdapSettings:UseSSL";
        private const string _LDAP_CRECEDENTIALS_DomainUserName = "LdapSettings:Credentials:DomainUserName";
        private const string _LDAP_CRECEDENTIALS_DomainPassword = "LdapSettings:Credentials:Password";
        private const string _LDAP_SearchBase = "LdapSettings:SearchBase";
        private const string _LDAP_ContainerName = "LdapSettings:ContainerName";
        private const string _LDAP_DomainName = "LdapSettings:DomainName";
        private const string _LDAP_DomainDistinguishedName = "LdapSettings:DomainDistinguishedName";


        public string ISLDAP
        {
            get {
                return Convert.ToString(_configuration.GetSection(_IS_LDAP).Value);
            }
        }

        public string LDAPUseDirectorySearcher
        {
            get {
                return Convert.ToString(_configuration.GetSection(_LDAP_USE_DIRECTORY_SEARCHER).Value);
            }
        }

        public string LDAPServerName
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_LDAP_ServerName).Value);
            }
        }

        public string LDAPServerPort
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_LDAP_ServerPort).Value);
            }
        }




        public string ETL32BitEXEPath
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_ETL_32BIT_EXE_PATH).Value);
            }
        }
        public string ETLBIVALSTagingConStr
        {
            get
            {
                return DecryptText(Convert.ToString(_configuration.GetSection(_ETL_BI_VAL_STG_DATA_ETLConString).Value), _configuration);
            }
        }
        public string ServiceModule
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_ServiceModule).Value);
            }
        }

        public string ETLDataMartConStr
        {
            get
            {
                return DecryptText(Convert.ToString(_configuration.GetSection(_ETL_DATA_MART_ETLConString).Value), _configuration);
            }
        }

        public string GetConnectionString(string config)
        {
            return  DecryptText(Convert.ToString(_configuration.GetConnectionString(config)), _configuration);
        }

        public string ETLPassword
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_ETL_Password).Value);
            }
        }
        public string ETLRIMConStr
        {
            get
            {
                return DecryptText(Convert.ToString(_configuration.GetSection(_ETL_RIM_ETLConString).Value), _configuration);
            }
        }
        
            public string ETLSourceDataConStr
        {
            get
            {
                return DecryptText(Convert.ToString(_configuration.GetSection(_ETL_SOURCE_DATA_ETLConString).Value), _configuration);
            }
        }
        public string ETLStagingConStr
        {
            get
            {
                return DecryptText(Convert.ToString(_configuration.GetSection(_ETL_STAGING_ETLConString).Value), _configuration);
            }
        }

        public string ETLLocation
        {
            get
            {
                return Convert.ToString(_configuration.GetSection(_ETL_Location).Value);
            }
        }

        public int LoginMaximumFailed
        {
            get
            {
                return Convert.ToInt32(_configuration.GetSection(_Login_MaximumFailed).Value);
            }
        }

        public double LoginAutomaticUnlock
        {
            get
            {
                return Convert.ToDouble(_configuration.GetSection(_Login_AutomaticUnlock).Value);
            }
        }
        public int PasswordExpiredDay
        {
            get
            {
                return Convert.ToInt32(_configuration.GetSection(_Password_Expired).Value);
            }
        }
        public int PasswordHistoryCount
        {
            get
            {
                return Convert.ToInt32(_configuration.GetSection(_Password_HistoryCount).Value);
            }
        }
        public bool PasswordSpecialChar
        {
            get
            {
                return Convert.ToBoolean(_configuration.GetSection(_Password_SpecialChar).Value);
            }
        }
        public bool PasswordNumeric
        {
            get
            {
                return Convert.ToBoolean(_configuration.GetSection(_Password_Numeric).Value);
            }
        }
        public bool PasswordLowercase
        {
            get
            {
                return Convert.ToBoolean(_configuration.GetSection(_Password_Lowercase).Value);
            }
        }
        public bool PasswordUppercase
        {
            get
            {
                return Convert.ToBoolean(_configuration.GetSection(_Password_Uppercase).Value);
            }
        }
        public int PasswordMinLength
        {
            get
            {
                return Convert.ToInt32(_configuration.GetSection(_Password_MinLength).Value);
            }
        }


        public string JWT_Secret { get; set; }
        public string AuthenticationSchema { get; set; }

        private readonly IConfiguration _configuration;
        public ApplicationSettings(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public int UploadMaxFileSize
        {
            get
            {
                return Convert.ToInt32(_configuration.GetSection(_UploadFileSize).Value);
            }
        }

        public string ASPNET_Environment
        {
            get
            {
                return Environment.GetEnvironmentVariable(_AspNetCoreEnvironment);
            }
        }

        public string GLOBAL_ConStr
        {
            get
            {
                return Environment.GetEnvironmentVariable(_GLOBAL_ConnectionString);
            }
        }

        public string GLOBAL_Read_ConStr
        {
            get
            {
                return Environment.GetEnvironmentVariable(_GLOBAL_Read_ConnectionString);
            }
        }

        public string ANTASENA_ConStr
        {
            get
            {
                return Environment.GetEnvironmentVariable(_ANTASENA_ConnectionString);
            }
        }

        public string RWA_ConStr
        {
            get
            {
                return Environment.GetEnvironmentVariable(_RWA_ConnectionString);
            }
        }

    }
}
