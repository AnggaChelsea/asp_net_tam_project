﻿using ARRA.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Model
{
    public class ExportProgressModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public string percentage { get; set; }        
        public string jobId { get; set; }        
    }
}
