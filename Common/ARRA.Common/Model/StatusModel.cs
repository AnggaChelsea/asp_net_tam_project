﻿using ARRA.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Model
{
    public class StatusModel
    {
        public string status { get; set; }

        public string message { get; set; }
        public long id { get; set; }
        
    }
}
