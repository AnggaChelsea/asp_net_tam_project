﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common
{
    public class ErrorMessages
    {
        public const string PasswordEmpty = "Password Required";
        public const string NewPasswordEmpty = "New Password Required";
        public const string OldPasswordInvalid = "Invalid Old Password";
        public const string OldNewPasswordSame = "New Password Cannot Same with Old Password";
        public const string PasswordNotMatch = "New Password and Confirm Password Not Match";
        public const string PasswordLength = "Password Doesn't Meet the Minimum Length ({0})";
        public const string PasswordUppercaseLetter = "Password Must Contains Uppercase Characters";
        public const string PasswordLowercaseLetter = "Password Must Contains Lowercase Characters";
        public const string PasswordDigit = "Password Must Contains Number Characters";
        public const string PasswordSpecialCharacter = "Password Must Contains Special Characters";
        public const string PasswordSameWithPrevPass = "Password Already Used Before (Cannot Same with {0} Password Before)";
        public const string UserInactive = "User Inactived, Please Contact Your Administrator";
        public const string ProvideActiveFlag = "Please provide active flag column to delete";
        public const string LoginInvalid = "Invalid Username or Password";
        public const string LoginLocked = "Account Locked, Please Contact Your Administrator to Unlock";
        public const string LoginLockedAuto = "Account Locked, Please Try Again After {0} Minutes";
        public const string PasswordExpired = "Password Expired, Please Change Password First";
        public const string NotAUthorizedToAction = "You are not authorized to do this action.";
        public const string ItemActioned = "This Task Already Approved/Rejected/Revised/Revoked, Please Refresh the Task List";
        public const string ApprovalGroupEmpty = "Approval Group is Empty, Please Assign User to Selected Group First";
        public const string ApprovalGroupRequired = "Approval Group is Required for Selected Menu Group";
        public const string UserActiveDelete = "Cannot Delete an Active User";
        public const string UserExists = "User with This Id Already Exists";
        public const string FileNotExists = "File Not Exists ({0})";
        public const string InvalidKeyValue = "Invalid Key Value ({0})";
        public const string GroupIsUsed = "This Group Is Still Used By Active User (cannot be deleted)";
        public const string AlreadyLock = "Already locked before, please check lock list";
        public const string AddFilterDate = "Please add date filter first";
        public const string AddFilterDatePeriod = "Please add date and period filter first";
        public const string BranchLocked = "Inputted Branch is Locked, Please Unlock First";
        public const string NotAuthorizedBranch = "You have no access for this branch ({0})";
        public const string ValidationExists = "Validation exists, Please cancel first to re-run";
        public const string ValidationPackageNotFound = "Validation Package Not Found, Please check System Parameter";
        public const string ReconciliationPackageNotFound = "Reconciliation Package Not Found, Please check System Parameter";
        public const string GenerateTFExists = "Generate TF exists, Please cancel first to re-run";
        public const string LockDataBeforeGenerateTF = "Please lock all local branch for this form first";
        public const string LockDataBeforeGenerateTFWoBR = "Please Lock this Form First";

        public const string NotAuthorized = "You Are Not Authorized to View This Screen. \nPlease Contact Your Administrator to Request The Access";
        public const string ZipFileFailed = "Zip File Failed";
        public const string ReconciliationExists = "Reconciliation exists, please cancel first to re-run";
        public const string UnidentifiedJob = "Unidentified Job Type";
        public const string GroupIdModuleRequired = "Group Id and Module Id Required";
        public const string NotAllowSave = "Your are not allow to save this record";
        public const string PathNotFound = "Path not found {0}";
        public const string FileNotFound = "File not found {0}";
        public const string UploadDataEmpty = "Please choose upload type first";
        public const string AddFilterToSave = "Please Add Filter (User Defined Filter) to Save First";
        public const string DuplicateUser = "Duplicate User on Group ({0})";
        public const string DuplicateBranch = "Duplicate Branch on Group ({0})";
        public const string BIValidationFailed = "Validation Failed, Please Check BI Validation Exception";
        public const string ReferenceNotFound = "Reference column not found";
        public const string ApprovalGroupActiveDelete = "Approval group is being used by an active user and cannot be deleted";
        public const string BranchGroupActiveDelete = "Branch group is being used by an active user and cannot be deleted";
        public const string BIBranchRequired = "BI Branch value required";
        public const string BranchRequired = "Bank Local Branch value required";
        public const string BIBranchAuth = "BI Branch not authorized ({0})";
        public const string BranchAuth = "Bank Local Branch not authorized ({0})";
        public const string LDAPSettingsInvalid = "Please Check LDAP Settings";
        public const string LDAPUserInvalid = "Please contact your Local Administrator, Username or Password Invalid";

        public const string CompleteValidationBeforeGenerateTF = "Please complete validation first";
        public const string CompleteReconBeforeGenerateTF = "Please complete reconciliation first";
        public const string UserActiveSession = "User is currently login or not logout properly, Please Contact Your Administrator";

        public static string GetSQLErrorMessage(int number,string message)
        {
            string result = "";
            switch (number)
            {
                case 8152:
                    result = "Invalid Text Length";
                    break;
                case 2627:
                    result = "Data with this key already exists, please input another one";
                    break;
                case 515:
                    result = "Please provide value for key columns. Exception detail ("+message+")";
                    break;
                case 236:
                case 241:
                case 245:
                case 248:
                    result = "Conversion data failed. Please check your data.";
                    break;
                case 8115:
                    result = "Data input exceeds maximum length. Please check your data.";
                    break;
                default:
                    result = message +"("+number.ToString()+")";
                    break;
            }

            return result;
        }
    }
}