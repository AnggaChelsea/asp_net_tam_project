﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common
{
    public class InfoMessages
    {
        public const string PendingApprovalStatus = "This Record is Pending for Approval, Last Submit by {0} on {1} with Action {2}";
        public const string WaitDependencies = "Waiting for Dependencies Process ({0})";
        public const string RecordExecuted = "Total {0} Executed";
    }
}
