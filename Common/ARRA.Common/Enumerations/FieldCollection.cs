﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Enumerations
{
    public enum FieldCollection
    {
        ERR_FLAG_X,
        RG_BRANCH,
        BRANCH,
        PERIOD_TP,
        GROUP_ID,
        MODULE,
        ID_OPRSNL,
        BRANCH_CD,
        USR_ID,
        JNS_TR_ANTAR_BANK, //puabpuasdoc
        PERAN_PELPOR_, //puabpuabdoc
        JNS_VAL,//puabpuabdoc
        JAM_TR, //puabpuabdoc
        IS_REPORTED, //puabpuabdoc
        TIME_FLAG // untuk pelaporan pagi, siang, sore
    }
}
