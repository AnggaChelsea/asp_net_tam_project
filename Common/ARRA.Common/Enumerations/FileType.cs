﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Enumerations
{
    public enum FileType
    {
        EXCEL,
        CSV,
        DELIMITED,
        FIXLENGTH,
        ACCESS
    }
}
