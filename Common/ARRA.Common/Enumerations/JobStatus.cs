﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Enumerations
{
    public enum JobStatus
    {
        Pending,
        Progress,
        Success,
        Failed,
        Cancelled,
        Direct,
        Cancelling,
        Temp,
        SuccessValidation,
        FailedValidation
    }
}