﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common.Enumerations
{
    public enum QueueJobType
    {
        EXPORT,
        EXPORT_VALIDATION,
        IMPORT,
        PROCESS_DATA_SOURCE,
        TRANSFORM,
        EXPORT_COMPARE,
        EXPORT_SUM_COMPARE,
        CANCEL_PROCESS,
        VALIDATION,
        GENERATE_TF,
        GENERATE_TF_SLIK,
        GENERATE_TF_GLOBAL,
        RECONCILIATION,
        MANUAL_UPLOAD,
        BI_VALIDATION,
        SOURCE_DEPENDENCY,
        PUSH_TXT_TO_BI,
        UPLOAD_ERROROJK,
        UPLOAD_DBLOJK,
        UPLOAD_ERRANTASENA,
        EXPORT_TEMPLATE,
        EXPORT_TEMPLATE_DTL,
        EXPORT_ADHOC,
        BACKUP,
        RESTORE,
        PURGING,
        CLEAN_ACT_SESSION
    }
}