﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Diagnostics;

namespace ARRA.Common
{
    public class Utility
    {
        private static IConfiguration _configuration;
        public Utility(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static string RunCMD(string exeName, string args)
        {
            string output = "";
            try
            {
                using (Process p = new Process())
                {
                    p.StartInfo.Arguments = args;
                    p.StartInfo.FileName = exeName;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.Start();
                    //can get p.Id here.
                    output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return output;
        }

        

        public static Newtonsoft.Json.Linq.JObject GetDateStruct(string val)
        {
            Newtonsoft.Json.Linq.JObject obj = new Newtonsoft.Json.Linq.JObject();
            obj.Add("year", Convert.ToDateTime(val).Year);
            obj.Add("month", Convert.ToDateTime(val).Month);
            obj.Add("day", Convert.ToDateTime(val).Day);
            return obj;
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static string SafeSqlString(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
            {
                return "";
            }
            else
            {
                return inputStr.Replace("'", "''");
            }
        }

        public static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        public static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".csv", "text/csv"},
                {".csv1", "application/vnd.ms-excel"},
                {".zip", "application/zip"}
            };
        }

    }
}