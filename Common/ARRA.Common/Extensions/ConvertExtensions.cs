﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ARRA.Common.Extensions
{
    public static class ConvertExtensions
    {
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            var tb = new DataTable(typeof(T).Name);

            //PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var props = (IDictionary<string, object>)items[0];

            //foreach (var prop in props)
            //{
            //    tb.Columns.Add(prop.Name, prop.PropertyType);
            //}
            foreach (var prop in props)
            {
                tb.Columns.Add(new DataColumn(prop.Key));
            }

            //foreach (var item in items)
            //{
            //    var values = new object[props.Length];
            //    for (var i = 0; i < props.Length; i++)
            //    {
            //        values[i] = props[i].GetValue(item, null);
            //    }

            //    tb.Rows.Add(values);
            //}
            foreach (var row in items)
            {
                var data = tb.NewRow();
                foreach (var prop in (IDictionary<string, object>)row)
                {
                    data[prop.Key] = prop.Value;
                }
                tb.Rows.Add(data);
            }

            return tb;
        }
    }
}
