﻿using System;

namespace ARRA.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
