﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARRA.Common
{
    public class ConvertValue
    {
        public static bool ToBoolean(object obj)
        {
            if (obj == null || string.IsNullOrEmpty(Convert.ToString(obj)))
            {
                return false;
            }
            else
            {
                return Convert.ToBoolean(obj);
            }
        }
        
    }
}
