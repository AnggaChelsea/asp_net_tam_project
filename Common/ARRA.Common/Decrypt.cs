﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace ARRA.Common
{
    public class Decrypt
    {
        //public static string DecryptText(string textToDecrypt)
        //{
        //    try
        //    {
        //        //string textToDecrypt = "6+PXxVWlBqcUnIdqsMyUHA==";
        //        string ToReturn = "";
        //        string publickey = "12345678";
        //        string secretkey = "87654321";
        //        byte[] privatekeyByte = { };
        //        privatekeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
        //        byte[] publickeybyte = { };
        //        publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
        //        MemoryStream ms = null;
        //        CryptoStream cs = null;
        //        byte[] inputbyteArray = new byte[textToDecrypt.Replace(" ", "+").Length];
        //        inputbyteArray = Convert.FromBase64String(textToDecrypt.Replace(" ", "+"));
        //        using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
        //        {
        //            ms = new MemoryStream();
        //            cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
        //            cs.Write(inputbyteArray, 0, inputbyteArray.Length);
        //            cs.FlushFinalBlock();
        //            Encoding encoding = Encoding.UTF8;
        //            ToReturn = encoding.GetString(ms.ToArray());
        //        }
        //        return ToReturn;
        //    }
        //    catch (Exception ae)
        //    {
        //        throw new Exception(ae.Message, ae.InnerException);
        //    }
        //}

        public static string DecryptText(string textToDecrypt, IConfiguration _configuration)
        {
            try
            {              
                byte[] iv = new byte[16];

                using (Aes aes = Aes.Create())
                {
                    //aes.Key = Encoding.UTF8.GetBytes(key);
                    aes.Key = CreateKey(_configuration.GetSection("ApplicationSettings:EncryptionPublicKey").Value);
                    aes.IV = iv;
                    ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                    var buffer = Convert.FromBase64String(textToDecrypt);
                    using (MemoryStream memoryStream = new MemoryStream(buffer))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ae)
            {
                throw new Exception(ae.Message, ae.InnerException);
            }
        }

        private static byte[] CreateKey(string password, int keyBytes = 32)
        {
            byte[] Salt = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80 };
            const int Iterations = 300;
            var keyGenerator = new Rfc2898DeriveBytes(password, Salt, Iterations);
            return keyGenerator.GetBytes(keyBytes);
        }
    }
}
